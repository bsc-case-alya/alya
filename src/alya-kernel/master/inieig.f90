!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine inieig()
  !-----------------------------------------------------------------------
  !****f* master/inieig
  ! NAME
  !    inieig
  ! DESCRIPTION
  !    This subroutine initializes the eigen solver arrays
  !    amatr ... Matrix stiffness
  !    cmass ... Consistent matrix mass
  ! USES
  ! USED BY
  !    Alya
  !***
  !-----------------------------------------------------------------------
  use def_master 
  use def_solver
  implicit none  
  integer(ip)  :: izmat

  if( INOTMASTER ) then 
     
     do izmat = 1,eigen_sol(1)%nzmat 
        amatr(izmat) = 0.0_rp
     end do
     
  end if

end subroutine inieig
 
