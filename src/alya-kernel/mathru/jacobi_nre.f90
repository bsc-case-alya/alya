!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!------------------------------------------------------------------------
!> @addtogroup Mathematics
!> @{
!> @file    jacobi_nre.f90
!> @author  Herbert Owen
!> @brief   computes all the eigenvalues and eigenvectors of real, symmetrix matrix A (3x3)
!> @details The eigenvalues go into matrix D, while the
!!          eigenvectors go into matrix V. NROT is the number of Jacobi rotations
!!          which were required.
!!          Beware A is altered.
!!          *This subroutine is taken from "Numerical Recipes F90", page 1225
!> @}
!------------------------------------------------------------------------

SUBROUTINE jacobi_nre(a,d,v,nrot,callersub)
  USE def_kintyp
  IMPLICIT NONE

  INTEGER(ip), INTENT(OUT) :: nrot
  !  REAL(rp), DIMENSION(:), INTENT(OUT) :: d
  !  REAL(rp), DIMENSION(:,:), INTENT(INOUT) :: a
  !  REAL(rp), DIMENSION(:,:), INTENT(OUT) :: v
  REAL(rp), DIMENSION(3), INTENT(OUT) :: d     ! for the moment I restrict it to dim 3 because asset_eq gives me trouble
  REAL(rp), DIMENSION(3,3), INTENT(INOUT) :: a
  REAL(rp), DIMENSION(3,3), INTENT(OUT) :: v
  character(*)             :: callersub  ! who is calling spcdec

  call runend('JACOBI_NRE: YOU SHOULD CODE THIS.. NOT THAT COMPLEX')

END SUBROUTINE jacobi_nre
