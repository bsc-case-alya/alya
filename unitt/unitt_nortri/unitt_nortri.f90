!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_nortri

  use def_kintyp
  use def_master
  use def_domain
  use mod_elmgeo
  use def_maths_bin
  use def_kermod
  use def_search_method
  use mod_parall
  use def_interpolation_method
  use mod_memory_basic
  use mod_communications
  use mod_random

  implicit none
  real(rp)               :: bouno(3)
  real(rp)               :: vec(3,3),rmod
  integer(ip)            :: iboun,p1,p2,p3

print*,'a'
  call Initia()                                               ! Initialization of the run
print*,'b'
  call Readom()                                               ! Domain reading
print*,'c'
  call Partit()                                               ! Domain partitioning
print*,'d'
  call Reaker()                                               ! Read Kermod
print*,'e'
  call Domtra()                                               ! Domain transformation
print*,'f'
  call Domain()                                               ! Domain construction
print*,'g'

  iboun = 1

  p1             = lnodb(1,iboun)
  p2             = lnodb(2,iboun)
  p3             = lnodb(3,iboun)
  call nortri(p1,p2,p3,coord,vec,ndime)
  bouno(1) = vec(1,3)
  bouno(2) = vec(2,3)
  bouno(3) = vec(3,3)
  print*,'1=',bouno(1:3)/sqrt(dot_product(bouno,bouno))
  
  p1             = lnodb(1,iboun)
  p2             = lnodb(3,iboun)
  p3             = lnodb(4,iboun)
  call nortri(p1,p2,p3,coord,vec,ndime)
  bouno(1) = bouno(1) + vec(1,3)
  bouno(2) = bouno(2) + vec(2,3)
  bouno(3) = bouno(3) + vec(3,3)
  print*,'2=',bouno(1:3)/sqrt(dot_product(bouno,bouno))

  p1             = lnodb(1,iboun)
  p2             = lnodb(2,iboun)
  p3             = lnodb(4,iboun)
  call nortri(p1,p2,p3,coord,vec,ndime)
  bouno(1) = bouno(1) + vec(1,3)
  bouno(2) = bouno(2) + vec(2,3)
  bouno(3) = bouno(3) + vec(3,3)
  print*,'3=',bouno(1:3)/sqrt(dot_product(bouno,bouno))

  p1             = lnodb(4,iboun)
  p2             = lnodb(2,iboun)
  p3             = lnodb(3,iboun)
  call nortri(p1,p2,p3,coord,vec,ndime)
  bouno(1) = bouno(1) + vec(1,3)
  bouno(2) = bouno(2) + vec(2,3)
  bouno(3) = bouno(3) + vec(3,3)
  print*,'4=',bouno(1:3)/sqrt(dot_product(bouno,bouno))

  
  !
  ! HEX: We assume that ordering points towards inside
  !
  rmod = sqrt(  bouno(1) * bouno(1) &
       &      + bouno(2) * bouno(2) &
       &      + bouno(3) * bouno(3) )
  bouno(1) = bouno(1) / rmod
  bouno(2) = bouno(2) / rmod
  bouno(3) = bouno(3) / rmod

  if( abs(bouno(1))        > zeror ) stop 1
  if( abs(bouno(2))        > zeror ) stop 1
  if( abs(bouno(3)+1.0_rp) > zeror ) stop 1
  
  call runend('O.K.!')

end program unitt_nortri
 
