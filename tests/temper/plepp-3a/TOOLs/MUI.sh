#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



#BSUB -n 17    
#BSUB -R"span[ptile=16]"
#BSUB -o OUT00.txt
#BSUB -e ERR00.txt
#BSUB -J 2016MAR29  
#BSUB -W 00:29    
# #BSUB -q bsc_case  

module purge
module load PYTHON/2.7.3
module load  gcc/4.9.1 intel/15.0.2 openmpi/1.8.1

export ALYAi=3  
export ALYAj=3    

export ALYA_PATH=/home/bsc21/bsc21704/z2016/REPOSITORY/ALYAs/ALYA_2016AUG24
time mpirun -np $ALYAi $ALYA_PATH/Executables/mui00/Alya.x Interior01 --name NEUMA : -np $ALYAj $ALYA_PATH/Executables/mui00/Alya.x vortex2D --name DIRIC


#
# vortex2D.dat 
#  CODE:               2
#  ALYA:               DIRIC 
# 
# Interior01.dat  
#  CODE:               1
#  ALYA:               NEUMA
# 
