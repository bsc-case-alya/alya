!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup mpio
!> @{
!> @file    mod_mpio_seq_log.f90
!> @author  Damien Dosimont
!> @date    27/09/2017
!> @brief   MPI-IO log manager (sequential)
!> @details This module records information on the I/O operations (sequential)
!>          Required MPIOLOG to be defined
!>          \verbatim
!>          Output format:
!>          FIELD, IO OPERATION, PAR/SEQ, FORMAT, IO WORKING PROCESSES, ALL IO PROCESSES, FILE SIZE (B), START TIMESTAMP (s), END TIMESTAMP (s), DURATION (s)
!>          \endverbatim
!> @}
!-----------------------------------------------------------------------

 module mod_mpio_seq_log

    use def_kintyp,                     only : ip,rp,lg,r1p
    use def_master,                     only : intost, retost, namda
    use def_master,                     only : IMASTER, ISEQUEN

    implicit none

    private

    character(150)                      :: fil_timer

    integer, parameter                  :: u=19589453

    real(rp)                            :: time1, time2

    character(20)                       :: s_object, s_operation, s_format, s_par, s_workers, s_code_size
    character(25)                       :: s_file_size

    public                              :: start_timer, end_timer, write_data, openfile_timer, closefile_timer, u, fil_timer, time1, time2, s_object, s_operation, s_format, s_par, s_workers, s_code_size, s_file_size

    contains

    subroutine start_timer(object, operation, format, workers, code_size, file_size)
        character(*), intent(in)                        :: object, operation, format
        integer(ip), intent(in), optional               :: workers, code_size
        integer(8), intent(in), optional               :: file_size
#ifdef MPIOLOG
        s_object=object
        s_operation=operation
        s_format=format
        s_par='SEQ'
        s_workers='1'
        s_code_size='1'
        s_file_size='0'
        if (present(code_size)) then
            s_code_size=intost(code_size)
        end if
        if (present(file_size)) then
            write(s_file_size,*) file_size
            s_file_size = adjustl(s_file_size)
        end if
        if (IMASTER .or. ISEQUEN) then
            call cputim(time1)
        end if
#endif
    end subroutine

    subroutine end_timer()
#ifdef MPIOLOG
        if (IMASTER .or. ISEQUEN) then
            call cputim(time2)
            call openfile_timer()
            call write_data()
            call closefile_timer()
        end if
#endif
    end subroutine

    subroutine write_data()
        write(u,*) trim(s_object), ", ", trim(s_operation), ", ", trim(s_par), ", ", trim(s_format), ", ", trim(s_workers), ", ",&
             trim(s_code_size), ", ", trim(s_file_size), ", ", trim(retost(time1)), ", ", trim(retost(time2)), ", ",&
             trim(retost(time2-time1))
    end subroutine


    subroutine openfile_timer()
        logical                         :: exist
        integer                         :: stat
        fil_timer = adjustl(trim(namda))//".mpio.log"
        open(unit=u, iostat=stat, file=fil_timer, status='old')
        if (stat == 0) then
            close (u)
            open(u, file=fil_timer, status="old", position="append", action="write", recl=1000)
        else
            close (u)
            open(u, file=fil_timer, status="new", action="write", recl=1000)
        end if
    end subroutine

    subroutine closefile_timer()
        close(u)
    end subroutine

end module
