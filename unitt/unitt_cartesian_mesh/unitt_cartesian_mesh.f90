!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_cartesian_mesh

  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_maths_bin
  use def_master
  use mod_memory
  
  implicit none
  real(rp),             parameter :: epsil = 1.0e-12_rp
  type(mesh_type_basic)           :: mesh
  integer(ip)                     :: ipoin,idime,inodb
  integer(ip)                     :: pelty,ii,iboun
  integer(8)                      :: memor_loc(2)
  integer(8)                      :: memor_tmp(2)
  real(rp)                        :: comin(3),comax(3)
  real(rp),             pointer   :: bobox(:,:,:)
  !
  ! BIN
  !
  call elmgeo_element_type_initialization()
  call mesh % init()
  call mesh % cartesian_mesh(3_ip,(/10_ip,10_ip,20_ip/),(/-1.0_rp,-2.0_rp,-3.0_rp/),(/1.0_rp,2.0_rp,3.0_rp/))
  call mesh % print_info()
  call mesh % output (FILENAME='bin')
  
end program unitt_cartesian_mesh
 
