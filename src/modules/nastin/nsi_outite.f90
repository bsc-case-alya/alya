!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_outite()
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_outite
  ! NAME 
  !    nsi_outite
  ! DESCRIPTION
  ! USES
  ! USED BY
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_nastin
  implicit none
  !
  ! End of an inner iteration
  !
  if( ISEQUEN ) then
     if(itinn(modul)==kfl_psmat_nsi) then
        kfl_psmat_nsi=0
        call pspltm(&
             npoin,npoin,solve(ivari_nsi)%ndofn,0_ip,c_dom,r_dom,amatr,&
             trim(title)//': '//namod(modul),0_ip,18.0_rp,'cm',&
             0_ip,0_ip,2_ip,lun_psmat_nsi)
        call nsi_openfi(8_ip)
     end if
  end if

end subroutine nsi_outite
