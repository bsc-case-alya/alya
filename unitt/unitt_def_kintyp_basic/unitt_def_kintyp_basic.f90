!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_def_kintyp_basic 
  
  use def_kintyp, only : ip, rp

  implicit none

  if (ip == 4 .or. ip == 8) then
    print *, "ip value is ", ip
  else
    print *, "ip value is invalid: ", ip
    stop 1
  end if
  if (rp == 4 .or. rp == 8 .or. rp == 16) then
    print *, "rp value is ", rp
  else
    print *, "rp value is invalid: ", rp
    stop 1
  end if

  stop 0
  
end program unitt_def_kintyp_basic
