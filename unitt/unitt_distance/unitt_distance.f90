!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_distance

  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_maths_bin
  use def_maths_tree
  use def_maths_octbin
  use def_master
  use mod_memory
  use def_kermod
  use def_domain
  use def_interpolation_method
  
  implicit none
  real(rp),             parameter :: epsil = 1.0e-12_rp
  type(mesh_type_basic)           :: mesh_out
  type(mesh_type_basic)           :: mesh_out2
  type(interpolation)             :: interp
  type(maths_skdtree)              :: skdtree
  type(maths_octree)              :: oct
  type(maths_bin)                 :: bin
  integer(ip)                     :: ipoin
  real(rp),             pointer   :: bobox(:,:,:)
  real(rp),             pointer   :: dista(:)
  real(rp),             pointer   :: res(:,:)
  character(len=5),     pointer   :: names(:)

  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction

  nullify(bobox,dista,res,names)
  allocate(dista(npoin))
  !
  ! Boundary bounding boxes
  !
  call meshe(ndivi) % boundary_bb(bobox)
  !
  ! Search strategy
  !
  call skdtree % init          ()
  call skdtree % input         ()!UNSORTED_DIVISION = .false.)
  call skdtree % fill          (bobox=bobox)

  call oct    % init          ()
  call oct    % input         (limit=4_ip)
  call oct    % fill          (bobox=bobox)

  call bin    % init          ()
  call bin    % input         (boxes=(/50_ip,50_ip/)) 
  call bin    % fill          (BOBOX=bobox)
  !
  ! Interpolation strategy
  !
  call interp % init       ()
  call interp % input      (oct,&
       &                   INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION,&
       &                   NAME='FIRST TRY')
  call interp % preprocess (meshe(ndivi) % coord,meshe(ndivi))
  call interp % distances  (meshe(ndivi) % coord,dista)

  call interp % init       ()
  call interp % input      (skdtree,&
       &                   INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION,&
       &                   NAME='FIRST TRY')
  call interp % preprocess (meshe(ndivi) % coord,meshe(ndivi))
  call interp % distances  (meshe(ndivi) % coord,dista)
  !
  ! Output mesh
  !
  call mesh_out % init()
  call oct      % mesh(&
       mesh_out % ndime,mesh_out % mnode,mesh_out % nelem,&
       mesh_out % npoin,mesh_out % lnods,mesh_out % ltype,&
       mesh_out % coord)
  call oct      % results(res,names)

  call mesh_out % output (          FILENAME='octree')
  call mesh_out % results(res,names,FILENAME='octree')

  deallocate(res)
  
  call mesh_out2 % init()
  call skdtree    % mesh(&
       mesh_out2 % ndime,mesh_out2 % mnode,mesh_out2 % nelem,&
       mesh_out2 % npoin,mesh_out2 % lnods,mesh_out2 % ltype,&
       mesh_out2 % coord)
  call skdtree    % results(res,names)

  call mesh_out2 % output (          FILENAME='skdtree')
  call mesh_out2 % results(res,names,FILENAME='skdtree')
 
  call runend('O.K.!')

end program unitt_distance
 
