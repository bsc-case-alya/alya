!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_exchange
  !
  ! Order strings
  !
  use def_kintyp_basic, only : ip,rp,lg
  use def_mpio
  use mod_restart
  use def_master
  use def_communications
  use mod_communications
  use mod_exchange
  use mod_parall
  implicit none

  type datos
     integer(ip)   :: as, a1 (2), a2 (2,2), a3 (3,3)
     real(rp)      :: bs, b1 (2), b2 (2,2), b3 (3,3)
     logical(lg)   :: cs, c1 (2)
     character(5)  :: ws, w1 (2)
  end type datos
  type(datos)   :: before
  type(datos)   :: after
  integer(ip)   :: my_rank,my_size

  INOTSLAVE     = .true.
  ISEQUEN       = .false. 
  IPARALL       = .true.
  call PAR_INIT()
  PAR_COMM_MY_CODE = MPI_COMM_WORLD
  call PAR_COMM_RANK_AND_SIZE(PAR_COMM_MY_CODE,my_rank,my_size)

  if( my_rank == 0 ) then
     IMASTER   = .true.
     ISLAVE    = .false.
     INOTSLAVE = .true.
  else
     IMASTER   = .false.
     ISLAVE    = .true.
     INOTSLAVE = .false.     
  end if

  before % as      = 1
  before % a1      = (/2,3/)
  before % a2      = reshape( (/4,5,6,7/) , (/2,2/) ) 
  before % a3      = reshape( (/8,9,10,11,12,13,14,15,16/) , (/3,3/) ) 

  before % bs      = 1.0_rp
  before % b1      = (/2.0_rp,3.0_rp/)
  before % b2      = reshape( (/4.0_rp,5.0_rp,6.0_rp,7.0_rp/) , (/2,2/) ) 
  before % b3      = reshape( (/8.0_rp,9.0_rp,10.0_rp,11.0_rp,12.0_rp,13.0_rp,14.0_rp,15.0_rp,16.0_rp/) , (/3,3/) ) 

  before % cs      = .true.
  before % c1      = (/.false.,.true./)

  before % ws      = 'ABCDE'
  before % w1(1)   = 'FGHIJ'
  before % w1(2)   = 'KLMNO'
  
  if( IMASTER ) after = before

  call exchange_init()
  call exchange_add(after % as)
  call exchange_add(after % a1)
  call exchange_add(after % a2)
  call exchange_add(after % a3)
  call exchange_add(after % bs)
  call exchange_add(after % b1)
  call exchange_add(after % b2)
  call exchange_add(after % b3)
  call exchange_add(after % cs)
  call exchange_add(after % c1)
  call exchange_add(after % ws)
  call exchange_add(after % w1)
  call exchange_end()

  if(     after % as /=      before % as  ) then
     print*,'as fails'
     stop 1
  end if
  if( abs(after % bs-before % bs)>zeror   )  then
     print*,'bs fails'
     stop 1
  end if
  if(     after % cs .neqv.  before % cs  ) then
     print*,'cs fails'
    stop 1
  end if
  if(     after % ws /=      before % ws  )  then
     print*,'ws fails'
     stop 1
  end if
  
  if( any(after % a1-before % a1/=0) ) then
     print*,'a1 fails'
     stop 1
  end if
  if( any(after % a2-before % a2/=0) ) then
     print*,'a2 fails'
     stop 1
  end if
  if( any(after % a3-before % a3/=0) ) then
     print*,'a3 fails'
     stop 1
  end if
  if( any(abs(after % b1-before % b1)>zeror) )  then
     print*,'b1 fails'
     stop 1
  end if
  if( any(abs(after % b2-before % b2)>zeror) )  then
     print*,'b2 fails'
     stop 1
  end if
  if( any(abs(after % b3-before % b3)>zeror) )  then
     print*,'b3 fails'
     stop 1
  end if
  if( any(after % c1 .neqv. before % c1) ) then
     print*,'c1 fails'
     stop 1
  end if
  if(     after % w1(1) /=      before % w1(1)  )  then
     print*,'w1 fails'
     stop 1
  end if
  if(     after % w1(2) /=      before % w1(2)  )  then
     print*,'w1 fails'
     stop 1
  end if
  
  call par_finali(1_ip)
  
  if( IMASTER ) print*,'unitt exchange is ok, yupiiiii'
  
end program unitt_exchange
