!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_signed_distance

  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_maths_bin
  use def_maths_tree
  use def_maths_octbin
  use def_master
  use mod_memory
  use def_kermod
  use def_domain
  use def_interpolation_method
  
  implicit none
  real(rp),             parameter :: epsil = 1.0e-12_rp
  type(bmsh_type_basic)           :: mesh
  type(interpolation)             :: interp
  type(maths_skdtree)             :: skdtree
  real(rp),             pointer   :: bobox(:,:,:)
  real(rp),             pointer   :: dista(:),xx(:,:)
  integer(ip)                     :: nn
  integer(8)                      :: count_rate8
  !
  ! Initialize system clock
  !
  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)
  !
  ! Initialize mesh
  !
  call elmgeo_element_type_initialization()
  nullify(bobox,dista,xx)

  IPARALL = .false.
  nn = 2
  allocate(dista(nn),xx(2,nn))

  xx(:,1) = [1.5_rp,0.5_rp]
  xx(:,2) = [1.5_rp,1.5_rp]
  
  call mesh % init()
  mesh % npoin = 3
  mesh % nelem = 2
  mesh % ndime = 2
  mesh % mnode = 2
  call mesh % alloca()
  mesh % coord(:,1) = [0.0_rp,1.0_rp]
  mesh % coord(:,2) = [1.0_rp,1.0_rp]
  mesh % coord(:,3) = [2.0_rp,1.0_rp]
  mesh % lnods(:,1) = [1,2]
  mesh % lnods(:,2) = [2,3]
  mesh % ltype      = BAR02
  call mesh % element_bb(bobox)
  !
  ! Search strategy
  !
  call skdtree % init          ()
  call skdtree % input         ()
  call skdtree % fill          (bobox=bobox)
  !
  ! Interpolation strategy
  !
  call interp % init       ()
  call interp % input      (skdtree,&
       &                   INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION,&
       &                   NAME='FIRST TRY')
  call interp % preprocess (xx,mesh)
  call interp % distances  (xx,dista)
  !
  ! Verify signed distance
  !
  if( sum(abs(interp % dista(1:nn) - [0.5_rp,-0.5_rp])) > 1.0e-12_rp ) then
     print*,     'Signed distance of point to boundary element is not computed correctly!'
     call runend('Signed distance of point to boundary element is not computed correctly!')
     stop 1
  end if

end program unitt_signed_distance
 
