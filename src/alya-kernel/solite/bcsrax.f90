!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!------------------------------------------------------------------------
!> @addtogroup Algebraic_Solver
!> @addtogroup Krylov_Solver
!> @{
!> @file    bcsrax.f90
!> @author  Guillaume Houzeaux
!> @brief   SpMV
!> @details Parallel SpMV
!>          Multiply a matrix by a vector YY = A XX
!>          where A is in CSR, COO or ELL format. This is the parallel
!>          version, including blocking and non-blocking send-receive
!>          MPI functions.
!>
!>          INPUT
!>             NBNODES .... Number of equations
!>             NBVAR ...... Number of variables
!>             AN ......... Matrix
!>             JA ......... List of elements
!>             IA ......... Pointer to list of elements
!>             XX ......... Vector
!>          OUTPUT
!>             YY ......... result vector
!>
!> @}
!------------------------------------------------------------------------

subroutine bcsrax(itask,nbnodes,nbvar,an,ja,ia,xx,yy)

  use def_kintyp,         only     :  ip,rp
  use def_master,         only     :  INOTMASTER,kfl_paral
  use def_solver,         only     :  solve_sol
  use mod_solver,         only     :  solver_parallel_SpMV
  implicit none
  integer(ip), intent(in)          :: itask,nbnodes,nbvar
  real(rp),    intent(in)          :: an(nbvar,nbvar,*)
  integer(ip), intent(in)          :: ja(*),ia(*)
  real(rp),    intent(inout)       :: xx(nbvar,*)
  real(rp),    intent(out), target :: yy(nbvar,*)

  if( itask == 0 ) then
     call solver_parallel_SpMV(solve_sol(1),an,xx,yy,OPENMP=.true.,MPI=.false.,TIMING=.true.)
  else
     if( solve_sol(1) % omp_interface == 0 ) then
        call solver_parallel_SpMV(solve_sol(1),an,xx,yy,OPENMP=.true.,TIMING=.true.,OPENMP_INTERFACE=.false.)
     else
        call solver_parallel_SpMV(solve_sol(1),an,xx,yy,OPENMP=.true.,TIMING=.true.)
     end if
  end if

end subroutine bcsrax
