!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_boupre(tract, bovel, gbcar, baloc, gbsha, gbvis, lnodb, pnodb) 

  use def_master,     only : ittim
  use def_kintyp,     only : ip, rp
  use def_nastin,     only : bprfs_nsi, nbpoi_nsi, kfl_neufs_nsi
  use def_domain,     only : npoin, ndime, mnodb

  implicit none

  real(rp),    intent(in) :: tract(ndime)
  real(rp),    intent(in) :: baloc(ndime,ndime)
  real(rp),    intent(in) :: bovel(ndime,mnodb)
  real(rp),    intent(in) :: gbcar(ndime,mnodb)
  real(rp),    intent(in) :: gbsha(mnodb)
  real(rp),    intent(in) :: gbvis 
  integer(ip), intent(in) :: lnodb(mnodb)
  integer(ip), intent(in) :: pnodb
  real(rp)                :: gbdst(ndime,ndime)
  real(rp)                :: gbgve(ndime,ndime)
  real(rp)                :: gbvel(ndime)
  real(rp)                :: tracn, ntaun, presgb
  integer(ip)             :: inodb, ipoin, idime, jdime

  if (kfl_neufs_nsi == 1_ip) then

     ! Interpolate velocity to gauss points
     gbvel(1:ndime) = 0.0_rp
     do inodb = 1,pnodb
        gbvel(1:ndime) = gbvel(1:ndime) + bovel(1:ndime,inodb) * gbsha(inodb)
     end do

     ! Compute velocity gradients at gauss points
     gbgve(1:ndime,1:ndime) = 0.0_rp
     do inodb = 1,pnodb
        do idime = 1,ndime
           do jdime = 1,ndime
              gbgve(jdime,idime) = gbgve(jdime,idime) &
                 + gbcar(jdime,inodb) * gbvel(idime)
           end do
        end do
     end do

     ! Compute deviatoric stress tensor at gauss point 
     gbdst(1:ndime,1:ndime) = gbvis * gbgve(1:ndime,1:ndime)

     ! Compute prescription pressure at gauss point:
     ! p^g = n^g_i * tau^g_ij * n^g_j - n^g_i * t^g_i
     tracn = 0.0_rp
     ntaun = 0.0_rp
     do idime = 1,ndime
        tracn = tracn + baloc(idime,ndime) * tract(idime)
        do jdime = 1,ndime
           ntaun = ntaun + baloc(jdime,ndime) * gbdst(jdime,idime) * baloc(idime,ndime)
        end do
     end do
     presgb = ntaun - tracn

     ! Extrapolate prescription pressure from gauss points to nodes
     do inodb = 1,pnodb
        ipoin = lnodb(inodb)
        bprfs_nsi(ipoin) = bprfs_nsi(ipoin) + presgb * gbsha(inodb) / real(nbpoi_nsi(ipoin),rp)
     end do

     if (ittim == 0_ip) then
        bprfs_nsi(1:npoin) = 0.0_rp
     end if

  end if

end subroutine nsi_boupre 
