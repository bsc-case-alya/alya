!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_physics_liquid
  use def_kintyp_basic, only : ip,rp
  use mod_physics,      only : liquid_state
  use mod_physics,      only : physics_initialize_liquid
  use mod_physics,      only : physics_set_liquid_temperature
  use mod_physics,      only : physics_set_liquid_pressure

  type(liquid_state)   :: liq   ! Declare liquid state
  real(rp)             :: T, P


  liq % name = "NHEPT"
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 689.94203019545978 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2204.1037214199523 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 371337.45128511835 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 371.54889666579027 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 3607.9840850230999 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 677.29845844657939 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2257.5278890605687 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 362602.79627669038 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 371.54889666579027 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 7745.5388355867890 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 677.29845844657939 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2257.5278890605687 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 362602.79627669038 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 540.20000000000000 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 7745.5388355867890 ) > 1.0e-2_rp ) stop 1

  
      
  liq % name = "NDODE"
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 752.04154041347761 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2180.0065037058139 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 359163.18019221531 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 489.51735791729647 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 7.4728021652556915 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 741.63104851923822 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2220.9736025451707 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 353164.49333345908 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 489.51735791729647 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 26.900769670031941 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 741.63104851923822 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2220.9736025451707 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 353164.49333345908 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 658.00000000000000 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 26.900769670031941 ) > 1.0e-2_rp ) stop 1
      
       
  liq % name = "H2O  "
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 997.56968441567938 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 4196.4731239928333 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 2454777.0844949898 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 373.16783899175135 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 1705.3312719397820 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 993.68400614202653 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 4180.6812963899793 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 2425119.3763280618 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 373.16783899175135 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 4247.9110984349727 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 993.68400614202653 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 4180.6812963899793 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 2425119.3763280618 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 548.74398844880284 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 4247.9110984349727 ) > 1.0e-2_rp ) stop 1
  
  
  


  liq % name = "OME1 "
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 788.07761716573862 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1945.4732645696415 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 371602.81738944486 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 314.97920517764499 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 38479.483562213914 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 770.21630182508807 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2036.9638114751592 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 360086.54914925701 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 314.97920517764499 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 67452.378476165686 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 770.21630182508807 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2036.9638114751592 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 360086.54914925701 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 480.60000000000000 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 67452.378476165686 ) > 1.0e-2_rp ) stop 1
  
  
  liq % name = "OMEX "
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 1070.0621043445751 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1989.7394145288706 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 356656.26114979805 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 439.80738299540064 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 127.34138584654575 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 1055.6964206567930 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1989.9264672261743 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 351543.69549293007 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 439.80738299540064 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 355.50855484704761 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 1055.6964206567930 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1989.9264672261743 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 351543.69549293007 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 649.75760000000000 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 355.50855484704761 ) > 1.0e-2_rp ) stop 1
  

  liq % name = "METOL"
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 796.22031480289024 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2472.2830193779682 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 1205068.7770068303 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 337.70290711005021 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 9739.4081578432906 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 782.65646691685538 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2565.1061989778614 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 1174080.8674971778 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 337.70290711005021 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 21725.942098813102 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 782.65646691685538 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2565.1061989778614 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 1174080.8674971778 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 494.77538672701343 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 21725.942098813102 ) > 1.0e-2_rp ) stop 1
  

  liq % name = "ETHOL"
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 796.47297177018038 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 3539.3788251129959 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 952694.16778310796 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 351.59162307916370 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 4337.5668937207238 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 782.62883439934922 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 3789.2081070549775 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 933953.22716213344 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 351.59162307916370 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 10477.619064282952 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 782.62883439934922 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 3789.2081070549775 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 933953.22716213344 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 512.89870649327418 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 10477.619064282952 ) > 1.0e-2_rp ) stop 1
  
  
  liq % name = "NOCTA"
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 707.03735194068418 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2194.4241554888854 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 368089.05274937127 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 398.83936716838440 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 1040.9832086908373 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 695.23328523794748 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2244.3720861681081 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 360390.34645728901 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 398.83936716838440 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 2464.7498424717305 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 695.23328523794748 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2244.3720861681081 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 360390.34645728901 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 568.70000000000005 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 2464.7498424717305 ) > 1.0e-2_rp ) stop 1
  
  
  liq % name = "NDECA"
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 733.91398358961465 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2183.9406512949654 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 361929.94921613880 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 447.45891648647353 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 87.685859190075931 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 722.88756499175577 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2230.0068701753789 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 355281.78459593409 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 447.45891648647353 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 254.42620899909036 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 722.88756499175577 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2230.0068701753789 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 355281.78459593409 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 617.70000000000005 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 254.42620899909036 ) > 1.0e-2_rp ) stop 1
  
  
  liq % name = "IOCTA"
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 698.60957536170645 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2045.5046091951522 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 311632.44680950366 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 372.36227558237448 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 3988.2195415886895 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 686.26910950816193 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2107.3115916551769 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 305573.40358071588 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 372.36227558237448 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 8320.4873611095591 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 686.26910950816193 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2107.3115916551769 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 305573.40358071588 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 543.96000000000004 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 8320.4873611095591 ) > 1.0e-2_rp ) stop 1
  
  







  liq % name = "124TB"
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 880.04970957002433 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1758.0900331488328 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 399105.83866109024 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 442.56490100194827 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 149.95753853447391 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 868.33084782118829 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1803.8523660578437 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 393115.65501043020 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 442.56490100194827 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 397.48232351065406 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 868.33084782118829 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1803.8523660578437 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 393115.65501043020 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 649.13000000000000 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 397.48232351065406 ) > 1.0e-2_rp ) stop 1
  
  




  liq % name = "135TB"
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 869.35429034551851 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1709.0811000407064 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 399686.65287555236 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 437.97733743136251 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 193.96518898418927 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 857.33079535289914 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1757.4832714136410 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 393073.97318169166 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 437.97733743136251 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 496.31977682736783 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 857.33079535289914 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1757.4832714136410 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 393073.97318169166 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 637.36000000000001 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 496.31977682736783 ) > 1.0e-2_rp ) stop 1
  




  liq % name = "CHEXA"
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 781.99602751278405 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1798.3091957869706 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 398712.66416720691 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 353.79371058198416 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 8250.3859917331029 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 768.60685904063757 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1860.7478301883853 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 389601.17189186218 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 353.79371058198416 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 16417.424273810841 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 768.60685904063757 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1860.7478301883853 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 389601.17189186218 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 553.58000000000004 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 16417.424273810841 ) > 1.0e-2_rp ) stop 1





  liq % name = "TOLUE"
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 873.70769685771768 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1670.9271347438544 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 420660.36112266488 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 383.71081839630540 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 2211.4213244843759 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 860.01679331424020 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1713.0343657565597 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 412516.31101092394 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 383.71081839630540 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 4879.3701825407725 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 860.01679331424020 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 1713.0343657565597 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 412516.31101092394 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 591.79999999999995 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 4879.3701825407725 ) > 1.0e-2_rp ) stop 1
  
 


  
  liq % name = "NUNDE"
  T = 288.15
  P = 101325.0
  call physics_initialize_liquid(liq, T, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 743.56408662542242 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2183.6767532032154 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 362508.00955150550 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 469.08621008445186 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 24.889664085326505 ) > 1.0e-2_rp ) stop 1

  T = 303.15
  call physics_set_liquid_temperature(liq, T)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 732.90471422940095 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2227.6467513000021 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 356132.79686458287 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 469.08621008445186 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 81.120189081181707 ) > 1.0e-2_rp ) stop 1

  P = 60e5
  call physics_set_liquid_pressure(liq, P)
  print*, liq % name, " @ ", T, " K, ", P, " Pa:"
  print*, "rho   = ", liq % rho 
  print*, "cp    = ", liq % cp 
  print*, "Lv    = ", liq % Lv 
  print*, "Tsat  = ", liq % Tsat
  print*, "psat  = ", liq % psat  
  print*, "rho "; if( abs( liq % rho  - 732.90471422940095 ) > 1.0e-3_rp ) stop 1
  print*, "cp  "; if( abs( liq % cp   - 2227.6467513000021 ) > 1.0e-2_rp ) stop 1
  print*, "Lv  "; if( abs( liq % Lv   - 356132.79686458287 ) > 1.0_rp )    stop 1
  print*, "Tsat"; if( abs( liq % Tsat - 639.00000000000000 ) > 1.0e-3_rp ) stop 1
  print*, "psat"; if( abs( liq % psat - 81.120189081181707 ) > 1.0e-2_rp ) stop 1
  
  
  
  
end program unitt_physics_liquid
