!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine voltet(x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4,vol)

!-----------------------------------------------------------------------
!
! Computes the volume of a tetrahedra
!
!-----------------------------------------------------------------------
  use      def_kintyp
  use      def_master, only : zeror
  implicit none
  real(rp),    intent(in)  :: x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4
  real(rp),    intent(out) :: vol

  real(rp)                 :: norm,pscal
  real(rp)                 :: xp,yp,zp,x1p,y1p,z1p

  xp = (y2-y1)*(z3-z1)-(z2-z1)*(y3-y1)
  yp = (z2-z1)*(x3-x1)-(x2-x1)*(z3-z1)
  zp = (x2-x1)*(y3-y1)-(y2-y1)*(x3-x1)

  norm = sqrt(xp*xp+yp*yp+zp*zp)

  if (abs(norm) > zeror) then

     xp = xp/norm
     yp = yp/norm
     zp = zp/norm

     x1p = x4-x1
     y1p = y4-y1
     z1p = z4-z1

     pscal = xp*x1p+yp*y1p+zp*z1p

     vol = norm*abs(pscal)/6.0_rp

  else

     vol = 0.0_rp

  end if

end subroutine voltet
