#!/usr/bin/python
#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



#import numpy as np
import sys
import os
import glob
import math

#===============================================================================================# 
INIT_KEY  = "COORDINATES"
END_KEY   = "END_" + INIT_KEY

def Read_alya_geo(fname):
    data = open(fname, "r")
    lines = data.readlines()
    data.close()

    nline = len(lines)
    global INIT_KEY
    INIT_KEY = INIT_KEY.replace("_", "") 
    INIT_KEY = INIT_KEY.replace("-", "") 
    INIT_KEY = INIT_KEY.replace("&", "") 
    END_KEY  = "END" + INIT_KEY 

    ok  = False 
    IDs = []
    for i in range(nline):
      line = lines[i]
      if(not line.find(INIT_KEY)<0): IDs.append(i+1)
      if(not line.find(END_KEY)<0):  IDs.append(i+0) 

    XYZ = []      
    for i in range(IDs[0], IDs[1]-1):
      line = lines[i]
      line = line.strip() 
      line = line.split()
      XYZ.append([eval(val) for val in line[1:]]) 
    
    print "  |_No elements:", len(XYZ)
    return XYZ 


def Write_file(fname, data, dime=1):
  ndata = len(data)
  fdata = open(fname, "w") 
  for i in range(ndata): 
    line = data[i] 
    print>> fdata, i+1, 
    if(dime>1):
      for j in range(dime): print>> fdata, line[j],  
    else: 
      print>> fdata, line, 
    print>> fdata
    
#===============================================================================================# 

FILE = sys.argv[1]
pts  = Read_alya_geo(FILE) 

VELOC = []
CON01 = []
CON02 = []
CON03 = []
CON04 = []
x_0 = 0.5
y_0 = 0.75
R   = 0.15

for pt in pts:
      u = -math.sin(math.pi * pt[0])**2 * math.sin(2.0*math.pi * pt[1])
      v =  math.sin(math.pi * pt[1])**2 * math.sin(2.0*math.pi * pt[0])
      r = math.sqrt( (pt[0] - x_0) **2 + (pt[1] - y_0) **2 )
      if ( r < R ):
        CON03.append( 1.0 )
      else:
        CON03.append( 0.0 )
      VELOC.append( [u,v] )
      CON01.append( 0.0 )
      CON02.append( 0.0 )
      CON04.append( 0.0 )

Write_file("VELOC.alya", VELOC, 2)
print "VELOC" 
Write_file("CON01.alya", CON01)
print "CON01"
Write_file("CON02.alya", CON02)
print "CON02"
Write_file("CON03.alya", CON03)
print "CON03"
Write_file("CON04.alya", CON04)
print "CON04"
#===============================================================================================# 
print "OK!! \n\n"
