#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import cantera as ct
import numpy as np


#
# Mixing molar fractions
#
def mix(X1,X2,weight1):
	ret = {}
	for k in X1.keys():
		if k in ret.keys():
			ret[k] += weight1*X1[k]
		else:
			ret[k] = weight1*X1[k]
	
	weight2 = 1.0 - weight1
	for k in X2.keys():
		if k in ret.keys():
			ret[k] += weight2*X2[k]
		else:
			ret[k] = weight2*X2[k]
		
	return ret

def psat(T):
	ppar = {}
	ppar[1] = 73.649
	ppar[2] = -7258.2
	ppar[3] = -7.3037
	ppar[4] = 4.1653e-06
	ppar[5] = 2.0

	psat    = np.exp(ppar[1] + ppar[2]/T + ppar[3]*np.log(T) + ppar[4]*T**ppar[5])

	return psat

def rho(T):
	Tc 		= 647.13
	rhopar = {}
	rhopar[1] = 98.343885
	rhopar[2] = 0.30542
	rhopar[3] = Tc
	rhopar[4] = 0.081

	rho = rhopar[1] / (rhopar[2]**(1.0 + (np.maximum(1.0e-8,(1.0 - T/rhopar[3]))**rhopar[4]))) 

	return rho



gas = ct.Solution('gri30.cti')


#
# Temperatures, pressure
#
Tenv = 23 + 273.15 # K
Tbod = 34 + 273.15 # K
P    = 101325.0    # Pa

#
# Relative humidity: 
#
RHenv = 0.2
XWaterEnv  = psat(Tenv) * RHenv / P
RHbod = 0.8
XWaterBod  = psat(Tbod) * RHbod / P

#
# Volume or Molar fractions:
#
XdryAir = {'O2': 0.21, 'N2': 0.79}
Xliq    = {'H2O':1.0}

Xenv = mix(Xliq,XdryAir,XWaterEnv)
Xbod = mix(Xliq,XdryAir,XWaterBod)

gas.TPX = Tbod, P, Xbod
print('Gas in sneeze: (@ T={}, RH={})'.format(Tbod,RHbod))
print(gas.report())

gas.TPX = Tenv, P, Xenv
print('###################')
print('###################')
print('###################')
print('Gas in environment: (@ T={}, RH={})'.format(Tenv,RHenv))
print(gas.report())


print('###################')
print('###################')
print('###################')
print('Density of liquid:')
print(rho(Tbod))

print('Modified correction factor in cough function: 3.29 * 1.89e-7 * rho(Tbod)')
print(3.29 * 1.89e-7 * rho(Tbod) )
