!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_maths_linear_regressions 
  
   use def_kintyp_basic,      only : ip, rp
   use mod_maths_geometry,    only : maths_boxes_union_measure
  
   implicit none

   integer(ip)        :: ndime,nbox
   real(rp), pointer  :: lbox(:,:) 
   real(rp)           :: res
   integer(ip)        :: icont
   
   !
   ! Example 1D
   !
   ndime = 1
   nbox  =33
   nullify(lbox)
   allocate(lbox(ndime*2,nbox))

   lbox(1,1) = -0.5_rp  
   lbox(2,1) =  1.2_rp  
   
   lbox(1,2) = 0.1_rp  
   lbox(2,2) = 1.1_rp  

   lbox(1,3) = 3.0_rp  
   lbox(2,3) = 4.3_rp  
   
   res = maths_boxes_union_measure(1_ip,3_ip,lbox)
   if( abs(res-3) > 1.e-12) stop 3
   deallocate(lbox)
  
   !
   ! Example 2D
   !
   ndime = 2
   nbox  = 4
   nullify(lbox)
   allocate(lbox(ndime*2,nbox))

   lbox(1,1) = -1.0_rp  
   lbox(2,1) = -1.0_rp  
   lbox(3,1) =  1.0_rp  
   lbox(4,1) =  1.0_rp  
   
   lbox(1,2) = -2.0_rp  
   lbox(2,2) = -2.0_rp  
   lbox(3,2) =  2.0_rp  
   lbox(4,2) =  2.0_rp  
   
   lbox(1,3) =  1.0_rp  
   lbox(2,3) =  1.0_rp  
   lbox(3,3) =  6.0_rp  
   lbox(4,3) =  3.0_rp  
   
   lbox(1,4) =  7.0_rp  
   lbox(2,4) = -2.0_rp  
   lbox(3,4) =  8.0_rp  
   lbox(4,4) = -1.0_rp 

   res = maths_boxes_union_measure(2_ip,4_ip,lbox)
   if( abs(res-26) > 1.e-12) stop 4
   deallocate(lbox)
  
   !
   ! Example 3D
   !
   ndime = 3
   nbox  = 3
   nullify(lbox)
   allocate(lbox(ndime*2,nbox))

   lbox(1,1) = -1.0_rp  
   lbox(2,1) = -1.0_rp  
   lbox(3,1) = -5.0_rp  
   lbox(4,1) =  1.0_rp  
   lbox(5,1) =  1.0_rp  
   lbox(6,1) =  5.0_rp  
   
   lbox(1,2) = -2.0_rp  
   lbox(2,2) = -2.0_rp  
   lbox(3,2) = -2.0_rp  
   lbox(4,2) =  2.0_rp  
   lbox(5,2) =  2.0_rp  
   lbox(6,2) =  2.0_rp  
   
   lbox(1,3) =  1.0_rp  
   lbox(2,3) =  1.0_rp  
   lbox(3,3) =  -3_rp  
   lbox(4,3) =  6.0_rp  
   lbox(5,3) =  3.0_rp  
   lbox(6,3) =  3.0_rp  
   
   res = maths_boxes_union_measure(3_ip,3_ip,lbox)
   if( abs(res-144) > 1.e-12) stop 5
   deallocate(lbox)
 
end program unitt_maths_linear_regressions
