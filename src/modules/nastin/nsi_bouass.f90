!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_bouass(&
     pevat,ndofn,pnodb,lboel,gbsha,gbsur,tract,wmatr,&
     wrhsi,elmat,elrhs)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_bouass
  ! NAME 
  !    nsi_bouass
  ! DESCRIPTION
  ! USES
  ! USED BY
  !    nsi_matrix
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_domain, only     :  ndime
  implicit none
  integer(ip), intent(in)  :: pevat,ndofn,pnodb
  integer(ip), intent(in)  :: lboel(pnodb)
  real(rp),    intent(in)  :: wmatr(pevat,pevat),wrhsi(pevat)
  real(rp),    intent(in)  :: gbsha(pnodb),gbsur,tract(ndime)
  real(rp),    intent(inout) :: elmat(pevat,pevat),elrhs(pevat)
  integer(ip)              :: ievat,jevat,inodb,idime,ievab

  do ievat = 1,pevat
     do jevat = 1,pevat
        elmat(jevat,ievat) = elmat(jevat,ievat) + gbsur * wmatr(jevat,ievat)
     end do
     elrhs(ievat) = elrhs(ievat) + gbsur * wrhsi(ievat)
  end do

  do inodb = 1,pnodb
     ievab = (lboel(inodb)-1) * ndofn
     do idime = 1,ndime
        ievab = ievab + 1
        elrhs(ievab) = elrhs(ievab) + gbsur * tract(idime) * gbsha(inodb)
     end do
  end do

end subroutine nsi_bouass
