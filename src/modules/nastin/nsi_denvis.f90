!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_denvis()
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_denvis
  ! NAME 
  !    nsi_denvis
  ! DESCRIPTION
  !    Smoothing of density and viscosity
  ! USES
  !    postpr
  !    memgen
  ! USED BY
  !    nsi_output
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_kermod
  use def_domain
  use def_nastin
  use mod_ker_proper 
  use mod_memory,  only : memory_alloca, memory_deallo
  implicit none
  integer(ip)       :: ipoin,ndofn,dummi
  real(rp)          :: xfact
  real(rp), pointer :: prope_tmp(:) 

  if( INOTMASTER ) then 

     nullify ( prope_tmp )
     call memory_alloca(mem_modul(1:2,modul),'PROPE_TMP','nsi_denvis',prope_tmp,npoin)
     call ker_proper('DENSI','NPOIN',dummi,dummi,prope_tmp)
     do ipoin = 1,npoin
        prope_nsi(1,ipoin) = prope_tmp(ipoin)
     end do
     call ker_proper('VISCO','NPOIN',dummi,dummi,prope_tmp)
     do ipoin = 1,npoin
        prope_nsi(2,ipoin) = prope_tmp(ipoin)
     end do
     call memory_deallo(mem_modul(1:2,modul),'PROPE_TMP','nsi_denvis',prope_tmp)
     
  end if

end subroutine nsi_denvis
