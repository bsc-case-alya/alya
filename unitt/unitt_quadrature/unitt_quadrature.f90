!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_quadrature
  !
  ! Read a mesh and output it
  !
  use def_kintyp_mesh_basic,   only : mesh_type_basic
  use def_elmtyp,              only : BAR3D,QUA04,HEX08
  use mod_elmgeo,              only : elmgeo_element_type_initialization
  use mod_elmgeo,              only : elmgeo_cartesian_derivatives_jacobian
  use mod_elmgeo,              only : element_type
  use mod_element_integration, only : element_shape_function_derivatives_jacobian
  use def_master

  implicit none

  type(mesh_type_basic)         :: mesh

  real(rp),         allocatable :: elcod(:,:)
  real(rp),         allocatable :: gpcar(:,:,:)
  real(rp),         allocatable :: xjaci(:,:,:)
  real(rp),         allocatable :: gpvol(:)
  real(rp),         allocatable :: gpdet(:)
  real(rp)                      :: xx
  integer(ip)                   :: ielem,pgaus,igaus,pdime,ii
  integer(ip)                   :: ipoin1,ipoin2,pnode,pelty
  integer(ip)                   :: inode

  call elmgeo_element_type_initialization()
  !---------------------------------------------------------------------
  !
  ! 1D CASE
  !
  !---------------------------------------------------------------------
  
  call mesh % init('MY_MESH')

  mesh % nelem = 8
  mesh % npoin = 8
  mesh % ndime = 2
  mesh % mnode = 2

  call mesh % alloca()

  mesh % lnods(:,1)  = (/  1,2 /)
  mesh % lnods(:,2)  = (/  2,3 /)
  mesh % lnods(:,3)  = (/  3,4 /) 
  mesh % lnods(:,4)  = (/  4,5 /)
  mesh % lnods(:,5)  = (/  5,6 /) 
  mesh % lnods(:,6)  = (/  6,7 /)  
  mesh % lnods(:,7)  = (/  7,8 /) 
  mesh % lnods(:,8)  = (/  8,1 /) 

  mesh % ltype(1:8)  = BAR3D

  mesh % coord(:,1 ) = (/ 0.0_rp , 0.0_rp /) 
  mesh % coord(:,2 ) = (/ 0.5_rp , 0.0_rp /) 
  mesh % coord(:,3 ) = (/ 1.0_rp , 0.0_rp /) 
  mesh % coord(:,4 ) = (/ 1.0_rp , 0.5_rp /) 
  mesh % coord(:,5 ) = (/ 1.0_rp , 1.0_rp /) 
  mesh % coord(:,6 ) = (/ 0.5_rp , 1.0_rp /) 
  mesh % coord(:,7 ) = (/ 0.0_rp , 1.0_rp /) 
  mesh % coord(:,8 ) = (/ 0.0_rp , 0.5_rp /)

  pgaus = 10
  pdime = 1
  allocate(elcod(1,mesh % mnode))
  allocate(gpcar(mesh % ndime,mesh % mnode,pgaus))
  allocate(xjaci(mesh % ndime,mesh % ndime,pgaus))
  allocate(gpvol(pgaus))
  allocate(gpdet(pgaus))


  pgaus = 10
  pelty = BAR3D
  mesh % quad(pelty) % topo  = element_type(pelty) % topology
  mesh % quad(pelty) % ndime = element_type(pelty) % dimensions
  mesh % quad(pelty) % ngaus = pgaus
  
  do ii = 1,3

     xx = 0.0_rp
     select case ( ii )
     case ( 1_ip ) ; call mesh % quad(pelty) % set(NAME='TRAPEZOIDAL')
     case ( 2_ip ) ; call mesh % quad(pelty) % set(NAME='GAUSS')
     case ( 3_ip ) ; mesh % quad(pelty) % ngaus = mesh % mnode ; call mesh % quad(pelty) % set(NAME='CLOSED')
     end select
     call mesh % iso(pelty)  % set(element_type(pelty) % number_nodes,mesh % quad(pelty))

     do ielem = 1,mesh % nelem

        pelty      = mesh % ltype(ielem)
        pgaus      = mesh % quad(pelty) % ngaus
        pnode      = element_type(pelty) % number_nodes
        ipoin1     = mesh % lnods(1,ielem)
        ipoin2     = mesh % lnods(2,ielem)
        elcod(1,1) = 0.0_rp
        elcod(1,2) = sqrt(dot_product(&
             mesh % coord(:,ipoin1)-mesh % coord(:,ipoin2),&
             mesh % coord(:,ipoin1)-mesh % coord(:,ipoin2)))

        call elmgeo_cartesian_derivatives_jacobian(pdime,pnode,pnode,pgaus,&
             elcod,mesh % iso(pelty) % deriv,xjaci,gpcar,gpdet)

        gpvol(1:pgaus) = mesh % quad(pelty) % weigp(1:pgaus) * gpdet(1:pgaus)
        do igaus = 1,pgaus
           xx = xx +  gpvol(igaus)
        end do
     end do
     
     print*,'x=',ii,xx

     call mesh % iso (pelty) % deallo()
     call mesh % quad(pelty) % deallo()
     
     if( abs(xx-4.0_rp) > 1.0e-08_rp ) then
        print*,'diff=',abs(xx-4.0_rp)
        stop 1 
     end if
     
  end do

  !---------------------------------------------------------------------
  !
  ! 2D CASE
  !
  !---------------------------------------------------------------------
  
  deallocate(elcod)
  deallocate(gpcar)
  deallocate(xjaci)
  deallocate(gpvol)
  deallocate(gpdet)
  call mesh % deallo()
  
  call mesh % init('MY_MESH')

  mesh % nelem = 1
  mesh % npoin = 4
  mesh % ndime = 2
  mesh % mnode = 4

  call mesh % alloca()

  mesh % lnods(:,1)  = (/  1,2,3,4 /)

  mesh % ltype(1)  = QUA04

  mesh % coord(:,1 ) = (/ 0.0_rp , 0.0_rp /) 
  mesh % coord(:,2 ) = (/ 2.0_rp , 0.0_rp /) 
  mesh % coord(:,3 ) = (/ 2.0_rp , 3.0_rp /) 
  mesh % coord(:,4 ) = (/ 0.0_rp , 3.0_rp /) 

  pgaus = 25
  pelty = QUA04

  allocate(elcod(mesh % ndime,mesh % mnode))
  allocate(gpcar(mesh % ndime,mesh % mnode,pgaus))
  allocate(xjaci(mesh % ndime,mesh % ndime,pgaus))
  allocate(gpvol(pgaus))
  allocate(gpdet(pgaus))

  mesh % quad(pelty) % topo  = element_type(pelty) % topology
  mesh % quad(pelty) % ndime = element_type(pelty) % dimensions
  mesh % quad(pelty) % ngaus = pgaus
  
  do ii = 1,3

     xx = 0.0_rp
     select case ( ii )
     case ( 1_ip ) ; call mesh % quad(pelty) % set(NAME='TRAPEZOIDAL')
     case ( 2_ip ) ; call mesh % quad(pelty) % set(NAME='GAUSS')
     case ( 3_ip ) ; mesh % quad(pelty) % ngaus = mesh % mnode ; call mesh % quad(pelty) % set(NAME='CLOSED')
     end select
     call mesh % iso(pelty)  % set(element_type(pelty) % number_nodes,mesh % quad(pelty))

     do ielem = 1,mesh % nelem

        pelty      = mesh % ltype(ielem)
        pgaus      = mesh % quad(pelty) % ngaus
        pnode      = element_type(pelty) % number_nodes
        pdime      = element_type(pelty) % dimensions
       do inode = 1,pnode
           elcod(1:pdime,inode) = mesh % coord(1:pdime,mesh % lnods(inode,ielem))
        end do
        call elmgeo_cartesian_derivatives_jacobian(pdime,pnode,pnode,pgaus,&
             elcod,mesh % iso(pelty) % deriv,xjaci,gpcar,gpdet)

        gpvol(1:pgaus) = mesh % quad(pelty) % weigp(1:pgaus) * gpdet(1:pgaus)
        do igaus = 1,pgaus
           xx = xx +  gpvol(igaus)
        end do
     end do
     
     print*,'x=',ii,xx

     call mesh % iso (pelty) % deallo()
     call mesh % quad(pelty) % deallo()
     
     if( abs(xx-6.0_rp) > 1.0e-08_rp ) then
        print*,'diff=',abs(xx-6.0_rp)
        stop 1 
     end if
     
  end do
  
  !---------------------------------------------------------------------
  !
  ! 3D CASE
  !
  !---------------------------------------------------------------------
  
  deallocate(elcod)
  deallocate(gpcar)
  deallocate(xjaci)
  deallocate(gpvol)
  deallocate(gpdet)
  call mesh % deallo()
  
  call mesh % init('MY_MESH')

  mesh % nelem = 1
  mesh % npoin = 8
  mesh % ndime = 3
  mesh % mnode = 8

  call mesh % alloca()

  mesh % lnods(:,1)  = (/  1,2,3,4,5,6,7,8 /)

  mesh % ltype(1)  = HEX08

  mesh % coord(:,1 ) = (/ 0.0_rp , 0.0_rp, 0.0_rp /) 
  mesh % coord(:,2 ) = (/ 2.0_rp , 0.0_rp, 0.0_rp /) 
  mesh % coord(:,3 ) = (/ 2.0_rp , 3.0_rp, 0.0_rp /) 
  mesh % coord(:,4 ) = (/ 0.0_rp , 3.0_rp, 0.0_rp /) 
  mesh % coord(:,5 ) = (/ 0.0_rp , 0.0_rp, 5.0_rp /) 
  mesh % coord(:,6 ) = (/ 2.0_rp , 0.0_rp, 5.0_rp /) 
  mesh % coord(:,7 ) = (/ 2.0_rp , 3.0_rp, 5.0_rp /) 
  mesh % coord(:,8 ) = (/ 0.0_rp , 3.0_rp, 5.0_rp /) 

  pgaus = 125
  pelty = HEX08

  allocate(elcod(mesh % ndime,mesh % mnode))
  allocate(gpcar(mesh % ndime,mesh % mnode,pgaus))
  allocate(xjaci(mesh % ndime,mesh % ndime,pgaus))
  allocate(gpvol(pgaus))
  allocate(gpdet(pgaus))


  mesh % quad(pelty) % topo  = element_type(pelty) % topology
  mesh % quad(pelty) % ndime = element_type(pelty) % dimensions
  mesh % quad(pelty) % ngaus = pgaus
  
  do ii = 1,3

     xx = 0.0_rp
     select case ( ii )
     case ( 1_ip ) ; call mesh % quad(pelty) % set(NAME='TRAPEZOIDAL')
     case ( 2_ip ) ; call mesh % quad(pelty) % set(NAME='GAUSS')
     case ( 3_ip ) ; mesh % quad(pelty) % ngaus = mesh % mnode ; call mesh % quad(pelty) % set(NAME='CLOSED')
     end select
     call mesh % iso(pelty)  % set(element_type(pelty) % number_nodes,mesh % quad(pelty))

     do ielem = 1,mesh % nelem

        pelty      = mesh % ltype(ielem)
        pgaus      = mesh % quad(pelty) % ngaus
        pnode      = element_type(pelty) % number_nodes
        pdime      = element_type(pelty) % dimensions
       do inode = 1,pnode
           elcod(1:pdime,inode) = mesh % coord(1:pdime,mesh % lnods(inode,ielem))
        end do
        call elmgeo_cartesian_derivatives_jacobian(pdime,pnode,pnode,pgaus,&
             elcod,mesh % iso(pelty) % deriv,xjaci,gpcar,gpdet)

        gpvol(1:pgaus) = mesh % quad(pelty) % weigp(1:pgaus) * gpdet(1:pgaus)
        do igaus = 1,pgaus
           xx = xx +  gpvol(igaus)
        end do
     end do
     
     print*,'x=',ii,xx

     call mesh % iso (pelty) % deallo()
     call mesh % quad(pelty) % deallo()
     
     if( abs(xx-30.0_rp) > 1.0e-08_rp ) then
        print*,'diff=',abs(xx-30.0_rp)
        stop 1 
     end if
     
  end do

end program unitt_quadrature
