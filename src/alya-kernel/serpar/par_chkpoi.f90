!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_chkpoi(ichec)
  !------------------------------------------------------------------------
  !****f* Parall/par_chkpoi
  ! NAME
  !    par_chkpoi
  ! DESCRIPTION
  !    Checkpoint. Retunrs ICHEC:
  !    ICHEC = 0 ... MPI REDUCE has performed well
  !
  !    
  ! OUTPUT
  !    ICHEC
  ! USED BY
  !    par_partit
  !***
  !------------------------------------------------------------------------
  use def_master
  use def_parall
  use mod_communications, only : PAR_SUM
  implicit none  
  integer(ip), intent(out) :: ichec
  integer(ip)              :: dummi
  !
  ! Checkpoint
  !
  if( kfl_paral>=0) then
     dummi = 1
     call PAR_SUM(dummi)
     ichec=nproc_par-dummi-1
  end if

end subroutine par_chkpoi

