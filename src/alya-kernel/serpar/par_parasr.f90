!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_parasr(wtask,ndims,rvars,ndimr,rvarr)
  !------------------------------------------------------------------------
  !****f* Parall/par_parasr
  ! NAME
  !    par_parasr
  ! DESCRIPTION
  !    Works with arrays to deal with parallelization
  ! OUTPUT
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_master, only     :  npasr,nparr,parrs,parre
  use def_master, only     :  ISLAVE
  implicit none
  character(3), intent(in) :: wtask
  integer(ip),  intent(in) :: ndims,ndimr
  real(rp),     target     :: rvars(ndims)
  real(rp),     target     :: rvarr(ndimr)

  if( ISLAVE ) then

     npasr = 0
     nparr = 0 

     select case ( wtask )

     case ( 'SLX' ) 
        !
        ! par_slaves
        !
        npasr =  ndims
        nparr =  ndimr
        parrs => rvars
        parre => rvarr
        call par_slaves(1_ip)
        
     case default
        
        call runend('PARASI: WRONG CASE')
        
     end select

     npasr = 0
     nparr = 0

  end if
  
end subroutine par_parasr
 
