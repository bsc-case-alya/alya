!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine btdbma(aglob,aloca,bmatr,n1,n2)
!------------------------------------------------------------------------
!                                      
!    This routine computes Ag = Bt Al B  when Ag and Al are stored 
!    as full matrices (Ag := aglob, Al := aloca, B := bmatr). The di-
!    mensions are Al -> Mat(n1,n1), Ag -> Mat(n2,n2), B -> Mat(n2,n1) 
!
!------------------------------------------------------------------------
  use      def_kintyp
  implicit none
  integer(ip), intent(in)  :: n1,n2
  real(rp),    intent(in)  :: aloca(n1,n1), bmatr(n1,n2)
  real(rp),    intent(out) :: aglob(n2,n2)
  integer(ip)              :: i,j,k,l

  do i=1,n2
     do j=1,n2
        aglob(i,j)=0.0_rp
        do k=1,n1
           do l=1,n1
              aglob(i,j)=aglob(i,j)+bmatr(k,i)*aloca(k,l)*bmatr(l,j)
           end do
        end do
     end do
  end do
  
end subroutine btdbma
