!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_bougat(&
     ndime,pnodb,npoin,lnodb,bovel,bocod,veloc,coord)
  !-----------------------------------------------------------------------
  !
  ! Gather operations
  !
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  implicit none
  integer(ip), intent(in)  :: ndime,pnodb,npoin
  integer(ip), intent(in)  :: lnodb(pnodb)
  real(rp),    intent(in)  :: veloc(ndime,npoin,*),coord(ndime,npoin)
  real(rp),    intent(out) :: bovel(ndime,pnodb),bocod(ndime,pnodb)
  integer(ip)              :: inodb,ipoin,idime
  !
  ! Current velocity and coordinates
  !
  do inodb=1,pnodb
     ipoin=lnodb(inodb)
     do idime=1,ndime
        bovel(idime,inodb) = veloc(idime,ipoin,1)
        bocod(idime,inodb) = coord(idime,ipoin)
     end do
  end do

end subroutine nsi_bougat
