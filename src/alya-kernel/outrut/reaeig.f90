!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine reaeig(itask)
  !-----------------------------------------------------------------------
  !****f* outrut/reaeig
  ! NAME 
  !    reaeig
  ! DESCRIPTION
  !    This routine reads the eigen solver data
  ! INPUT
  ! OUTPUT
  ! USES
  ! USED BY
  !    ***_reanut
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only    :  ip,rp
  use def_solver, only    :  eigen_sol 
  use def_master, only    :  kfl_symgr
  use def_inpout
  implicit none
  integer(ip), intent(in) :: itask

  if( itask == 1 ) then
     !
     ! Solver type
     !
     if(exists('DIREC')) eigen_sol(1)%kfl_algso = 0
     if(exists('EIGE1')) eigen_sol(1)%kfl_algso = 1
     if(exists('EIGE2')) eigen_sol(1)%kfl_algso = 2
     if(exists('EIGE3')) eigen_sol(1)%kfl_algso = 3
     !
     ! Solver name
     !
     if(exists('DIREC')) eigen_sol(1)%wsolv = 'DIRECT'
     if(exists('EIGE1')) eigen_sol(1)%wsolv = 'ITERATIVE 1'
     if(exists('EIGE2')) eigen_sol(1)%wsolv = 'ITERATIVE 2'
     if(exists('EIGE3')) eigen_sol(1)%wsolv = 'ITERATIVE 3'
     !
     ! Solver parameters
     !
     if(exists('ITERA')) eigen_sol(1)%miter = getint('ITERA', 1_ip,   '#Max iterations')
     if(exists('TOLER')) eigen_sol(1)%solco = getrea('TOLER', 1e-4_rp,'#Tolerance')
     if(exists('NUMBE')) eigen_sol(1)%neiva = getint('NUMBE', 10_ip,  '#Number of eigenvalues')
     if(exists('SHIFT')) eigen_sol(1)%shift = getrea('SHIFT',-1.0_rp,   '#amount of shift')
     !
     ! Mass matrix
     !
     if(exists('MASS ')) then
        if(exists('DIAGO').or.exists('LUMPE')) then
           eigen_sol(1)%kfl_massm = 0
        else
           eigen_sol(1)%kfl_massm = 1
           kfl_symgr = 1
        end if
     end if

  end if

end subroutine reaeig
