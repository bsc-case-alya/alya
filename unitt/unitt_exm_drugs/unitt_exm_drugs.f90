!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_exm_drugs
   !test drug interactions

   use def_kintyp_basic, only : ip,rp
   use def_master
   use def_domain 
   use def_kermod
   use mod_exm_drugs
   implicit none



   mem_modul = 0_ip
   modul = 1_ip

   nmate = 1_ip

   print *,'Testing 1 drug, additive effect'
   call test_additive_ndrugs( 1_ip )
   print *,'Testing 2 drugs, additive effect'
   call test_additive_ndrugs( 2_ip )
   print *,'Testing 3 drugs, additive effect'
   call test_additive_ndrugs( 3_ip )

   print *,'Test array drug flattening for TOR model'
   call test_drug_flattening()

   print *,'Testing 3 drugs, additive effect, different h'
   call test_h( )

   contains

   subroutine test_drug_flattening()
      implicit none
      integer(ip), parameter :: nvars_tor = 24_ip

      real(rp), dimension(NVARS_DRUG) :: dosis1, dosis2, ic501, ic502, h
      real(rp), dimension(nvars_tor)  :: drugdmate, correct
      integer(ip)                     :: ndrugs

      ndrugs = 2_ip

      dosis1 =  (/  0.6_rp,  0.61_rp,  0.62_rp,  0.63_rp,  0.64_rp,  0.65_rp,  0.66_rp /)
      ic501  =  (/110.0_rp, 111.0_rp, 112.0_rp, 113.0_rp, 114.0_rp, 115.0_rp, 116.0_rp /)
      dosis2 =  (/ 0.61_rp,  0.62_rp,  0.63_rp,  0.64_rp,  0.65_rp,  0.66_rp,  0.67_rp /)
      ic502  =  (/111.0_rp, 112.0_rp, 113.0_rp, 114.0_rp, 115.0_rp, 116.0_rp, 117.0_rp /)
      h      =  (/  1.0_rp,   1.0_rp,   1.0_rp,   1.0_rp,   1.0_rp,   1.0_rp,   1.0_rp /)

      call unitt_exm_drugs_allocate(ndrugs)
      call unitt_exm_drugs_set(1_ip, 1_ip, "D1   ", dosis1, ic501, h)
      call unitt_exm_drugs_set(1_ip, 2_ip, "D2   ", dosis2, ic502, h)

      call exm_drugs_get_drugdmate(1_ip, drugdmate, nvars_tor/4_ip, 2_ip)

      correct = -1.0_rp
      !TODO: this is getting screwed up, need to fix TOR model drug usage
      correct(1_ip:nvars_tor/2_ip:2_ip) = dosis1(1_ip:6_ip)
      correct(2_ip:nvars_tor/2_ip:2_ip) = ic501(1_ip:6_ip)
      correct(nvars_tor/2_ip+1:nvars_tor:2_ip) = dosis2(1_ip:6_ip)
      correct(nvars_tor/2_ip+2:nvars_tor:2_ip) = ic502(1_ip:6_ip)

      print *,"drugdmate = ",drugdmate 
      print *,'correct   = ',correct

      if( sum( abs(drugdmate - correct) )>epsilon(1.0_rp) ) stop 1

   end subroutine test_drug_flattening


   subroutine test_additive_ndrugs( ndrugs )
      implicit none
      integer(ip), intent(in) :: ndrugs

      integer(ip) :: imate, idrug

      real(rp), dimension(NVARS_DRUG) :: dosis1, dosis2, dosis3, ic501, ic502, ic503, k, correct, h, diff
      type(DRUG_CONDUCTANCES)         :: dc
   
      if( ndrugs<1_ip ) then
         print *,'The test was meant for at least 1 drug'
         stop 1
      end if


      if( ndrugs>3_ip ) then
         print *,'The test was not meant for more than 3 drugs'
         stop 1
      end if

      call unitt_exm_drugs_allocate(ndrugs)
   
      dosis1 =  (/  0.6_rp,  0.61_rp,  0.62_rp,  0.63_rp,  0.64_rp,  0.65_rp,  0.66_rp/)
      ic501  =  (/110.0_rp, 111.0_rp, 112.0_rp, 113.0_rp, 114.0_rp, 115.0_rp, 116.0_rp/)
      dosis2 =  (/ 0.61_rp,  0.62_rp,  0.63_rp,  0.64_rp,  0.65_rp,  0.66_rp,  0.67_rp/)
      ic502  =  (/111.0_rp, 112.0_rp, 113.0_rp, 114.0_rp, 115.0_rp, 116.0_rp, 117.0_rp/)
      dosis3 =  (/ 0.62_rp,  0.63_rp,  0.64_rp,  0.65_rp,  0.66_rp,  0.67_rp,  0.68_rp/)
      ic503  =  (/112.0_rp, 113.0_rp, 114.0_rp, 115.0_rp, 116.0_rp, 117.0_rp, 118.0_rp/)
      h      =  (/  1.0_rp,   1.0_rp,   1.0_rp,   1.0_rp,   1.0_rp,   1.0_rp,   1.0_rp/)


      call                  unitt_exm_drugs_set(1_ip, 1_ip, "D1   ", dosis1, ic501, h )
      if (ndrugs>1_ip) call unitt_exm_drugs_set(1_ip, 2_ip, "D2   ", dosis2, ic502, h )
      if (ndrugs>2_ip) call unitt_exm_drugs_set(1_ip, 3_ip, "D3   ", dosis3, ic503, h )
   
      !print *," BEFORE CALCULATE "
      dc =  exm_drugs_calculate_effect(1_ip)
   
      if(ndrugs==1_ip) correct = ( 1.0_rp - ( 1.0_rp - 1.0_rp/(1.0_rp+dosis1/ic501) ) )
      if(ndrugs==2_ip) correct = ( 1.0_rp - ( 1.0_rp - 1.0_rp/(1.0_rp+dosis1/ic501) ) - &
                                            ( 1.0_rp - 1.0_rp/(1.0_rp+dosis2/ic502) ) )
      if(ndrugs==3_ip) correct = ( 1.0_rp - ( 1.0_rp - 1.0_rp/(1.0_rp+dosis1/ic501) ) - &
                                            ( 1.0_rp - 1.0_rp/(1.0_rp+dosis2/ic502) ) - &
                                            ( 1.0_rp - 1.0_rp/(1.0_rp+dosis3/ic503) ) )
      
      print *, "Calculated conductances = ",dc
      print *, "Correct conductances = ",correct
   
      diff(1) = abs(dc % pca - correct(1_ip))
      diff(2) = abs(dc % gkr - correct(2_ip))
      diff(3) = abs(dc % gna - correct(3_ip))
      diff(4) = abs(dc % gk1 - correct(4_ip))
      diff(5) = abs(dc % gnal- correct(5_ip))
      diff(6) = abs(dc % gks - correct(6_ip))
      diff(7) = abs(dc % Ito - correct(7_ip))
      print *, "Diff = ",diff

      if ( any( abs(diff)  > 100*epsilon(1.0_rp) ) ) stop 1

   end subroutine test_additive_ndrugs


   subroutine test_h( )
      implicit none

      integer(ip) :: imate, idrug, ndrugs

      real(rp), dimension(NVARS_DRUG) :: dosis1, dosis2, dosis3, ic501, ic502, ic503, k, correct, diff, h1, h2, h3
      type(DRUG_CONDUCTANCES)         :: dc

      ndrugs = 3_ip

      call unitt_exm_drugs_allocate(ndrugs)
     
      dosis1 =  (/  0.6_rp,  0.61_rp,  0.62_rp,  0.63_rp,  0.64_rp,  0.65_rp,  0.66_rp /)
      ic501  =  (/110.0_rp, 111.0_rp, 112.0_rp, 113.0_rp, 114.0_rp, 115.0_rp, 116.0_rp /)
      dosis2 =  (/ 0.61_rp,  0.62_rp,  0.63_rp,  0.64_rp,  0.65_rp,  0.66_rp,  0.67_rp /)
      ic502  =  (/111.0_rp, 112.0_rp, 113.0_rp, 114.0_rp, 115.0_rp, 116.0_rp, 117.0_rp /)
      dosis3 =  (/ 0.62_rp,  0.63_rp,  0.64_rp,  0.65_rp,  0.66_rp,  0.67_rp,  0.68_rp /)
      ic503  =  (/112.0_rp, 113.0_rp, 114.0_rp, 115.0_rp, 116.0_rp, 117.0_rp, 118.0_rp /)
      h1     =  (/  0.1_rp,   0.2_rp,   0.3_rp,   0.4_rp,   0.5_rp,   0.6_rp,   0.7_rp /)
      h2     =  (/  0.2_rp,   0.3_rp,   0.4_rp,   0.5_rp,   0.6_rp,   0.7_rp,   0.8_rp /)
      h3     =  (/  0.3_rp,   0.4_rp,   0.5_rp,   0.6_rp,   0.7_rp,   0.8_rp,   0.9_rp /)


      call unitt_exm_drugs_set(1_ip, 1_ip, "D1   ", dosis1, ic501, h1 )
      call unitt_exm_drugs_set(1_ip, 2_ip, "D2   ", dosis2, ic502, h2 )
      call unitt_exm_drugs_set(1_ip, 3_ip, "D3   ", dosis3, ic503, h3 )
   
      !print *," BEFORE CALCULATE "
      dc =  exm_drugs_calculate_effect(1_ip)
   
      if(ndrugs==1_ip) correct = ( 1.0_rp - ( 1.0_rp - 1.0_rp/(1.0_rp+(dosis1/ic501)**h1) ) )
      if(ndrugs==2_ip) correct = ( 1.0_rp - ( 1.0_rp - 1.0_rp/(1.0_rp+(dosis1/ic501)**h1) ) - &
                                            ( 1.0_rp - 1.0_rp/(1.0_rp+(dosis2/ic502)**h2) ) )
      if(ndrugs==3_ip) correct = ( 1.0_rp - ( 1.0_rp - 1.0_rp/(1.0_rp+(dosis1/ic501)**h1) ) - &
                                            ( 1.0_rp - 1.0_rp/(1.0_rp+(dosis2/ic502)**h2) ) - &
                                            ( 1.0_rp - 1.0_rp/(1.0_rp+(dosis3/ic503)**h3) ) )
      

      correct(:) = max( 0.0_rp, correct(:) )
      print *, "Calculated conductances = ",dc
      print *, "Correct conductances = ",correct
   
      
      diff(1) = abs(dc % pca - correct(1_ip))
      diff(2) = abs(dc % gkr - correct(2_ip))
      diff(3) = abs(dc % gna - correct(3_ip))
      diff(4) = abs(dc % gk1 - correct(4_ip))
      diff(5) = abs(dc % gnal- correct(5_ip))
      diff(6) = abs(dc % gks - correct(6_ip))
      diff(7) = abs(dc % Ito - correct(7_ip))

      print *, "Difference = ", diff, ' eps = ', 100*epsilon(1.0_rp)
      print *, "diff > eps = ", diff> 100*epsilon(1.0_rp)
      
      if ( any( abs(diff)  > 100*epsilon(1.0_rp) ) ) stop 1

   end subroutine test_h

end program unitt_exm_drugs
