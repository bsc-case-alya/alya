!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> 
!> @author  david oks 
!> @date    2021-08-12
!> @brief   Compute number of Neumann boundary elements per node
!> @details Compute number of boundary elements per node, considering
!>          only Neumann boundary condition elements
!> 
!-----------------------------------------------------------------------

subroutine nsi_bougra()

  use def_nastin,         only :  kfl_neufs_nsi, kfl_fixbo_nsi, NSI_FRACTIONAL_STEP
  use def_nastin,         only :  bvnat_nsi, bprfs_nsi, nbpoi_nsi
  use def_kintyp,         only :  ip, rp
  use def_master,         only :  modul, mem_modul, INOTMASTER
  use def_domain,         only :  npoin, nboun, nnode, ltypb, lnodb
  use def_kermod,         only :  kfl_adj_prob, kfl_cost_type
  use mod_memory,         only :  memory_alloca
  use mod_communications, only :  PAR_MAX

  implicit none

  integer(ip) :: inodb, ipoin, iboun

  !
  ! Compute flag for Neumann BCs in FS
  !
  kfl_neufs_nsi = 0_ip
  if ( INOTMASTER .and. NSI_FRACTIONAL_STEP ) then
     iboun = 0_ip
     do while( iboun < nboun )
        iboun = iboun + 1
        if( kfl_fixbo_nsi(iboun) == 2_ip    .or. &
          & kfl_fixbo_nsi(iboun) == 6_ip    .or. &
          & kfl_fixbo_nsi(iboun) == 20_ip ) then
           if( bvnat_nsi(1,iboun,1) /= 0.0_rp .or. kfl_fixbo_nsi(iboun) == 20_ip ) then
              kfl_neufs_nsi = 1_ip
              iboun = nboun
           end if
        end if
     end do
  end if
  call PAR_MAX(kfl_neufs_nsi,'IN MY CODE')
  !
  ! Compute number of boundary elements per node and press
  !
  if( kfl_neufs_nsi == 1_ip ) then
     ! Allocate and initialize arrays
     call memory_alloca( mem_modul(1:2,modul),'BPRFS_NSI','nsi_bougra',bprfs_nsi,max(1_ip,npoin))
     call memory_alloca( mem_modul(1:2,modul),'NBPOI_NSI','nsi_bougra',nbpoi_nsi,max(1_ip,npoin))
     nbpoi_nsi(1:npoin) = 0_ip
     bprfs_nsi(1:npoin) = 0.0_rp
     ! Compute list
     do iboun = 1,nboun
        if( kfl_fixbo_nsi(iboun)  ==  2 .or.  &  ! Pressure (in bvnat_nsi)
          & kfl_fixbo_nsi(iboun)  ==  3 .or.  &  ! Wall law
          & kfl_fixbo_nsi(iboun)  == 13 .or.  &  ! Wall law + open pressure
          & kfl_fixbo_nsi(iboun)  ==  5 .or.  &  ! Dynamic pressure
          & kfl_fixbo_nsi(iboun)  ==  6 .or.  &  ! Open flow
          & kfl_fixbo_nsi(iboun)  == 10 .or.  &  ! Dynamic pressure + open flow
          & kfl_fixbo_nsi(iboun)  == 11 .or.  &  ! Dynamic pressure + open flow
          & kfl_fixbo_nsi(iboun)  == 12 .or.  &  ! Nodal pressure (in bpess_nsi)
          & kfl_fixbo_nsi(iboun)  == 15 .or.  &  ! Outflow with pressure dependent on density (Low Mach regime)
          & kfl_fixbo_nsi(iboun)  == 17 .or.  &  ! Outflow with pressure dependent on density (Low Mach regime)
          & kfl_fixbo_nsi(iboun)  == 18 .or.  &  ! u.n in weak form
          & kfl_fixbo_nsi(iboun)  == 19 .or.  &  ! Impose traction
          & kfl_fixbo_nsi(iboun)  == 20 .or.  &  ! Stable outflow
          & kfl_fixbo_nsi(iboun)  == 22 .or.  &  ! Boundary traction is imposed from an auxiliary RANS simulation (Two-layer wall model)
          & (kfl_adj_prob         == 1  .and. &
          &  kfl_cost_type        == 5  .and. &
          &  kfl_fixbo_nsi(iboun) == 1 ) ) then  ! Adjoint terms: dF/dU and dF/dX
           do inodb = 1,nnode(ltypb(iboun))
              ipoin = lnodb(inodb,iboun)
              nbpoi_nsi(ipoin) = nbpoi_nsi(ipoin) + 1
           end do
        end if
     end do
  end if

end subroutine nsi_bougra 
