f = open("c.nsi.cvg", 'r')
data = f.read()
f.close()
data = data.split("\n")
i = 0
for line in data:
    i += 1
    if line.startswith("#"):
        pass
    else:
        l = line.split()
        if float(l[9]) > 200:
            print("Check: KO; line " + str(i))
            exit(1)
print("Check: OK")
exit(0)