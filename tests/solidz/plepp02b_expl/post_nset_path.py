#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



# Post-process Alya node set file data
# Created by G.Guillamet <gerard.guillamet@bsc.es>
# Date: 6 October 2017

from operator import itemgetter

# Inputs
namemod = 'block'
nameinp = namemod + '-node.sld.set'
nameout = 'alya-solution-path-block.txt'
lines_h = 13    # Time (Line 13, position 12)

# Initialize and get number of nodes from node-set
with open(namemod + '.set.dat', 'r') as infile:
    numline = 0
    for line in infile:
        numline += 1
nnodes = numline - 2

# Reading
timeList,lineList = [],[]
with open(nameinp, 'r') as file:
    lc = 0
    for (i, line) in enumerate(file):
            lc += 1
            linecurr = line.split()
            if lc >= lines_h:
                    if linecurr[1] == 'Time':
                        timeList.append(float(linecurr[3]))
                        lineList.append(lc)

# Times and lines for postprocess
firs_time_List = timeList[0]
firs_line_List = lineList[0]
last_time_List = timeList[-1]
last_line_List = lineList[-1]

# Save data
coordxList, coordyList = [], []
with open(nameinp, 'r') as file:
    lc = 0
    for line in file:
        lc += 1
        if lc >= firs_line_List+1 and lc < firs_line_List+nnodes+1:
            linecurr = line.split()
            coordx = float(linecurr[3])
            coordy = float(linecurr[4])
            coordxList.append(coordx)
            coordyList.append(coordy)

displxList, displyList = [], []
with open(nameinp, 'r') as file:
    lc = 0
    for line in file:
        lc += 1
        if lc >= last_line_List+1 and lc < last_line_List+nnodes+1:
            linecurr = line.split()
            displx = float(linecurr[1])
            disply = float(linecurr[2])
            displxList.append(displx)
            displyList.append(disply)

# Sort Nodes according to their coordinates
data = []
for i in range(len(coordxList)):
    data.append([coordxList[i], coordyList[i], displxList[i], displyList[i]])
sorted_data = sorted(data,key=itemgetter(0))

lineouts = []
for i in range(len(coordxList)):
    lineouts.append('%1.6e %1.6e %1.6e %1.6e\n' % (sorted_data[i][0],sorted_data[i][1],sorted_data[i][2],sorted_data[i][3]))

# Write file output
with open(nameout, 'w') as file:
        file.writelines(lineouts)

