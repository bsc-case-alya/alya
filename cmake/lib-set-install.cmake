#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



set_target_properties(${PROJECT_NAME} PROPERTIES POSITION_INDEPENDENT_CODE ON)

set(CMAKE_Fortran_MODULE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
set(${PROJECT_NAME}_MODULE_DIR ${CMAKE_Fortran_MODULE_DIRECTORY} CACHE INTERNAL "${PROJECT_NAME}: module directory" FORCE)
get_property(MY_INCLUDE_DIRS DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
list(APPEND MY_INCLUDE_DIRS ${${PROJECT_NAME}_MODULE_DIR})
set(${PROJECT_NAME}_INCLUDE_DIRS ${MY_INCLUDE_DIRS} CACHE INTERNAL "${PROJECT_NAME}: list of include directories" FORCE)

install(TARGETS ${PROJECT_NAME} DESTINATION lib)
install(DIRECTORY ${CMAKE_Fortran_MODULE_DIRECTORY} DESTINATION modules FILES_MATCHING REGEX "\\.mod$" PATTERN "CMakeFiles" EXCLUDE)
