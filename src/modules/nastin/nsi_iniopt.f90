!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_iniopt()
  !-----------------------------------------------------------------------
  !   
  ! This routine loads the solver data for the adjoint equations of the NS equation.
  !
  !-----------------------------------------------------------------------
  use def_domain
  use def_nastin
  use def_master
  use def_kermod, only : kfl_adj_prob,kfl_cos_opt,sens,sens_mesh,costf,kfl_dvar_type,kfl_cost_type
  use def_solver
  implicit none

  integer(ip)  :: izrhs,idesvar
    
  if (kfl_cos_opt == 1) costf = 0.0_rp
  
  if (kfl_adj_prob == 1) then
!     resdiff_nsi = 0.0_rp 
    dcost_dx_nsi = 0.0_rp
  endif
  
  if (kfl_dvar_type == 5) then
    sens_mesh = 0.0_rp
  endif
  
!   if ( kfl_coupl(ID_NASTIN,ID_TURBUL) == 0 ) then
!     if (kfl_dvar_type == 5) then
!       sens_mesh = 0.0_rp
!     else
!       sens = 0.0_rp
!     endif
!   endif
 
end subroutine nsi_iniopt
 
