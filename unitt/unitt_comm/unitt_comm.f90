!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_comm
  !
  ! Test point to point communications
  !
  use def_kintyp_basic,                 only : ip,rp
  use def_kintyp_comm,                  only : comm_data_par_basic
  use def_master
  use mod_communications_tools
  use mod_communications_point_to_point
  use mod_communications_global
  use mod_parall
  use def_mpi
  implicit none

  type(comm_data_par_basic) :: comm
  integer(ip)               :: my_rank,my_size,ii
  real(rp),     pointer     :: xx(:)
  real(rp)                  :: rr(2)
  integer(ip)               :: jj(2)
  
  allocate(PAR_COMM_MY_CODE_ARRAY(1))
  commd => PAR_COMM_MY_CODE_ARRAY(1)
  nullify(xx)
  PAR_COMM_MY_CODE = MPI_COMM_WORLD
  INOTSLAVE     = .true.
  ISEQUEN       = .false. 
  IPARALL       = .true.

  call PAR_INIT()
  call PAR_COMM_RANK_AND_SIZE(PAR_COMM_MY_CODE,my_rank,my_size)

  if( my_rank == 0 ) then
     IMASTER   = .true.
     ISLAVE    = .false.
     INOTSLAVE = .true.
  else
     IMASTER   = .false.
     ISLAVE    = .true.
     INOTSLAVE = .false.     
  end if

  !---------------------------------------------------------------------
  !
  ! INTERFACE NODE EXCHANGE
  !
  !---------------------------------------------------------------------

  do ii = 1,2
     call commd % init()
     commd % RANK4          = my_rank
     commd % PAR_COMM_WORLD = MPI_COMM_WORLD
     commd % SIZE4          = my_size
     PAR_COMM_MY_CODE       = MPI_COMM_WORLD
     PAR_MY_CODE_RANK       = my_rank

     select case ( my_rank )
     case ( 0 )
        npoin            = 0
     case ( 1 )
        allocate(xx(10))
        !                   2      2
        !                                                      3      3
        xx                = [1.0_rp,2.0_rp,3.0_rp,4.0_rp,5.0_rp,6.0_rp,7.0_rp,8.0_rp,9.0_rp,10.0_rp]
        npoin             = 10
        commd % nneig     = 2
        commd % bound_dim = 4
        call commd % alloca()
        commd % neights    = [2,3]
        commd % bound_size = [1,3,5]
        commd % bound_perm = [1,2,6,7]
     case ( 2 )
        allocate(xx(8))
        !                                           1       1
        !                                           3       3
        xx                = [10.0_rp,20.0_rp,30.0_rp,40.0_rp,50.0_rp,60.0_rp,70.0_rp,80.0_rp]
        npoin             = 8
        commd % nneig     = 2
        commd % bound_dim = 4
        call commd % alloca()
        commd % neights    = [1,3]
        commd % bound_size = [1,3,5]
        commd % bound_perm = [4,5,4,5]
     case ( 3 )
        allocate(xx(8))
        !                   1        1
        !                                                                         2        2
        xx                = [100.0_rp,200.0_rp,300.0_rp,400.0_rp,500.0_rp,600.0_rp,700.0_rp,800.0_rp]
        npoin             = 8
        commd % nneig     = 2
        commd % bound_dim = 4
        call commd % alloca()
        commd % neights    = [1,2]
        commd % bound_size = [1,3,5]
        commd % bound_perm = [1,2,7,8]     
     end select
     commd % bound_invp => commd % bound_perm

     ! rank=1: [1,2,3,4,5,6,7,8,9,10]
     ! rank=2: [10,20,30,1,2,60,70,80]
     ! rank=3: [6,7,300,400,500,600,40,50]

     if( ii == 2 ) then
        comm % nneig          =  commd % nneig
        comm % bound_dim      =  commd % bound_dim
        comm % neights        => commd % neights   
        comm % bound_size     => commd % bound_size
        comm % bound_perm     => commd % bound_perm
        comm % RANK4          =  commd % RANK4         
        comm % PAR_COMM_WORLD =  commd % PAR_COMM_WORLD
        call PAR_INTERFACE_NODE_EXCHANGE(xx,'TAKE MIN',comm)
     else
        call PAR_INTERFACE_NODE_EXCHANGE(xx,'TAKE MIN','IN MY CODE')
     end if
     
     select case ( my_rank )
     case ( 1 )
        if( abs(xx(1)- 1.0_rp) > 1.0e-06_rp ) stop 1
        if( abs(xx(2)- 2.0_rp) > 1.0e-06_rp ) stop 2    
        if( abs(xx(6)- 6.0_rp) > 1.0e-06_rp ) stop 3
        if( abs(xx(7)- 7.0_rp) > 1.0e-06_rp ) stop 4    
     case ( 2 )
        if( abs(xx(4)- 1.0_rp) > 1.0e-06_rp ) stop 5
        if( abs(xx(5)- 2.0_rp) > 1.0e-06_rp ) stop 6    
     case ( 3 )
        if( abs(xx(1)- 6.0_rp) > 1.0e-06_rp ) stop 7
        if( abs(xx(2)- 7.0_rp) > 1.0e-06_rp ) stop 8
        if( abs(xx(7)-40.0_rp) > 1.0e-06_rp ) stop 9
        if( abs(xx(8)-50.0_rp) > 1.0e-06_rp ) stop 10
     end select

     if(associated(xx)) print*,'xx=',my_rank,': ',int(xx,ip)
     if(associated(xx)) deallocate(xx)
  end do
  
  !---------------------------------------------------------------------
  !
  ! PAR_AVERAGE
  !
  !---------------------------------------------------------------------

  rr = real(my_rank+1,rp)
  call PAR_AVERAGE(2_ip,rr)
  if(my_rank == 0 ) print*,'rr 1=',rr
  if( any(abs(rr-9.0_rp/3.0_rp) > 1.0e-06_rp ) ) stop 11

  rr = real(my_rank+1,rp)
  call PAR_AVERAGE(2_ip,rr,INCLUDE_ROOT=.true.)
  if(my_rank == 0 ) print*,'rr 2=',rr
  if( any(abs(rr-10.0_rp/4.0_rp) > 1.0e-06_rp ) ) stop 12
  
  jj = my_rank+1
  call PAR_AVERAGE(2_ip,jj)
  if(my_rank == 0 ) print*,'jj 1=',jj
  if( any(abs(jj-int(9.0_rp/3.0_rp,ip)) > 0 ) ) stop 13

  jj = my_rank+1
  call PAR_AVERAGE(2_ip,jj,INCLUDE_ROOT=.true.)
  if(my_rank == 0 ) print*,'jj 2=',jj
  if( any(abs(jj-int(10.0_rp/4.0_rp,ip)) > 0 ) ) stop 14
  
  !---------------------------------------------------------------------
  !
  ! PAR_MAX
  !
  !---------------------------------------------------------------------

  rr = 3.0_rp-real(my_rank,rp) ! 3,2,1,0
  call PAR_MAX(2_ip,rr)
  if(my_rank == 0 ) print*,'rr 3=',rr
  if( any(abs(rr-2.0_rp) > 1.0e-06_rp ) ) stop 11
  
  rr = 3.0_rp-real(my_rank,rp) ! 3,2,1,0
  call PAR_MAX(2_ip,rr,INCLUDE_ROOT=.true.)
  if(my_rank == 0 ) print*,'rr 4=',rr
  if( any(abs(rr-3.0_rp) > 1.0e-06_rp ) ) stop 11
  
  !---------------------------------------------------------------------
  !
  ! PAR_MIN
  !
  !---------------------------------------------------------------------

  rr = real(my_rank+1,rp) ! 1,2,3,4
  call PAR_MIN(2_ip,rr)
  if(my_rank == 0 ) print*,'rr 3=',rr
  if( any(abs(rr-2.0_rp) > 1.0e-06_rp ) ) stop 11
  
  rr = real(my_rank+1,rp) ! 1,2,3,4
  call PAR_MIN(2_ip,rr,INCLUDE_ROOT=.true.)
  if(my_rank == 0 ) print*,'rr 4=',rr
  if( any(abs(rr-1.0_rp) > 1.0e-06_rp ) ) stop 11
  
  call par_finali(1_ip)

end program unitt_comm
