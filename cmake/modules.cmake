#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



foreach(MOD ${MODULES})
  string(TOUPPER ${MOD} UPMOD)
  if(WITH_MODULE_${UPMOD})
    message(STATUS "Module ${MOD} enabled.")
    add_definitions(-D'${UPMOD}_MODULE=use')
    set(MODU ${MOD}_MODULE_DIR)
    list(APPEND MODULES_MODULE_DIR ${${MODU}})
    list(APPEND MODULES_LIB ${MOD})
  endif()
endforeach()
