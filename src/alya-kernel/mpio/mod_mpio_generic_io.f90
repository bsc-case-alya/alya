!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup mpio
!> @{
!> @file    mod_mpio_generic_io.f90
!> @author  Damien Dosimont
!> @date    27/09/2017
!> @brief   MPI-IO generic
!> @details This module is a bridge that redirects to the sequential or the parallel
!>          versions of the MPI I/O operations
!> @}
!-----------------------------------------------------------------------

module mod_mpio_generic_io

    use def_master
    use mod_mpio_seq_generic_io
    use mod_mpio_par_generic_io

    implicit none

    private

    interface MPIO_READ
        module procedure                        MPIO_READ_INT_V,&
                                                MPIO_READ_INT_M,&
                                                MPIO_READ_REAL_V,&
                                                MPIO_READ_REAL_M
    end interface

    public :: MPIO_READ

    contains

    subroutine MPIO_READ_INT_V(buf, filename, dim)
        integer(ip), pointer, intent(inout)            :: buf(:)
        character(*),         intent(in)               :: filename
        integer(ip),          intent(inout)            :: dim
        if (ISEQUEN) then
            call mpio_seq_read(buf, filename, dim)
        else
            call mpio_par_read(buf, filename, dim)
        end if
    end subroutine

    subroutine MPIO_READ_INT_M(buf, filename, lines, columns)
        integer(ip), pointer, intent(inout)            :: buf(:,:)
        character(*),         intent(in)               :: filename
        integer(ip),          intent(inout)            :: lines, columns
        if (ISEQUEN) then
            call mpio_seq_read(buf, filename, lines, columns)
        else
            call mpio_par_read(buf, filename, lines, columns)
        end if
    end subroutine

    subroutine MPIO_READ_REAL_V(buf, filename, dim)
        real(rp), pointer, intent(inout)               :: buf(:)
        character(*),         intent(in)               :: filename
        integer(ip),          intent(inout)            :: dim
        if (ISEQUEN) then
            call mpio_seq_read(buf, filename, dim)
        else
            call mpio_par_read(buf, filename, dim)
        end if
    end subroutine

    subroutine MPIO_READ_REAL_M(buf, filename, lines, columns)
        real(rp), pointer, intent(inout)               :: buf(:,:)
        character(*),         intent(in)               :: filename
        integer(ip),          intent(inout)            :: lines, columns
        if (ISEQUEN) then
            call mpio_seq_read(buf, filename, lines, columns)
        else
            call mpio_par_read(buf, filename, lines, columns)
        end if
    end subroutine

end module
