!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_skdtree
  !
  ! Test copy, extract and merge operators
  !
  use def_maths_tree
  use def_kintyp_mesh_basic, only: mesh_type_basic
  use def_elmtyp,            only: BAR02
  use mod_elmgeo,            only: elmgeo_element_type_initialization
  use def_master

  implicit none

  type(mesh_type_basic)           :: mesh_out
  type(mesh_type_basic)           :: input_mesh
  type(maths_skdtree)             :: the_skdtree
  real(rp),             pointer   :: bobox(:,:,:)
  integer(8)                      :: count_rate8
  
  type(i1p),            pointer   :: list_entities(:)    
  character(len=5),     pointer   :: names(:)
  real(rp),             pointer   :: xx(:,:)
  real(rp),             pointer   :: res(:,:)
  integer(ip)                     :: iboun, isin

  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)

  nullify(xx,res,names,list_entities)
  !
  ! Create mesh
  !
  call elmgeo_element_type_initialization()
 
  call input_mesh % init('MY_MESH')
  
  input_mesh % nelem = 8
  input_mesh % npoin = 8
  input_mesh % ndime = 2
  input_mesh % mnode = 2
  
  call input_mesh % alloca()

  input_mesh % lnods(:,1)  = (/  1,2 /)
  input_mesh % lnods(:,2)  = (/  2,3 /)
  input_mesh % lnods(:,3)  = (/  3,4 /) 
  input_mesh % lnods(:,4)  = (/  4,5 /)
  input_mesh % lnods(:,5)  = (/  5,6 /) 
  input_mesh % lnods(:,6)  = (/  6,7 /)  
  input_mesh % lnods(:,7)  = (/  7,8 /) 
  input_mesh % lnods(:,8)  = (/  8,1 /) 

  input_mesh % ltype(1:8)  = BAR02

  input_mesh % coord(:,1 ) = (/ 0.0_rp , 3.0_rp /) 
  input_mesh % coord(:,2 ) = (/ 1.0_rp , 1.0_rp /) 
  input_mesh % coord(:,3 ) = (/ 4.0_rp , 0.0_rp /) 
  input_mesh % coord(:,4 ) = (/ 6.0_rp , 1.0_rp /) 
  input_mesh % coord(:,5 ) = (/ 4.0_rp , 3.0_rp /) 
  input_mesh % coord(:,6 ) = (/ 6.0_rp , 5.0_rp /) 
  input_mesh % coord(:,7 ) = (/ 4.0_rp , 6.0_rp /) 
  input_mesh % coord(:,8 ) = (/ 1.0_rp , 5.0_rp /)

  call input_mesh % output (FILENAME='mesh')

  !
  ! Element bounding boxes
  !
  nullify(bobox)  
  call input_mesh % element_bb(bobox)
  !----------------------------------------------------------------------
  !
  ! SKD TREE
  ! 
  !----------------------------------------------------------------------

  call the_skdtree  % init  ()
  call the_skdtree  % input () 
  call the_skdtree  % fill  (BOBOX=bobox)

  call mesh_out     % init()  
  call the_skdtree  % mesh(&
       mesh_out % ndime,mesh_out % mnode,mesh_out % nelem,&
       mesh_out % npoin,mesh_out % lnods,mesh_out % ltype,&
       mesh_out % coord)
  call the_skdtree  % results(res,names)
  call mesh_out     % output (FILENAME='skdtree')
  call mesh_out     % results(res,names,FILENAME='skdtree')

  nullify(xx)
  allocate(xx(input_mesh % ndime,1))
  xx(:,1) = (/ 0.0_rp,6.0_rp /)

  call the_skdtree  % candidate(xx,list_entities,METHOD=1_ip)

  print *,'entities: ',list_entities(1) % l
  
  if( size(list_entities(1) % l) /= 3 ) then
     print *,'wrong numer of boundary boxes (not equal to 3): ',size(list_entities(1) % l)
     stop 1     
  else
     isin = 0_ip
     do iboun = 1,3
        if (list_entities(1) % l(iboun) == 1) isin = isin + 1_ip
        if (list_entities(1) % l(iboun) == 7) isin = isin + 1_ip
        if (list_entities(1) % l(iboun) == 8) isin = isin + 1_ip                      
     end do
     if (isin < 3) then
        print*,'wrong boundary boxes identifiers (the good ones are 1, 7, and  8): ',list_entities(1) % l(:)
        stop 1
     end if
  end if
  
end program unitt_skdtree
