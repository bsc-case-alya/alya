!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_refere()
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_refere
  ! NAME 
  !    nsi_refere
  ! DESCRIPTION
  !    This routine computes the error with respect to a reference solution
  ! USES
  ! USED BY
  !    nsi_output
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_nastin
  use def_inpout
  use mod_communications, only : PAR_SUM
  implicit none
  integer(ip) :: idime,ipoin,itotn,ntotn
  real(rp)    :: residual(4)

  if( kfl_refer_nsi > 0 ) then 

     if( INOTMASTER ) then
        
        residual = 0.0_rp
        ntotn    = npoin*ndime
        
        do ipoin = 1,npoin
           itotn = (ipoin-1)*ndime + 1
           do idime = 1,ndime
              itotn = itotn + 1
              residual(1) = residual(1) + ( xfiel(kfl_refer_nsi) % a(idime,ipoin,1) - unkno(itotn) ) ** 2
              residual(2) = residual(2) + ( xfiel(kfl_refer_nsi) % a(idime,ipoin,1) ) ** 2
           end do
           residual(3) = residual(3) + ( xfiel(kfl_refer_nsi+1) % a(1,ipoin,1) - unkno(ntotn+ipoin) ) ** 2
           residual(4) = residual(4) + ( xfiel(kfl_refer_nsi+1) % a(1,ipoin,1) ) ** 2
        end do
     end if

     call PAR_SUM(4_ip,residual,'IN THE WORLD')
     
     difve_nsi = sqrt( residual(1) / (residual(2)+zeror) )
     difpr_nsi = sqrt( residual(3) / (residual(4)+zeror) )

  end if

end subroutine nsi_refere
