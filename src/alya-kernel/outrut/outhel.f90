!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine outhel
  !------------------------------------------------------------------------
  !****f* master/outhel
  ! NAME
  !    outhel
  ! DESCRIPTION
  !    This routine output a help message
  ! OUTPUT
  !   
  ! USED BY
  !    openfi
  !***
  !------------------------------------------------------------------------

  use      def_master
  implicit none

  write(6,1)
  stop

1 format(&
       & '   ',/,&
       & '   ALYA USAGE:',/,&
       & '   ',/,&
       & '   Alya.x [-h -c] name ',/,&
       & '   ',/,&
       & '   -h  --help  ............ Displays this help',/,&
       & '   -c  --check ............ Check data file',/,&
       & '   -e  --export ........... Export mesh in MPIO format',/,&
       & '   -w  --write ............ Read partitioned problem',/,&
       & '   -r  --read ............. Write parallelization partition',/,&
       & '   -p  --write-rst ........ Write restart',/,&
       & '   -c  --read-rst ......... Read restart to continue the run',/,&
       & '   -i  --read-rst-init .... Read restart to initialize the run',/,&
       & '   -f  --file name ........ Problem name',/,&
       & '   ',/,&
       & '   ',/,&
       & '   Runs Alya for problem name. ',/,&
       & '   ',/,&
       & '   The following I/O files are located/created in current directory (mod is any activated module extension)',/,&
       & '   * means optional:',/,&
       & '   ',/,&
       & '   (I)    name.dat:                     run data',/,&
       & '   (I)    name.ker.dat:                 kernel data',/,&
       & '   (I)    name.dom.dat:                 mesh data',/,&
       & '   (I*)   name.cou.dat:                 coupling data',/,&
       & '   (I)    name.mod.dat:                 module data',/,&
       & '   ',/,&
       & '   (O)    name.log:                     run log',/,&      
       & '   (O)    name.ker.log:                 kernel log',/,&      
       & '   (O*)   name.mem:                     memory',/,&
       & '   (O*)   name.liv:                     live info',/,&
       & '   (O)    name-partition.par.post.msh   partition mesh in GiD format',/,&
       & '   (O)    name-partition.par.post.res   partition results in GiD format',/,&
       & '   (O)    name-VAR.post.alyabin:        postprocess file of variable VAR',/,&
       & '   (O)    name-VAR.mod.sol              solver information for variable VAR',/,&
       & '   (O)    name-VAR.mod.cso              solver convergence for variable VAR',/,&
       & '   (O)    name.mod.cvg:                 module convergence',/,&      
       & '   (O)    name.mod.rst:                 module restart',/,&  
       & '   ')

end subroutine outhel
