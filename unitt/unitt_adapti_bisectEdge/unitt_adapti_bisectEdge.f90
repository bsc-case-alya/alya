!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_adaptivity
  !
  ! Test boundary mesh
  !
  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_master
  use mod_memory
  
  use mod_quality,    only: compute_mesh_quality_sizeShape
  use mod_quality,    only: compute_mesh_quality_shape
  use mod_quality,    only: print_q_stats
  use mod_quality,    only: quality_deallo
  use mod_adapt,      only: adapt_mesh_to_metric
  use mod_metric,     only: mesh_metric_type
  use mod_meshEdges,  only: compute_mesh_edgeLengths
  use mod_meshTopology, only: node_to_elems_type
  use mod_out_paraview, only: out_paraview_inp
  use def_adapt,              only : memor_adapt 
  !use mod_debugTools, only: out_debug_text, out_debug_paraview
  
  implicit none

  real(rp),             parameter :: epsil = 1.0e-12_rp
  type(mesh_type_basic)           :: mesh
  type(mesh_type_basic)           :: mesh_bou
  integer(ip)                     :: ipoin,idime,ielem,inode,iboun,idim,ix,iy,quad(4),hex(8),iz
  integer(ip)                     :: pelty,ii
  real(rp)                        :: dista,xx_test(2)
  integer(8)                      :: count_rate8
  integer(8)                      :: memor_loc(2)
  integer(8)                      :: memor_tmp(2)
  real(rp) :: hx,hy,x0,x1,y0,y1,r,PI,z0,z1,hz
  integer(ip) :: nx,ny,ratioXY,nz, itet

  type(mesh_metric_type) :: metric
  real(rp), allocatable, target :: M(:,:,:)
  logical(lg) :: isSizeField
  
  real(rp), pointer :: q(:)
  real(rp),    pointer               :: edge_lengths(:)
  integer(ip), pointer               :: edge_to_node(:,:)
  integer(ip) :: select_metric 
  
  type(node_to_elems_type) :: node_to_elems
    
  integer(ip), pointer :: mapNodes_old_to_new(:)
  
  logical(lg) , parameter :: out_paraview_test = .false.

  memor_loc = 0_8
  memor_tmp = 0_8
  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)
  !
  ! Generate mesh
  !
  call elmgeo_element_type_initialization()
  !
  call mesh     % init('MY_MESH')
  call mesh_bou % init('BOUNDARY')

  ratioXY = 1_ip  !2_ip
  nx = 1_ip !20_ip
  ny = nx
  nx = nx*ratioXY
  
  mesh % nelem = nx*ny*2
  mesh % npoin = (nx+1)*(ny+1)
  mesh % ndime = 2
  mesh % mnode = 3
  call mesh % alloca()
  mesh % ltype(:)  = TRI03
  
  x0 = -1.0_rp*ratioXY
  x1 =  1.0_rp*ratioXY
  y0 = -1.0_rp
  y1 =  1.0_rp
  hx = (x1-x0)/nx
  hy = (y1-y0)/ny
  do iy=0,ny
    do ix=0,nx
      inode = (1+ix) + iy*(1+nx)
      mesh%coord(:, inode ) = (/ x0 + hx*ix, y0 + hy*iy/)
      if((ix>0).and.(iy>0)) then
        quad(1) = (1+ix-1) + (iy-1)*(1+nx) 
        quad(2) = (1+ix  ) + (iy-1)*(1+nx) 
        quad(3) = (1+ix  ) + (iy-0)*(1+nx) 
        quad(4) = (1+ix-1) + (iy-0)*(1+nx) 
        mesh%lnods(:, (2*ix - 1) + (iy-1)*(2*nx) ) = quad(1:3)
        mesh%lnods(:, (2*ix    ) + (iy-1)*(2*nx) ) = quad((/1,3,4/))
      end if
    end do
  end do
  
  !Metrics to be selected:
  select_metric = 1_ip
  
  isSizeField = .true.
  call metric%alloca(mesh%ndime,mesh%npoin,isSizeField)
  PI=4.0_rp*DATAN(1.0_rp)
  do iy=0_ip,ny
    do ix=0_ip,nx
      inode = (1+ix) + iy*(1+nx)
      select case (select_metric)
        case(  1_ip  )
          
          metric%M = hx/2.0
          
        case default
          metric%M = hx
      end select
    end do
  end do

  call compute_mesh_quality_shape(mesh,q)
  call quality_deallo(q)
  call compute_mesh_quality_sizeShape(mesh,metric,q)
  if(out_paraview_test) then
    call out_paraview_inp(mesh,filename='./unitt/unitt_adapti_bisectEdge/mesh_0',nodeField=metric%M(1,1,:),elemField=q)
  end if
  call quality_deallo(q)

!   !call adapt_mesh_to_metric(mesh,metric,mapNodes_input_to_adapted_mesh=mapNodes_old_to_new,lock_valid_elems=.false.)
  call adapt_mesh_to_metric(mesh,metric,lock_valid_elems=.false.)

  call compute_mesh_quality_shape(mesh,q)
  call quality_deallo(q)
  call compute_mesh_quality_sizeShape(mesh,metric,q)
  if(out_paraview_test) then
    call out_paraview_inp(mesh,filename='./unitt/unitt_adapti_bisectEdge/mesh_1',nodeField=metric%M(1,1,:),elemField=q)
  end if
  call quality_deallo(q)

  if(mesh%nelem.ne.4) then
    print*,'bisection error'
    call runend('Some error in the adaptive process: inner edge (with boundary nodes) should be bisected...')
  end if

  call mesh%deallo
  call metric%deallo
!   call memory_deallo(memor_adapt,'mapNodes_old_to_new','unitt_adaptivity',mapNodes_old_to_new)
  
!   call runend('O.K.!')                                               ! Finish Alya
  
end program unitt_adaptivity     
