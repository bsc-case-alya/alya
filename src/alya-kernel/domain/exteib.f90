!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine exteib(iimbo,pnodb,iboib,bouno,fringe)
  !-----------------------------------------------------------------------
  !****f* exteib/exteib
  ! NAME
  !    exteib
  ! DESCRIPTION
  !    This routines computes the exterior normal to the IB 
  ! USED BY
  !    domain
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_master, only     :  imbou
  use def_domain, only     :  ndime,coord
  implicit none
  integer(ip), intent(in)  :: iimbo,pnodb,iboib,fringe
  real(rp),    intent(out) :: bouno(ndime)
  integer(ip)              :: p1,p2,p3
  real(rp)                 :: xfact,vec(3,3)

  if ( fringe == 0 ) then
     if( pnodb == 2 ) then
        p1       = imbou(iimbo) % lnoib(1,iboib)
        p2       = imbou(iimbo) % lnoib(2,iboib)
        vec(1,1) = imbou(iimbo) % cooib(1,p2) - imbou(iimbo) % cooib(1,p1)
        vec(2,1) = imbou(iimbo) % cooib(2,p2) - imbou(iimbo) % cooib(2,p1)
        bouno(1) =  vec(2,1)
        bouno(2) = -vec(1,1)
     else if( pnodb == 3 ) then
        p1       = imbou(iimbo) % lnoib(1,iboib)
        p2       = imbou(iimbo) % lnoib(2,iboib)
        p3       = imbou(iimbo) % lnoib(3,iboib)
        call nortri(p1,p2,p3,imbou(iimbo) % cooib,vec,ndime)
        bouno(1) = 0.5_rp*vec(1,3)
        bouno(2) = 0.5_rp*vec(2,3)
        bouno(3) = 0.5_rp*vec(3,3)
     else
        call runend('EXTEIB: COULD NOT COMPUTE EXTERIOR NORMAL')
     end if
     call vecuni(ndime,bouno,xfact)
  else
     if( pnodb == 2 ) then
        p1       = imbou(iimbo) % lnofb(1,iboib)
        p2       = imbou(iimbo) % lnofb(2,iboib)
        vec(1,1) = coord(1,p2) - coord(1,p1)
        vec(2,1) = coord(2,p2) - coord(2,p1)
        bouno(1) =  vec(2,1)
        bouno(2) = -vec(1,1)
     else if( pnodb == 3 ) then
        p1       = imbou(iimbo) % lnofb(1,iboib)
        p2       = imbou(iimbo) % lnofb(2,iboib)
        p3       = imbou(iimbo) % lnofb(3,iboib)
        call nortri(p1,p2,p3,coord,vec,ndime)
        bouno(1) = 0.5_rp*vec(1,3)
        bouno(2) = 0.5_rp*vec(2,3)
        bouno(3) = 0.5_rp*vec(3,3)
     else
        call runend('EXTEIB: COULD NOT COMPUTE EXTERIOR NORMAL')
     end if
     call vecuni(ndime,bouno,xfact)

  end if

end subroutine exteib
