!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



module mod_bouder

  use def_kintyp, only     :  ip,rp
  implicit none
  
  public :: bouder

contains

!-----------------------------------------------------------------------
!****f* Domain/bouder
! NAME
!    bouder
! DESCRIPTION
!    This routine calculates the baloc and eucta.
! USES
!    vecnor
!    vecpro
! USED BY
!    nsm_bouope
!    tem_bouope
! SOURCE
!***
!-----------------------------------------------------------------------

  pure subroutine bouder(nnodb,ndime,ndimb,deriv,bocod,baloc,eucta)

    integer(ip), intent(in)  :: nnodb,ndime,ndimb
    real(rp),    intent(in)  :: deriv(max(1_ip,ndimb),nnodb)
    real(rp),    intent(in)  :: bocod(ndime,nnodb)
    real(rp),    intent(out) :: eucta,baloc(ndime,ndime)
    integer(ip)              :: i,j,k
     
    if( ndime == 1 ) then
       !
       ! 1D
       !
       baloc(1,1) =   1.0_rp
       eucta      =   baloc(1,1)*baloc(1,1)
       eucta      =   sqrt(eucta)
       
    else if( ndime == 2 ) then
       !
       ! 2D
       !
       do i=1,ndime
          do j=1,ndimb
             baloc(i,j)=0.0_rp
             do k=1,nnodb
                baloc(i,j)=baloc(i,j)+bocod(i,k)*deriv(j,k)
             end do
          end do
       end do

       baloc(1,2) =   baloc(2,1)
       baloc(2,2) = - baloc(1,1)
       eucta      =   baloc(1,2) * baloc(1,2) &
            &       + baloc(2,2) * baloc(2,2)
       eucta      =   sqrt(eucta)

    else if( ndime == 3 ) then
       !
       ! 3D
       !
       do i=1,3
          do j=1,2
             baloc(i,j)=0.0_rp
             do k=1,nnodb
                baloc(i,j)=baloc(i,j)+bocod(i,k)*deriv(j,k)
             end do
          end do
       end do
       
       baloc(1,3) =   baloc(2,1) * baloc(3,2) - baloc(3,1) * baloc(2,2)
       baloc(2,3) =   baloc(3,1) * baloc(1,2) - baloc(1,1) * baloc(3,2)
       baloc(3,3) =   baloc(1,1) * baloc(2,2) - baloc(2,1) * baloc(1,2)
       eucta      =   baloc(1,3) * baloc(1,3) &
            &       + baloc(2,3) * baloc(2,3) &
            &       + baloc(3,3) * baloc(3,3)
       eucta      =   sqrt(eucta)

       ! recalculate t1 so that it is orthogonal to t2
       baloc(1,1) =   baloc(2,3) * baloc(3,2) - baloc(3,3) * baloc(2,2)
       baloc(2,1) =   baloc(3,3) * baloc(1,2) - baloc(1,3) * baloc(3,2)
       baloc(3,1) =   baloc(1,3) * baloc(2,2) - baloc(2,3) * baloc(1,2)

    end if

  end subroutine bouder

end module mod_bouder
