!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



module def_meteo
  use def_kintyp

  !----------------------------------------------------------------------
  !
  ! Meteo
  !
  !----------------------------------------------------------------------


  type meteo
     integer(ip)                    :: version, nx, ny, iproj
     real(rp)                       :: xfcst, xlvl, startlat, startlon, starti, startj, &
          deltalat, deltalon, dx, dy, xlonc, &
          truelat1, truelat2, earth_radius
     real(rp), pointer, dimension(:,:) :: slab
     logical                       :: is_wind_grid_rel
     character (len=8)             :: startloc
     character (len=9)             :: field
     character (len=24)            :: hdate
     character (len=25)            :: units
     character (len=32)            :: map_source
     character (len=46)            :: desc
  end type meteo

  ! Projection codes for proj_info structure:
  INTEGER, PUBLIC, PARAMETER  :: PROJ_LATLON = 0
  INTEGER, PUBLIC, PARAMETER  :: PROJ_LC = 1
  INTEGER, PUBLIC, PARAMETER  :: PROJ_PS = 2
  INTEGER, PUBLIC, PARAMETER  :: PROJ_PS_WGS84 = 102
  INTEGER, PUBLIC, PARAMETER  :: PROJ_MERC = 3
  INTEGER, PUBLIC, PARAMETER  :: PROJ_GAUSS = 4
  INTEGER, PUBLIC, PARAMETER  :: PROJ_CYL = 5
  INTEGER, PUBLIC, PARAMETER  :: PROJ_CASSINI = 6
  INTEGER, PUBLIC, PARAMETER  :: PROJ_ALBERS_NAD83 = 105
  INTEGER, PUBLIC, PARAMETER  :: PROJ_ROTLL = 203
  real(rp), parameter :: EARTH_RADIUS_M = 6370000.0_rp   ! same as MM5 system

  integer(8)                        ::     memor_meteo(2)
  !real(rp),pointer            :: uvel(:),vvel(:),temp(:),pres(:)




end module def_meteo
