!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_mesh_grandpa_permutation
  !
  ! Test grandparent permutation index calculation 
  !
  !     1. Create "mesh"
  !     2. Extract "mesh_1" and "mesh_2" from "mesh" using a mask such that:
  !          "mesh" = "mesh_1" + "mesh_2" 
  !     3. Extract the boundary mesh "mesh_bou_2" from "mesh_2" 
  !     4. Compute the grandparent permutation index arrays that give the "mesh" indices
  !          for each index in "mesh_bou_2", for both nodes and elements. 
  !
  ! ----------------------------------------------------------------------------
  !
  !        mesh        =     mesh_1      +     mesh_2
  !
  !     -------------                       -------------
  !     |   |   |   |                       |   |   |   |
  !     ------------      ---------         ------------
  !     |   |   |   |  =  |   |   |      +          |   |
  !     -------------     -------------             -----
  !     |   |   |   |     |   |   |   |     
  !     -------------     -------------     
  !
  ! ----------------------------------------------------------------------------
  use def_elmtyp
  use def_kintyp_basic
  use def_kintyp_mesh_basic
  use mod_elmgeo
  implicit none

  type(mesh_type_basic) :: mesh       ! Original mesh
  type(mesh_type_basic) :: mesh_1     ! Mesh extracted with mask
  type(mesh_type_basic) :: mesh_2     ! Mesh complementary to "mesh_1": mesh_2 = mesh \ mesh_1
  type(mesh_type_basic) :: mesh_bou   ! Boundary mesh of "mesh"
  type(mesh_type_basic) :: mesh_bou_1 ! Boundary mesh of "mesh_1"
  type(mesh_type_basic) :: mesh_bou_2 ! Boundary mesh of "mesh_2"
  logical(lg), pointer  :: lmask(:)   ! Mask used to extract "mesh_1" from "mesh"

  integer(ip), pointer  :: coord_is_same(:) 
  integer(ip)           :: ipoin, kpoin, idime, ielem
  integer(ip)           :: inode, knode, kboun


  nullify(lmask)
  nullify(coord_is_same)


  ! Initialize meshes
  call elmgeo_element_type_initialization()

  call mesh       % init('MY_MESH')
  call mesh_1     % init('MY_MESH')
  call mesh_2     % init('MY_MESH')
  call mesh_bou   % init('BOUNDARY')
  call mesh_bou_1 % init('BOUNDARY')
  call mesh_bou_2 % init('BOUNDARY')

  ! Define mesh
  mesh % nelem = 9
  mesh % npoin = 16
  mesh % ndime = 2
  mesh % mnode = 4

  call mesh % alloca()

  mesh % lnods(:,1)  = (/ 12 ,  7 , 5 , 10 /)
  mesh % lnods(:,2)  = (/ 14 ,  9 , 7 , 12 /)
  mesh % lnods(:,3)  = (/ 16 , 15 , 9 , 14 /) 
  mesh % lnods(:,4)  = (/  7 ,  4 , 2 ,  5 /)
  mesh % lnods(:,5)  = (/  9 ,  8 , 4 ,  7 /) 
  mesh % lnods(:,6)  = (/ 15 , 13 , 8 ,  9 /)  
  mesh % lnods(:,7)  = (/  4 ,  3 , 1 ,  2 /) 
  mesh % lnods(:,8)  = (/  8 ,  6 , 3 ,  4 /) 
  mesh % lnods(:,9)  = (/ 13 , 11 , 6 ,  8 /)

  mesh % ltype(1:9)  = QUA04

  mesh % coord(:,1 ) = (/ 0.000000e+00_rp , 1.000000e+00_rp /) 
  mesh % coord(:,2 ) = (/ 0.000000e+00_rp , 6.666667e-01_rp /) 
  mesh % coord(:,3 ) = (/ 3.333333e-01_rp , 1.000000e+00_rp /) 
  mesh % coord(:,4 ) = (/ 3.333333e-01_rp , 6.666667e-01_rp /) 
  mesh % coord(:,5 ) = (/ 0.000000e+00_rp , 3.333333e-01_rp /) 
  mesh % coord(:,6 ) = (/ 6.666667e-01_rp , 1.000000e+00_rp /) 
  mesh % coord(:,7 ) = (/ 3.333333e-01_rp , 3.333333e-01_rp /) 
  mesh % coord(:,8 ) = (/ 6.666667e-01_rp , 6.666667e-01_rp /) 
  mesh % coord(:,9 ) = (/ 6.666667e-01_rp , 3.333333e-01_rp /) 
  mesh % coord(:,10) = (/ 0.000000e+00_rp , 0.000000e+00_rp /) 
  mesh % coord(:,11) = (/ 1.000000e+00_rp , 1.000000e+00_rp /) 
  mesh % coord(:,12) = (/ 3.333333e-01_rp , 0.000000e+00_rp /) 
  mesh % coord(:,13) = (/ 1.000000e+00_rp , 6.666667e-01_rp /) 
  mesh % coord(:,14) = (/ 6.666667e-01_rp , 0.000000e+00_rp /) 
  mesh % coord(:,15) = (/ 1.000000e+00_rp , 3.333333e-01_rp /) 
  mesh % coord(:,16) = (/ 1.000000e+00_rp , 0.000000e+00_rp /) 

  allocate(lmask(mesh%nelem))
  lmask(1:5) = .true.
  lmask(6:9) = .false.


  ! Extract mesh using mask
  call mesh_1 % extract(mesh,lmask,mesh_cmp=mesh_2)


  ! Extract boundary meshes for all volume meshes
  call mesh_bou % boundary_mesh(mesh)
  call mesh_bou % output(filename='boundary')
  call mesh_bou_1 % boundary_mesh(mesh_1)
  call mesh_bou_1 % output(filename='boundary_1')
  call mesh_bou_2 % boundary_mesh(mesh_2)
  call mesh_bou_2 % output(filename='boundary_2')

  allocate(coord_is_same(mesh % ndime))


  ! Get grandparent mesh permutation arrays for nodes and elements
  call mesh_bou_2 % grandpa_permutation(mesh_2)


  ! Check node permutation array in mesh_bou_2 with respect to mesh 
  do kpoin = 1, mesh_bou_2 % npoin
     ipoin = mesh_bou_2 % permn_grandpa(kpoin)
     do idime = 1, mesh_bou_2 % ndime
        if( abs(mesh % coord(idime,ipoin) - mesh_bou_2 % coord(idime,kpoin)) > 1.0e-12_rp ) then
           print*,'Wrong grandparent node permutation in mesh_bou_2 with respect to mesh'
           stop 1 
        end if
     end do
  end do


  ! Check element permutation array in mesh_bou_2 with respect to mesh
  do kboun = 1, mesh_bou_2 % nelem
     ielem = mesh_bou_2 % perme_grandpa(kboun)
     coord_is_same(:) = 0
     do knode = 1, mesh_bou_2 % mnode
        kpoin = mesh_bou_2 % lnods(knode,kboun)
        do inode = 1, mesh % mnode
           ipoin = mesh % lnods(inode,ielem)
           do idime = 1, mesh_bou_2 % ndime
              if( abs(mesh % coord(idime,ipoin) - mesh_bou_2 % coord(idime,kpoin)) <= 1.0e-12_rp ) then
                 coord_is_same(idime) = coord_is_same(idime) + 1
              end if
           end do
        end do
     end do
     if( any(coord_is_same < mesh_bou_2 % mnode) ) then
        print*,'Wrong grandparent element permutation in mesh_bou_2 with respect to mesh'
        stop 1 
     end if
  end do


  deallocate(lmask)
  deallocate(coord_is_same)


end program unitt_mesh_grandpa_permutation
