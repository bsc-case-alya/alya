!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_deallo(itask)
  !------------------------------------------------------------------------
  !****f* Parall/par_deallo
  ! NAME
  !    par_deallo
  ! DESCRIPTION
  !    Deallocate memory for master arrays used by Parall 
  ! OUTPUT
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_parall
  use mod_memchk
  use mod_parall, only : par_memor
  implicit none
  integer(ip), intent(in) :: itask
  integer(4)              :: istat

  select case (itask)

  case(1_ip)
     !
     ! PARR1
     !
     call memchk(two,istat,par_memor,'PARR1','par_deallo',parr1)
     deallocate(parr1,stat=istat)
     if(istat/=0) call memerr(two,'par_deallo','PARR1',0_ip) 

  case(2_ip)
     !
     ! PARR2
     !
     call memchk(two,istat,par_memor,'PARR2','par_deallo',parr2)
     deallocate(parr2,stat=istat)
     if(istat/=0) call memerr(two,'par_deallo','PARR2',0_ip) 

  case(3)
     !
     ! PARR3
     !
     call memchk(two,istat,par_memor,'PARR3','par_deallo',parr3)
     deallocate(parr3,stat=istat)
     if(istat/=0) call memerr(two,'par_deallo','PARR3',0_ip) 

  case(4_ip)
     !
     ! PARI1
     !
     call memchk(two,istat,par_memor,'PARI1','par_deallo',pari1)
     deallocate(pari1,stat=istat)
     if(istat/=0) call memerr(two,'par_deallo','PARI1',0_ip) 

  end select

end subroutine par_deallo
