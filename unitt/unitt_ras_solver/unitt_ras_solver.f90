!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_ras_solver
  !
  ! Order strings
  !
  use def_kintyp_basic, only : ip,rp,i1p
  use def_master,       only : rate_time,zeror
  use def_mat_csr
  use def_mat_dia
  use def_mat_sky
  use def_iterative_solvers
  use def_direct_solvers
  use def_solver
  use def_solvers
  use mod_driver_solvers
  use def_all_solvers
  use def_preconditioners
  implicit none

  class(solver),           pointer   :: sol
  type(mat_csr)                      :: a
  real(rp),                pointer   :: x(:),b(:),r(:)
  integer(ip)                        :: ii,iz,num
  integer(8)                         :: count_rate8
  ! Deflated CG
  integer(ip)                        :: ngrou
  integer(ip),             pointer   :: ia(:)
  integer(ip),             pointer   :: ja(:)
  type(i1p),               pointer   :: lgrou(:)
  integer(ip),             pointer   :: pgrou(:)
  !
  ! Nullify
  !
  nullify(x,b,r,ia,ja,lgrou,pgrou,sol)
  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)
  !
  ! Allocate matrix
  !
  call a % init   ()
  a % nrows = 11
  a % nz    = 31
  call a % alloca ()
  allocate(b(a % nrows))
  allocate(x(a % nrows))
  allocate(r(a % nrows))

  iz = 0
  a % iA(1) = 1 
  a % iA(2) = 3 
  iz = iz + 1 ; a % vA(1,1,iz) = 1.0_rp ; a % jA(iz) = 1
  iz = iz + 1 ; a % vA(1,1,iz) = 0.0_rp ; a % jA(iz) = 2

  do ii = 2,a % nrows-1
     if( ii == 2 ) then
        iz = iz + 1 ; a % vA(1,1,iz) =  0.0_rp ; a % jA(iz) = ii-1
     else 
        iz = iz + 1 ; a % vA(1,1,iz) = -1.0_rp ; a % jA(iz) = ii-1
     end if
     iz = iz + 1 ; a % vA(1,1,iz) =  2.0_rp ; a % jA(iz) = ii
     if( ii == a % nrows-1 ) then
        iz = iz + 1 ; a % vA(1,1,iz) =  0.0_rp ; a % jA(iz) = ii+1
     else
        iz = iz + 1 ; a % vA(1,1,iz) = -1.0_rp ; a % jA(iz) = ii+1
     end if
     a % iA(ii+1) = a % iA(ii) + 3
  end do

  ii = a % nrows
  a % iA(ii+1) = a % iA(ii) + 2
  iz = iz + 1 ; a % vA(1,1,iz) = 0.0_rp ; a % jA(iz) = ii-1
  iz = iz + 1 ; a % vA(1,1,iz) = 1.0_rp ; a % jA(iz) = ii

  call a % output(FMT='DENSE')

  !ngrou = 2
  !allocate(lgrou(ngrou))
  !allocate(lgrou(1)%l(6))
  !allocate(lgrou(2)%l(7))
  !lgrou(1) % l = (/1,2,3,4,5,6/)
  !lgrou(2) % l = (/5,6,7,8,9,10,11/)

  allocate(pgrou(a % nrows))
  pgrou = (/1,1,1,1,1,2,2,2,2,2,2/)
  ngrou = 2
  allocate(lgrou(ngrou))
  allocate(lgrou(1)%l(8))
  allocate(lgrou(2)%l(8))
  lgrou(1) % l = (/1,2,3,4,5,6,7,8/)
  lgrou(2) % l = (/4,5,6,7,8,9,10,11/)
  !
  ! Solvers
  !
  do ii = 1,3

     x     = 0.0_rp
     b     = 0.1_rp
     b(1)  = 0.0_rp
     b(11) = 0.0_rp
      
     call select_solver(ii,num)
     
     print*,'alloca'
     call solver_alloca(sol,num)
     print*,'init'
     call solver_init  (sol)
     print*,'dim'
     call solver_dim   (sol,a)

     sol % input % relax            = 0.7_rp
     sol % input % solco            = 1.0e-8_rp
     sol % input % miter            = 1000
     sol % input % kfl_exres        = 1
     sol % input % kfl_cvgso        = 1
     sol % input % lun_cvgso        = 90+ii
     sol % input % max_dof_ras      = 0
     sol % input % verbose          = 1     
     sol % input % kfl_preco        = SOL_SOLVER_RAS
     !
     ! Set solver/preconditioner arrays
     !
     print*,'set preconditioner'
     select type ( sol )
     class is ( iterative_solver )
        call preconditioner_alloca(sol)    
        if( sol % input % kfl_preco == SOL_SOLVER_RAS ) then
           select type ( v => sol % preco(1) % p )
           class is ( ras )
              if (      ii == 1 ) then
                 call v % set(a)                 
              else if ( ii == 2 ) then
                 call v % set(a,lgrou)
              else if ( ii == 3 ) then
                 call v % set(a,lgrou,pgrou)                 
              end if
           end select
        end if       
     end select
     !
     ! Set groups and symbolic factorization
     !
     print*,'symbolic'
     select type ( sol )
     class is ( ras ) ; call sol % set(a)
     end select
     !
     ! Solver preprocess, solve and postprocess
     !
     print*,'preprocess'
     call solver_preprocess (sol,b,x,a)
     print*,'solve' 
     call solver_solve      (sol,b,x,a)
     print*,'postprocess'
     call solver_postprocess(sol,b,x,a)

     !print*,'x=',x
     !
     ! Residual
     !
     select type ( sol )
     class is ( direct_solver )
        call a % residual(x,r,b)
        sol % output % resip_final = sol % parallel_L2norm(r)
        sol % output % resid_final = sol % output % resip_final 
     end select
     !
     ! Output
     !
     print*,'resip     = ',sol % output % resip_final
     print*,'resid     = ',sol % output % resid_final
     print*,'iters     = ',sol % output % iters
     print*,'||b||     = ',sol % output % bnorm
     print*,'||L^-1b|| = ',sol % output % bnorp

     if( sol % output % resip_final > sol % input % solco ) stop 1
     
     print*,'deallocate'

     call solver_deallo(sol)
     deallocate(sol)
     
  end do

contains

  subroutine select_solver(ii,num)
    
    integer(ip), intent(in)  :: ii
    integer(ip), intent(out) :: num

    print*,' '
    print*,'RICHARDSON, PREC=RAS'
    print*,'--------------------'
    num = SOL_SOLVER_RICHARDSON 

  end subroutine select_solver
  
end program unitt_ras_solver


