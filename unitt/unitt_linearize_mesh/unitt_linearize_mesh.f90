!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_linearize_mesh

  use def_kintyp_basic,      only : ip,rp
  use def_kintyp_mesh_basic, only : mesh_type_basic 
  use mod_elmgeo,            only : elmgeo_element_type_initialization
  use def_elmtyp

  implicit none
  type(mesh_type_basic) :: mesh
  type(mesh_type_basic) :: mesh_linear
  integer(ip)           :: ielem
  
  call elmgeo_element_type_initialization()
  call mesh        % init()
  call mesh_linear % init()
  !
  ! 2D mesh
  !
  mesh % nelem =  6
  mesh % npoin = 27
  mesh % ndime =  2
  mesh % mnode = 16

  call mesh % alloca()

  mesh % ltype       = [ QUA09,QUA04,TRI03,TRI03,TRI06,QUA16 ]
  mesh % coord(:, 1) = [  0.0_rp, 0.0_rp ]
  mesh % coord(:, 2) = [  1.0_rp, 0.0_rp ]
  mesh % coord(:, 3) = [  1.0_rp, 1.0_rp ]
  mesh % coord(:, 4) = [  0.0_rp, 1.0_rp ]
  mesh % coord(:, 5) = [  0.5_rp, 0.0_rp ]
  mesh % coord(:, 6) = [  1.0_rp, 0.5_rp ]
  mesh % coord(:, 7) = [  0.5_rp, 1.0_rp ] 
  mesh % coord(:, 8) = [  0.0_rp, 0.5_rp ]
  mesh % coord(:, 9) = [  0.5_rp, 0.5_rp ]
  mesh % coord(:,10) = [  1.0_rp, 2.0_rp ]
  mesh % coord(:,11) = [  0.5_rp, 2.0_rp ]
  mesh % coord(:,12) = [  0.0_rp, 2.0_rp ]
  mesh % coord(:,13) = [ -0.5_rp, 1.0_rp ]
  mesh % coord(:,14) = [ -1.0_rp, 1.0_rp ]
  mesh % coord(:,15) = [ -0.5_rp, 0.5_rp ]

  mesh % coord(:,16) = [  1.5_rp, 0.0_rp ]
  mesh % coord(:,17) = [  1.5_rp, 0.5_rp ]
  mesh % coord(:,18) = [  1.5_rp, 1.0_rp ]
  mesh % coord(:,19) = [  1.5_rp, 2.0_rp ]

  mesh % coord(:,20) = [  2.0_rp, 0.0_rp ]
  mesh % coord(:,21) = [  2.0_rp, 0.5_rp ]
  mesh % coord(:,22) = [  2.0_rp, 1.0_rp ]
  mesh % coord(:,23) = [  2.0_rp, 2.0_rp ]

  mesh % coord(:,24) = [  2.5_rp, 0.0_rp ]
  mesh % coord(:,25) = [  2.5_rp, 0.5_rp ]
  mesh % coord(:,26) = [  2.5_rp, 1.0_rp ]
  mesh % coord(:,27) = [  2.5_rp, 2.0_rp ]

  mesh % lnods(1: 9,1) = [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
  mesh % lnods(1: 4,2) = [ 7, 3, 10, 11 ]
  mesh % lnods(1: 3,3) = [ 4, 7, 12 ]
  mesh % lnods(1: 3,4) = [ 7, 11, 12 ]
  mesh % lnods(1: 6,5) = [ 1, 4, 14, 8, 13, 15 ]
  mesh % lnods(1:16,6) = [ 2, 24, 27, 10, 16, 20, 25, 26, 23, 19, 3, 6, 17, 21, 22, 18 ]

  call mesh_linear % linearize(mesh)  
  call mesh_linear % print_info()  
  call mesh_linear % output (FILENAME='linear_mesh_2d')
  call mesh_linear % deallo()  
  call mesh        % deallo()
  !
  ! HEX27 and TET10
  ! 
  mesh % nelem =  3
  mesh % npoin = 41
  mesh % ndime =  3
  mesh % mnode = 27

  call mesh % alloca()
  
  mesh % ltype(1)      = HEX27
  mesh % lnods(:,1)    = [(ielem,ielem=1,27)]
  mesh % coord(1:3, 1) = [ -1.0_rp , -1.0_rp , -1.0_rp ]     
  mesh % coord(1:3, 2) = [  1.0_rp , -1.0_rp , -1.0_rp ]     
  mesh % coord(1:3, 3) = [  1.0_rp ,  1.0_rp , -1.0_rp ]     
  mesh % coord(1:3, 4) = [ -1.0_rp ,  1.0_rp , -1.0_rp ]     
  mesh % coord(1:3, 5) = [ -1.0_rp , -1.0_rp ,  1.0_rp ]     
  mesh % coord(1:3, 6) = [  1.0_rp , -1.0_rp ,  1.0_rp ]     
  mesh % coord(1:3, 7) = [  1.0_rp ,  1.0_rp ,  1.0_rp ]     
  mesh % coord(1:3, 8) = [ -1.0_rp ,  1.0_rp ,  1.0_rp ]     
  mesh % coord(1:3, 9) = [  0.0_rp , -1.0_rp , -1.0_rp ]     
  mesh % coord(1:3,10) = [  1.0_rp ,  0.0_rp , -1.0_rp ]     
  mesh % coord(1:3,11) = [  0.0_rp ,  1.0_rp , -1.0_rp ]     
  mesh % coord(1:3,12) = [ -1.0_rp ,  0.0_rp , -1.0_rp ]     
  mesh % coord(1:3,13) = [ -1.0_rp , -1.0_rp ,  0.0_rp ]     
  mesh % coord(1:3,14) = [  1.0_rp , -1.0_rp ,  0.0_rp ]     
  mesh % coord(1:3,15) = [  1.0_rp ,  1.0_rp ,  0.0_rp ]     
  mesh % coord(1:3,16) = [ -1.0_rp ,  1.0_rp ,  0.0_rp ]     
  mesh % coord(1:3,17) = [  0.0_rp , -1.0_rp ,  1.0_rp ]     
  mesh % coord(1:3,18) = [  1.0_rp ,  0.0_rp ,  1.0_rp ]     
  mesh % coord(1:3,19) = [  0.0_rp ,  1.0_rp ,  1.0_rp ]     
  mesh % coord(1:3,20) = [ -1.0_rp ,  0.0_rp ,  1.0_rp ]     
  mesh % coord(1:3,21) = [  0.0_rp ,  0.0_rp , -1.0_rp ]     
  mesh % coord(1:3,22) = [  0.0_rp , -1.0_rp ,  0.0_rp ]     
  mesh % coord(1:3,23) = [  1.0_rp ,  0.0_rp ,  0.0_rp ]     
  mesh % coord(1:3,24) = [  0.0_rp ,  1.0_rp ,  0.0_rp ]     
  mesh % coord(1:3,25) = [ -1.0_rp ,  0.0_rp ,  0.0_rp ]    
  mesh % coord(1:3,26) = [  0.0_rp ,  0.0_rp ,  1.0_rp ]    
  mesh % coord(1:3,27) = [  0.0_rp ,  0.0_rp ,  0.0_rp ]    

  mesh % ltype(2)      = TET10
  mesh % lnods(:,2)    = [(ielem+27,ielem=1,10)]
  mesh % coord(1:3,28) = [  2.0_rp ,  0.0_rp ,  0.0_rp ]
  mesh % coord(1:3,29) = [  3.0_rp ,  0.0_rp ,  0.0_rp ]
  mesh % coord(1:3,30) = [  2.0_rp ,  1.0_rp ,  0.0_rp ]
  mesh % coord(1:3,31) = [  2.0_rp ,  0.0_rp ,  1.0_rp ]
  mesh % coord(1:3,32) = [  2.5_rp ,  0.0_rp ,  0.0_rp ]
  mesh % coord(1:3,33) = [  2.5_rp ,  0.5_rp ,  0.0_rp ]
  mesh % coord(1:3,34) = [  2.0_rp ,  0.5_rp ,  0.0_rp ]
  mesh % coord(1:3,35) = [  2.0_rp ,  0.0_rp ,  0.5_rp ]
  mesh % coord(1:3,36) = [  2.5_rp ,  0.0_rp ,  0.5_rp ]
  mesh % coord(1:3,37) = [  2.0_rp ,  0.5_rp ,  0.5_rp ]

  mesh % ltype(3)      = TET04
  mesh % lnods(:,3)    = [(ielem+37,ielem=1,4)]
  mesh % coord(1:3,38) = [  4.0_rp ,  0.0_rp ,  0.0_rp ]   
  mesh % coord(1:3,39) = [  5.0_rp ,  0.0_rp ,  0.0_rp ]   
  mesh % coord(1:3,40) = [  5.0_rp ,  1.0_rp ,  0.0_rp ]   
  mesh % coord(1:3,41) = [  4.0_rp ,  0.0_rp ,  1.0_rp ]
  
  call mesh_linear % linearize(mesh)  
  call mesh_linear % print_info()  
  call mesh_linear % output (FILENAME='linear_mesh_3d')
  call mesh_linear % deallo()  
  !
  ! Tretahedrize mesh
  !
  call mesh_linear % tet04_mesh(mesh)  
  call mesh_linear % print_info()  
  call mesh_linear % output (FILENAME='tet04_mesh_3d')

  call mesh        % deallo()

end program unitt_linearize_mesh
