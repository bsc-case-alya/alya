!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_mesh_tags
  !
  ! Test copy, extract and merge operators
  !
  use def_elmtyp
  use def_kintyp_basic
  use def_kintyp_mesh_basic
  use mod_elmgeo
  implicit none

  type(mesh_type_basic) :: mesh
  type(mesh_type_basic) :: mesh_1
  type(mesh_type_basic) :: mesh_2
  type(mesh_type_basic) :: mesh_cpy
  logical(lg), pointer  :: lmask(:)
  integer(ip)           :: ipoin,idime,ielem,inode

  call elmgeo_element_type_initialization()

  call mesh     % init('MY_MESH')
  call mesh_1   % init('MY_MESH')
  call mesh_2   % init('MY_MESH')
  call mesh_cpy % init('MY_MESH')

  mesh % nelem = 9
  mesh % npoin = 16
  mesh % ndime = 2
  mesh % mnode = 4
  mesh % ntags = 2
  call mesh % alloca()

  mesh % lnods(:,1)  = (/  12 ,7 ,5 ,10  /)
  mesh % lnods(:,2)  = (/  14 ,9 ,7 ,12   /)
  mesh % lnods(:,3)  = (/  16, 15, 9 ,14  /) 
  mesh % lnods(:,4)  = (/  7, 4 ,2 ,5   /)
  mesh % lnods(:,5)  = (/  9 ,8 ,4 ,7  /) 
  mesh % lnods(:,6)  = (/  15, 13, 8, 9 /)  
  mesh % lnods(:,7)  = (/  4 ,3 ,1 ,2  /) 
  mesh % lnods(:,8)  = (/  8 ,6 ,3 ,4  /) 
  mesh % lnods(:,9)  = (/  13 ,11, 6, 8  /)

  mesh % ltype(1:9)  = QUA04

  mesh % coord(:,1 ) = (/   0.000000e+00_rp ,  1.000000e+00_rp /) 
  mesh % coord(:,2 ) = (/   0.000000e+00_rp ,  6.666667e-01_rp /) 
  mesh % coord(:,3 ) = (/   3.333333e-01_rp ,  1.000000e+00_rp /) 
  mesh % coord(:,4 ) = (/   3.333333e-01_rp ,  6.666667e-01_rp /) 
  mesh % coord(:,5 ) = (/   0.000000e+00_rp ,  3.333333e-01_rp /) 
  mesh % coord(:,6 ) = (/   6.666667e-01_rp ,  1.000000e+00_rp /) 
  mesh % coord(:,7 ) = (/   3.333333e-01_rp ,  3.333333e-01_rp /) 
  mesh % coord(:,8 ) = (/   6.666667e-01_rp ,  6.666667e-01_rp /) 
  mesh % coord(:,9 ) = (/   6.666667e-01_rp ,  3.333333e-01_rp /) 
  mesh % coord(:,10) = (/   0.000000e+00_rp ,  0.000000e+00_rp /) 
  mesh % coord(:,11) = (/   1.000000e+00_rp ,  1.000000e+00_rp /) 
  mesh % coord(:,12) = (/   3.333333e-01_rp ,  0.000000e+00_rp /) 
  mesh % coord(:,13) = (/   1.000000e+00_rp ,  6.666667e-01_rp /) 
  mesh % coord(:,14) = (/   6.666667e-01_rp ,  0.000000e+00_rp /) 
  mesh % coord(:,15) = (/   1.000000e+00_rp ,  3.333333e-01_rp /) 
  mesh % coord(:,16) = (/   1.000000e+00_rp ,  0.000000e+00_rp /) 

  mesh % tags(1) % TYPE = 2 ! ELEMENT
  mesh % tags(2) % TYPE = 1 ! NODES

  call mesh % alloca_tag(1_ip)
  do ielem = 1,mesh % nelem
     mesh % tags(1) % values(ielem) = real(ielem,rp)
  end do
  call mesh % alloca_tag(2_ip)
  do ipoin = 1,mesh % npoin
     mesh % tags(2) % values(ipoin) = real(ipoin,rp)
  end do

  print*,'copy'
  mesh_cpy = mesh
  do ielem = 1,mesh % nelem
     do inode = 1,mesh % mnode
        if( mesh_cpy % lnods(inode,ielem) /= mesh % lnods(inode,ielem) ) then
           print*,'Wrong lnods'
           stop 1 
        end if
     end do
     if( abs(mesh_cpy % tags(1) % values(ielem) - mesh % tags(1) % values(ielem)) > 1.0e-12_rp ) then
        print*,'Wrong tag'
        stop 2
     end if
  end do
  do ipoin = 1,mesh % npoin
     do idime = 1,mesh % ndime
        if( abs(mesh_cpy % coord(idime,ipoin)-mesh % coord(idime,ipoin)) > 1.0e-12_rp ) then
           print*,'Wrong coord'
           stop 1
        end if
        if( abs(mesh_cpy % tags(2) % values(ipoin) - mesh % tags(2) % values(ipoin)) > 1.0e-12_rp ) then
           print*,'Wrong tag'
           stop 2
        end if
     end do
  end do
  !
  ! Extract and merge
  !
  allocate(lmask(mesh%nelem))
  lmask(1:5) = .true.
  lmask(6:9) = .false.

  print*,'extract'
  call mesh_1 % extract(mesh,lmask,mesh_cmp=mesh_2)  
  print*,'merge'
  call mesh_1 % merge(mesh_2)

  do ielem = 1,mesh_1 % nelem
     if( abs(mesh_1 % tags(1) % values(ielem) - mesh % tags(1) % values(ielem)) > 1.0e-12_rp ) then
        print*,'Wrong tag=',mesh_1 % tags(1) % values(ielem) , mesh % tags(1) % values(ielem)
        stop 2
     end if
  end do
  call mesh_1   % deallo()
  call mesh_2   % deallo()
  if( mesh_1   % memor(1) /= 0_8 ) stop 5
  if( mesh_2   % memor(1) /= 0_8 ) stop 6
  !
  ! Append
  !
  call mesh_1 % init()
  call mesh_2 % init()
  print*,'extract'
  call mesh_1 % extract(mesh,lmask,mesh_cmp=mesh_2)  
  print*,'append'
  call mesh_1 % append(mesh_2)
  do ielem = 1,mesh_1 % nelem
     if( abs(mesh_1 % tags(1) % values(ielem) - mesh % tags(1) % values(ielem)) > 1.0e-12_rp ) then
        print*,'Wrong tag=',mesh_1 % tags(1) % values(ielem) , mesh % tags(1) % values(ielem)
        stop 2
     end if
  end do  
  !
  ! Memory check
  !
  call mesh     % deallo()
  call mesh_cpy % deallo()
  call mesh_1   % deallo()
  call mesh_2   % deallo()
  if( mesh     % memor(1) /= 0_8 ) then ; print*,'memor=',mesh     % memor(1) ; stop 7  ; end if 
  if( mesh_cpy % memor(1) /= 0_8 ) then ; print*,'memor=',mesh_cpy % memor(1) ; stop 8  ; end if 
  if( mesh_1   % memor(1) /= 0_8 ) then ; print*,'memor=',mesh_1   % memor(1) ; stop 9  ; end if 
  if( mesh_2   % memor(1) /= 0_8 ) then ; print*,'memor=',mesh_2   % memor(1) ; stop 10 ; end if 

end program unitt_mesh_tags
