!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_parallel_solver
  !
  ! Order strings
  !
  use def_kintyp_basic, only : ip,rp
  use def_master,       only : rate_time,zeror
  use def_mat_csr
  use def_mat_dia
  use def_iterative_solvers
  use def_direct_solvers
  use mod_driver_solvers
  use def_solver
  use def_solvers
  use def_all_solvers
  use def_master
  use def_communications
  use mod_communications
  use def_preconditioners
  implicit none

  class(iterative_solver), pointer   :: sol
  type(mat_csr)                      :: a
  real(rp),                pointer   :: x(:),b(:),r(:)
  integer(ip)                        :: ii,iz
  integer(ip)                        :: my_rank,my_size
  integer(8)                         :: count_rate8
  ! Deflated CG
  integer(ip)                        :: nzgro
  integer(ip)                        :: ngrou
  integer(ip),             pointer   :: ia(:)
  integer(ip),             pointer   :: ja(:)
  integer(ip),             pointer   :: lgrou(:)
  ! Linelet
  integer(ip)                        :: nline
  integer(ip),             pointer   :: lline(:)
  integer(ip),             pointer   :: lrenup(:)

  nullify(x,b,r,ia,ja,lgrou,lline,lrenup,sol)

  IPARALL   = .true.
  ISEQUEN   = .false.

  call PAR_INIT          ()
  PAR_COMM_MY_CODE = MPI_COMM_WORLD
  call PAR_COMM_RANK_AND_SIZE(PAR_COMM_MY_CODE,my_rank,my_size)
  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)
  !
  ! Allocate matrix
  !
  call a % init   ()

  if( my_rank >= 1 ) then
     ISLAVE = .true.
     INOTSLAVE = .false.
     a % nrows =  6
     a % nz    = 16
  else
     ISLAVE = .false.
     INOTSLAVE = .true.
  end if

  call a % alloca ()
  if( a % nrows > 0 ) then
     allocate(b(a % nrows))
     allocate(x(a % nrows))
     allocate(r(a % nrows))
  end if

  if( my_rank == 1 ) then
     !
     ! 1  2  3  4  5  6
     ! o--o--o--o--o--o
     !
     iz = 0
     a % iA(1) = 1 
     a % iA(2) = 3 
     iz = iz + 1 ; a % vA(1,1,iz) = 1.0_rp ; a % jA(iz) = 1
     iz = iz + 1 ; a % vA(1,1,iz) = 0.0_rp ; a % jA(iz) = 2

     do ii = 2,a % nrows-1
        iz = iz + 1 ; a % vA(1,1,iz) = -1.0_rp ; a % jA(iz) = ii-1
        iz = iz + 1 ; a % vA(1,1,iz) =  2.0_rp ; a % jA(iz) = ii
        iz = iz + 1 ; a % vA(1,1,iz) = -1.0_rp ; a % jA(iz) = ii+1
        a % iA(ii+1) = a % iA(ii) + 3
     end do

     ii = a % nrows
     a % iA(ii+1) = a % iA(ii) + 2
     iz = iz + 1 ; a % vA(1,1,iz) = -1.0_rp ; a % jA(iz) = ii-1
     iz = iz + 1 ; a % vA(1,1,iz) =  1.0_rp ; a % jA(iz) = ii

  else if( my_rank == 2 ) then
     !
     ! 6  5  4  3  2  1
     ! o--o--o--o--o--o
     !
     iz = 0
     a % iA(1) = 1 
     a % iA(2) = 3 
     iz = iz + 1 ; a % vA(1,1,iz) = 1.0_rp ; a % jA(iz) = 1
     iz = iz + 1 ; a % vA(1,1,iz) = 0.0_rp ; a % jA(iz) = 2

     do ii = 2,a % nrows-1
        iz = iz + 1 ; a % vA(1,1,iz) = -1.0_rp ; a % jA(iz) = ii-1
        iz = iz + 1 ; a % vA(1,1,iz) =  2.0_rp ; a % jA(iz) = ii
        iz = iz + 1 ; a % vA(1,1,iz) = -1.0_rp ; a % jA(iz) = ii+1
        a % iA(ii+1) = a % iA(ii) + 3
     end do

     ii = a % nrows
     a % iA(ii+1) = a % iA(ii) + 2
     iz = iz + 1 ; a % vA(1,1,iz) = -1.0_rp ; a % jA(iz) = ii-1
     iz = iz + 1 ; a % vA(1,1,iz) =  1.0_rp ; a % jA(iz) = ii

  end if
  !
  ! Deflated
  !
  if( a % nrows > 0 ) allocate(lgrou(a % nrows))
  ngrou = 2
  nzgro = 4
  allocate(ia(ngrou+1))
  allocate(ja(nzgro))
  ia    = (/1,3,5/)
  ja    = (/1,2,1,2/)
  if ( my_rank == 1 ) then
     lgrou = (/0,1,1,1,1,2 /)
  else if ( my_rank == 2 ) then
     lgrou = (/0,2,2,2,2,2 /)
  end if
  !
  ! Linelet
  !
  !nline    = 2
  !allocate(lline(nline+1))
  !lline(1) = 1
  !lline(2) = 4
  !lline(3) = 7
  !allocate(lrenup(lline(nline+1)))  
  !lrenup   = (/1,2,3,11,10,9/)
  nline = 0
  if ( my_rank == 1 .or.my_rank == 2 ) then
     nline    = 1
     allocate(lline(nline+1))
     lline(1) =  1
     lline(2) =  7
     allocate(lrenup(lline(nline+1)))  
     lrenup   = (/1,2,3,4,5,6/)
  end if
  !
  ! Solvers
  !
  do ii = 1,16

     if( associated(x) ) x    = 0.0_rp
     if( associated(b) ) b    = 0.1_rp
     if( associated(b) ) b(6) = 0.05_rp
     if( my_rank > 0   ) b(1) = 0.0_rp

     if( my_rank == 0 ) print*,''
     if(      ii == 1 ) then
        if( my_rank == 0 ) print*,'JACOBI'
        if( my_rank == 0 ) print*,'------'
        allocate(jacobi :: sol)
     else if( ii == 2 ) then
        if( my_rank == 0 ) print*,'CG'
        if( my_rank == 0 ) print*,'--'
        allocate(cg :: sol)
     else if( ii == 3 ) then
        if( my_rank == 0 ) print*,'BICGSTAB'
        if( my_rank == 0 ) print*,'--------'
        allocate(bicgstab :: sol)
     else if( ii == 4 ) then
        if( my_rank == 0 ) print*,'GMRES, PREC=SOR'
        if( my_rank == 0 ) print*,'---------------'
        allocate(gmres :: sol)
     else if( ii == 5 ) then
        if( my_rank == 0 ) print*,'SOR'
        if( my_rank == 0 ) print*,'---'
        allocate(sor :: sol)
     else if( ii == 6 ) then
        if( my_rank == 0 ) print*,'SSOR'
        if( my_rank == 0 ) print*,'----'
        allocate(ssor :: sol)
     else if( ii == 7 ) then
        if( my_rank == 0 ) print*,'GMRES, PREC=SSOR'
        if( my_rank == 0 ) print*,'----------------'
        allocate(gmres :: sol)
     else if( ii == 8 ) then
        if( my_rank == 0 ) print*,'GMRES, PREC=DIAG (RIGHT)'
        if( my_rank == 0 ) print*,'------------------------'
        allocate(gmres :: sol)
     else if( ii == 9 ) then
        if( my_rank == 0 ) print*,'GMRES, PREC=DIAG (LEFT)'
        if( my_rank == 0 ) print*,'-----------------------'
        allocate(gmres :: sol)
     else if( ii == 10 ) then
        if( my_rank == 0 ) print*,'GMRES, PREC=SOR (RIGHT)'
        if( my_rank == 0 ) print*,'--------------------------------'
        allocate(gmres :: sol)
     else if( ii == 11 ) then
        if( my_rank == 0 ) print*,'GMRES, PREC=SSOR (RIGHT)'
        if( my_rank == 0 ) print*,'------------------------'
        allocate(gmres :: sol)
     else if( ii == 12 ) then
        if( my_rank == 0 ) print*,'DEFLATED CG, PREC=DIAG'
        if( my_rank == 0 ) print*,'----------------------'
        allocate(dcg :: sol)
     else if( ii == 13 ) then
        if( my_rank == 0 ) print*,'RICHARDSON, PREC=APPROX_INV (SYMMETRIC)'
        if( my_rank == 0 ) print*,'---------------------------------------'
        allocate(richardson :: sol)
     else if( ii == 14 ) then
        if( my_rank == 0 ) print*,'RICHARDSON, PREC=APPROX_INV (UNSYMMETRIC)'
        if( my_rank == 0 ) print*,'-----------------------------------------'
        allocate(richardson :: sol)
     else if( ii == 15 ) then
        if( my_rank == 0 ) print*,'RICHARDSON, PREC=LINELET'
        if( my_rank == 0 ) print*,'------------------------'
        allocate(richardson :: sol)
      else if( ii == 16 ) then
        if( my_rank == 0 ) print*,'MINRES, PREC=DIAG'
        if( my_rank == 0 ) print*,'-----------------'
        allocate(minres :: sol)
     end if
     call solver_init(sol)

     allocate(sol % comm)
     call sol % comm % init()
     sol % comm % RANK4          = int(my_rank,4)
     sol % comm % SIZE4          = int(my_size,4)
     sol % comm % PAR_COMM_WORLD = PAR_COMM_MY_CODE

     if ( my_rank == 1 ) then
        sol % comm % nneig         = 1
        sol % comm % bound_dim     = 1
        call sol % comm % alloca()
        sol % comm % neights(1)    = 2
        sol % comm % bound_perm(1) = 6
        sol % comm % bound_size(1) = 1
        sol % comm % bound_size(2) = 2
        sol % comm % npoi1         = 5
        sol % comm % npoi2         = 6
        sol % comm % npoi3         = 6
        sol % comm % npoin         = 6
     else if ( my_rank == 2 ) then
        sol % comm % nneig         = 1
        sol % comm % bound_dim     = 1
        call sol % comm % alloca()
        sol % comm % neights(1)    = 1
        sol % comm % bound_perm(1) = 6
        sol % comm % bound_size(1) = 1
        sol % comm % bound_size(2) = 2
        sol % comm % npoi1         = 5
        sol % comm % npoi2         = 6
        sol % comm % npoi3         = 5
        sol % comm % npoin         = 6
     end if
     
     call solver_dim (sol,a)

     sol % input % relax     = 0.5_rp
     sol % input % kfl_symm  = 1
     if( ii == 4 .or. ii == 10 ) then
        sol % input % kfl_preco       = SOL_SOLVER_SOR
     else if( ii == 7 .or. ii == 11 ) then
        sol % input % kfl_preco       = SOL_SOLVER_SSOR
     else if ( ii == 13 ) then
        sol % input % kfl_preco       = SOL_SOLVER_APPROX_INVERSE
        sol % input % levels_sparsity = 1 ! 6
        sol % input % relax           = 1.0_rp
      else if ( ii == 14 ) then
        sol % input % kfl_preco       = SOL_SOLVER_APPROX_INVERSE
        sol % input % levels_sparsity = 1 ! 6
        sol % input % relax           = 1.0_rp
        sol % input % kfl_symm        = 0
     else if ( ii == 15 ) then
        sol % input % kfl_preco       = SOL_SOLVER_LINELET
        sol % input % relax           = 0.5_rp        
    else
        sol % input % kfl_preco = SOL_SOLVER_DIAGONAL
     end if
     
     if( ii == 8 .or. ii == 10 .or. ii == 11 ) then
        sol % input % kfl_leftr = SOL_RIGHT_PRECONDITIONING
     else
        sol % input % kfl_leftr = SOL_LEFT_PRECONDITIONING
     end if

     sol % input % nkryd     = 4
     sol % input % solco     = 1.0e-8_rp
     sol % input % miter     = 1000
     sol % input % kfl_exres = 1
     sol % input % kfl_cvgso = 1
     sol % input % lun_cvgso = 90+ii

     select type ( sol )
     class is ( dcg ) ; call sol % set(ngrou,lgrou,ia,ja)
     end select
     if( my_rank == 0 ) print*,'allocate preconditioner'
     call preconditioner_alloca(sol)
     if( ii == 15 ) then
        select type ( v => sol % preco(1) % p )
        class is ( linelet ) ; call v % set(nline,lline,lrenup)
        end select
     end if
     
     if( my_rank == 0 ) print*,'solver setup'     
     call solver_setup (sol,a)
     call sol % parallel_exchange(b)
     !call solver_preprocess(sol,b,x,a)
     
     if( my_rank == 0 ) print*,'set preconditioner'
     call preconditioner_setup(sol,a)
     

     if( my_rank == 0 ) print*,'solve'
     call sol % solve (b,x,a)

     if( my_rank == 0 ) then
        print*,'resip_init = ',sol % output % resip_init
        print*,'resid_init = ',sol % output % resid_init
        print*,'resip      = ',sol % output % resip_final
        print*,'resid      = ',sol % output % resid_final
        print*,'iters      = ',sol % output % iters
        print*,'||b||      = ',sol % output % bnorm
        print*,'||L^-1b||  = ',sol % output % bnorp
     end if
     !if( my_rank /= 0 ) print*,'x(inter)  = ',x(6)

     if( sol % output % resip_final > sol % input % solco ) stop 1

     if( my_rank == 0 ) print*,'deallocate'
     call sol % deallo()
     deallocate(sol)

  end do

  call par_finali(1_ip)

end program unitt_parallel_solver


