#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import sys
import os
import json

def openJson(file):
    try:
        with open(file) as json_data:
            f = json.load(json_data)
    except IOError:
        print("Could not open the JSON config file: "+file)
        raise
    except ValueError:
        print("File format Error in the JSON config file: "+file)
        raise
    except Exception:
        print("Error while opening the JSON file: "+file)
        raise
    else:
        return f

json_file = sys.argv[1]
directory = json_file.rsplit("/",1)[0] #Remove .json from name
try:
    f = openJson(json_file)
except:
    exit(1)
if "bad" in json_file:
    print("Ignoring "+json_file)
    exit(0)
if "enabled" in f:
    if not f["enabled"]:
        print("Ignoring "+json_file)
        exit(0)
fields = ["authors", "name", "comparisons", "description", "executions", "postprocess"]
for field in fields:
    try:
        f[field]
    except:
        print("Missing field: "+field)
        exit(2)
if not f["name"] in directory:
    print("Incorrect name test: "+f["name"])
    exit(3)
if not os.path.isdir(directory):
    print("Directory does not exist: "+directory)
    exit(4)
dat = f["name"] + ".dat"
#if not os.path.isfile(dat):
#    print("Dat file does not exist: "+dat)
#    exit(5)
print(json_file+" format OK!")

