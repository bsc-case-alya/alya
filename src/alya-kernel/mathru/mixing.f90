!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine mixing(itask,wemix,slmix,whmix,value)
  !-----------------------------------------------------------------------
  !****f* mathru/mixing
  ! NAME
  !   mixing
  ! DESCRIPTION
  !   Computes a mixing function
  ! OUTPUT 
  !   WEIGH
  ! USES
  ! USED BY
  !    tur_tworod
  !    tur_updedd
  !***
  !-----------------------------------------------------------------------
  use      def_kintyp 
  implicit none
  integer(ip), intent(in)  :: itask
  real(rp),    intent(in)  :: slmix,value,whmix
  real(rp),    intent(out) :: wemix

  select case(itask)

  case(1)
     !
     ! From 0 to 1. Equal to 0.5 at 1.
     !
     wemix  = 0.5_rp*(tanh(slmix*(value-whmix))+1.0_rp)

  end select

end subroutine mixing
