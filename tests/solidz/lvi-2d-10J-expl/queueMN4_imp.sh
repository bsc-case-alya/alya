#!/bin/bash
#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



#
#  Submit jobs in MN-IV
#     sbatch < job.sh
#
#SBATCH --job-name=lvi-3d-expl
#SBATCH -D .
#SBATCH --error=%j.err
#SBATCH --output=%j.out
#SBATCH --qos=debug
#SBATCH --ntasks=10
#SBATCH --tasks-per-node=48
#SBATCH --time=00:05:00

#
ALYAPATH="/home/bsc21/bsc21946/alya/master/Executables/plepp/Alya.x"
#ALYAPATH="/home/bsc21/bsc21946/alya/solidz-rbo/Executables/plepp/Alya.x"
MODEL1=block
MODEL2=impactor
CPUSM1=5
CPUSM2=5
#
# Launches ALYA
#
mpirun -np $CPUSM1 $ALYAPATH $MODEL1 --name SOLID : -np $CPUSM2 $ALYAPATH $MODEL2 --name FLUID
