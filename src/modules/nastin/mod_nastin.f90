!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    mod_nastin.f90
!> @author  houzeaux
!> @date    2020-04-22
!> @brief   Module for Nastin
!> @details Main nastin module with some useful tools
!-----------------------------------------------------------------------

module mod_nastin

  use def_master
  use def_nastin
  implicit none
  private

  public :: nastin_main, nastin_solution_strategy

contains

  subroutine nastin_main(order)
    implicit none
    integer(ip), intent(in) :: order
    call Nastin(order)
  end subroutine nastin_main
  
  !-----------------------------------------------------------------------
  !> 
  !> @author  houzeaux
  !> @date    2020-04-22
  !> @brief   Numerical strategy
  !> @details Set some parameters determining the solution strategy
  !> 
  !-----------------------------------------------------------------------

  subroutine nastin_solution_strategy()

    if(      kfl_algor_nsi == 1 ) then
       !
       ! Monolithic
       !
       NSI_MONOLITHIC          = .true.
       NSI_SCHUR_COMPLEMENT    = .false.
       NSI_FRACTIONAL_STEP     = .false.
       NSI_SEMI_IMPLICIT       = .false.
       NSI_ASSEMBLY_CONVECTIVE = .true.
       NSI_ASSEMBLY_VISCOUS    = .true.
       
    else if( kfl_algor_nsi == 5 ) then
       !
       ! Schur complement
       !
       NSI_MONOLITHIC          = .false.
       NSI_SCHUR_COMPLEMENT    = .true.
       NSI_FRACTIONAL_STEP     = .false.
       NSI_SEMI_IMPLICIT       = .false.
       NSI_ASSEMBLY_CONVECTIVE = .true.
       NSI_ASSEMBLY_VISCOUS    = .true.
       
    else if( kfl_algor_nsi == 3 ) then
       !
       ! Semi implicit 
       !
       NSI_MONOLITHIC          = .false.
       NSI_SCHUR_COMPLEMENT    = .false.
       NSI_FRACTIONAL_STEP     = .false.
       NSI_SEMI_IMPLICIT       = .true.
       NSI_ASSEMBLY_CONVECTIVE = .false.
       NSI_ASSEMBLY_VISCOUS    = .true.
       
    else if( kfl_algor_nsi == 2 ) then
       !
       ! Fractional step
       !
       NSI_MONOLITHIC          = .false.
       NSI_SCHUR_COMPLEMENT    = .false.
       NSI_FRACTIONAL_STEP     = .true.
       NSI_SEMI_IMPLICIT       = .false.
       NSI_ASSEMBLY_CONVECTIVE = .false.
       NSI_ASSEMBLY_VISCOUS    = .false.
              
    end if

  end subroutine nastin_solution_strategy

end module mod_nastin
!> @}
