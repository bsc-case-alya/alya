!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_memory
  !
  ! Order strings
  !
  use def_kintyp_basic, only : ip,rp,r1p,r2p,r3p,i1p,i2p,i3p,lg
  use mod_memory
  use mod_matrix

  implicit none

  integer(ip), pointer :: ip_1(:)
  integer(ip), pointer :: ip_2(:,:)
  integer(ip), pointer :: ip_3(:,:,:)

  real(rp),    pointer :: rp_1(:)
  real(rp),    pointer :: rp_2(:,:)
  real(rp),    pointer :: rp_3(:,:,:)

  type(r1p),   pointer :: aux_r1p(:)
  type(r2p),   pointer :: aux_r2p(:)
  type(r3p),   pointer :: aux_r3p(:)

  type(i1p),   pointer :: aux_i1p(:)
  type(i2p),   pointer :: aux_i2p(:)
  type(i3p),   pointer :: aux_i3p(:)

  integer(ip), pointer :: lg_1(:)
  integer(ip), pointer :: lg_2(:,:)
  integer(ip), pointer :: lg_3(:,:,:)
  
  integer(ip)          :: ii
  integer(8)           :: memor(2)
  
  nullify(ip_1,rp_1,lg_1)
  nullify(ip_2,rp_2,lg_2)
  nullify(ip_3,rp_3,lg_3)
  nullify(aux_r1p)
  nullify(aux_r2p)
  nullify(aux_r3p)
  nullify(aux_i1p)
  nullify(aux_i2p)
  nullify(aux_i3p)
  memor = 0_8
  !
  ! integer(:)
  !
  call memory_resize(memor,'ip_1','unit_memory',ip_1,4_ip)
  call memory_deallo(memor,'ip_1','unit_memory',ip_1)

  call memory_alloca(memor,'ip_1','unit_memory',ip_1,4_ip,lboun=0_ip)
  ip_1 = (/ 0,1,2,3 /)
  call memory_resize(memor,'ip_1','unit_memory',ip_1,4_ip)
  call memory_resize(memor,'ip_1','unit_memory',ip_1,4_ip)
  call memory_resize(memor,'ip_1','unit_memory',ip_1,3_ip)
  print*,'ip_1=',ip_1
  if( size(ip_1) /= 3 ) stop 1
  if( ip_1(1) /= 1 ) stop 1
  if( ip_1(2) /= 2 ) stop 1
  call memory_deallo(memor,'ip_1','unit_memory',ip_1)
  if( memor(1) /= 0_8 ) then
     print*,'memor int(:)=',memor(1)
     stop 1
  end if
  !
  ! integer(:,:)
  !
  call memory_resize(memor,'ip_2','unit_memory',ip_2,4_ip,3_ip)
  call memory_deallo(memor,'ip_2','unit_memory',ip_2)
  call memory_alloca(memor,'ip_2','unit_memory',ip_2,4_ip,2_ip,lboun1=0_ip,lboun2=1_ip)
  ip_2(:,1) = (/ 0,1,2,3 /)
  ip_2(:,2) = (/ 0,4,5,6 /)
  call memory_resize(memor,'ip_2','unit_memory',ip_2,4_ip,3_ip)
  call memory_resize(memor,'ip_2','unit_memory',ip_2,4_ip,3_ip)
  call memory_resize(memor,'ip_2','unit_memory',ip_2,3_ip,2_ip)
  print*,'ip_2=',ip_2
  if( size(ip_2,1) /=3 .and. size(ip_2,2) /=2 ) stop 1
  if( ip_2(1,1) /= 1 ) stop 1
  if( ip_2(2,1) /= 2 ) stop 1
  if( ip_2(1,2) /= 4 ) stop 1
  if( ip_2(2,2) /= 5 ) stop 1
  call memory_deallo(memor,'ip_2','unit_memory',ip_2)
  if( memor(1) /= 0_8 ) then
     print*,'memor int(:,:)=',memor(1)
     stop 1
  end if
  !
  ! integer(:,:,:)
  !
  call memory_resize(memor,'ip_3','unit_memory',ip_3,4_ip,3_ip,2_ip)
  call memory_deallo(memor,'ip_3','unit_memory',ip_3)
  call memory_alloca(memor,'ip_3','unit_memory',ip_3,3_ip,2_ip,2_ip)
  ip_3(:,1,1) = (/  1, 2, 3 /)
  ip_3(:,2,1) = (/  4, 5, 6 /)
  ip_3(:,1,2) = (/  7, 8, 9 /)
  ip_3(:,2,2) = (/ 10,11,12 /)
  call memory_resize(memor,'ip_3','unit_memory',ip_3,4_ip,3_ip,5_ip)
  call memory_resize(memor,'ip_3','unit_memory',ip_3,4_ip,3_ip,5_ip)
  call memory_resize(memor,'ip_3','unit_memory',ip_3,2_ip,2_ip,2_ip)
  print*,'ip_3=',ip_3
  if( size(ip_3,1) /=2 .and. size(ip_3,2) /=2 .and. size(ip_3,3) /=2 ) stop 1
  if( ip_3(1,1,1) /=  1 ) stop 1
  if( ip_3(2,1,1) /=  2 ) stop 1
  if( ip_3(1,2,1) /=  4 ) stop 1
  if( ip_3(2,2,1) /=  5 ) stop 1
  if( ip_3(1,1,2) /=  7 ) stop 1
  if( ip_3(2,1,2) /=  8 ) stop 1
  if( ip_3(1,2,2) /= 10 ) stop 1
  if( ip_3(2,2,2) /= 11 ) stop 1
  call memory_deallo(memor,'ip_3','unit_memory',ip_3)
  if( memor(1) /= 0_8 ) then
     print*,'memor int(:,:,:)=',memor(1)
     stop 1
  end if
  !
  ! real
  !
  call memory_resize(memor,'rp_1','unit_memory',rp_1,4_ip)
  call memory_deallo(memor,'rp_1','unit_memory',rp_1)

  call memory_alloca(memor,'rp_1','unit_memory',rp_1,3_ip)
  rp_1 = (/ 1.0_rp,2.0_rp,3.0_rp /)
  call memory_resize(memor,'rp_1','unit_memory',rp_1,4_ip)
  call memory_resize(memor,'rp_1','unit_memory',rp_1,4_ip)
  call memory_resize(memor,'rp_1','unit_memory',rp_1,2_ip)
  print*,'rp_1=',rp_1
  if( size(rp_1) /=2 ) stop 1
  if( rp_1(1) /= 1.0_rp ) stop 1
  if( rp_1(2) /= 2.0_rp ) stop 1
  call memory_deallo(memor,'rp_1','unit_memory',rp_1)

  call memory_resize(memor,'rp_2','unit_memory',rp_2,4_ip,3_ip)
  call memory_deallo(memor,'rp_2','unit_memory',rp_2)
  call memory_alloca(memor,'rp_2','unit_memory',rp_2,3_ip,2_ip)
  rp_2(:,1) = (/ 1.0_rp,2.0_rp,3.0_rp /)
  rp_2(:,2) = (/ 4.0_rp,5.0_rp,6.0_rp /)
  call memory_resize(memor,'rp_2','unit_memory',rp_2,4_ip,3_ip)
  call memory_resize(memor,'rp_2','unit_memory',rp_2,4_ip,3_ip)
  call memory_resize(memor,'rp_2','unit_memory',rp_2,2_ip,2_ip)
  print*,'rp_2=',rp_2
  if( size(rp_2,1) /=2 .and. size(rp_2,2) /=2 ) stop 1
  if( rp_2(1,1) /= 1.0_rp ) stop 1
  if( rp_2(2,1) /= 2.0_rp ) stop 1
  if( rp_2(1,2) /= 4.0_rp ) stop 1
  if( rp_2(2,2) /= 5.0_rp ) stop 1
  call memory_deallo(memor,'rp_2','unit_memory',rp_2)

  call memory_resize(memor,'rp_3','unit_memory',rp_3,4_ip,3_ip,2_ip)
  call memory_deallo(memor,'rp_3','unit_memory',rp_3)
  call memory_alloca(memor,'rp_3','unit_memory',rp_3,3_ip,2_ip,2_ip)
  rp_3(:,1,1) = (/  1.0_rp, 2.0_rp, 3.0_rp /)
  rp_3(:,2,1) = (/  4.0_rp, 5.0_rp, 6.0_rp /)
  rp_3(:,1,2) = (/  7.0_rp, 8.0_rp, 9.0_rp /)
  rp_3(:,2,2) = (/ 10.0_rp,11.0_rp,12.0_rp /)
  call memory_resize(memor,'rp_3','unit_memory',rp_3,4_ip,3_ip,5_ip)
  call memory_resize(memor,'rp_3','unit_memory',rp_3,4_ip,3_ip,5_ip)
  call memory_resize(memor,'rp_3','unit_memory',rp_3,2_ip,2_ip,2_ip)
  print*,'rp_3=',rp_3
  if( size(rp_3,1) /=2 .and. size(rp_3,2) /=2 .and. size(rp_3,3) /=2 ) stop 1
  if( rp_3(1,1,1) /=  1.0_rp ) stop 1
  if( rp_3(2,1,1) /=  2.0_rp ) stop 1
  if( rp_3(1,2,1) /=  4.0_rp ) stop 1
  if( rp_3(2,2,1) /=  5.0_rp ) stop 1
  if( rp_3(1,1,2) /=  7.0_rp ) stop 1
  if( rp_3(2,1,2) /=  8.0_rp ) stop 1
  if( rp_3(1,2,2) /= 10.0_rp ) stop 1
  if( rp_3(2,2,2) /= 11.0_rp ) stop 1
  call memory_deallo(memor,'rp_3','unit_memory',rp_3)
  
  if( memor(1) /= 0_8 ) then
     print*,'memor real=',memor(1)
     stop 1
  end if
  !
  ! r1p
  !
  memor = 0_8
  call memory_alloca(memor,'AUX_R1P','unit_memory',aux_r1p,3_ip)
  do ii = 1,size(aux_r1p)
     call memory_alloca(memor,'AUX_R1P % A','unit_memory',aux_r1p(ii)%a,ii-1_ip)
  end do
  call memory_deallo(memor,'AUX_R1P','unit_memory',aux_r1p)

  print*,'aux_r1p=',memor
  if( memor(1) /= 0_8 ) then
     print*,'memor r1p=',memor(1)
     stop 1
  end if
  !
  ! r2p
  !
  memor = 0_8
  call memory_alloca(memor,'AUX_R2P','unit_memory',aux_r2p,3_ip)
  do ii = 1,size(aux_r2p)
     call memory_alloca(memor,'AUX_R2P % A','unit_memory',aux_r2p(ii)%a,ii-1_ip,ii)
  end do
  call memory_deallo(memor,'AUX_R2P','unit_memory',aux_r2p)

  print*,'aux_r2p=',memor
  if( memor(1) /= 0_8 ) then
     print*,'memor r2p=',memor(1)
     stop 1
  end if
  !
  ! r3p
  !
  memor = 0_8
  call memory_alloca(memor,'AUX_R3P','unit_memory',aux_r3p,3_ip)
  do ii = 1,size(aux_r3p)
     call memory_alloca(memor,'AUX_R3P % A','unit_memory',aux_r3p(ii)%a,ii-1_ip,ii,ii)
  end do
  call memory_deallo(memor,'AUX_R3P','unit_memory',aux_r3p)

  print*,'aux_r3p=',memor
  if( memor(1) /= 0_8 ) then
     print*,'memor r3p=',memor(1)
     stop 1
  end if
  !
  ! i1p
  !
  memor = 0_8
  call memory_alloca(memor,'AUX_I1P','unit_memory',aux_i1p,3_ip)
  do ii = 1,size(aux_i1p)
     call memory_alloca(memor,'AUX_I1P % L','unit_memory',aux_i1p(ii)%l,ii-1_ip)
  end do
  call memory_deallo(memor,'AUX_I1P','unit_memory',aux_i1p)

  print*,'aux_i1p=',memor
  if( memor(1) /= 0_8 ) then
     print*,'memor i1p=',memor(1)
     stop 1
  end if
  !
  ! i2p
  !
  memor = 0_8
  call memory_alloca(memor,'AUX_I2P','unit_memory',aux_i2p,3_ip)
  do ii = 1,size(aux_i2p)
     call memory_alloca(memor,'AUX_I2P % L','unit_memory',aux_i2p(ii)%l,ii-1_ip,ii)
  end do
  call memory_deallo(memor,'AUX_I2P','unit_memory',aux_i2p)

  print*,'aux_i2p=',memor
  if( memor(1) /= 0_8 ) then
     print*,'memor i2p=',memor(1)
     stop 1
  end if
  !
  ! i3p
  !
  memor = 0_8
  call memory_alloca(memor,'AUX_I3P','unit_memory',aux_i3p,3_ip)
  do ii = 1,size(aux_i3p)
     call memory_alloca(memor,'AUX_I3P % L','unit_memory',aux_i3p(ii)%l,ii-1_ip,ii,ii)
  end do
  call memory_deallo(memor,'AUX_I3P','unit_memory',aux_i3p)

  print*,'aux_i3p=',memor
  if( memor(1) /= 0_8 ) then
     print*,'memor i3p=',memor(1)
     stop 1
  end if
  !
  ! lg
  !
  call memory_alloca(memor,'lg_1','unit_memory',lg_1,2_ip)
  call memory_deallo(memor,'lg_1','unit_memory',lg_1)
  call memory_alloca(memor,'lg_2','unit_memory',lg_2,2_ip,3_ip)
  call memory_deallo(memor,'lg_2','unit_memory',lg_2)
  call memory_alloca(memor,'lg_3','unit_memory',lg_3,2_ip,3_ip,4_ip)
  call memory_deallo(memor,'lg_3','unit_memory',lg_3)
  print*,'lg=',memor
  if( memor(1) /= 0_8 ) then
     print*,'memor lg=',memor(1)
     stop 1
  end if
 
end program unitt_memory
