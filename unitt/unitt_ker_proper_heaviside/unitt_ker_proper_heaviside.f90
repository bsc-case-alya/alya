!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_ker_proper_heaviside
  !
  ! Get last non-zero position
  !
  use def_kintyp_basic,       only : ip, rp

#ifdef VECTOR_SIZE
  use mod_ker_proper_generic, only : ker_proper_heaviside

  implicit none

  real(rp)    :: x(VECTOR_SIZE),x_0,H(VECTOR_SIZE)
  integer(ip) :: VECTOR_DIM

  VECTOR_DIM =  size(x)
  if( VECTOR_DIM >= 3 ) then

     x    =  0.0_rp

     x_0  =  0.5_rp
     x(1) = -1.0_rp
     x(2) =  0.0_rp
     x(3) =  1.0_rp

     call ker_proper_heaviside(VECTOR_DIM,x,x_0,H) 

     if( H(1) /= 0.0_rp ) stop 1
     if( H(2) /= 0.0_rp ) stop 1
     if( H(3) /= 1.0_rp ) stop 1

  end if

#endif

end program unitt_ker_proper_heaviside
