!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine elmmas(pnode,pgaus,gpvol,gpsha,elmat)
  !-----------------------------------------------------------------------
  !****f* domain/elmmas
  ! NAME
  !    elmmas
  ! DESCRIPTION
  !    This routines calculates the element mass matrix (symmetric) 
  ! OUTPUT
  !    ELMAT : Consistent mass matrix
  ! USED BY
  !    Domain
  !*** 
  !-----------------------------------------------------------------------
  use def_kintyp, only      : rp,ip
  implicit none
  integer(ip), intent(in)  :: pnode,pgaus
  real(rp),    intent(in)  :: gpvol(pgaus),gpsha(pnode,pgaus)
  real(rp),    intent(out) :: elmat(pnode,pnode)
  integer(ip)              :: igaus,inode,jnode
  real(rp)                 :: fact1

  do inode=1,pnode
     do jnode=1,pnode
        elmat(inode,jnode)=0.0_rp
     end do
  end do

  do igaus=1,pgaus
     do inode=1,pnode
        fact1=gpvol(igaus)*gpsha(inode,igaus)
        do jnode=1,pnode
           elmat(inode,jnode)=elmat(inode,jnode)&
                +fact1*gpsha(jnode,igaus)
        end do     
     end do
  end do

end subroutine elmmas
