!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_recarr(itask,ndimi,ndimr,ivari,rvari)
  !------------------------------------------------------------------------
  !****f* Parall/par_recarr
  ! NAME
  !    par_recarr
  ! DESCRIPTION
  ! OUTPUT
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_master
  implicit none
  integer(ip), intent(in) :: itask,ndimi,ndimr
  integer(ip), target     :: ivari(ndimi)
  real(rp),    target     :: rvari(ndimr)

  npari = 0
  nparr = 0
  nparc = 0

  select case ( itask )

  case ( 1_ip ) 

     npari =  ndimi
     parin => ivari 
     call par_receiv()

  case ( 2_ip )

     nparr =  ndimr
     parre => rvari
     call par_receiv()

  end select
  
end subroutine par_recarr
 
