#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import sys
import os
import math
import numpy as np
from scipy.special import erf
import cantera as ct

def strainFlow(xglob,yglob,a,x0=0.0,y0=0.0,width=np.inf):
	x = xglob - x0
	y = yglob - y0
	if abs(x) < width/2.0:
		U =  0.0
		V = -1.0 * a * y
	else:
		U = 0.0
		V = 0.0

	return(U,V)

print(sys.argv)

meshName	= sys.argv[1]
mech		= sys.argv[2]
fuel		= sys.argv[3]
oxidizer	= sys.argv[4]
Tf			= float(sys.argv[5])
Tox			= float(sys.argv[6])
P			= float(sys.argv[7])

fuelBoundaries = sys.argv[8].split(',')
oxiBoundaries = sys.argv[9].split(',')

strain	  =  float(sys.argv[10])
Nsec		=  int(sys.argv[11])

#
# Shift center of strain flow and Z profile
#
if len(sys.argv)>12:
	x0 = float(sys.argv[12])
else:
	x0 = 0.0 

if len(sys.argv)>13:
	y0 = float(sys.argv[13])
else:
	y0 = 0.0

coordfile	   = '{}.coord'.format(meshName)

os.system('rm -rf Fields')
os.system('mkdir Fields')

tempfile		= 'Fields/TEMPE.alya'
enthfile		= 'Fields/ENTHA.alya'
velofile		= 'Fields/VELOC.alya'
concePatt		= 'Fields/CO{:03d}.alya'



gasFuel						  = ct.Solution(mech)
gasFuel.TPY					  = (Tf,P,fuel)

gasOxidizer					  = ct.Solution(mech)
gasOxidizer.TPY				  = (Tox,P,oxidizer)
diff						  = gasOxidizer.thermal_conductivity/(gasOxidizer.density_mass * gasOxidizer.cp_mass)
lengthScale					  = ( 2.0 * diff / strain )**0.5
print('Length scale: {}'.format(lengthScale))

gasProdu					  = ct.Solution(mech)
gasProdu.TP					  = (Tox,P)
gasProdu.set_equivalence_ratio(1.0,fuel,oxidizer)

#
# Find stoichiometric mixture fraction
#
fuelName = fuel.split(':')[0]
Zst = gasProdu.Y[gasProdu.species_index(fuelName)]
print('Zst: {}'.format(Zst))

#
# Reset gas temperature before equilibrate: 
#
H = gasOxidizer.enthalpy_mass + (gasFuel.enthalpy_mass-gasOxidizer.enthalpy_mass)*Zst
gasProdu.HP	= (H,P)


gasProdu.equilibrate('HP')

#
# Reset to have more pyrene as initial condition
#
Y = gasProdu.Y
indexPyrene = gasProdu.species_index('A4')
Y[indexPyrene] = 1e-7
gasProdu.HPY= H,P,Y

gas							  = ct.Solution(mech)
gas.TP						  = (Tf,P)

print(gasFuel.report())
print(gasOxidizer.report())
print(gasProdu.report())


ymin =  np.inf
ymax = -np.inf
fCoord		   =open(coordfile,'r')
for line in fCoord:
	data=line.split()

	pid = int(data[0])
	dims = len(data)-1
	x   = float(data[1])
	y   = float(data[2])
	ymax = max(ymax,y)
	ymin = min(ymin,y)
fCoord.close()

width = 1.5 * (ymax-ymin)
print('Total thickness of domain: {}'.format(ymax-ymin))
U,V = strainFlow(0.0,ymax,strain)
print('Downward velocity: {}'.format(V))
U,V = strainFlow(0.0,ymin,strain)
print('Upward velocity:   {}'.format(V))


fCoord		   =open(coordfile,'r')
fTemp		   =open(tempfile,'w')
fEnth		   =open(enthfile,'w')
fVelo		   =open(velofile,'w')
fCon		   =[]

nclas_ssm = gasProdu.n_species + Nsec
print('Creating {} fields.'.format(nclas_ssm))

for ii in range(nclas_ssm):
	fCon.append( open(concePatt.format(ii+1),'w') )

Y = np.zeros([gasOxidizer.n_species]) 

print('---| Start writing initial condition')

for line in fCoord:
	data=line.split()

	pid = int(data[0])
	dims = len(data)-1
	x   = float(data[1]) - x0
	y   = float(data[2]) - y0


	U,V = strainFlow(x,y,strain,width=width)

	zeta = y / lengthScale
	Z = 0.5 * (1.0 - erf( zeta ))
	zLeft	= Zst*0.6
	zRight	= Zst*1.4
	if abs(x) < width/2.0:
		H = gasOxidizer.enthalpy_mass + (gasFuel.enthalpy_mass-gasOxidizer.enthalpy_mass)*Z
		if Z <= zLeft:
			wSt = Z / zLeft
			wOx = 1.0 - wSt
			Y = gasOxidizer.Y * wOx + gasProdu.Y * wSt
		elif Z >= zRight:
			wSt = (1.0-Z) / (1.0-zRight)
			wF	 = 1.0 - wSt
			Y = gasFuel.Y * wF + gasProdu.Y * wSt
		else:
			Y = gasProdu.Y
	else:
		H = gasOxidizer.enthalpy_mass
		Y = gasOxidizer.Y

	gas.HPY	= (H,P,Y)
	T = gas.T

	#
	# Write IC  
	#
	fEnth.write('{} {}\n'.format(pid,H))
	fTemp.write('{} {}\n'.format(pid,T))
	fVelo.write('{} {} {}\n'.format(pid,U,V))
	for ii in range(gasOxidizer.n_species):
		fCon[ii].write('{} {}\n'.format(pid,Y[ii]))
		

fCoord.close()
fTemp.close()
for ii in range(nclas_ssm):
	fCon[ii].close()
print('---| End writing initial condition')


#
# Write fileds file
#
with open('fields.dat','w') as f:

	f.write('  FIELD={}\n'.format(1))
	f.write('	INCLUDE ./mesh/Fields/VELOC.alya\n')
	f.write('  END_FIELD\n\n')

	f.write('  FIELD={}\n'.format(2))
	f.write('	INCLUDE ./mesh/Fields/ENTHA.alya\n')
	f.write('  END_FIELD\n\n')

	f.write('  FIELD={}\n'.format(3))
	f.write('	INCLUDE ./mesh/Fields/TEMPE.alya\n')
	f.write('  END_FIELD\n\n')
	for ii in range(nclas_ssm):
		f.write('  FIELD={}\n'.format(ii+4))
		f.write('	INCLUDE ./mesh/Fields/CO{:03d}.alya\n'.format(ii+1))
		f.write('  END_FIELD\n\n')

#
# Write filed size file
#
with open('field_size.dat','w') as f:
	f.write('  FIELDS={}\n'.format( 3 + nclas_ssm))
	f.write('	FIELD: 1, DIMEN= 2, NODES\n')
	f.write('	FIELD: 2, DIMEN= 1, NODES\n')
	f.write('	FIELD: 3, DIMEN= 1, NODES\n')
	for ii in range(nclas_ssm):
		f.write('	FIELD: {}, DIMEN= 1, NODES\n'.format(ii+4))
	f.write('  END_FIELDS\n')
	
YSFuel = np.zeros([nclas_ssm])
YSOxid = np.zeros([nclas_ssm])

for ii in range(gasProdu.n_species):
	YSFuel[ii] = gasFuel.Y[ii]
	
for ii in range(gasProdu.n_species):
	YSOxid[ii] = gasOxidizer.Y[ii]

#
# Write species boundary conditions
#
with open('speciesBoundary.dat','w') as f:
	for ii in range(nclas_ssm):
		f.write('  CODES, NODES, CLASS = {}\n'.format(ii+1))
		for bc in fuelBoundaries:
			f.write('{:>10}  1  {}\n'.format(bc, YSFuel[ii]))
		for bc in oxiBoundaries:
			f.write('{:>10}  1  {}\n'.format(bc, YSOxid[ii]))
		f.write('  END_CODES\n\n')



