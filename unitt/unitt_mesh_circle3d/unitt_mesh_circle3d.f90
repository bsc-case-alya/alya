!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_mesh_extract_copy
  !
  ! Test copy, extract and merge operators
  !
  use def_elmtyp
  use def_kintyp_basic
  use def_kintyp_mesh_basic
  use mod_elmgeo
  implicit none

  type(mesh_type_basic) :: mesh
  real(rp)              :: center(3)
  real(rp)              :: normal(3)
  real(rp)              :: radius

  call elmgeo_element_type_initialization()

  radius = 2.0_rp
  center = (/ 0.5_rp,0.5_rp,0.5_rp /)
  normal = (/ 1.0_rp,1.0_rp,1.0_rp /)
  
  call mesh % init         ('MY_MESH')
  call mesh % mesh_3Dcircle(center,normal,radius,10_ip,20_ip)  
  call mesh % output       (filename='circle3d')
  call mesh % deallo       ()
  call mesh % init         ('MY_MESH')
  call mesh % mesh_3Dring(center,normal,radius,5_ip,4_ip)
  call mesh % output       (filename='ring3d')
  call mesh % deallo       ()
    
end program unitt_mesh_extract_copy
