#include "cantera/thermo.h"
#include <iostream>
#include "cantera/IdealGasMix.h"
#include "cantera/zerodim.h"
#include "vector"
#include <sstream>
#include "cantera/transport.h"

//static CommDom* ptr_class = NULL;

// Declare some functions and pointes
//std::vector<std::unique_ptr<Cantera::IdealGasMix>> gasint;
//std::vector<std::unique_ptr<Cantera::IdealGasConstPressureReactor>> React;
//std::vector<std::unique_ptr<Cantera::ReactorNet>> net;

static Cantera::IdealGasMix* ptr_gas = NULL;

static Cantera::ReactorNet* ptr_net = NULL;

static Cantera::Transport* ptr_tr = NULL;
void cantera_sum (double* cc, double* cp_mass_frac, int cp_nsp, double* W_k,double* cp_low, double* cp_high); 

double get_dt (double* net, int nsp, double* t);

extern "C"
{

  const char*
  string_f2c(const char* licName, int nameLen)
  {
    char* name;
    name = (char*)malloc(sizeof(char)*(nameLen));
    for(int i = 0; i<nameLen; i++) name[i] = licName[i];
    name[nameLen] = '\0';
    return name ;
  }

    // Gets the Nasa coeffcients of each species (Need to be run only once)
    void cantera_coeff_(const char* fmech, int* n_fmech, double* cc, double* W_k)
    {   int type, nsp;
        double temp[15]; 
        double minTemp, maxTemp, refPressure;

        Cantera::IdealGasMix gas(string_f2c(fmech,n_fmech[0]));
        nsp = gas.nSpecies();
        gas.getMolecularWeights(W_k);
        for(int hh=0; hh<nsp; hh++)  
            W_k[hh] = W_k[hh] / 1000.0; // Convert into SI units (kg/mol)

        Cantera::MultiSpeciesThermo& sp = gas.speciesThermo();
        for(int jj=0,kk=0; jj<nsp; jj++){ 
            sp.reportParams(jj, type, temp, minTemp, maxTemp, refPressure);
            for(int ii=0; ii<15; ii++,kk++){
                cc[kk] = temp[ii];
            }
        }
    }

    void cantera_divide_(double *num, double *den, double *res)
    {
       *res = *num/ *den;
    }

    void cantera_log_(double *in)
    {
       *in = log10(*in);
    }
    void cantera_alya_cp_(int* nsp, double* cc, double  *mass_frac, double* W_k , double *low_coeffs, double *high_coeffs)
    {
        cantera_sum (cc, mass_frac, nsp[0], W_k ,low_coeffs, high_coeffs);
    }

    void cantera_elemh_(double *mass_frac, double *H)
    {   int ha;
	ptr_gas[0].setMassFractions(&mass_frac[0]);
        ha = ptr_gas[0].elementIndex("H");
        *H = ptr_gas[0].elementalMassFraction(ha);
    }
    void cantera_elemc_(double *mass_frac, double *C)
    {   int ca;
	ptr_gas[0].setMassFractions(&mass_frac[0]);
        ca = ptr_gas[0].elementIndex("C");
        *C = ptr_gas[0].elementalMassFraction(ca);

    }
    void cantera_elemo_(double *mass_frac, double *O)
    {   int oa;
	ptr_gas[0].setMassFractions(&mass_frac[0]);
        oa = ptr_gas[0].elementIndex("O");
        *O = ptr_gas[0].elementalMassFraction(oa);
    }
    void cantera_reduced_(char* non_key)
    {   
        int size_red;
        std::string str1 ("END_REDUCTION"); 
        int reac_multi[ptr_gas[0].nReactions()];
        for (int j=0; j<ptr_gas[0].nReactions(); j++) reac_multi[j] = 1;
        // Delimit "non_key" string to get an array of non key species
        std::string s;
        std::stringstream ss;
        ss << non_key;
        ss >> s;
        std::vector<std::string>   result;
        std::stringstream  data(s);
        std::string line;
        while(std::getline(data,line,','))
        {
            result.push_back(line);
        }
        size_red = result.size();
        // Get index of reactions including the non key species
        int spec_index[size_red];
        if (str1.compare(result[0]) != 0) {
            for (int j=0; j<size_red; j++) spec_index[j] = ptr_gas[0].speciesIndex(result[j]);
            for(int i=0; i<size_red; i++){
                for(int j=0; j < ptr_gas[0].nReactions(); j++){
                    if ( (ptr_gas[0].reactantStoichCoeff(spec_index[i], j) > 0.001 || ptr_gas[0].productStoichCoeff(spec_index[i], j) > 0.001) )
                        reac_multi[j] = 0;
                }
            }
            for(int j=0; j < ptr_gas[0].nReactions(); j++) ptr_gas[0].setMultiplier(j, reac_multi[j]);
        }
        result.clear();
    }

    void cantera_prog_(char* prog, int *prog_index)
    {   
        int size_red;
        // Delimit "non_key" string to get an array of non key species
        std::string s;
        std::stringstream ss;
        ss << prog;
        ss >> s;
        std::vector<std::string>   result;
        std::stringstream  data(s);
        std::string line;
        while(std::getline(data,line,','))
        {
            result.push_back(line);
        }
        size_red = result.size();
        for (int j=0; j<size_red; j++) prog_index[j]  = ptr_gas[0].speciesIndex(result[j]);
        result.clear();
    }

    // Integration Source terms
    void cantera_initialization_(int *n_size, const char* fmech, int* n_fmech)
    {
        ptr_gas = new Cantera::IdealGasMix(string_f2c(fmech,n_fmech[0]));
	ptr_tr = Cantera::newTransportMgr("Mix", &ptr_gas[0], 0);
    }

    void cantera_trim_(const char* fmech, int* n_fmech, int* key_index)
    {
        int size_key;   
        std::string s;
        std::stringstream ss;
        ss << string_f2c( fmech,n_fmech[0] );
        ss >> s;
        std::vector<std::string>   result;
        std::stringstream  data(s);
        std::string line;
        while(std::getline(data,line,','))
        {
            result.push_back(line);
        }
        size_key = result.size();
        for(int j=0; j < size_key; j++) key_index[j] = ptr_gas[0].speciesIndex(result[j]);
    }

    void cantera_dac_integrate_(double *temp, double *p,  double *mass_frac, int* reac_multi, double* t)
    {
        for(int j=0; j < ptr_gas[0].nReactions(); j++) ptr_gas[0].setMultiplier(j, reac_multi[j]);
        ptr_gas[0].setState_TPY(*temp, *p, &mass_frac[0]);
        Cantera::IdealGasConstPressureReactor r;
        r.insert(ptr_gas[0]);
        Cantera::ReactorNet sim;
        sim.addReactor(r);
        // Integrate Source Terms
        sim.advance(*t);
        *temp =  r.temperature();
        *p =  r.pressure();
        ptr_gas[0].getMassFractions(&mass_frac[0]);
    }

    void cantera_integrate_(double *temp, double *p,  double *mass_frac, double *t)
    { 
        ptr_gas[0].setState_TPY(*temp, *p, &mass_frac[0]);
        Cantera::IdealGasConstPressureReactor r;
        r.insert(ptr_gas[0]);
        Cantera::ReactorNet sim;
        sim.addReactor(r);
        // Integrate Source Terms
        sim.advance(*t);
        *temp =  r.temperature();
        *p =  r.pressure();
        ptr_gas[0].getMassFractions(&mass_frac[0]);
    }

    void cantera_equi_integrate_(double *temp, double *p,  double *mass_frac, double *mass_frac_eq, double *t, int* all_spec_index)
    { 
        for(int j=0; j < ptr_gas[0].nSpecies(); j++){
            if(all_spec_index[j] == 0) mass_frac[j] = mass_frac_eq[j];
        }
        ptr_gas[0].setState_TPY(*temp, *p, &mass_frac[0]);
        Cantera::IdealGasConstPressureReactor r;
        r.insert(ptr_gas[0]);
        Cantera::ReactorNet sim;
        sim.addReactor(r);
        // Integrate Source Terms
        sim.advance(*t);
        *temp =  r.temperature();
        *p =  r.pressure();
        ptr_gas[0].getMassFractions(&mass_frac[0]);
    }

    void cantera_equilibrate_(double *temp, double *p,  double *mass_frac,char* non_key, int* all_spec_index)
    {   
        int size_red;
        int reac_multi[ptr_gas[0].nReactions()]; 
        for (int j=0; j<ptr_gas[0].nReactions(); j++) reac_multi[j] = 1;
        // Delimit "non_key" string to get an array of non key species
        std::string s;
        std::stringstream ss;
        ss << non_key;
        ss >> s;
        std::vector<std::string>   result;
        std::stringstream  data(s);
        std::string line;
        while(std::getline(data,line,','))
        {
            result.push_back(line);
        }
        size_red = result.size();
        // Get index of reactions including the non key species
        int spec_index[size_red];
        for (int j=0; j<size_red; j++) spec_index[j] = ptr_gas[0].speciesIndex(result[j]);

        for (int j=0; j<ptr_gas[0].nSpecies(); j++) {
            for (int i=0; i<size_red; i++) {
                if (j == spec_index[i]) all_spec_index[j] = 0;
            }
        }
        ptr_gas[0].setState_TPY(*temp, *p, &mass_frac[0]);
        ptr_gas[0].equilibrate("HP");
        ptr_gas[0].getMassFractions(&mass_frac[0]); 
        result.clear();
    }

    void cantera_partial_molar_enthalpies_(int *nspec, double *temp, double *p, double *mass_frac, double *partial_h)
    {
        ptr_gas[0].setState_TPY(*temp, *p, &mass_frac[0]);
        ptr_gas[0].getPartialMolarEnthalpies(&partial_h[0]);
        for(int ii=0; ii < *nspec; ii++)
            partial_h[ii] = partial_h[ii] / 1000.0;  // Convert into SI units (J/mol)
    }

    void cantera_mix_diff_coeffs_(double *temp, double *p, double *mass_frac, double *diff_coeffs, double * den)
    {
        ptr_gas[0].setState_TPY(*temp, *p, &mass_frac[0]);
        *den = ptr_gas[0].density();
        ptr_tr[0].getMixDiffCoeffs(&diff_coeffs[0]);
    }

    void cantera_get_enthalpy_from_ty_(double *p, double *temp, double *mass_frac, double *enthalpy_spec_RT)
    {
        ptr_gas[0].setState_TPY(*temp, *p, &mass_frac[0]);
        ptr_gas[0].getEnthalpy_RT(&enthalpy_spec_RT[0]);
    }

    void cantera_get_temperature_from_hy_(double *p, double *enthalpy, double *mass_frac, double *temp)
    {
        ptr_gas[0].setMassFractions(&mass_frac[0]);
        ptr_gas[0].setState_HP(*enthalpy, *p);
        *temp = ptr_gas[0].temperature();
    }

    void cantera_equilibrium_from_hy_(double *p, double *enthalpy, double *mass_frac, double *temp_eq)
    {
        ptr_gas[0].setMassFractions(&mass_frac[0]);
        ptr_gas[0].setState_HP(*enthalpy, *p);
        ptr_gas[0].equilibrate("HP");
        ptr_gas[0].getMassFractions(&mass_frac[0]);
        *temp_eq = ptr_gas[0].temperature();
    }

    void cantera_odepim_ref_adaptive_(int *nspec, double *temp, double *p,  double *mass_frac, double* t)
    {
/*
Notes: Working conditions maybe expensive (Still in development) 
        ittr_limit = 20;
        tol      = 1e-9;
        dt       = 1e-6;
        while(erroh > 0.002){
        erroh = std::abs(yk1[4]-yk[4])/yk[4];
        dt = dt/3.0;
 
*/
        double  net[*nspec];
        double  yk1[*nspec];
        double  yk[*nspec];
        double  yk_ref[*nspec];
        double  yk_pred[*nspec];
        double  w_k[*nspec];
        double  con[*nspec];
        double  pro[*nspec];
        double  errorArr[*nspec];
        double  dt;
        double  entha,temp1,temp2;
        double  rho;
        double  err,erroh,erroTemp,tol,cuttm;
        int     ittr_limit,count,count2;
 
        ptr_gas[0].getMolecularWeights(w_k);
        ptr_gas[0].setState_TPY(*temp, *p, &mass_frac[0]);
        ptr_gas[0].getNetProductionRates(net);
        ptr_gas[0].getDestructionRates(con);
        ptr_gas[0].getCreationRates(pro);
        ptr_gas[0].getMassFractions(yk1);
        entha  = ptr_gas[0].enthalpy_mass();
        cuttm = 0;
        ittr_limit = 200;
        count2 = 0;
        tol      = 1e-6;
        erroh    = 3.0;
        //dt = get_dt(net,*nspec,t);
        //dt = std::min(1e-5,(1/net[4]));
        erroTemp = 2.0;
        dt = get_dt(net,*nspec,t);
        dt = std::min(1e-6,dt);
        //tol = dt/10;
        //dt = 5e-7;
        while(erroh > 0.002 || erroTemp > 0.2){
//        while(erroh > 0.00002){
            //if(count2 < 1){ 
            //  dt = dt/1.0;
            //}
            //else{
            //  dt = dt/3.0;
            //}
            dt = dt/3.0;
            //tol = dt/10;
            count2 = count2 + 1; 
            ptr_gas[0].setState_TPY(*temp, *p, &mass_frac[0]);
            ptr_gas[0].getNetProductionRates(net);
            ptr_gas[0].getDestructionRates(con);
            ptr_gas[0].getCreationRates(pro);
            ptr_gas[0].getMassFractions(yk1);
            //dt = get_dt(net,*nspec,t);
            //dt = std::min(5e-6,dt);
            cuttm = 0;
            count2 = count2 + 1;
            while( (*t-dt) > cuttm){
                for (int i=0; i< *nspec; i++) yk_ref[i] = yk1[i];
                ptr_gas[0].setMassFractions(&yk_ref[0]);
                ptr_gas[0].setState_HP(entha,*p);
                temp1 = ptr_gas[0].temperature(); 
                err = 10.0 * tol;
                count = 0;
                while (err > tol && ittr_limit > count){
                    for (int i=0; i< *nspec; i++) yk[i] = yk1[i];
                    ptr_gas[0].setMassFractions(&yk[0]);
                    ptr_gas[0].setState_HP(entha,*p);
                    ptr_gas[0].getDestructionRates(con);
                    ptr_gas[0].getCreationRates(pro);
                    ptr_gas[0].getNetProductionRates(net);
                    rho = ptr_gas[0].density(); 
                    for (int i=0; i< *nspec; i++){
                        if (yk[i] > 0.0) {
                            yk1[i] = ( yk_ref[i] + dt * (pro[i] * (w_k[i]/rho)) ) / ( 1 + ( dt * con[i] * (w_k[i]/rho) / yk[i] ) );
                        }
                        else{
                            yk1[i] = yk_ref[i] + (net[i] * w_k[i] * dt) / rho;
                        }
                        if  (yk[i] > 1e-9){
                            errorArr[i] = std::abs( ( log10( yk1[i] ) / log10( yk[i] ) ) - 1 );
                        }
                        else{
                            errorArr[i] = 0.0; 
                        }
                    }
                    err = *std::max_element(errorArr,errorArr + *nspec);
                    count = count + 1;
	        }
	        cuttm =  cuttm + dt;
            }

            ptr_gas[0].setMassFractions(&yk1[0]);
            ptr_gas[0].setState_HP(entha,*p);
            temp2 = ptr_gas[0].temperature(); 
            erroh = std::abs(yk1[4]-yk[4])/yk[4];
            erroTemp = std::abs(temp2-temp1)/temp1;
        }

        dt = *t - cuttm;
        dt = dt/10.0;
        for (int j=0; j< 10; j++){
            for (int i=0; i< *nspec; i++) yk_ref[i] = yk1[i];
            err = 10.0 * tol;
            count = 0;
            while (err > tol && ittr_limit > count){
                for (int i=0; i< *nspec; i++) yk[i] = yk1[i];
                ptr_gas[0].setMassFractions(&yk[0]);
                ptr_gas[0].setState_HP(entha,*p);
                ptr_gas[0].getDestructionRates(con);
                ptr_gas[0].getCreationRates(pro);
                ptr_gas[0].getNetProductionRates(net);
                rho = ptr_gas[0].density();
                for (int i=0; i< *nspec; i++){
                     if (yk[i] > 0.0) {
                        yk1[i] = ( yk_ref[i] + dt * (pro[i] * (w_k[i]/rho)) ) / ( 1 + ( dt * abs(con[i])* (w_k[i]/rho) / yk[i] ) );
                     }
                     else {
                        yk1[i] = yk_ref[i] + (net[i] * w_k[i] * dt) / rho;
                     }
                     if (yk[i] > 1e-9){
                        errorArr[i] = std::abs( ( log10( yk1[i] ) / log10( yk[i] ) ) - 1 );
                     }
                     else {
                        errorArr[i] = 0.0; 
                     }
                }
                err = *std::max_element(errorArr,errorArr + *nspec);
                count = count + 1;
            }
        }
        ptr_gas[0].setMassFractions(&yk1[0]);
        ptr_gas[0].setState_HP(entha,*p);
        *temp =  ptr_gas[0].temperature();
        *p =  ptr_gas[0].pressure();
        ptr_gas[0].getMassFractions(&mass_frac[0]);
    }

} // extern "C"

// sums up the matrix rows to give the mixture coeffceints 
void cantera_sum (double* cp_cc,double* cp_mass_frac, int cp_nsp, double* W_k ,double* cp_low, double* cp_high)
{
    double sum_coeffs[15];
    std::vector< std::vector<double> > xxx(cp_nsp);
    for(int ii=0; ii<cp_nsp; ii++) xxx[ii] =  std::vector<double>(15,0);

    for(int ii=0, gg=0; ii<cp_nsp; ii++) {	 
        for(int jj=0; jj<15; jj++, gg++) xxx[ii][jj] = (cp_cc[gg]*Cantera::GasConstant*cp_mass_frac[ii]/W_k[ii]) / 1000.0;
    }
    for(int ii=0; ii<15; ii++) 
    {sum_coeffs[ii] = 0;
        for(int jj=0; jj<cp_nsp; jj++) {
            sum_coeffs[ii] = sum_coeffs[ii] + xxx[jj][ii];
        }
    }
    for(int ii=0; ii<6; ii++) cp_low[ii]  = sum_coeffs[8+ii]; 
    for(int ii=0; ii<6; ii++) cp_high[ii] = sum_coeffs[1+ii];
}

double get_dt (double* net, int nsp, double *t)
{
    double net_inv[nsp];
    double dt_div;
    double dt;

    dt_div = *t/10;
    for (int i=0; i< nsp; i++) net_inv[i] = 1.0 / std::abs(net[i]);
    std::sort(net_inv, net_inv + nsp);
    dt = net_inv[0]*0.001;
    //std::cout << dt << "Dt from source" << std::endl;
    //dt = std::min(dt,dt_div);
    return dt;
}

