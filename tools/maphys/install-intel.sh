if [ $# -eq 0 ]
then
  export BASEDIR=.
else
  export BASEDIR=$1
fi

export SCOTCHDIR=${BASEDIR}/scotch
export MUMPSDIR=${BASEDIR}/MUMPS_5.2.1
export MAPHYSDIR=${BASEDIR}/maphys

NPROCS_COMPILE=4

module purge
module load intel/2018.4 impi/2018.4 mkl/2018.4 bsc/1.0

#
# 1. Scotch
#
echo "----------- Install scotch -----------"

cd ${SCOTCHDIR}/src
mkdir install

make clean

cat <<EOF > Makefile.inc
EXE		=
LIB		= .so
OBJ		= .o

MAKE		= make
AR		= ar
ARFLAGS		= -ruv
CAT		= cat
CCS		= icc
CCP		= mpiicc
MPI_INC         = -I/apps/INTEL/2017.4/impi/2017.3.196/intel64/include
CCD		= icc \$(MPI_INC)
CFLAGS		= -O3 -DCOMMON_FILE_COMPRESS_GZ -DCOMMON_PTHREAD -DCOMMON_RANDOM_FIXED_SEED -DSCOTCH_RENAME -DSCOTCH_PTHREAD -restrict -DIDXSIZE64
CLIBFLAGS	= -shared -fPIC
LDFLAGS		= -lz -lm -lrt -pthread
CP		= cp
LEX		= flex -Pscotchyy -olex.yy.c
LN		= ln
MKDIR		= mkdir -p
MV		= mv
RANLIB		= ranlib
YACC		= bison -pscotchyy -y -b y
EOF

make scotch ptscotch esmumps ptesmumps -j ${NPROCS_COMPILE}
make prefix=${SCOTCHDIR}/install install

#
# 2. Mumps
#
echo "----------- Install mumps -----------"

cd ${MUMPSDIR}

make clean

cat <<EOF > Makefile.inc
SCOTCHDIR  = ${SCOTCHDIR}/install
ISCOTCH    = -I\$(SCOTCHDIR)/include
LSCOTCH    = -L\$(SCOTCHDIR)/lib -lesmumps -lscotch -lptesmumps -lptscotch -lscotcherr
LPORDDIR = \$(topdir)/PORD/lib/
IPORD    = -I\$(topdir)/PORD/include/
LPORD    = -L\$(LPORDDIR) -lpord
ORDERINGSF  = -Dpord -Dptscotch
ORDERINGSC  = \$(ORDERINGSF)
LORDERINGS = \$(LPORD) \$(LSCOTCH)
IORDERINGSF = \$(ISCOTCH)
IORDERINGSC = \$(IPORD) \$(ISCOTCH)
PLAT    =
LIBEXT  = .so
PIC     = -fPIC
OUTC    = -o 
OUTF    = -o 
RM = /bin/rm -f
CC = mpiicc
FC = mpiifort
FL = mpiifort
AR = ar vr 
RANLIB  = echo
LAPACK = -L\$(MKLROOT)/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core
SCALAP = -L\$(MKLROOT)/lib/intel64 -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64
LIBPAR = \$(SCALAP) \$(LAPACK)
INCSEQ = -I\$(topdir)/libseq
LIBSEQ  = \$(LAPACK) -L\$(topdir)/libseq -lmpiseq
LIBBLAS = -L\$(MKLROOT)/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core 
LIBOTHERS = -lpthread
CDEFS   = -DAdd_
OPTF    = -O2 -nofor_main \$(PIC) -DBLR_MT -qopenmp -DGEMMT_AVAILABLE
OPTL    = -O2 -nofor_main \$(PIC) -qopenmp
OPTC    = -O2 \$(PIC) -qopenmp
INCS = \$(INCPAR)
LIBS = \$(LIBPAR)
LIBSEQNEEDED =
EOF

make all -j ${NPROCS_COMPILE}

#
# 3. Maphys
#
echo "----------- Install maphys -----------"

cd ${MAPHYSDIR}
mkdir build install
cd build
export SCOTCH_DIR=${SCOTCHDIR}
export PTSCOTCH_DIR=${SCOTCHDIR}
export MUMPS_DIR=${MUMPSDIR}
cmake .. -DMAPHYS_SDS_PASTIX=OFF -DCMAKE_INSTALL_PREFIX=../install -DMAPHYS_BLASMT=ON -DBUILD_SHARED_LIBS=ON
make install -j ${NPROCS_COMPILE}
