!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Parall
!> @{
!> @file    par_initialize_mpi.f90
!> @author  houzeaux
!> @date    2020-05-08
!> @brief   Start Parall 
!> @details Turn on parall
!> @} 
!-----------------------------------------------------------------------

subroutine par_initialize_mpi()
  use def_kintyp_basic,         only : rp
  use mod_outfor,               only : outfor
  use mod_communications_tools, only : PAR_COMM_SET_ERRHANDLER
  use def_parall
  use mod_parall
  real(rp) :: time1,time2
  
  call cputim(time1)
  call par_initia()                                      ! Initialize MPI
  call par_errors(1_ip)
  call cputim(time2)
  cpu_paral(1)=time2-time1

  call PAR_COMM_SET_ERRHANDLER()
  
end subroutine par_initialize_mpi
