!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Redist
!> @{
!> @file    Redist.f90
!> @author  houzeaux
!> @date    2019-06-17
!> @brief   Redistribution
!> @details redistribution of data
!> @} 
!-----------------------------------------------------------------------

subroutine Redist()
  
  use def_kintyp,   only : ip
  use def_master,   only : ITASK_REDIST
  use def_master,   only : iblok
  use def_master,   only : nblok
  use mod_moduls,   only : moduls
  use mod_messages, only : messages_live
  implicit none
  
  call messages_live('REDISTRIBUTION','START SECTION')
  do iblok = 1,nblok
     call moduls(ITASK_REDIST)
  end do
  call Kermod(ITASK_REDIST)
  call messages_live('REDISTRIBUTION','END SECTION')
    
end subroutine Redist
