#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(init_openacc)
  if (WITH_GPU)
    FIND_PACKAGE(OpenACC)
    if (NOT OpenACC_Fortran_FOUND)
      message(STATUS "OpenACC Fortran not found! You can ignore this warning unless errors occur during the build.")
      set(OpenACC_Fortran_FOUND TRUE CACHE INTERNAL "")
      set(OpenACC_Fortran_FLAGS ${OpenACC_C_FLAGS} CACHE INTERNAL "")
    else()
      set(OpenACC_Fortran_FLAGS ${OpenACC_Fortran_FLAGS} CACHE INTERNAL "")
    endif()
    set(OpenACC_C_FLAGS ${OpenACC_C_FLAGS} CACHE INTERNAL "")
    set(OpenACC_CXX_FLAGS ${OpenACC_CXX_FLAGS} CACHE INTERNAL "")
  endif()
endfunction()

function(target_compile_flags_openacc)
  if (WITH_GPU)
    SET_TARGET_PROPERTIES(${PROJECT_NAME} PROPERTIES
                        COMPILE_FLAGS "${OpenACC_Fortran_FLAGS}")
  endif()
endfunction()

function(target_link_flags_openacc)
  if (WITH_GPU)
    SET_TARGET_PROPERTIES(${PROJECT_NAME} PROPERTIES
                        LINK_FLAGS "${OpenACC_Fortran_FLAGS}")
  endif()
endfunction()
