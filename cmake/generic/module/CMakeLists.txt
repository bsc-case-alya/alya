#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



get_filename_component(ProjectId ${CMAKE_CURRENT_LIST_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})

project(${ProjectId} LANGUAGES Fortran)
file(GLOB_RECURSE SRC_FILES ${CMAKE_HOME_DIRECTORY}/src/modules/${PROJECT_NAME}/*.f90)
add_library(${PROJECT_NAME} ${LIBRARY_TYPE} ${SRC_FILES})

include(compiler)
set_flags()
set_defs()
set_parall()
include(dependencies)

include(lib-management)
alya_link(kernel) 

include(lib-set-install)
