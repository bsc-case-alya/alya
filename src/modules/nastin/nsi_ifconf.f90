!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_ifconf(itask)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_ifconf
  ! NAME 
  !    nsi_ifconf
  ! DESCRIPTION
  !    This routine checks if the flow is confined or not and look for a node
  !    to impose pressure 
  ! USED BY
  !    nsi_solite
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_nastin
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipoin,idime,ielem,kbopo,iperi
  integer(ip)             :: inode

  return

  select case(itask)

  case(2_ip)

     if( INOTMASTER .and. kfl_confi_nsi == 1 .and. nodpr_nsi /= 0 .and. nperi > 0 ) then
        do iperi = 1,nperi
           if( lperi(2,iperi) == nodpr_nsi ) then
              nodpr_nsi = lperi(1,iperi)
           end if
        end do

     end if

  end select

end subroutine nsi_ifconf
