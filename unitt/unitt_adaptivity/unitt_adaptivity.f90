!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_adaptivity ! adapting mesh to analytical mesh size (sequential, only via mod_adapt no mod_amr)
  !
  ! Test boundary mesh
  !
  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_master
  use mod_memory
  
  use mod_quality,    only: compute_mesh_quality_sizeShape
  use mod_quality,    only: compute_mesh_quality_shape
  use mod_quality,    only: print_q_stats
  use mod_quality,    only: quality_deallo
  use mod_adapt,      only: adapt_mesh_to_metric
  use mod_metric,     only: mesh_metric_type
  use mod_meshEdges,  only: compute_mesh_edgeLengths
  use mod_meshTopology, only: node_to_elems_type
  use mod_out_paraview, only: out_paraview_inp
  use def_adapt,              only : memor_adapt 
  !use mod_debugTools, only: out_debug_text, out_debug_paraview
  
  implicit none

  real(rp),             parameter :: epsil = 1.0e-12_rp
  type(mesh_type_basic)           :: mesh
  type(mesh_type_basic)           :: mesh_bou
  integer(ip)                     :: ipoin,idime,ielem,inode,iboun,idim,ix,iy,quad(4),hex(8),iz
  integer(ip)                     :: pelty,ii
  real(rp)                        :: dista,xx_test(2)
  integer(8)                      :: count_rate8
  integer(8)                      :: memor_loc(2)
  integer(8)                      :: memor_tmp(2)
  real(rp) :: hx,hy,x0,x1,y0,y1,r,PI,z0,z1,hz
  integer(ip) :: nx,ny,ratioXY,nz, itet

  type(mesh_metric_type) :: metric
  real(rp), allocatable, target :: M(:,:,:)
  logical(lg) :: isSizeField
  
  real(rp), pointer :: q(:)
  real(rp),    pointer               :: edge_lengths(:)
  integer(ip), pointer               :: edge_to_node(:,:)
  integer(ip) :: select_metric 
  
  type(node_to_elems_type) :: node_to_elems
    
  integer(ip), pointer :: mapNodes_old_to_new(:)
  
  logical(lg) :: out_debug_text, out_debug_paraview
  
  out_debug_text     =.false.
  out_debug_paraview =.false.


!   print*,'1'
  memor_loc = 0_8
  memor_tmp = 0_8
  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)
  !
  ! Generate mesh
  !
  call elmgeo_element_type_initialization()
  !
  call mesh     % init('MY_MESH')
  call mesh_bou % init('BOUNDARY')
!   print*,'2'
  if(.false.) then 
    
    ! MESH 1
    mesh % nelem = 2
    mesh % npoin = 4
    mesh % ndime = 2
    mesh % mnode = 3
    call mesh % alloca()
    mesh % lnods(:,1)  = (/  1 ,2 ,3  /)
    mesh % lnods(:,2)  = (/  2 ,4 ,3   /)
    mesh % ltype(1:2)  = TRI03
    mesh % coord(:,1 ) = (/ 0.000000e+00_rp , 0.000000e+00_rp /)
    mesh % coord(:,2 ) = (/ 1.000000e+00_rp , 0.000000e+00_rp /)
    mesh % coord(:,3 ) = (/ 0.5e+00_rp , (sqrt(3.0_rp)/2.0_rp) /)
    mesh % coord(:,4 ) = (/ 1.5e+00_rp , (sqrt(3.0_rp)/2.0_rp) /)
    
    ! MESH 2
!     mesh % nelem = 2
!     mesh % npoin = 4
!     mesh % ndime = 2
!     mesh % mnode = 3
!     call mesh % alloca()
!     mesh % lnods(:,1)  = (/  1 ,2 ,3  /)
!     mesh % lnods(:,2)  = (/  1 ,3 ,4   /)
!     mesh % ltype( : )  = TRI03
!     mesh % coord(:,1 ) = (/ 0.000000e+00_rp , 0.000000e+00_rp /)
!     mesh % coord(:,2 ) = (/ 1.000000e+00_rp , 0.000000e+00_rp /)
!     mesh % coord(:,3 ) = (/ 1.000000e+00_rp , 1.000000e+00_rp /)
!     mesh % coord(:,4 ) = (/ 0.000000e+00_rp , 1.000000e+00_rp /)
    !
    !MESH 3
!     mesh % nelem = 24
!     mesh % npoin = 20
!     mesh % ndime = 2
!     mesh % mnode = 3
!     call mesh % alloca()
!     mesh % ltype(:)  = TRI03
!     mesh % lnods(:,1)  = (/  2 ,5 ,4 /)
!     mesh % lnods(:,2)  = (/  1 ,2 ,4  /)
!     mesh % lnods(:,3)  = (/  2 ,3 ,6  /)
!     mesh % lnods(:,4)  = (/  2 ,6 ,5  /)
!     mesh % lnods(:,1+4)= (/  1 ,2 ,5  /) + 3
!     mesh % lnods(:,2+4)= (/  1 ,5 ,4  /) + 3
!     mesh % lnods(:,3+4)= (/  2 ,3 ,6  /) + 3
!     mesh % lnods(:,4+4)= (/  2 ,6 ,5  /) + 3
!     mesh % lnods(:,1+8)= (/  1 ,2 ,5  /) + 6
!     mesh % lnods(:,2+8)= (/  1 ,5 ,4  /) + 6
!     mesh % lnods(:,3+8)= (/  3 ,6 ,5  /) + 6
!     mesh % lnods(:,4+8)= (/  2 ,3 ,5  /) + 6
!
!     mesh % lnods(:,1+12)  = (/  13,15, 6 /)
!     mesh % lnods(:,2+12)  = (/  3 ,13, 6 /)
!     mesh % lnods(:,3+12)  = (/  13,14,16 /)
!     mesh % lnods(:,4+12)  = (/  13,16,15 /)
!     mesh % lnods(:,1+16)= (/  6,15,9/)
!     mesh % lnods(:,2+16)= (/  15,17,9/)
!     mesh % lnods(:,3+16)= (/  15,16,18/)
!     mesh % lnods(:,4+16)= (/  15,18,17/)
!     mesh % lnods(:,1+20)= (/  9,17,19/)
!     mesh % lnods(:,2+20)= (/  9,19,12/)
!     mesh % lnods(:,3+20)= (/  17,18,19/)
!     mesh % lnods(:,4+20)= (/  18,20,19 /)
!     mesh % coord(:,1 ) = (/-1.0e+00_rp ,-1.0e+00_rp /)
!     mesh % coord(:,2 ) = (/ 0.0e+00_rp ,-1.0e+00_rp /)
!     mesh % coord(:,3 ) = (/ 1.0e+00_rp ,-1.0e+00_rp /)
!     mesh % coord(:,1+3)= (/-1.0e+00_rp , 0.0e+00_rp /)
!     mesh % coord(:,2+3)= (/ 0.0e+00_rp , 0.0e+00_rp /)
!     mesh % coord(:,3+3)= (/ 1.0e+00_rp , 0.0e+00_rp /)
!     mesh % coord(:,1+6)= (/-1.0e+00_rp , 1.0e+00_rp /)
!     mesh % coord(:,2+6)= (/ 0.0e+00_rp , 1.0e+00_rp /)
!     mesh % coord(:,3+6)= (/ 1.0e+00_rp , 1.0e+00_rp /)
!     mesh % coord(:,1+9)= (/-1.0e+00_rp , 2.0e+00_rp /)
!     mesh % coord(:,2+9)= (/ 0.0e+00_rp , 2.0e+00_rp /)
!     mesh % coord(:,3+9)= (/ 1.0e+00_rp , 2.0e+00_rp /)
!     mesh % coord(:,13 )= (/ 2.0e+00_rp ,-1.0e+00_rp /)
!     mesh % coord(:,15 )= (/ 2.0e+00_rp , 0.0e+00_rp /)
!     mesh % coord(:,17 )= (/ 2.0e+00_rp , 1.0e+00_rp /)
!     mesh % coord(:,19 )= (/ 2.0e+00_rp , 2.0e+00_rp /)
!     mesh % coord(:,13+1)= (/ 3.0e+00_rp ,-1.0e+00_rp /)
!     mesh % coord(:,15+1)= (/ 3.0e+00_rp , 0.0e+00_rp /)
!     mesh % coord(:,17+1)= (/ 3.0e+00_rp , 1.0e+00_rp /)
!     mesh % coord(:,19+1)= (/ 3.0e+00_rp , 2.0e+00_rp /)
!     !
!     !
    isSizeField = .true.
    call metric%alloca(mesh%ndime,mesh%npoin,isSizeField)
!     metric%M = 1.0
!     metric%M(1,1,5) = 0.5
!     metric%M(1,1,8) = metric%M(1,1,5)
!     metric%M(1,1,15) = 2.0
!     metric%M(1,1,17) = metric%M(1,1,15)
  
  !metric%M = metric%M/2.0
  
  metric%M = 0.0_rp
!   do idim=1,mesh%ndime
!     metric%M(idim,idim,:) = 1.0
!   end do
  metric%M(1,1,:) = 1.0_rp
! !   metric%M(1,1,(/5/))  = (1.0/10.0)**2
! !   metric%M(2,2,(/5/))  = (1.0/10.0)**2
!   metric%M(1,1,(/5/))  = (1.0/0.3)!**2
!   metric%M(2,2,(/5/))  = (1.0/0.3)!**2
!   metric%M(1,1,(/8/))  = (1.0/0.3)!**2
!   metric%M(2,2,(/8/))  = (1.0/0.3)!**2
! !   metric%M(1,1,(/2/))  = (1.0/0.5)**2
! !   metric%M(2,2,(/2/))  = (1.0/0.5)**2
! !   metric%M(1,1,(/1,2/))  = (1.0/2.0)**2
! !   metric%M(2,2,(/1,2/))  = (1.0/2.0)**2
  end if
  if(.true.) then !2D
    ratioXY = 2_ip  !2_ip
    nx = 5_ip !20_ip
    ny = nx
    nx = nx*ratioXY
    
    mesh % nelem = nx*ny*2
    mesh % npoin = (nx+1)*(ny+1)
    mesh % ndime = 2
    mesh % mnode = 3
    call mesh % alloca()
    mesh % ltype(:)  = TRI03
    
    x0 = -1.0_rp*ratioXY
    x1 =  1.0_rp*ratioXY
    y0 = -1.0_rp
    y1 =  1.0_rp
    hx = (x1-x0)/nx
    hy = (y1-y0)/ny
    do iy=0,ny
      do ix=0,nx
        inode = (1+ix) + iy*(1+nx)
        mesh%coord(:, inode ) = (/ x0 + hx*ix, y0 + hy*iy/)
        if((ix>0).and.(iy>0)) then
          quad(1) = (1+ix-1) + (iy-1)*(1+nx) 
          quad(2) = (1+ix  ) + (iy-1)*(1+nx) 
          quad(3) = (1+ix  ) + (iy-0)*(1+nx) 
          quad(4) = (1+ix-1) + (iy-0)*(1+nx) 
          mesh%lnods(:, (2*ix - 1) + (iy-1)*(2*nx) ) = quad(1:3)
          mesh%lnods(:, (2*ix    ) + (iy-1)*(2*nx) ) = quad((/1,3,4/))
        end if
      end do
    end do
    
    !Metrics to be selected:
    !1 => refine
    !2 => coarsen
    !3 => refine and coarsen 
    !4 => refine and coarsen extreme
    select_metric = 3_ip
    
    isSizeField = .true.
    call metric%alloca(mesh%ndime,mesh%npoin,isSizeField)
    PI=4.0_rp*DATAN(1.0_rp)
    do iy=0_ip,ny
      do ix=0_ip,nx
        inode = (1+ix) + iy*(1+nx)
        select case (select_metric)
          case(  1_ip  )
            !!!******* Metric with small size in center
            r = sqrt(sum( mesh%coord(:,inode)**2_ip ))
            if(r>1.0_rp) then
              r = 1.0_rp
            end if
            metric%M(1,1,inode) = (hx/5.0_rp)*(1.0_rp-r) + hx*r
            !metric%M = hx/2.0

          case(  2_ip  )
            !!!******* Metric with small size in center
            r = sqrt(sum( mesh%coord(:,inode)**2_ip ))
            r = r**(1.0_rp)
            if(r>1.0_rp) then
              r = 1.0_rp
            end if
            metric%M(1,1,inode) = (hx*5.0_rp)*(1.0_rp-r) + hx*r
            !print*,r,"  ",metric%M(1,1,inode)
            
          case(  3_ip  )
            !!!******* Metric combining small and big
            r = sin(mesh%coord(1,inode)*PI/ratioXY)*cos(mesh%coord(2,inode)*PI/2.0)
            if(r>=0.0_rp) then
              if(r>1.0_rp) then
                r = 1.0_rp
              end if
              metric%M(1,1,inode) = (hx/5.0_rp)*(r) + hx*(1.0_rp-r)
            else
              r = abs(r)
              if(r>1.0_rp) then
                r = 1.0_rp
              end if
              metric%M(1,1,inode) = (hx*5.0_rp)*(r) + hx*(1.0_rp-r)
            end if
            
          case(  4_ip  )
            !!!******* Metric combining small and big
            r = sin(mesh%coord(1,inode)*PI/ratioXY)*cos(mesh%coord(2,inode)*PI/2.0)
            if(r>=0.0_rp) then
              if(r>1.0_rp) then
                r = 1.0_rp
              end if
              metric%M(1,1,inode) = (hx/5.0_rp)*(r) + hx*(1.0_rp-r)
              metric%M(1,1,inode) = metric%M(1,1,inode)/5.0
            else
              r = abs(r)
              if(r>1.0_rp) then
                r = 1.0_rp
              end if
              metric%M(1,1,inode) = (hx*5.0_rp)*(r) + hx*(1.0_rp-r)
              metric%M(1,1,inode) = metric%M(1,1,inode)*5.0
            end if
            
            
          case default
            metric%M = hx
        end select
      end do
    end do
  end if
  if(.false.) then !3D
    ratioXY = 2_ip
    nx = 10_ip
    ny = nx
    nz = nx
    nx = nx*ratioXY
    
    mesh % nelem = nx*ny*ny*6_ip
    mesh % npoin = (nx+1)*(ny+1)*(nz+1)
    mesh % ndime = 3
    mesh % mnode = 4
    call mesh % alloca()
    mesh % ltype(:)  = TET04
    
    x0 = -1.0_rp*ratioXY
    x1 =  1.0_rp*ratioXY
    y0 = -1.0_rp
    y1 =  1.0_rp
    z0 = -1.0_rp
    z1 =  1.0_rp
    hx = (x1-x0)/nx
    hy = (y1-y0)/ny
    hz = (z1-z0)/nz
    itet = 0
    loopZ: do iz=0_ip,nz
      loopY: do iy=0_ip,ny
        loopX: do ix=0_ip,nx
          !
          inode = (1+ix) + iy*(1+nx) + iz*((1+nx) + ny*(1+nx))
          mesh%coord(:, inode ) = (/ x0 + hx*ix, y0 + hy*iy, z0 + hz*iz/)
          if((ix>0).and.(iy>0).and.(iz>0)) then
            hex(1) = (1+ix-1) + (iy-1)*(1+nx) + (iz-1)*((1+nx) + ny*(1+nx))
            hex(2) = (1+ix  ) + (iy-1)*(1+nx) + (iz-1)*((1+nx) + ny*(1+nx))
            hex(3) = (1+ix  ) + (iy-0)*(1+nx) + (iz-1)*((1+nx) + ny*(1+nx))
            hex(4) = (1+ix-1) + (iy-0)*(1+nx) + (iz-1)*((1+nx) + ny*(1+nx))
            hex(5) = (1+ix-1) + (iy-1)*(1+nx) + (iz  )*((1+nx) + ny*(1+nx))
            hex(6) = (1+ix  ) + (iy-1)*(1+nx) + (iz  )*((1+nx) + ny*(1+nx))
            hex(7) = (1+ix  ) + (iy-0)*(1+nx) + (iz  )*((1+nx) + ny*(1+nx))
            hex(8) = (1+ix-1) + (iy-0)*(1+nx) + (iz  )*((1+nx) + ny*(1+nx))
            itet = itet+1
            mesh%lnods(:, itet ) = hex((/1,2,7,6/))
            itet = itet+1
            mesh%lnods(:, itet ) = hex((/1,2,3,7/))
            itet = itet+1
            mesh%lnods(:, itet ) = hex((/1,3,4,7/))
            itet = itet+1
            mesh%lnods(:, itet ) = hex((/1,6,7,5/))
            itet = itet+1
            mesh%lnods(:, itet ) = hex((/1,7,4,8/))
            itet = itet+1
            mesh%lnods(:, itet ) = hex((/1,7,8,5/))
          end if
          !
        end do loopX
      end do loopY
    end do loopZ
    
    !Metrics to be selected:
    !1 => refine
    !2 => coarsen
    !3 => refine and coarsen => ARREGLAR, HI HA UN ERROR!!!!!
    select_metric = 3_ip
    
    isSizeField = .true.
    call metric%alloca(mesh%ndime,mesh%npoin,isSizeField)
    PI=4.0_rp*DATAN(1.0_rp)
    loopNode: do inode=1,mesh%npoin
      !
      select case (select_metric)
        case(  1_ip  )
          !!!******* Metric with small size in center
          r = sqrt(sum( mesh%coord(:,inode)**2_ip ))
          if(r>1.0_rp) then
            r = 1.0_rp
          end if
          metric%M(1,1,inode) = (hx/5.0_rp)*(1.0_rp-r) + hx*r

        case(  2_ip  )
          !!!******* Metric with small size in center
          r = sqrt(sum( mesh%coord(:,inode)**2_ip ))
          r = r**(1.0_rp)
          if(r>1.0_rp) then
            r = 1.0_rp
          end if
          metric%M(1,1,inode) = (hx*5.0_rp)*(1.0_rp-r) + hx*r
          print*,r,"  ",metric%M(1,1,inode)
        
        case(  3_ip  )
          !!!******* Metric combining small and big
          r = sin(mesh%coord(1,inode)*PI/ratioXY)*cos(mesh%coord(2,inode)*PI/2.0)*cos(mesh%coord(3,inode)*PI/2.0)
          if(r>=0.0_rp) then
            if(r>1.0_rp) then
              r = 1.0_rp
            end if
            metric%M(1,1,inode) = (hx/5.0_rp)*(r) + hx*(1.0_rp-r)
          else
            r = abs(r)
            if(r>1.0_rp) then
              r = 1.0_rp
            end if
            metric%M(1,1,inode) = (hx*3.0_rp)*(r) + hx*(1.0_rp-r)
          end if
        
        case default
          metric%M = hx
      end select
    end do loopNode
  end if

  !mesh%coord(1,1:(nx+1)) = mesh%coord(1,1:(nx+1)) + hx/2.0
  !mesh%coord(2,1:(nx+1))=mesh%coord(2,1:(nx+1))+(1-sqrt(3.0)/2.0)/(nx/2.0)

  !call out_paraview_inp(mesh,filename='./unitt/unitt_adaptivity/mesh_check',nodeField=metric%M(1,1,:))
  
  if(out_debug_text) print*,'_______________________________________________________________'
  call compute_mesh_quality_shape(mesh,q)
  call print_q_stats(q)
  call quality_deallo(q)
  
  call compute_mesh_quality_sizeShape(mesh,metric,q)
  call print_q_stats(q)
  if(out_debug_paraview) call mesh%output(filename='./unitt/unitt_adaptivity/mesh')
  if(out_debug_paraview) call out_paraview_inp(mesh,filename='./unitt/unitt_adaptivity/mesh_0',nodeField=metric%M(1,1,:),elemField=q)
  call quality_deallo(q)
  
  if(out_debug_text) print*,'_______________________________________________________________'
  if(out_debug_text) print*,'_____________________CHECKING..._______________________________'
  call node_to_elems%set(mesh)
  
  if(out_debug_text) print*,'_______________________________________________________________'
  if(out_debug_text) print*,'_____________________IMPLEMENTING...___________________________'
    
  call adapt_mesh_to_metric(mesh,metric,mapNodes_input_to_adapted_mesh=mapNodes_old_to_new,lock_valid_elems=.false.)
  
  if(out_debug_text) print*,'_______________________________________________________________'
  if(out_debug_text) print*,'_____________________Checking quality....______________________'
  call compute_mesh_quality_shape(mesh,q)
  call print_q_stats(q)
  call quality_deallo(q)
  
  call compute_mesh_quality_sizeShape(mesh,metric,q)
  call print_q_stats(q)
  if(out_debug_paraview) call out_paraview_inp(mesh,filename='./unitt/unitt_adaptivity/mesh_1',nodeField=metric%M(1,1,:),elemField=q)
  call quality_deallo(q)
  
  call mesh%deallo
  call metric%deallo
  call memory_deallo(memor_adapt,'mapNodes_old_to_new','unitt_adaptivity',mapNodes_old_to_new)
  
!   call runend('O.K.!')                                               ! Finish Alya
  ! 
end program unitt_adaptivity     
