!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine linint(ncoef,value,xcoef,varia,dvari)

  !-----------------------------------------------------------------------
  !****f* mathru/linint
  ! NAME 
  !    linint
  ! DESCRIPTION
  !    Compute a variable and its derivatives using linear interpolation
  ! USES
  ! USED BY
  !    *
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  implicit none
  integer(ip), intent(in)  :: ncoef
  real(rp),    intent(in)  :: value,xcoef(2,ncoef)
  real(rp),    intent(out) :: varia,dvari
  integer(ip)              :: icoef,jcoef
  real(rp)                 :: xvalu

  if(value<xcoef(1,1)) then
     xvalu=xcoef(1,1)
  else if(value>xcoef(1,ncoef)) then
     xvalu=xcoef(1,ncoef)
  else
     xvalu=value
  end if

  icoef=1
  do while(icoef<ncoef)
     icoef=icoef+1
     if(xcoef(1,icoef)>=xvalu) then
        jcoef=icoef-1
        icoef=ncoef
     end if
  end do

  dvari=(xcoef(2,jcoef+1)-xcoef(2,jcoef))/(xcoef(1,jcoef+1)-xcoef(1,jcoef))
  varia=dvari*(xvalu-xcoef(1,jcoef))+xcoef(2,jcoef)
  
end subroutine linint
