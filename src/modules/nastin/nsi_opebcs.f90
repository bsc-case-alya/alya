!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_opebcs()
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_opebcs
  ! NAME 
  !    nsi_opebcs
  ! DESCRIPTION
  !    This routine imposes boiundary conditions according to codes
  ! USES
  ! USED BY
  !    nsi_turnon
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_inpout
  use def_master
  use def_domain
  use def_nastin 
  use mod_memchk
  implicit none

  !if( INOTMASTER .and. kfl_wwork == 1 ) then

  !if( kfl_conbc_nsi == 0 ) then
  !   iffun      =  1
  !   kfl_funno  => kfl_funno_nsi
  !else
  !   iffun      =  0
  !end if
  !ifbop     =  0
  !ifloc     =  1
  !skcos_nsi => skcos
  !kfl_fixrs => kfl_fixrs_nsi
  !kfl_fixno => kfl_fixno_nsi
  !bvess     => bvess_nsi(:,:,1)
  !call reacod(1_ip)
  
end subroutine nsi_opebcs
