!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!------------------------------------------------------------------------
!> @addtogroup Mathematics
!> @{
!> @file    eigsrt.f90
!> @author  Herbert Owen
!> @brief   Sorts the eigenvalues, D, into ascending order.
!> @details The eigenvectors, V, are rearranged accordingly.
!> @} 
!------------------------------------------------------------------------
subroutine eigsrt(D,V)

  use def_kintyp
  implicit none
  real(rp),    intent(inout) :: D(3), V(3,3)
  real(rp)                   :: P
  integer(ip)                :: idime, jdime, K
  
  do idime=1,2
     K = idime
     P = D(idime)
     do jdime = idime + 1,3
        if (D(jdime) >= P) then
           K = jdime
           P = D(jdime)
        end if
     end do

     if (K /= idime) then
        D(K) = D(idime)
        D(idime) = P
        do jdime=1,3
           P = V(jdime,idime)
           V(jdime,idime) = V(jdime,K)
           V(jdime,K) = P
        end do
     end if
  end do

end subroutine eigsrt


