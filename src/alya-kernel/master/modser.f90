!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine modser
  !------------------------------------------------------------------------
  !****f* master/outerr
  ! NAME 
  !    outerr
  ! DESCRIPTION
  !    This routine checks the order of modules iterations, and compute
  !    number of modules and services
  ! OUTPUT
  !    LMORD(IORDE,IBLOK) ... Module number to run at IORDE position
  !                           within block IBLOK
  !    NMODU ................ Number of modules
  !    NSERV ................ Number of services
  ! USES
  ! USED BY
  !    Reapro
  !***
  !------------------------------------------------------------------------
  use def_kintyp
  use def_master
  implicit none
  integer(ip) :: imodu,iorde,iserv,jblok
  !
  ! Automatic ordering of the modules: LMORD
  !
  if( lmord(1,1) == 0 ) then
     nblok = 1     
     iorde = 0
     do imodu = 1,mmodu-1
        if( kfl_modul(imodu) /= 0 ) then
           iorde = iorde + 1
           lmord(iorde,1) = imodu
        end if
     end do
  else
     
  end if
  !
  ! Count the number of modules used: NMODU
  !
  nmodu = 0
  do imodu = 1,mmodu
     if( kfl_modul(imodu) /= 0 ) nmodu = nmodu + 1
  end do

end subroutine modser
