!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!------------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_penpre.f90
!> @author  Guillaume Houzeaux
!> @brief   Pressure penalization
!> @details Penalize the pressure equation as:
!>          \verbatim
!>          App.p^i+1 + eps*diag(App).p^i + Apu.u = bp + eps*diag(App) p^{i-1}
!>          Q <= eps*diag(App)
!>          \endverbatim
!>          where eps is a user-defined constant
!> @} 
!------------------------------------------------------------------------

subroutine nsi_penpre(App,Q,bp)
  use def_kintyp
  use def_domain
  use def_master
  use def_nastin

  implicit none
  real(rp),    intent(out)   :: App(nzdom)
  real(rp),    intent(out)   :: Q(nzdom)
  real(rp),    intent(out)   :: bp(npoin)
  integer(ip)                :: ipoin,jpoin,izdom
  real(rp)                   :: xdiag
  
  do ipoin = 1,npoin

     izdom = r_dom(ipoin) - 1
     jpoin = 0
     do while( jpoin /= ipoin )
        izdom = izdom + 1
        jpoin = c_dom(izdom)
     end do 

     xdiag      = App(izdom) * prepe_nsi
     App(izdom) = App(izdom) + xdiag
     Q(izdom)   = Q(izdom)   + xdiag
     bp(ipoin)  = bp(ipoin)  + xdiag * press(ipoin,1)

  end do

end subroutine nsi_penpre

