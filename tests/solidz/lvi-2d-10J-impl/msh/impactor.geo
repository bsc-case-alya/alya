/*

impactor - 2d

Inputs:
L	- Length
T	- Thickness

Mesh:
lc_# 	- Characteristic Length of Edge/Point
nnZ     - Number of Nodes in Z or width direction

*/

// - - - - - - - - - - - - - - - - - - - - - - -
// Inputs
// - - - - - - - - - - - - - - - - - - - - - - -

// Geometry
R    =  8.0;     // mm

// Mesh
lc_S  =  0.75;  // element size

nntop  = Ceil(R/(lc_S*2));     // Number of Nodes piezo arcs

// - - - - - - - - - - - - - - - - - - - - - - -
// Start of Code
// - - - - - - - - - - - - - - - - - - - - - - -

lc=lc_S;

// Create 4 points to define the base
//                       { x     y  z mesh}
p01 = newp; Point(p01) = { -R,   R, 0,  lc};
p02 = newp; Point(p02) = {  0,   R, 0,  lc};
p03 = newp; Point(p03) = {  R,   R, 0,  lc};
p04 = newp; Point(p04) = {  0,   0, 0,  lc};

// Define lines
l01 = newl; Circle(l01) = {p01, p02, p04};
l02 = newl; Circle(l02) = {p04, p02, p03};
l03 = newl; Line(l03) = {p01,p02};
l04 = newl; Line(l04) = {p02,p03};
l05 = newl; Line(l05) = {p02,p04};

// Line loop and define surface (base)
ll01=newll; Line Loop(ll01) = {l01,-l05,-l03};
ll02=newll; Line Loop(ll02) = {l02,-l04,l05};

// Base surface
s01=news; Plane Surface(s01) = {ll01};
s02=news; Plane Surface(s02) = {ll02};

// - - - - - - - - - - - - - - - - - - - - - - -
// Material Codes
// - - - - - - - - - - - - - - - - - - - - - - -

Physical Surface("MATER_BULK") = {s01,s02};
// - - - - - - - - - - - - - - - - - - - - - - -
// Boundary Conditions
// - - - - - - - - - - - - - - - - - - - - - - -

Physical Line("BCBOU_SUR")   = {l01,l02};
Physical Line("BCBOU_TOP")   = {l03,l04};

Transfinite Line {l03,l04,l05} = nntop;

Mesh.CharacteristicLengthFactor = lc_S;

