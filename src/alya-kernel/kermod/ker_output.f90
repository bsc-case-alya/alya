!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine ker_output()
  !------------------------------------------------------------------------
  !****f* Kermod/ker_output
  ! NAME 
  !    ker_output
  ! DESCRIPTION
  !    Output module variables
  ! USES
  !    ker_outvar
  ! USED BY
  !    ker_endste (itask=1)
  !    ker_turnof (itask=2)
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use mod_witness
  use mod_output_postprocess
  implicit none
  integer(ip) :: ivari,ivarp
  external    :: ker_outvar
  !
  ! Create and output witness meshes
  !
  if( ittyp == ITASK_INITIA ) then
     call witness_mesh_create()   
  end if
  !
  ! Postprocess variables
  !
  call output_postprocess_variables(ker_outvar)
    
end subroutine ker_output
