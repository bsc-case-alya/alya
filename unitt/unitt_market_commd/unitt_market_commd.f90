!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_market_commd

  use def_kintyp_basic
  use def_master
  use mod_parall
  use mod_memory
  use mod_matrix_market
  use mod_strings
  
  implicit none
  integer(ip)          :: ii,ineig,dom_i
  integer(ip), pointer :: ja(:)
  
  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction

  call Turnon()                                               ! Read modules
  call Solmem()                                               ! Solver and output memory
  call Begrun()                                               ! Initial computations

  if( ISLAVE ) then

     print*,'printing neighors'
     allocate(ja(commd % bound_dim))
     do ineig = 1,commd % nneig
        dom_i = commd % neights(ineig)
        do ii = commd % bound_size(ineig),commd % bound_size(ineig+1)-1
           ja(ii) = dom_i
        end do
     end do
     call matrix_market_matrix(1_ip,1_ip,commd % nneig,IA=commd % bound_size,JA=ja,A=commd % bound_perm,&
          FILENAME=trim(namda)//'-bound_perm-'//integer_to_string(kfl_paral)//'.mtx',PERMA=lninv_loc)
     call matrix_market_vector(commd % bound_size,&
          FILENAME=trim(namda)//'-bound_size-'//integer_to_string(kfl_paral)//'.mtx')
     call matrix_market_vector(commd % neights,&
          FILENAME=trim(namda)//'-neights-'//integer_to_string(kfl_paral)//'.mtx')
     deallocate(ja)

  end if

  call runend('O.K.!')
  
end program unitt_market_commd
