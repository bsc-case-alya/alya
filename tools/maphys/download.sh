if [ $# -eq 0 ]
then
  export BASEDIR=.
else
  export BASEDIR=$1
fi

export SCOTCHDIR=${BASEDIR}/scotch
export MUMPSDIR=${BASEDIR}/MUMPS_5.2.1
export MAPHYSDIR=${BASEDIR}/maphys

export MUMPSZIP=MUMPS_5.2.1.tar.gz

CURRENTDIR=`pwd`


rm -fr $BASEDIR
mkdir -p $BASEDIR

git clone https://gitlab.inria.fr/scotch/scotch.git $SCOTCHDIR
git clone --recurse-submodules https://gitlab.inria.fr/solverstack/maphys/maphys.git $MAPHYSDIR
cd $BASEDIR
wget http://mumps.enseeiht.fr/$MUMPSZIP
tar -xf $MUMPSZIP
