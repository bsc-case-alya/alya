GiD Post Results File 1.0
 
GaussPoints GP Elemtype Quadrilateral
Number of Gauss Points:   1
Natural Coordinates: Internal
End GaussPoints
Result ANIP1 ALYA  0.00000000E+00 Vector OnGaussPoints GP
ComponentNames ANIP1_X,ANIP1_Y,ANIP1_Z
Values
          1  0.00000000E+000  0.00000000E+000
          2  0.00000000E+000  0.00000000E+000
          3  0.00000000E+000  0.00000000E+000
          4  0.00000000E+000  0.00000000E+000
          5  0.20000000E+001  0.00000000E+000
          6  0.00000000E+000  0.00000000E+000
          7  0.00000000E+000  0.00000000E+000
          8  0.00000000E+000  0.00000000E+000
          9  0.00000000E+000  0.00000000E+000
End values

GaussPoints GP Elemtype Quadrilateral
Number of Gauss Points:   1
Natural Coordinates: Internal
End GaussPoints
Result ANIP2 ALYA  0.00000000E+00 Vector OnGaussPoints GP
ComponentNames ANIP2_X,ANIP2_Y,ANIP2_Z
Values
          1  0.00000000E+000  0.00000000E+000
          2  0.00000000E+000  0.00000000E+000
          3  0.00000000E+000  0.00000000E+000
          4  0.00000000E+000  0.00000000E+000
          5  0.00000000E+000  0.20000000E+001
          6  0.00000000E+000  0.00000000E+000
          7  0.00000000E+000  0.00000000E+000
          8  0.00000000E+000  0.00000000E+000
          9  0.00000000E+000  0.00000000E+000
End values

