#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



#BSUB -n 32  
#BSUB -R"span[ptile=16]"
#BSUB -o OUT00.txt
#BSUB -e ERR00.txt
#BSUB -J 2016MAY10  
#BSUB -W 00:29  
# #BSUB -q bsc_case  

ALYA_PATH=/home/bsc21/bsc21704/z2016/REPOSITORY/ALYAs/ALYA_2016Mar12/

ALYAi=1  
ALYAj=4  

#ALYAi=8
#ALYAj=24

ln -s cou.dat  Interior01.cou.dat
ln -s cou.dat    vortex2D.cou.dat
#
time mpirun -np $ALYAj $ALYA_PATH/Executables/unix/Alya.x Interior01 : -np $ALYAi $ALYA_PATH/Executables/unix/Alya.x vortex2D 
#
rm *.cou.dat 

#
#  2016MAY02: CPLNG01_C01 <- CPLNG01_01   
#    
