!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nzecob(&
     nlist,ncoef,nzbou,liste,touch)
  !-----------------------------------------------------------------------
  !
  ! This routine computes the number of non-zero coefficients of a
  ! mesh graph stored in compressed sparse row (CSR) format 
  !                 
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp,lg
  use def_domain, only       :  nnode,nboun,mnodb,lnodb,ltypb
  implicit none
  integer(ip), intent(in)    :: nlist,ncoef
  integer(ip), intent(in)    :: liste(nlist)
  integer(ip), intent(inout) :: nzbou
  logical(lg), intent(inout) :: touch(ncoef)
  integer(ip)                :: jboun,jnode,jpoin,nnodj,jposi,jlist
  integer(ip)                :: kboun,knode,kpoin,nnodk,kposi,klist

  do jlist=1,nlist                                      ! Loop over those elements 
     jboun=liste(jlist)                                 ! where the point is
     nnodj=nnode(ltypb(jboun))
     do jnode=1,nnodj
        jpoin=lnodb(jnode,jboun)
        jposi=(jlist-1)*mnodb+jnode
        if(.not.touch(jposi)) then                      ! Position not touched           
           do klist=1,nlist                             ! Search other elements 
              kboun=liste(klist)                        ! where JPOIN is and 
              nnodk=nnode(ltypb(kboun))
              do knode=1,nnodk                          ! touch their position
                 kpoin=lnodb(knode,kboun)
                 if(kpoin==jpoin) then
                    kposi=(klist-1)*mnodb+knode
                    touch(kposi)=.true.
                 end if
              end do
           end do
           nzbou = nzbou+1
        end if
     end do
  end do

end subroutine nzecob
