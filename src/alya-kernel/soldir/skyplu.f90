!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine skyplu(neqns,lpdof,in_up,in_lo)

!-----------------------------------------------------------------------
!
! This routine obtains the positions IN_UP and IN_LO where the
! upper and lower matrices start.      
!
!-----------------------------------------------------------------------
  use def_kintyp
  implicit none
  integer(ip), intent(in)    :: neqns
  integer(ip), intent(in)    :: lpdof(neqns)
  integer(ip), intent(out)   :: in_up
  integer(ip), intent(inout) :: in_lo

  in_up = 1 + neqns
  in_lo = in_up + lpdof(neqns)

end subroutine skyplu
