#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



project(cavtri03)

if (WITH_MPI_SEQUENTIAL_TESTS)
  add_test(NAME "${PROJECT_NAME}_sequential" COMMAND ${CMAKE_SOURCE_DIR}/test/alya/alya ${CMAKE_CURRENT_SOURCE_DIR} "${CMAKE_CURRENT_BINARY_DIR}/sequential" "${MPIEXEC_EXECUTABLE}" ${MPIEXEC_PREFLAGS} "${CMAKE_BINARY_DIR}/src/alya/alya" ${PROJECT_NAME})
else()
  add_test(NAME "${PROJECT_NAME}_sequential" COMMAND ${CMAKE_SOURCE_DIR}/test/alya/alya ${CMAKE_CURRENT_SOURCE_DIR} "${CMAKE_CURRENT_BINARY_DIR}/sequential" "${CMAKE_BINARY_DIR}/src/alya/alya" ${PROJECT_NAME})
endif()
