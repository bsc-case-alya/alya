!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_solmon()
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_solmon
  ! NAME 
  !    nsi_solmon
  ! DESCRIPTION
  !    This routine solves an iteration for the incompressible NS equations
  !    using a monolitic scheme.
  ! USES
  !    nsi_ifconf
  !    nsi_matrix
  !    Soldir
  !    Solite
  !    nsi_rotunk
  ! USED BY
  !    nsi_solite
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_nastin
  implicit none
  !
  ! Construct the system matrix and right-hand-side
  !
  call nsi_inisol(2_ip)                                 ! Initialize solver
  call nsi_matrix()
  !call nsi_rescon()                                     ! Algebraic residual
  !
  ! Transform from global to local systems 
  !
  call nsi_rotunk(one,unkno)
  !
  ! Solve the algebraic system
  !
  call solver(rhsid,unkno,amatr,pmatr)
  !
  ! Transform from local to global systems 
  !
  call nsi_rotunk(two,unkno)

end subroutine nsi_solmon
