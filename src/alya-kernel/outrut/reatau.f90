!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine reatau(kfl_taust)
  !-----------------------------------------------------------------------
  !****f* outrut/reatau
  ! NAME 
  !    reatau
  ! DESCRIPTION
  !    This routine reads the tau strategy used to compute tau in subroutines ADR_tau(new)  or/and in tauadr (old)
  ! INPUT
  ! OUTPUT
  ! USES
  ! USED BY
  !    ***_reanut
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only      :  ip
  use def_inpout, only      :  words
  implicit none
  integer(ip),  intent(out) :: kfl_taust

  if(words(2)=='OFF  ') then ! No stabilization
     kfl_taust=0 
  else if(words(2)=='CODIN') then ! Codina
     kfl_taust=1
  else if(words(2)=='EXACT') then
     kfl_taust=2
  else if(words(2)=='SHAKI') then
     kfl_taust=3
  else if(words(2)=='INCLU') then ! Codina including DT in TAU
     kfl_taust=5
  else if(words(2)=='TIMES') then ! TAU =DT
     kfl_taust=6
  end if

end subroutine reatau
