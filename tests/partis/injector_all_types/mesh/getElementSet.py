#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import sys

name=sys.argv[1]
with open('{}.dims.dat'.format(name),'r') as f:
    line = f.readline()
    line = f.readline()
    line = line.split()
    nelem = int(line[1])

with open('{}.set.elm'.format(name),'w') as f:
    for i in range(nelem):
        f.write('{} 1\n'.format(i+1))

with open('{}.partfie.dat'.format(name),'w') as f:
	j = 1
	for i in range(nelem):
		f.write('{} {}\n'.format(i+1,j))
		if i > 17:
			j = 2
