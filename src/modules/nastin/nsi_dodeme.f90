!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_dodeme(itask,Auu,Aup,Apu,App,Q,bu,bp)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_dodeme
  ! NAME 
  !    nsi_dodeme
  ! DESCRIPTION
  !    Interfaces Nastin with Dodeme
  !    ITASK = 1 ... Change pressure boundary conditions
  !          = 2 ... Impose Dirichlet b.c.
  !                  Impose hole nodes velocity and pressure to zero
  ! USES
  ! USED BY
  !    nsi_dodeme
  !***
  !----------------------------------------------------------------------- 
  use def_master
  use def_elmtyp
  use def_domain
  use def_solver
  use def_nastin
  implicit none
  integer(ip), intent(in)    :: itask
  real(rp),    intent(inout) :: Auu(ndime,ndime,nzdom) 
  real(rp),    intent(inout) :: Aup(ndime,nzdom)
  real(rp),    intent(inout) :: Apu(ndime,nzdom)
  real(rp),    intent(inout) :: App(*) 
  real(rp),    intent(inout) :: Q(*)
  real(rp),    intent(inout) :: bu(ndime,*),bp(*) 
  real(rp)                   :: qdiag,appdiag,auudiag(3),time1,p,u
  integer(ip)                :: ipoin,ii,idime,ibopo,intij,iboun,jzdom,jpoin,ppoin
  integer(ip)                :: izdom,kpoin,jdime,ielem,inode,imeth,inodb
  integer(ip)                :: isubd,jsubd,itype,pnode,pnodb,pblty


end subroutine nsi_dodeme
