!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!------------------------------------------------------------------------
!> @addtogroup Coupling
!> @{
!> @name    Coupling arrays
!> @file    def_coupli.f90
!> @author  Guillaume Houzeaux
!> @date    03/03/2014
!> @brief   Open coupling files
!> @details Open coupling files
!> @}
!------------------------------------------------------------------------

subroutine cou_openfi()
  use def_parame
  use def_master
  use def_domain
  use def_coupli
  use mod_iofile
  implicit none
  character(150) :: fil_coupl_res,fil_coupl_dat,fil_coupl_cvg

  if( INOTSLAVE ) then

     fil_coupl_dat = adjustl(trim(namda)) // '.cou.dat'
     fil_coupl_res = adjustl(trim(namda)) // '.cou.log'
     fil_coupl_cvg = adjustl(trim(namda)) // '.cou.cvg'

     call iofile(7_ip,lun_coupl_dat,trim(fil_coupl_dat),'COUPLING DATA','old')
     if( .not. file_opened ) lun_coupl_dat = 0

     if( lun_coupl_dat /= 0 ) then
        if( kfl_rstar == 2 ) then
           call iofile(0_ip,lun_coupl_res,fil_coupl_res,'COUPLING RESULTS'    ,'old','formatted','append')
           call iofile(0_ip,lun_coupl_cvg,fil_coupl_cvg,'COUPLING CONVERGENCE','old','formatted','append')
        else
           call iofile(0_ip,lun_coupl_res,fil_coupl_res,'COUPLING RESULTS')
           call iofile(0_ip,lun_coupl_cvg,fil_coupl_cvg,'COUPLING CONVERGENCE')
        end if
     end if

  end if

end subroutine cou_openfi
