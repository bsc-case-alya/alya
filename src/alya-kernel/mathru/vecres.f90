!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine vecres(norm,n,v1,v2,redif,zero)

!-----------------------------------------------------------------------
!
! Compute the relative difference between two vectors:
! 
! redif = ||v1 - v2|| / ||v1||      
!
!-----------------------------------------------------------------------
  use      def_kintyp
  implicit none
  integer(ip), intent(in)  :: n,norm
  real(rp),    intent(in)  :: v1(n),v2(n),zero
  real(rp),    intent(out) :: redif
  integer(ip)              :: i
  real(rp)                 :: numer,denom,va,vo

  redif = 0.0_rp
  numer = 0.0_rp
  denom = 0.0_rp

  select case(norm)

  case(0)
     do i = 1,n
        va = v1(i)
        vo = v2(i)
        numer = max(numer,abs(va-vo))
        denom = max(denom,abs(va))
     end do
     if(denom.gt.zero) redif = numer/denom

  case(1)
     do i = 1,n
        va = v1(i)
        vo = v2(i)
        numer = numer + abs(va-vo)
        denom = denom + abs(va)
     end do
     if(denom>zero) redif = numer/denom

  case(2)
     do i = 1,n
        va = v1(i)
        vo = v2(i)
        numer = numer + (va-vo)*(va-vo)
        denom = denom + va*va
     end do
     if(denom>zero) then
        redif = sqrt(numer/denom)
     else
        redif = 0.0_rp
     end if

  end select

end subroutine vecres
