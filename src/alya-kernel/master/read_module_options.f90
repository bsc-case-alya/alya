!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine read_module_options()
  !------------------------------------------------------------------------
  !****f* kernel/read_module_options
  ! NAME 
  !    read_module_options
  ! DESCRIPTION
  !    This routine reads the generic module data
  ! USES
  ! USED BY
  !    ***_reapro
  !***
  !------------------------------------------------------------------------
  use def_master
  use def_inpout
  use mod_ecoute, only :  ecoute
  implicit none
  character(10) :: messa

  if( INOTSLAVE ) then
     ! 
     ! Reach the section
     !      
     messa=exmod(modul)//'_REAPRO'
     rewind(lisda)
     do while(words(1)/='RUNDA')
        call ecoute(messa)
     end do
     do while(words(1)/='ENDRU')
        call ecoute(messa)
     end do
     do while(words(1)/='PROBL')
        call ecoute(messa)
     end do
     !
     ! Read data
     !      
     do while(words(1)/='ENDPR')
        call ecoute(messa)
        if(words(1)==namod(modul)(1:5)) then   
           if(exists('ON   ').or.exists('ACTIV')) then
              kfl_modul(modul) = 1
              do while(words(1)/='END'//namod(modul)(1:2))
                 call ecoute(messa)
                 if(words(1)=='DELAY') then
                    kfl_delay(modul) = 1
                    ndela(modul)     = int(param(2),ip)
                 else if(words(1)=='CONVE') then
                    if(exists('YES  ').or. exists('ON   ')) kfl_conve(modul) = 1
                    if(exists('NO   ').or. exists('OFF  ')) kfl_conve(modul) = 0
                 else if(words(1)=='SOLVE') then
                    if( words(2) == 'ATEAC' ) kfl_solve(modul) = AT_EACH_TIME_STEP
                    if( words(2) == 'ATBEG' ) kfl_solve(modul) = AT_BEGINNING
                 end if
              end do
           end if
        end if
     end do
  end if

end subroutine read_module_options
