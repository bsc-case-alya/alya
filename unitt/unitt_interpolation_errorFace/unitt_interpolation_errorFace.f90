!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_interpolaiton_errorFace

  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_search_method
  use def_maths_bin
  use def_maths_tree
  use def_maths_skdtree  
  use def_master
  use mod_memory
  use def_kermod
  use def_domain
  use def_interpolation_method
  use mod_maths
  use mod_quality, only: isPointInTet

  implicit none
  type(maths_bin)                 :: bin
  type(maths_octree)              :: oct
  type(interpolation)             :: interp

  integer(ip)                     :: ii,jj,kk,num,nn
  real(rp),             pointer   :: bobox(:,:,:)
  real(rp),             pointer   :: values_in(:)
  real(rp),             pointer   :: values_out(:)
  real(rp),             pointer   :: points_to_interpolate(:,:)
  
  real(rp)                :: dom_origin(3)
  real(rp)                :: dom_length
  real(rp)                :: coordTet(3,4), p(3), coordTetBis(3,4)
  integer(ip)             :: isInside
  type(mesh_type_basic)   :: mesh_simple
    
  logical(lg), parameter :: out_debug    = .false.
  logical(lg), parameter :: do_test_mesh = .false. ! unnecessary, testing bad elem is enough
  logical(lg) :: isFound1, isFound2

  call Initia()                                               ! Initialization of the run
  if(do_test_mesh) then
    call Readom()                                               ! Domain reading
    call Partit()                                               ! Domain partitioning
    call Reaker()                                               ! Read Kermod
    call Domtra()                                               ! Domain transformation
    call Domain()                                               ! Domain construction
  end if
  
  nn = 1

  print*,'nn=',nn
  allocate(points_to_interpolate(3,nn))
  do ii=1,nn
    points_to_interpolate(1:3,ii)=(/-3659.9457918494900_rp,-3616.7709651908694_rp,-11.320876632319466_rp/)
  end do
  
  !open(unit=10,file='./coord.txt',status='old')
  !do ii = 1,nn
  !   read(10,*) kk,points_to_interpolate(1:3,ii)
  !end do

  ! ------------------------------------------------------------------------------------------------------------------
  ! ------------------------------------------------------------------------------------------------------------------
  ! ------------------------------------------------------------------------------------------------------------------
  if(do_test_mesh) then
    print*,'---------------------------------------'
    print*,'------------ORIGINAL MESH--------------'
    print*,'---------------------------------------'
  
    nullify(bobox,values_in,values_out)
    allocate(values_in (meshe(ndivi) % npoin))
    allocate(values_out(nn))
    call meshe(ndivi) % element_bb (bobox)
    do ii = 1,meshe(ndivi) % npoin
       values_in(ii) = 1.0_rp
    end do
    
    do kk = 2,2
       num = kk*10

       call oct    % init      ()
       call oct    % input     (PARAM=(/20.0_rp/))
       call oct    % fill      (BOBOX=bobox)

       call interp % init      ()
       call interp % input     (oct,INTERPOLATION_METHOD=INT_ELEMENT_INTERPOLATION)
       call interp % preprocess(points_to_interpolate,meshe(ndivi))

       print*,'nrows= ',interp % matrix(0) % nrows
       call interp % values    (values_in,values_out)
     
       print*,'nelem: ',meshe(ndivi)%nelem
       do ii = 1,nn
          if( .not. interp % found(ii) ) print*,'not found point= ',ii
          if( abs(values_out(ii)-1.0_rp) > 1.0e-12_rp ) then
             print*,'wrong value ',ii,values_out(ii)
          end if
       end do

       call interp % deallo    ()
       call oct    % deallo    ()

       print*,'  points to find=         ',size(points_to_interpolate,2)
       print*,'  oct % fill=             ',oct % times(1)
       print*,'  oct % max elem=         ',int(oct % stats(4),ip)
       print*,'  interp, candidate=      ',interp % times(1)
       print*,'  interp, element search= ',interp % times(2)
       print*,'  interp, values=         ',interp % times(3)

    end do
  
    deallocate(values_in)
    deallocate(values_out)
    deallocate(bobox)
    
  end if
  ! ------------------------------------------------------------------------------------------------------------------
  ! ------------------------------------------------------------------------------------------------------------------
  ! ------------------------------------------------------------------------------------------------------------------
  if(do_test_mesh) then
    print*,'---------------------------------------'
    print*,'------------RESCALING MESH-------------'
    print*,'---------------------------------------'
    
    dom_length = 0.820046368015E+05_rp
    dom_origin = sum(meshe(ndivi)%coord(:,1:meshe(ndivi)%npoin),2) / meshe(ndivi)%npoin
    
    print*,'dom_origin:',dom_origin
  
    meshe(ndivi)%coord(1,:) = meshe(ndivi)%coord(1,:) - dom_origin(1)
    meshe(ndivi)%coord(2,:) = meshe(ndivi)%coord(2,:) - dom_origin(2)
    meshe(ndivi)%coord(3,:) = meshe(ndivi)%coord(3,:) - dom_origin(3)
  
    meshe(ndivi)%coord = meshe(ndivi)%coord/dom_length
  
    do ii=1,nn
      points_to_interpolate(1:3,ii)=(points_to_interpolate(1:3,ii) - dom_origin)/dom_length
    end do
  
    nullify(bobox,values_in,values_out)
    allocate(values_in (meshe(ndivi) % npoin))
    allocate(values_out(nn))
    call meshe(ndivi) % element_bb (bobox)
    do ii = 1,meshe(ndivi) % npoin
       values_in(ii) = 1.0_rp
    end do
    
    do kk = 2,2
       num = kk*10

       call oct    % init      ()
       call oct    % input     (PARAM=(/20.0_rp/))
       call oct    % fill      (BOBOX=bobox)

       call interp % init      ()
       call interp % input     (oct,INTERPOLATION_METHOD=INT_ELEMENT_INTERPOLATION)
       call interp % preprocess(points_to_interpolate,meshe(ndivi))

       print*,'nrows= ',interp % matrix(0) % nrows
       call interp % values    (values_in,values_out)

       do ii = 1,nn
          if( .not. interp % found(ii) ) print*,'not found point= ',ii
          if( abs(values_out(ii)-1.0_rp) > 1.0e-12_rp ) then
             print*,'wrong value ',ii,values_out(ii)
          end if
       end do

       call interp % deallo    ()
       call oct    % deallo    ()

       print*,'points to find=         ',size(points_to_interpolate,2)
       print*,'oct % fill=             ',oct % times(1)
       print*,'oct % max elem=         ',int(oct % stats(4),ip)
       print*,'interp, candidate=      ',interp % times(1)
       print*,'interp, element search= ',interp % times(2)
       print*,'interp, values=         ',interp % times(3)

    end do
    
    deallocate(values_in)
    deallocate(values_out)
    deallocate(bobox)
  end if
  ! ------------------------------------------------------------------------------------------------------------------
  ! ------------------------------------------------------------------------------------------------------------------
  ! ------------------------------------------------------------------------------------------------------------------
  if(.true.) then
    print*,'---------------------------------------'
    print*,'------------SINGLE TET MESH------------'
    print*,'---------------------------------------'
    
    do ii=1,nn
      points_to_interpolate(1:3,ii)=(/-3659.9457918494900_rp,-3616.7709651908694_rp,-11.320876632319466_rp/)
    end do
  
    call mesh_simple     % init('MY_MESH')
    mesh_simple % nelem = 1_ip
    mesh_simple % npoin = 4_ip
    mesh_simple % ndime = 3_ip
    mesh_simple % mnode = 4_ip

    call mesh_simple % alloca()
    mesh_simple % lnods(:,1)  = (/  1_ip,2_ip,3_ip,4_ip  /)
    mesh_simple % ltype(1)  = TET04
    mesh_simple % coord(:,1 ) = (/  -3659.5695260351499_rp, -3707.6115828050883_rp,  257.62926966922561_rp /)
    mesh_simple % coord(:,2 ) = (/  -3610.1457241751273_rp, -3703.0493052109168_rp, -78.648452311726686_rp /)
    mesh_simple % coord(:,3 ) = (/  -3728.5401069046038_rp, -3532.3933974807724_rp,  14.170012527098363_rp /)
    mesh_simple % coord(:,4 ) = (/  -3644.1583465027929_rp, -3605.5311413336412_rp,  42.639396581581103_rp /)
    
    nullify(bobox,values_in,values_out)
    allocate(values_in (mesh_simple % npoin))
    allocate(values_out(nn))
    call mesh_simple % element_bb (bobox)
    do ii = 1,mesh_simple % npoin
       values_in(ii) = 1.0_rp
    end do

!     do ii = 1,nn
!       coordTet = mesh_simple % coord(:,mesh_simple % lnods(:,1))
!       p        = points_to_interpolate(:,ii)
!       isInside = isPointInTet(p,coordTet,out_debug)
!       print*,'isPointInTet: ',isInside>0_ip,' -> isInside: ',isInside
!     end do
  
    do kk = 2,2
       num = kk*10

       call oct    % init      ()
       call oct    % input     (PARAM=(/20.0_rp/))
       call oct    % fill      (BOBOX=bobox)

       call interp % init      ()
       call interp % input     (oct,INTERPOLATION_METHOD=INT_ELEMENT_INTERPOLATION)
       call interp % preprocess(points_to_interpolate,mesh_simple)

       if(out_debug) print*,'nrows= ',interp % matrix(0) % nrows
       call interp % values    (values_in,values_out)

       do ii = 1,nn
         coordTet = mesh_simple % coord(:,mesh_simple % lnods(:,1))
         p        = points_to_interpolate(:,ii)
         isInside = isPointInTet(p,coordTet,out_debug)
         print*,'Checking hyperplanes => isPointInTet: ',isInside>0_ip,' -> isInside: ',isInside
         
         isFound1 = interp % found(ii)
         if( .not. interp % found(ii) ) then
           print*,'    ======================================================='
           print*,'    ======> Not found point= ',ii,' <========== CACAAAAAAA'
           print*,'    ======================================================='
         else
           print*,'    ======================================================='
           print*,'    ======> Found point    = ',ii,' <========== IUHUUUUUUU'
           print*,'    ======================================================='
         end if
         if( abs(values_out(ii)-1.0_rp) > 1.0e-12_rp ) then
            print*,'wrong value ',ii,values_out(ii)
         end if
       end do

       call interp % deallo    ()
       call oct    % deallo    ()

       if(out_debug) then
         print*,'  points to find=         ',size(points_to_interpolate,2)
         print*,'  oct % fill=             ',oct % times(1)
         print*,'  oct % max elem=         ',int(oct % stats(4),ip)
         print*,'  interp, candidate=      ',interp % times(1)
         print*,'  interp, element search= ',interp % times(2)
         print*,'  interp, values=         ',interp % times(3)
       end if
    end do

    if(out_debug) then
      print*,'Checking difference between found element and single element mesh'
      coordTet    = mesh_simple  % coord(:,mesh_simple  % lnods(:,1    ))
      coordTetBis = meshe(ndivi) % coord(:,meshe(ndivi) % lnods(:,22257))
      do ii=1,4
        print*,'inode tets: ',ii
        print*,'tet1: ',coordTetBis(:,ii)
        print*,'tet2: ',coordTet(:,ii)
        print*,'diff: ',coordTet(:,ii)-coordTetBis(:,ii)
      end do
    end if
    
    call mesh_simple%deallo()
  end if
  ! ------------------------------------------------------------------------------------------------------------------
  ! ------------------------------------------------------------------------------------------------------------------
  ! ------------------------------------------------------------------------------------------------------------------
  print*,'---------------------------------------'
  print*,'-------SINGLE TET MESH RESCALED--------'
  print*,'---------------------------------------'
  if(.true.) then
    do ii=1,nn
      points_to_interpolate(1:3,ii)=(/-3659.9457918494900_rp,-3616.7709651908694_rp,-11.320876632319466_rp/)
    end do
  
    call mesh_simple     % init('MY_MESH')
    mesh_simple % nelem = 1_ip
    mesh_simple % npoin = 4_ip
    mesh_simple % ndime = 3_ip
    mesh_simple % mnode = 4_ip

    call mesh_simple % alloca()
    mesh_simple % lnods(:,1)  = (/  1_ip,2_ip,3_ip,4_ip  /)
    mesh_simple % ltype(1)  = TET04
    mesh_simple % coord(:,1 ) = (/  -3659.5695260351499_rp, -3707.6115828050883_rp,  257.62926966922561_rp /)
    mesh_simple % coord(:,2 ) = (/  -3610.1457241751273_rp, -3703.0493052109168_rp, -78.648452311726686_rp /)
    mesh_simple % coord(:,3 ) = (/  -3728.5401069046038_rp, -3532.3933974807724_rp,  14.170012527098363_rp /)
    mesh_simple % coord(:,4 ) = (/  -3644.1583465027929_rp, -3605.5311413336412_rp,  42.639396581581103_rp /)
    
    dom_length = sqrt(sum((mesh_simple%coord(:,2)-mesh_simple%coord(:,1))**2))
    dom_origin = sum(mesh_simple%coord(:,1:mesh_simple%npoin),2) / mesh_simple%npoin
    
    if(out_debug) print*,'dom_length:',dom_length
    if(out_debug) print*,'dom_origin:',dom_origin
  
    mesh_simple%coord(1,:) = (mesh_simple%coord(1,:) - dom_origin(1))/dom_length
    mesh_simple%coord(2,:) = (mesh_simple%coord(2,:) - dom_origin(2))/dom_length
    mesh_simple%coord(3,:) = (mesh_simple%coord(3,:) - dom_origin(3))/dom_length
  
    do ii=1,nn
      points_to_interpolate(1:3,ii)=(points_to_interpolate(1:3,ii) - dom_origin)/dom_length
    end do
    
    nullify(bobox,values_in,values_out)
    allocate(values_in (mesh_simple % npoin))
    allocate(values_out(nn))
    call mesh_simple % element_bb (bobox)
    do ii = 1,mesh_simple % npoin
       values_in(ii) = 1.0_rp
    end do
    
    do kk = 2,2
       num = kk*10

       call oct    % init      ()
       call oct    % input     (PARAM=(/20.0_rp/))
       call oct    % fill      (BOBOX=bobox)

       call interp % init      ()
       call interp % input     (oct,INTERPOLATION_METHOD=INT_ELEMENT_INTERPOLATION)
       call interp % preprocess(points_to_interpolate,mesh_simple)

       if(out_debug) print*,'nrows= ',interp % matrix(0) % nrows
       call interp % values    (values_in,values_out)

       do ii = 1,nn
         coordTet = mesh_simple % coord(:,mesh_simple % lnods(:,1))
         p        = points_to_interpolate(:,ii)
         isInside = isPointInTet(p,coordTet)
         print*,'Checking hyperplanes => isPointInTet: ',isInside>0_ip,' -> isInside: ',isInside
          
         
         isFound2 = interp % found(ii)
         if( .not. interp % found(ii) ) then
           print*,'    ======================================================='
           print*,'    ======> Not found point= ',ii,' <========== CACAAAAAAA'
           print*,'    ======================================================='
         else
           print*,'    ======================================================='
           print*,'    ======> Found point    = ',ii,' <========== IUHUUUUUUU'
           print*,'    ======================================================='
         end if
         if( abs(values_out(ii)-1.0_rp) > 1.0e-12_rp ) then
            print*,'wrong value ',ii,values_out(ii)
         end if
       end do

       call interp % deallo    ()
       call oct    % deallo    ()

       if(out_debug) then
         print*,'  points to find=         ',size(points_to_interpolate,2)
         print*,'  oct % fill=             ',oct % times(1)
         print*,'  oct % max elem=         ',int(oct % stats(4),ip)
         print*,'  interp, candidate=      ',interp % times(1)
         print*,'  interp, element search= ',interp % times(2)
         print*,'  interp, values=         ',interp % times(3)
       end if
    end do
    
    call mesh_simple%deallo()
  end if

  print*,'---------------------------------------'
  print*,'-------  SUMMARY OF THE TEST   --------'
  print*,'---------------------------------------'
  print*,'Translating and rescaling mod_elemgeo, both strategies find target point'
  print*,'Without doing that in mod_elemgeo, only the rescaled element and point are found.'
  
  if(isFound1.and.isFound2) then
    call runend('O.K.!')
  else
    call runend('Not finding point in mesh in rescaled or original coords.')
  end if

end program unitt_interpolaiton_errorFace
