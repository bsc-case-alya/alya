#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(init_pyjac)
  set(SUNDIALS_INCLUDE_DIR "${SUNDIALS_DIR}/include" CACHE INTERNAL "")
  set(SUNDIALS_LIB_DIR "${SUNDIALS_DIR}/lib64" CACHE INTERNAL "")
  set(PYJAC_LIB_DIR ${CMAKE_SOURCE_DIR}/deps/pyjac_int/lib CACHE INTERNAL "")
endfunction()

function(pyjac)
  if (WITH_PYJAC MATCHES "ON")
    if (NOT PROJECT_NAME MATCHES pyjac_int)
	  add_dependencies(${PROJECT_NAME} pyjac_int)
	  target_link_libraries(${PROJECT_NAME} pyjac_int)
	  add_definitions(-DPYJAC=1)
    endif()
    include_directories(${SUNDIALS_INCLUDE_DIR})
    link_directories(${SUNDIALS_LIB_DIR})
    link_directories(${PYJAC_LIB_DIR})
    target_link_libraries(${PROJECT_NAME} ${PYJAC_LIB_DIR}/libc_pyjac.a ${SUNDIALS_LIB_DIR}/libsundials_cvodes.so ${SUNDIALS_LIB_DIR}/libsundials_nvecserial.so )
  endif()
endfunction()
