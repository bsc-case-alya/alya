!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup mpio
!> @{
!> @file    mod_mpio_postpr.f90
!> @author  Damien Dosimont
!> @date    27/09/2017
!> @brief   MPI-IO post process
!> @details This module is a bridge that redirects to the sequential or the parallel
!>          versions of the post process parallel I/O operations
!-----------------------------------------------------------------------

module mod_mpio_postpr

  use def_master
  use mod_mpio_seq_postpr
  use mod_mpio_par_postpr

  implicit none

  private

  public :: posmpio_real_v, posmpio_real_m, posmpio_int_v, posmpio_int_m
  
contains

  subroutine posmpio_real_v()
    if (ISEQUEN) then
       call seq_posmpio_real_v()
    else
       call par_posmpio_real_v()
    end if
  end subroutine posmpio_real_v

  subroutine posmpio_real_m()
    if (ISEQUEN) then
       call seq_posmpio_real_m()
    else
       call par_posmpio_real_m()
    end if
  end subroutine posmpio_real_m

  subroutine posmpio_int_v()
    if (ISEQUEN) then
       call seq_posmpio_int_v()
    else
       call par_posmpio_int_v()
    end if
  end subroutine posmpio_int_v

  subroutine posmpio_int_m()
    if (ISEQUEN) then
       call seq_posmpio_int_m()
    else
       call par_posmpio_int_m()
    end if
  end subroutine posmpio_int_m

end module mod_mpio_postpr
!> @}
