!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_projection
  !
  !   Target       Source
  ! 5 o----o 6  11 o----o 12
  !   |    |       |    |
  !   |    |       |    |
  ! 3 o----o 4   9 o----o 10
  !   |    |       |    |
  !   |    |       |    |
  ! 1 o----o 2   7 o----o  8
  !             
  !  mass(2,4,6) = 1,2,2
  !
  !
  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_maths_bin
  use def_maths_tree
  use def_maths_octbin
  use def_master
  use mod_memory
  use def_kermod
  use def_domain
  use def_interpolation_method
  use def_projection_method
  use mod_bouder
  use mod_parall
  use mod_communications_global
  
  implicit none
  real(rp),             parameter :: epsil = 1.0e-12_rp
  type(interpolation),  target    :: interp
  type(projection)                :: projec
  type(maths_bin)                 :: bin
  type(maths_bin)                 :: bin_par
  type(bmsh_type_basic), pointer  :: mesh_target
  type(bmsh_type_basic), pointer  :: mesh_source
  integer(ip)                     :: ii,iboun,idime
  logical(lg),          pointer   :: mask(:)
  real(rp),             pointer   :: bobox(:,:,:)
  real(rp),             pointer   :: subox(:,:,:)
  real(rp),             pointer   :: rr(:)
  real(rp),             pointer   :: yy(:)
  integer(8)                      :: memor_loc(2)

  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction

  nullify(bobox,mask,yy,rr)

  if( nboun > 0 ) allocate(mask(nboun))
  do iboun = 1,nboun
     if( kfl_codbo(iboun) == 1 ) then
        mask(iboun) = .false.
     else
        mask(iboun) = .true.
     end if
  end do
  !
  ! Boundary mesh
  !
  mesh_source => meshe(ndivi) % boundary
  mesh_target => meshe(ndivi) % boundary
  
  call mesh_source % element_bb   (bobox)
  !
  ! Sequential bin
  !
  call bin  % init         ()
  call bin  % input        (boxes=(/10_ip,10_ip/)) 
  call bin  % fill         (BOBOX=bobox,MASK=mask)
  !
  !  Parallel bin
  !
  if( IPARALL ) then
     allocate(subox(2,ndime,0:npart))
     subox = 0.0_rp
     if( inotmaster .and. associated(bobox) ) then
        do idime = 1,ndime
           subox(1,idime,kfl_paral) = minval(bobox(1,idime,:))
           subox(2,idime,kfl_paral) = maxval(bobox(2,idime,:))
        end do
     else 
        subox(1,:,kfl_paral) =  1.0e16_rp
        subox(2,:,kfl_paral) = -1.0e16_rp
     end if
     call PAR_SUM(subox,INCLUDE_ROOT=.true.)
     call bin_par % init      ()
     call bin_par % input     (boxes=(/10_ip,10_ip,1_ip/),name = 'BIN PAR')
     call bin_par % fill      (BOBOX=subox)
  end if
  !
  ! Interpolation strategy
  !
  if( IPARALL ) then
     call interp % init       ()
     call interp % input      (bin,bin_par,&
          &                   COMM=PAR_COMM_WORLD,&
          &                   INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION,&
          &                   NAME='INTERP')
  else
     call interp % init       ()
     call interp % input      (bin,&
          &                   INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION,&
          &                   NAME='INTERP')     
  end if
  !
  ! Counter number of gauss points and nodes
  !
  if( associated(mask) ) mask = .not. mask
  call projec % init       ()
  call projec % input      (BOUNDARY_PROJECTION,interp,elmar,mesh_source,mesh_target,COMM=commd)
  call projec % preprocess (mask)
  !
  ! Allocate
  !
  call memory_alloca(memor_loc,'RR','unitt_projection',rr,npoin)
  call memory_alloca(memor_loc,'YY','unitt_projection',yy,npoin)
  !
  ! Compute values
  !
  do ii = 1,npoin
     if( lninv_loc(ii) == 7 .or. lninv_loc(ii) == 9 .or. lninv_loc(ii) == 11 ) then
        rr(ii) = 2.0_rp
     else
        rr(ii) = 0.0_rp
     end if
  end do

  call projec % values(rr,yy)
  !
  ! Check
  !
  do ii = 1,npoin
     print*,'projected solution=',lninv_loc(ii),yy(ii)
     if( lninv_loc(ii) == 2 .or. lninv_loc(ii) == 4 .or. lninv_loc(ii) == 6 ) then
        if( abs(yy(ii)-2.0_rp) > 1.0e-12_rp ) stop 1
     end if
  end do

  call bin    % deallo()
  call interp % deallo()
  call projec % deallo()
  if( projec % memor(1) /= 0_8 ) stop 2
  if( bin    % memor(1) /= 0_8 ) stop 3
  if( interp % memor(1) /= 0_8 ) stop 4
  call runend('O.K.!')

end program unitt_projection
 
