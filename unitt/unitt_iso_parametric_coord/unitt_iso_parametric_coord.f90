!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_iso_parametric_coord
  !
  ! Order strings 
  !
  use def_kintyp_basic
  use def_domain,   only : ndime
  use mod_elmgeo,   only : elmgeo_element_type_initialization
  use def_master,   only : zeror
  use mod_lagrange, only : shape3
  implicit none

  integer(ip), parameter   :: pnode=64
  real(rp)                 :: shapf(pnode),x(3)
  integer(ip)              :: inode,ii,jj,kk,nn
  integer(ip)              :: list(pnode)
  
#ifndef NDIMEPAR
  ndime = 3
#endif

  call elmgeo_element_type_initialization()

  list = 0
  nn = nint(real(pnode,rp)**(1.0_rp/3.0_rp),ip)
  print*,'nb nodes in each direction=',nn
  do ii = 1,nn
     x(1) = -1.0_rp + 2.0_rp*real(ii-1,rp)/real(nn-1,rp)
     do jj = 1,nn
        x(2) = -1.0_rp + 2.0_rp*real(jj-1,rp)/real(nn-1,rp)
        do kk = 1,nn
           x(3) = -1.0_rp + 2.0_rp*real(kk-1,rp)/real(nn-1,rp)
           call shape3(x,pnode,shapf)
           do inode = 1,pnode
              if( abs(shapf(inode)-1.0_rp)<1.0e-03_rp ) then
                 write(*,'(a,i2,a,f24.21,a,f24.21,a,f24.21,a)') '(1:3,',inode,')=(/ ',x(1),'_rp, ',x(2),'_rp, ',x(3),'_rp /)'
                 if(list(inode) == 1) stop 2
                 list(inode)=1
              end if
           end do
        end do
     end do
  end do
  
  do inode = 1,pnode
     if(list(inode)==0) print*,'lost=',inode
  end do
  
end program unitt_iso_parametric_coord
