!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine mergli( lista, lsize, nodes, nnode, me )
!-----------------------------------------------------------------------
!****f* Domain/mergli
! NAME
!    mergli
! DESCRIPTION
!    This routine merges to list of nodes
! OUTPUT
!    LISTA
!    LSIZE
! USED BY
!    domgra
!***
!-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,lg
  implicit none
  integer(ip), intent(inout) :: lsize,lista(*)
  integer(ip), intent(in)    :: nnode,me
  integer(ip), intent(in)    :: nodes(nnode)
  integer(ip)                :: ii, jj, n1, n2
  logical(lg)                :: noEncontrado

  if( me < 0 ) then
    do ii = 1,nnode
      n1 = nodes(ii)
      jj = 1
      noEncontrado = .true.
      do while( jj <= lsize .and. noEncontrado )
        n2 = lista(jj)
        if ( n1 == n2 ) then
          noEncontrado = .false.
        end if
        jj = jj + 1
      end do
      if ( noEncontrado ) then
        lsize = lsize + 1
        lista(lsize) = n1
      end if
    end do

  else

    do ii = 1, nnode
      n1 = nodes(ii)
      if( n1 /= me ) then
        jj = 1
        noEncontrado = .true.
        do while( jj <= lsize .and. noEncontrado )
          n2 = lista(jj)
          if( n1 == n2 ) then
            noEncontrado = .false.
          end if
          jj = jj + 1
        end do
        if( noEncontrado ) then
          lsize = lsize + 1
          lista(lsize) = n1
        end if
      end if
    end do
  end if

end subroutine mergli
