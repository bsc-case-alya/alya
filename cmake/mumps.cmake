#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(init_mumps)
  set(MUMPS_INCLUDE_DIR "${MUMPS_DIR}/include" CACHE INTERNAL "MUMPS: include directory" FORCE)
  set(MUMPS_LIB_DIR "${MUMPS_DIR}/lib" CACHE INTERNAL "MUMPS: library directory" FORCE)
endfunction()

function(define_mumps)
  if (NOT WITH_MUMPS MATCHES "OFF")
    add_definitions(-DMUMPS)
  endif()
endfunction()

function(link_mumps)
  if (WITH_MUMPS MATCHES "auto")
    #set(MUMPS_HOME "${CMAKE_BINARY_DIR}/auto/mumps")
    #add_dependencies(${PROJECT_NAME} mumps)
    #add_definitions(-DMUMPS)
    #include_directories(${MUMPS_INCLUDE_DIR})
    #target_link_libraries(${PROJECT_NAME} ${MUMPS_HOME}/lib/libblas.a)
    #target_link_libraries(${PROJECT_NAME} ${MUMPS_HOME}/lib/libdmumps.a)
    #target_link_libraries(${PROJECT_NAME} ${MUMPS_HOME}/lib/liblapack.a)
    #target_link_libraries(${PROJECT_NAME} ${MUMPS_HOME}/lib/libmumps_common.a)
    #target_link_libraries(${PROJECT_NAME} ${MUMPS_HOME}/lib/libpord.a)
    #target_link_libraries(${PROJECT_NAME} ${MUMPS_HOME}/lib/libsmumps.a)
    #set_target_properties(${PROJECT_NAME} PROPERTIES INSTALL_RPATH "${MUMPS_HOME}/lib")
    #set_target_properties(${PROJECT_NAME} PROPERTIES BUILD_WITH_INSTALL_RPATH ON)
  elseif (WITH_MUMPS MATCHES "external")
    include(metis)
    link_metis()
    include(openmp)
    target_compile_flags_openmp()
    target_link_flags_openmp()
    include_directories(${MUMPS_INCLUDE_DIR})
    find_library(LIBDMUMPS dmumps PATHS ${MUMPS_LIB_DIR})
    find_library(LIBMUMPS_COMMON mumps_common PATHS ${MUMPS_LIB_DIR})
    find_library(LIBPORD pord PATHS ${MUMPS_LIB_DIR})
    target_link_libraries(${PROJECT_NAME} ${LIBDMUMPS})
    target_link_libraries(${PROJECT_NAME} ${LIBMUMPS_COMMON})
    target_link_libraries(${PROJECT_NAME} ${LIBPORD})
    target_link_libraries(${PROJECT_NAME} hwloc)
  endif()
endfunction()
