!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_brotyp(itask)
  !-------------------------------------------------------------------------------
  !****f* Parall/par_brotyp
  ! NAME
  !    par_brotyp
  ! DESCRIPTION
  !    Broadcast PAI1P(PARD1) array of type(i1p)
  ! INPUT
  !    PAI1P from master
  ! OUTPUT
  ! USED BY
  !    par_partit
  !***
  !-------------------------------------------------------------------------------
  use def_parame 
  use def_domain 
  use def_parall
  use def_master
  use mod_memchk
  use mod_parall, only : par_memor
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipari,nsize_loc,ii,kk
  integer(4)              :: istat
  integer(ip), pointer    :: lsize_loc(:),lai1p_loc(:)

  nparr = 0
  npari = 0
  nparc = 0

  select case(itask)

  case(1_ip)

     !-------------------------------------------------------------------
     ! 
     ! Type(1ip)
     !
     !-------------------------------------------------------------------

     allocate( lsize_loc(pard1),stat=istat)
     call memchk( zero, istat, par_memor, 'LSIZE_LOC', 'par_brotyp', lsize_loc )
     ! 
     ! Master: LAI1P (int) <= PAI1P (type)
     !
     if( IMASTER ) then
        nsize_loc = 0
        do ipari = 1,pard1
           lsize_loc(ipari) = size(pai1p(ipari)%l)
           nsize_loc = nsize_loc + lsize_loc(ipari)
        end do

        allocate( lai1p_loc(nsize_loc),stat=istat)
        call memchk( zero, istat, par_memor, 'LAI1P_LOC', 'par_brotyp', lai1p_loc )
        kk = 0
        do ipari = 1,pard1
           do ii = 1,lsize_loc(ipari)
              kk = kk + 1
              lai1p_loc(kk) = pai1p(ipari)%l(ii) 
           end do
        end do
     end if

     npari =  pard1
     parin => lsize_loc
     strin =  'LSIZE_LOC'
     call par_broadc()  

     if( ISLAVE ) then
        nsize_loc = 0
        do ipari = 1,pard1
           nsize_loc = nsize_loc + lsize_loc(ipari)
        end do
        allocate( lai1p_loc(nsize_loc),stat=istat)
        call memchk( zero, istat, par_memor, 'LAI1P_LOC', 'par_brotyp', lai1p_loc )
     end if

     npari =  nsize_loc
     parin => lai1p_loc
     strin =  'LAI1P_LOC'
     call par_broadc()  

     if( ISLAVE ) then
        kk = 0
        do ipari = 1,pard1
           allocate( pai1p(ipari)%l(lsize_loc(ipari)),stat=istat)
           call memchk( zero, istat, par_memor, 'PAI1P', 'par_brotyp', pai1p(ipari)%l )
           do ii = 1,lsize_loc(ipari)
              kk = kk + 1
              pai1p(ipari)%l(ii) = lai1p_loc(kk) ! PAI1P <= LAI1P
           end do
        end do
     end if

     call memchk( two, istat, par_memor, 'LAI1P_LOC','par_brotyp', lai1p_loc)
     deallocate( lai1p_loc, stat=istat )
     if(istat/=0) call memerr( two, 'LAI1P_LOC', 'par_brotyp',0_ip)

     call memchk( two, istat, par_memor, 'LSIZE_LOC','par_brotyp', lsize_loc)
     deallocate( lsize_loc, stat=istat )
     if(istat/=0) call memerr( two, 'LSIZE_LOC', 'par_brotyp',0_ip)

  end select

end subroutine par_brotyp

