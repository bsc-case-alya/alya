if [ $# -eq 0 ]
then
  export BASEDIR=.
else
  export BASEDIR=$1
fi

export MUMPSDIR=${BASEDIR}/MUMPS_5.4.0

export MUMPSZIP=MUMPS_5.4.0.tar.gz

CURRENTDIR=`pwd`


rm -fr $MUMPSDIR
mkdir -p $BASEDIR

cd $BASEDIR
wget http://mumps.enseeiht.fr/$MUMPSZIP
tar -xf $MUMPSZIP
