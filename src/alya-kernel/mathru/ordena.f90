!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine ordena(n,v)

!-----------------------------------------------------------------------
!****f* Mathru/ordena
! NAME 
!    nsi_reabcs
! DESCRIPTION
!    This routine orders array v cording to its first column
! USES
! USED BY
!    nsi_reabcs
!***
!-----------------------------------------------------------------------
  use       def_kintyp
  implicit   none
  integer(ip), intent(in)    :: n
  real(rp),    intent(inout) :: v(2,n)
  integer(ip)                :: i,j
  real(rp)                   :: v1,v2 

  do j=2,n
     v1=v(1,j)
     v2=v(2,j)
     do i=j-1,1,-1
        if(v(1,i)<=v1) go to 30
        v(1,i+1)=v(1,i)
        v(2,i+1)=v(2,i)
     end do
     i=0
30   v(1,i+1)=v1
     v(2,i+1)=v2
  end do

end subroutine ordena
