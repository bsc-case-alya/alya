!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Coupling
!> @{
!> @file    coupling_destructor.f90
!> @author  houzeaux
!> @date    2019-06-11
!> @brief   Destroy coupling
!> @details Reinitialize all coupling arrays
!> @} 
!-----------------------------------------------------------------------

subroutine coupli_destructor()

  use def_kintyp,          only : ip
  use mod_coupling_memory, only : COU_DEALLOCATE_SINGLE_COUPLING
  use def_coupli,          only : mcoup
  use def_coupli,          only : coupling_type
  implicit none

  integer(ip) :: icoup 

  do icoup = 1,mcoup
     call COU_DEALLOCATE_SINGLE_COUPLING(coupling_type(icoup)) 
  end do
  
end subroutine coupli_destructor
