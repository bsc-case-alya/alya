#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import sys
meshName=sys.argv[1]
geofile='{}.geo.dat'.format(meshName)
coord='{}.coord'.format(meshName)

f=open(geofile,'r')
g=open(coord,'w')


line = True
inCoord = False

boundMax=[float('-Inf'),float('-Inf'),float('-Inf')]
boundMin=[float('Inf' ),float('Inf' ),float('Inf' )]


while line:
	line = f.readline()
	
	if inCoord:
		if 'END' in line:
			if boundMin[2] == float('Inf' ):
				boundMin[2] = 0.0
			if boundMax[2] == float('-Inf'):
				boundMax[2] = 0.0
			print('--| Bounding Box: ({},{},{}),({},{},{})'.format(boundMin[0],boundMin[1],boundMin[2],boundMax[0],boundMax[1],boundMax[2]))
			print('--| End writing coordinates')
			inCoord = False
			line = False
		else:
			data = line.split()
			for i in range(len(data)-1):
				boundMax[i] = max(boundMax[i],float(data[i+1])) 
				boundMin[i] = min(boundMin[i],float(data[i+1])) 
			g.write(line)

	if line:
		if 'COORD' in line:
			inCoord = True
			print('--| Start writing coordinates')


f.close()
g.close()

