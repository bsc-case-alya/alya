#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(init_cantera)
  set(CANTERA_INCLUDE_DIR "${CANTERA_DIR}/include" CACHE INTERNAL "")
  set(CANTERA_INCLUDE_CANTERA_DIR "${CANTERA_INCLUDE_DIR}/cantera" CACHE INTERNAL "")
  set(CANTERA_LIB_DIR ${CANTERA_DIR}/lib CACHE INTERNAL "")
endfunction()

function(cantera)
  if (WITH_CANTERA MATCHES "external")
    if (NOT PROJECT_NAME MATCHES cantera_fwrapper)
      add_dependencies(${PROJECT_NAME} cantera_fwrapper)
      add_definitions(-DCANTERA=1)
      target_link_libraries(${PROJECT_NAME} cantera_fwrapper)
    endif()
    include_directories(${CANTERA_INCLUDE_DIR} ${CANTERA_INCLUDE_CANTERA_DIR})
    link_directories(${CANTERA_LIB_DIR})
    if (CANTERA_DIR STREQUAL "")
      target_link_libraries(${PROJECT_NAME} libcantera_fortran.a libcantera.a)
    else()
      target_link_libraries(${PROJECT_NAME} ${CANTERA_LIB_DIR}/libcantera_fortran.a ${CANTERA_LIB_DIR}/libcantera.a)
      endif()
  endif()
endfunction()
