!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Domain
!> @{
!> @file    mod_rulepw.f90
!> @author  houzeaux
!> @date    2020-04-04
!> @brief   Iso-parametric arrays
!> @details An n-point Gaussian quadrature rule is a quadrature rule constructed 
!>          to yield an exact result for polynomials of degree 2n-1
!>          POSGP(NDIME,NGAUS) ... Local coordinates of Gauss points
!>          WEIGP(NGAUS) ......... Weights of Gauss points
!-----------------------------------------------------------------------

module mod_rulepw

  use def_kintyp_basic,           only : ip,rp
  use def_quadrature,             only : set_quadrature

  implicit none
  private

  interface rulepw
     module procedure &
          rulepw_scalar,&
          rulepw_vector
  end interface rulepw
  
  public :: rulepw
    
contains

  subroutine rulepw_scalar(ndime,ngaus,topo,type,posgp,weigp,ierro)

    integer(ip),           intent(in)    :: ndime
    integer(ip),           intent(in)    :: ngaus
    integer(ip),           intent(in)    :: topo
    integer(ip),           intent(in)    :: type
    real(rp),              intent(out)   :: posgp(max(1_ip,ndime))
    real(rp),              intent(out)   :: weigp
    integer(ip),           intent(inout) :: ierro
    real(rp)                             :: weigp_vec(1)
    real(rp)                             :: posgp_vec(max(1_ip,ndime),1)

    call rulepw_vector(ndime,ngaus,topo,type,posgp_vec,weigp_vec,ierro)

    posgp = posgp_vec(:,1)
    weigp = weigp_vec(1)
    
  end subroutine rulepw_scalar
  
  subroutine rulepw_vector(ndime,ngaus,topo,type,posgp,weigp,ierro)

    integer(ip),           intent(in)    :: ndime
    integer(ip),           intent(in)    :: ngaus
    integer(ip),           intent(in)    :: topo
    integer(ip),           intent(in)    :: type
    real(rp),              intent(out)   :: posgp(max(1_ip,ndime),ngaus)
    real(rp),              intent(out)   :: weigp(ngaus)
    integer(ip),           intent(inout) :: ierro

    ierro = 0
    posgp = 0.0_rp
    weigp = 0.0_rp

    call set_quadrature(ndime,ngaus,topo,type,posgp,weigp,ierro)

  end subroutine rulepw_vector

end module mod_rulepw
!> @}
