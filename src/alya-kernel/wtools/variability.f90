!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup CPU_Time
!> @{
!> @file    variability.f90
!> @author  houzeaux
!> @date    2019-04-02
!> @brief   Variability
!> @details Subroutine used for variability study
!> @} 
!-----------------------------------------------------------------------

subroutine variability(time1)

  use def_kintyp,         only : ip,rp
  use mod_communications, only : PAR_MAX
  use mod_communications, only : PAR_AVERAGE
 implicit none
  real(rp), intent(in) :: time1
  integer(ip)          :: rank_max
  real(rp)             :: timem,timea,timei,timef,timed
  integer(ip), save    :: ipass = 0

  call cputim(timei)
  timem = time1
  timea = time1
  call PAR_MAX(timem,rank_max_owner=rank_max)
  call PAR_AVERAGE(timea)

  call cputim(timef)
  timed = timef-timei
  timei = timef
  if( ipass > 0 ) write(90,10) timed,timea,timem,rank_max

  ipass = ipass + 1

10 format(3(1x,e13.6),i4)
  
end subroutine variability
