!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine mixmax(slope,f,g,value)
  !-----------------------------------------------------------------------
  !****f* mathru/mixing
  ! NAME
  !   mixing
  ! DESCRIPTION
  !   Mix two functions f and g
  ! OUTPUT 
  !   WEIGH
  ! USES
  ! USED BY
  !***
  !-----------------------------------------------------------------------
  use def_kintyp 
  implicit none
  real(rp),    intent(in)  :: slope,f,g
  real(rp),    intent(out) :: value
  real(rp)                 :: H

  if(g==0.0_rp) then
     value  = f
  else if(f==0.0_rp) then
     value  = g
  else
     value  = f/g 
     H      = 0.5_rp*(tanh(slope*(value-1.0_rp))+1.0_rp)
     value  = f*H+(1.0_rp-H)*g
  end if

end subroutine mixmax
