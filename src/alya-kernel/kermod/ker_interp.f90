!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup kermod
!> @{
!> @file    ker_interp.f90
!> @author  houzeaux
!> @date    2019-06-17
!> @brief   Array redistribution
!> @details Array redistribution and reallocation in case values are
!>          not needed
!> @} 
!-----------------------------------------------------------------------

subroutine ker_interp()
  
  use def_master
  use def_domain
  use def_kermod
  use mod_parall,        only : par_memor
  use mod_ker_proper,    only : ker_proper_allocate_properties
  use mod_ker_arrays,    only : ker_arrays
  use mod_ker_updpro,    only : ker_updpro
  use def_AMR
  use mod_AMR_interpolate
  implicit none
  !
  ! Boundary conditions
  !
  call ker_inibcs()
  call AMR_interpolate(walld,'NPOIN',MEMOR=par_memor,VARIABLE_NAME='WALLD')
  call AMR_interpolate(walln,'NPOIN',MEMOR=par_memor,VARIABLE_NAME='WALLN')
  !
  ! Arrays
  !
  call ker_arrays('INTERPOLATE')
  !
  ! Reallocate properties and recompute them
  !
  call ker_proper_allocate_properties()
  call ker_updpro()

end subroutine ker_interp

