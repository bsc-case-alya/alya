!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_concou
!-----------------------------------------------------------------------
!****f* Nastin/nsi_concou
! NAME 
!    nsi_concou
! DESCRIPTION
!    This routine checks the Nastin convergence of the run and
!    set the general convergence flags.
! USED BY
!    Nastin
!***
!-----------------------------------------------------------------------
  use      def_master
  use      def_nastin
  use mod_outfor, only : outfor
  implicit none
  !
  ! Check convergence
  !
  glres(modul) = max(resid_nsi,resip_nsi)
  if( kfl_conve(modul) == 1 ) then
     if( glres(modul) > cotol_nsi ) kfl_gocou = 1
  end if   
  !
  ! Output residuals
  !
  coutp(1) = 'VELOCITY'
  routp(1) = resid_nsi
  call outfor(9_ip,lun_outpu,' ')
  coutp(1) = 'PRESSURE'
  routp(1) = resip_nsi
  call outfor(9_ip,lun_outpu,' ')

  call nsi_coupli(ITASK_CONCOU) 

end subroutine nsi_concou
