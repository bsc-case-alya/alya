#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import os

def rewriteField(name,val):
	oldname = '{}.alya'.format(name)
	newname = '{}.new.alya'.format(name)
	
	fOld = open(oldname,'r')
	fNew = open(newname,'w')
	
	line = True
	
	while line:
		line = fOld.readline()
		data = line.split()
		if len(data) > 1:
			ip  = int(data[0])
			
			output = '{}'.format(ip)
			for v in val:
				output += ' {}'.format(v)
			output += '\n'

			fNew.write(output)
	
	fOld.close()
	fNew.close()
	
	command = 'mv {0} {1}'.format(newname, oldname)
	os.system(command)


def rewriteH(HZ0, HZ1):
	oldname = 'CON03.alya'
	newname = 'ENTHA.alya'
	
	fOld = open(oldname,'r')
	fNew = open(newname,'w')
	
	line = True
	
	while line:
		line = fOld.readline()
		data = line.split()
		if len(data) > 1:
			ip  = int(data[0])
			Z   = float(data[1])

			output = '{}'.format(ip)
			output += ' {}'.format( HZ0 + (HZ1-HZ0) * Z )
			output += '\n'

			fNew.write(output)
	
	fOld.close()
	fNew.close()
	

def rewriteYc(Zst, Ycst):
	zname = 'CON03.alya'
	ycname = 'CON01.alya'
	
	fz = open(zname,'r')
	fyc = open(ycname,'w')
	
	line = True
	
	while line:
		line = fz.readline()
		data = line.split()
		if len(data) > 1:
			ip  = int(data[0])
			Z   = float(data[1])

			if Z <= Zst:
			    eta = Z/Zst
			    yc = eta * Ycst
			else:
			    eta = (1.0-Z)/(1.0-Zst)
			    yc = eta * Ycst

			output = '{}'.format(ip)
			output += ' {}'.format( yc )
			output += '\n'

			fyc.write(output)
	
	fz.close()
	fyc.close()


rewriteYc(0.055,0.025)


