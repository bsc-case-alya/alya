#!/bin/bash
#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



#This is how it's meant to be executed
../Alya.g --write-rst --time-steps 3 activation_restart
cp activation_restart.exm.dat activation_restart.exm.dat.bak
sed -i 's/0.35 0.0 1.0 0.15 0.0/0.35 0.3 1.0 0.15 2.0e-4/g' activation_restart.exm.dat
../Alya.g --read-rst --time-steps 6 activation_restart
cp activation_restart.exm.dat.bak activation_restart.exm.dat

