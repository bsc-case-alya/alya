!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_out_res_eocoe(xx,nsize,ipass_aux)
  !
  ! outputs results for eocoe project
  !
  use def_kintyp
  implicit none

  integer(ip), intent(in)    :: nsize
  real(rp),    intent(in)    :: xx(nsize)
  integer(ip), intent(in)    :: ipass_aux

  integer(ip)        :: i
  character(50)      :: char_aux


  write(char_aux,'(i1)')ipass_aux
  char_aux='result'//trim(char_aux)//'.txt'
  open(777,file=char_aux)
  ! xx
  write(777,'(i15)')nsize
  do i=1,nsize
     write(777,'(e17.10)')xx(i)
  end do
  close(777)

 end subroutine nsi_out_res_eocoe
