#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(init_maphys)
  if (WITH_MAPHYS MATCHES "ON")
    find_package(PkgConfig REQUIRED)
    set(PKG_CONFIG_USE_CMAKE_PREFIX_PATH ON)
    #list(APPEND CMAKE_PREFIX_PATH "${MAPHYS_DIR}/lib/pkgconfig")
    set(ENV{PKG_CONFIG_PATH} "$ENV{PKG_CONFIG_PATH}:${MAPHYS_DIR}/lib/pkgconfig")
    if(NOT MAPHYS_FOUND)
      # Seach for maphys using pkg-config
      if(PKG_CONFIG_FOUND AND PKG_CONFIG_EXECUTABLE)
        message(STATUS "Looking for MAPHYS - found using PkgConfig")
        pkg_search_module(MAPHYS maphys)
      endif()
      if(MAPHYS_FOUND)
        message(STATUS "Found MAPHYS")
      else()
        message(FATAL_ERROR "MAPHYS NOT FOUND")
      endif()
    endif()
  endif()
endfunction()

function(define_maphys)
  if (WITH_MAPHYS MATCHES "ON")
    add_definitions(-DMAPHYS)
  endif()
endfunction()

function(include_maphys)
  if (WITH_MAPHYS MATCHES "ON")
    STRING(REPLACE modules include MAPHYS_INCLUDE_DIRS2 ${MAPHYS_INCLUDE_DIRS})
    include_directories(${PROJECT_NAME} ${MAPHYS_INCLUDE_DIRS2})
    include_directories(${PROJECT_NAME} ${MAPHYS_INCLUDE_DIRS})
  endif()
endfunction()

function(link_maphys)
  if (WITH_MAPHYS MATCHES "ON")
    foreach(MAPHYS_LIBRARY ${MAPHYS_LIBRARIES})
      find_library(${MAPHYS_LIBRARY}_FULL ${MAPHYS_LIBRARY} PATHS ${MAPHYS_LIBRARY_DIRS})
      target_link_libraries(${PROJECT_NAME} ${${MAPHYS_LIBRARY}_FULL})
    endforeach()
  endif()
endfunction()
