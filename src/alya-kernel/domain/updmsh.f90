!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine updmsh(itask)
!-----------------------------------------------------------------------
!****f* domain/updmsh
! NAME
!    updmsh
! DESCRIPTION
!    This routine performs update of mesh data. Depending on the 
!    refinement strategy, some data is saved (to be used in the
!    generation of the new mesh and projections) and some arrays
!    are deallocated (depending if they change or not)
! USES
! USED BY
!    Newmsh
!***
!-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use mod_memchk
  implicit none
  integer(ip) :: itask,inode,inodb
  integer(ip) :: ielem,ielty,iboun
  integer(4)  :: istat 


end subroutine updmsh
