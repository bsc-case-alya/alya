!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine extcog(iboun,ielem,pnodb,pnode,exwor)
  !-----------------------------------------------------------------------
  !
  ! This routine calculates the normal baloc at the center of
  ! gravity of the boundary element and assign it to exwor.
  ! The orientation is not a problem as the point belongs to
  ! two adjacent surfaces.      
  !   
  !   exwor <- baloc
  !                 
  !    /|\     /|\      
  !     |       |
  !     o-------+-------o
  !           iboun
  !   
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_domain, only     :  ndime,ndimb,ntens,mnode,mnodb,coord,lnods,&
       &                      lnodb,ltypb,elmar
  use mod_bouder
  implicit  none
  integer(ip), intent(in)  :: iboun,ielem,pnodb,pnode
  real(rp),    intent(out) :: exwor(ndime)
  integer(ip)              :: idime,inodb,ipoin,iblty,inode
  real(rp)                 :: xnorm,eucta
  real(rp)                 :: baloc(ndime,ndime)
  real(rp)                 :: bocod(ndime,pnodb),elcod(ndime,mnode)
  !
  ! Identifies the coordinates of the boundary nodes
  !
  iblty=ltypb(iboun)
  do inodb=1,pnodb
     ipoin=lnodb(inodb,iboun)
     do idime=1,ndime
        bocod(idime,inodb)=coord(idime,ipoin)
     end do
  end do
  do inode=1,pnode
     ipoin=lnods(inode,ielem)
     do idime=1,ndime
        elcod(idime,inode)=coord(idime,ipoin)
     end do
  end do
  !
  ! Coordinates of the center of gravity
  !
  call bouder(&
       pnodb,ndime,ndimb,elmar(iblty)%dercg,bocod,baloc,eucta) ! BALOC
  call chenor(pnode,baloc,bocod,elcod)                ! Check BALOC
  !
  ! Set exwor=baloc and normalize it
  !      
  xnorm=0.0_rp
  do idime=1,ndime
     exwor(idime)=baloc(idime,ndime)
     xnorm=xnorm+exwor(idime)*exwor(idime)
  end do
  do idime=1,ndime
     exwor(idime)=exwor(idime)/sqrt(xnorm)
  end do

end subroutine extcog
