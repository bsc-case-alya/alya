#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



message(STATUS "Compiling with NVIDIA-HPC-SDK")
set(CMAKE_Fortran_COMPILER "mpif90")
set(CUSTOM_Fortran_FLAGS "-Mpreprocess -Mbackslash -Mextend -Mnoopenmp -Mnoidiom")
set(CUSTOM_Fortran_FLAGS_RELEASE "-fast")
set(CUSTOM_Fortran_FLAGS_DEBUG "-Mdclchk -Minform=inform -Mbounds -Mchkptr -Mchkstk -traceback -g")
set(CUSTOM_Fortran_FLAGS_I4 "")
set(CUSTOM_Fortran_FLAGS_I8 "")
set(CUSTOM_Fortran_FLAGS_O0 "-O0")
set(CUSTOM_Fortran_FLAGS_O1 "-O1")
set(CUSTOM_Fortran_FLAGS_O2 "-O2")
set(CUSTOM_Fortran_FLAGS_O3 "-O3")
set(CUSTOM_Fortran_FLAGS_STD2008 "")
if (WITH_GPU)
  message(STATUS "GPU options enabled")
  set(CUSTOM_Fortran_FLAGS "-Mpreprocess -Mbackslash -Mextend -Mnoopenmp -Mnoidiom -acc=gpu,noautopar,noroutineseq -gpu=cc70,cuda10.2")
  set(CUSTOM_Fortran_FLAGS_DEBUG "-Mdclchk -Minform=inform -Mbounds -Mchkptr -Mchkstk -traceback -g -acc=strict -gpu=debug -Minfo=all,ftn,intensity,pcast,pfo,time ")
endif()
