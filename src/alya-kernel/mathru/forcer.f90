!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine forcer()

  !------------------------------------------------------------------------
  !
  ! This function results in an error so that alya stops and the traceback tells where you are
  !
  !------------------------------------------------------------------------

  use def_kintyp, only     :  ip,rp
  implicit none

  real(rp)                 :: hhhhh

  call cputim(hhhhh)

  hhhhh = sin(hhhhh)
  hhhhh = sqrt(hhhhh-5.0)
  
end subroutine forcer
 
