!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Coupling
!> @{
!> @file    Endzon.f90
!> @author  Guillaume Houzeaux
!> @date    30/09/2014
!> @brief   Check block convergence if coupling
!> @details Check block convergence if coupling
!> @} 
!-----------------------------------------------------------------------

subroutine Endzon(itask)
  
  use def_kintyp,    only : ip
  use def_master,    only : iblok
  use def_master,    only : kfl_gocou
  use def_master,    only : mmodu
  use def_master,    only : ITASK_ENDZON
  use def_coupli,    only : mcoup
  use def_coupli,    only : kfl_gozon
  use def_coupli,    only : coupling_driver_iteration
  use def_coupli,    only : coupling_driver_number_couplings
  use mod_couplings, only : COU_CHECK_CONVERGENCE
  use mod_moduls,    only : moduls 
  use mod_immersed,  only : cou_update_couplings
  use def_coupli,    only : kfl_efect
  
  implicit none
  
  integer(ip), intent(in) :: itask
  !
  ! Check convergence if we have coupling
  !
  if( mcoup > 0 ) then
     !
     ! Call modules to exchange data
     !
     call moduls(ITASK_ENDZON)
     !
     ! Update couplings when using Embedded Finite Element Coupling Method (EFECT)
     !
     if( kfl_efect .and. coupling_driver_iteration(iblok) > 0 ) call cou_update_couplings()
     !
     ! We are currently in block IBLOK
     !  
     if( coupling_driver_number_couplings(iblok) /= 0 ) then
        call COU_CHECK_CONVERGENCE(iblok,kfl_gozon)
        if( kfl_gozon == 1 ) kfl_gocou = 1
     else
        kfl_gozon = 0 
     end if
     !
     ! Output convergence
     !
     call cou_cvgunk()   
     
  else
     !
     ! Go to next block
     !
     kfl_gozon = 0
     
  end if

#ifdef COMMDOM  
  call moduls(ITASK_ENDZON) 
#endif 

end subroutine Endzon
