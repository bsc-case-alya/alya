!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine xexcha(cpxva)
  !-----------------------------------------------------------------------
  !****f* xexcha
  ! NAME
  !    iexcha
  ! DESCRIPTION
  !    This routine exchange complex data individually.
  ! USES
  ! USED BY
  !    nsi_sendat
  !    nsa_sendat
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  implicit none
  complex(rp) :: cpxva

  nparx=nparx+1

  if(parii==2) then
     if(kfl_ptask==1) then
        if(kfl_paral<=0) parcx(nparx) = cpxva
        if(kfl_paral>=1) cpxva        = parcx(nparx)
     else if(kfl_ptask==0) then
        if(kfl_paral<=0) parcx(nparx) = cpxva
     else if(kfl_ptask==2) then
        cpxva = parcx(nparx) 
     end if
  end if

end subroutine xexcha
