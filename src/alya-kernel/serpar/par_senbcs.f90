!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_senbcs()
  !------------------------------------------------------------------------
  !****f* Parall/par_senbcs
  ! NAME
  !    par_senbcs
  ! DESCRIPTION
  !    Send boundary conditions to slaves  
  ! OUTPUT
  ! USED BY
  !    Domain
  !***
  !------------------------------------------------------------------------
  use def_kintyp
  use def_parame
  use def_parall
  use def_domain
  use def_master
  use mod_memchk
  use mod_domain, only : domain_memory_allocate
  use mod_domain, only : domain_memory_deallocate
  implicit none  

  strin = 'par_senbcs'
  strre = 'par_senbcs'
  strch = 'par_senbcs'

  !----------------------------------------------------------------------
  !
  ! Codes
  !
  !----------------------------------------------------------------------
  !
  ! Slaves allocate memory
  !
  if( ISLAVE ) then
     nboun_2 = nboun
     call domain_memory_allocate('KFL_CODNO')
     call domain_memory_allocate('KFL_CODBO')
  end if
  !
  ! Gather code arrays
  !
  if( kfl_icodn > 0 ) call par_parari('GAT',NPOIN_TYPE,mcono*npoin,kfl_codno)
  if( kfl_icodb > 0 ) call par_parari('GAT',NBOUN_TYPE,nboun,kfl_codbo)
  !
  ! Master deallocates memory
  !
  if( IMASTER ) then
     call domain_memory_deallocate('KFL_CODNO')
     call domain_memory_deallocate('KFL_CODBO')
  end if

end subroutine par_senbcs
