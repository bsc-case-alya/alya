!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_wrapper_gmsh
  !
  ! Test copy, extract and merge operators
  !
  use def_kintyp_basic
  use def_kintyp_mesh_basic
  use def_search_method
  use def_maths_bin  
  use mod_alya2gmsh
  use def_domain
  use def_master
  use def_parame
  use mod_memory
  use def_kermod
  use def_elmtyp
  use mod_memory

  use mod_elmgeo
  
  implicit none

  real(rp)              :: min_size_amr, max_size_amr
  real(rp)              :: volume, min_volume, max_volume, min_volume2, max_volume2
  real(rp)              :: dista, all_bobox(3,2)
  real(rp), pointer     :: cog(:), mesh_size(:)
  real(rp), pointer     :: bobox(:,:,:)
  real(rp), pointer     :: elcod(:,:)
  
  integer(ip)           :: ielem, idime, inode, pnode, ipoin, iloop, pelty
  integer(8)            :: memor_loc(2)
  
  type(mesh_type_basic) :: mesh_new, mesh_bou, mesh_vol, mesh_bin, mesh_cop
  type(maths_bin)       :: bin

  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction
  !
  ! Create bin structure and its corresponding mesh
  !
  memor_loc = 0_8
  nullify(bobox)  
  call meshe(ndivi) % element_bb(bobox)

  call bin      % init  ()  
  call bin      % input (boxes=(/50_ip,50_ip,50_ip/))  
  call bin      % fill  (BOBOX=bobox)
  call mesh_vol % init()
  call bin      % mesh(&
       mesh_vol % ndime,mesh_vol % mnode,mesh_vol % nelem,&
       mesh_vol % npoin,mesh_vol % lnods,mesh_vol % ltype,&
       mesh_vol % coord,CRITERION=SEARCH_MESH_LEAVES,MEMORY_COUNTER=memor_loc)
  !
  ! Allocate lninv_loc and leinv_loc for the mesh and fill them
  !
  all_bobox(:,1) =  1.0e10_rp
  all_bobox(:,2) = -1.0e10_rp
  
  allocate(mesh_vol % leinv_loc(mesh_vol % nelem))
  do ielem = 1, mesh_vol % nelem
     mesh_vol % leinv_loc(ielem) = ielem
  end do
  allocate(mesh_vol % lninv_loc(mesh_vol % npoin))
  do ipoin = 1, mesh_vol % npoin
     do idime = 1,mesh_vol % ndime
        all_bobox(idime,1) = min(all_bobox(idime,1), mesh_vol % coord(idime, ipoin))
        all_bobox(idime,2) = max(all_bobox(idime,2), mesh_vol % coord(idime, ipoin))               
     end do
     mesh_vol % lninv_loc(ipoin) = ipoin
  end do
  ! 
  ! Determine the mesh center of gravity of each element (cog) 
  ! and the new mesh size as the distance to cog and the 
  ! center of gravity of the problem boundaty box  
  !
  allocate(mesh_size(mesh_vol % nelem))
  allocate(cog(mesh_vol % ndime))    
  do ielem = 1, mesh_vol % nelem
     pnode        = nnode(mesh_vol % ltype(ielem))
     cog(:) = 0.0_rp
     do inode = 1,pnode
        ipoin = mesh_vol % lnods(inode,ielem)        
        do idime = 1,mesh_vol % ndime
           cog(idime) = cog(idime) + mesh_vol % coord(idime, ipoin)
        end do
     end do
     cog(:) = cog(:) /real(pnode,rp)
     dista = 0.0_rp
     do idime = 1, mesh_vol % ndime
        dista = dista + (cog(idime) - 0.5_rp*(all_bobox(idime,2) - all_bobox(idime,1)))**2
     end do
     dista = dista/10
     if (dista > 1.0e-3_rp) then
        dista = sqrt(dista)
     else
        dista = sqrt(1.0e-3_rp)
     end if
     mesh_size(ielem) = dista
  end do
  !
  ! Obtain the boundary of the input mesh
  !
  call mesh_bou % init('BOUNDARY_MESH')  
  call mesh_bou % boundary_mesh(meshe(ndivi))
  !
  ! Allocate lninv_loc and leinv_loc for the mesh and fill them
  !  
  allocate(mesh_bou % leinv_loc(mesh_bou % nelem))
  do ielem = 1, mesh_bou % nelem
     mesh_bou % leinv_loc(ielem) = ielem
  end do
  allocate(mesh_bou % lninv_loc(mesh_bou % npoin))
  do ipoin = 1, mesh_bou % npoin
     mesh_bou % lninv_loc(ipoin) = ipoin
  end do

  min_size_amr = minval(mesh_size)
  max_size_amr = maxval(mesh_size)
  !
  ! Determine maximum and minimum volumes for the input mesh
  !
  min_volume =  1.0e10_rp
  max_volume = -1.0e10_rp  
  pelty = abs(ltype(1))
  pnode = nnode(ltype(1))
  allocate(elcod(ndime,nelem))
  do ielem = 1, nelem
     do inode = 1,pnode
        ipoin                = lnods(inode,ielem)
        elcod(1:ndime,inode) = coord(1:ndime,ipoin)
     end do
     call elmgeo_element_volume(ndime,pelty,elcod,volume)
     max_volume = max(volume, max_volume)
     min_volume = min(volume, min_volume)    
  end do
  !
  ! Perfom the remeshing
  !
  call alya2gmsh_remeshing(mesh_new, mesh_vol, mesh_bou, mesh_size, min_size_amr, max_size_amr)
  !
  ! Determine maximum and minimum volumes for the new mesh
  !
  min_volume2 =  1.0e10_rp
  max_volume2 = -1.0e10_rp  
  pelty = abs(mesh_new % ltype(1))
  pnode = nnode(mesh_new % ltype(1))
  deallocate(elcod)
  allocate(elcod(ndime,mesh_new % nelem))
  do ielem = 1, mesh_new % nelem
     do inode = 1,pnode
        ipoin                = mesh_new % lnods(inode,ielem)
        elcod(1:ndime,inode) = mesh_new % coord(1:ndime,ipoin)
     end do
     call elmgeo_element_volume(ndime,pelty,elcod,volume)
     max_volume2 = max(volume, max_volume2)
     min_volume2 = min(volume, min_volume2)    
  end do
  !
  ! If the new mesh is not the same as the old one, gmsh is activated, 
  ! it has to accomplish some charactaeristcs with respect to the volumes between the new and old meshes 
  if ((mesh_new % nelem /= nelem  ) .and. (min_volume/min_volume2 < 100_rp) .and. (max_volume/max_volume2 < 3.0_rp)) then 
     print *, "the ratios between the old element volumes and the new ones are not bigger enoug"
     stop 1
  end if
  !
  ! Deallocate variables
  !
  call mesh_new % deallo()         
  deallocate(mesh_size)
  deallocate(cog)

  call runend('O.K.!')
  
end program unitt_wrapper_gmsh
