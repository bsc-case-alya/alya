/*-----------------------------------------------------------------------*/
/*                                                                       */ 
/*  This file is part of open-alya.                                      */ 
/*                                                                       */ 
/*  open-alya is free software: you can redistribute it and/or modify    */ 
/*  it under the terms of the GNU General Public License as published by */ 
/*  the Free Software Foundation, either version 3 of the License, or    */ 
/*  (at your option) any later version.                                  */ 
/*                                                                       */ 
/*  open-alya is distributed in the hope that it will be useful,         */ 
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of       */ 
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */ 
/*  GNU General Public License for more details.                         */ 
/*                                                                       */ 
/*  You should have received a copy of the GNU General Public License    */ 
/*  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   */ 
/*                                                                       */ 
/*-----------------------------------------------------------------------*/



#include <stdio.h>
#ifdef __unix__
#include <sched.h>
#endif

void par_core_num( int *core_num ) {
  
  int thread_num;

#ifdef __unix__
#ifdef _OPENMP
#pragma omp parallel private(thread_num) shared(core_num)
  {
    thread_num = omp_get_thread_num();
    core_num[thread_num] = sched_getcpu();
  }
#else
  core_num[0] = sched_getcpu();
#endif
#else 
  core_num[0] = -1;
#endif
}
