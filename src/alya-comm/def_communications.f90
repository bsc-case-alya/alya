!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Communication_Toolbox
!> @{
!> @file    def_communications.f90
!> @author  houzeaux
!> @date    2020-05-13
!> @brief   Variables
!> @details Variables needed for communications
!-----------------------------------------------------------------------

module def_communications

  use def_kintyp_basic,      only : ip,rp,lg,r1p,i1p
  use def_master,            only : kfl_paral
  use def_master,            only : IPARALL,IMASTER,ISLAVE,INOTSLAVE,ISEQUEN
  use def_master,            only : INOTMASTER
  use def_kintyp_comm,       only : comm_data_par
  use def_kintyp_comm,       only : comm_data_par_basic
  use def_master,            only : current_code,current_zone,current_subd
  use mod_memory_basic,      only : memory_alloca
  use mod_memory_basic,      only : memory_deallo
  use mod_memory_basic,      only : memory_size
  use mod_memory_basic,      only : memory_resize
  use mod_parall,            only : par_code_zone_subd_to_color
  use mod_parall,            only : PAR_COMM_COLOR
  use mod_parall,            only : PAR_COMM_MY_CODE
  use mod_parall,            only : PAR_COMM_MY_CODE_WM
  use mod_parall,            only : PAR_COMM_COLOR_ARRAY
  use mod_parall,            only : PAR_COMM_MY_CODE_ARRAY
  use mod_parall,            only : PAR_MY_CODE_RANK
  use mod_parall,            only : PAR_MY_CODE_RANK_WM
  use mod_parall,            only : PAR_MY_WORLD_RANK
  use mod_parall,            only : PAR_COMM_COLOR_PERM
  use mod_parall,            only : PAR_COMM_NULL
  use mod_parall,            only : PAR_COMM_WORLD
  use mod_parall,            only : PAR_COMM_CURRENT
  use mod_parall,            only : PAR_INTEGER
  use mod_parall,            only : PAR_REAL
  use mod_parall,            only : PAR_COMM_SFC
  use mod_parall,            only : PAR_COMM_SFC_WM
  use mod_parall,            only : PAR_COMM_MPIO
  use mod_parall,            only : PAR_COMM_MPIO_WM
  use mod_parall,            only : commd
  use mod_parall,            only : color_target
  use mod_parall,            only : color_source
  use mod_parall,            only : par_memor
  use mod_parall,            only : PAR_CODE_SIZE
  use mod_optional_argument, only : optional_argument
  use mod_std  
  use def_mpi

end module def_communications
