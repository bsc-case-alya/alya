!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!------------------------------------------------------------------------
!> @addtogroup Solidz
!> @{
!> @file    nsi_wetno.f90
!> @date    18/05/2016
!> @author  Alfonso Santiago
!> @brief   Postprocess wet nodes
!> @details Postprocess wet nodes
!> @}
!------------------------------------------------------------------------
subroutine nsi_wetno(touched)

  use def_kintyp,    only : ip,rp
  use def_master,    only:  modul, TIME_N, ITER_AUX, ITER_K, inotmaster, current_code
  use def_domain,    only: ndime, npoin
  use def_coupli,    only :  coupling_type, mcoup
  use mod_parall,    only : PAR_MY_CODE_RANK

  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!
  implicit none
  integer(ip)                       :: icoup

  logical(ip)                       :: code_j
  integer(ip)                       :: n_wets, idime
  real(rp), intent(inout)           :: touched(npoin)
  integer(ip), pointer              :: wets(:) => null()
  !-----------------------------------------------------------------------||---!
  !                                                                            !
  !-----------------------------------------------------------------------||---!

  touched = 0.0_rp
  n_wets  = -1
  !
    do icoup=1_ip,  mcoup
        code_j = current_code == coupling_type(icoup) % code_target
        if(code_j) then
            n_wets =  coupling_type(icoup)%wet%npoin_wet
            wets   => coupling_type(icoup)%wet%lpoin_wet(:)

                if(inotmaster .and. associated(wets)) touched( wets(1:n_wets) ) = real(icoup,rp)

        endif
    enddo


end subroutine nsi_wetno

