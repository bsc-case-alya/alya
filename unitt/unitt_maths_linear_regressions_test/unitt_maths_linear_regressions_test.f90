!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_maths_linear_regressions 
  
   use def_kintyp_basic,      only : ip, rp
   use mod_maths_basic,       only : maths_weighted_linear_regression
   use mod_maths_basic,       only : maths_linear_regression
  
  implicit none
 
   real(rp),    pointer     :: W(:)
   real(rp),    pointer     :: X(:)
   real(rp),    pointer     :: Y(:)
   real(rp)                 :: a, b


   nullify(X,Y,W)
   allocate(X(3),Y(3),W(3))

   X = (/  1.0_rp,2.0_rp,1.0_rp /) 
   Y = (/  2.0_rp,3.0_rp,3.0_rp /) 
   w = 1.0_rp
   
   call maths_weighted_linear_regression(X,Y,3_ip,a,b,W)

   print*,'a,b=',a,b
   
   call maths_linear_regression(X,Y,2_ip,a,b,2_ip)

   print*,'a,b=',a,b
   
   stop
  
end program unitt_maths_linear_regressions
