#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



#
# To use Cantera do:
# module load python/2.7.13
# source /gpfs/projects/bsc21/Cantera-Alya/cantera/INTEL/bin/setup_cantera
#

import cantera as ct

gas = ct.Solution('gri30.cti')

gas.TPX = 400.0, ct.one_atm, "N2:0.79, O2:0.21"

print(gas.report())

print('Enthalpy: {} J/(kgK)'.format(gas.enthalpy_mass))




