!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_solidz_csm_simd

   use def_kintyp,       only : ip,rp,lg
   use def_domain,       only : ndime
   use mod_sld_vect_csm

   implicit none

#ifdef VECTOR_SIZE
#define pgaus 5

   logical(ip) :: ierror
   integer(ip) :: ii, jj, kk, ll, ig, iv
   real(rp)    :: vprops(9), theta
   real(rp)    :: t2DA(VECTOR_SIZE,2,2,pgaus), t2DB(VECTOR_SIZE,2,2,pgaus)
   real(rp)    :: t3DA(VECTOR_SIZE,3,3,pgaus), t3DB(VECTOR_SIZE,3,3,pgaus)
   real(rp)    :: t43DA(VECTOR_SIZE,3,3,3,3,pgaus), t43DB(VECTOR_SIZE,3,3,3,3,pgaus)
   real(rp)    :: v2DA(VECTOR_SIZE,3,pgaus)
   real(rp)    :: v3DA(VECTOR_SIZE,6,pgaus), v3DB(VECTOR_SIZE,6,pgaus), v3DC(VECTOR_SIZE,6,pgaus)
   real(rp)    :: v43DA(VECTOR_SIZE,6,6,pgaus), v43DB(VECTOR_SIZE,6,6,pgaus), v43DC(VECTOR_SIZE,6,6,pgaus)
   real(rp)    :: Q3DA(VECTOR_SIZE,3,3)

   ! Create a symmetric 2D tesnor
   ! - fill
   kk = 0_ip
   do ig = 1, pgaus; do ii = 1, 2; do jj = 1, 2
      kk = kk + 1_ip
      t2DB(1:VECTOR_SIZE,ii,jj,ig) = real(kk,rp)
   enddo; enddo; enddo
   ! - simmetrize
   do ig = 1, pgaus; do ii = 1, 2; do jj = 1, 2
      t2DA(1:VECTOR_SIZE,ii,jj,ig) = 0.5_rp * (t2DB(1:VECTOR_SIZE,ii,jj,ig) + t2DB(1:VECTOR_SIZE,jj,ii,ig))
   enddo; enddo; enddo

   ! Create a symmetric 3D tesnor
   ! - fill
   kk = 0_ip
   do ig = 1, pgaus; do ii = 1, 3; do jj = 1, 3
      kk = kk + 1_ip
      t3DB(1:VECTOR_SIZE,ii,jj,ig) = real(kk,rp)
   enddo; enddo; enddo
   ! - simmetrize
   do ig = 1, pgaus; do ii = 1, 3; do jj = 1, 3
      t3DA(1:VECTOR_SIZE,ii,jj,ig) = 0.5_rp * (t3DB(1:VECTOR_SIZE,ii,jj,ig) + t3DB(1:VECTOR_SIZE,jj,ii,ig))
   enddo; enddo; enddo   

   ! Compact/Uncompact 2D - stress tensor
   write(*,*) 'Compact/uncompact 2D stress tensor'
   ndime = 2_ip
   ! - compacting
   call sld_vcsm_compact_stress_at_GP(int(pgaus,ip),t2DA,v2DA)
   ! - uncompacting
   call  sld_vcsm_uncompact_stress_at_GP(int(pgaus,ip),v2DA,t2DB)
   ! - checking
   ierror = compare(int(VECTOR_SIZE*2*2*pgaus,ip),t2DA,t2DB)


   ! Compact/Uncompact 3D - stress tensor
   write(*,*) 'Compact/uncompact 3D stress tensor'
   ndime = 3_ip
   ! - compacting
   call sld_vcsm_compact_stress_at_GP(int(pgaus,ip),t3DA,v3DA)
   ! - uncompacting
   call sld_vcsm_uncompact_stress_at_GP(int(pgaus,ip),v3DA,t3DB)
   ! - checking
   ierror = compare(int(VECTOR_SIZE*3*3*pgaus,ip),t3DA,t3DB)

  
   ! Compact/Uncompact 2D - strain tensor
   write(*,*) 'Compact/uncompact 2D strain tensor'
   ndime = 2_ip
   ! - compacting
   call sld_vcsm_compact_strain_at_GP(int(pgaus,ip),t2DA,v2DA)
   ! - uncompacting
   call sld_vcsm_uncompact_strain_at_GP(int(pgaus,ip),v2DA,t2DB)
   ! - checking
   ierror = compare(int(VECTOR_SIZE*2*2*pgaus,ip),t2DA,t2DB)


   ! Compact/Uncompact 3D - strain tensor
   write(*,*) 'Compact/uncompact 3D strain tensor'
   ndime = 3_ip
   ! - compacting
   call sld_vcsm_compact_strain_at_GP(int(pgaus,ip),t3DA,v3DA)
   ! - uncompacting
   call sld_vcsm_uncompact_strain_at_GP(int(pgaus,ip),v3DA,t3DB)
   ! - checking
   ierror = compare(int(VECTOR_SIZE*3*3*pgaus,ip),t3DA,t3DB)  


   ! Compact/Uncompact 3D - elasticity tensor
   write(*,*) 'Compact/uncompact 3D stiffness tensor'
   ndime = 3_ip
   ! - manufactured solution
   kk = 0_ip
   do ig = 1, pgaus
      do iv = 1, VECTOR_SIZE
         kk = kk + 1_ip
         vprops(1) = 1000.0_rp + (10000.0_rp/real(pgaus*VECTOR_SIZE,rp))*(real(kk,rp) - 1.0_rp) ! E 
         vprops(2) = (0.49_rp/real(pgaus*VECTOR_SIZE,rp))*(real(kk,rp) - 1.0_rp)                ! mu
         vprops(3) = vprops(1) / ( 2.0_rp * ( 1.0_rp + vprops(2) ) )
         vprops(4) = vprops(1) * vprops(2) / ( ( 1.0_rp + vprops(2) ) * ( 1.0_rp - 2.0_rp * vprops(2) ) )
         ! compacted
         v43DA(iv,:,:,ig) = get_elasticity_tensor_isolin_3D_compacted(vprops(1:2)) 
         ! tensorial
         t43DA(iv,:,:,:,:,ig) = get_elasticity_tensor_isolin_3D(vprops(3:4)) 
      enddo
   enddo
   ! - compacting
   call sld_vcsm_compact_stiffness_tensor_at_GP(int(pgaus,ip),t43DA,v43DB)
   ! - checking
   ierror = compare(int(VECTOR_SIZE*6*6*pgaus,ip),v43DA,v43DB)
   ! - uncompacting
   call sld_vcsm_uncompact_stiffness_tensor_at_GP(int(pgaus,ip),v43DA,t43DB)
   ! - checking
   ierror = compare(int(VECTOR_SIZE*3*3*3*3*pgaus,ip),t43DA,t43DB)


   ! Local2Global / Global2Local transformation
   write(*,*) 'Local2Global / Global2Local compacted stress tensor'
   ndime = 3_ip
   ! - define stress tensor
   kk = 0_ip
   do ig = 1, pgaus; do iv = 1, VECTOR_SIZE
      v3DA(iv,1,ig) = 100.0_rp * real(ig + iv,rp)
      v3DA(iv,2,ig) =  40.0_rp * real(ig + iv,rp)
      v3DA(iv,3,ig) =  50.0_rp * real(ig + iv,rp)
      v3DA(iv,4,ig) =   0.0_rp * real(ig + iv,rp)
      v3DA(iv,5,ig) =   0.0_rp * real(ig + iv,rp)
      v3DA(iv,6,ig) =  30.0_rp * real(ig + iv,rp)
   end do; enddo
   ! - create rotation matrix 
   kk = 0_ip
   do iv = 1, VECTOR_SIZE
      kk = kk + 1_ip
      theta =  (360.0_rp/real(VECTOR_SIZE,rp))*(real(kk,rp) - 1.0_rp)
      Q3DA(iv,:,:) = get_rotation_matrix_axis_Z(theta)   
   enddo
   ! - create manufactured solution from the Mohr Circle solution
   do ig= 1, pgaus; do iv = 1, VECTOR_SIZE
      kk = kk + 1_ip
      theta =  (360.0_rp/(VECTOR_SIZE))*(real(kk,rp) - 1.0_rp)
      v3DB(iv,:,ig) = rotate_compacted_stress_3D_axisZ_MorhCircle(theta,v3DA(iv,:,ig))
   enddo; enddo
   ! - rotate from GLOBAL to LOCAL CSYS
   call sld_vcsm_compact_transformation_stress_at_GP(0_ip,int(pgaus,ip),Q3DA,v3DA,v3DC)
   ierror = compare(int(VECTOR_SIZE*6*pgaus,ip),v3DC,v3DB)
   ! - rotate from LOCAL to GLOBAL CSYS
   call sld_vcsm_compact_transformation_stress_at_GP(1_ip,int(pgaus,ip),Q3DA,v3DB,v3DC)
   ierror = compare(int(VECTOR_SIZE*6*pgaus,ip),v3DC,v3DA)


   ! Local2Global / Global2Local transformation
   write(*,*) 'Local2Global / Global2Local compacted strain tensor'
   ndime = 3_ip
   ! - define stress tensor
   kk = 0_ip
   do ig = 1, pgaus; do iv = 1, VECTOR_SIZE
      v3DA(iv,1,ig) =   0.01_rp * real(ig + iv,rp)
      v3DA(iv,2,ig) =  -0.01_rp * real(ig + iv,rp)
      v3DA(iv,3,ig) =   1.00_rp * real(ig + iv,rp)
      v3DA(iv,4,ig) =   0.00_rp * real(ig + iv,rp)
      v3DA(iv,5,ig) =   0.00_rp * real(ig + iv,rp)
      v3DA(iv,6,ig) =   0.04_rp * real(ig + iv,rp)
   end do; enddo
   ! - create rotation matrix 20deg in X-Y plane
   kk = 0_ip
   do iv = 1, VECTOR_SIZE
      kk = kk + 1_ip
      theta =  (360.0_rp/real(VECTOR_SIZE,rp))*(real(kk,rp) - 1.0_rp)
      Q3DA(iv,:,:) = get_rotation_matrix_axis_Z(theta)   
   enddo
   ! - create manufactured solution from the Mohr Circle solution
   do ig= 1, pgaus; do iv = 1, VECTOR_SIZE
      kk = kk + 1_ip
      theta =  (360.0_rp/real(VECTOR_SIZE,rp))*(real(kk,rp) - 1.0_rp)
      v3DB(iv,:,ig) = rotate_compacted_strain_3D_axisZ_MorhCircle(theta,v3DA(iv,:,ig))
   enddo; enddo
   ! - rotate from GLOBAL to LOCAL CSYS
   call sld_vcsm_compact_transformation_strain_at_GP(0_ip,int(pgaus,ip),Q3DA,v3DA,v3DC)
   ierror = compare(int(VECTOR_SIZE*6*pgaus,ip),v3DC,v3DB)
   ! - rotate from LOCAL to GLOBAL CSYS
   call sld_vcsm_compact_transformation_strain_at_GP(1_ip,int(pgaus,ip),Q3DA,v3DB,v3DC)
   ierror = compare(int(VECTOR_SIZE*6*pgaus,ip),v3DC,v3DA)


   ! Local2Global / Global2Local transformation
   write(*,*) 'Local2Global / Global2Local compacted orthotropic tensor'
   ndime = 3_ip
   ! - create rotation matrix in X-Y plane
   kk = 0_ip
   do iv = 1, VECTOR_SIZE
      kk = kk + 1_ip
      theta =  (360.0_rp/real(VECTOR_SIZE,rp))*(real(kk,rp) - 1.0_rp)
      Q3DA(iv,:,:) = get_rotation_matrix_axis_Z(theta)
   enddo
   ! - create manufactured solution 
   do ig= 1, pgaus; do iv = 1, VECTOR_SIZE
      vprops = [100.0_rp, 200.0_rp, 300.0_rp, 0.1_rp, 0.2_rp, 0.1_rp, 10.0_rp, 20.0_rp, 30.0_rp]
      v43DA(iv,:,:,ig) = get_elasticity_tensor_ortlin_3D_compacted(vprops(1:9))
   enddo; enddo
   ! - create manufactured solution by rotation the 4th order tensor
   call sld_vcsm_uncompact_stiffness_tensor_at_GP(int(pgaus,ip),v43DA,t43DA)
   do ig= 1, pgaus; do iv = 1, VECTOR_SIZE
      t43DB(iv,:,:,:,:,ig) = transformate_4th_tensor_3D(Q3DA(iv,:,:),t43DA(iv,:,:,:,:,ig))
   enddo; enddo
   call sld_vcsm_compact_stiffness_tensor_at_GP(int(pgaus,ip),t43DB,v43DB)
   ! - rotate from GLOBAL to LOCAL CSYS
   call sld_vcsm_compact_transformation_stiffness_at_GP(0_ip,int(pgaus,ip),Q3DA,v43DA,v43DC)
   ierror = compare(int(VECTOR_SIZE*6*6*pgaus,ip),v43DC,v43DB)
   ! - rotate from LOCAL to GLOBAL CSYS
   call sld_vcsm_compact_transformation_stiffness_at_GP(1_ip,int(pgaus,ip),Q3DA,v43DB,v43DC)
   ierror = compare(int(VECTOR_SIZE*6*pgaus,ip),v3DC,v3DA)

#else
   print*,'NOT TESTED BECAUSE VECTOR_SIZE = 1'
   stop 0
#endif


   contains

      function compare(n,A,B) result(equal)
         implicit none
         integer(ip), intent(in) :: n
         real(rp),    intent(in) :: A(*), B(*)
         logical(lg)             :: equal
         integer(ip)             :: ii
         real(rp), parameter     :: tol = 1.0e-8_rp
         equal = .true.
         do ii = 1, n
             if( abs(A(ii) - B(ii)) > tol )then
                equal = .false.
                print*,ii,A(ii),B(ii),abs(A(ii) - B(ii))
             endif
         enddo
         if( .not. equal ) stop 1
      end function compare 


      function transformate_4th_tensor_3D(Q,A) result(B)
         implicit none
         real(rp), intent(in) :: Q(3,3)
         real(rp), intent(in) :: A(3,3,3,3)
         real(rp)             :: B(3,3,3,3)
         integer(ip)          :: ii, jj, kk, ll, mm, nn, pp, qq
         do ii = 1,3; do jj = 1,3; do kk = 1,3; do ll = 1,3
            B(ii,jj,kk,ll) = 0.0_rp
            do mm = 1,3; do nn = 1,3; do pp = 1,3; do qq = 1,3
               B(ii,jj,kk,ll) = B(ii,jj,kk,ll) + &
                  Q(ii,mm) * Q(jj,nn) * Q(kk,pp) * Q(ll,qq) * A(mm,nn,pp,qq)
            enddo; enddo; enddo; enddo
         enddo; enddo; enddo; enddo
      end function transformate_4th_tensor_3D

     
      function get_rotation_matrix_axis_Z(theta) result(Q)
         implicit none
         real(rp), intent(in) :: theta   ! Degrees
         real(rp)             :: Q(3,3)
         real(rp)             :: th
         real(rp), parameter  :: coef = (16.0_rp*ATAN(1.0_rp/5.0_rp) - 4.0_rp*ATAN(1.0_rp/239.0_rp))/180.0_rp 
         th = theta * coef
         Q  = reshape( [ cos(th), -sin(th),   0.0_rp,   &
                         sin(th),  cos(th),   0.0_rp,   &
                          0.0_rp,   0.0_rp,   1.0_rp ],  &
                       [ 3,3 ] )
      end function get_rotation_matrix_axis_Z 
      

      function get_elasticity_tensor_isolin_3D(props) result(C)
         implicit none
         real(rp), intent(in) :: props(:)
         real(rp)             :: C(3,3,3,3)
         real(rp)             :: lmb, mu
         integer(ip)          :: ii, jj, kk, ll
         real(rp), parameter  :: Idd(3,3) = reshape( [ 1.0_rp, 0.0_rp, 0.0_rp  , &
                                                       0.0_rp, 1.0_rp, 0.0_rp  , &
                                                       0.0_rp, 0.0_rp, 1.0_rp ], &
                                                     [ 3, 3 ])
         lmb = props(1)
         mu  = props(2)
         do ii = 1, 3;  do jj = 1, 3; do kk = 1, 3; do ll = 1, 3
            C(ii,jj,kk,ll) = lmb * ( Idd(ii,ll) * Idd(jj,kk) + Idd(ii,kk) * Idd(jj,ll) ) + &
                             mu  * ( Idd(ii,jj) * Idd(kk,ll) )
         enddo; enddo; enddo; enddo
      end function get_elasticity_tensor_isolin_3D


      function get_elasticity_tensor_isolin_3D_compacted(props) result(C)
         implicit none
         real(rp), intent(in) :: props(:)
         real(rp)             :: C(6,6)
         real(rp)             :: E, nu
         E   = props(1)
         nu  = props(2)
         C(:,:) = 0.0_rp
         C(1,1) = 1.0_rp - nu
         C(2,1) = nu
         C(3,1) = nu
         C(1,2) = nu
         C(2,2) = 1.0_rp - nu
         C(3,2) = nu
         C(1,3) = nu
         C(2,3) = nu
         C(3,3) = 1.0_rp - nu
         C(4,4) = (1.0 - 2.0_rp * nu) * 0.5_rp
         C(5,5) = (1.0 - 2.0_rp * nu) * 0.5_rp
         C(6,6) = (1.0 - 2.0_rp * nu) * 0.5_rp
         C(:,:) = E / ( ( 1.0_rp + nu ) * ( 1.0_rp - 2.0_rp * nu ) ) * C(:,:)
      end function get_elasticity_tensor_isolin_3D_compacted


      function get_elasticity_tensor_ortlin_3D_compacted(props) result(C)
         implicit none
         real(rp), intent(in) :: props(:)
         real(rp)             :: C(6,6)
         real(rp)             :: E11, E22, E33
         real(rp)             :: v12, v13, v23, v21, v31, v32
         real(rp)             :: G12, G13, G23
         real(rp)             :: delta, auxS1
         E11 = props(1)
         E22 = props(2)
         E33 = props(3)
         v12 = props(4)
         v13 = props(5)
         v23 = props(6)
         G12 = props(7)
         G13 = props(8)
         G23 = props(9)
         v21 = (v12*E22)/E11
         v31 = (v13*E33)/E11
         v32 = (v23*E33)/E22
         C(:,:) = 0.0_rp
         delta = (1.0_rp - v12*v21 - v23*v32 - v31*v13 - 2.0_rp*v12*v23*v31)/(E11*E22*E33)
         auxS1 = E22*E33*delta
         C(1,1) = (1.0_rp - v23*v32)/auxS1
         C(1,2) = (v21 + v31*v23)/auxS1
         C(1,3) = (v31 + v21*v32)/auxS1
         auxS1 = E33*E11*delta
         C(2,1) = (v12 + v13*v32)/auxS1
         C(2,2) = (1.0_rp - v31*v13)/auxS1
         C(2,3) = (v32 + v31*v12)/auxS1
         auxS1 = E11*E22*delta
         C(3,1) = (v13 + v12*v23)/auxS1
         C(3,2) = (v23 + v13*v21)/auxS1
         C(3,3) = (1.0_rp - v12*v21)/auxS1
         C(4,4) = G23
         C(5,5) = G13
         C(6,6) = G12
      end function get_elasticity_tensor_ortlin_3D_compacted


      function rotate_compacted_stress_3D_axisZ_MorhCircle(theta,str) result(rts)
         implicit none
         real(rp), intent(in) :: theta
         real(rp), intent(in) :: str(:)
         real(rp)             :: rts(6)
         real(rp)             :: th
         real(rp), parameter  :: coef = (16.0_rp*ATAN(1.0_rp/5.0_rp) - 4.0_rp*ATAN(1.0_rp/239.0_rp))/180.0_rp
         th = theta * coef
         rts(1) = str(1) * cos(th)**2 + str(2) * sin(th)**2 + str(6) * sin(th) * cos(th) * 2.0_rp
         rts(2) = str(1) * sin(th)**2 + str(2) * cos(th)**2 - str(6) * sin(th) * cos(th) * 2.0_rp
         rts(3) = str(3)
         rts(4) = str(4)
         rts(5) = str(5)
         rts(6) = (str(2) - str(1)) * sin(th) * cos(th) + str(6) * ( cos(th)**2 - sin(th)**2)
      end function rotate_compacted_stress_3D_axisZ_MorhCircle


      function rotate_compacted_strain_3D_axisZ_MorhCircle(theta,str) result(rts)
         implicit none
         real(rp), intent(in) :: theta
         real(rp), intent(in) :: str(:)
         real(rp)             :: rts(6)
         real(rp)             :: th
         real(rp), parameter  :: coef = (16.0_rp*ATAN(1.0_rp/5.0_rp) - 4.0_rp*ATAN(1.0_rp/239.0_rp))/180.0_rp 
         th = theta * coef
         rts(1) = str(1) * cos(th)**2 + str(2) * sin(th)**2 + str(6) * sin(th) * cos(th)
         rts(2) = str(1) * sin(th)**2 + str(2) * cos(th)**2 - str(6) * sin(th) * cos(th)
         rts(3) = str(3)
         rts(4) = str(4)
         rts(5) = str(5)
         rts(6) = 2.0_rp*(str(2) - str(1)) * sin(th) * cos(th) + str(6) * ( cos(th)**2 - sin(th)**2)
      end function rotate_compacted_strain_3D_axisZ_MorhCircle

end program unitt_solidz_csm_simd
