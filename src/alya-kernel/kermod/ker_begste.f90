!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Kermod
!> @{
!> @file    nsi_begste.f90
!> @author  Guillaume Houzeaux
!> @date    05/06/2013
!> @brief   Begin a time step
!> @details Begin a time step
!> @} 
!-----------------------------------------------------------------------
subroutine ker_begste()
  
  use def_master,        only : ITASK_BEGSTE
  use mod_mass_matrix,   only : mass_matrix_consistent
  use mod_ker_subdomain, only : ker_subdomain_motion
  use mod_eccoupling,    only : eccou_save_log, kfl_exmsld_ecc
  use mod_immersed,      only : cou_update_couplings  
  use mod_biofibers,     only : kfl_biofibers, biofibers
  use def_coupli,        only : kfl_dimbou

  implicit none
  !
  ! Transient fields
  !
  call calc_kx_tran_fiel()
  !
  ! Update fields: Velocity, temperature, concentration and displacement functions
  !
  call ker_velfun(ITASK_BEGSTE)
  call ker_temfun(ITASK_BEGSTE)
  call ker_confun(ITASK_BEGSTE)
  call ker_disfun(ITASK_BEGSTE)
  call ker_arefun(ITASK_BEGSTE)
  !
  ! Update subdomains
  !
  call ker_subdomain_motion()  
  !
  ! Update couplings 
  !
  if( kfl_dimbou ) call cou_update_couplings()
  !
  ! Weighted consistent mass... Kermod should have been read
  !
  call mass_matrix_consistent(CONSISTENT_WEIGHTED_MASS=.true.)
  !
  ! Bio-fibers
  !
  if( kfl_biofibers )then
     call biofibers % update_fibers_at_nodes(ITASK_BEGSTE)
  endif

end subroutine ker_begste
