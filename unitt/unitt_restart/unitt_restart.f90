!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_restart
  !
  ! Order strings
  !
  use def_kintyp_basic, only : ip,rp
  use def_mpio
  use mod_restart
  use def_master
  implicit none

  character(10) :: wtest(4)
  integer(ip)   :: nn,itask,nunit,ii
  integer(ip)   :: as, a1 (2), a2 (2,2)
  integer(ip)   :: as_,a1_(2), a2_(2,2)
  real(rp)      :: bs, b1 (2), b2 (2,2)
  real(rp)      :: bs_,b1_(2), b2_(2,2)

  INOTSLAVE     = .true.
  ISEQUEN       = .true.
  IPARALL       = .false.
  mpio_flag_rst = 0
  nunit         = 10_ip
  
  as      = 1
  a1      = (/2,3/)
  a2(1,1) = 4
  a2(2,1) = 5
  a2(1,2) = 6
  a2(2,2) = 7

  bs      = 1.0_rp
  b1      = (/2.0_rp,3.0_rp/)
  b2(1,1) = 4.0_rp
  b2(2,1) = 5.0_rp
  b2(1,2) = 6.0_rp
  b2(2,2) = 7.0_rp

  a1_ = a1 ; a2_ = a2 ; as_ = as
  b1_ = b1 ; b2_ = b2 ; bs_ = bs
  
  do ii = 1,2
     if( ii == 1 ) then
        itask = ITASK_WRITE_RESTART
     else
        itask = ITASK_READ_RESTART 
     end if 
     if( ii == 1 ) then       
        open(unit=nunit,file='restart.txt',form='unformatted',status='unknown') 
     else
        open(unit=nunit,file='restart.txt',form='unformatted',status='old')         
     end if
     call restart_ini(itask,nunit)
     call restart_add(as,'as')
     call restart_add(a1,'a1')
     call restart_add(a2,'a2')
     call restart_add(bs,'bs')
     call restart_add(b1,'b1')
     call restart_add(b2,'b2')
     call restart_end(itask)
     close(unit=nunit)
     if( ii == 1 ) then
        as = 0 
        a1 = 0
        a2 = 0
        bs = 0.0_rp
        b1 = 0.0_rp
        b2 = 0.0_rp
     end if
  end do
  
  if( as /= as_ ) then
     print*,'Error as=',as,as_
     stop 1
  end if
  if( any(abs(a1-a1_) > 0 ) ) then
     print*,'Error a1=',a1,a1_
     stop 1
  end if
  if( any(abs(a2-a2_) > 0 ) ) then
     print*,'Error a2=',a2,a2_
     stop 1
  end if

  if(     abs(bs-bs_) > 1.0e-12_rp   ) then
     print*,'Error bs=',bs,bs_
     stop 1
  end if
  if( any(abs(b1-b1_) > 1.0e-12_rp ) ) then
     print*,'Error b1=',b1,b1_
     stop 1
  end if
  if( any(abs(b2-b2_) > 1.0e-12_rp ) ) then
     print*,'Error b2=',b2,b2_
     stop 1
  end if

end program unitt_restart
