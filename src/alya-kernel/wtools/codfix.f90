!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine codfix(ndime,kfl_fixno)
!-----------------------------------------------------------------------
!****f* wtool/codifx
! NAME 
!    codfix
! DESCRIPTION
!    This routine codes fixno
! USES
! USED BY
!    
!***
!-----------------------------------------------------------------------
  use      def_kintyp
  implicit none
  integer(ip), intent(in)    :: ndime
  integer(ip), intent(inout) :: kfl_fixno(ndime)
  integer(ip)                :: icode,idime
  real(rp)                   :: rcode

  icode = kfl_fixno(1)
  do idime = 1,ndime
     rcode = icode/10**(ndime-idime)
     kfl_fixno(idime) = int(rcode)
     icode = icode - kfl_fixno(idime)*10**(ndime-idime)
  end do

end subroutine codfix

