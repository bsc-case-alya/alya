!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine iniste(itask)
  !-----------------------------------------------------------------------
  !****f* master/iniste
  ! NAME
  !    iniste
  ! DESCRIPTION
  !    This routine initialize variables at the beginning of a time step
  !    ITASK=1 ... Before modules have computed their time steps
  !    ITASK=2 ... After modules have computed their time steps
  ! USES
  ! USED BY
  !    Begste
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_postpr, only : ncoun_pos
  use def_coupli, only : kfl_gozon
  use def_domain, only : coord, npoin, lnods, nelem, ltype
  use def_kermod, only : kfl_reset
  
#ifdef CATA
  use tcp
#endif

  implicit none
  integer(ip), intent(in) :: itask
  integer(ip), save       :: ipass=0
  
  select case ( itask )

  case ( 1_ip )
     !
     ! Before modules have computed their time steps
     !
     if( kfl_timco == 1 ) dtinv = 0.0_rp
     if( ipass     == 0 ) then
        ipass = 1
        call cputim(cpu_times)
     end if
     
  case ( 2_ip )
     !
     ! After modules have computed their time steps 
     !
     kfl_goblk = 1
     kfl_gocou = 1
     kfl_gozon = 1
     itcou     = 1
     iblok     = 1
     iitrf     = 1 
     if( micou(iblok) == 0 ) kfl_gocou = 0
     if ( kfl_reset < 1_ip ) then 
        ittim     = ittim + 1
        itti2     = itti2 + 1
     endif
     !
     ! Postprocess counter
     !
     ncoun_pos = 0
     !
     ! call coprocessing 
     !
#ifdef CATA
     if (kfl_paral==1) call testcoprocessor(ittim,cutim,npoin,coord,nelem,lnods,ltype)
#endif
 
  end select

end subroutine iniste
