!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Solver
!> @{
!> @file    solver_destructor.f90
!> @author  houzeaux
!> @date    2019-06-11
!> @brief   Destroy solver
!> @details Reinitialize all solver arrays
!> @} 
!-----------------------------------------------------------------------

subroutine solver_destructor()

  use def_kintyp, only : ip
  use def_master, only : mmodu
  use def_master, only : momod
  use def_master, only : kfl_modul
  use mod_solver, only : solver_deallocate
  use def_solver, only : SOL_NO_SOLVER
  implicit none

  integer(ip) :: ivari,imodu
  
  do imodu = 1,mmodu
     if( kfl_modul(imodu) == 1 ) then
        if( associated(momod(imodu) % solve) ) then
           do ivari = 1,size(momod(imodu) % solve,KIND=ip)
              if( momod(imodu) % solve(ivari) % kfl_algso /= SOL_NO_SOLVER ) then
                 call solver_deallocate(momod(imodu) % solve(ivari),ivari)
                 if( momod(imodu) % solve(ivari) % kfl_clean_precond == 0 ) then
                    momod(imodu) % solve(ivari) % kfl_clean_precond = -1
                 end if
              end if
           end do
        end if
     end if
  end do

end subroutine solver_destructor
