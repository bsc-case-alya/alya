!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_bmsh
  !
  ! Test copy, extract and merge operators
  !
  use def_elmtyp
  use def_kintyp_basic
  use def_kintyp_mesh_basic
  use def_kintyp_mesh
  use mod_memory_basic
  use mod_elmgeo
  implicit none

  type(mesh_type)               :: mesh
  type(bmsh_type_basic)         :: bmsh
  integer(ip)                   :: ipoin,idime,ielem,inode
  real(rp),             pointer :: cbntr(:,:)

  nullify(cbntr)

  call elmgeo_element_type_initialization()

  call mesh % init('MY_MESH')
  
  mesh % nelem = 9
  mesh % npoin = 16
  mesh % ndime = 2
  mesh % mnode = 4
  mesh % mnodb = 2
  mesh % nboun = 4
  call mesh % alloca()
  allocate(mesh % ltypb(mesh % nboun))
  allocate(mesh % lnodb(mesh % mnodb,mesh % nboun))
  allocate(mesh % lelbo(mesh % nboun))

  print*,'a'
  mesh % lnods(:,1)  = (/  12 ,7 ,5 ,10  /)
  mesh % lnods(:,2)  = (/  14 ,9 ,7 ,12   /)
  mesh % lnods(:,3)  = (/  16, 15, 9 ,14  /) 
  mesh % lnods(:,4)  = (/  7, 4 ,2 ,5   /)
  mesh % lnods(:,5)  = (/  9 ,8 ,4 ,7  /) 
  mesh % lnods(:,6)  = (/  15, 13, 8, 9 /)  
  mesh % lnods(:,7)  = (/  4 ,3 ,1 ,2  /) 
  mesh % lnods(:,8)  = (/  8 ,6 ,3 ,4  /) 
  mesh % lnods(:,9)  = (/  13 ,11, 6, 8  /)

  mesh % ltype(1:9)  = QUA04

  mesh % coord(:,1 ) = (/   0.000000e+00_rp ,  1.000000e+00_rp /) 
  mesh % coord(:,2 ) = (/   0.000000e+00_rp ,  6.666667e-01_rp /) 
  mesh % coord(:,3 ) = (/   3.333333e-01_rp ,  1.000000e+00_rp /) 
  mesh % coord(:,4 ) = (/   3.333333e-01_rp ,  6.666667e-01_rp /) 
  mesh % coord(:,5 ) = (/   0.000000e+00_rp ,  3.333333e-01_rp /) 
  mesh % coord(:,6 ) = (/   6.666667e-01_rp ,  1.000000e+00_rp /) 
  mesh % coord(:,7 ) = (/   3.333333e-01_rp ,  3.333333e-01_rp /) 
  mesh % coord(:,8 ) = (/   6.666667e-01_rp ,  6.666667e-01_rp /) 
  mesh % coord(:,9 ) = (/   6.666667e-01_rp ,  3.333333e-01_rp /) 
  mesh % coord(:,10) = (/   0.000000e+00_rp ,  0.000000e+00_rp /) 
  mesh % coord(:,11) = (/   1.000000e+00_rp ,  1.000000e+00_rp /) 
  mesh % coord(:,12) = (/   3.333333e-01_rp ,  0.000000e+00_rp /) 
  mesh % coord(:,13) = (/   1.000000e+00_rp ,  6.666667e-01_rp /) 
  mesh % coord(:,14) = (/   6.666667e-01_rp ,  0.000000e+00_rp /) 
  mesh % coord(:,15) = (/   1.000000e+00_rp ,  3.333333e-01_rp /) 
  mesh % coord(:,16) = (/   1.000000e+00_rp ,  0.000000e+00_rp /) 

  print*,'b=',memory_size(mesh % ltypb),memory_size(mesh % lnodb),memory_size(mesh % lelbo)

  mesh % ltypb       = BAR02
  mesh % lnodb(:,1)  = (/  1,2 /)
  mesh % lnodb(:,2)  = (/  3,1 /)
  mesh % lnodb(:,3)  = (/  2,5 /)
  mesh % lnodb(:,4)  = (/  5,10 /)
  mesh % lelbo(:)    = (/ 7,7,4,1 /)

  print*,'c'
  call mesh % associate_boundary  ()
  print*,'d'
  call mesh % boundary % centroid (cbntr)

  print*,'centroids 1= ',cbntr(:,1)
  print*,'centroids 2= ',cbntr(:,2)
  print*,'centroids 3= ',cbntr(:,3)
  print*,'centroids 3= ',cbntr(:,4)

  if( abs(cbntr(1,1)-0.000000000000000_rp) > 1.0e-06_rp ) stop 1
  if( abs(cbntr(2,1)-0.833333350000000_rp) > 1.0e-06_rp ) stop 1 

  if( abs(cbntr(1,2)-0.166666650000000_rp) > 1.0e-06_rp ) stop 1
  if( abs(cbntr(2,2)-1.000000000000000_rp) > 1.0e-06_rp ) stop 1 

  if( abs(cbntr(1,3)-0.000000000000000_rp) > 1.0e-06_rp ) stop 1
  if( abs(cbntr(2,3)-0.500000000000000_rp) > 1.0e-06_rp ) stop 1 

  if( abs(cbntr(1,4)-0.000000000000000_rp) > 1.0e-06_rp ) stop 1
  if( abs(cbntr(2,4)-0.166666650000000_rp) > 1.0e-06_rp ) stop 1 

end program unitt_bmsh
