!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_ann_framework
  use def_kintyp_basic,   only : ip,rp
  use def_master,         only : rate_time, zeror
  use mod_ann_framework,  only : ann_framework
  use mod_ann_framework,  only : ANN_BACKEND_TORCH 
  use mod_ann_framework,  only : ANN_PRECISION_SINGLE 
  use mod_ann_framework,  only : ANN_PRECISION_DOUBLE 
  use mod_ann_scaling,    only : ANN_SCALING_LINEAR

  type(ann_framework)            :: framework 
  real(rp)                       :: input(3) 
  real(rp)                       :: output(1) 
  real(rp)                       :: input2(10,3) 
  real(rp)                       :: output2(10,1) 
  character(len=:), allocatable  :: char_pass
  integer(8)                     :: count_rate8

  !
  ! Initialize count for timers
  !
  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)

  !
  ! Set framework properties
  ! 
  call framework % init()
  call framework % set (index = int(1,kind=4))
  call framework % set (backend = ANN_BACKEND_TORCH )
  char_pass = "ANN_model.pt"
  call framework % set (path = char_pass )
  call framework % set (precision = ANN_PRECISION_SINGLE )

  call framework % scaling_in % set (n_dim = 1_ip)
  call framework % scaling_in % alloc_shape()
  call framework % scaling_in % set (i_shape =1_ip , n_shape=3_ip)
  call framework % scaling_in % alloc()
  call framework % scaling_in % set (inds = (/ 1_ip /),          &
      &                              typ=ANN_SCALING_LINEAR,     &
      &                              coeff=4.197958816023442_rp, &
      &                              shift=-0.024495088764061812_rp  )
  call framework % scaling_in % set (inds = (/ 2_ip /),          &
      &                              typ=ANN_SCALING_LINEAR,     &
      &                              coeff=3.9992557073567103_rp,&
      &                              shift=2.602362769721367e-13_rp  )
  call framework % scaling_in % set (inds = (/ 3_ip /),          &
      &                              typ=ANN_SCALING_LINEAR,     &
      &                              coeff=167.41571492671218_rp,&
      &                              shift=9.009126777925758e-12_rp  )

  call framework % scaling_out % set (n_dim = 1_ip)
  call framework % scaling_out % alloc_shape()
  call framework % scaling_out % set (i_shape =1_ip , n_shape=1_ip)
  call framework % scaling_out % alloc()
  call framework % scaling_out % set (inds = (/ 1_ip /),          &
      &                               typ=ANN_SCALING_LINEAR,     &
      &                               coeff=33.280543439850746_rp,&
      &                               shift=0.0007796904720459574_rp  )

  !
  ! Read file
  !
#ifdef TORCH
  print*, "Reading file", trim(framework % path)
  call initialize_neural_networks(1)
  call framework % read_file() 
  print*, "Done reading file"
#else
  print*, "Not reading ANN file because was not compiled with Torch"
#endif

  !
  ! Test non-vectorized evaluation 
  !
  input = 0.0
  input(1) = 0.1
  input(2) = 0.9
  input(3) = 0.02
  call framework % scaling_in % unscale(input, input) 
  print*, "Input=", input
  call framework % eval(input=input, output=output) 
  print*, "Output=", output
  call framework % scaling_out % scale(output, output) 
#ifdef TORCH
  print*, "Output=", output; if( abs( output(1)  - (0.846406400) ) > 1.0e-5_rp ) stop 1
#endif


  !
  ! Small vectorized test 
  !
  input2(:,1) = 0.1
  input2(:,2) = 0.9
  input2(:,3) = 0.02

  input2(2,1) = 0.11
  input2(2,2) = 0.9
  input2(2,3) = 0.02

  input2(3,1) = 0.1
  input2(3,2) = 0.92
  input2(3,3) = 0.02

  input2(4,1) = 0.1
  input2(4,2) = 0.9
  input2(4,3) = 0.03

  input2(5,1) = 0.11
  input2(5,2) = 0.92
  input2(5,3) = 0.03

  call framework % scaling_in % unscale(10_ip, input2, input2) 
  print*, "Input2=", input2
  call framework % eval(nvec=10_ip, input=input2, output=output2) 
  print*, "Output2=", output2
  call framework % scaling_out % scale(10_ip, output2, output2) 
  print*, "Output2=", output2
#ifdef TORCH
  print*, "output2(1,1)=", output2(1,1); if( abs( output2(1,1)  - (0.846406400) ) > 1.0e-5_rp ) stop 1
  print*, "output2(2,1)=", output2(2,1); if( abs( output2(2,1)  - (0.541784704) ) > 1.0e-5_rp ) stop 1
  print*, "output2(3,1)=", output2(3,1); if( abs( output2(3,1)  - (0.928905904) ) > 1.0e-5_rp ) stop 1
  print*, "output2(4,1)=", output2(4,1); if( abs( output2(4,1)  - (0.832901418) ) > 1.0e-5_rp ) stop 1
  print*, "output2(5,1)=", output2(5,1); if( abs( output2(5,1)  - (0.630887091) ) > 1.0e-5_rp ) stop 1
#endif


  !
  ! Test switching to Double precision at runtime
  ! 
  call framework % set (precision = ANN_PRECISION_DOUBLE )

  input = 0.0
  input(1) = 0.1
  input(2) = 0.9
  input(3) = 0.02
  call framework % scaling_in % unscale(input, input) 
  print*, "InputDP=", input
  call framework % eval(input=input, output=output) 
  print*, "OutputDP=", output
  call framework % scaling_out % scale(output, output) 
#ifdef TORCH
  print*, "OutputDP=", output; if( abs( output(1)  - (0.846406400) ) > 1.0e-5_rp ) stop 1
#endif

  input2(:,1) = 0.1
  input2(:,2) = 0.9
  input2(:,3) = 0.02

  input2(2,1) = 0.11
  input2(2,2) = 0.9
  input2(2,3) = 0.02

  input2(3,1) = 0.1
  input2(3,2) = 0.92
  input2(3,3) = 0.02

  input2(4,1) = 0.1
  input2(4,2) = 0.9
  input2(4,3) = 0.03

  input2(5,1) = 0.11
  input2(5,2) = 0.92
  input2(5,3) = 0.03

  call framework % scaling_in % unscale(10_ip, input2, input2) 
  print*, "Input2DP=", input2
  call framework % eval(nvec=10_ip, input=input2, output=output2) 
  print*, "Output2DP=", output2
  call framework % scaling_out % scale(10_ip, output2, output2) 
  print*, "Output2DP=", output2
#ifdef TORCH
  print*, "output2DP(1,1)=", output2(1,1); if( abs( output2(1,1)  - (0.846406400) ) > 1.0e-5_rp ) stop 1
  print*, "output2DP(2,1)=", output2(2,1); if( abs( output2(2,1)  - (0.541784704) ) > 1.0e-5_rp ) stop 1
  print*, "output2DP(3,1)=", output2(3,1); if( abs( output2(3,1)  - (0.928905904) ) > 1.0e-5_rp ) stop 1
  print*, "output2DP(4,1)=", output2(4,1); if( abs( output2(4,1)  - (0.832901418) ) > 1.0e-5_rp ) stop 1
  print*, "output2DP(5,1)=", output2(5,1); if( abs( output2(5,1)  - (0.630887091) ) > 1.0e-5_rp ) stop 1
#endif

end program unitt_ann_framework
