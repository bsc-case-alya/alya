!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_pararr(wtask,ntype,ndimr,rvarr)
  !------------------------------------------------------------------------
  !****f* Parall/par_pararr
  ! NAME
  !    par_pararr
  ! DESCRIPTION
  !    Works with arrays to deal with parallelization
  ! OUTPUT
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_kintyp,         only : ip,rp
  use def_master,         only : npari,nparr,nparc,parin,parre,parr1,kfl_paral
  use def_master,         only : party,parki,pardi,IPARALL,pard1,pari1,kfl_desti_par
  use def_master,         only : NPOIN_TYPE,NBOPO_TYPE,NBOUN_TYPE,NELEM_TYPE,lntra
  use def_domain,         only : npoin,nbopo,nboun,nelem
  use mod_communications, only : PAR_MIN,PAR_MAX,PAR_SUM
  use mod_par_slexca,     only : par_slexca
  implicit none
  character(3), intent(in) :: wtask
  integer(ip),  intent(in) :: ntype,ndimr
  real(rp),     target     :: rvarr(ndimr)

  if( IPARALL ) then

     npari = 0
     nparc = 0
     nparr = 0

     select case ( wtask )

     case ( 'SND' ) 

        nparr =  ndimr
        parre => rvarr
        call par_sendin() 

     case ( 'RCV' )

        nparr =  ndimr
        parre => rvarr
        call par_receiv() 

     case ( 'GAT' ) 

        if( ntype == NPOIN_TYPE .or. ntype == NBOPO_TYPE .or. ntype == NBOUN_TYPE .or. ntype == NELEM_TYPE ) then
           if( ntype == NPOIN_TYPE ) pard1 = ndimr/npoin
           if( ntype == NELEM_TYPE ) pard1 = ndimr/nelem
           if( ntype == NBOPO_TYPE ) pard1 = ndimr/max(1_ip,nbopo)
           if( ntype == NBOUN_TYPE ) pard1 = ndimr/max(1_ip,nboun)
           party = ntype
           if( pard1 == 1 ) then
              parki =  2
              pardi =  1
           else
              parki =  6
              pardi =  1
           end if
        else
           party =  ntype
           parki =  2
           pardi =  1
           npari =  ndimr
        end if
        parr1 => rvarr
        call par_mygather()

     case ( 'IBI' ) 

        pari1 => lntra
        pard1 =  ndimr/npoin
        parr1 => rvarr(1:ndimr)
        call par_slexib(2_ip)

     case default

        call runend('PARARR: WRONG CASE')

     end select

     nparr = 0
     nullify(parre)

  end if

end subroutine par_pararr
 
