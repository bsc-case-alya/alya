!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_interpolate_nearest_node 

  use def_kintyp
  use def_master
  use def_domain
  use mod_elmgeo
  use def_elmtyp
  use def_maths_bin
  use def_maths_tree
  use def_kermod
  use def_search_method
  use mod_parall
  use def_interpolation_method
  use mod_memory_basic
  use mod_maths_geometry
  use mod_random
  implicit none
  type(mesh_type_basic)  :: mesh
  real(rp),    parameter :: eps = 1.0e-10_rp
  real(rp),    pointer   :: bobox(:,:,:)
  integer(ip)            :: pelty,ielem,inode,ipoin,ii,nn,kpoin,jj,imeth
  type(maths_bin)        :: bin_seq
  type(maths_octree)     :: oct_seq
  real(rp),    pointer   :: xx(:,:),vv(:,:),dd(:,:)
  real(rp),    pointer   :: xcoor(:)
  type(interpolation)    :: interp
  integer(8)             :: count_rate8
  integer(8)             :: memor_loc(2)
  integer(8)             :: memor_tmp(2) 
  real(rp)               :: dimin,dista,rpoin

  nullify(bobox)
  nullify(xcoor)
  nullify(xx)
  nullify(vv)
  nullify(dd)

  memor_loc = 0_8
  memor_tmp = 0_8
  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)
  !
  ! Create mesh
  !
  call elmgeo_element_type_initialization()
  !
  ! Mesh
  !
  call mesh % init('MY_MESH')

  mesh % nelem = 9
  mesh % npoin = 1000
  mesh % ndime = 2
  mesh % mnode = 4
  call mesh % alloca()
  call random_initialization()
  !
  ! random
  !  
  do ii = 1,mesh % npoin
     mesh % coord(1,ii) = random_generate_number()
     mesh % coord(2,ii) = random_generate_number()
  end do
  nn = 100
  allocate(xx(2,nn))
  allocate(vv(2,nn))
  do ii = 1,nn
     xx(1,ii) = -1.0_rp + 3.0_rp * random_generate_number()
     xx(2,ii) = -1.0_rp + 3.0_rp * random_generate_number()
  end do
!!$  rpoin = sqrt(real(mesh % npoin,rp))
!!$  do jj = 1,int(rpoin,ip)
!!$     do ii = 1,int(rpoin,ip)
!!$        ipoin = (jj-1)*int(rpoin,ip) + ii
!!$        mesh % coord(1,ipoin) = real(ii,rp)
!!$        mesh % coord(2,ipoin) = real(jj,rp)
!!$     end do
!!$  end do
!!$  nn = mesh % npoin
!!$  allocate(xx(2,nn))
!!$  allocate(vv(2,nn))
!!$  rpoin = sqrt(real(mesh % npoin,rp))
!!$  do jj = 1,int(rpoin,ip)
!!$     do ii = 1,int(rpoin,ip)
!!$        ipoin = (jj-1)*int(rpoin,ip) + ii
!!$        xx(1,ipoin) = real(ii,rp)
!!$        xx(1,ipoin) = real(jj,rp)
!!$     end do
!!$  end do
  !
  ! Sequential search meth
  !
  do imeth = 1,2

     print*,'----------------------------------------'
     if( imeth == 1 ) then 
        ii = max(1_ip,int(20.0_rp*random_generate_number(),ip))
        jj = max(1_ip,int(20.0_rp*random_generate_number(),ip))
        print*,'constructing bin 1=',ii,jj
        call bin_seq % init     ()
        call bin_seq % input    (BOXES=(/ii,jj/)) 
        call bin_seq % fill     (coord=mesh % coord)
        !
        ! Interpolation
        !
        call interp % init      ()
        call interp % input     (bin_seq,INTERPOLATION_METHOD=INT_NEAREST_NODE)
        call interp % preprocess(xx,mesh)
        call interp % values    (mesh % coord,vv)
        print*,'ave. # elements per bin=  ',bin_seq % stats(1)
        print*,'max. # elements in a bin= ',bin_seq % stats(2)
        print*,'saving ratio [0,1]=       ',bin_seq % stats(5)
        print*,'time fill=                ',bin_seq % times(1)
        print*,'time candidate=           ',bin_seq % times(2)
        print*,'max memory                ',bin_seq % memor(2)
     else
        print*,'constructing octree'
        call oct_seq % init     ()
        !call oct_seq % input    (limit=10_ip,METHOD=METHOD_KDTREE,ADJUST_BOUNDING_BOXES=.true.) 
        call oct_seq % input    (limit=10_ip,ADJUST_BOUNDING_BOXES=.true.) 
        call oct_seq % fill     (coord=mesh % coord)
        !
        ! Interpolation
        !
        call interp % init      ()
        call interp % input     (oct_seq,INTERPOLATION_METHOD=INT_NEAREST_NODE)
        call interp % preprocess(xx,mesh)
        call interp % values    (mesh % coord,vv)     
        print*,'# leaves=                 ',oct_seq % stats(1)
        print*,'% addition element=       ',oct_seq % stats(2)
        print*,'max level=                ',oct_seq % stats(3)
        print*,'max elements in box=      ',oct_seq % stats(4)
        print*,'saving ratio [0,1]=       ',oct_seq % stats(5)
        print*,'time fill=                ',oct_seq % times(1)
        print*,'time candidate=           ',oct_seq % times(2)
        print*,'max memory                ',oct_seq % memor(2)
     end if
     !
     ! Check
     !
     print*,'check results'
     do ii = 1,nn
        dimin = huge(1.0_rp)
        do ipoin = 1,mesh % npoin
           dista = (mesh % coord(1,ipoin) - xx(1,ii))**2 + (mesh % coord(2,ipoin) - xx(2,ii))**2
           dista = sqrt(dista)
           if( dista < dimin ) then
              dimin = dista
              kpoin = ipoin
           end if
        end do
        dista = (vv(1,ii) - xx(1,ii))**2 + (vv(2,ii) - xx(2,ii))**2
        dista = sqrt(dista)
        if( abs(dista-dimin) > 1.0e-12_rp ) then
           print*,'xx=   ',xx(:,ii)
           print*,'vv=   ',vv(:,ii)
           print*,'coord=',mesh % coord(:,kpoin)
           print*,'wrong result=',dista,dimin
           stop 1
        end if
     end do

     if( imeth == 1 ) call bin_seq % deallo()
     if( imeth == 2 ) call oct_seq % deallo()
     call interp  % deallo()

  end do

end program unitt_interpolate_nearest_node
