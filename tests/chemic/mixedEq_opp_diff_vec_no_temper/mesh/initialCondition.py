#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import sys
import os
import math
import numpy as np
from scipy.special import erf

def strainFlow(x,y,a,width=np.inf):
	if abs(x) < width/2.0:
		U =  a * x
		V = -1.0 * a * y
	else:
		U = a * width/2.0 * x/abs(x)
		V = 0.0

	return(U,V)

print(sys.argv)

meshName	= sys.argv[1]
strain	  =  float(sys.argv[2])

coordfile	   = '{}.coord'.format(meshName)

velofile		= 'VELOC.alya'
enthfile		= 'ENTHA.alya'
zfile			= 'Z.alya'
ycfile			= 'YC.alya'



diff						  = 4.53293789309e-05 
lengthScale					  = ( 2.0 * diff / strain )**0.5
print('Length scale: {}'.format(lengthScale))


ymin =  np.inf
ymax = -np.inf
fCoord		   =open(coordfile,'r')
for line in fCoord:
	data=line.split()

	pid = int(data[0])
	dims = len(data)-1
	x   = float(data[1])
	y   = float(data[2])
	ymax = max(ymax,y)
	ymin = min(ymin,y)
fCoord.close()

widthMom  = 1.0 * (ymax-ymin)
widthScal = 1.5 * (ymax-ymin)
print('Total thickness of domain: {}'.format(ymax-ymin))
U,V = strainFlow(0.0,ymax,strain)
print('Downward velocity: {}'.format(V))
U,V = strainFlow(0.0,ymin,strain)
print('Upward velocity:   {}'.format(V))


fCoord		   =open(coordfile,'r')
fVelo		   =open(velofile,'w')
fEnth		   =open(enthfile,'w')
fZ   		   =open(zfile,'w')
fYc  		   =open(ycfile,'w')

print('---| Start writing initial condition')

for line in fCoord:
	data=line.split()

	pid = int(data[0])
	dims = len(data)-1
	x   = float(data[1])
	y   = float(data[2])


	U,V = strainFlow(x,y,strain,width=widthMom)

	zeta = y / lengthScale
	Z = 0.5 * (1.0 - erf( zeta ))
	Yc = 1.0
	if abs(x) > widthScal/2.0:
		Yc = 0.0
		Z  = 0.0

	H = 157862.724475 + (313423.49325-157862.724475)*Z

	#
	# Write IC  
	#
	fVelo.write('{} {} {}\n'.format(pid,U,V))
	fEnth.write('{} {}\n'.format(pid,H))
	fZ.write('{} {}\n'.format(pid,Z))
	fYc.write('{} {}\n'.format(pid,Yc))
		

fCoord.close()
fVelo.close()
fEnth.close()
fZ.close()
fYc.close()
print('---| End writing initial condition')



