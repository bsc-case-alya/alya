!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Parall
!> @{
!> @file    par_turnon.f90
!> @author  houzeaux
!> @date    2020-05-08
!> @brief   Start Parall 
!> @details Turn on parall
!> @} 
!-----------------------------------------------------------------------

subroutine par_turnon()
  use def_kintyp_basic, only : rp
  use mod_outfor,       only : outfor
  use def_parall
  use mod_parall
  real(rp) :: time1,time2

  call cputim(time1)
  !
  ! Broadcast data
  !
  call par_inidat()  
  call par_sendat(1_ip)                                
  !
  ! Open files
  !
  call par_openfi(0_ip)
  call par_openfi(1_ip)
  call outfor(27_ip,lun_outpu_par,' ')
  
  call cputim(time2)
  cpu_paral(2)=time2-time1

end subroutine par_turnon
