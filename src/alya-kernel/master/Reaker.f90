!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!> @addtogroup Turnon
!> @{
!> @file    Turnon.f90
!> @author  Guillaume Houzeaux
!> @brief   Turnon the run
!> @details Read Kermod to define some important parameters before the
!>          construction of the domain
!>
!> @}
!-----------------------------------------------------------------------
subroutine Reaker()

  use def_kintyp, only : ip
  use def_master, only : ITASK_TURNON
  use def_master, only : kfl_check_data_file

  implicit none
  
  !----------------------------------------------------------------------
  !
  ! Export mesh
  !
  !----------------------------------------------------------------------

  call mpio_export_domain()

  !----------------------------------------------------------------------
  !
  ! Read kermod data... AND
  ! do some calculations required by Kermod= NMATE, NSUBD
  !
  !----------------------------------------------------------------------

  call domvar(0_ip)

  call Kermod(-ITASK_TURNON)
  if( kfl_check_data_file == 1) call runend('O.K.!')
  !
  ! Turn on coupling
  !
  call cou_turnon()
  
  !----------------------------------------------------------------------
  !
  ! Open domain files
  !
  !----------------------------------------------------------------------

  call opfdom(4_ip)

end subroutine Reaker
