!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_exampl()
  !-----------------------------------------------------------------------
  !****f* Parall/par_exampl
  ! NAME
  !    par_exampl
  ! DESCRIPTION
  !    This routine exchange data 
  ! USES
  ! USED BY
  !    Reapro
  !***
  !-----------------------------------------------------------------------
  use def_domain
  use def_master
  use def_parall
  implicit none
  integer(ip) :: ix,iy,iz,ipx,ipy,ipz,ielem

  npart_par=nxexp*nyexp*nzexp
  npart=npart_par
  ielem=0
  do iy=1,nyexa
     ipy=int(real(iy-1)/real(nyexa)*real(nyexp))
     do iz=1,nzexa
        ipz=int(real(iz-1)/real(nzexa)*real(nzexp))
        do ix=1,nxexa
           ielem=ielem+1
           ipx=int(real(ix-1)/real(nxexa)*real(nxexp))
           lepar_par(ielem)=ipy*nxexp*nzexp+ipz*nxexp+ipx+1
        end do
     end do
  end do

end subroutine par_exampl
