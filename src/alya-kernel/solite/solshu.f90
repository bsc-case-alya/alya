!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine solshu(nzmat,ndofn,npoin,rhsid,unkno,amatr)
  !------------------------------------------------------------------------
  !****f* solshu/solshu
  ! NAME 
  !    solshu
  ! DESCRIPTION
  !    Bridge to Schur solver
  ! USES
  ! USED BY 
  !***
  !------------------------------------------------------------------------
  use def_master, only    :   INOTMASTER,IMASTER
  use def_master, only    :   aii,aib,abi,abb,bbi,bbb,xxi,xxb
  use def_domain, only    :   npoin_ii
  use def_solver 
  implicit none
  integer(ip), intent(in) :: ndofn
  integer(ip), intent(in) :: nzmat
  integer(ip), intent(in) :: npoin
  real(rp),    target     :: rhsid(npoin*ndofn)
  real(rp),    target     :: unkno(npoin*ndofn)
  real(rp),    target     :: amatr(nzmat*ndofn)
  real(rp)                :: dummv(1),dummm(1,1)
  integer(ip)             :: poaii,poaib,poabi,poabb
  !
  ! Bridge to Schur complement solver
  ! 
  if( INOTMASTER ) then
     poaii =  1
     poaib =  poaii + nzdom_aii * solve_sol(1) % ndof2
     poabi =  poaib + nzdom_aib * solve_sol(1) % ndof2
     poabb =  poabi + nzdom_abi * solve_sol(1) % ndof2
     aii   => amatr(poaii:)
     aib   => amatr(poaib:)
     abi   => amatr(poabi:)
     abb   => amatr(poabb:)
     bbi   => rhsid
     bbb   => rhsid(npoin_ii*ndofn+1:)
     xxi   => unkno
     xxb   => unkno(npoin_ii*ndofn+1:)
  end if
  if( IMASTER ) then
     call par_cgschu(solve_sol(1)%ndofn,dummv,&       ! Schur complement solver
          dummv,dummv,dummv,dummm,dummm,dummm,dummm)
  else
     call par_cgschu(solve_sol(1)%ndofn,bbi,&
          bbb,xxi,xxb,aii,aib,abi,abb)
  end if

end subroutine solshu
