!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!------------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_gpu_create.f90
!> @author  Guillermo Oyarzun
!> @brief   Create data in GPU
!> @details Allocates GPU data for the repartitioning
!> @}
!------------------------------------------------------------------------
subroutine nsi_gpu_create()

  use def_kintyp
  use def_master
  use def_domain
  use def_nastin

  use mod_memory_basic


#ifdef OPENACCHHH
  use def_parall,                only : kfl_cores_per_gpu
  use openacc
#endif
  integer(ip)::  ndim1_old
  integer(ip)::  ndim2_old
  integer(ip)::  ndim3_old



  ndim1_old = memory_size(veloc,1_ip)
  ndim2_old = memory_size(veloc,2_ip)
  ndim3_old = memory_size(veloc,3_ip)



#ifdef OPENACCHHH
  if (kfl_paral /= 0 ) then
    
     if( kfl_savda == 0 ) then
        !$acc enter data copyin (coord,ltype,lnods,lnodb,gravi_nsi, &
        !$acc                   dt_rho_nsi,mass_rho_nsi, rhsid,     &
        !$acc                   veloc)

     else
        !$acc enter data copyin (coord,ltype,lnods,lnodb,gravi_nsi, &
        !$acc                   elmda_gpvol, elmda_gpcar ,          &
        !$acc                   dt_rho_nsi, mass_rho_nsi, rhsid,    &
        !$acc                   veloc)
     end if


     if(associated(velom)) then
            !$acc enter data copyin(velom)
     end if

  end if  
#endif

end subroutine nsi_gpu_create

