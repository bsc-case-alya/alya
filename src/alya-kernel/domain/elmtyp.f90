!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine elmtyp()
  !-----------------------------------------------------------------------
  !****f* Domain/elmtyp
  ! NAME
  !    cderda
  ! DESCRIPTION
  !    This routine defines the different types of elements:
  !
  !    NNODE ... number fo nodes
  !
  !                 1D           2D           3D
  !                 ----------------------------------
  !    LTOPO ... -1 Lines          -
  !               0    -    Quadrilateral  Hexahedra
  !               1    -    Triangle       Tetrahedra
  !               2    -           -       Pentahedra
  !               3    -           -       Pyramid
  !
  !    LLAPL ... 1 if Hessian matrix should be considered
  !              0 otherwise
  !
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------

  use def_kintyp_basic, only : ip
  use def_domain,       only : nelty,lenex
  use def_domain,       only : ldime,lorde
  use def_domain,       only : ltopo,nnode
  use def_domain,       only : llapl
  use def_domain,       only : mface
  use def_elmgeo,       only : element_type

  implicit none
  integer(ip) :: pelty,inode,pnode

  !---------------------------------------------------------------------
  !
  ! Copy element databse to global variable
  !
  !---------------------------------------------------------------------
  
  do pelty = 1,size(element_type)
     ldime(pelty) = element_type(pelty) % dimensions
     lorde(pelty) = element_type(pelty) % order
     ltopo(pelty) = element_type(pelty) % topology
     nnode(pelty) = element_type(pelty) % number_nodes
     if( element_type(pelty) % hessian ) then
        llapl(pelty) = 1
     else
        llapl(pelty) = 0
     end if
  end do
  
  !---------------------------------------------------------------------
  !
  ! Derived parameters
  !
  !---------------------------------------------------------------------
  !
  ! Next element node
  !
  do pelty = 1,nelty
     pnode = nnode(pelty)
     if( pnode > 0 ) then
        do inode = 1,pnode-1
           lenex(inode,pelty) = inode + 1
        end do
        lenex(pnode,pelty) = 1
     end if
  end do
  !
  ! Mirror NNODE
  !
  do pelty = 1,nelty
     nnode(-pelty) = nnode(pelty)
  end do
  mface = maxval(element_type(:) % number_faces)
  
end subroutine elmtyp
