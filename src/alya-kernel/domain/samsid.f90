!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine samsid(orige,desti,poin1,poin2,ndime,istru)
  !-----------------------------------------------------------------------
  ! NAME
  !    samsid
  ! DESCRIPTION
  !    Determine if a point is inside a triangle using the same side technique
  ! USED BY
  !    insiib
  !***
  !----------------------------------------------------------------------- 

  use def_kintyp
  
  implicit   none
  integer(ip), intent(in)   :: ndime
  integer(ip), intent(out)  :: istru
  integer(ip)               :: idime
  real(rp),    intent(in)   :: poin1(ndime),poin2(ndime),orige(ndime),desti(ndime)
  real(rp)                  :: side1(3),side2(3),side3(3),cp1(3),cp2(3),resul
  
  side1(3) = 0.0_rp
  side2(3) = 0.0_rp 
  side3(3) = 0.0_rp 
  do idime=1,ndime
     side1(idime) = desti(idime) - orige(idime)
     side2(idime) = poin1(idime) - orige(idime)
     side3(idime) = poin2(idime) - orige(idime)
  end do
  
  call vecpro(side1,side2,cp1,3)
  call vecpro(side1,side3,cp2,3)
  
  resul=0.0_rp
  do idime = 1,ndime
     resul = resul + (cp1(idime) * cp2(idime))
  end do
  if (resul >= 0) then
     istru = 1
  else
     istru = 0
  end if
  
end subroutine samsid
