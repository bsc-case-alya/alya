!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_errors(itask)
!------------------------------------------------------------------------
!****f* Parall/par_errors
! NAME
!    par_errors
! DESCRIPTION
!    Check errors
! OUTPUT
! USED BY
!    Parall
!***
!------------------------------------------------------------------------
  use      def_kintyp
  use      def_domain
  use      def_master
  use      def_parall
  implicit none
  integer(ip), intent(in) :: itask

  if(kfl_paral==0) then

     select case(itask)

     case(1)
        if(rp/=8) then
           call runend('PARALL: ONLY WORKS WITH DOUBLE PRECISION')
        end if
        
        !if(ip/=4) then
        !   call runend('PARALL: ONLY WORKS WITH 4-BYTE INTEGERS')
        !end if
        
        if(kfl_autbo==1) then
           call runend('PARALL: CANNOT USE AUTOMATIC BOUNDARY GENERATION')
        end if
        
        !if(kfl_ptask==2.and.kfl_postp_par==1) then
        !   call runend('PARALL: WHEN DOING RESTART, MASTER CANNOT BE IN CHARGE OF POSTPROCESS')
        !end if
        
     case(2)

     end select

  end if

end subroutine par_errors
