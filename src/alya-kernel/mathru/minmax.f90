!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine minmax(ndim1,ndim2,ndim3,vecto,vemin,vemax)
  !------------------------------------------------------------------------
  !****f* mathru/minmax
  ! NAME 
  !    minmax
  ! DESCRIPTION
  !    Compute the minimum and maximum of a vector 
  ! USES
  ! USED BY
  !    Modules: *_cvgunk
  !***
  !------------------------------------------------------------------------
  use      def_kintyp
  use      def_master
  use      mod_communications, only : PAR_MIN
  use      mod_communications, only : PAR_MAX
  implicit none
  integer(ip), intent(in)  :: ndim1,ndim2,ndim3
  real(rp),    intent(in)  :: vecto(ndim1,ndim2)
  real(rp),    intent(out) :: vemin,vemax
  integer(ip)              :: idim1,idim2,ndim4
  real(rp)                 :: uvalu
  real(rp),    target      :: vtmin(1),vtmax(1)

  vemin= huge(1.0_rp)
  vemax=-huge(1.0_rp)

  if(kfl_paral==-1.or.(kfl_paral==0.and.kfl_ptask==0)) then
     ! 
     ! Sequential case
     !
     if(ndim1==1) then
        do idim2=1,ndim2
           if(vecto(1,idim2)>vemax) vemax=vecto(1,idim2)
           if(vecto(1,idim2)<vemin) vemin=vecto(1,idim2)
        end do

     else if(ndim3<0) then
        ndim4=-ndim3
        do idim2=1,ndim2
           if(vecto(ndim4,idim2)>vemax) vemax=vecto(ndim4,idim2)
           if(vecto(ndim4,idim2)<vemin) vemin=vecto(ndim4,idim2)
        end do

     else
        ndim4=min(ndim1,ndim3)
        do idim2=1,ndim2
           uvalu=0.0_rp
           do idim1=1,ndim4
              uvalu=uvalu+vecto(idim1,idim2)*vecto(idim1,idim2)
           end do
           uvalu=sqrt(uvalu)
           if(uvalu>vemax) vemax=uvalu
           if(uvalu<vemin) vemin=uvalu
        end do
     end if

  else
     !
     ! Parallel case
     !
     if(kfl_paral>=1) then
        if(ndim1==1) then
           do idim2=1,ndim2
              if(vecto(1,idim2)>vemax) vemax=vecto(1,idim2)
              if(vecto(1,idim2)<vemin) vemin=vecto(1,idim2)
           end do
        else
           ndim4=min(ndim1,ndim3)
           do idim2=1,ndim2
              uvalu=0.0_rp
              do idim1=1,ndim4
                 uvalu=uvalu+vecto(idim1,idim2)*vecto(idim1,idim2)
              end do
              uvalu=sqrt(uvalu)
              if(uvalu>vemax) vemax=uvalu
              if(uvalu<vemin) vemin=uvalu
           end do
        end if
     end if
     !
     ! Minimum
     !
     call PAR_MIN(vemin)
     !
     ! Maximum
     ! 
     call PAR_MAX(vemax)
    
  end if

end subroutine minmax
