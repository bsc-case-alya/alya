!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine reasla
!-----------------------------------------------------------------------
!****f* Domain/reasla
! NAME
!    reasla
! DESCRIPTION
!    LMASL(IPOIN) = IMASL  >  0: IPOIN is the master of IMASL
!                          <  0: IPOIN is the slave of -IMASL
!                          =  0: IPOIN is neither a master nor a slave
!    This routine also calls to the routines that creare R_SOL, C_SOL
!    and PERMX.
! USED BY
!    cresla
!***
!-----------------------------------------------------------------------
  use      def_domain
  use      def_master
  use      def_inpout
  implicit none
!  integer(ip) :: islav,imast
!  real(rp)    :: vslav(9),vmast(9)
!  logical(lg) :: slerr
      
end subroutine reasla

