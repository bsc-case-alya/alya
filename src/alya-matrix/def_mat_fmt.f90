!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Maths
!> @{
!> @file    def_mat_csr.f90
!> @author  guillaume
!> @date    2021-01-26
!> @brief   Matrix
!> @details Matrix formats
!-----------------------------------------------------------------------

module def_mat_fmt

  use def_mat
  use def_mat_csr, only : mat_csr
  use def_mat_coo, only : mat_coo
  use def_mat_ell, only : mat_ell
  use def_mat_den, only : mat_den
  use def_mat_blk, only : mat_blk
  use def_mat_dia, only : mat_dia
  use def_mat_tri, only : mat_tri

end module def_mat_fmt
!> @}
