!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine arrind(&
     nlist,ncoef,liste,touch,nzdom,ipoin)
  !-----------------------------------------------------------------------
  !                 
  ! This routine constructs the arrays of indexes for a mesh graph.
  ! These are organized as follows (CSR format):
  !   
  ! R_DOM(IPOIN) = coefficient of the graph where IPOIN starts,
  ! C_DOM(NZDOM) = column of the NZDOM coefficient of the graph.
  !              
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp,lg
  use def_domain, only       :  mnode,lnods,r_dom,c_dom,lnnod
  implicit none
  integer(ip), intent(in)    :: nlist,ncoef,ipoin
  integer(ip), intent(in)    :: liste(nlist)
  integer(ip), intent(inout) :: nzdom
  logical(lg), intent(inout) :: touch(ncoef)
  integer(ip)                :: jelem,jnode,jpoin,nnodj,jposi,jlist
  integer(ip)                :: kelem,knode,kpoin,nnodk,kposi,klist

  r_dom(ipoin) = nzdom

  do jlist = 1,nlist
     jelem = liste(jlist)
     nnodj = lnnod(jelem)
     !nnodj = nnode(ltype(jelem))
     do jnode = 1,nnodj
        jpoin = lnods(jnode,jelem)
        jposi = (jlist-1)*mnode+jnode
        if(.not.touch(jposi)) then
           do klist = 1,nlist
              kelem = liste(klist)
              nnodk = lnnod(kelem)
              !nnodk = nnode(ltype(kelem))
              do knode = 1,nnodk
                 kpoin = lnods(knode,kelem)
                 if(kpoin==jpoin) then
                    kposi = (klist-1)*mnode+knode
                    touch(kposi) = .true.
                 end if
              end do
           end do
           c_dom(nzdom) = jpoin
           nzdom = nzdom+1
        end if
     end do
  end do

end subroutine arrind
