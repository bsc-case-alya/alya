!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_maths_maxloc_nonzero
  !
  ! Get last non-zero position
  !
  use def_kintyp_basic, only : ip, rp
  use mod_maths_arrays

  implicit none

  integer(ip) :: ii,ll(3)

  ll = 0
  ii = maths_maxloc_nonzero(ll)
  if( ii /= 0 ) then
     print*,'Failure a' ;stop 1
  end if
  
  ll = (/1,2,0/)
  ii = maths_maxloc_nonzero(ll)
  if( ii /= 2 ) then
     print*,'Failure b' ;stop 1
  end if
  
  ll = (/3,1,2/)
  ii = maths_maxloc_nonzero(ll)
  if( ii /= 3 ) then
     print*,'Failure c' ;stop 1
  end if
  
end program unitt_maths_maxloc_nonzero
