#
# --|  Columns displayed:
# --|  1. Time step                 2. Global iteration        3. Current time          4. Overall time
# --|  5. Count element assembly    6. Max element assembly    7. Ave element assembly  
# --|  8. Count boundary assembly   9. Max boundary assembly  10. Ave boundary assembly 
# --| 11. Count node assembly      12. Max node assembly      13. Ave node assembly     
# --| 14. Count particle assembly  15. Max particle assembly  16. Ave particle assembly 
# --| 17. Solvers
# --------- --------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- 
# Time step Global it Current time  Overall time  # elem. ass.  Max elem. ass Ave elem. ass # boun. ass.  Max boun. ass Ave boun. ass # node  ass.  Max node  ass Ave node  ass # part. ass.  Max part. ass Ave part  ass  Solvers
# --------- --------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- ------------- 
