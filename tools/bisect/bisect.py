import sys
import os
import subprocess as subp
import signal
import re
import time


def option(value, short, long=""):
    return value == short or value == long and long != ""


def command(line, stdout=False, output=False, timeout=1200):
    if output:
        stdout = True
    if not stdout:
        line += ""
    if output:
        line += ""
        output = ""
        try:
            p = subp.Popen(line,
                           stdout=subp.PIPE,
                           stderr=subp.STDOUT,
                           shell=True)

            output, err = p.communicate(timeout=timeout)
        except subp.TimeoutExpired:
            os.kill(p.pid, signal.SIGTERM)
            output, err = p.communicate()
            return output.decode('utf-8')+"\nEXECUTION TIME OUT"
        else:
            return output.decode('utf-8')
    try:
        p = subp.Popen(line, stdout=subp.PIPE, shell=True, stderr=subp.PIPE)
        output, err = p.communicate(timeout=timeout)
    except subp.TimeoutExpired:
        os.kill(p.pid, signal.SIGTERM)
        return False
    else:
        if p.returncode != 0:
            return False
    return True


def get_sha_list(commit_list):
    sha_list = []
    f = open(commit_list, 'r')
    data = f.read()
    data = data.split("\n")
    for line in data:
        l = line.split(", ")
        try:
            sha_list.append(l[1])
        except:
            break
    f.close()
    return sha_list


def is_sha_in_sha_list(sha, sha_list):
    if sha in sha_list:
        return sha
    else:
        for s in sha_list:
            if s.startswith(sha):
                return s
    return None


def get_new_index(min, max):
    return int((max-min) / 2 + min)


def run_job(job_name, commit, user, host, path):
    output = command("ssh " + user + "@" + host + " ' cd " + path + ' && sbatch --export=SHA="' + commit + '" ' + job_name + "'", output=True)
    print(output)
    try:
        job_id = re.findall("Submitted batch job \d+", output)[0]
        job_id = int(re.findall("\d+", job_id)[0])
    except:
        print("Cannot launch JOB!")
        raise Exception
    while True:
        time.sleep(60)
        output = command("ssh " + user + "@" + host + " sacct --jobs " + str(job_id), output=True)
        output = output.split("\n")[2]
        job_status = output.split()[5]
        if job_status in ["COMPLETED"]:
            return True
        elif job_status in ["FAILED"]:
            return False
        elif job_status in ["RUNNING", "PENDING"]:
            continue
        else:
            print("JOB status error: " + job_status)
            raise Exception


def run_test(commit, user, host, path):
    if not run_job("COMPILATION.SB", commit, user, host, path):
        print("Compilation failed!")
        raise Exception
    return run_job("SIMULATION.SB", commit, user, host, path)


def print_status(sha, status):
    if status is None:
        return
    elif status:
        print("sha: " + sha + ": " + "good")
    else:
        print("sha: " + sha + ": " + "bad")


user = None
host = None
test_path = None
commit_list = None
commit_good = None
commit_bad = None

for i in range(len(sys.argv)):
    if option(sys.argv[i], "--user"):
        user = sys.argv[i+1]
    if option(sys.argv[i], "--host"):
        host = sys.argv[i+1]
    if option(sys.argv[i], "--test-path"):
        test_path = sys.argv[i+1]
    if option(sys.argv[i], "--commit-list"):
        commit_list = sys.argv[i+1]
    if option(sys.argv[i], "--good"):
        commit_good = sys.argv[i + 1]
    if option(sys.argv[i], "--bad"):
        commit_bad = sys.argv[i + 1]


if user is None:
    print("Missing user path")
    exit(1)
if host is None:
    print("Missing host path")
    exit(1)
if test_path is None:
    print("Missing test path")
    exit(1)
if commit_list is None:
    print("Missing commit list")
    exit(1)
if commit_good is None:
    print("Missing commit good")
    exit(1)
if commit_bad is None:
    print("Missing commit bad")
    exit(1)

sha_list = get_sha_list(commit_list)
good = is_sha_in_sha_list(commit_good, sha_list)
if good is None:
    print("Commit good is not in the commit list")
else:
    print("Good commit is " + good)
bad = is_sha_in_sha_list(commit_bad, sha_list)
if bad is None:
    print("Commit bad is not in the commit list")
else:
    print("Bad commit is " + bad)
sha_list = sha_list[sha_list.index(bad):sha_list.index(good)+1]
sha_status = {}
for sha in sha_list:
    sha_status[sha] = None
sha_status[bad] = False
sha_status[good] = True
sha_list_t = sha_list

while len(sha_list_t) > 2:
    index = get_new_index(0, len(sha_list_t)-1)
    current_commit = sha_list_t[index]
    print("Current commit " + current_commit + " (index: " + str(sha_list.index(current_commit)) + ")")
    sha_status[current_commit] = run_test(current_commit, user, host, test_path)
    if sha_status[current_commit]:
        sha_list_t = sha_list_t[0:index+1]
    else:
        sha_list_t = sha_list_t[index:-1]
    for sha in sha_list:
        status = sha_status[sha]
        print_status(sha, status)





