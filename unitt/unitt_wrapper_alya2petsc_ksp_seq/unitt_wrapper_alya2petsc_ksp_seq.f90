!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_wrapper_alya2petsc_ksp_seq

    use def_kintyp
    use def_domain
    use def_master
    use def_parame
    use mod_memory
    use def_kermod
    use def_elmtyp
    use mod_memory
    use mod_elmgeo
    use mod_parall
    use mod_alya2petsc
    use mod_communications
    
    implicit none

    integer(ip)         :: aa, it, ii, jj
    real(rp)            :: rnorm, unorm
    real(rp), pointer   :: resid(:) => null()
    type(soltyp)        :: solver


    ! ------------------------------------------------- |
    ! Create Alya structures assembing a Elasticity eq.
    ! ------------------------------------------------- |

    ! Read geometry from Alya files
    call Initia()  ! Initialization of the run
    call Readom()  ! Domain reading
    call Partit()  ! Domain partitioning
    call Reaker()  ! Read Kermod
    call Domtra()  ! Domain transformation
    call Domain()  ! Domain construction

    ! Read some useful variables
    if( INOTMASTER )then
       nzrhs = (size(meshe(ndivi) % r_dom(:)) - 1_ip) * ndime
       nzmat = (size(meshe(ndivi) % c_dom(:))) * ndime**2
    endif

    ! Allocate memory and dimensions for the solver
    ! emmulating soldef
    if( INOTMASTER )then
       solver % ndofn = ndime
       solver % ndof2 = solver % ndofn ** 2
       solver % nequa = meshe(ndivi) % npoin
       solver % nequ1 = meshe(ndivi) % npoi1
       solver % nequ2 = meshe(ndivi) % npoi2
       solver % nequ3 = meshe(ndivi) % npoi3 
       solver % nequa_own = meshe(ndivi) % npoin_own
       solver % nunkn = solver % ndofn * solver % nequa
       solver % ncols = solver % ndofn * solver % nequa
       solver % nzmat = nzsol * solver % ndof2
       solver % ia => meshe(ndivi) % r_dom
       solver % ja => meshe(ndivi) % c_dom
       solver % ia_full => meshe(ndivi) % r_dom_own
       solver % ja_full => meshe(ndivi) % c_dom_own
       solver % conf_file = 'petscconf'
    endif

    ! Allocate memory for the RHS and AMATR
    if( INOTMASTER )then
       if( .not. associated(kfl_fixno) ) allocate(kfl_fixno(ndime,max(1,npoin)));  kfl_fixno = 0_ip
       if( .not. associated(displ) ) allocate(displ(ndime,max(1,npoin),1)); displ = 0.0_rp
       if( .not. associated(rhsid) ) allocate(rhsid(max(1,nzrhs))); rhsid = 0.0_rp
       if( .not. associated(resid) ) allocate(resid(max(1,nzrhs))); resid = 0.0_rp
       if( .not. associated(unkno) ) allocate(unkno(max(1,nzrhs))); unkno = 0.0_rp
       if( .not. associated(amatr) ) allocate(amatr(max(1,nzmat))); amatr = 0.0_rp
    endif

    ! ------------------------------------------------- |
    ! Work
    ! ------------------------------------------------- |
    !call alya2petsc_initialise()
    call alya2petsc_createLinearSolver(solver % petsc, solver)

    ! Non-linear iterations
    do it = 1, 3

        if( INOTMASTER )then
    
            ! Initialise
            rhsid = 0.0_rp; unkno = 0.0_rp; amatr = 0.0_rp

            ! Assembly and Boundary conditions
            call update_dirichlet_boundary_conditions()
            call assemble_isotropic_linear_elasticity()
            call impose_dirichlet_boundary_conditions(amatr, rhsid) 

            ! Solve
            call alya2petsc_solution(solver % petsc, amatr, rhsid, unkno)
            call alya2petsc_residualnorm(solver % petsc, resid, rnorm)

            ! Communicate nodal values at the interface
            call PAR_INTERFACE_NODE_EXCHANGE(solver % ndofn, rhsid, 'SUM', 'IN MY CODE')
            call PAR_INTERFACE_NODE_EXCHANGE(solver % ndofn, unkno, 'SUM', 'IN MY CODE')

            ! Update displacement
            do aa = 1, npoin
                do ii = 1, ndime
                    jj = (aa - 1_ip) * ndime + ii
                    displ(ii,aa,1) = displ(ii,aa,1) + unkno(jj)
                enddo
            enddo

            ! Compute norm of UNKO
            call alya2petsc_unknonorm(solver % petsc, unorm)

        endif

        call PAR_MAX(rnorm)
        if( IMASTER .or. (.not. IPARALL) ) write(*, "('PETSC: Solver norm2 = ',E17.5,A)") rnorm

        call PAR_MAX(unorm)
        if( IMASTER .or. (.not. IPARALL) ) write(*, "('PETSC: Solution norm2 = ',E17.5,A)") unorm

    enddo

    call alya2petsc_destroyLinearSolver(solver % petsc)
    call alya2petsc_finalise()

    ! ------------------------------------------------- |
    ! Write results in a file
    ! ------------------------------------------------- |
    do aa = 1, npoin
        write(100+kfl_paral,*) coord(1,aa), coord(2,aa), displ(1,aa,1), displ(2,aa,1)
    enddo

    ! ------------------------------------------------- |
    ! Check if unnitt test pass
    ! ------------------------------------------------- |
    ! Unitt test stop criteria
    if( rnorm > 1.0e-10_rp .or. unorm > 1.0e-10_rp )then
        stop 1
    endif

    call par_finali(1_ip)


    contains 

    subroutine assemble_isotropic_linear_elasticity()
        use def_kintyp_basic
        use def_kintyp_mesh_basic
        use def_domain
        use def_master
        use def_parame
        use mod_memory
        use def_kermod
        use def_elmtyp
        use mod_memory
        use mod_elmgeo
        use mod_parall

        implicit none

        integer(ip)                     :: ii, jj, kk, ll, idof, jdof
        integer(ip)                     :: ielem, igaus, inode, jnode
        integer(ip)                     :: pelty, pmate, pnode, pgaus, pdofn
        real(rp),         pointer       :: el_X(:,:)
        real(rp),         pointer       :: el_U(:,:)
        real(rp),         pointer       :: gp_W                   
        real(rp),         pointer       :: gp_N(:)               
        real(rp),         pointer       :: gp_dNde(:,:)         
        real(rp),         pointer       :: gp_J(:,:)
        real(rp)                        :: gp_J_det
        real(rp),         pointer       :: gp_J_inv(:,:)
        real(rp),         pointer       :: gp_dNdX(:,:)   
        real(rp),         pointer       :: gp_dUdX(:,:)
        real(rp),         pointer       :: gp_S(:,:)
        real(rp),         pointer       :: gp_E(:,:)
        real(rp)                        :: tr_E
        real(rp)                        :: gp_factor
        real(rp),         pointer       :: el_MAT(:,:)
        real(rp),         pointer       :: el_RHS(:)
        real(rp),         pointer       :: Inn(:,:)
        real(rp),         pointer       :: Cijkl(:,:,:,:)
        real(rp),         parameter     :: bf(3) = [0.0_rp,0.0_rp,0.0_rp]
        real(rp),         parameter     :: lam1 = 40.38e3_rp
        real(rp),         parameter     :: lam2 = 26.92e3_rp 

        ! Assemble MAT and RHS
        if( INOTMASTER )then

            ! Identity matrix 
            allocate(Inn(ndime,ndime)); Inn = 0.0_rp
            do ii = 1, ndime
                Inn(ii,ii) = 1.0_rp
            enddo

            ! Constant stiffness matrix
            allocate(Cijkl(ndime,ndime,ndime,ndime)); Cijkl = 0.0_rp
            do ii = 1, ndime; do jj = 1, ndime; do kk = 1, ndime; do ll = 1, ndime
                Cijkl(ii,jj,kk,ll) = lam1 * Inn(ii,jj) * Inn(kk,ll) + lam2 * &
                    ( Inn(ii,kk) * Inn(jj,ll) + Inn(ii,ll) * Inn(jj,kk) ) 
            end do; end do; end do; end do

            ! Elemental loop
            do ielem = 1, meshe(ndivi) % nelem

                pelty = ltype(ielem)
                pmate = lmate(ielem)
                pnode = nnode(pelty)
                pgaus = ngaus(pelty)
                pdofn = ndime * pnode

                ! Allocate some local variables
                allocate(el_X(ndime,pnode))
                allocate(el_U(ndime,pnode))
                allocate(gp_J(ndime,ndime))
                allocate(gp_J_inv(ndime,ndime))
                allocate(gp_dNdX(ndime,pnode))
                allocate(gp_dUdX(ndime,ndime))
                allocate(gp_S(ndime,ndime))
                allocate(gp_E(ndime,ndime))
                allocate(el_RHS(pdofn));         el_RHS = 0.0_rp
                allocate(el_MAT(pdofn,pdofn));   el_MAT = 0.0_rp

                ! Gather coordinates
                do concurrent ( inode = 1:pnode )
                    el_X(1:ndime,inode) = coord(1:ndime,lnods(inode,ielem))
                end do

                ! Gather displacement
                do concurrent ( inode = 1:pnode )
                    el_U(1:ndime,inode) = displ(1:ndime,lnods(inode,ielem),1)
                end do

                ! Get elemental J and RHS
                do igaus = 1, pgaus

                    ! Get interpolation
                    gp_W => elmar(pelty) % weigp(igaus) 
                    gp_N => elmar(pelty) % shape(:,igaus)
                    gp_dNde => elmar(pelty) % deriv(:,:,igaus)

                    ! Cartesian derivatives (dN/dX)
                    gp_J = matmul(gp_dNde,transpose(el_X))
                    call invmtx(gp_J,gp_J_inv,gp_J_det,ndime)
                    gp_dNdX = matmul(gp_J_inv, gp_dNde)
                    gp_dUdX = matmul(gp_dNdX,transpose(el_U))

                    ! Numerical integration factor  
                    gp_factor = gp_W * gp_J_det

                    ! Elemental assembly
                    ! MAT
                    do inode = 1, pnode
                        do ii = 1, ndime
                            idof = ndime * (inode - 1_ip) + ii
                            do jnode = 1, pnode
                                do kk = 1, ndime
                                    jdof = ndime * (jnode - 1_ip) + kk
                                    do jj = 1, ndime
                                        do ll = 1, ndime
                                            el_MAT(idof,jdof) = el_MAT(idof,jdof) + Cijkl(ii,jj,kk,ll) * &
                                                gp_dNdx(ll,jnode) * gp_dNdx(jj,inode) * gp_factor
                                        enddo
                                    enddo
                                enddo
                            enddo
                        enddo
                    enddo

                    ! Constitutive model
                    gp_E = (gp_dUdX + transpose(gp_dUdX)) * 0.5_rp
                    tr_E = 0.0_rp
                    do ii = 1, ndime
                        tr_E = tr_E + gp_E(ii,ii)
                    enddo
                    gp_S = lam1 * tr_E * Inn + 2.0_rp * lam2 * gp_E

                    ! RHS
                    do inode = 1, pnode
                        do ii = 1, ndime
                            do jj = 1, ndime  
                                idof = ndime * (inode - 1_ip) + ii
                                el_RHS(idof) = el_RHS(idof) - (gp_S(ii,jj) * gp_dNdx(jj,inode) + gp_N(inode) * bf(ii)) * gp_factor
                            enddo
                        enddo
                    enddo

                enddo

                ! Global assembly
                call assemble_ELMAT_to_CSR_NDOF( &
                    pnode, ndime, lnods(:,ielem), el_MAT, amatr ) 

                call assemble_ELRHS_to_CSR_NDOF( &
                    pnode, ndime, lnods(:,ielem), el_RHS, rhsid )

                ! Release local memmory
                deallocate(el_X)
                deallocate(el_U)
                deallocate(gp_J)
                deallocate(gp_J_inv)
                deallocate(gp_S)
                deallocate(gp_E)
                deallocate(gp_dUdX)
                deallocate(gp_dNdx)
                deallocate(el_RHS)
                deallocate(el_MAT)

            enddo

            ! Release global memmory
            deallocate(Inn)
            deallocate(Cijkl)

        end if

    end subroutine assemble_isotropic_linear_elasticity


    subroutine update_dirichlet_boundary_conditions()
        use def_kintyp_basic
        use def_kintyp_mesh_basic
        use def_domain
        use def_master
        use def_parame
        use mod_memory
        use def_kermod
        use def_elmtyp
        use mod_memory
        use mod_elmgeo
        use mod_parall

        implicit none

        integer(ip)               :: ielem, ipoin, inode, idime
        integer(ip)               :: pelty, pnode
        real(rp),   pointer       :: el_X(:,:)
        real(rp),   parameter     :: deltaX = 1e-3_rp

        ! Assemble MAT and RHS
        if( INOTMASTER )then

            ! Elemental loop
            do ielem = 1, meshe(ndivi) % nelem

                pelty = ltype(ielem)
                pnode = nnode(pelty)

                ! Allocate some local variables
                allocate(el_X(ndime,pnode))

                ! Gather coordinates
                do concurrent ( inode = 1:pnode )
                    el_X(1:ndime,inode) = coord(1:ndime,lnods(inode,ielem))
                end do

                ! Impose dirichlet BC
                do inode = 1, pnode
                    ipoin = lnods(inode,ielem)
                    do idime = 1, ndime
                        ! Bot side fixed
                        if( el_X(1,inode) < 0.0_rp + deltaX )then
                            kfl_fixno(idime,ipoin) = 1_ip
                            displ(idime,ipoin,1) = 0.0_rp
                        elseif( el_X(1,inode) > 10.0_rp - deltaX  )then
                            if( idime == 2_ip )then
                                kfl_fixno(idime,ipoin) = 1_ip
                                displ(idime,ipoin,1) = -0.5_rp
                            endif
                        endif

                    enddo
                enddo

                ! Release local memmory
                deallocate(el_X)

            enddo

        end if

    end subroutine update_dirichlet_boundary_conditions


    subroutine impose_dirichlet_boundary_conditions(amatr,rhsid)
        use def_domain, only : ndime, npoin, r_dom, c_dom, kfl_fixno

        implicit none

        real(rp),    intent(inout)    :: amatr(ndime,ndime,*) 
        real(rp),    intent(inout)    :: rhsid(ndime,*)
        integer(ip)                   :: ipoin,jpoin
        integer(ip)                   :: idime,jdime
        integer(ip)                   :: izdom

        ! For each node equation, put the contribution of imposed neighbor nodes at the RHS
        do ipoin = 1,npoin
            do idime = 1,ndime           
                if( kfl_fixno(idime,ipoin) == 1_ip ) then                
                    rhsid(idime,ipoin) = 0.0_rp
                end if
            end do
        end do

        ! For each IMPOSED node equation, put zero in all the coefficients except in their diagonals
        do ipoin = 1,npoin

            ! For each node equation, put zero at the imposed neighbor nodes
            do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1

                jpoin = c_dom(izdom)   

                ! Put 0 to Dirchilet DoFs (rows)
                do jdime = 1,ndime
                    if( kfl_fixno(jdime,jpoin) == 1_ip ) then            
                        do idime = 1,ndime
                            amatr(jdime,idime,izdom) = 0.0_rp
                        end do
                    end if
                end do

                ! Put 0 to Dirchilet DoFs (columns)
                do idime = 1,ndime
                    if( kfl_fixno(idime,ipoin) == 1_ip ) then
                        do jdime = 1,ndime              
                            amatr(jdime,idime,izdom)  = 0.0_rp
                        end do
                    end if
                end do

                ! Put 1 to the diagonal
                if (ipoin == jpoin) then
                    do jdime = 1,ndime
                        if( kfl_fixno(jdime,ipoin) == 1_ip ) then                
                            amatr(jdime,jdime,izdom)  = 1.0_rp
                        end if
                    end do
                end if

            end do

        end do

    end subroutine impose_dirichlet_boundary_conditions


    subroutine assemble_ELMAT_to_CSR_NDOF(pnode,pdofn,lnods,elmat,amatr)
        use def_domain, only : r_dom, c_dom

        integer(ip), intent(in)       :: pnode
        integer(ip), intent(in)       :: pdofn
        integer(ip), intent(in)       :: lnods(pnode)
        real(rp),    intent(in)       :: elmat(pnode*pdofn,pnode*pdofn)
        real(rp),    intent(inout)    :: amatr(pdofn,pdofn,*)
        integer(ip)                   :: inode,jnode
        integer(ip)                   :: ipoin,jpoin,izsol,jcolu
        integer(ip)                   :: idofn,jdofn,iposi,jposi


        do inode = 1, pnode
            ipoin = lnods(inode)
            do jnode = 1,pnode
                jpoin = lnods(jnode)
                izsol = r_dom(ipoin)
                jcolu = c_dom(izsol)
                do while( jcolu /= jpoin .and. izsol < r_dom(ipoin + 1_ip) - 1_ip )
                    izsol = izsol + 1_ip
                    jcolu = c_dom(izsol)
                end do
                if( jcolu == jpoin ) then
                    do idofn = 1, pdofn
                        iposi = (inode - 1_ip) * pdofn + idofn
                        do jdofn = 1,pdofn
                            jposi = (jnode - 1_ip) * pdofn + jdofn
                            amatr(jdofn,idofn,izsol) = amatr(jdofn,idofn,izsol) + elmat(iposi,jposi)
                        end do
                    end do
                end if
            end do
        end do
    
    end subroutine assemble_ELMAT_to_CSR_NDOF


    subroutine assemble_ELRHS_to_CSR_NDOF(pnode,pdofn,lnods,elrhs,rhsid)

        integer(ip), intent(in)       :: pnode
        integer(ip), intent(in)       :: pdofn
        integer(ip), intent(in)       :: lnods(pnode)
        real(rp),    intent(in)       :: elrhs(pnode*pdofn)
        real(rp),    intent(inout)    :: rhsid(pdofn,*)
        integer(ip)                   :: inode, ipoin, idofn, iposi

        do inode = 1, pnode
            ipoin = lnods(inode)
            do idofn = 1, pdofn
                iposi = (inode - 1_ip) * pdofn + idofn
                rhsid(idofn,ipoin) = rhsid(idofn,ipoin) + elrhs(iposi)
            enddo
        end do
 
    end subroutine assemble_ELRHS_to_CSR_NDOF


end program unitt_wrapper_alya2petsc_ksp_seq
