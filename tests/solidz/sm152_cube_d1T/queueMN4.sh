#!/bin/bash
#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



#
#  Submit jobs in MN-IV
#     sbatch < job.sh
#
#
#SBATCH --job-name=sm1521T
#SBATCH --workdir=.
#SBATCH --error=%j.err
#SBATCH --output=%j.out
#SBATCH --qos=debug
#SBATCH --ntasks=8
#SBATCH --cpus-per-task=4
#SBATCH --tasks-per-node=8
#SBATCH --time=00:01:00

module purge
module load intel/2018.1
module load impi/2018.1

ALYAPATH="/home/bsc21/bsc21946/solidz-dev"
#ALYAPATH="/home/bsc21/bsc21946/alya"
PROBLEMNAME=sm152_cube_d1T
#
# Launches ALYA
#
srun $ALYAPATH/Executables/unix/Alya.x $PROBLEMNAME
#mpirun -np 6 $ALYAPATH/Executables/unix/Alya.x $PROBLEMNAME
