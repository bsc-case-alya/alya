#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(alya_all)
  foreach(LIBR ${ARGN})
    set(L alya-${LIBR})
    set(M alya-${LIBR}_MODULE_DIR)
    list(APPEND ALYA_LIB_ALL ${L})
    list(APPEND ALYA_LIB_ALL_MODULE_DIR ${${M}})
  endforeach()
  set(alya-libs ${ALYA_LIB_ALL} CACHE INTERNAL "" FORCE)
  set(alya-libs_MODULE_DIR ${ALYA_LIB_ALL_MODULE_DIR} CACHE INTERNAL "" FORCE)
endfunction()

function(alya_link)

  set(ALYA_LIB "")
  set(ALYA_LIB_MODULE_DIR "")
  foreach(LIBR ${ARGN})
    set(L alya-${LIBR})
    set(M alya-${LIBR}_INCLUDE_DIRS)
    list(APPEND ALYA_LIB ${L})
    list(APPEND ALYA_LIB_MODULE_DIR ${${M}})
  endforeach()
  include_directories(${ALYA_LIB_MODULE_DIR})
  target_link_libraries(${PROJECT_NAME} ${ALYA_LIB})
endfunction()

function(alya_target)

endfunction()
