!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine assric(ndofn,pnode,lnods,elrhs,elmat,elunk,rhsid)
  !------------------------------------------------------------------------
  !****f* mathru/assric
  ! NAME 
  !    Assemble residual for matrix-free richardson solver
  ! DESCRIPTION
  !    Assembly of the RHS
  ! USES
  ! USED BY
  !    *_elmope
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only        :  ip,rp 
  use def_domain, only        :  npoin
  implicit none
  integer(ip),  intent(in)    :: ndofn,pnode
  integer(ip),  intent(in)    :: lnods(pnode)
  real(rp),     intent(inout) :: elrhs(*)
  real(rp),     intent(in)    :: elmat(pnode*ndofn,pnode*ndofn)
  real(rp),     intent(in)    :: elunk(*)
  real(rp),     intent(inout) :: rhsid(*)
  integer(ip)                 :: inode,jnode,ipoin,idofl,idofg,idofn,ndof2

  if(ndofn==1) then
     !
     ! 1 DOF
     !
     do inode=1,pnode
        ipoin=lnods(inode)
        do jnode=1,pnode
           elrhs(inode) = elrhs(inode) - elmat(inode,jnode) * elunk(jnode)
        end do
        rhsid(ipoin) = rhsid(ipoin) + elrhs(inode)
     end do
  else
     call runend('ASSRIC: NOT CODED')
  end if

end subroutine assric
