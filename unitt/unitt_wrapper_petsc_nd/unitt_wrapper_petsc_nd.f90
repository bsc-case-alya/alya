!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_wrapper_petsc

# include <petsc/finclude/petscksp.h>

    use petscksp
    use def_kintyp_basic
    use iso_c_binding

    use def_kintyp_basic
    use def_kintyp_mesh_basic
    use def_maths_bin 
    use def_domain
    use def_master
    use def_parame
    use mod_memory
    use def_kermod
    use def_elmtyp
    use mod_memory
    use mod_elmgeo
    use mod_communications_tools, only : PAR_COMM_TO_INT

    implicit none

    !call test_alya2petsc_alya_elasticity()
    call test_alya2petsc_alya_elasticity_opt()


    contains 

    subroutine test_alya2petsc_alya_elasticity()
        use def_kintyp_basic
        use def_kintyp_mesh_basic
        use def_maths_bin 
        use def_domain
        use def_master
        use def_parame
        use mod_memory
        use def_kermod
        use def_elmtyp
        use mod_memory
        use mod_elmgeo
        use mod_parall,      only : PAR_COMM_MY_CODE_WM
        use mod_renumbering, only : renumbering_lexical_order_type
        use mod_maths_solver, only : maths_conjugate_gradient
        use mod_maths_solver, only : maths_direct
        
        implicit none

        ! --< General / Alya stuff > ----------
        integer(ip)           :: ierror
        integer(ip)           :: ii, jj, kk, ll, idof, jdof
        integer(ip)           :: ielem, igaus, inode, jnode, idime, iboun, ipoin
        integer(ip)           :: pelty, pmate, pnode, pgaus, pdofn
        integer(ip)           :: nzrhs_loc
        real(rp),  pointer    :: el_X(:,:)
        real(rp),  pointer    :: gp_W                   
        real(rp),  pointer    :: gp_N(:)               
        real(rp),  pointer    :: gp_dNde(:,:)         
        real(rp),  pointer    :: gp_dNdx(:,:)   
        real(rp),  pointer    :: gp_J(:,:)
        real(rp)              :: gp_J_det
        real(rp),  pointer    :: gp_J_inv(:,:)
        real(rp)              :: gp_factor
        real(rp),  pointer    :: el_MAT(:,:)
        real(rp),  pointer    :: el_RHS(:)
        real(rp)              :: adiag, val
        real(rp),  parameter  :: deltaX = 1.0e-8_rp
        integer(ip), pointer  :: lninv_lex(:) => null()
        character(len=120)    :: str
        real(rp),  parameter  :: ux = 0.2_rp 
        real(rp),  parameter  :: bf(3) = [0.0_rp,0.0_rp,0.0_rp]
        real(rp)              :: lam1, lam2
        real(rp),  pointer    :: Inn(:,:)
        real(rp),  pointer    :: Cijkl(:,:,:,:)

        ! --< PETSc stuff > ----------
        PetscErrorCode         :: ierr
        Mat                    :: A
        Vec                    :: b
        Vec                    :: x
        Vec                    :: r
        KSP                    :: solver
        PC                     :: preconditioner
        PetscInt               :: ai, aj, bj
        PetscScalar            :: xx, yy
        PetscScalar, pointer   :: xx_v(:)
        PetscReal              :: xnorm
        PetscScalar, parameter :: mrone = -1.0
        PetscInt,    parameter :: n = 5
        PetscReal,   parameter :: rtol = 1.0e-10

        ! ------------------------------------------------- |
        ! Create Alya structures assembing a Laplace eq.
        ! ------------------------------------------------- |

        ! Read geometry from Alya files
        call Initia()  ! Initialization of the run
        call Readom()  ! Domain reading
        call Partit()  ! Domain partitioning
        call Reaker()  ! Read Kermod
        call Domtra()  ! Domain transformation
        call Domain()  ! Domain construction

        ! Allocate memory for the RHS and AMATR
        ! - allocate for BC
        allocate(kfl_fixno(ndime,max(1,npoin))); kfl_fixno = 0_ip
        allocate(displ(ndime,max(1_ip,npoin),1)); displ = 0.0_rp
        ! - emulating memunk subroutine
        nzrhs = (size(meshe(ndivi) % r_dom(:)) - 1_ip) * ndime
        allocate(rhsid(max(1_ip,nzrhs))); rhsid = 0.0_rp
        allocate(unkno(max(1_ip,nzrhs))); unkno = 0.0_rp

        nzmat = (size(meshe(ndivi) % c_dom(:))) * ndime**2
        allocate(amatr(max(1_ip,nzmat))); amatr = 0.0_rp


        ! Assemble MAT and RHS
        if( INOTMASTER )then

            ! Identity matrix 
            allocate(Inn(ndime,ndime)); Inn = 0.0_rp
            do ii = 1, ndime
                Inn(ii,ii) = 0.1_rp
            enddo

            ! Constant stiffness matrix
            lam1 = 40.38e3_rp
            lam2 = 26.92e3_rp
            allocate(Cijkl(ndime,ndime,ndime,ndime)); Cijkl = 0.0_rp
            do ii = 1, ndime; do jj = 1, ndime; do kk = 1, ndime; do ll = 1, ndime
                Cijkl(ii,jj,kk,ll) = lam1 * Inn(ii,jj) * Inn(kk,ll) + lam2 * &
                    ( Inn(ii,kk) * Inn(jj,ll) + Inn(ii,ll) * Inn(jj,kk) ) 
            end do; end do; end do; end do


            ! Elemental loop
            do ielem = 1, meshe(ndivi) % nelem

                pelty = ltype(ielem)
                pmate = lmate(ielem)
                pnode = nnode(pelty)
                pgaus = ngaus(pelty)
                pdofn = ndime * pnode

                ! Allocate some local variables
                allocate(el_X(ndime,pnode))
                allocate(gp_J(ndime,ndime))
                allocate(gp_J_inv(ndime,ndime))
                allocate(gp_dNdx(ndime,pnode))
                allocate(el_RHS(pdofn));         el_RHS = 0.0_rp
                allocate(el_MAT(pdofn,pdofn));   el_MAT = 0.0_rp

                ! Gather coordinates
                do concurrent ( inode = 1:pnode )
                    el_X(1:ndime,inode) = coord(1:ndime,lnods(inode,ielem))
                end do

                ! Get elemental J and RHS
                do igaus = 1, pgaus

                    ! Get interpolation
                    gp_W => elmar(pelty) % weigp(igaus) 
                    gp_N => elmar(pelty) % shape(:,igaus)
                    gp_dNde => elmar(pelty) % deriv(:,:,igaus)

                    ! Cartesian derivatives (dN/dX)
                    gp_J = matmul(gp_dNde,transpose(el_X))
                    call invmtx(gp_J,gp_J_inv,gp_J_det,ndime)
                    gp_dNdx = matmul(gp_J_inv, gp_dNde)

                    ! Numerical integration factor  
                    gp_factor = gp_W * gp_J_det

                    ! Elemental assembly
                    ! MAT
                    do inode = 1, pnode
                        do ii = 1, ndime
                            idof = ndime * (inode - 1_ip) + ii
                            do jnode = 1, pnode
                                do kk = 1, ndime
                                    jdof = ndime * (jnode - 1_ip) + kk
                                    do jj = 1, ndime
                                        do ll = 1, ndime
                                            el_MAT(idof,jdof) = el_MAT(idof,jdof) + Cijkl(ii,jj,kk,ll) * &
                                                gp_dNdx(ll,jnode) * gp_dNdx(jj,inode) * gp_factor
                                        enddo
                                    enddo
                                enddo
                            enddo
                        enddo
                    enddo

                    ! RHS
                    do inode = 1, pnode
                        do ii = 1, ndime
                            idof = ndime * (inode - 1_ip) + ii
                            el_RHS(idof) = el_RHS(idof) - gp_N(inode) * bf(ii) * gp_factor
                        enddo
                    enddo

                enddo

                ! Impose dirichlet BC
                do inode = 1, pnode
                    ipoin = lnods(inode,ielem)
                    do ii = 1, ndime
                        ! Bot side fixed
                        if( el_X(1,inode) < 0.0 + deltaX )then
                            kfl_fixno(ii,ipoin) = 1_ip
                            displ(ii,ipoin,1) = 0.0_rp
                        elseif( el_X(1,inode) > 10.0_rp - deltaX  )then
                            if( ii == 2_ip )then
                                kfl_fixno(ii,ipoin) = 1_ip
                                displ(ii,ipoin,1) = -0.5_rp
                            endif
                        endif

                    enddo
                enddo

                ! Global assembly
                call assemble_ELMAT_to_CSR_NDOF( &
                    pnode, ndime, ielem, lnods(:,ielem), el_MAT, &
                    meshe(ndivi) % r_dom(:), &
                    meshe(ndivi) % c_dom(:), &
                    amatr ) 

                call assemble_ELRHS_to_CSR_NDOF( &
                    pnode, ndime, ielem, lnods(:,ielem), el_RHS, &
                    rhsid )

                ! Release local memmory
                deallocate(el_X)
                deallocate(gp_J)
                deallocate(gp_J_inv)
                deallocate(gp_dNdx)
                deallocate(el_RHS)
                deallocate(el_MAT)

            enddo

            ! Apply BC
            call impose_dirichlet_vector( amatr, rhsid, unkno, displ(:,:,1) ) 

            ! Release global memmory
            deallocate(Inn)
            deallocate(Cijkl)
            deallocate(kfl_fixno)

        end if


        ! ------------------------------------------------- |
        ! From Alya structures to PETSc one
        ! ------------------------------------------------- |

        allocate(meshe(ndivi) % coo_rows(max(1,nzmat)))
        allocate(meshe(ndivi) % coo_cols(max(1,nzmat)))

        call renumbering_lexical_order_type( &
            meshe(ndivi), &
            lninv_lex, &
            'WITHOUT HALOS', &
            ndime &
        )

        if( INOTEMPTY )then
            call csr_to_coo_glb_NDOF( &
                nzrhs / ndime, &
                ndime, &
                lninv_lex, &
                meshe(ndivi) % r_dom(:), &
                meshe(ndivi) % c_dom(:), &
                meshe(ndivi) % coo_rows(:), &
                meshe(ndivi) % coo_cols(:) &
            )
        endif

        ! ------------------------------------------------- |
        ! Solve using PETSc 
        ! ------------------------------------------------- |
        if( INOTMASTER )then

            ! Local dimension for petsc
            nzrhs_loc = npoin_own * ndime

            ! Assign Alya communicator to the PETSc' one
            PETSC_COMM_WORLD = PAR_COMM_TO_INT(PAR_COMM_MY_CODE_WM)
            ! PETSc initialisation using the flobal communicator
            call PetscInitialize(PETSC_NULL_CHARACTER, ierr)
            if( ierr > 0 )then
                print *, "Error: Could not initialize PETSc, aborting..."
                stop
            endif
        
            ! 
            ! Create and fill MATRIX
            !
            call MatCreate(PETSC_COMM_WORLD, A, ierr); CHKERRA(ierr)
            call MatSetType(A, MATMPIAIJ, ierr); CHKERRA(ierr)
            call MatSetSizes(A, nzrhs_loc, nzrhs_loc, PETSC_DETERMINE, PETSC_DETERMINE, ierr); CHKERRA(ierr)
            call MatSetup(A, ierr); CHKERRA(ierr)

            do ii = 1, nzmat
                ai = meshe(ndivi) % coo_rows(ii)
                aj = meshe(ndivi) % coo_cols(ii)
                xx = amatr(ii)
                call MatSetValue( A, ai, aj, xx, ADD_VALUES, ierr); CHKERRA(ierr)
            enddo

            call MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY, ierr); CHKERRA(ierr)
            call MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY, ierr); CHKERRA(ierr)
            !call MatView(A, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)

            !
            ! Create and fill RHS
            !
            call VecCreate(PETSC_COMM_WORLD, b, ierr); CHKERRA(ierr)
            call VecSetType(b, VECMPI, ierr); CHKERRA(ierr)
            call VecSetSizes(b, nzrhs_loc, PETSC_DETERMINE, ierr); CHKERRA(ierr)

            do jj = 1, nzrhs
                bj = lninv_lex(jj) - 1 
                yy = rhsid(jj)
                call VecSetValue(b, bj, yy, ADD_VALUES, ierr); CHKERRA(ierr)
            enddo
        
            call VecAssemblyBegin(b, ierr); CHKERRA(ierr)
            call VecAssemblyEnd(b, ierr); CHKERRA(ierr)
            !call VecView(b, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)

            !
            ! Initialise SOLUTION and RESIDUAL vectors
            !
            call VecDuplicate(b, x, ierr); CHKERRA(ierr)
            call VecDuplicate(b, r, ierr); CHKERRA(ierr)

            !
            ! Solve 
            !
            call KSPCreate(PETSC_COMM_WORLD, solver, ierr); CHKERRA(ierr)
            call KSPSetOperators(solver, A, A, ierr); CHKERRA(ierr)
            call KSPSetTolerances(solver, rtol, PETSC_DEFAULT_REAL, PETSC_DEFAULT_REAL, PETSC_DEFAULT_INTEGER, ierr); CHKERRA(ierr)
            call KSPGetPC(solver,preconditioner,ierr)
            call KSPSetType(solver,KSPCG,ierr)
            call PCSetType(preconditioner,PCJACOBI,ierr)
            call KSPSetFromOptions(solver, ierr); CHKERRA(ierr)
            call KSPSetUp(solver, ierr); CHKERRA(ierr)
            call KSPView(solver, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)
            call KSPSolve(solver, b, x, ierr); CHKERRA(ierr)
            !call VecView(x, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)

            ! ------------------------------------------------- |
            ! Check norm
            ! ------------------------------------------------- |
            call MatMult(A, x, r, ierr); CHKERRA(ierr)
            call VecAXPY(r, mrone, b, ierr); CHKERRA(ierr)
            call VecNorm(r, NORM_2, xnorm, ierr); CHKERRA(ierr)
            write(str, "('norm2 = ',E17.5,A)") xnorm, C_NEW_LINE
            call PetscPrintf(PETSC_COMM_WORLD, trim(str), ierr); CHKERRA(ierr)

            ! Unitt test stop criteria
            if( xnorm > 1.0e-8_rp )then
            !    stop 1
            endif

            ! ------------------------------------------------- |
            ! Write solution
            ! ------------------------------------------------- |
            call VecGetArrayF90(x,xx_v,ierr)
            do ipoin = 1, npoin_own
                ii = (ipoin - 1_ip) * ndime
                write(100+kfl_paral,*) coord(1,ipoin), coord(2,ipoin), xx_v(ii+1), xx_v(ii+2), unkno(ii+1), unkno(ii+2)
            enddo
            call VecRestoreArrayF90(x,xx_v,ierr)


            ! ------------------------------------------------- |
            ! Release memory
            ! ------------------------------------------------- |
            call MatDestroy(A, ierr); CHKERRA(ierr)
            call VecDestroy(b, ierr); CHKERRA(ierr)
            call KSPDestroy(solver, ierr); CHKERRA(ierr)
            call VecDestroy(x, ierr); CHKERRA(ierr)
            call PetscFinalize( ierr )

        endif

        call MPI_Finalize( ierror )

    end subroutine test_alya2petsc_alya_elasticity

    subroutine test_alya2petsc_alya_elasticity_opt()
        use def_kintyp_basic
        use def_kintyp_mesh_basic
        use def_maths_bin 
        use def_domain
        use def_master
        use def_parame
        use mod_memory
        use def_kermod
        use def_elmtyp
        use mod_memory
        use mod_elmgeo
        use mod_parall,      only : PAR_COMM_MY_CODE, PAR_COMM_MY_CODE_WM
        use mod_renumbering, only : renumbering_lexical_order_type
        use mod_maths_solver, only : maths_conjugate_gradient
        use mod_maths_solver, only : maths_direct
        
        implicit none

        ! --< General / Alya stuff > ----------
        integer(ip)           :: ierror
        integer(ip)           :: ii, jj, kk, ll, idof, jdof
        integer(ip)           :: ielem, igaus, inode, jnode, idime, iboun, ipoin
        integer(ip)           :: pelty, pmate, pnode, pgaus, pdofn
        integer(ip)           :: nzrhs_loc
        integer(ip), pointer  :: nndiag(:) => null(), nnoutd(:) => null()
        real(rp),  pointer    :: el_X(:,:)
        real(rp),  pointer    :: gp_W                   
        real(rp),  pointer    :: gp_N(:)               
        real(rp),  pointer    :: gp_dNde(:,:)         
        real(rp),  pointer    :: gp_dNdx(:,:)   
        real(rp),  pointer    :: gp_J(:,:)
        real(rp)              :: gp_J_det
        real(rp),  pointer    :: gp_J_inv(:,:)
        real(rp)              :: gp_factor
        real(rp),  pointer    :: el_MAT(:,:)
        real(rp),  pointer    :: el_RHS(:)
        real(rp)              :: adiag, val
        real(rp),  parameter  :: deltaX = 1.0e-8_rp
        integer(ip), pointer  :: lninv_lex(:) => null()
        character(len=120)    :: str
        real(rp),  parameter  :: ux = 0.2_rp 
        real(rp),  parameter  :: bf(3) = [0.0_rp,0.0_rp,0.0_rp]
        real(rp)              :: lam1, lam2
        real(rp),  pointer    :: Inn(:,:)
        real(rp),  pointer    :: Cijkl(:,:,:,:)

        ! --< PETSc stuff > ----------
        PetscErrorCode         :: ierr
        Mat                    :: A
        Vec                    :: b
        Vec                    :: x
        Vec                    :: r
        KSP                    :: solver
        PC                     :: preconditioner
        PetscInt               :: ai, aj, bj
        PetscScalar            :: xx, yy
        PetscScalar, pointer   :: xx_v(:)
        PetscReal              :: xnorm
        PetscScalar, parameter :: mrone = -1.0
        PetscInt,    parameter :: n = 5
        PetscReal,   parameter :: rtol = 1.0e-10

        ! ------------------------------------------------- |
        ! Create Alya structures assembing a Laplace eq.
        ! ------------------------------------------------- |

        ! Read geometry from Alya files
        call Initia()  ! Initialization of the run
        call Readom()  ! Domain reading
        call Partit()  ! Domain partitioning
        call Reaker()  ! Read Kermod
        call Domtra()  ! Domain transformation
        call Domain()  ! Domain construction

        ! Allocate memory for the RHS and AMATR
        ! - allocate for BC
        allocate(kfl_fixno(ndime,max(1,npoin))); kfl_fixno = 0_ip
        allocate(displ(ndime,max(1_ip,npoin),1)); displ = 0.0_rp
        ! - emulating memunk subroutine
        nzrhs = (size(meshe(ndivi) % r_dom(:)) - 1_ip) * ndime
        allocate(rhsid(max(1_ip,nzrhs))); rhsid = 0.0_rp
        allocate(unkno(max(1_ip,nzrhs))); unkno = 0.0_rp

        nzmat = (size(meshe(ndivi) % c_dom(:))) * ndime**2
        allocate(amatr(max(1_ip,nzmat))); amatr = 0.0_rp


        ! Assemble MAT and RHS
        if( INOTMASTER )then

            ! Identity matrix 
            allocate(Inn(ndime,ndime)); Inn = 0.0_rp
            do ii = 1, ndime
                Inn(ii,ii) = 0.1_rp
            enddo

            ! Constant stiffness matrix
            lam1 = 40.38e3_rp
            lam2 = 26.92e3_rp
            allocate(Cijkl(ndime,ndime,ndime,ndime)); Cijkl = 0.0_rp
            do ii = 1, ndime; do jj = 1, ndime; do kk = 1, ndime; do ll = 1, ndime
                Cijkl(ii,jj,kk,ll) = lam1 * Inn(ii,jj) * Inn(kk,ll) + lam2 * &
                    ( Inn(ii,kk) * Inn(jj,ll) + Inn(ii,ll) * Inn(jj,kk) ) 
            end do; end do; end do; end do


            ! Elemental loop
            do ielem = 1, meshe(ndivi) % nelem

                pelty = ltype(ielem)
                pmate = lmate(ielem)
                pnode = nnode(pelty)
                pgaus = ngaus(pelty)
                pdofn = ndime * pnode

                ! Allocate some local variables
                allocate(el_X(ndime,pnode))
                allocate(gp_J(ndime,ndime))
                allocate(gp_J_inv(ndime,ndime))
                allocate(gp_dNdx(ndime,pnode))
                allocate(el_RHS(pdofn));         el_RHS = 0.0_rp
                allocate(el_MAT(pdofn,pdofn));   el_MAT = 0.0_rp

                ! Gather coordinates
                do concurrent ( inode = 1:pnode )
                    el_X(1:ndime,inode) = coord(1:ndime,lnods(inode,ielem))
                end do

                ! Get elemental J and RHS
                do igaus = 1, pgaus

                    ! Get interpolation
                    gp_W => elmar(pelty) % weigp(igaus) 
                    gp_N => elmar(pelty) % shape(:,igaus)
                    gp_dNde => elmar(pelty) % deriv(:,:,igaus)

                    ! Cartesian derivatives (dN/dX)
                    gp_J = matmul(gp_dNde,transpose(el_X))
                    call invmtx(gp_J,gp_J_inv,gp_J_det,ndime)
                    gp_dNdx = matmul(gp_J_inv, gp_dNde)

                    ! Numerical integration factor  
                    gp_factor = gp_W * gp_J_det

                    ! Elemental assembly
                    ! MAT
                    do inode = 1, pnode
                        do ii = 1, ndime
                            idof = ndime * (inode - 1_ip) + ii
                            do jnode = 1, pnode
                                do kk = 1, ndime
                                    jdof = ndime * (jnode - 1_ip) + kk
                                    do jj = 1, ndime
                                        do ll = 1, ndime
                                            el_MAT(idof,jdof) = el_MAT(idof,jdof) + Cijkl(ii,jj,kk,ll) * &
                                                gp_dNdx(ll,jnode) * gp_dNdx(jj,inode) * gp_factor
                                        enddo
                                    enddo
                                enddo
                            enddo
                        enddo
                    enddo

                    ! RHS
                    do inode = 1, pnode
                        do ii = 1, ndime
                            idof = ndime * (inode - 1_ip) + ii
                            el_RHS(idof) = el_RHS(idof) - gp_N(inode) * bf(ii) * gp_factor
                        enddo
                    enddo

                enddo

                ! Impose dirichlet BC
                do inode = 1, pnode
                    ipoin = lnods(inode,ielem)
                    do ii = 1, ndime
                        ! Bot side fixed
                        if( el_X(1,inode) < 0.0 + deltaX )then
                            kfl_fixno(ii,ipoin) = 1_ip
                            displ(ii,ipoin,1) = 0.0_rp
                        elseif( el_X(1,inode) > 10.0_rp - deltaX  )then
                            if( ii == 2_ip )then
                                kfl_fixno(ii,ipoin) = 1_ip
                                displ(ii,ipoin,1) = -0.5_rp
                            endif
                        endif

                    enddo
                enddo

                ! Global assembly
                call assemble_ELMAT_to_CSR_NDOF( &
                    pnode, ndime, ielem, lnods(:,ielem), el_MAT, &
                    meshe(ndivi) % r_dom(:), &
                    meshe(ndivi) % c_dom(:), &
                    amatr ) 

                call assemble_ELRHS_to_CSR_NDOF( &
                    pnode, ndime, ielem, lnods(:,ielem), el_RHS, &
                    rhsid )

                ! Release local memmory
                deallocate(el_X)
                deallocate(gp_J)
                deallocate(gp_J_inv)
                deallocate(gp_dNdx)
                deallocate(el_RHS)
                deallocate(el_MAT)

            enddo

            ! Apply BC
            call impose_dirichlet_vector( amatr, rhsid, unkno, displ(:,:,1) ) 

            ! Release global memmory
            deallocate(Inn)
            deallocate(Cijkl)
            deallocate(kfl_fixno)

        end if


        ! ------------------------------------------------- |
        ! From Alya structures to PETSc one
        ! ------------------------------------------------- |

        allocate(meshe(ndivi) % coo_rows(max(1,nzmat)))
        allocate(meshe(ndivi) % coo_cols(max(1,nzmat)))

        call renumbering_lexical_order_type( &
            meshe(ndivi), &
            lninv_lex, &
            'WITHOUT HALOS', &
            ndime &
        )

        if( INOTEMPTY )then

            call csr_to_coo_glb_NDOF( &
                nzrhs / ndime, &
                ndime, &
                lninv_lex, &
                meshe(ndivi) % r_dom(:), &
                meshe(ndivi) % c_dom(:), &
                meshe(ndivi) % coo_rows(:), &
                meshe(ndivi) % coo_cols(:) &
            )

            call alya2petsc_preallocation_structures( &
                nzrhs / ndime, &
                ndime, &
                meshe(ndivi) % r_dom(:), &
                meshe(ndivi) % c_dom(:), &
                nndiag, &
                nnoutd &
            )

        endif

        ! ------------------------------------------------- |
        ! Solve using PETSc 
        ! ------------------------------------------------- |
        if( INOTMASTER )then

            ! Local dimension for petsc
            nzrhs_loc = npoin_own * ndime

            ! Assign Alya communicator to the PETSc' one
            PETSC_COMM_WORLD = PAR_COMM_TO_INT(PAR_COMM_MY_CODE_WM)
           ! PETSc initialisation using the flobal communicator
            call PetscInitialize(PETSC_NULL_CHARACTER, ierr)
            if( ierr > 0 )then
                print *, "Error: Could not initialize PETSc, aborting..."
                stop
            endif
        
            ! 
            ! Create and fill MATRIX
            !
            call MatCreate(PETSC_COMM_WORLD, A, ierr); CHKERRA(ierr)
            call MatSetType(A, MATMPIAIJ, ierr); CHKERRA(ierr)
            call MatSetSizes(A, nzrhs_loc, nzrhs_loc, PETSC_DETERMINE, PETSC_DETERMINE, ierr); CHKERRA(ierr)
            !call MatSetup(A, ierr); CHKERRA(ierr)
            call MatMPIAIJSetPreallocation(A, 0, Alya2PETScINTa(nndiag), 0, Alya2PETScINTa(nnoutd), ierr); CHKERRA(ierr)

            do ii = 1, nzmat
                ai = meshe(ndivi) % coo_rows(ii)
                aj = meshe(ndivi) % coo_cols(ii)
                xx = amatr(ii)
                write(500,*) ai, aj, xx
                call MatSetValue( A, ai, aj, xx, ADD_VALUES, ierr); CHKERRA(ierr)
            enddo

            call MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY, ierr); CHKERRA(ierr)
            call MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY, ierr); CHKERRA(ierr)
            !call MatView(A, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)

            !
            ! Create and fill RHS
            !
            call VecCreate(PETSC_COMM_WORLD, b, ierr); CHKERRA(ierr)
            call VecSetType(b, VECMPI, ierr); CHKERRA(ierr)
            call VecSetSizes(b, nzrhs_loc, PETSC_DETERMINE, ierr); CHKERRA(ierr)

            do jj = 1, nzrhs
                bj = lninv_lex(jj) - 1 
                yy = rhsid(jj)
                write(501,*) bj, yy
                call VecSetValue(b, bj, yy, ADD_VALUES, ierr); CHKERRA(ierr)
            enddo
        
            call VecAssemblyBegin(b, ierr); CHKERRA(ierr)
            call VecAssemblyEnd(b, ierr); CHKERRA(ierr)
            !call VecView(b, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)

            !
            ! Initialise SOLUTION and RESIDUAL vectors
            !
            call VecDuplicate(b, x, ierr); CHKERRA(ierr)
            call VecDuplicate(b, r, ierr); CHKERRA(ierr)

            !
            ! Solve 
            !
            call KSPCreate(PETSC_COMM_WORLD, solver, ierr); CHKERRA(ierr)
            call KSPSetOperators(solver, A, A, ierr); CHKERRA(ierr)
            call KSPSetTolerances(solver, rtol, PETSC_DEFAULT_REAL, PETSC_DEFAULT_REAL, PETSC_DEFAULT_INTEGER, ierr); CHKERRA(ierr)
            call KSPGetPC(solver,preconditioner,ierr)
            call KSPSetType(solver,KSPCG,ierr)
            call PCSetType(preconditioner,PCJACOBI,ierr)
            call KSPSetFromOptions(solver, ierr); CHKERRA(ierr)
            call KSPSetUp(solver, ierr); CHKERRA(ierr)
            call KSPView(solver, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)
            call KSPSolve(solver, b, x, ierr); CHKERRA(ierr)
            !call VecView(x, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)

            ! ------------------------------------------------- |
            ! Check norm
            ! ------------------------------------------------- |
            call MatMult(A, x, r, ierr); CHKERRA(ierr)
            call VecAXPY(r, mrone, b, ierr); CHKERRA(ierr)
            call VecNorm(r, NORM_2, xnorm, ierr); CHKERRA(ierr)
            write(str, "('norm2 = ',E17.5,A)") xnorm, C_NEW_LINE
            call PetscPrintf(PETSC_COMM_WORLD, trim(str), ierr); CHKERRA(ierr)

            ! Unitt test stop criteria
            if( xnorm > 1.0e-6_rp )then
                stop 1
            endif

            ! ------------------------------------------------- |
            ! Write solution
            ! ------------------------------------------------- |
            call VecGetArrayF90(x,xx_v,ierr)
            do ipoin = 1, npoin_own
                ii = (ipoin - 1_ip) * ndime
                write(100+kfl_paral,*) coord(1,ipoin), coord(2,ipoin), xx_v(ii+1), xx_v(ii+2)
            enddo
            call VecRestoreArrayF90(x,xx_v,ierr)

            ! ------------------------------------------------- |
            ! Release memory
            ! ------------------------------------------------- |
            call MatDestroy(A, ierr); CHKERRA(ierr)
            call VecDestroy(b, ierr); CHKERRA(ierr)
            call KSPDestroy(solver, ierr); CHKERRA(ierr)
            call VecDestroy(x, ierr); CHKERRA(ierr)
            call PetscFinalize( ierr )

        endif

        call MPI_Finalize( ierror )

    end subroutine test_alya2petsc_alya_elasticity_opt

    !
    ! Auxiliar routines
    !
    subroutine assemble_ELMAT_to_CSR_1DOF(pnode,pevat,ielem,lnods,elmat,ia,ja,an)

        integer(ip), intent(in)                      :: pnode
        integer(ip), intent(in)                      :: pevat
        integer(ip), intent(in)                      :: ielem
        integer(ip), intent(in)                      :: lnods(pnode)
        integer(ip), intent(in)                      :: ia(*)
        integer(ip), intent(in)                      :: ja(*)
        real(rp),    intent(in)                      :: elmat(pevat,pevat)
        real(rp),    intent(inout)                   :: an(*)
        integer(ip)                                  :: inode,jnode
        integer(ip)                                  :: ipoin,jpoin,izsol,jcolu

        do inode = 1,pnode
            ipoin = lnods(inode)
            do jnode = 1,pnode
                jpoin = lnods(jnode)
                izsol = ia(ipoin)
                jcolu = ja(izsol)
                do while( jcolu /= jpoin .and. izsol < ia(ipoin+1)-1)
                    izsol = izsol + 1
                    jcolu = ja(izsol)
                end do
                if( jcolu == jpoin ) then
                    an(izsol) = an(izsol) + elmat(inode,jnode)
                end if
            end do
        end do
 
    end subroutine assemble_ELMAT_to_CSR_1DOF


    subroutine assemble_ELMAT_to_CSR_NDOF(pnode,pdofn,ielem,lnods,elmat,ia,ja,an)

        integer(ip), intent(in)                      :: pnode
        integer(ip), intent(in)                      :: pdofn
        integer(ip), intent(in)                      :: ielem
        integer(ip), intent(in)                      :: lnods(pnode)
        integer(ip), intent(in)                      :: ia(*)
        integer(ip), intent(in)                      :: ja(*)
        real(rp),    intent(in)                      :: elmat(pnode*pdofn,pnode*pdofn)
        real(rp),    intent(inout)                   :: an(pdofn,pdofn,*)
        integer(ip)                                  :: inode,jnode
        integer(ip)                                  :: ipoin,jpoin,izsol,jcolu
        integer(ip)                                  :: idofn,jdofn,iposi,jposi


        do inode = 1, pnode
            ipoin = lnods(inode)
            do jnode = 1,pnode
                jpoin = lnods(jnode)
                izsol = ia(ipoin)
                jcolu = ja(izsol)
                do while( jcolu /= jpoin .and. izsol < ia(ipoin + 1_ip) - 1_ip )
                    izsol = izsol + 1_ip
                    jcolu = ja(izsol)
                end do
                if( jcolu == jpoin ) then
                    do idofn = 1, pdofn
                        iposi = (inode - 1_ip) * pdofn + idofn
                        do jdofn = 1,pdofn
                            jposi = (jnode - 1_ip) * pdofn + jdofn
                            an(jdofn,idofn,izsol) = an(jdofn,idofn,izsol) + elmat(iposi,jposi)
                        end do
                    end do
                end if
            end do
        end do
    
    end subroutine assemble_ELMAT_to_CSR_NDOF

    
    subroutine assemble_ELRHS_to_CSR_1DOF(pnode,pevat,ielem,lnods,elrhs,bn)

        integer(ip), intent(in)                      :: pnode
        integer(ip), intent(in)                      :: pevat
        integer(ip), intent(in)                      :: ielem
        integer(ip), intent(in)                      :: lnods(pnode)
        real(rp),    intent(in)                      :: elrhs(pevat)
        real(rp),    intent(inout)                   :: bn(*)
        integer(ip)                                  :: inode,ipoin

        do inode = 1,pnode
            ipoin = lnods(inode)
            bn(ipoin) = bn(ipoin) + elrhs(inode)
        end do
 
    end subroutine assemble_ELRHS_to_CSR_1DOF


    subroutine assemble_ELRHS_to_CSR_NDOF(pnode,pdofn,ielem,lnods,elrhs,bn)

        integer(ip), intent(in)                      :: pnode
        integer(ip), intent(in)                      :: pdofn
        integer(ip), intent(in)                      :: ielem
        integer(ip), intent(in)                      :: lnods(pnode)
        real(rp),    intent(in)                      :: elrhs(pnode*pdofn)
        real(rp),    intent(inout)                   :: bn(pdofn,*)
        integer(ip)                                  :: inode, ipoin, idofn, iposi

        do inode = 1, pnode
            ipoin = lnods(inode)
            do idofn = 1, pdofn
                iposi = (inode - 1_ip) * pdofn + idofn
                bn(idofn,ipoin) = bn(idofn,ipoin) + elrhs(iposi)
            enddo
        end do
 
    end subroutine assemble_ELRHS_to_CSR_NDOF


    subroutine csr_to_coo_1DOF(nn,ia,ja,row,col)
        
        implicit none

        integer(ip),  intent(in)     :: nn
        integer(ip),  intent(in)     :: ia(nn+1)
        integer(ip),  intent(in)     :: ja(*)
        integer(ip),  intent(inout)  :: row(:)
        integer(ip),  intent(inout)  :: col(:)
        integer(ip)                  :: ii, iz, jj

        do ii = 1,nn
            do iz = ia(ii),ia(ii+1)-1
                jj       = ja(iz)
                row(iz)  = ii - 1_ip
                col(iz)  = jj - 1_ip
            end do
        end do

    end subroutine csr_to_coo_1DOF


    subroutine csr_to_coo_glb_1DOF(nn,ninv,ia,ja,row,col)
        
        implicit none

        integer(ip),  intent(in)     :: nn
        integer(ip),  intent(in)     :: ninv(:)
        integer(ip),  intent(in)     :: ia(nn+1)
        integer(ip),  intent(in)     :: ja(*)
        integer(ip),  intent(inout)  :: row(:)
        integer(ip),  intent(inout)  :: col(:)
        integer(ip)                  :: ii, iz, jj

        do ii = 1,nn
            do iz = ia(ii),ia(ii+1)-1
                jj       = ja(iz)
                row(iz)  = ninv(ii) - 1_ip
                col(iz)  = ninv(jj) - 1_ip
            end do
        end do

    end subroutine csr_to_coo_glb_1DOF


    subroutine csr_to_coo_glb_NDOF(nn,ndofn,ninv,ia,ja,row,col)
        
        implicit none

        integer(ip),  intent(in)     :: nn
        integer(ip),  intent(in)     :: ndofn
        integer(ip),  intent(in)     :: ninv(:)
        integer(ip),  intent(in)     :: ia(nn+1)
        integer(ip),  intent(in)     :: ja(*)
        integer(ip),  intent(inout)  :: row(:)
        integer(ip),  intent(inout)  :: col(:)
        integer(ip)                  :: ii, iz, jj, kz
        integer(ip)                  :: idofn, ii_idofn, jdofn, jj_jdofn

        kz = 0
        do ii = 1, nn
            do iz = ia(ii), ia(ii+1)-1
                jj = ja(iz)
                do idofn = 1, ndofn
                    ii_idofn = (ii - 1_ip) * ndofn + idofn
                    do jdofn = 1, ndofn 
                        jj_jdofn = (jj - 1_ip) * ndofn + jdofn
                        kz       = kz + 1_ip
                        row(kz)  = ninv(ii_idofn) - 1_ip
                        col(kz)  = ninv(jj_jdofn) - 1_ip
                    end do
                end do
            end do
        end do

        if( kz /= ((ia(nn+1) - 1_ip) * ndofn * ndofn) ) then
            print*, kz, ((ia(nn+1) - 1_ip) * ndofn * ndofn)
            call runend('GRAPHS_CSR_TO_COO: PROBLEM WITH MATRIX SIZE')
        end if

    end subroutine csr_to_coo_glb_NDOF


    subroutine alya2petsc_preallocation_structures(nn,ndofn,ia,ja,nndiag,nnoutd)
        use def_kintyp_dims
        use def_master, only : kfl_paral
        implicit none

        integer(ip),  intent(in)             :: nn
        integer(ip),  intent(in)             :: ndofn
        integer(ip),  intent(in)             :: ia(nn+1)
        integer(ip),  intent(in)             :: ja(*)
        integer(ip),  intent(inout), pointer :: nndiag(:)
        integer(ip),  intent(inout), pointer :: nnoutd(:)
        integer(ip),  pointer                :: nnout2(:)
        integer(ip)                          :: ii, iz, jj, id, jd, zd, aa, bb
        integer(ip)                          :: insd, outs, out2

        if( .not. associated(nndiag) ) allocate(nndiag(npoin*ndofn)); nndiag = 0_ip
        if( .not. associated(nnoutd) ) allocate(nnoutd(npoin*ndofn)); nnoutd = 0_ip
        !if( .not. associated(nnout2) ) allocate(nnout2(npoin*ndofn)); nnout2 = 0_ip

        ! Set diagonal limit composedx
        zd = ndofn * npoin_own

        ! Loop BCSR
        do ii = 1, nn 
            do iz = ia(ii), ia(ii+1)-1
                jj = ja(iz)
                ! Recover COO rows
                do aa = 1, ndofn
                    id = (ii - 1_ip) * ndofn + aa
                    ! Initialise inside / outside counters
                    insd = 0_ip
                    outs = 0_ip 
                    out2 = 0_ip 
                    ! Recover COO columns
                    do bb = 1, ndofn 
                        jd = (jj - 1_ip) * ndofn + bb
                        ! Count according if inside (insd) or outside (outs) the diagonal
                        if( id > zd .or. jd > zd )then
                            outs = outs + 1_ip
                        else
                            insd = insd + 1_ip
                        endif
                        ! Count interior nodes
                        if( ii > npoi1 .and. ii <= npoin_own )then
                            out2 = out2 + 1_ip
                        endif
                    end do
                    nndiag(id) = nndiag(id) + insd
                    nnoutd(id) = nnoutd(id) + max(outs,out2)
                    !nnout2(id) = nnout2(id) + out2
                end do
            end do
        end do

        !do ii = 1, npoin*ndofn
        !    write(300+kfl_paral,*) ii, nndiag(ii), nnoutd(ii), nnout2(ii)
        !enddo

    end subroutine alya2petsc_preallocation_structures


    subroutine impose_dirichlet_vector( A, b, u, variable)

        use def_kintyp
        use def_domain
        use def_master

        implicit none

        real(rp),    intent(inout)    :: A(ndime,ndime,nzdom) 
        real(rp),    intent(inout)    :: b(ndime,npoin)
        real(rp),    intent(out)      :: u(*)
        real(rp),    intent(in)       :: variable(ndime,npoin)
        real(rp)                      :: A_2(ndime*npoin,ndime*npoin)         
        real(rp)                      :: b_2(ndime*npoin)         
        integer(ip)                   :: ipoin,jpoin
        integer(ip)                   :: idime,jdime
        integer(ip)                   :: izdom,idofn

        ! For each node equation, put the contribution of imposed neighbor nodes at the RHS
        do ipoin = 1,npoin
            do izdom = r_dom(ipoin), r_dom(ipoin+1) - 1
                jpoin = c_dom(izdom)   
                ! BE CAREFUL, INDEX A(jdime,idime,izdom) IS NOW FINE, DO NOT CHANGE TO A(idime,jdime,izdom)
                do jdime = 1,ndime           
                    if( kfl_fixno(jdime,jpoin) == 1_ip ) then                
                        do idime = 1,ndime
                            b(idime,ipoin) =  b(idime,ipoin) - A(jdime,idime,izdom) * variable(jdime,jpoin)
                        end do
                    end if
                end do
            end do
        end do

        ! For each node equation, put zero at the imposed neighbor nodes
        do ipoin = 1,npoin
            do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                jpoin = c_dom(izdom)   
                do jdime = 1,ndime
                    if( kfl_fixno(jdime,jpoin) == 1_ip ) then            
                        do idime = 1,ndime
                            A(jdime,idime,izdom) = 0.0_rp
                        end do
                    end if
                end do
            end do
        end do

        ! For each IMPOSED node equation, put zero in all the coefficients except in their diagonals
        do ipoin = 1,npoin
            ! Put 0 to the IMPOSED node DoFs
            do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                jpoin = c_dom(izdom)   
                do idime = 1,ndime
                    if( kfl_fixno(idime,ipoin) == 1_ip ) then
                        do jdime = 1,ndime              
                            A(jdime,idime,izdom)  = 0.0_rp
                        end do
                    end if
                end do
            end do

            ! Put 1 to the diagonal
            do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
                jpoin = c_dom(izdom)   
                if (ipoin == jpoin) then
                    do jdime = 1,ndime
                        if( kfl_fixno(jdime,ipoin) == 1_ip ) then                
                            A(jdime,jdime,izdom)  = 1.0_rp
                        end if
                    end do
                end if
            end do
            ! Put the value of IMPOSED nodes at the RHS 
            do idime = 1,ndime
                if( kfl_fixno(idime,ipoin) == 1_ip ) then
                    b(idime,ipoin)  = variable(idime,ipoin)
                    idofn           = (ipoin-1)*ndime + idime
                    u(idofn)        = variable(idime,ipoin)
                end if
            end do
        end do

    end subroutine impose_dirichlet_vector

    pure function Alya2PETScINT(xx) result(pp)
        integer(ip), intent(in) :: xx 
        PetscInt                :: pp
        pp = xx
    end function Alya2PETScINT

    pure function Alya2PETScINTa(xx) result(pp)
        integer(ip), intent(in) :: xx(:) 
        PetscInt                :: pp(size(xx))
        integer(ip)             :: ii
        do ii = 1, size(xx)
            pp(ii) = xx(ii)
        enddo
    end function Alya2PETScINTa

end program unitt_wrapper_petsc
