#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import sys
import os
import math
import numpy as np

print(sys.argv)

#
# Inputs
#
S_L = 2.866
H = 148964.8843817232
rhoB_per_rhoU = 0.12129503309788398 / 0.6186803785647305 


#
# File names
#
meshName = sys.argv[1]

coordfile = "{}.coord".format(meshName)
velofile = "VELOC.alya"
enthfile = "ENTHA.alya"
ycfile = "YC.alya"



fCoord = open(coordfile, "r")
fVelo = open(velofile, "w")
fEnth = open(enthfile, "w")
fYc = open(ycfile, "w")

print("---| Start writing initial condition")

for line in fCoord:
    data = line.split()

    pid = int(data[0])
    dims = len(data) - 1
    x = float(data[1])
    y = float(data[2])

    
    V = 0.0
    U = S_L


    if x < 0:
        Yc = 0
    else:
        Yc = 0.45964259086064196
        U /= rhoB_per_rhoU

    #
    # Write IC
    #
    fVelo.write("{} {} {}\n".format(pid, U, V))
    fEnth.write("{} {}\n".format(pid, H))
    fYc.write("{} {}\n".format(pid, Yc))


fCoord.close()
fVelo.close()
fEnth.close()
fYc.close()
print("---| End writing initial condition")
