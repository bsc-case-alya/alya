#include <cvodes/cvodes.h>
#include <sunmatrix/sunmatrix_dense.h>
#include <sunlinsol/sunlinsol_dense.h>
#include <sundials/sundials_types.h>
#include <nvector/nvector_serial.h>

extern "C" {
#include "dydt.h"
#include "jacob.h"
#include "mass_mole.h"
}

#include <ctime>
#include <iostream>
#include <stdio.h>
#include <fstream>

// Rosenbrock stuff

#include <boost/numeric/odeint.hpp>
#include <boost/phoenix/core.hpp>
#include <boost/phoenix/core.hpp>
#include <boost/phoenix/operator.hpp>

typedef boost::numeric::ublas::vector< double > vector_type;
typedef boost::numeric::ublas::matrix< double > matrix_type;

using namespace boost::numeric::odeint;
namespace phoenix = boost::phoenix;
using std::ofstream;

int eval_jacob_cvodes(realtype t, N_Vector y, N_Vector ydot, SUNMatrix Jac, void* f, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3);
int dydt_cvodes(realtype t, N_Vector y, N_Vector ydot, void* f);

int m_count = 0;

struct stiff_system {
    void operator()( const vector_type &x , vector_type &dxdt , double /* t */  ) {
         double pyjac_x   [NSP];
         double pyjac_dxdt[NSP];
         
         for(int ss=0; ss<NSP; ss++){
             pyjac_x[ss] = x[ss];
             pyjac_dxdt[ss] = 0;
         }
         double pressure = 101325.15;
         dydt(0., pressure, pyjac_x, pyjac_dxdt);
         
         for(int ss=0; ss<NSP; ss++){
             dxdt[ss] = pyjac_dxdt[ss];
         }
    }
};

struct stiff_system_jacobi {
    void operator()( const vector_type & x, matrix_type &J , const double & t, vector_type &dfdt  ) {
    
        double pyjac_x[NSP];
        double pyjac_J[(NSP)*(NSP)];
        for(int ss=0; ss<NSP; ss++){
            pyjac_x[ss] = x[ss];
        }
        eval_jacob(0, 101325., pyjac_x, pyjac_J);
        for(int ii=0; ii<NSP; ii++){
           for(int jj=0; jj<NSP; jj++){
    	   J(jj,ii) = pyjac_J[ii*(NSP)+jj];
           }
        }
    }
};

struct output_observer{
    void operator()( const vector_type &x , double dt  ){
        m_count++;
	}
};


extern "C"
{

// Test Function for unitt test 
void test_pyjac_(int *nsp){
      std::cout << NSP << std::endl;	
      *nsp = NSP;	
}

void cantera_integrate_pyjac_(double *temp, double *p,  double *mass_frac, double *t) {
      // Declare Stuff  
      N_Vector y;
      SUNContext sunctx;
      double f, summ;
      SUNMatrix A;
      SUNLinearSolver LS;
      int m_maxsteps = 10000;
      double tfinal;
      realtype reltol = RCONST(1.0e-08);
      realtype abstol = RCONST(1.0e-12);
      realtype tn;
      double Xkin[NSP-1] = {0};
      double Ykin[NSP-1] = {0};
      // Make them NULL
      y = NULL;
      A = NULL;
      LS = NULL;
      void *cvode_mem = NULL;

      int retval = SUNContext_Create(NULL, &sunctx);
      y = N_VNew_Serial(NSP,sunctx);

      tfinal = *t;

      // Set temperature 
      
      NV_Ith_S(y,0) = *temp;

      // Set Pressure

      f = *p;

      // Set Mass fractions NSP-1 

      for (int j=0 ; j < NSP-1 ; j++){
          NV_Ith_S(y,j+1) = mass_frac[j];
      }
   
      cvode_mem = CVodeCreate(CV_BDF,sunctx);
      int flag = CVodeInit(cvode_mem, dydt_cvodes, 0, y);
      flag = CVodeSStolerances(cvode_mem, reltol, abstol);
      A = SUNDenseMatrix(NSP, NSP, sunctx);
      LS = SUNLinSol_Dense(y, A, sunctx);
      flag = CVodeSetLinearSolver(cvode_mem,LS,A);
      flag = CVodeSetJacFn(cvode_mem,  eval_jacob_cvodes);
      flag = CVodeSetMaxNumSteps(cvode_mem, m_maxsteps);
      flag = CVodeSetUserData(cvode_mem, &f);
      tn = 0.0;
      flag = CVode(cvode_mem, tfinal, y, &tn, CV_NORMAL);

      // Compute mass fraction of last species 

      summ = 0.0;
      for (int j=0 ; j < NSP-1 ; j++){
          summ = summ + NV_Ith_S(y,j+1);
          mass_frac[j] = NV_Ith_S(y,j+1);
      }

      mass_frac[NSP-1] = 1 - summ;
      *p = f;
      *temp = NV_Ith_S(y,0);
   
      N_VDestroy(y);
      CVodeFree(&cvode_mem);
   }

void cantera_integrate_rosenbrock_(double *temp, double *p,  double *mass_frac, double *t) {

     vector_type phi(NSP, 0.0);
     double Ykin[NSP-1] = {0};
     double t0 = 0.0;
     double tf;
     double dt_obs   = 1e-06;

     tf = *t;
     // Set unknowns 

     phi[0] = *temp;
     for (int j=0 ; j < NSP-1 ; j++){
         phi[j+1] = mass_frac[j];
     }

     size_t num_of_steps = integrate_const( make_dense_output< rosenbrock4< double > >(1.e-6, 1.e-6), 
		                            std::make_pair(stiff_system(), stiff_system_jacobi()),
					    phi, t0, tf, dt_obs,output_observer());
     double summ = 0.0;
     for (int j=0 ; j < NSP-1 ; j++){
         summ = summ + phi[j+1];
         mass_frac[j] = phi[j+1];
     }

     mass_frac[NSP-1] = 1 - summ;
     *temp = phi[0];
}


}

int eval_jacob_cvodes(double t, N_Vector y, N_Vector ydot, SUNMatrix jac, void* f, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3)
{       
	double* local_y = NV_DATA_S(y);
	double temp[NSP][NSP] = {0};
	eval_jacob((double)t, *(double*)f, local_y, (double*)temp);

	for (int i=0; i < NSP ; i++) {
            for (int j=0; j < NSP ; j++) {
                SM_ELEMENT_D(jac,j,i) = temp[i][j];
	    }
	}
	return 0;
}

int dydt_cvodes(realtype t, N_Vector y, N_Vector ydot, void* f)
{
	double* local_y = NV_DATA_S(y);
	double* local_dy = NV_DATA_S(ydot);
	dydt((double)t, *(double*)f, local_y, local_dy);
	return 0;
}


