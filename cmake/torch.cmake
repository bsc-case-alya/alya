#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(init_torch)
  if (WITH_TORCH MATCHES "external")
    find_package(Torch REQUIRED GLOBAL)
  endif()
endfunction()

function(define_torch)
  if (WITH_TORCH MATCHES "external")
    add_definitions(-DTORCH=1)
  endif()
endfunction()

function(link_torch)
  if (WITH_TORCH MATCHES "external")
    add_dependencies(${PROJECT_NAME} torch_wrapper)
    target_link_libraries(${PROJECT_NAME} torch_wrapper)
  endif()
endfunction()

