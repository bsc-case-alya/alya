!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_exm_drugs
   !test phase change in Cardiac Cycle

   use def_kintyp_basic, only : ip,rp
   use def_master
   use def_domain 
   use def_kermod
   use mod_sld_cardiac_cycle
   use def_master,           only :  TIME_N, cutim
   implicit none



   mem_modul = 0_ip
   modul = 1_ip
   nmate = 1_ip
   cutim = 1.0_rp
   

   !no transition scenario 1
   ! -- no change in calcium
   ! -- volume decreases
   ! -- criterion is calcium
   call test_set_cavity_phase( 1_ip, 4_ip )
   call test_set_cavity_phase_counter( 1_ip, 0_ip )
   call test_set_phase41_transition_method( PHASE_TRANSITION_DCAL )
   call test_set_volcai( (/1.0_rp,2.0_rp,2.0_rp/) )
   call test_set_cavity_dvol( 1_ip, -1.0_rp )
   print *,'Testing 4->1 transition based on calcium difference, dcal=0, dvol<0, phase_counter==0, no transition case'
   call sld_cardiac_phase_transition(1_ip, 1_ip, 0.1_rp)
   call assert( cavities(1) % phase_counter == 0_ip, "Phase counter increased")
   call assert( cavities(1) % phase == 4_ip, "Phase changed")


   !no transition scenario 2
   ! -- calcium increases
   ! -- no change in volume 
   ! -- criterion is volume
   call test_set_cavity_phase( 1_ip, 4_ip )
   call test_set_cavity_phase_counter( 1_ip, 0_ip )
   call test_set_phase41_transition_method( PHASE_TRANSITION_DVOL )
   call test_set_volcai( (/1.0_rp,2.0_rp,3.0_rp/) )
   call test_set_cavity_dvol( 1_ip, 0.0_rp  )
   print *,'Testing 4->1 transition based on volume difference, dcal>0, dvol==0, phase_counter==0, no transition case'
   call sld_cardiac_phase_transition(1_ip, 1_ip, 0.1_rp)
   call assert( cavities(1) % phase_counter == 0_ip, "Phase counter increased")
   call assert( cavities(1) % phase == 4_ip, "Phase changed")


   ! -- criterion is volume
   ! -- calcium increases
   ! -- volume decreases
   ! -- phase counter<5, expect increase in counter , no phase change 
   call test_set_cavity_phase( 1_ip, 4_ip )
   call test_set_cavity_phase_counter( 1_ip, 0_ip )
   call test_set_phase41_transition_method( PHASE_TRANSITION_DVOL )
   call test_set_volcai( (/1.0_rp,2.0_rp,3.0_rp/) )
   call test_set_cavity_dvol( 1_ip, -1.0_rp  )
   print *,'Testing 4->1 transition based on volume difference, dcal>0, dvol<0, phase_counter==0, no transition case'
   call sld_cardiac_phase_transition(1_ip, 1_ip, 0.1_rp)
   call assert( cavities(1) % phase_counter == 1_ip, "Phase counter not increased")
   call assert( cavities(1) % phase == 4_ip, "Phase changed")
   call test_set_cavity_dvol( 1_ip, 1.0_rp  ) !test if counter gets reset
   ! -- volume does not decrease
   ! -- reset phase counter counter , no phase change 
   call sld_cardiac_phase_transition(1_ip, 1_ip, 0.1_rp)
   call assert( cavities(1) % phase_counter == 0_ip, "Phase counter not reset after positive dvol")



   ! -- criterion is calcium
   ! -- calcium increases
   ! -- volume stable
   ! Expect phase change
   call test_set_cavity_phase( 1_ip, 4_ip )
   call test_set_cavity_phase_counter( 1_ip, 0_ip )
   call test_set_phase41_transition_method( PHASE_TRANSITION_DCAL )
   call test_set_volcai( (/1.0_rp,2.0_rp,3.0_rp/) )
   call test_set_cavity_dvol( 1_ip,  0.0_rp  )
   print *,'Testing 4->1 transition based on calcium difference, dcal=1, dvol<0, phase_counter==0, transition case'
   call sld_cardiac_phase_transition(1_ip, 1_ip, 0.1_rp)
   call assert( cavities(1) % phase_counter == 0_ip, "Phase counter increased")
   call assert( cavities(1) % phase == 1_ip, "Phase has not changed")


   !--------------------------------------------------------------------------
   !
   !
   !

   !no transition scenario 3
   ! -- criterion is calcium
   ! -- calcium stable
   ! -- volume decrease, phase counter>5
   ! Expect no phase change
   call test_set_cavity_phase( 1_ip, 4_ip )
   call test_set_cavity_phase_counter( 1_ip, 10_ip )
   call test_set_phase41_transition_method( PHASE_TRANSITION_DCAL )
   call test_set_volcai( (/1.0_rp,2.0_rp,2.0_rp/) )
   call test_set_cavity_dvol( 1_ip, -1.0_rp  ) 
   print *,'Testing 4->1 transition based on calcium difference, dcal=0, dvol<0, phase_counter==10, no transition case'
   call sld_cardiac_phase_transition(1_ip, 1_ip, 0.1_rp)
   call assert( cavities(1) % phase_counter == 10_ip, "Phase counter changed")
   call assert( cavities(1) % phase == 4_ip, "Phase changed")


   !no transition scenario 4
   ! -- criterion is volume
   ! -- calcium increase
   ! -- volume stable, phase counter>5
   ! Expect no phase change, counter should be reset
   call test_set_cavity_phase( 1_ip, 4_ip )
   call test_set_cavity_phase_counter( 1_ip, 10_ip )
   call test_set_phase41_transition_method( PHASE_TRANSITION_DVOL )
   call test_set_volcai( (/1.0_rp,2.0_rp,3.0_rp/) )
   call test_set_cavity_dvol( 1_ip,  0.0_rp  ) 
   print *,'Testing 4->1 transition based on volume difference, dcal>0, dvol==0, phase_counter==10, no transition case'
   call sld_cardiac_phase_transition(1_ip, 1_ip, 0.1_rp)
   call assert( cavities(1) % phase_counter == 0_ip, "Phase counter not reset")
   call assert( cavities(1) % phase == 4_ip, "Phase changed")


   ! -- criterion is volume
   ! -- calcium increase
   ! -- volume decrease, phase counter>5
   ! Expect  phase change, counter should be reset
   call test_set_cavity_phase( 1_ip, 4_ip )
   call test_set_cavity_phase_counter( 1_ip, 10_ip )
   call test_set_phase41_transition_method( PHASE_TRANSITION_DVOL )
   call test_set_volcai( (/1.0_rp,2.0_rp,3.0_rp/) )
   call test_set_cavity_dvol( 1_ip, -1.0_rp  ) 
   print *,'Testing 4->1 transition based on volume difference, dcal=1, dvol<0, phase_counter==10, transition case'
   call sld_cardiac_phase_transition(1_ip, 1_ip, 0.1_rp)
   call assert( cavities(1) % phase_counter == 0_ip, "Phase counter was not reset")
   call assert( cavities(1) % phase == 1_ip, "Phase has not changed")


   contains

   subroutine assert(condition, message)
      implicit none
      logical(lg), intent(in)    :: condition
      character(len=*), intent(in) :: message

      if( .not. condition ) then
         print *, "ASSERTION FAILED: "//trim(message)
         stop 1
      end if

   end subroutine assert
   

end program unitt_exm_drugs
