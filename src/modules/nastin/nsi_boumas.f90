!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_boumas(&
     pevat,pnodb,ndofn,lboel,gbsha,&
     gbden,baloc,wmatr)

  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_boumas
  ! NAME 
  !    nsi_boumas
  ! DESCRIPTION
  !    This routine computes the surface integral that comes when integrating 
  !    by parts the mass equation for low mach
  !    int(rho vel·n qh)
  ! USES
  !   
  ! USED BY
  !    nsi_bouope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp             
  use def_domain, only     :  ndime
  implicit none
  integer(ip), intent(in)  :: pnodb,pevat,ndofn
  integer(ip), intent(in)  :: lboel(pnodb)
  real(rp),    intent(in)  :: gbden
  real(rp),    intent(in)  :: gbsha(pnodb)
  real(rp),    intent(in)  :: baloc(ndime,ndime)
  real(rp),    intent(out) :: wmatr(pevat,pevat)
  integer(ip)              :: inodb,jnodb,jdime,ievab,jevab
  real(rp)                 :: fact0

  do inodb=1, pnodb
     ievab = (lboel(inodb) -1)*ndofn + ndime +1
     do jnodb = 1, pnodb
        fact0 = gbsha(inodb)*gbsha(jnodb)*gbden 
        jevab = (lboel(jnodb) -1)*ndofn
        do jdime =1, ndime    
           jevab = jevab +1
           wmatr(ievab, jevab) = wmatr (ievab, jevab) + fact0*baloc(jdime, ndime)
        end do
     end do
  end do

end subroutine nsi_boumas
