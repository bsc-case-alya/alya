!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_order_boundary_nodes3d

  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_maths_bin
  use def_maths_tree
  use def_maths_octbin
  use def_master
  use mod_memory
  use def_kermod
  use def_domain
  use mod_bouder

  implicit none
  real(rp)    :: bnorm,baloc(3,3),bocod(3,64)
  real(rp)    :: dot,eucta,cog(3),bog(3),elcod(3,64)
  integer(ip) :: iboun,ielem,pblty,pnodb,igaub
  integer(ip) :: pnode,inodb,ipoin,inode,pelty

  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction

  call Turnon()                                               ! Read modules
  call Solmem()                                               ! Solver and output memory
  call Begrun()                                               ! Initial computations

  call Restar(ITASK_READ_RESTART)                             ! Read restart
  call Iniunk()                                               ! Initial solution

  !call Filter(ITASK_INITIA)                                   ! Initial filtering
  call Output(ITASK_INITIA)                                   ! Initial output
  
  do iboun = 1,nboun
     ielem = lelbo(iboun)
     pelty = ltype(ielem)
     !if( pelty == TET10 ) then
        pblty = ltypb(iboun)
        pnodb = element_type(pblty) % number_nodes
        pnode = element_type(pelty) % number_nodes
        cog = 0.0_rp
        bog = 0.0_rp
        do inodb = 1,pnodb
           ipoin                = lnodb(inodb,iboun)
           bocod(1:ndime,inodb) = coord(1:ndime,ipoin)
           bog(1:ndime)         = bog(1:ndime) + bocod(1:ndime,inodb)
        end do
        do inode = 1,pnode
           ipoin                = lnods(inode,ielem)
           elcod(1:ndime,inode) = coord(1:ndime,ipoin)
           cog(1:ndime)         = cog(1:ndime) + elcod(1:ndime,inode)
        end do
        bog(1:ndime) = bog(1:ndime)/real(pnodb,rp)
        cog(1:ndime) = cog(1:ndime)/real(pnode,rp)
        do igaub = 1,ngaus(pblty)
           call bouder(&
                pnodb,ndime,ndimb,elmar(pblty) % deriv(:,:,igaub),&  ! Cartesian derivative
                bocod,baloc,eucta)                                   ! and Jacobian
           bnorm                = sqrt(dot_product(baloc(1:ndime,ndime),baloc(1:ndime,ndime)))
           baloc(1:ndime,ndime) = baloc(1:ndime,ndime) / bnorm
           dot                  = dot_product(bog(1:ndime)-cog(1:ndime),baloc(1:ndime,ndime))
           if( dot < 0.0_rp ) then
              print*,'------------------------------'
              print*,'pelty=',element_type(pelty) % name
              print*,'pblty=',element_type(pblty) % name
              print*,'iface=',elmgeo_boundary_face(pelty,lnods(:,ielem),lnodb(:,iboun))
              print*,'ielem=',ielem
              print*,'iboun=',iboun
              print*,'lnodb=',lnodb(:,iboun)
              print*,'baloc=',baloc(1:ndime,ndime)
              print*,'cog=  ',cog(1:ndime)
              print*,'bog=  ',bog(1:ndime)
              print*,'vec=  ',bog(1:ndime)-cog(1:ndime)
              stop 1
           end if
        end do
     !end if
  end do
  call runend('O.K.!')

end program unitt_order_boundary_nodes3d
