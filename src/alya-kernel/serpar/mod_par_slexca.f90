!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup ???
!> @{
!> @file    par_slexca.f90
!> @author  houzeaux
!> @date    2022-08-31
!> @brief   ???
!> @details ???
!>          ???
!-----------------------------------------------------------------------

module mod_par_slexca

  use def_master
  use def_parame
  use def_parall
  use def_domain
  use mod_parall,               only : commd,PAR_COMM_MY_CODE
  use mod_parall,               only : PAR_INTEGER
  use mod_parall,               only : PAR_REAL
  use mod_parall,               only : par_memor
  use mod_parall,               only : sendbuff_rp
  use mod_parall,               only : recvbuff_rp
  use mod_parall,               only : sendbuff_ip
  use mod_parall,               only : recvbuff_ip
  use mod_communications_tools, only : PAR_MPI_RUNEND
  use mod_communications_tools, only : PAR_WAITALL
  use mod_memory
  use mod_memchk
  use def_mpi
#include "def_mpi.inc"

  implicit none

  MY_MPI_REQUEST, allocatable  :: my_ireq4(:)
  MY_MPI_STATUS,  allocatable  :: my_status4(:) 
  integer(4)                   :: count4 = 0

contains
  
  subroutine par_slexca
    
    !-----------------------------------------------------------------------
    !****f* Parall/par_slexca
    ! NAME
    !    par_slexch
    ! DESCRIPTION
    !    This subroutine exchange arrays between master and slaves
    !    commd%bound_perm(jj):      permutation array
    !    loc_sparr1:                my local values
    !    loc_rparr1:                values given by neighbor ii
    !    commd%bound_dim:           size of communication array
    !    nneig:                     number of neighbors that share same group
    !    commd%neights(ii):         number of subdomain ii
    !    commd%bound_size(ii):      where my local arrays sart to exchange with ii
    !    commd%bound_size(ii+1)
    !        -commd%bound_size(ii): number of groups to exchange with ii
    ! USED BY
    !    Parall
    !***
    !-----------------------------------------------------------------------

    integer(ip) :: ipoin,ii,jj,kk,bsize,ini
    integer(ip) :: ndofi,ndofj
    integer(4)  :: istat,bsize4,dom_i
    real(rp)    :: time1,time2
    integer(4)  :: ierr4

    call cputim(time1)

    if( ISLAVE ) then

       if(party==3) then
          !
          ! Node
          !
          if(pardi==1.and.parki==1) then 

             !-------------------------------------------------------------
             !
             ! INT(NPOIN)
             !
             !-------------------------------------------------------------

             if( memory_size(sendbuff_ip) <  commd%bound_dim ) then
                call memory_deallo(par_memor,'SENDBUFF_IP','par_slexca',sendbuff_ip)
                call memory_alloca(par_memor,'SENDBUFF_IP','par_slexca',sendbuff_ip,commd%bound_dim,'DO_NOT_INITIALIZE')
             end if
             if( memory_size(recvbuff_ip) <  commd%bound_dim ) then
                call memory_deallo(par_memor,'RECVBUFF_IP','par_slexca',recvbuff_ip)
                call memory_alloca(par_memor,'RECVBUFF_IP','par_slexca',recvbuff_ip,commd%bound_dim,'DO_NOT_INITIALIZE')
             end if

             do jj= 1, commd%bound_dim
                ipoin = commd%bound_perm(jj)
                sendbuff_ip(jj) = pari1(ipoin)
             enddo

             do ii= 1, commd%nneig
                dom_i = commd%neights(ii)

                ini   = commd%bound_size(ii)
                bsize = commd%bound_size(ii+1) - ini

#ifdef MPI_OFF
#else
                bsize4=int(bsize,4)
                call MPI_Sendrecv( sendbuff_ip(ini:), bsize4,&
                     PAR_INTEGER,  dom_i, 0_4,     &
                     recvbuff_ip(ini:), bsize4,              &
                     PAR_INTEGER, dom_i, 0_4,      &
                     PAR_COMM_MY_CODE, status, istat )
                if( istat /= MPI_SUCCESS ) call PAR_MPI_RUNEND(istat,'PAR_SLEXCA')
#endif
             enddo

             do jj= 1, commd%bound_dim
                ipoin = commd%bound_invp(jj)
                pari1(ipoin) = pari1(ipoin) + recvbuff_ip(jj)
             enddo

          else if(pardi==1.and.parki==2) then

             !-------------------------------------------------------------
             !
             ! REAL(NPOIN)
             !
             !-------------------------------------------------------------

             if( ipass_par == 0 ) then

                ipass_par = 1
                allocate(my_ireq4(commd%nneig*2))

                if( memory_size(sendbuff_rp) <  commd%bound_dim ) then
                   call memory_deallo(par_memor,'SENDBUFF_RP','par_slexca',sendbuff_rp)
                   call memory_alloca(par_memor,'SENDBUFF_RP','par_slexca',sendbuff_rp,commd%bound_dim,'DO_NOT_INITIALIZE')
                end if
                if( memory_size(recvbuff_rp) <  commd%bound_dim ) then
                   call memory_deallo(par_memor,'RECVBUFF_RP','par_slexca',recvbuff_rp)
                   call memory_alloca(par_memor,'RECVBUFF_RP','par_slexca',recvbuff_rp,commd%bound_dim,'DO_NOT_INITIALIZE')
                end if

                do jj= 1, commd%bound_dim
                   ipoin = commd%bound_perm(jj)
                   sendbuff_rp(jj) = parr1(ipoin)
                enddo

                kk = 0
                do ii = 1,commd % nneig
                   dom_i = commd % neights(ii)

                   ini   = commd % bound_size(ii)
                   bsize = commd % bound_size(ii+1) - ini

#ifdef MPI_OFF
#else

                   bsize4=int(bsize,4)
                   kk = kk + 1
                   call MPI_Isend( sendbuff_rp(ini:ini+bsize-1), bsize4, &
                        PAR_REAL,  dom_i, 0_4,   &
                        PAR_COMM_MY_CODE, my_ireq4(kk), istat )
                   if( istat /= MPI_SUCCESS ) call PAR_MPI_RUNEND(istat,'PAR_SLEXCA')
                   kk = kk + 1
                   call MPI_Irecv( recvbuff_rp(ini:ini+bsize-1), bsize4, &
                        PAR_REAL,  dom_i, 0_4,   &
                        PAR_COMM_MY_CODE, my_ireq4(kk), istat )
                   if( istat /= MPI_SUCCESS ) call PAR_MPI_RUNEND(istat,'PAR_SLEXCA')
#endif
                enddo

             else


                ipass_par = 0
                count4    = commd % nneig*2
#ifdef MPI_OFF
#else
                call PAR_WAITALL(count4,my_ireq4)
                deallocate(my_ireq4)
#endif

                do jj = 1,commd % bound_dim
                   ipoin = commd % bound_invp(jj)
                   parr1(ipoin) = parr1(ipoin) + recvbuff_rp(jj)
                enddo


             end if


          else if (pardi>=1.and.parki==5) then

             !-------------------------------------------------------------
             !
             ! REAL(PARD1,NPOIN) => REAL(PARD1*NPOIN) 
             !
             !-------------------------------------------------------------

             if( ipass_par == 0 ) then

                ipass_par = 1

                allocate(my_ireq4(commd%nneig*2))

                if( memory_size(sendbuff_rp) <  pard1*commd%bound_dim ) then
                   call memory_deallo(par_memor,'SENDBUFF_RP','par_slexca',sendbuff_rp)
                   call memory_alloca(par_memor,'SENDBUFF_RP','par_slexca',sendbuff_rp,pard1*commd%bound_dim,'DO_NOT_INITIALIZE')
                end if
                if( memory_size(recvbuff_rp) <  pard1*commd%bound_dim ) then
                   call memory_deallo(par_memor,'RECVBUFF_RP','par_slexca',recvbuff_rp)
                   call memory_alloca(par_memor,'RECVBUFF_RP','par_slexca',recvbuff_rp,pard1*commd%bound_dim,'DO_NOT_INITIALIZE')
                end if

                do jj= 1, commd%bound_dim
                   ipoin = commd%bound_perm(jj)
                   ndofi = pard1*(ipoin-1)
                   ndofj = pard1*(jj-1)
                   do ii= 1, pard1
                      ndofi = ndofi + 1
                      ndofj = ndofj + 1
                      sendbuff_rp(ndofj) = parr1(ndofi)
                   enddo
                enddo

                kk = 0
                do ii= 1, commd%nneig
                   dom_i = commd%neights(ii)

                   ini   = pard1*(commd%bound_size(ii)-1) + 1
                   bsize = pard1*(commd%bound_size(ii+1)-commd%bound_size(ii))

#ifdef MPI_OFF
#else
                   bsize4 = int(bsize,4)
                   kk = kk + 1
                   call MPI_Isend( sendbuff_rp(ini:ini+bsize-1), bsize4, &
                        PAR_REAL,  dom_i, 0_4,   &
                        PAR_COMM_MY_CODE, my_ireq4(kk), istat )
                   if( istat /= MPI_SUCCESS ) call PAR_MPI_RUNEND(istat,'PAR_SLEXCA')
                   kk = kk + 1
                   call MPI_Irecv( recvbuff_rp(ini:ini+bsize-1), bsize4, &
                        PAR_REAL,  dom_i, 0_4,   &
                        PAR_COMM_MY_CODE, my_ireq4(kk), istat )              
                   if( istat /= MPI_SUCCESS ) call PAR_MPI_RUNEND(istat,'PAR_SLEXCA')
#endif
                end do

             else

                ipass_par = 0
                count4    = commd % nneig*2

#ifdef MPI_OFF
#else
                call PAR_WAITALL(count4,my_ireq4)
                deallocate(my_ireq4)

#endif
                do jj = 1,commd%bound_dim
                   ipoin = commd%bound_invp(jj)
                   ndofi = pard1*(ipoin-1)
                   ndofj = pard1*(jj-1)
                   do ii = 1,pard1
                      ndofi = ndofi + 1
                      ndofj = ndofj + 1
                      parr1(ndofi) = parr1(ndofi) + recvbuff_rp(ndofj)
                   end do
                end do

             end if

          else if(pardi==2.and.parki==2) then

             !-------------------------------------------------------------
             !
             ! REAL(PARD1,NPOIN)
             !
             !-------------------------------------------------------------

             if( memory_size(sendbuff_rp) <  pard1*commd%bound_dim ) then
                call memory_deallo(par_memor,'SENDBUFF_RP','par_slexca',sendbuff_rp)
                call memory_alloca(par_memor,'SENDBUFF_RP','par_slexca',sendbuff_rp,pard1*commd%bound_dim,'DO_NOT_INITIALIZE')
             end if
             if( memory_size(recvbuff_rp) <  pard1*commd%bound_dim ) then
                call memory_deallo(par_memor,'RECVBUFF_RP','par_slexca',recvbuff_rp)
                call memory_alloca(par_memor,'RECVBUFF_RP','par_slexca',recvbuff_rp,pard1*commd%bound_dim,'DO_NOT_INITIALIZE')
             end if

             do jj= 1, commd%bound_dim
                ipoin = commd%bound_perm(jj)
                do ii= 1, pard1
                   sendbuff_rp(pard1*(jj-1)+ii) = parr2(ii,ipoin)
                enddo
             enddo

             do ii= 1, commd%nneig
                dom_i = commd%neights(ii)

                ini   = pard1*(commd%bound_size(ii)-1)   + 1
                bsize = pard1*(commd%bound_size(ii+1)-1) - ini + 1

#ifdef MPI_OFF
#else
                bsize4 = int(bsize,4)
                call MPI_Sendrecv( &
                     sendbuff_rp(ini:), bsize4,&
                     PAR_REAL,  dom_i, 0_4,     &
                     recvbuff_rp(ini:), bsize4,              &
                     PAR_REAL, dom_i, 0_4,      &
                     PAR_COMM_MY_CODE, status, istat )
                if( istat /= MPI_SUCCESS ) call PAR_MPI_RUNEND(istat,'PAR_SLEXCA')
#endif
             enddo

             do jj= 1, commd%bound_dim
                ipoin = commd%bound_invp(jj)
                do ii= 1, pard1
                   parr2(ii,ipoin) = parr2(ii,ipoin) + recvbuff_rp(pard1*(jj-1)+ii)
                enddo
             enddo

          else if(pardi==1.and.parki==6) then

             call runend('OBSOLETE')

          end if

       end if
    end if

    call cputim(time2)
    cpu_paral(25)=cpu_paral(25)+time2-time1

  end subroutine par_slexca

end module mod_par_slexca
!> @}
