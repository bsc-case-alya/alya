!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine skylpo(ndofn,npoin,neqns,lpont,lpdof)
!-----------------------------------------------------------------------
!
!     This routine constructs the array LPDOF corresponding to the
!     matrix with NDOFN x NDOFN block components from LPONT
!
!-----------------------------------------------------------------------
  use def_kintyp
  implicit none
  integer(ip) :: ndofn,npoin,neqns
  integer(ip) :: lpdof(neqns), lpont(npoin)
  integer(ip) :: ipoin,idofn,ieqns,ndof2,ndofh,acdof,lprev,lpon0,heigh
!
! Initial operations
!
  ndof2 = ndofn*ndofn
  ndofh = ndofn*(ndofn-1)/2
!
! First node
!
  acdof = 0
  do idofn = 1,ndofn
     lpdof(idofn) = acdof
     acdof = acdof + idofn 
  end do
!
! Rest of nodes
!
  do ipoin = 2,npoin
     lprev = lpont(ipoin-1)
     lpon0 = ndof2*lprev + (ipoin-1)*ndofh 
     heigh = (lpont(ipoin)-lprev)*ndofn
     acdof = 0
     do idofn = 1,ndofn
        ieqns = (ipoin-1)*ndofn + idofn
        lpdof(ieqns) = lpon0 + heigh*idofn + acdof
        acdof = acdof + idofn 
     end do
  end do
  
end subroutine skylpo
