!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_search_skdtree

  use def_kintyp_basic
  use def_parame
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_search_method
  use def_maths_tree
  use def_maths_skdtree  
  use def_master
  use mod_memory
  use def_kermod
  use def_domain
  use def_interpolation_method
  use mod_random
  implicit none
  type(mesh_type_basic)           :: mesh_out
  type(maths_skdtree)             :: skdtree
  type(maths_skd_tree)            :: skd_tree
  integer(ip)                     :: ipoin,idime,inodb,nn
  integer(ip)                     :: pelty,ii,iboun,imeth
  integer(ip)                     :: pblty,ifoun,pnodb,iboun_min
  integer(8)                      :: memor_loc(2)
  integer(8)                      :: memor_tmp(2)
  real(rp)                        :: xx_test(2),dista,dimin
  real(rp)                        :: proje(3),coloc(3)
  real(rp)                        :: vmin(3),r,theta
  real(rp)                        :: bocod(2,64)
  real(rp)                        :: shapf(64)
  real(rp)                        :: shapf_min(64)
  real(rp)                        :: derit(2,64)
  real(rp),             pointer   :: xx(:,:),vv(:,:)
  real(rp),             pointer   :: bobox(:,:,:)
  type(interpolation)             :: interp
  character(len=5),     pointer   :: names(:)
  real(rp),             pointer   :: res(:,:)
  real(rp)                        :: pp_near(2)
  type(i1p),            pointer   :: list_entities(:)    

  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction

  memor_loc = 0_8
  memor_tmp = 0_8
  nullify(bobox,xx,vv,names,res,list_entities)
  !
  ! Test points
  !
  call random_initialization()
  nn = 2
  allocate(xx(2,nn))
  allocate(vv(2,nn))
  
  !do ii = 1,nn
  !   r        =  0.5_rp * random_generate_number()
  !   theta    =  2.0_rp * pi * random_generate_number()
  !   xx(1,ii) =  3.5_rp + r*cos(theta)
  !   xx(2,ii) =  2.5_rp + r*sin(theta)
  !end do
  xx(1,1) = 3.8480165415067926_rp
  xx(2,1) = 2.6252807654732044_rp 

  xx(1,2) = 3.0869980326450550_rp
  xx(2,2) = 2.2572638117830661_rp
  !
  ! Bounding boxes
  !
  imeth = 1
  if( .not. associated(meshe(ndivi) % boundary) ) then
     allocate(meshe(ndivi) % boundary)
     meshe(ndivi) % boundary % mesh => meshe(ndivi)
     call meshe(ndivi) % boundary % init  ()
     call meshe(ndivi) % boundary % assoc (ndime,mnodb,nboun,npoin,lnodb,ltypb,coord,LELBO=lelbo,LBOEL=lboel)
     meshe(ndivi) % boundary % mesh => meshe(ndivi)
  end if
  call meshe(ndivi) % boundary_bb(bobox)
  
  !----------------------------------------------------------------------
  !
  ! Skdtree
  !  
  !----------------------------------------------------------------------
  
  print*,'--------------------------------'
  print*,'SKDTREE'
  print*,'--------------------------------'

  call skdtree  % init    ()
  call skdtree  % input   (LIMIT=1_ip)
  call skdtree  % fill    (BOBOX=bobox)
  
  call skdtree % candidate(xx,list_entities,METHOD=CANDIDATE_NEAREST)
  if( associated(list_entities(1) % l) ) then
     print*,'list1=',list_entities(1) % l
  else
     print*,'nothing found'
     stop 2
  end if
  if( associated(list_entities(2) % l) ) then
     print*,'list2=',list_entities(2) % l
  end if
  deallocate(list_entities)
  nullify(list_entities)
  
  call mesh_out % init   () 
  call skdtree     % mesh(&
       mesh_out % ndime,mesh_out % mnode,mesh_out % nelem,&
       mesh_out % npoin,mesh_out % lnods,mesh_out % ltype,&
       mesh_out % coord,MEMORY_COUNTER=memor_loc)
  call skdtree  % results(res,names)
  call mesh_out % output (          FILENAME='skdtree')
  call mesh_out % results(res,names,FILENAME='skdtree')
  call skdtree  % results(res,names,ONLY_DEALLOCATE=.true.)
  call mesh_out % deallo (MEMORY_COUNTER=memor_loc)

 
  call interp % init      ()
  call interp % input     (skdtree,INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION)
  if( imeth == 1 ) then
     call interp % preprocess(xx,meshe(ndivi) % boundary)
  else
     call interp % preprocess(xx,meshe(ndivi))     
  end if
  call interp % values    (meshe(ndivi) % coord,vv)
  print*,'stats interp=',interp  % stats(2)
  print*,'stats kdtree=',skdtree % stats(5)
  !
  ! Memory
  !
  call interp % deallo()
  print*,'memor interp=',interp % memor(1),interp % memor(2)
  if( interp % memor(1) /= 0 ) then
     print*,'bad interp memory'
     stop 1
  end if
  !
  ! Check nearest boundary
  !
  print*,'checking'
  do ii = 1,nn
     dimin = huge(1.0_rp)
     iboun_min = 0
     do iboun = 1,nboun
        pblty                  = ltypb(iboun)
        pnodb                  = element_type(pblty) % number_nodes
        bocod(1:ndime,1:pnodb) = coord(1:ndime,lnodb(1:pnodb,iboun))
        call elmgeo_projection_on_a_face(&
             ndime,pblty,bocod,xx(:,ii),proje)
        call elmgeo_natural_coordinates_on_boundaries(&
             ndime,pblty,pnodb,bocod, &
             shapf,derit,proje, & 
             coloc,ifoun,NEAREST_POINT=pp_near)
        if( ifoun > 0 ) then
           dista = sqrt(dot_product(pp_near(1:ndime)-xx(1:ndime,ii),pp_near(1:ndime)-xx(1:ndime,ii)))
           if( dista < dimin ) then
              dimin = dista
              iboun_min = iboun
              vmin(1:ndime) = pp_near(1:ndime)
              shapf_min = shapf
           end if
        end if
     end do
     if( abs(vv(1,ii)-vmin(1)) > 1.0e-10_rp .or. abs(vv(2,ii)-vmin(2)) > 1.0e-10_rp ) then
        print*,'bad search --------------------------------------------------'
        print*,'iboun=',iboun_min
        print*,'xx=   ',xx(:,ii)
        print*,'vv=   ',vv  (1:ndime,ii),' dista=',sqrt(dot_product(vv(1:ndime,ii)-xx(1:ndime,ii),vv(1:ndime,ii)-xx(1:ndime,ii)))
        print*,'vmin= ',vmin(1:ndime),' dista=',dimin
        print*,'shap= ',shapf(1:2)
     end if
  end do  
  print*,'end checking'

  call skdtree  % deallo()
  !----------------------------------------------------------------------
  !
  ! skd_tree
  !  
  !----------------------------------------------------------------------

  print*,'--------------------------------'
  print*,'SKD_TREE'
  print*,'--------------------------------'

  call skd_tree  % init    ()
  call skd_tree  % input   (LIMIT=1_ip)
  call skd_tree  % fill    (BOBOX=bobox)

  call skd_tree % candidate(xx,list_entities)
  if( associated(list_entities(1) % l) ) then
     print*,'list1=',list_entities(1) % l
  else
     print*,'nothing found'
     stop 2
  end if
  if( associated(list_entities(2) % l) ) then
     print*,'list2=',list_entities(2) % l
  end if
     
  vv = 0
  call interp % init      ()
  call interp % input     (skd_tree,INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION)
  if( imeth == 1 ) then
     call interp % preprocess(xx,meshe(ndivi) % boundary)
  else
     call interp % preprocess(xx,meshe(ndivi))     
  end if
  call interp % values    (meshe(ndivi) % coord,vv)
  print*,'stats interp=',interp  % stats(2)
  print*,'stats kdtree=',skd_tree % stats(5)
  !
  ! Memory
  !
  call interp % deallo()
  print*,'memor interp=',interp % memor(1),interp % memor(2)
  if( interp % memor(1) /= 0 ) then
     print*,'bad interp memory'
     stop 1
  end if
  !
  ! Check nearest boundary
  !
  print*,'checking'
  do ii = 1,nn
     dimin = huge(1.0_rp)
     iboun_min = 0
     do iboun = 1,nboun
        pblty                  = ltypb(iboun)
        pnodb                  = element_type(pblty) % number_nodes
        bocod(1:ndime,1:pnodb) = coord(1:ndime,lnodb(1:pnodb,iboun))
        call elmgeo_projection_on_a_face(&
             ndime,pblty,bocod,xx(:,ii),proje)
        call elmgeo_natural_coordinates_on_boundaries(&
             ndime,pblty,pnodb,bocod, &
             shapf,derit,proje, & 
             coloc,ifoun,NEAREST_POINT=pp_near)
        if( ifoun > 0 ) then
           dista = sqrt(dot_product(pp_near(1:ndime)-xx(1:ndime,ii),pp_near(1:ndime)-xx(1:ndime,ii)))
           if( dista < dimin ) then
              dimin = dista
              iboun_min = iboun
              vmin(1:ndime) = pp_near(1:ndime)
              shapf_min = shapf
           end if
        end if
     end do
     if( abs(vv(1,ii)-vmin(1)) > 1.0e-10_rp .or. abs(vv(2,ii)-vmin(2)) > 1.0e-10_rp ) then
        print*,'bad search --------------------------------------------------'
        print*,'iboun=',iboun_min
        print*,'xx=   ',xx(:,ii)
        print*,'vv=   ',vv  (1:ndime,ii),' dista=',sqrt(dot_product(vv(1:ndime,ii)-xx(1:ndime,ii),vv(1:ndime,ii)-xx(1:ndime,ii)))
        print*,'vmin= ',vmin(1:ndime),' dista=',dimin
        print*,'shap= ',shapf(1:2)
     end if
  end do
  print*,'end checking'

  call skd_tree  % deallo()

  
!!$  stop
!!$  call kdtree_old % init()
!!$  call kdtree_old % construct(meshe(ndivi))
!!$  call kdtree_old % find(xx(:,1),iboun,dista,proje)
!!$  print*,'kdtree_old=',iboun,dista
!!$  
!!$  !----------------------------------------------------------------------
!!$  !
!!$  ! Okdtree
!!$  !  
!!$  !----------------------------------------------------------------------
!!$
!!$  call okdtree  % init    ()
!!$  call okdtree  % input   ()
!!$  print*,'A'
!!$  call okdtree  % fill    (BOBOX=bobox)
!!$  print*,'B'
!!$  
!!$  call interp % init      ()
!!$  call interp % input     (okdtree,INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION)
!!$  if( imeth == 1 ) then
!!$     call interp % preprocess(xx,meshe(ndivi) % boundary)
!!$  else
!!$     call interp % preprocess(xx,meshe(ndivi))     
!!$  end if
!!$  call interp % values    (meshe(ndivi) % coord,vv)
!!$  !
!!$  ! Memory
!!$  !
!!$  call interp % deallo()
!!$  print*,'memor interp=',interp % memor(1),interp % memor(2)
!!$  if( interp % memor(1) /= 0 ) then
!!$     print*,'bad interp memory'
!!$     stop 1
!!$  end if
!!$  !
!!$  ! Check interpolation
!!$  !
!!$  do ii = 1,size(xx,2)
!!$ !   print*,'xx=',xx(:,1)
!!$ !    print*,'vv=',vv(:,1)
!!$  end do
!!$  call okdtree  % deallo()
!!$  print*,'memor okdtree=',okdtree % memor(1),okdtree % memor(2)
!!$  if( okdtree % memor(1) /= 0 ) then
!!$     print*,'bad okdtree memory'
!!$  end if
!!$  
!!$
!!$  !print*,'dista=',sqrt(dot_product(xx(1:2,1)-(/3.59375,1.640625/),xx(1:2,1)-(/3.59375,1.640625/)))
!!$  call meshe(ndivi) % boundary % disassoc()
  call runend('O.K.!')

end program unitt_search_skdtree
 
