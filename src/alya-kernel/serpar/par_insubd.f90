!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_insubd(ii,jpoin,ifoun)
  !------------------------------------------------------------------------
  !****f* parall/par_insubd
  ! NAME 
  !    par_insubd
  ! DESCRIPTION
  !    Check if point JPOIN is in boundary with my neighbor II
  ! USES
  ! USED BY 
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only     :  ip
  use mod_parall, only     :  commd
  implicit none
  integer(ip), intent(in)  :: ii,jpoin
  integer(ip), intent(out) :: ifoun
  integer(ip)              :: ll

  ifoun = 0
  ll    = commd % bound_size(ii) 
  do while( ll <= commd % bound_size(ii+1)-1 )
     if( commd % bound_perm(ll) == jpoin ) then
        ifoun = 1
        return
     end if
     ll = ll + 1
  end do
  
end subroutine par_insubd
