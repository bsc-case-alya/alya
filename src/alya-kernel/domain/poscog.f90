!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine poscog(poscg,ndime,smplx)

!-----------------------------------------------------------------------
!
!     This routine computes the natural coordinates of the center of
!     gravity
!
!-----------------------------------------------------------------------
  use       def_kintyp
  implicit  none
  integer(ip), intent(in)  :: smplx
  integer(ip), intent(in)  :: ndime
  real(rp),    intent(out) :: poscg(ndime)

  poscg=0.0_rp

  if(smplx==1) then
     if(ndime==1) then
        poscg(1)=0.5_rp
     else if(ndime==2) then
        poscg(1)=1.0_rp/3.0_rp
        poscg(2)=1.0_rp/3.0_rp
     else if(ndime==3) then
        poscg(1)=0.25_rp
        poscg(2)=0.25_rp
        poscg(3)=0.25_rp
     end if
  end if
  
end subroutine poscog
