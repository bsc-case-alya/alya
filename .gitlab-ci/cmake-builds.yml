stages:
  - build
  - asynchronous_job_trigger
  - advanced_build
  - cleanup

.cmake_build_configuration: &cmake_build_configuration
  dependencies: []
  only:
    refs:
      - merge_requests
    changes:
      - .gitlab-ci.yml
      - .gitlab-ci/**/*
      - "**/CMakeLists.txt"
      - cmake/**/*
      - config/**/*
      - src/**/*
      - deps/**/*
      - tests/**/*
      - unitt/**/*
  artifacts:
    when: on_failure
    paths:
      - build/Testing/Temporary/LastTest.log
    expire_in: 1 day
    public: false

.cmake_setup: &cmake_setup
    - mkdir build
    - cd build

.cmake_build: &cmake_build
    - make clean
    - make -j$PARALLEL_BUILD
    - ctest

.cmake_advanced_build: &cmake_advanced_build
    - cmake -DMPI_FORTRAN_BINDINGS=MPIFH ..
    - make clean
    - make -j$PARALLEL_BUILD
    - ctest
    - cmake -DMPI_FORTRAN_BINDINGS=USEMPIF08 ..
    - make -j$PARALLEL_BUILD
    - ctest

build:cmake-gnu-kernel:
  stage: build
  image: bscalya/alya-build:latest
  <<: *cmake_build_configuration
  script:
    - rm -fr src/private
    - rm -fr submodules
    - rm -fr Thirdparties
    - for module in src/modules/*; do if [ -d $module ]; then rm -fr $module; fi; done
    - *cmake_setup
    - export OMPI_MCA_btl_vader_single_copy_mechanism=none
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" ..
    - *cmake_build

build:cmake-gnu-no-private:
  stage: build
  image: bscalya/alya-build:latest
  <<: *cmake_build_configuration
  script:
    - rm -fr src/private
    - *cmake_setup
    - export OMPI_MCA_btl_vader_single_copy_mechanism=none
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" ..
    - *cmake_build

build:cmake-gnu:
  stage: build
  image: bscalya/alya-build:latest
  <<: *cmake_build_configuration
  script:
    - *cmake_setup
    - export OMPI_MCA_btl_vader_single_copy_mechanism=none
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" ..
    - *cmake_build

build:cmake-gnu-force-mpi:
  stage: build
  image: bscalya/alya-build:latest
  <<: *cmake_build_configuration
  script:
    - *cmake_setup
    - export OMPI_MCA_btl_vader_single_copy_mechanism=none
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" -DWITH_MPI_SEQUENTIAL_TESTS=ON ..
    - *cmake_build

build:cmake-gnu-no-mpi:
  stage: build
  image: bscalya/alya-build:latest
  <<: *cmake_build_configuration
  script:
    - *cmake_setup
    - export OMPI_MCA_btl_vader_single_copy_mechanism=none
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" -DWITH_MPI=OFF ..
    - *cmake_build

build:cmake-gnu-i8:
  stage: build
  image: bscalya/alya-build:latest
  <<: *cmake_build_configuration
  script:
    - *cmake_setup
    - export OMPI_MCA_btl_vader_single_copy_mechanism=none
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" -DINTEGER_SIZE=8 ..
    - *cmake_build

build:cmake-gnu-ndimepar:
  stage: build
  image: bscalya/alya-build:latest
  <<: *cmake_build_configuration
  script:
    - *cmake_setup
    - export OMPI_MCA_btl_vader_single_copy_mechanism=none
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" -DWITH_NDIMEPAR=ON ..
    - *cmake_build


.postbld_cleanup_configuration: &postbld_cleanup_configuration
  stage: cleanup
  image: bscalya/python:latest
  only:
    refs:
      - merge_requests
    changes:
      - .gitlab-ci.yml
      - .gitlab-ci/**/*
      - "**/CMakeLists.txt"
      - cmake/**/*
      - config/**/*
      - src/**/*
      - deps/**/*
      - tests/**/*
      - unitt/**/*
  when: on_failure
  script:
    - export PYTHONUNBUFFERED=1
    - python3 $CICD_PATH/draft_mr.py -a $GITLAB_API_TOKEN -i $CI_PROJECT_ID -d $CI_PIPELINE_ID -m $CI_MERGE_REQUEST_IID -u $GITLAB_API_URL 
    - python3 $CICD_PATH/kill_current_pipeline.py -a $GITLAB_API_TOKEN -i $CI_PROJECT_ID -d $CI_PIPELINE_ID -m $CI_MERGE_REQUEST_IID -u $GITLAB_API_URL 

build:cmake-gnu-gmsh:
  stage: advanced_build
  image: bscalya/alya-build:latest
  needs: ["advanced_build_trigger"]
  <<: *cmake_build_configuration
  script:
    - *cmake_setup
    - export OMPI_MCA_btl_vader_single_copy_mechanism=none
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" -DWITH_GMSH=auto ..
    - *cmake_advanced_build

postbld_cleanup:gmsh:
  needs: ["build:cmake-gnu-gmsh"]
  <<: *postbld_cleanup_configuration

build:cmake-gnu-tubes:
  stage: advanced_build
  image: bscalya/alya-build:latest
  needs: ["advanced_build_trigger"]
  <<: *cmake_build_configuration
  script:
    - *cmake_setup
    - export OMPI_MCA_btl_vader_single_copy_mechanism=none
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" -DWITH_TUBES=auto -DWITH_OPENMP=ON ..
    - *cmake_advanced_build

postbld_cleanup:tubes:
  needs: ["build:cmake-gnu-tubes"]
  <<: *postbld_cleanup_configuration

build:cmake-gnu-commdom:
  stage: advanced_build
  image: bscalya/alya-build:latest
  needs: ["advanced_build_trigger"]
  <<: *cmake_build_configuration
  script:
    - *cmake_setup
    - export OMPI_MCA_btl_vader_single_copy_mechanism=none
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" -DWITH_COMMDOM=auto ..
    - *cmake_advanced_build

postbld_cleanup:commdom:
  needs: ["build:cmake-gnu-commdom"]
  <<: *postbld_cleanup_configuration

build:cmake-gnu-commdom-solidz:
  stage: advanced_build
  image: bscalya/alya-build:latest
  needs: ["advanced_build_trigger"]
  <<: *cmake_build_configuration
  script:
    - *cmake_setup
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" -DWITH_COMMDOM=auto -DWITH_ALL_MODULES=OFF -DWITH_MODULE_SOLIDZ=ON ..
    - *cmake_advanced_build

postbld_cleanup:commdom-solidz:
  needs: ["build:cmake-gnu-commdom-solidz"]
  <<: *postbld_cleanup_configuration

build:cmake-gnu-fti:
  stage: advanced_build
  image: bscalya/alya-build:latest
  needs: ["advanced_build_trigger"]
  <<: *cmake_build_configuration
  script:
    - *cmake_setup
    - export OMPI_MCA_btl_vader_single_copy_mechanism=none
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" -DWITH_FTI=auto ..
    - *cmake_advanced_build

postbld_cleanup:fti:
  needs: ["build:cmake-gnu-fti"]
  <<: *postbld_cleanup_configuration

build:cmake-gnu-petsc:
  stage: advanced_build
  image: bscalya/alya-build:latest
  needs: ["advanced_build_trigger"]
  <<: *cmake_build_configuration
  script:
    - git clone -b release https://gitlab.com/petsc/petsc.git petsc
    - cd petsc
    - PETSC_DIR=/petsc-install
    - export PYTHONUNBUFFERED=1
    - python3 configure --prefix=$PETSC_DIR --download-fblaslapack=1
    - make -j$PARALLEL_BUILD
    - make install
    - cd ..
    - *cmake_setup
    - export OMPI_MCA_btl_vader_single_copy_mechanism=none
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" -DWITH_PETSC=external -DPETSC_DIR=$PETSC_DIR ..
    - *cmake_advanced_build

postbld_cleanup:petsc:
  needs: ["build:cmake-gnu-petsc"]
  <<: *postbld_cleanup_configuration

build:cmake-gnu-torch:
  stage: advanced_build
  image: bscalya/alya-build:latest
  needs: ["advanced_build_trigger"]
  <<: *cmake_build_configuration
  script:
    - wget https://download.pytorch.org/libtorch/cpu/libtorch-cxx11-abi-shared-with-deps-1.12.1%2Bcpu.zip
    - unzip libtorch-cxx11-abi-shared-with-deps-1.12.1+cpu.zip
    - export Torch_DIR=`realpath ./libtorch/share/cmake/Torch/`
    - echo $Torch_DIR
    - ls $Torch_DIR
    - *cmake_setup
    - export OMPI_MCA_btl_vader_single_copy_mechanism=none
    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" -DWITH_TORCH=external ..
    - *cmake_advanced_build

postbld_cleanup:torch:
  needs: ["build:cmake-gnu-torch"]
  <<: *postbld_cleanup_configuration

#build:cmake-gnu-conversion-extra:
#  <<: *cmake_build_configuration
#  script:
#    - *cmake_setup
#    - cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root" -DCMAKE_BUILD_TYPE=Release -DCONF=none -DCMAKE_Fortran_COMPILER=mpif90 -DCUSTOM_Fortran_FLAGS="-Werror -Wconversion-extra -x f95-cpp-input -ffree-line-length-none -fimplicit-none" -DCUSTOM_Fortran_FLAGS_I8=-m64 -DCUSTOM_Fortran_FLAGS_O0=-O0 -DCUSTOM_Fortran_FLAGS_O1=-O1 -DCUSTOM_Fortran_FLAGS_O2=-O2 -DCUSTOM_Fortran_FLAGS_O3=-O3 -DCUSTOM_Fortran_FLAGS_STD2008="-std=f2008" -DWITH_REAL16=ON -DMPIEXEC_PREFLAGS="--allow-run-as-root" ..
#    - *cmake_build


build:cmake-oneapi:
  stage: build
  image: bscalya/alya-oneapi-build:latest
  <<: *cmake_build_configuration
  script:
    - *cmake_setup
    - source /opt/intel/oneapi/setvars.sh
    - export CC=icc
    - export CXX=icpc
    - export FC=ifort
    - export F90=ifort
    - export I_MPI_F90=ifort
    - cmake -DWITH_CTEST=OFF ..
    - *cmake_build

advanced_build_trigger:
  stage: asynchronous_job_trigger
  image: bscalya/python:latest
  script:
    - echo "Dummy job to trigger jobs asynchronously..."
  only:
    refs:
      - merge_requests

