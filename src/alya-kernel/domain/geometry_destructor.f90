!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Domain
!> @{
!> @file    geometry_destructor.f90
!> @author  houzeaux
!> @date    2019-06-11
!> @brief   Destroy domain
!> @details Reinitialize all domain arrays
!> @} 
!-----------------------------------------------------------------------

subroutine geometry_destructor()

  use def_kintyp
  use def_master
  use mod_domain
  implicit none

  call domain_memory_deallocate('GEOMETRY' )
  call domain_memory_deallocate('LGROU_DOM')
  call domain_memory_deallocate('LNINV_LOC')
  call domain_memory_deallocate('LEINV_LOC')
  call domain_memory_deallocate('LBINV_LOC')
  call domain_memory_deallocate('LESET'    )
  call domain_memory_deallocate('LBSET'    )
  call domain_memory_deallocate('LNSET'    )
  call domain_memory_deallocate('KFL_CODNO')
  call domain_memory_deallocate('KFL_CODBO')
  call domain_memory_deallocate('XFIEL % A')

end subroutine geometry_destructor
