!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_fringe_nodes

  !---------------------------------------------------------------!
  !                                                               !
  ! Finds fringe nodes on background mesh (x) for region          !
  ! overlapping with patch mesh (o):                              !
  !                                                               !
  !         x---x---x---x                                         !
  !         |   |   |   |                                         !
  !         x---x---x---x                                         !
  !         | o---o |   |                                         !
  !         x-|-x-|-x---x                                         !
  !         | o---o |   |                                         !
  !         x---x---x---x                                         !
  !                                                               !
  !---------------------------------------------------------------!

  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_maths_bin
  use def_maths_tree
  use def_maths_octbin
  use def_master
  use mod_memory
  use def_kermod
  use def_domain
  use def_interpolation_method
  use mod_immersed, only : cou_get_nodes_in_surf
  use mod_immersed, only : cou_get_wet_elements
  use mod_immersed, only : cou_get_fringe_nodes
  
  implicit none
  real(rp),             parameter :: epsil = 1.0e-12_rp
  type(mesh_type_basic)           :: mesh_backg
  type(bmsh_type_basic)           :: mesh_patch
  type(interpolation)             :: interp
  type(maths_skdtree)             :: skdtree
  real(rp),             pointer   :: bobox(:,:,:), dista(:)
  integer(ip),          pointer   :: lnfri(:),     lnins(:),     lewet(:)
  integer(ip),          pointer   :: lnfri_ref(:), lnins_ref(:), lewet_ref(:)
  integer(ip)                     :: npoin_backg, nelem_backg
  integer(ip)                     :: ipoin,       ielem,         inode
  integer(8)                      :: count_rate8
  !
  ! Initialize system clock
  !
  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)
  !
  ! Nullify and allocate
  !
  !IPARALL    = .false.
  npoin_backg = 16
  nelem_backg = 9
  nullify( bobox )
  nullify( dista )
  nullify( lnins )
  nullify( lnfri )
  nullify( lewet )
  nullify( lnins_ref )
  nullify( lnfri_ref )
  nullify( lewet_ref )
  allocate( dista    (npoin_backg) )
  allocate( lnins    (npoin_backg) )
  allocate( lnfri    (npoin_backg) )
  allocate( lewet    (nelem_backg) )
  allocate( lnins_ref(npoin_backg) )
  allocate( lnfri_ref(npoin_backg) )
  allocate( lewet_ref(nelem_backg) )
  !
  ! Initialize arrays 
  !
  call elmgeo_element_type_initialization()
  do ipoin = 1,npoin_backg
     lnfri(ipoin)     = 0_ip
     lnins(ipoin)     = 0_ip
     lnfri_ref(ipoin) = 0_ip
     lnins_ref(ipoin) = 0_ip
  end do
  do ielem = 1,nelem_backg
     lewet(ielem)     = 0_ip
     lewet_ref(ielem) = 0_ip
  end do
  !
  ! Define background volume mesh
  !
  call mesh_backg % init()
  mesh_backg % npoin = npoin_backg
  mesh_backg % nelem = nelem_backg
  mesh_backg % ndime = 2
  mesh_backg % mnode = 4
  call mesh_backg % alloca()
  mesh_backg % lnods(:,1)  = [  12,  7,  5, 10 ]
  mesh_backg % lnods(:,2)  = [  14,  9,  7, 12 ]
  mesh_backg % lnods(:,3)  = [  16, 15,  9, 14 ]
  mesh_backg % lnods(:,4)  = [   7,  4,  2,  5 ] 
  mesh_backg % lnods(:,5)  = [   9,  8,  4,  7 ]
  mesh_backg % lnods(:,6)  = [  15, 13,  8,  9 ]
  mesh_backg % lnods(:,7)  = [   4,  3,  1,  2 ] 
  mesh_backg % lnods(:,8)  = [   8,  6,  3,  4 ] 
  mesh_backg % lnods(:,9)  = [  13, 11,  6,  8 ]
  mesh_backg % ltype(1:9)  = QUA04
  mesh_backg % coord(:,1 ) = [ 0.0_rp, 3.0_rp ]
  mesh_backg % coord(:,2 ) = [ 0.0_rp, 2.0_rp ]
  mesh_backg % coord(:,3 ) = [ 1.0_rp, 3.0_rp ]
  mesh_backg % coord(:,4 ) = [ 1.0_rp, 2.0_rp ]
  mesh_backg % coord(:,5 ) = [ 0.0_rp, 1.0_rp ]
  mesh_backg % coord(:,6 ) = [ 2.0_rp, 3.0_rp ]
  mesh_backg % coord(:,7 ) = [ 1.0_rp, 1.0_rp ]
  mesh_backg % coord(:,8 ) = [ 2.0_rp, 2.0_rp ]
  mesh_backg % coord(:,9 ) = [ 2.0_rp, 1.0_rp ]
  mesh_backg % coord(:,10) = [ 0.0_rp, 0.0_rp ]
  mesh_backg % coord(:,11) = [ 3.0_rp, 3.0_rp ]
  mesh_backg % coord(:,12) = [ 1.0_rp, 0.0_rp ]
  mesh_backg % coord(:,13) = [ 3.0_rp, 2.0_rp ]
  mesh_backg % coord(:,14) = [ 2.0_rp, 0.0_rp ]
  mesh_backg % coord(:,15) = [ 3.0_rp, 1.0_rp ]
  mesh_backg % coord(:,16) = [ 3.0_rp, 0.0_rp ]
  !
  ! Define patch boundary mesh
  !
  call mesh_patch % init()
  mesh_patch % npoin = 4
  mesh_patch % nelem = 4
  mesh_patch % ndime = 2
  mesh_patch % mnode = 2
  call mesh_patch % alloca()
  mesh_patch % lnods(:,1) = [ 1, 2 ]
  mesh_patch % lnods(:,2) = [ 2, 3 ]
  mesh_patch % lnods(:,3) = [ 3, 4 ]
  mesh_patch % lnods(:,4) = [ 4, 1 ]
  mesh_patch % ltype      = BAR02
  mesh_patch % coord(:,1) = [ 0.5_rp, 0.5_rp ]
  mesh_patch % coord(:,2) = [ 1.5_rp, 0.5_rp ]
  mesh_patch % coord(:,3) = [ 1.5_rp, 1.5_rp ]
  mesh_patch % coord(:,4) = [ 0.5_rp, 1.5_rp ]
  call mesh_patch % element_bb(bobox)
  !
  ! Define search strategy
  !
  call skdtree % init      ()
  call skdtree % input     ()
  call skdtree % fill      (bobox=bobox)
  !
  ! Define interpolation strategy
  !
  call interp % init       ()
  call interp % input      (skdtree,&
       &                   INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION,&
       &                   NAME='FIRST TRY')
  call interp % preprocess (mesh_backg % coord, mesh_patch)
  call interp % distances  (mesh_backg % coord, dista)
  !
  ! Find nodes inside surface, wet elements & fringe nodes
  call cou_get_nodes_in_surf(mesh_backg, interp % dista, lnins)
  call cou_get_wet_elements (mesh_backg, lnins,          lewet)
  call cou_get_fringe_nodes (mesh_backg, lewet,   lnins, lnfri)
  !
  ! Define reference arrays
  !
  lewet_ref(1)  = 1_ip
  lewet_ref(2)  = 1_ip
  lewet_ref(4)  = 1_ip
  lewet_ref(5)  = 1_ip
  lnins_ref(7)  = 1_ip
  lnfri_ref(2)  = 1_ip 
  lnfri_ref(4)  = 1_ip 
  lnfri_ref(5)  = 1_ip 
  lnfri_ref(8)  = 1_ip 
  lnfri_ref(9)  = 1_ip 
  lnfri_ref(10) = 1_ip 
  lnfri_ref(12) = 1_ip 
  lnfri_ref(14) = 1_ip
  !
  ! Check results
  !
  do ielem = 1, mesh_backg % nelem 
     if( lewet_ref(ielem) /= lewet(ielem) ) then
        print*,'Wrong wet nodes'
        stop 1
     end if
  end do
  do ipoin = 1, mesh_backg % npoin
     if( lnins_ref(ipoin) /= lnins(ipoin) ) then
        print*,'Wrong nodes inside overlapping mesh'
        stop 1
     end if
     if( lnfri_ref(ipoin) /= lnfri(ipoin) ) then
        print*,'Wrong fringe nodes'
        stop 1
     end if
  end do

  ! ! Print results 
  ! print*,'wet elements'
  ! do ielem = 1, mesh_backg % nelem 
  !    if( lewet(ielem) == 1 ) print*,ielem
  ! end do
  ! print*,'nodes in overlapping region'
  ! do ipoin = 1, mesh_backg % npoin
  !    if( lnins(ipoin) == 1 ) print*,ipoin
  ! end do
  ! print*,'fringe nodes'
  ! do ipoin = 1, mesh_backg % npoin
  !    if( lnfri(ipoin) == 1 ) print*,ipoin
  ! end do

end program unitt_fringe_nodes
