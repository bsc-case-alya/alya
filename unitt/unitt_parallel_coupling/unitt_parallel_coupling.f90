!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------!
! Copyright 2005 - 2022 Barcelona Supercomputing Center.          !
! Distributed under the ALYA AVAILABLE SOURCE ("ALYA AS") LICENSE !
! for nonprofit scientific purposes only.                         !
! See companion file LICENSE.txt.                                 !
!-----------------------------------------------------------------!


program unitt_parallel_coupling

  use def_master
  use def_kintyp
  use def_domain
  use mod_elmgeo
  use def_maths_bin
  use def_kermod
  use def_search_method
  use mod_parall
  use def_interpolation_method
  use mod_memory_basic
  use mod_communications_global
  use def_projection_method
  use def_coupling_method
  use mod_communications

  implicit none


  real(rp),    parameter :: eps = 1.0e-10_rp
  real(rp),    pointer   :: bobox(:,:,:)
  real(rp),    pointer   :: subox(:,:,:)
  integer(ip)            :: pelty,ielem,inode,ipoin,ii
  integer(ip)            :: idimen,iboun,idime
  type(maths_bin)        :: bin_seq
  type(maths_bin)        :: bin_par
  real(rp),    pointer   :: xx(:,:),vv(:,:),dd(:,:)
  real(rp),    pointer   :: v1(:)
  real(rp),    pointer   :: xcoor(:)
  type(interpolation)    :: interp
  type(coupling_met)     :: coupl
  real(rp),    pointer   :: rr(:)
  real(rp),    pointer   :: yy(:)
  integer(8)             :: memor_loc(2)
  logical(lg), pointer   :: mask(:)
  real(rp)               :: x1,x2,x3,x4,m1,m2,m3,t1,t2,t3,Nx1,Nx2,Lp

  !
  !   Target       Source
  ! 5 o----o 6  11 o----o 12
  !   | 2  |       | 4  |
  !   |    |       |    |
  ! 3 o----o 4   9 o----o 10
  !   | 1  |       | 3  |
  !   |    |       |    |
  ! 1 o----o 2   7 o----o  8
  !             
  !  mass(2,4,6) = 1,2,2
  !
  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction
  nullify(bobox)
  nullify(subox)
  nullify(xcoor)
  nullify(xx)
  nullify(vv)
  nullify(v1)
  nullify(dd)
  nullify(yy,rr)
  nullify(mask)
  !
  ! Sequential search method
  !
  print*,'nboun=',nboun
  if( nboun > 0 ) allocate(mask(nboun))
  if(associated(mask)) mask = .true.
  do iboun = 1,nboun 
     if( kfl_codbo(iboun) == 6 ) then
        mask(iboun) = .false.
     else
        mask(iboun) = .true.
     end if
  end do
  call meshe(ndivi) % boundary_bb(bobox)
  call bin_seq % init  ()
  call bin_seq % input (boxes=(/10_ip,10_ip,1_ip/)) 
  call bin_seq % fill  (BOBOX=bobox,MASK=mask)
  if(IPARALL) then
     ! Test Case in parallel
     allocate(subox(2,ndime,0:npart))
     subox = 0.0_rp
     if( inotmaster ) then
        do idime = 1,ndime
           subox(1,idime,kfl_paral) = minval(bobox(1,idime,:))
           subox(2,idime,kfl_paral) = maxval(bobox(2,idime,:))
        end do
     else 
        subox(1,:,:) =  huge(1.0_rp)*0.1_rp
        subox(2,:,:) = -huge(1.0_rp)*0.1_rp
     end if
     call PAR_SUM(subox)
     call bin_par % init  ()
     call bin_par % input (boxes=(/10_ip,10_ip,1_ip/)) 
     call bin_par % fill  (BOBOX=subox)
     !
     ! Interpolation
     !
     call coupl % init      ()
     call coupl % input     (meshe(ndivi),bin_seq,search_method_par=bin_par,COMM=PAR_COMM_WORLD,&
          & INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION)
     if(associated(mask)) then
        mask = .not. mask
     end if
     call coupl % preprocess(meshe(ndivi),mask=mask)
  else 
     ! Test Case in sequential
     allocate(xx(2,3))  
     allocate(vv(2,3))
     allocate(v1(3))
     call coupl % init      ()
     call coupl % input     (meshe(ndivi),bin_seq,COMM=PAR_COMM_WORLD,INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION)
     if( associated(mask) ) mask = .not. mask
     call coupl % preprocess(meshe(ndivi),mask=mask)
  end if
  call memory_alloca(memor_loc,'RR','unitt_projection',rr,npoin)
  call memory_alloca(memor_loc,'YY','unitt_projection',yy,npoin)

  !
  ! Compute values
  !
  do ii = 1,npoin
     !if( lninv_loc(ii) == 3 .or. lninv_loc(ii) == 8 .or. lninv_loc(ii) == 12) then
     !if( lninv_loc(ii) == 7 .or. lninv_loc(ii) == 9 .or. lninv_loc(ii) == 11 ) then
     !if( lninv_loc(ii) == 2 .or. lninv_loc(ii) == 17.or. lninv_loc(ii) == 5.or. & 
     !   lninv_loc(ii)==23 .or.lninv_loc(ii)==9 .or. lninv_loc(ii)==29.or. lninv_loc(ii)==13   ) then
     if( lninv_loc(ii) == 2 .or. lninv_loc(ii) == 5.or. lninv_loc(ii) == 9.or. & 
          lninv_loc(ii)==13   ) then
        !rr(ii) = 2.0_rp
        rr(ii) = meshe(ndivi) % coord(2,ii)
     else
        rr(ii) = 0.0_rp
     end if
  end do
  call coupl%values_proj(rr,yy)
  !
  ! Check
  !
  do ii = 1,npoin
     print*,'projected solution=',lninv_loc(ii),yy(ii)
     !if( lninv_loc(ii) == 2 .or. lninv_loc(ii) == 4 .or. lninv_loc(ii) == 6 ) then
     !   if( abs(yy(ii)-2.0_rp) > 1.0e-12_rp ) stop 1
     !end if
  end do

  call bin_seq % deallo()

  if(IPARALL) call bin_par % deallo()
  call coupl   % deallo()

  print*,'memor int=',kfl_paral, coupl  % memor(1) 
  print*,'memor seq=',kfl_paral, bin_seq % memor(1)
  print*,'memor par=',kfl_paral, bin_par % memor(1)    


  if( bin_seq % memor(1) /= 0_8 ) then
     print*,'memor seq=',bin_seq % memor(1) ; stop 1
  end if
  if(IPARALL) then
     if( bin_par % memor(1) /= 0_8 ) then
        print*,'memor par=',bin_par % memor(1) ; stop 1
     end if
  end if
  if( coupl  % memor(1) /= 0_8 ) then
     print*,'memor int=',coupl  % memor(1) ; stop 1
  end if

  call runend('O.K.!')




end program unitt_parallel_coupling
 
