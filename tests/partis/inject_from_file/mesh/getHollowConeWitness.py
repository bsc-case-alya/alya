#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import numpy as np


def ringLine(center,normal,r,dr,dn):
	#
	line = '    RING   '
	line += 'X={}, '.format(center[0])
	line += 'Y={}, '.format(center[1])
	line += 'Z={}, '.format(center[2])
	line += 'NX={}, '.format(normal[0])
	line += 'NY={}, '.format(normal[1])
	line += 'NZ={}, '.format(normal[2])
	line += 'R={}, '.format(r)
	line += 'DR={}, '.format(dr)
	line += 'DN={}'.format(dn)
	line += '\n'

	return line


r25cm 	= np.linspace( 22e-2, 28e-2,  7)
r50cm 	= np.linspace( 45e-2, 55e-2, 11)

print(r25cm)
print(r50cm)

nWit 	= len(r25cm)+len(r50cm)
shift 	= 0.0
dr	= 1e-2
dn	= 1e-1


with open('geomWitness.dat','w') as f:
	
	f.write('WITNESS, GEOMETRY NUMBER = {}\n'.format(nWit))
	#
	# 5 mm
	#
	for r in r25cm:
		f.write( ringLine([25e-2+shift,0,0],[1,0,0],r,dr,dn) )
	for r in r50cm:
		f.write( ringLine([50e-2+shift,0,0],[1,0,0],r,dr,dn) )

	f.write('END_WITNESS\n')


