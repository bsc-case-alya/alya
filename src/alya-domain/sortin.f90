!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine sortin(n,a)

!-----------------------------------------------------------------------
!
! Sort a vector
!
!-----------------------------------------------------------------------
  use       def_kintyp
  implicit none
  integer(ip) :: n,i,j,t
  integer(ip) :: a(n)
  
  if( n <= 1 ) return

  do i=1,n-1
     do j=i+1,n
        if (a(i)>a(j)) then
           t   =a(i)
           a(i)=a(j) 
           a(j)=t
        end if
     end do
  end do
  
end subroutine sortin

subroutine sorti3(n,a)

  !-----------------------------------------------------------------------
  !
  ! Sort a vector c according to a and then b
  !
  !-----------------------------------------------------------------------
  use       def_kintyp
  implicit none
  integer(ip) :: n,i,j,t1,t2,t3
  integer(ip) :: a(3,n)

  if( n <= 1 ) return
  
  do i = 1,n-1
     do j = i+1,n
        if( a(1,i) > a(1,j) ) then
           t1     = a(1,i)
           a(1,i) = a(1,j) 
           a(1,j) = t1
           t2     = a(2,i)
           a(2,i) = a(2,j) 
           a(2,j) = t2
           t3     = a(3,i)
           a(3,i) = a(3,j) 
           a(3,j) = t3
        end if
     end do
  end do

  do i = 1,n-1
     do j = i+1,n
        if( a(1,i) == a(1,j) ) then
           if( a(2,i) > a(2,j) ) then
              t1     = a(1,i)
              a(1,i) = a(1,j) 
              a(1,j) = t1
              t2     = a(2,i)
              a(2,i) = a(2,j) 
              a(2,j) = t2
              t3     = a(3,i)
              a(3,i) = a(3,j) 
              a(3,j) = t3
           end if
        end if
     end do
  end do

  do i = 1,n-1
     do j = i+1,n
        if( a(1,i) == a(1,j) .and. a(2,i) == a(2,j) ) then
           if( a(3,i) > a(3,j) ) then
              t1     = a(1,i)
              a(1,i) = a(1,j) 
              a(1,j) = t1
              t2     = a(2,i)
              a(2,i) = a(2,j) 
              a(2,j) = t2
              t3     = a(3,i)
              a(3,i) = a(3,j) 
              a(3,j) = t3
           end if
        end if
     end do
  end do

end subroutine sorti3
