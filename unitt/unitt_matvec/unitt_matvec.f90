!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_matvec
  !
  ! test MV product
  !
  use def_kintyp_basic, only : ip,rp
  use def_master,       only : zeror
  use def_mat_fmt

  implicit none

  type(mat_tri)           :: a_tri
  type(mat_csr)           :: a_csr
  type(mat_coo)           :: a_coo
  type(mat_den)           :: a_den
  type(mat_dia)           :: a_dia
  type(mat_ell)           :: a_ell
  integer(ip)             :: nrows,ndof1,ndof2,nz
  integer(ip),   pointer  :: z(:)
  real(rp),      pointer  :: s(:)
  real(rp),      pointer  :: t(:)
  real(rp),      pointer  :: x(:,:)
  real(rp),      pointer  :: y(:,:)
  real(rp),      pointer  :: d(:,:)
  real(rp),      pointer  :: b(:,:)

  ndof1 = 1
  ndof2 = 1
  nrows = 3
  nz    = 9
  allocate(x(ndof1,nrows),y(ndof1,nrows),d(ndof1,nrows),b(ndof1,nrows),z(nrows))
  allocate(s(nrows),t(nrows))

  b      = 1.0_rp
  y      = 99.0_rp
  x(1,:) = (/-1.0_rp,-2.0_rp,-3.0_rp/)
  call a_tri % init()
  call a_csr % init()
  call a_coo % init()
  call a_den % init()
  call a_dia % init()
  call a_ell % init()
  !
  ! CSR
  !
  print*,'CSR FORMAT -----------------------'
  a_csr % ndof1 = ndof1
  a_csr % ndof2 = ndof2
  a_csr % nrows = nrows
  a_csr % nz    = nz   
  call a_csr % alloca()

  a_csr % iA(1:a_csr % nrows+1) = (/1_ip,4_ip,7_ip,10_ip/)
  a_csr % jA(1:a_csr % nz)      = (/1_ip,2_ip,3_ip,1_ip,2_ip,3_ip,1_ip,2_ip,3_ip/)
  a_csr % vA(1,1,1:a_csr % nz)  = (/1.0_rp,2.0_rp,3.0_rp,4.0_rp,5.0_rp,6.0_rp,7.0_rp,8.0_rp,9.0_rp/)

  call a_csr % mv(x,y,2_ip,3_ip)
  print*,'solu=',y
  if( abs(y(1,1)-99.0_rp) > zeror ) stop 1
  if( abs(y(1,2)+32.0_rp) > zeror ) stop 1
  if( abs(y(1,3)+50.0_rp) > zeror ) stop 1

  call a_csr % diag(DIAGONAL=d)
  print*,'diag=',d
  if( abs(d(1,1)-1.0_rp) > zeror ) stop 1
  if( abs(d(1,2)-5.0_rp) > zeror ) stop 1
  if( abs(d(1,3)-9.0_rp) > zeror ) stop 1

  call a_csr % diagz(z)
  print*,'diaz=',z
  if( z(1) /= 1 ) stop 1
  if( z(2) /= 5 ) stop 1
  if( z(3) /= 9 ) stop 1
  print*,'norm i=', a_csr % norm('i')
  print*,'norm 1=', a_csr % norm('1')
  print*,'norm f=', a_csr % norm('f')
  if( abs(a_csr % norm('i')-24.0_rp              ) > zeror ) stop 1
  if( abs(a_csr % norm('1')-18.0_rp              ) > zeror ) stop 1
  if( abs(a_csr % norm('f')-16.881943016134134_rp) > zeror ) stop 1

  call a_csr % residual(x,y,b,2_ip,3_ip)
  if( abs(y(1,1)+98.0_rp) > zeror ) stop 1
  if( abs(y(1,2)-33.0_rp) > zeror ) stop 1
  if( abs(y(1,3)-51.0_rp) > zeror ) stop 1
  y = 99.0_rp

  s = 1.0_rp
  call a_csr % mv_lower(s,t)
  if( abs(t(1)- 1.0_rp) > zeror ) stop 1
  if( abs(t(2)- 9.0_rp) > zeror ) stop 1
  if( abs(t(3)-24.0_rp) > zeror ) stop 1

  s = 1.0_rp
  call a_csr % mv_upper(s,t)
  if( abs(t(1)- 6.0_rp) > zeror ) stop 1
  if( abs(t(2)-11.0_rp) > zeror ) stop 1
  if( abs(t(3)- 9.0_rp) > zeror ) stop 1

  call a_csr % deallo()
  !
  ! DEN
  !
  print*,'DEN FORMAT -----------------------'
  a_den % nrows = 3
  a_den % ncols = 3
  call a_den % alloca()

  a_den % vA(1,1,1,1:a_csr % ncols) = (/1.0_rp,2.0_rp,3.0_rp/)
  a_den % vA(1,1,2,1:a_csr % ncols) = (/4.0_rp,5.0_rp,6.0_rp/)
  a_den % vA(1,1,3,1:a_csr % ncols) = (/7.0_rp,8.0_rp,9.0_rp/)

  call a_den % mv(x,y,2_ip,3_ip)
  print*,'solu=',y
  if( abs(y(1,1)-99.0_rp) > zeror ) stop 1
  if( abs(y(1,2)+32.0_rp) > zeror ) stop 1
  if( abs(y(1,3)+50.0_rp) > zeror ) stop 1
  call a_den % diag(DIAGONAL=d)
  print*,'diag=',d
  if( abs(d(1,1)-1.0_rp) > zeror ) stop 1
  if( abs(d(1,2)-5.0_rp) > zeror ) stop 1
  if( abs(d(1,3)-9.0_rp) > zeror ) stop 1

  print*,'norm i=', a_den % norm('i')
  print*,'norm 1=', a_den % norm('1')
  print*,'norm f=', a_den % norm('f')
  if( abs(a_den % norm('i')-24.0_rp           ) > zeror ) stop 1
  if( abs(a_den % norm('1')-18.0_rp           ) > zeror ) stop 1
  if( abs(a_den % norm('f')-16.881943016134134_rp) > zeror ) stop 1

  call a_den % residual(x,y,b,2_ip,3_ip)
  print*,'resi=',y
  if( abs(y(1,1)+98.0_rp) > zeror ) stop 1
  if( abs(y(1,2)-33.0_rp) > zeror ) stop 1
  if( abs(y(1,3)-51.0_rp) > zeror ) stop 1  
  y = 99.0_rp
  
  s = 1.0_rp
  call a_den % mv_lower(s,t)
  if( abs(t(1)- 1.0_rp) > zeror ) stop 1
  if( abs(t(2)- 9.0_rp) > zeror ) stop 1
  if( abs(t(3)-24.0_rp) > zeror ) stop 1

  s = 1.0_rp
  call a_den % mv_upper(s,t)
  if( abs(t(1)- 6.0_rp) > zeror ) stop 1
  if( abs(t(2)-11.0_rp) > zeror ) stop 1
  if( abs(t(3)- 9.0_rp) > zeror ) stop 1

  call a_den % deallo()
  !
  ! COO
  !
  print*,'COO FORMAT -----------------------'
  a_coo % ndof1 = ndof1
  a_coo % ndof2 = ndof2
  a_coo % nrows = nrows
  a_coo % nz    = nz   
  call a_coo % alloca()

  a_coo % xA(1:a_coo % nz)      = (/1_ip,1_ip,1_ip,2_ip,2_ip,2_ip,3_ip,3_ip,3_ip/)
  a_coo % yA(1:a_coo % nz)      = (/1_ip,2_ip,3_ip,1_ip,2_ip,3_ip,1_ip,2_ip,3_ip/)
  a_coo % vA(1,1,1:a_coo % nz)  = (/1.0_rp,2.0_rp,3.0_rp,4.0_rp,5.0_rp,6.0_rp,7.0_rp,8.0_rp,9.0_rp/)

  call a_coo % mv(x,y,2_ip,3_ip)
  print*,'solu=',y
  if( abs(y(1,1)-99.0_rp) > zeror ) stop 1
  if( abs(y(1,2)+32.0_rp) > zeror ) stop 1
  if( abs(y(1,3)+50.0_rp) > zeror ) stop 1
  call a_coo % diag(DIAGONAL=d)
  print*,'diag=',d
  if( abs(d(1,1)-1.0_rp) > zeror ) stop 1
  if( abs(d(1,2)-5.0_rp) > zeror ) stop 1
  if( abs(d(1,3)-9.0_rp) > zeror ) stop 1
  call a_coo % diagz(z)
  print*,'diaz=',z
  if( z(1) /= 1 ) stop 1
  if( z(2) /= 5 ) stop 1
  if( z(3) /= 9 ) stop 1
  print*,'norm i=', a_coo % norm('i')
  print*,'norm 1=', a_coo % norm('1')
  print*,'norm f=', a_coo % norm('f')
  if( abs(a_coo % norm('i')-24.0_rp           ) > zeror ) stop 1
  if( abs(a_coo % norm('1')-18.0_rp           ) > zeror ) stop 1
  if( abs(a_coo % norm('f')-16.881943016134134_rp) > zeror ) stop 1

  call a_coo % residual(x,y,b,2_ip,3_ip)
  print*,'resi=',y
  if( abs(y(1,1)+98.0_rp) > zeror ) stop 1
  if( abs(y(1,2)-33.0_rp) > zeror ) stop 1
  if( abs(y(1,3)-51.0_rp) > zeror ) stop 1  
  y = 99.0_rp
  
  s = 1.0_rp
  call a_coo % mv_lower(s,t)
  if( abs(t(1)- 1.0_rp) > zeror ) stop 1
  if( abs(t(2)- 9.0_rp) > zeror ) stop 1
  if( abs(t(3)-24.0_rp) > zeror ) stop 1

  s = 1.0_rp
  call a_coo % mv_upper(s,t)
  if( abs(t(1)- 6.0_rp) > zeror ) stop 1
  if( abs(t(2)-11.0_rp) > zeror ) stop 1
  if( abs(t(3)- 9.0_rp) > zeror ) stop 1

  call a_coo % deallo()
  !
  ! TRI
  !
  print*,'TRI FORMAT -----------------------'
  a_tri % nrows = 3
  a_tri % ncols = 3
  call a_tri % alloca()

  a_tri % vA(1,1,1,1:a_tri % nrows) = (/0.0_rp,4.0_rp,8.0_rp/)
  a_tri % vA(1,1,2,1:a_tri % nrows) = (/1.0_rp,5.0_rp,9.0_rp/)
  a_tri % vA(1,1,3,1:a_tri % nrows) = (/2.0_rp,6.0_rp,0.0_rp/)

  call a_tri % mv(x,y,2_ip,3_ip)
  print*,'solu=',y
  if( abs(y(1,1)-99.0_rp) > zeror ) stop 1
  if( abs(y(1,2)+32.0_rp) > zeror ) stop 1
  if( abs(y(1,3)+43.0_rp) > zeror ) stop 1
  call a_tri % diag(DIAGONAL=d)
  print*,'diag=',d
  if( abs(d(1,1)-1.0_rp) > zeror ) stop 1
  if( abs(d(1,2)-5.0_rp) > zeror ) stop 1
  if( abs(d(1,3)-9.0_rp) > zeror ) stop 1

  print*,'norm i=', a_tri % norm('i')
  print*,'norm 1=', a_tri % norm('1')
  print*,'norm f=', a_tri % norm('f')

  y = 99.0_rp
  call a_tri % residual(x,y,b,2_ip,3_ip)
  print*,'resi=',y
  if( abs(y(1,1)+98.0_rp) > zeror ) stop 1
  if( abs(y(1,2)-33.0_rp) > zeror ) stop 1
  if( abs(y(1,3)-44.0_rp) > zeror ) stop 1  
  y = 99.0_rp  

  s = 1.0_rp
  call a_tri % mv_lower(s,t)
  print*,'t lower=',t
  if( abs(t(1)- 1.0_rp) > zeror ) stop 2
  if( abs(t(2)- 9.0_rp) > zeror ) stop 2
  if( abs(t(3)-17.0_rp) > zeror ) stop 2

  s = 1.0_rp
  call a_tri % mv_upper(s,t)
  print*,'t upper=',t
  if( abs(t(1)- 3.0_rp) > zeror ) stop 3
  if( abs(t(2)-11.0_rp) > zeror ) stop 3
  if( abs(t(3)- 9.0_rp) > zeror ) stop 3
  
  call a_tri % deallo()

  a_den % nrows = 3
  a_den % ncols = 3
  call a_den % alloca()

  a_den % vA(1,1,1,1:a_den % ncols) = (/1.0_rp,2.0_rp,0.0_rp/)
  a_den % vA(1,1,2,1:a_den % ncols) = (/4.0_rp,5.0_rp,6.0_rp/)
  a_den % vA(1,1,3,1:a_den % ncols) = (/0.0_rp,8.0_rp,9.0_rp/)

  call a_den % mv(x,y,2_ip,3_ip)
  print*,'solu=',y
  call a_den % deallo()
  if( abs(y(1,1)-99.0_rp) > zeror ) stop 1
  if( abs(y(1,2)+32.0_rp) > zeror ) stop 1
  if( abs(y(1,3)+43.0_rp) > zeror ) stop 1
  !
  ! DIA
  !
  print*,'DIA FORMAT -----------------------'
  a_dia % nrows = 3
  a_dia % ncols = 3
  a_dia % ndof1 = 1
  call a_dia % alloca()

  a_dia % vA(1,1:a_dia % nrows) = (/1.0_rp,2.0_rp,3.0_rp/)

  call a_dia % mv(x,y,2_ip,3_ip)
  print*,'solu=',y
  if( abs(y(1,1)-99.0_rp) > zeror ) stop 1
  if( abs(y(1,2)+ 4.0_rp) > zeror ) stop 1
  if( abs(y(1,3)+ 9.0_rp) > zeror ) stop 1
  
  call a_dia % diag(DIAGONAL=d)
  print*,'diag=',d
  if( abs(d(1,1)-1.0_rp) > zeror ) stop 1
  if( abs(d(1,2)-2.0_rp) > zeror ) stop 1
  if( abs(d(1,3)-3.0_rp) > zeror ) stop 1
  
  print*,'norm i=', a_dia % norm('i')
  print*,'norm 1=', a_dia % norm('1')
  print*,'norm f=', a_dia % norm('f')

  s = 1.0_rp
  call a_dia % mv_lower(s,t)
  print*,'t lower=',t
  if( abs(t(1)- 1.0_rp) > zeror ) stop 2
  if( abs(t(2)- 2.0_rp) > zeror ) stop 2
  if( abs(t(3)- 3.0_rp) > zeror ) stop 2

  s = 1.0_rp
  call a_dia % mv_upper(s,t)
  print*,'t upper=',t
  if( abs(t(1)- 1.0_rp) > zeror ) stop 3
  if( abs(t(2)- 2.0_rp) > zeror ) stop 3
  if( abs(t(3)- 3.0_rp) > zeror ) stop 3

  call a_dia % deallo()
  !
  ! ELL
  !
  print*,'ELL FORMAT -----------------------'
  a_ell % nrows = 3
  a_ell % comax = 3
  a_ell % ndof1 = 1
  a_ell % ndof2 = 1
  call a_ell % alloca()

  a_ell % vA(1,1,1:3,1) = (/1.0_rp,2.0_rp,0.0_rp/)
  a_ell % vA(1,1,1:3,2) = (/4.0_rp,5.0_rp,6.0_rp/)
  a_ell % vA(1,1,1:3,3) = (/8.0_rp,9.0_rp,0.0_rp/)

  a_ell % jA(1:3,1)     = (/1_ip,2_ip,2_ip/) 
  a_ell % jA(1:3,2)     = (/1_ip,2_ip,3_ip/) 
  a_ell % jA(1:3,3)     = (/2_ip,3_ip,3_ip/) 

  call a_ell % mv(x,y,2_ip,3_ip)
  print*,'solu=',y
  if( abs(y(1,1)-99.0_rp) > zeror ) stop 1
  if( abs(y(1,2)+32.0_rp) > zeror ) stop 1
  if( abs(y(1,3)+43.0_rp) > zeror ) stop 1

  call a_ell % diag(DIAGONAL=d)
  print*,'diag=',d
  if( abs(d(1,1)-1.0_rp) > zeror ) stop 1
  if( abs(d(1,2)-5.0_rp) > zeror ) stop 1
  if( abs(d(1,3)-9.0_rp) > zeror ) stop 1

  call a_ell % diagz(z)
  print*,'diaz=',z
  if( z(1) /= 1 ) stop 1
  if( z(2) /= 2 ) stop 1
  if( z(3) /= 2 ) stop 1

  a_ell % vA(1,1,1:3,1) = (/1.0_rp,2.0_rp,3.0_rp/)
  a_ell % vA(1,1,1:3,2) = (/4.0_rp,5.0_rp,6.0_rp/)
  a_ell % vA(1,1,1:3,3) = (/7.0_rp,8.0_rp,9.0_rp/)
  
  a_ell % jA(1:3,1)     = (/1_ip,2_ip,3_ip/) 
  a_ell % jA(1:3,2)     = (/1_ip,2_ip,3_ip/) 
  a_ell % jA(1:3,3)     = (/1_ip,2_ip,3_ip/) 
 
  print*,'norm i=', a_ell % norm('i')
  print*,'norm 1=', a_ell % norm('1')
  print*,'norm f=', a_ell % norm('f')

  if( abs(a_ell % norm('i')-24.0_rp           ) > zeror ) stop 1
  if( abs(a_ell % norm('1')-18.0_rp           ) > zeror ) stop 1
  if( abs(a_ell % norm('f')-16.881943016134134_rp) > zeror ) stop 1

  call a_ell % residual(x,y,b,2_ip,3_ip)
  print*,'resi=',y
  if( abs(y(1,1)+98.0_rp) > zeror ) stop 1
  if( abs(y(1,2)-33.0_rp) > zeror ) stop 1
  if( abs(y(1,3)-51.0_rp) > zeror ) stop 1  
  y = 99.0_rp  

  s = 1.0_rp
  call a_ell % mv_lower(s,t)
  if( abs(t(1)- 1.0_rp) > zeror ) stop 1
  if( abs(t(2)- 9.0_rp) > zeror ) stop 1
  if( abs(t(3)-24.0_rp) > zeror ) stop 1

  s = 1.0_rp
  call a_ell % mv_upper(s,t)
  if( abs(t(1)- 6.0_rp) > zeror ) stop 1
  if( abs(t(2)-11.0_rp) > zeror ) stop 1
  if( abs(t(3)- 9.0_rp) > zeror ) stop 1

  call a_ell % deallo()
  
end program unitt_matvec

