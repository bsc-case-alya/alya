!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_elmmat_chm(pnode,ielem,elrbu)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_elmmat_chm
  ! NAME 
  !    nsi_elmmat_chm
  ! DESCRIPTION
  !    Modify RHS related to the concentrations terms
  ! USES
  ! USED BY
  !***
  !----------------------------------------------------------------------- 
  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime
  use def_master, only       :  RhsadjNas_chm
  
  implicit none
  integer(ip), intent(in)    :: pnode,ielem
  real(rp),    intent(inout) :: elrbu(ndime,pnode)
  integer(ip)                :: inode,idime
  
  !----------------------------------------------------------------------
  !
  !                 !  ELRHS = ELRHS - [dRs/du] [Lambda_s]  sent by chemic
  !
  !----------------------------------------------------------------------

  do idime = 1,ndime
    do inode = 1,pnode
      elrbu(idime,inode) = elrbu(idime,inode) - RhsadjNas_chm(ielem)%a(idime, inode)
    enddo
  enddo

  
end subroutine nsi_elmmat_chm
