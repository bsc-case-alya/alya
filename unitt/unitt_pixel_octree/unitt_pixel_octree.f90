!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_search_skdtree

  use def_kintyp_basic
  use def_parame
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_maths_skdtree
  use def_maths_tree
  use def_search_method
  use def_master
  use mod_memory
  use def_kermod
  use def_domain
  use def_interpolation_method
  use mod_random
  use mod_partitioning
  implicit none
  type(mesh_type_basic)           :: mesh_out
  type(maths_octree)              :: skdtree
  integer(ip)                     :: ipoin,idime,inodb,nn
  integer(ip)                     :: pelty,ii
  integer(ip)                     :: kpart,ipart
  integer(8)                      :: memor_loc(2)
  integer(8)                      :: memor_tmp(2)
  real(rp)                        :: proje(3),coloc(3),vol1,vol2
  real(rp)                        :: vmin(3),r,theta
  real(rp)                        :: bocod(2,64)
  real(rp)                        :: shapf(64)
  real(rp)                        :: shapf_min(64)
  real(rp)                        :: derit(2,64)
  real(rp),             pointer   :: bobox(:,:,:)
  character(len=5),     pointer   :: names(:)
  real(rp),             pointer   :: res(:,:)
  integer(ip),          pointer   :: lpart(:)
  
  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction

  memor_loc = 0_8
  memor_tmp = 0_8
  nullify(bobox,names,res,lpart)
  !
  ! Bounding boxes
  !
  call meshe(ndivi) % element_bb(bobox)

  !----------------------------------------------------------------------
  !
  ! Octree
  !  
  !----------------------------------------------------------------------

  call skdtree  % init    ()
  call skdtree  % input   (LIMIT=20_ip)
  call skdtree  % fill    (BOBOX=bobox)
  
  call mesh_out % init    () 
  call skdtree  % mesh    (&
       mesh_out % ndime,mesh_out % mnode,mesh_out % nelem,&
       mesh_out % npoin,mesh_out % lnods,mesh_out % ltype,&
       mesh_out % coord,CRITERION=SEARCH_MESH_FILL,MEMORY_COUNTER=memor_loc)
  call skdtree  % results (res,names,CRITERION=SEARCH_MESH_FILL)
  call mesh_out % output  (          FILENAME='skdtree')
  call mesh_out % results (res,names,FILENAME='skdtree')
  call mesh_out % deallo  (MEMORY_COUNTER=memor_loc)
  
  !----------------------------------------------------------------------
  !
  ! Using groups
  !  
  !----------------------------------------------------------------------

  kpart = 20
  
  deallocate(bobox)
  allocate(lpart(npoin))
  do ipoin = 1,npoin
     lpart(ipoin) = 0
  end do
  
  call partitioning_frontal(kpart,npoin,r_dom,c_dom,lpart)
  
  allocate(bobox(2,ndime,kpart))
  do ipart = 1,kpart
     bobox(1,:,ipart) =  huge(1.0_rp)
     bobox(2,:,ipart) = -huge(1.0_rp)
  end do
  do ipoin = 1,npoin
     ipart = lpart(ipoin)
     bobox(1,:,ipart) = min(bobox(1,:,ipart),coord(:,ipoin))
     bobox(2,:,ipart) = max(bobox(2,:,ipart),coord(:,ipoin))
  end do
  call mesh_out % init()
  call mesh_out % mesh_bb(bobox)
  call mesh_out % output (FILENAME='bounding_boxes')
  vol1 = mesh_out     % volume()
  vol2 = meshe(ndivi) % volume()
  print*,'volume=',vol1,vol2

  call runend('O.K.!')
  
end program unitt_search_skdtree
 
