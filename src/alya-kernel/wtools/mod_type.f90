!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Kernel
!> @{
!> @file    mod_type.f90
!> @author  houzeaux
!> @date    2020-06-17
!> @brief   Return the type of a variable
!> @details Return the type of a variable
!-----------------------------------------------------------------------

module mod_type

  implicit none
  private
  
  interface my_type
     module procedure my_type0,my_type1,my_type2,my_type3,my_type4
  end interface my_type

  public :: my_type
  
contains

  function my_type0(object) result(current_type)

    class(*),         intent(in)  :: object
    character(len=:), allocatable :: current_type

    select type(object)
       
    type is (real(4))
       
       current_type = 'real'
       
    type is (real(8))
       
       current_type = 'real'
       
    type is (integer(4))
       
       current_type = 'integer'
       
    type is (integer(8))
       
       current_type = 'integer'
       
    class default

       current_type = 'unknown'       
       
    end select
    
  end function my_type0

  function my_type1(object) result(current_type)

    class(*),         dimension(:), intent(in) :: object
    character(len=:), allocatable              :: current_type

    select type(object)
       
    type is (real(4))
       
       current_type = 'real'
       
    type is (real(8))
       
       current_type = 'real'
       
    type is (integer(4))
       
       current_type = 'integer'
       
    type is (integer(8))
       
       current_type = 'integer'
       
    end select

  end function my_type1

  function my_type2(object) result(current_type)

    class(*),         dimension(:,:), intent(in) :: object
    character(len=:), allocatable                :: current_type

    select type(object)
       
    type is (real(4))
       
       current_type = 'real'
       
    type is (real(8))
       
       current_type = 'real'
       
    type is (integer(4))
       
       current_type = 'integer'
       
    type is (integer(8))
       
       current_type = 'integer'
       
    class default

       current_type = 'unknown'
       
    end select

  end function my_type2
  
  function my_type3(object) result(current_type)

    class(*),         dimension(:,:,:), intent(in) :: object
    character(len=:), allocatable                  :: current_type

    select type(object)
       
    type is (real(4))
       
       current_type = 'real'
       
    type is (real(8))
       
       current_type = 'real'
       
    type is (integer(4))
       
       current_type = 'integer'
       
    type is (integer(8))
       
       current_type = 'integer'
       
    class default

       current_type = 'unknown'
       
    end select

  end function my_type3

  function my_type4(object) result(current_type)

    class(*),         dimension(:,:,:,:), intent(in) :: object
    character(len=:), allocatable                    :: current_type

    select type(object)
       
    type is (real(4))
       
       current_type = 'real'
       
    type is (real(8))
       
       current_type = 'real'
       
    type is (integer(4))
       
       current_type = 'integer'
       
    type is (integer(8))
       
       current_type = 'integer'
       
    class default

       current_type = 'unknown'
       
    end select

  end function my_type4

end module mod_type
!> @}

