!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Kernel
!> @{
!> @file    mod_func.f90
!> @author  houzeaux and eduardo perez
!> @date    2020-02-03
!> @brief   Functions
!> @details Generic functions
!-----------------------------------------------------------------------

module mod_func

  use def_kintyp
  implicit none

  abstract interface
#ifdef R4
     real(4) function func(x,gradx)
       real(4)           :: x(:)
       real(4), optional :: gradx(:,:)
     end function func
#elif R16
     real(16) function func(x,gradx)
       real(16)           :: x(:)
       real(16), optional :: gradx(:,:)
     end function func
#else
     real(8) function func(x,gradx)
       real(8)           :: x(:)
       real(8), optional :: gradx(:,:)
     end function func
#endif     
  end interface

  type func_ptr
     procedure(func), pointer, nopass :: f
  end type func_ptr

contains

  real(rp) function func_identity(val,grad)
    real(rp)           :: val(:)
    real(rp), optional :: grad(:,:)
    func_identity = val(1)
  end function func_identity

  real(rp) function func_square(val,grad)
    real(rp)           :: val(:)
    real(rp), optional :: grad(:,:)
    func_square = dot_product(val,val)
  end function func_square

  real(rp) function func_unity(val,grad)
    real(rp)           :: val(:)
    real(rp), optional :: grad(:,:)
    func_unity = 1.0_rp
  end function func_unity

  real(rp) function func_sine(val,grad)
    real(rp)           :: val(:)
    real(rp), optional :: grad(:,:)
    func_sine = sin(val(1))
  end function func_sine

  real(rp) function func_cosine(val,grad)
    real(rp)           :: val(:)
    real(rp), optional :: grad(:,:)
    func_cosine = cos(val(1))
  end function func_cosine

  real(rp) function func_norm(val,grad)
    real(rp)           :: val(:)
    real(rp), optional :: grad(:,:)
    func_norm = sqrt(dot_product(val,val))
  end function func_norm

  real(rp) function func_div(val,grad)
    real(rp)           :: val(:)
    real(rp), optional :: grad(:,:)
    integer(ip)        :: ii    
    func_div = 0.0_rp
    if( present(grad) ) then
       do ii = 1,min(size(grad,1),size(grad,2))
          func_div = func_div + grad(ii,ii)
       end do
    end if
  end function func_div

  real(rp) function func_rotational_norm(val,grad)
    real(rp)           :: val(:)
    real(rp), optional :: grad(:,:)
    integer(ip)        :: ii
    real(rp)           :: rot(3)
    func_rotational_norm = 0.0_rp
    if( present(grad) ) then
       if( size(grad,1) == 2 ) then
          rot(1) = grad(1,2)-grad(2,1)
          func_rotational_norm = abs(rot(1))
       else
          rot(1) = grad(2,3)-grad(3,2)
          rot(2) = grad(3,1)-grad(1,3)
          rot(3) = grad(1,2)-grad(2,1)
          func_rotational_norm = sqrt(dot_product(rot,rot))
       end if
    end if
  end function func_rotational_norm

  subroutine func_initialization(my_func)
    type(func_ptr), intent(inout) :: my_func(:,:)
    integer(ip)                   :: ii,jj
    do jj = 1,size(my_func,2)
       do ii = 1,size(my_func,1)
          my_func(ii,jj) % f => func_unity
       end do
    end do    
  end subroutine func_initialization

end module mod_func
!> @}
