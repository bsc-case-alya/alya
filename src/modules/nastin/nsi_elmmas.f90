!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_elmmas(&
     pnode,pgaus,gpsha,gpvol,gpden,elmas)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_elmma4
  ! NAME 
  !    nsi_elmma4
  ! DESCRIPTION
  !    Compute element matrix and RHS
  ! USES
  ! USED BY
  !    nsi_elmop3
  !***
  !----------------------------------------------------------------------- 
  use def_kintyp, only       :  ip,rp
  implicit none
  integer(ip), intent(in)    :: pnode,pgaus
  real(rp),    intent(in)    :: gpsha(pnode,pgaus)
  real(rp),    intent(in)    :: gpvol(pgaus)
  real(rp),    intent(in)    :: gpden(pgaus)
  real(rp),    intent(out)   :: elmas(pnode)
  integer(ip)                :: inode,igaus
  real(rp)                   :: xfact

     elmas = 0.0_rp
     do igaus = 1,pgaus
        xfact = gpvol(igaus) * gpden(igaus)
        do inode = 1,pnode
           elmas(inode) = elmas(inode) + gpsha(inode,igaus) * xfact
        end do
     end do

end subroutine nsi_elmmas
