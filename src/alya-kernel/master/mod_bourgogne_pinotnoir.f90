!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



module mod_bourgogne_pinotnoir

contains
  !DEC$ ATTRIBUTES FORCEINLINE :: bourgogne
  subroutine bourgogne(itask)

    use def_kintyp, only : ip,rp
    use def_master, only : INOTSLAVE,cpu_initi
    use def_parall
    implicit none
    integer(ip), intent(in) :: itask
    !CHARACTER(50)           :: s = "Alya-License.lic"
    CHARACTER(500)          :: licenseName
    INTEGER(4)              :: hourCounter = 50
    REAL(rp)                :: caca
    LOGICAL                 :: license_exist

#ifdef BOURGOGNE

    if( INOTSLAVE ) then
      !CALL get_command_argument(1, licenseName
      !licenseName = s

      CALL get_command_argument(2, licenseName)

      INQUIRE(FILE=licenseName, EXIST=license_exist)
      if(license_exist) then


        if( itask == 1 ) then
          call check(licenseName)

        else
          !Actualize License
          call cputim(caca)
          caca = ((caca - cpu_initi))
          call actualize(licenseName, caca)     
        end if
      else
        STOP "License FILE do not exist"
      end if
    end if

#endif

  end subroutine bourgogne

end module mod_bourgogne_pinotnoir
