#!/bin/bash
#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



#SBATCH --job-name=ts
#SBATCH --output=output.out
#SBATCH --error=output.err
#SBATCH --ntasks=4
#SBATCH --qos=debug
#SBATCH --time=00:02:00
export CASE=${PWD##*/}  
export ROMIO_HINTS=./io_hints
export I_MPI_EXTRA_FILESYSTEM_LIST=gpfs
export I_MPI_EXTRA_FILESYSTEM=on
module unload intel
module load gcc
mpirun $ALYA/alya/Executables/unix-i8-TS/Alya.x $CASE
mpirun mpio2txt $CASE
mv *.post.mpio.txt base/4p/
