#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(gmsh)
  if (WITH_GMSH MATCHES "auto")
    set(GMSH_HOME "${CMAKE_BINARY_DIR}/auto/gmsh")
    add_dependencies(${PROJECT_NAME} gmsh)
    add_definitions(-DALYA_GMSH)
    include_directories(${GMSH_INCLUDE_DIR})
    set_property(GLOBAL PROPERTY FIND_LIBRARY_USE_LIB64_PATHS ON)
    include(GNUInstallDirs)
    if (APPLE)
      set(GMSH_LIBRARY ${GMSH_HOME}/${CMAKE_INSTALL_LIBDIR}/libgmsh.dylib)
    else()
      set(GMSH_LIBRARY ${GMSH_HOME}/${CMAKE_INSTALL_LIBDIR}/libgmsh.so)
    endif()
    target_link_libraries(${PROJECT_NAME} ${GMSH_LIBRARY})
    set_target_properties(${PROJECT_NAME} PROPERTIES INSTALL_RPATH "${GMSH_HOME}/${CMAKE_INSTALL_LIBDIR}")
    set_target_properties(${PROJECT_NAME} PROPERTIES BUILD_WITH_INSTALL_RPATH ON)
  elseif (WITH_GMSH MATCHES "external")
    add_definitions(-DALYA_GMSH)
    set(GMSH_INCLUDE_DIR "${GMSH_DIR}/include" CACHE INTERNAL "GMSH: include directory" FORCE)
    set_property(GLOBAL PROPERTY FIND_LIBRARY_USE_LIB64_PATHS ON)
    include(GNUInstallDirs)
    include_directories(${GMSH_INCLUDE_DIR})
    find_library(GMSH_LIBRARY gmsh PATHS ${GMSH_DIR}/${CMAKE_INSTALL_LIBDIR})
    target_link_libraries(${PROJECT_NAME} ${GMSH_LIBRARY})
  endif()
endfunction()
