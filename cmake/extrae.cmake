#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(init_extrae)
  set(EXTRAE_INCLUDE_DIR "${EXTRAE_DIR}/include" CACHE INTERNAL "")
  set(EXTRAE_LIB_DIR "${EXTRAE_DIR}/lib" CACHE INTERNAL "")
endfunction()

function(extrae)
  if (WITH_EXTRAE MATCHES "external")
    add_definitions(-DALYA_EXTRAE)
    include_directories(${EXTRAE_INCLUDE_DIR})
    link_directories(${EXTRAE_LIB_DIR})

    find_library(EXTRAE_LIBRARY
       NAMES mpitracef
       PATHS ${EXTRAE_LIB_DIR} 
       REQUIRED
    )

    target_link_libraries(${PROJECT_NAME} ${EXTRAE_LIBRARY})
  endif()
endfunction()
