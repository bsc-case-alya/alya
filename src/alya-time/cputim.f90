!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup CPU_Time
!> @{
!> @file    cputim.f90
!> @author  houzeaux
!> @date    2018-12-30
!> @brief   Compute current time
!> @details This routine finds out the CPU time in seconds
!-----------------------------------------------------------------------

subroutine cputim(rtime)

  use def_kintyp,    only : rp
  use def_master,    only : rate_time
  implicit none
  real(rp), intent(out) :: rtime
  real(8)               :: rtim8
  real(4)               :: rtim4,elap4(2),etime
  integer(4)            :: itim4
  integer(8)            :: itim8

  if( 1 == 1 ) then
    
     call system_clock(itim8)
     rtime = real(itim8,rp) * rate_time

  else if( 1 == 2 ) then

     call cpu_time(rtime)

  else if( 1 == 3 ) then
     !
     ! This method requires linking with -lrt
     ! Commented temporarly (discussion is required)
     ! ! Using C Wall time routine
     ! call wall_time(rtim8)
     ! ! Just in case rp is not 8
     ! rtime = rtim8
  end if

end subroutine cputim
