#
# -----------------
# ALGEBRAIC SOLVER:
# -----------------
# 
# TYPE OF SOLVER        : CONJUGATE GRADIENT
# --------------
# MINIMUM TOLERANCE     = 0.1000E-08
# MAX. ITERATIONS       =       5000
# ADAPTIVE RESIDUAL     : OFF
# NON-ZERO COEFF.       =       1792 (R*8), MEMORY=   14.00 kbytes
# FORMAT                : CSR
#
# PARALLELIZATION       : 
# ---------------
# SpMV MPI MATRIX       : PARTIAL ROW (SQUARE MATRIX)
# SpMV MPI COMMUNICAT.  : NON-BLOCKING
#
# PRECONDITIONER        : NONE                                              
# --------------
# LEFT/RIGHT            : M^{-1/2} A M^{-1/2} M^{1/2} x = M^{-1/2} b
#
# --|  Columns displayed:
# --|  1. Number of iterations   2. Initial prec. residual   
# --|  3. Final prec. residual   4. Initial residual         
# --|  5. Final residual         6. Convergence rate         
# --|  7. CPU time               8. Orthogonality            
# --|  9. RHS norm ||b||        10. Precon. upd (1=yes,0=no) 
# --| 11. Cond. number kappa    12. Min. eigenvalue          
# --| 13. Max. eigenvalue                                    
# --|                                                        
# --| -999 Means was not calculated                          
# --| -888 Means was not converged                           
# --|                                                        
#                                                            
#     1                 2                 3                 4                 5                 6                 7                 8                 9            10                11                12                13
#
# Time step:        0, Outer iter:      0, Inner iter:      0
     18   0.10000000E+001   0.49267080E-009   0.10000000E+001   0.49267098E-009   0.21955909E+001   0.92983246E-004  -0.49636381E-008   0.30298515E-003             1  -0.99900000E+003  -0.99900000E+003  -0.99900000E+003
