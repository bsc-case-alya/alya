!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_sequential_solver
  !
  ! Order strings
  !
  use def_kintyp_basic, only : ip,rp 
  use def_master,       only : rate_time,zeror
  use def_mat_csr
  use def_mat_dia
  use def_mat_sky
  use def_iterative_solvers
  use def_direct_solvers
  use def_solver
  use def_solvers
  use mod_driver_solvers
  use def_all_solvers
  use def_preconditioners
  implicit none

  class(solver),           pointer   :: sol
  type(mat_csr)                      :: a
  type(mat_sky)                      :: sky
  real(rp),                pointer   :: x(:),b(:),r(:)
  integer(ip)                        :: ii,iz,num
  integer(8)                         :: count_rate8
  ! Deflated CG
  integer(ip)                        :: nzgro
  integer(ip)                        :: ngrou
  integer(ip),             pointer   :: ia(:)
  integer(ip),             pointer   :: ja(:)
  integer(ip),             pointer   :: lgrou(:)
  ! Linelet
  integer(ip)                        :: nline
  integer(ip),             pointer   :: lline(:)
  integer(ip),             pointer   :: lrenup(:)
  !
  ! Nullify
  !
  nullify(x,b,r,ia,ja,lgrou,lline,lrenup,sol)
  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)
  !
  ! Allocate matrix
  !
  call a % init   ()
  a % nrows = 11
  a % nz    = 31
  call a % alloca ()
  allocate(b(a % nrows))
  allocate(x(a % nrows))
  allocate(r(a % nrows))

  x    = 0.0_rp
  b    = 0.0_rp
  b(6) = 1.0_rp
  
  iz = 0
  a % iA(1) = 1 
  a % iA(2) = 3 
  iz = iz + 1 ; a % vA(1,1,iz) = 1.0_rp ; a % jA(iz) = 1
  iz = iz + 1 ; a % vA(1,1,iz) = 0.0_rp ; a % jA(iz) = 2

  do ii = 2,a % nrows-1
     if( ii == 2 ) then
        iz = iz + 1 ; a % vA(1,1,iz) =  0.0_rp ; a % jA(iz) = ii-1
     else 
        iz = iz + 1 ; a % vA(1,1,iz) = -1.0_rp ; a % jA(iz) = ii-1
     end if
     iz = iz + 1 ; a % vA(1,1,iz) =  2.0_rp ; a % jA(iz) = ii
     if( ii == a % nrows-1 ) then
        iz = iz + 1 ; a % vA(1,1,iz) =  0.0_rp ; a % jA(iz) = ii+1
     else
        iz = iz + 1 ; a % vA(1,1,iz) = -1.0_rp ; a % jA(iz) = ii+1
     end if
     a % iA(ii+1) = a % iA(ii) + 3
  end do

  ii = a % nrows
  a % iA(ii+1) = a % iA(ii) + 2
  iz = iz + 1 ; a % vA(1,1,iz) = 0.0_rp ; a % jA(iz) = ii-1
  iz = iz + 1 ; a % vA(1,1,iz) = 1.0_rp ; a % jA(iz) = ii

  call a % output(FMT='DENSE')
  !
  ! Skyline matrix
  !
  call sky % init()
  call sky % csr2sky(a)
  !
  ! Deflated
  !
  !allocate(lgrou(a % nrows))
  !ngrou = 2
  !nzgro = 4
  !allocate(ia(ngrou+1))
  !allocate(ja(nzgro))
  !ia    = (/1,3,5/)
  !ja    = (/1,2,1,2/)
  !lgrou = (/0,1,1,1,1,2,2,2,2,2,0/)

  allocate(lgrou(a % nrows))
  ngrou = 9
  nzgro = 25
  allocate(ia(ngrou+1))
  allocate(ja(nzgro))
  ia    = (/1,3,6,9,12,15,18,21,24,26/)
  ja    = (/1,2,1,2,3,2,3,4,3,4,5,4,5,6,5,6,7,6,7,8,7,8,9,8,9/)
  lgrou = (/0,1,2,3,4,5,6,7,8,9,0/)

  !ngrou = 0
  !
  ! Linelet
  !
  !nline    = 2
  !allocate(lline(nline+1))
  !lline(1) = 1
  !lline(2) = 4
  !lline(3) = 7
  !allocate(lrenup(lline(nline+1)))  
  !lrenup   = (/1,2,3,11,10,9/)
  nline    = 1
  allocate(lline(nline+1))
  lline(1) =  1
  lline(2) = 12
  allocate(lrenup(lline(nline+1)))  
  lrenup   = (/1,2,3,4,5,6,7,8,9,10,11/)
  !
  ! Solvers
  !
  !do ii = 1,19
  do ii = 6,6

     x     = 0.0_rp
     b     = 0.1_rp
     b(1)  = 0.0_rp
     b(11) = 0.0_rp
     
     print*,''
     call select_solver(ii,num)
     
     call solver_alloca(sol,num)
     call solver_init  (sol)
     call solver_dim   (sol,a)

     sol % input % relax              = 0.5_rp
     sol % input % kfl_symm           = 1
     if( ii == 4 .or. ii == 10 ) then
        sol % input % kfl_preco       = SOL_SOLVER_SOR
     else if( ii == 7 .or. ii == 11 ) then
        sol % input % kfl_preco       = SOL_SOLVER_SSOR
     else if ( ii == 13 ) then
        sol % input % kfl_preco       = SOL_SOLVER_APPROX_INVERSE
        sol % input % levels_sparsity = 6
        sol % input % relax           = 1.0_rp
     else if ( ii == 14 ) then
        sol % input % kfl_preco       = SOL_SOLVER_APPROX_INVERSE
        sol % input % levels_sparsity = 6
        sol % input % relax           = 1.0_rp
        sol % input % kfl_symm        = 0
     else if ( ii == 15 ) then
        sol % input % kfl_preco       = SOL_SOLVER_LINELET
        sol % input % relax           = 1.0_rp        
     else
        sol % input % kfl_preco = SOL_SOLVER_DIAGONAL
     end if
     
     if( ii == 8 .or. ii == 10 .or. ii == 11 ) then
        sol % input % kfl_leftr = SOL_RIGHT_PRECONDITIONING
     else
        sol % input % kfl_leftr = SOL_LEFT_PRECONDITIONING
     end if

     if( ii == 12 ) sol % input % kfl_defas = SOL_CSR
     if( ii == 19 ) sol % input % kfl_defas = SOL_SKYLINE
     
     sol % input % nkryd            = 4     
     sol % input % solco            = 1.0e-8_rp
     sol % input % miter            = 1000
     sol % input % kfl_exres        = 1
     sol % input % kfl_cvgso        = 1
     sol % input % lun_cvgso        = 90+ii     
     !
     ! Set solver/preconditioner arrays
     !
     select type ( sol )
     class is ( dcg ) ; call sol % set(ngrou,lgrou,ia,ja)
     end select

     select type ( sol )
     class is ( iterative_solver )
        call preconditioner_alloca(sol)    
        if( sol % input % kfl_preco == SOL_SOLVER_LINELET ) then
           select type ( v => sol % preco(1) % p )
           class is ( linelet ) ; call v % set(nline,lline,lrenup)
           end select
        end if
     end select
     !
     ! Symbolic factorization
     !
     select type ( sol )
     class is ( direct_factorization ) ; call sol % set(a)
     end select
     !
     ! Solver preprocess, solve and postprocess
     !
     print*,'solver'
     if( ii == 18 ) then
        call solver_preprocess (sol,b,x,sky)
        call solver_solve      (sol,b,x,sky)
        call solver_postprocess(sol,b,x,sky)
     else
        call solver_preprocess (sol,b,x,a)
        call solver_solve      (sol,b,x,a)
        call solver_postprocess(sol,b,x,a)
     end if

     select type ( sol )
     class is ( direct_solver )
        call a % residual(x,r,b)
        sol % output % resip_final = sol % parallel_L2norm(r)
     end select
     !
     ! Output
     !
     print*,'resip     = ',sol % output % resip_final
     print*,'resid     = ',sol % output % resid_final
     print*,'iters     = ',sol % output % iters
     print*,'||b||     = ',sol % output % bnorm
     print*,'||L^-1b|| = ',sol % output % bnorp

     if( sol % output % resip_final > sol % input % solco ) stop 1
     
     print*,'deallocate'
     call sol % deallo()
     deallocate(sol)
     
  end do

contains

  subroutine select_solver(ii,num)
    
    integer(ip), intent(in)  :: ii
    integer(ip), intent(out) :: num

     if(      ii == 1 ) then
        print*,'JACOBI, PREC=DIAG'
        print*,'-----------------'
        num = SOL_SOLVER_JACOBI
     else if( ii == 2 ) then
        print*,'CG, PREC=DIAG'
        print*,'-------------'
        num = SOL_SOLVER_CG
     else if( ii == 3 ) then
        print*,'BICGSTAB, PREC=DIAG'
        print*,'-------------------'
        num = SOL_SOLVER_BICGSTAB
      else if( ii == 4 ) then
        print*,'GMRES, PREC=SOR'
        print*,'---------------'
        num = SOL_SOLVER_GMRES
      else if( ii == 5 ) then
        print*,'SOR'
        print*,'---'
        num = SOL_SOLVER_SOR
      else if( ii == 6 ) then
        print*,'SSOR'
        print*,'----'
        num = SOL_SOLVER_SSOR
     else if( ii == 7 ) then
        print*,'GMRES, PREC=SSOR'
        print*,'----------------'
        num = SOL_SOLVER_GMRES
     else if( ii == 8 ) then
        print*,'GMRES, PREC=DIAG (RIGHT)'
        print*,'------------------------'
        num = SOL_SOLVER_GMRES
     else if( ii == 9 ) then
        print*,'GMRES, PREC=DIAG (LEFT)'
        print*,'-----------------------'
        num = SOL_SOLVER_GMRES
     else if( ii == 10 ) then
        print*,'GMRES, PREC=SOR (RIGHT)'
        print*,'-----------------------'
        num = SOL_SOLVER_GMRES
     else if( ii == 11 ) then
        print*,'GMRES, PREC=SSOR (RIGHT)'
        print*,'------------------------'
        num = SOL_SOLVER_GMRES
     else if( ii == 12 ) then
        print*,'DEFLATED CG, PREC=DIAG'
        print*,'----------------------'
        num = SOL_SOLVER_DEFLATED_CG 
     else if( ii == 13 ) then
        print*,'RICHARDSON, PREC=APPROX_INV (SYMMETRIC)'
        print*,'---------------------------------------'
        num = SOL_SOLVER_RICHARDSON 
     else if( ii == 14 ) then
        print*,'RICHARDSON, PREC=APPROX_INV (UNSYMMETRIC)'
        print*,'-----------------------------------------'
        num = SOL_SOLVER_RICHARDSON
     else if( ii == 15 ) then
        print*,'RICHARDSON, PREC=LINELET'
        print*,'------------------------'
        num = SOL_SOLVER_RICHARDSON 
     else if( ii == 16 ) then
        print*,'MINRES, PREC=DIAG'
        print*,'-----------------'
        num = SOL_SOLVER_MINRES 
      else if( ii == 17 ) then
        print*,'LU FACTORIZATION'
        print*,'----------------'
        num = SOL_SOLVER_DIRECT
      else if( ii == 18 ) then
        print*,'CHOLESKY'
        print*,'--------'
        num = SOL_DIRECT_SOLVER_SKYLINE_ALYA 
     else if( ii == 19 ) then
        print*,'DEFLATED CG, PREC=DIAG'
        print*,'----------------------'
        num = SOL_SOLVER_DEFLATED_CG 
     end if    

  end subroutine select_solver
  
end program unitt_sequential_solver


