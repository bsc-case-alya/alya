!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine gpcabo(&
     pnode,pgaus,pnodb,lboel,shaga,gpcar,gbsha,gbcar)
  !-----------------------------------------------------------------------
  !****f* Domain/gpcabo
  ! NAME 
  !    gpcabo
  ! DESCRIPTION
  !    This routine computes the cartesian derivates on the 
  !    boundary Gauss points 
  ! USES
  ! USED BY
  !    nsi_bouset
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_domain, only     :  ndime,mnode
  implicit none
  integer(ip), intent(in)  :: pnode,pgaus,pnodb 
  integer(ip), intent(in)  :: lboel(pnodb)
  real(rp),    intent(in)  :: shaga(pgaus,pnode)
  real(rp),    intent(in)  :: gpcar(ndime,mnode,pgaus)
  real(rp),    intent(in)  :: gbsha(pnodb)
  real(rp),    intent(out) :: gbcar(ndime,pnode)
  integer(ip)              :: inode,idime,inodb,igaus

  do inode=1,pnode                            
     do idime=1,ndime                          
        gbcar(idime,inode)=0.0_rp                
        do inodb=1,pnodb                         
           do igaus=1,pgaus
              gbcar(idime,inode)=gbcar(idime,inode)&
                   +shaga(igaus,lboel(inodb))&
                   *gbsha(inodb)*gpcar(idime,inode,igaus)
           end do
        end do
     end do
  end do

end subroutine gpcabo
