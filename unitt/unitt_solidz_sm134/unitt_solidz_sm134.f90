!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_solidz_csm_simd

   use def_kintyp_basic, only : ip,rp,lg
   use def_domain,       only : ndime
   use def_solidz,       only : parco_sld

   implicit none

   real(rp)                  :: fsn(3,3)
   real(rp)                  :: F(3,3), F_ij, F_rnd(3,3)
   real(rp)                  :: W_p, W_2p, W_m, W_2m
   real(rp)                  :: P(3,3), P_ref(3,3), P_p(3,3), P_2p(3,3), P_m(3,3), P_2m(3,3), dP(3,3)
   real(rp)                  :: A1(3,3,3,3), A1_ref(3,3,3,3)
   integer(ip)               :: ii, jj, kk, ll
   real(rp)                  :: err_max
   real(rp), parameter       :: per = 1.0e-5_rp 
   real(rp), parameter       :: Ide(3,3) = reshape( (/       &
    &                             1.0_rp, 0.0_rp, 0.0_rp   , &
    &                             0.0_rp, 1.0_rp, 0.0_rp   , &
    &                             0.0_rp, 0.0_rp, 1.0_rp /), &
    &                                           (/ 3 , 3 /) )

   ! Allocate some solidz variables
   allocate(parco_sld(9,1))
   
   ! Define material properties
   parco_sld(1,1) = 10.000_rp
   parco_sld(2,1) =  0.059_rp
   parco_sld(3,1) =  8.023_rp
   parco_sld(4,1) = 18.472_rp
   parco_sld(5,1) = 16.026_rp
   parco_sld(6,1) =  2.481_rp
   parco_sld(7,1) = 11.120_rp
   parco_sld(8,1) =  0.216_rp
   parco_sld(9,1) = 11.436_rp
 
   ! Define deformation gradient tensor
   write(*,*) 'Deformation gradien tensor :'
   F_rnd = 23042021.0_rp
   call random_number(F_rnd)
   F = F_rnd 
   do ii = 1, 3
      do jj = 1, 3
         if( ii == jj )then
            F(ii,jj) = (1.2_rp - 0.8_rp) * F_rnd(ii,jj) + 0.8_rp
         else
            F(ii,jj) = (0.5_rp - 0.0_rp) * F_rnd(ii,jj)
         endif
      enddo
   enddo
   ! - output 
   do ii = 1, 3
      write(*,'(3(3x,E15.7))') F(ii,:)
   enddo

   ! Define the basis
   write(*,*) 'Fiber basis :'
   fsn = get_rotation_matrix_axis_Z(30.0_rp)
   ! - output
   do ii = 1, 3
      write(*,'(3(3x,E15.7))') fsn(ii,:)
   enddo

   write(*,*) 'Numerical stress :'
   ! Compute stress by numerical differentiation
   do ii = 1, 3
      do jj = 1, 3
         ! Get original value
         F_ij = F(ii,jj)
         ! Compute energy after perturbation
         F(ii,jj) = F_ij + per 
         W_p = sm134_get_effectiveStoredEnergy(parco_sld,F,fsn)
         F(ii,jj) = F_ij + 2.0_rp * per 
         W_2p = sm134_get_effectiveStoredEnergy(parco_sld,F,fsn)
         F(ii,jj) = F_ij - per 
         W_m = sm134_get_effectiveStoredEnergy(parco_sld,F,fsn)
         F(ii,jj) = F_ij - 2.0_rp * per 
         W_2m = sm134_get_effectiveStoredEnergy(parco_sld,F,fsn)
         ! Compute stress using the 4th order approximation
         P_ref(ii,jj) = (-W_2p + 8.0_rp * W_p - 8.0_rp * W_m + W_2m) / (12.0_rp * per)
         ! Set original value
         F(ii,jj) = F_ij
      enddo
   enddo
   ! - output
   do ii = 1, 3
      write(*,'(3(3x,E15.7))') P_ref(ii,:)
   enddo

   ! Compute stress 
   write(*,*) 'Analytical stress :'
   P = sm134_get_PK1(parco_sld,F,fsn)
   ! - output
   do ii = 1, 3
      write(*,'(3(3x,E15.7))') P(ii,:) 
   enddo

   ! Make comparison
   err_max = compare(9_ip,P_ref,P)
   write(*,*) 'Error :',err_max 

   
   write(*,*) '1st Elasticirty tensor reference :'
   ! Compute stress by numerical differentiation
   do ii = 1, 3
      do jj = 1, 3
         ! Get original value
         F_ij = F(ii,jj)
         ! Compute energy after perturbation
         F(ii,jj) = F_ij + per
         P_p = sm134_get_PK1(parco_sld,F,fsn)
         F(ii,jj) = F_ij + 2.0_rp * per
         P_2p = sm134_get_PK1(parco_sld,F,fsn)
         F(ii,jj) = F_ij - per
         P_m = sm134_get_PK1(parco_sld,F,fsn)
         F(ii,jj) = F_ij - 2.0_rp * per
         P_2m = sm134_get_PK1(parco_sld,F,fsn)
         ! Compute stress using the 4th order approximation
         dP = (-P_2p + 8.0_rp * P_p - 8.0_rp * P_m + P_2m) / (12.0_rp * per)
         do kk = 1, 3
            do ll = 1, 3
               A1_ref(kk,ll,ii,jj) = dP(kk,ll)  
            enddo
         enddo
         ! Set original value
         F(ii,jj) = F_ij
      enddo
   enddo
   ! - output
   do ii = 1, 3; do jj = 1, 3; do kk = 1, 3
      write(*,'(3(3x,E15.7))') A1_ref(ii,jj,kk,:)
   enddo; enddo; enddo

   ! Compute stress
   write(*,*) '1s Elasticity tensor computed :'
   A1 = sm134_get_A1(parco_sld,F,fsn)
   ! - output
   do ii = 1, 3; do jj = 1, 3; do kk = 1, 3
      write(*,'(3(3x,E15.7))') A1(ii,jj,kk,:)
   enddo; enddo; enddo

   ! Make comparison
   err_max = compare(81_ip,A1_ref,A1)
   ! - output
   write(*,*) 'Error :',err_max
   

   contains


      function compare(n,A,B) result(err_max)
         implicit none
         integer(ip), intent(in) :: n
         real(rp),    intent(in) :: A(*), B(*)
         logical(lg)             :: equal
         real(rp)                :: err_max, num, den
         integer(ip)             :: ii
         real(rp), parameter     :: tol = 1.0e-5_rp
         err_max = tiny(0.0_rp)
         equal = .true.
         num = 0.0_rp
         den = 0.0_rp
         do ii = 1, n
            den = den + B(ii)**2
            num = num + (A(ii) - B(ii))**2
         enddo
         num = sqrt(num)
         if( den == 0.0_rp ) den = 1.0_rp
         den = sqrt(den)
         err_max = num / den
         if( err_max > tol )then
              write(*,*) 'Relative error :',err_max
             stop 1
         endif
      end function compare

      function compare_err(n,A,B) result(err_max)
         implicit none
         integer(ip), intent(in) :: n
         real(rp),    intent(in) :: A(*) !< value assumed as true
         real(rp),    intent(in) :: B(*) !< value to compare
         logical(lg)             :: equal
         real(rp)                :: err, err_max
         integer(ip)             :: ii
         real(rp), parameter     :: tol = 1.0e-4_rp
         err_max = tiny(0.0_rp)
         equal = .true.
         do ii = 1, n
             if( A(ii) < 1.0e-10_rp .or. B(ii) < 1.0e-10_rp )then
                ! Absolute error
                err = abs(A(ii) - B(ii))
             else
                ! Relative error
                err = abs(A(ii) - B(ii)) / abs(A(ii))
             endif
             if( err > tol )then
                equal = .false.
                write(*,'(i4,3(3x,E15.7))') ii,A(ii),B(ii),err
             endif
             err_max = max(err,err_max)
         enddo
         if( .not. equal ) stop 1
      end function compare_err


      function sm134_get_effectiveStoredEnergy(props,F,fsn) result(W)
         implicit none
         real(rp),    intent(in)  :: props(9)
         real(rp),    intent(in)  :: F(3,3)
         real(rp),    intent(in)  :: fsn(3,3)
         real(rp)                 :: W
         integer(ip)              :: ii, jj
         real(rp)                 :: K, a, b, af, bf, as, bs, afs, bfs
         real(rp)                 :: f0(3), s0(3), n0(3)
         real(rp)                 :: J, J_bar
         real(rp)                 :: I1, I1_bar
         real(rp)                 :: I4f, I4f_bar, I4f_bar_p
         real(rp)                 :: I4s, I4s_bar, I4s_bar_p
         real(rp)                 :: I8fs, I8fs_bar
         real(rp)                 :: C(3,3)
         real(rp)                 :: W_vol, W_iso, W_4f, W_4s, W_8fs
         ! Recover material properties
         K = props(1)
         a = props(2)
         b = props(3)
         af = props(4)
         bf = props(5)
         as = props(6)
         bs = props(7)
         afs = props(8)
         bfs = props(9)
         ! Recover basis vector
         f0 = fsn(:,1)
         s0 = fsn(:,2)
         n0 = fsn(:,3)
         ! Compute Jacobian
         J = det(F)
         ! Compute right Cauchy strain tensor and its inverse
         C = matmul(transpose(F),F)
         ! Invariants
         J_bar  = J**(-2.0_rp / 3.0_rp)
         I1 = TRACE(C)
         I1_bar = J_bar * I1 
         I4f = dot_product(f0,matmul(C,f0))
         I4f_bar = J_bar * I4f
         I4f_bar_p = max(0.0_rp,I4f_bar - 1.0_rp)
         I4s = dot_product(s0,matmul(C,s0))
         I4s_bar = J_bar * I4s
         I4s_bar_p = max(0.0_rp,I4s_bar - 1.0_rp)
         I8fs = 0.5_rp * (dot_product(f0,matmul(C,s0)) + dot_product(s0,matmul(C,f0))) 
         I8fs_bar = J_bar * I8fs
         ! Volumetric contribution
         W_vol = 0.5_rp * K * ( J - 1.0_rp )**2 
         W_iso = 0.5_rp * a / b * ( exp(b * (I1_bar - 3.0_rp )) - 1.0_rp )
         W_4f =  0.5_rp * af / bf * ( exp(bf * I4f_bar_p**2) - 1.0_rp )
         W_4s =  0.5_rp * as / bs * ( exp(bs * I4s_bar_p**2) - 1.0_rp )
         W_8fs = 0.5_rp * afs / bfs * ( exp(bfs * I8fs_bar**2) - 1.0_rp )
         ! Add all the contributions
         W = W_vol + W_iso + W_4f + W_4s + W_8fs
      end function sm134_get_effectiveStoredEnergy


      function sm134_get_PK1(props,F,fsn) result(P)
         implicit none
         real(rp),    intent(in)  :: props(9)
         real(rp),    intent(in)  :: F(3,3)
         real(rp),    intent(in)  :: fsn(3,3)
         real(rp)                 :: P(3,3)
         real(rp)                 :: S(3,3)
         S = sm134_get_PK2(props,F,fsn)
         P = matmul(F,S)
      end function sm134_get_PK1


      function sm134_get_PK2(props,F,fsn) result(S)
         implicit none
         real(rp),    intent(in)  :: props(9)
         real(rp),    intent(in)  :: F(3,3)
         real(rp),    intent(in)  :: fsn(3,3)
         real(rp)                 :: S(3,3)
         real(rp)                 :: K, a, b, af, bf, as, bs, afs, bfs
         real(rp)                 :: f0(3), s0(3), n0(3)
         real(rp)                 :: f0xf0(3,3), s0xs0(3,3), n0xn0(3,3), f0xs0(3,3)
         real(rp)                 :: J, J_bar
         real(rp)                 :: I1, I1_bar
         real(rp)                 :: I4f, I4f_bar, I4f_bar_p
         real(rp)                 :: I4s, I4s_bar, I4s_bar_p
         real(rp)                 :: I8fs, I8fs_bar
         real(rp)                 :: C(3,3), C_inv(3,3)
         real(rp)                 :: S_vol(3,3), S_iso(3,3), S_4f(3,3), S_4s(3,3), S_8fs(3,3)
         ! Material properties
         K = props(1)
         a = props(2)
         b = props(3)
         af = props(4)
         bf = props(5)
         as = props(6)
         bs = props(7)
         afs = props(8)
         bfs = props(9)
         ! Recover basis vector
         f0 = fsn(:,1); f0xf0 = OUTX(f0,f0) 
         s0 = fsn(:,2); s0xs0 = OUTX(s0,s0)
         n0 = fsn(:,3); n0xn0 = OUTX(n0,n0)
         f0xs0 = 0.5_rp * ( OUTX(f0,s0) + OUTX(s0,f0) )
         ! Jacobian
         J = det(F)
         ! Compute right Cauchy strain tensor and its inverse
         C = matmul(transpose(F),F) 
         C_inv = INV(C) 
         ! Invariants
         J_bar  = J**(-2.0_rp / 3.0_rp) 
         I1     = TRACE(C)
         I1_bar = J_bar * I1
         I4f = dot_product(f0,matmul(C,f0))
         I4f_bar = J_bar * I4f 
         I4f_bar_p = max(0.0_rp,I4f_bar - 1.0_rp)
         I4s = dot_product(s0,matmul(C,s0))
         I4s_bar = J_bar * I4s
         I4s_bar_p = max(0.0_rp,I4s_bar - 1.0_rp)
         I8fs = 0.5_rp * (dot_product(f0,matmul(C,s0)) + dot_product(s0,matmul(C,f0)))
         I8fs_bar = J_bar * I8fs
         ! Volumetric contribution
         S_vol = K * (J - 1.0_rp) * J * C_inv
         S_iso = J_bar * a * exp(b * (I1_bar - 3.0_rp)) * (Ide - 1.0_rp / 3.0_rp * I1 * C_inv)
         S_4f =  2.0_rp * J_bar * af * I4f_bar_p * exp(bf * I4f_bar_p**2) * (f0xf0 - 1.0_rp / 3.0_rp * I4f * C_inv)
         S_4s =  2.0_rp * J_bar * as * I4s_bar_p * exp(bs * I4s_bar_p**2) * (s0xs0 - 1.0_rp / 3.0_rp * I4s * C_inv)
         S_8fs = 2.0_rp * J_bar * afs * I8fs_bar * exp(bfs * I8fs_bar**2) * (f0xs0 - 1.0_rp / 3.0_rp * I8fs * C_inv)
         ! Add all the contributions
         S = S_vol + S_iso + S_4f + S_4s + S_8fs
      end function sm134_get_PK2


      function sm134_get_A1(props,F,fsn) result(A1)
         implicit none
         real(rp),    intent(in)  :: props(9)
         real(rp),    intent(in)  :: F(3,3)
         real(rp),    intent(in)  :: fsn(3,3)
         integer(ip)              :: ii, jj, kk, ll, mm, nn
         real(rp)                 :: S(3,3) 
         real(rp)                 :: A1(3,3,3,3) !> A1 = dPdF
         real(rp)                 :: A2(3,3,3,3) !> A2 = dSdE
         S = sm134_get_PK2(props,F,fsn)
         A2 = sm134_get_A2(props,F,fsn)
         A1 = 0.0_rp
         do ii=1,3; do jj=1,3; do kk=1,3; do ll=1,3
            do mm=1,3; do nn=1,3
               A1(ii,jj,kk,ll) = A1(ii,jj,kk,ll) + A2(mm,jj,nn,ll) * F(ii,mm) * F(kk,nn)
            enddo; enddo
            A1(ii,jj,kk,ll) = A1(ii,jj,kk,ll) + Ide(ii,kk) * S(jj,ll)
         enddo; enddo; enddo; enddo
      end function sm134_get_A1


      function sm134_get_A2(props,F,fsn) result(dSdE)
         implicit none
         real(rp),    intent(in)  :: props(9)
         real(rp),    intent(in)  :: F(3,3)
         real(rp),    intent(in)  :: fsn(3,3)
         real(rp)                 :: dSdE(3,3,3,3)
         real(rp)                 :: K, a, b, af, bf, as, bs, afs, bfs
         real(rp)                 :: f0(3), s0(3), n0(3)
         real(rp)                 :: f0xf0(3,3), s0xs0(3,3), n0xn0(3,3), f0xs0(3,3)
         real(rp)                 :: J, J_bar, J_43
         real(rp)                 :: I1, I1_bar
         real(rp)                 :: I4f, I4f_bar, I4f_bar_p
         real(rp)                 :: I4s, I4s_bar, I4s_bar_p
         real(rp)                 :: I8fs, I8fs_bar
         real(rp)                 :: C(3,3), C_inv(3,3)
         real(rp)                 :: aux(3,3)
         real(rp)                 :: dSdE_vol(3,3,3,3), dSdE_iso(3,3,3,3), dSdE_4f(3,3,3,3)
         real(rp)                 :: dSdE_4s(3,3,3,3), dSdE_8fs(3,3,3,3)
         real(rp)                 :: CixCi(3,3,3,3), CixCiR(3,3,3,3), CixCiL(3,3,3,3), CiXXCi(3,3,3,3)
         real(rp)                 :: CixI(3,3,3,3), IxCi(3,3,3,3)
         real(rp)                 :: I1xI1(3,3,3,3), I4fxI4f(3,3,3,3), I4sxI4s(3,3,3,3), I8fsxI8fs(3,3,3,3)
         real(rp)                 :: f0xf0xCi(3,3,3,3), Cixf0xf0(3,3,3,3), s0xs0xCi(3,3,3,3), Cixs0xs0(3,3,3,3)
         real(rp)                 :: Cixfs0xfs0(3,3,3,3), fs0xfs0xCi(3,3,3,3) 
         ! Material properties
         K = props(1)
         a = props(2)
         b = props(3)
         af = props(4)
         bf = props(5)
         as = props(6)
         bs = props(7)
         afs = props(8)
         bfs = props(9)
         ! Recover basis vector
         f0 = fsn(:,1); f0xf0 = OUTX(f0,f0) 
         s0 = fsn(:,2); s0xs0 = OUTX(s0,s0)
         n0 = fsn(:,3); n0xn0 = OUTX(n0,n0)
         f0xs0 = 0.5_rp * ( OUTX(f0,s0) + OUTX(s0,f0) )
         ! Jacobian
         J = det(F)
         ! Compute right Cauchy strain tensor and its inverse
         C = matmul(transpose(F),F) 
         C_inv = INV(C) 
         ! Invariants
         J_bar  = J**(-2.0_rp / 3.0_rp) 
         J_43   = J**(-4.0_rp / 3.0_rp)
         I1     = TRACE(C)
         I1_bar = J_bar * I1
         I4f = dot_product(f0,matmul(C,f0))
         I4f_bar = J_bar * I4f 
         I4f_bar_p = max(0.0_rp,I4f_bar - 1.0_rp)
         I4s = dot_product(s0,matmul(C,s0))
         I4s_bar = J_bar * I4s
         I4s_bar_p = max(0.0_rp,I4s_bar - 1.0_rp)
         I8fs = 0.5_rp * (dot_product(f0,matmul(C,s0)) + dot_product(s0,matmul(C,f0)))
         I8fs_bar = J_bar * I8fs
         ! Auxiliar Terms
         CixCi   = OUTM(C_inv,C_inv) 
         CixCiR  = OUTMR(C_inv,C_inv) 
         CixCiL  = OUTML(C_inv,C_inv)
         CiXXCi  = CixCiR + CixCiL
         IxCi    = OUTM(Ide,C_inv)
         CixI    = OUTM(C_inv,Ide)
         aux     = (Ide - 1.0_rp / 3.0_rp * I1 * C_inv)
         I1xI1   = OUTM(aux,aux)
         aux     = (f0xf0 - 1.0_rp / 3.0_rp * I4f * C_inv)
         I4fxI4f = OUTM(aux,aux)
         aux     = (s0xs0 - 1.0_rp / 3.0_rp * I4s * C_inv)
         I4sxI4s = OUTM(aux,aux)
         aux     = (f0xs0 - 1.0_rp / 3.0_rp * I8fs * C_inv)
         I8fsxI8fs = OUTM(aux,aux)
         f0xf0xCi = OUTM(f0xf0,C_inv)
         Cixf0xf0 = OUTM(C_inv,f0xf0)
         s0xs0xCi = OUTM(s0xs0,C_inv)
         Cixs0xs0 = OUTM(C_inv,s0xs0)
         fs0xfs0xCi = OUTM(f0xs0,C_inv)
         Cixfs0xfs0 = OUTM(C_inv,f0xs0)
         ! Volumetric contribution
         !dSdE_vol = Kct*((J**2)*CixCi - 0.5_rp*((J**2) - 1.0_rp)*i_Ci)
         dSdE_vol = K * J * ((2.0_rp * J - 1.0_rp) * CixCi - (J - 1.0_rp) * CiXXCi)
         dSdE_iso = 2.0_rp * J_bar * a * exp(b * (I1_bar - 3.0_rp)) * ( -IxCi / 3.0_rp - &
            CixI / 3.0_rp + I1 * CixCi / 9.0_rp + I1 * CiXXCi / 6.0_rp ) + &
            2.0_rp * J**(-4.0_rp / 3.0_rp) * a * b * exp(b * (I1_bar - 3.0_rp)) * I1xI1
         dSdE_4f = 4.0_rp * J_bar * af * I4f_bar_p * exp(bf * I4f_bar_p**2) * ( - f0xf0xCi / 3.0_rp - &
            Cixf0xf0 / 3.0_rp + I4f * CixCi / 9.0_rp + I4f * CiXXCi / 6.0_rp  ) + &
            4.0_rp * J**(-4.0_rp / 3.0_rp) * af * I4f_bar_p / abs(I4f_bar - 1.0_rp) * exp(bf * I4f_bar_p**2) * I4fxI4f + &
            8.0_rp * J**(-4.0_rp / 3.0_rp) * af * bf * I4f_bar_p**2 * exp(bf * I4f_bar_p**2) * I4fxI4f 
         dSdE_4s = 4.0_rp * J_bar * as * I4s_bar_p * exp(bs * I4s_bar_p**2) * ( - s0xs0xCi / 3.0_rp - &
            Cixs0xs0 / 3.0_rp + I4s * CixCi / 9.0_rp + I4s * CiXXCi / 6.0_rp  ) + &
            4.0_rp * J**(-4.0_rp / 3.0_rp) * as * I4s_bar_p / abs(I4s_bar - 1.0_rp) * exp(bs * I4s_bar_p**2) * I4sxI4s + &
            8.0_rp * J**(-4.0_rp / 3.0_rp) * as * bs * I4s_bar_p**2 * exp(bs * I4s_bar_p**2) * I4sxI4s
         dSdE_8fs = 4.0_rp * J_bar * afs * I8fs_bar *  exp(bfs * I8fs_bar**2) * ( - fs0xfs0xCi / 3.0_rp - &
             Cixfs0xfs0 / 3.0_rp + i8fs * CixCi / 9.0_rp + i8fs * CiXXCi / 6.0_rp ) + &
             4.0_rp * J**(-4.0_rp / 3.0_rp) * afs * exp(bfs * I8fs_bar**2) * I8fsxI8fs + &
             8.0_rp * J**(-4.0_rp / 3.0_rp) * afs * bfs * I8fs_bar**2 * exp(bfs * I8fs_bar**2) * I8fsxI8fs
         ! Add all the contributions
         dSdE = dSdE_vol + dSdE_iso + dSdE_4f + dSdE_4s + dSdE_8fs
      end function sm134_get_A2


      pure function TRACE(M) result(r)
         implicit none
         real(rp),    intent(in) :: M(3,3)
         real(rp)                :: r
         integer(ip)             :: ii
         r = 0.0_rp
         do ii = 1, 3
             r = r + M(ii,ii)
         enddo
     end function TRACE
     

      pure function INV(M) result(Mi)
         implicit none
         real(rp), dimension(3,3), intent(in) :: M
         real(rp), dimension(3,3)             :: Mi
         real(rp)                             :: det
         det = M(1,1)*M(2,2)*M(3,3) + M(1,3)*M(2,1)*M(3,2) + &
               M(3,1)*M(1,2)*M(2,3) - M(3,1)*M(2,2)*M(1,3) - &
               M(3,3)*M(1,2)*M(2,1) - M(1,1)*M(2,3)*M(3,2)
         Mi(1,1) =  (M(2,2)*M(3,3) - M(3,2)*M(2,3))/det
         Mi(1,2) = -(M(1,2)*M(3,3) - M(1,3)*M(3,2))/det
         Mi(1,3) =  (M(1,2)*M(2,3) - M(2,2)*M(1,3))/det
         Mi(2,1) = -(M(2,1)*M(3,3) - M(3,1)*M(2,3))/det
         Mi(2,2) =  (M(1,1)*M(3,3) - M(1,3)*M(3,1))/det
         Mi(2,3) = -(M(1,1)*M(2,3) - M(2,1)*M(1,3))/det
         Mi(3,1) =  (M(2,1)*M(3,2) - M(3,1)*M(2,2))/det
         Mi(3,2) = -(M(1,1)*M(3,2) - M(3,1)*M(1,2))/det
         Mi(3,3) =  (M(1,1)*M(2,2) - M(1,2)*M(2,1))/det
      end function INV


      function det(A) result(detA)
         implicit none
         real(rp), intent(in) :: A(3,3)
         real(rp)             :: detA, t1, t2, t3
         t1 = A(2,2) * A(3,3) - A(3,2) * A(2,3)
         t2 = A(2,1) * A(3,3) - A(3,1) * A(2,3)
         t3 = A(2,1) * A(3,2) - A(3,1) * A(2,2)
         detA = A(1,1) * t1 - A(1,2) * t2 + A(1,3) * t3
      end function det 


      pure function OUTX(a,b) result(R)
         implicit none
         real(rp), intent(in) :: a(3)
         real(rp), intent(in) :: b(3)
         real(rp)             :: R(3,3)
         integer(ip)          :: ii, jj
         do ii = 1, 3
            do jj = 1, 3
               R(ii,jj) = a(ii)*b(jj)
            enddo
         enddo
      end function OUTX


      pure function OUTM(A,B) result(R)
         implicit none
         real(rp), intent(in) :: A(3,3)
         real(rp), intent(in) :: B(3,3)
         real(rp)             :: R(3,3,3,3)
         integer(ip)          :: ii, jj, kk, ll
         do ll = 1, 3
            do kk = 1, 3
               do jj = 1, 3
                  do ii = 1, 3
                     R(ii,jj,kk,ll) = A(ii,jj)*B(kk,ll)
                  enddo
               enddo
            enddo
         enddo
      end function OUTM


      pure function OUTMR(A,B) result(R)
         implicit none
         real(rp), intent(in) :: A(3,3)
         real(rp), intent(in) :: B(3,3)
         real(rp)             :: R(3,3,3,3)
         integer(ip)          :: ii, jj, kk, ll
         do ll = 1, 3
            do kk = 1, 3
               do jj = 1, 3
                  do ii = 1, 3
                     R(ii,jj,kk,ll) = A(ii,kk)*B(jj,ll)
                  enddo
               enddo
            enddo
         enddo
      end function OUTMR


      pure function OUTML(A,B) result(R)
         implicit none
         real(rp), intent(in) :: A(3,3)
         real(rp), intent(in) :: B(3,3)
         real(rp)             :: R(3,3,3,3)
         integer(ip)          :: ii, jj, kk, ll
         do ll = 1, 3
            do kk = 1, 3
               do jj = 1, 3
                  do ii = 1, 3
                     R(ii,jj,kk,ll) = A(ii,ll)*B(jj,kk)
                  enddo
               enddo
            enddo
         enddo
      end function OUTML


      function get_PK1_from_PK2(F,S) result(P)
         implicit none
         real(rp),    intent(in)  :: F(3,3)       
         real(rp),    intent(in)  :: S(3,3)       
         real(rp)                 :: P(3,3)
         P = matmul(F,S) 
      end function get_PK1_from_PK2


      function get_rotation_matrix_axis_Z(theta) result(Q)
         implicit none
         real(rp), intent(in) :: theta   ! Degrees
         real(rp)             :: Q(3,3)
         real(rp)             :: th
         real(rp), parameter  :: coef = (16.0_rp*ATAN(1.0_rp/5.0_rp) - 4.0_rp*ATAN(1.0_rp/239.0_rp))/180.0_rp
         th = theta * coef
         Q  = reshape( [ cos(th), -sin(th),   0.0_rp,   &
                         sin(th),  cos(th),   0.0_rp,   &
                          0.0_rp,   0.0_rp,   1.0_rp ],  &
                       [ 3,3 ] )
      end function get_rotation_matrix_axis_Z

end program unitt_solidz_csm_simd
