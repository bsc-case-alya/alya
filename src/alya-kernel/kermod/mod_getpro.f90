!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> 
!> @author  houzeaux
!> @date    2021-11-11
!> @brief   Get properties
!> @details Get properties during an assembly
!>
!-----------------------------------------------------------------------

module mod_getpro

  use def_kintyp_basic,       only : ip,rp,lg
  use def_ker_proper,         only : typ_valpr_ker
  use def_ker_proper,         only : SCALAR_PROPERTY
  use def_ker_proper,         only : xxx_operations
  use def_ker_proper,         only : xxx_init
  use def_kermod,             only : densi_ker
  use def_kermod,             only : visco_ker
  use def_kermod,             only : poros_ker
  use def_kermod,             only : condu_ker
  use def_kermod,             only : sphea_ker
  use def_kermod,             only : dummy_ker
  use def_kermod,             only : turmu_ker
  use def_kermod,             only : absor_ker
  use def_kermod,             only : scatt_ker
  use def_kermod,             only : mixin_ker
  use def_kermod,             only : anipo_ker
  use def_kermod,             only : walvi_ker
  use mod_ker_proper_generic, only : ker_element_operations_value

#ifndef VECTOR_SIZE
  use def_master,       only : VECTOR_SIZE 
#endif
#ifdef OPENACCHHH
#define DEF_VECT ivect
#else
#define DEF_VECT 1:VECTOR_SIZE
#endif
  !
  ! Private properties
  !
#ifndef PROPER_PRIVATE_OFF
  use mod_ker_proper_turmu
#endif
  
  implicit none
  private

  public :: getpro_val
  
contains

  !-----------------------------------------------------------------------
  !> 
  !> @author  guillaume
  !> @date    2021-11-12
  !> @brief   Value of a property
  !> @details Compute the GP value of a property
  !> 
  !-----------------------------------------------------------------------
  
  subroutine getpro_val(wname,xvalu,pelty,pnode,pgaus,pmate,list_elements,lnods_loc,gpcar)

#if defined OPENACCHHH && defined _OPENACC 
    use openacc
#endif

    character(5),                  intent(in)    :: wname
    real(rp),                      intent(inout) :: xvalu(:,:)
    integer(ip),                   intent(in)    :: pelty
    integer(ip),                   intent(in)    :: pnode
    integer(ip),                   intent(in)    :: pgaus
    integer(ip),                   intent(in)    :: pmate
    integer(ip),                   intent(in)    :: list_elements(:)
    real(rp),            optional, intent(in)    :: gpcar(:,:,:,:)
    integer(ip),                   intent(in)    :: lnods_loc(:,:)

    type(typ_valpr_ker), pointer                 :: prope_ker
    integer(ip)                                  :: kk,ivect,igaus,idime,ilaws
    logical(lg)                                  :: lpalloc

#ifndef PROPER_PRIVATE_OFF
  procedure(xxx_operations), pointer             :: subru_operations
  procedure(xxx_init),       pointer             :: subru_init
#endif

    select case ( wname )
    case ( 'DENSI' )  ; prope_ker => densi_ker
#ifdef OPENACCHHH
       !$acc enter data copyin(densi_ker, densi_ker % value_const)
#endif
    case ( 'VISCO' )  ; prope_ker => visco_ker
#ifdef OPENACCHHH
       !$acc enter data copyin(visco_ker, visco_ker % value_const)
#endif 
    case ( 'MIXIN' )  ; prope_ker => mixin_ker
    case ( 'POROS' )  ; prope_ker => poros_ker
    case ( 'CONDU' )  ; prope_ker => condu_ker
    case ( 'SPHEA' )  ; prope_ker => sphea_ker
    case ( 'DUMMY' )  ; prope_ker => dummy_ker
    case ( 'TURBU' )  ; prope_ker => turmu_ker
    case ( 'ABSOR' )  ; prope_ker => absor_ker
    case ( 'SCATT' )  ; prope_ker => scatt_ker
    case ( 'ANIPO' )  ; prope_ker => anipo_ker
    case ( 'WALLV' )  ; prope_ker => walvi_ker         
    end select

    ilaws = prope_ker % ilaws(pmate)
    
    if( prope_ker % kfl_exist == 0 ) then
       !
       ! Put value to zero
       !
       xvalu = 0.0_rp
       
    else if ( prope_ker % llaws(ilaws) % where == 'CONST' ) then
       !
       ! Property is constant 
       !
       if( prope_ker % kfl_type == SCALAR_PROPERTY ) then
          
#ifdef OPENACCHHH
#ifdef _OPENACC
          lpalloc = acc_is_present(xvalu) 
          if ( .not. lpalloc) then
             !$acc enter data copyin (xvalu(1:VECTOR_SIZE,1:pgaus))
          end if
#endif
          !$acc parallel loop gang vector default(present) 
          do ivect = 1, VECTOR_SIZE
#endif                        
             do igaus = 1,pgaus
                xvalu(DEF_VECT,igaus) = prope_ker % value_const(1,pmate)
             end do
             
#ifdef OPENACCHHH                      
          end do
          !$acc end parallel loop
          
#ifdef _OPENACC
          if ( .not. lpalloc) then
             !$acc update self      (xvalu(1:VECTOR_SIZE,1:pgaus))
             !$acc exit data delete (xvalu(1:VECTOR_SIZE,1:pgaus)) 
          end if
#endif
#endif                 
          
       else
          
          kk = 0
          do igaus = 1,pgaus
             do idime = 1,prope_ker % dim
                kk = kk + 1
                xvalu(DEF_VECT,kk) = prope_ker % value_const(idime,pmate)
             end do
          end do
          
       end if
       
       
    else
       !
       ! Non constant
       !
       select case ( prope_ker % on_the_fly(pmate) )
          
       case ( 0_ip )
          !
          ! Properties copied from memory
          !
          ilaws = prope_ker % ilaws(pmate)
          
          select case ( prope_ker % llaws(ilaws) % where )
             
          case ( 'IELEM' ) 
             !
             ! Property is computed element-wise
             !  
             do ivect = 1,VECTOR_SIZE
                do igaus = 1,pgaus * prope_ker % dim
                   xvalu(ivect,igaus) = prope_ker % value_ielem(list_elements(ivect)) % a(igaus)
                end do
             end do

          end select
          
       case ( 1_ip )
          !
          ! On-the-fly
          !
#ifndef PROPER_PRIVATE_OFF
          
          select case ( prope_ker % wlaws(pmate) )
          case ( 'VRMAN' ) ; subru_operations => ker_proper_turmu_vreman_operations      ; subru_init => ker_proper_turmu_vreman_init
          case ( 'WALE ' ) ; subru_operations => ker_proper_turmu_wale_operations        ; subru_init => ker_proper_turmu_wale_init
          case ( 'ILSA ' ) ; subru_operations => ker_proper_turmu_ilsa_operations        ; subru_init => ker_proper_turmu_ilsa_init
          case ( 'SMAGO' ) ; subru_operations => ker_proper_turmu_smagorinsky_operations ; subru_init => ker_proper_turmu_vreman_init
          end select

          call ker_element_operations_value(&
               int(VECTOR_SIZE,ip),pelty,pnode,pgaus,pmate,list_elements,&
               lnods_loc,gpcar,prope_ker,subru_init,subru_operations,xvalu)
#endif
          
       end select

    end if

    !--------------------------------------------------------------------
    !
    ! Deallocate when copied from memory
    !
    !--------------------------------------------------------------------
    
    if( prope_ker % on_the_fly(pmate) == 0 ) then
       !
       ! This part deallocates the prope_ker from the GPU
       !
       select case ( wname )
       case ( 'DENSI' ) ; prope_ker => densi_ker
#ifdef OPENACCHHH
          !$acc exit data delete(densi_ker, densi_ker % value_const)
#endif 
       case ( 'VISCO' ) ; prope_ker => visco_ker
#ifdef OPENACCHHH
          !$acc exit data delete(visco_ker, visco_ker % value_const)
#endif 
       end select

    end if
    
  end subroutine getpro_val
  
end module mod_getpro
!> @}
