#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



###################################################################
# Copyright 2005 - 2021 Barcelona Supercomputing Center.          #
# Distributed under the ALYA AVAILABLE SOURCE ("ALYA AS") LICENSE #
# for nonprofit scientific purposes only.                         #
# See companion file LICENSE.txt.                                 #
###################################################################


function(init_mkl)
  set(MKL_LIB_DIR "${MKL_DIR}/lib/intel64" CACHE INTERNAL "MKL: library directory" FORCE)
endfunction()

function(link_mkl)
  if (WITH_MKL MATCHES "external")
    foreach(MKL_LIBRARY IN ITEMS dl mkl_scalapack_lp64 mkl_intel_lp64 mkl_sequential mkl_core mkl_blacs_intelmpi_lp64 pthread m)
      find_library(${MKL_LIBRARY}_FULL ${MKL_LIBRARY} PATHS ${MKL_LIB_DIR})
      target_link_libraries(${PROJECT_NAME} ${${MKL_LIBRARY}_FULL})
    endforeach()
  endif()
endfunction()
