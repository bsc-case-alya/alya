reset
set style line  1 lt 1 lc rgb "red"    lw 4
set style line  2 lt 1 lc rgb "blue"   lw 1 
set style line  3 lt 1 lc rgb "black"  lw 1 
set xrange[-0.1:2.1]
set yrange[-1.3:1.1]
set xlabel 'x'
set ylabel 'y'
set xtics ("0"       0, "2" 2)
set ytics ("-1.2" -1.2, "1" 1)
set key bottom left
plot 'results.dat' u 1:2 not w filledcurves lc rgb "grey90", 'partis-inclined.pts.res' u 5:6 t 'Trajectory' w l ls 1
