!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Timste
!> @{
!> @file    Timste.f90
!> @author  Guillaume Houzeaux
!> @author  Adria Quintanas-Corominas
!> @brief
!> @details
!> @}
!-----------------------------------------------------------------------
subroutine Endtim()

  use def_master,       only : ITASK_ENDTIM
  use def_master,       only : iblok, nblok
  use mod_moduls,       only : moduls
  implicit none

  ! Compute the time step for each module
  call Kermod(ITASK_ENDTIM)
  do iblok = 1,nblok
     call moduls(ITASK_ENDTIM)
  end do

end subroutine Endtim