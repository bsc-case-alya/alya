!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine soldod(nbvar,an,bb,xx)
  !-----------------------------------------------------------------------
  !****f* solite/soldod
  ! NAME 
  !    soldod
  ! DESCRIPTION
  !    Take off nodes out of my zone
  !    Take off hole nodes
  ! USES
  ! USED BY
  !    nsi_dodem1
  !***
  !----------------------------------------------------------------------- 
  use def_kintyp, only       :  ip,rp,lg
  use def_master, only       :  INOTMASTER,modul,lzone,current_zone
  use def_domain, only       :  npoin,c_dom,r_dom,c_sym,r_sym,nzone
  use def_domain, only       :  lnoch
  use def_solver, only       :  solve_sol
  use def_elmtyp, only       :  NOHOL
  implicit none
  integer(ip), intent(in)    :: nbvar
  real(rp),    intent(inout) :: an(nbvar,nbvar,*)
  real(rp),    intent(inout) :: bb(nbvar,*)
  real(rp),    intent(inout) :: xx(nbvar,*)
  integer(ip)                :: ipoin,kpoin,izdom,jpoin,kk,ll
  logical(lg), pointer       :: not_in_my_zone(:)
  !
  ! Cancel lines for nodes not in current zone
  !
  if( nzone > 1 .and. INOTMASTER ) then

     allocate( not_in_my_zone(npoin) )
     do ipoin = 1,npoin
        not_in_my_zone(ipoin) = .true.
     end do
     do ipoin = 1,npoin
        not_in_my_zone(ipoin) = .false.
     end do

     do ipoin = 1,npoin
        if( not_in_my_zone(ipoin) ) then
           do kk = 1,nbvar
              bb(kk,ipoin) = 0.0_rp
              xx(kk,ipoin) = 0.0_rp
           end do
           do izdom = r_dom(ipoin),r_dom(ipoin+1) - 1
              jpoin = c_dom(izdom)
              do kk = 1,nbvar
                 do ll = 1,nbvar
                    an(ll,kk,izdom) = 0.0_rp
                 end do
              end do
              if( ipoin == jpoin ) then                 
                 do kk = 1,nbvar
                    an(kk,kk,izdom) = 0.0_rp
                 end do
              end if
           end do
        end if
     end do

     deallocate( not_in_my_zone )

  end if

end subroutine soldod
