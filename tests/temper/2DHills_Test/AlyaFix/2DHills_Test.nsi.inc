$PHYSICAL_PROBLEM
 PROBLEM_DEFINITION
   TEMPORAL_DERIVATIVES      On
   CONVECTIVE_TERM           On
   VISCOUS_TERM              divergence
   GRAVITY        NORM=  0.1983459E-02 GX=  0.0000000E+00 GY=  0.1000000E+01 GZ=  0.0000000E+00
   AXES_ROTATION  NORM=  0.5667026E-04 OX=  0.0000000E+00 OY=  0.0000000E+00 OZ=  0.1000000E+01
   TURBULENCE_COUPLING from_turbul              
   TEMPERATURE_COUPLING      BOUSSI  TR  295.000 G 9.8  GX=0.0  GY=0.0  GZ=-1.0
 END_PROBLEM_DEFINITION
$END_PHYSICAL_PROBLEM
