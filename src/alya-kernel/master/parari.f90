!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine parari(wtask,ntype,ndimi,rvari)
  !------------------------------------------------------------------------
  !****f* Parall/parari
  ! NAME
  !    parari
  ! DESCRIPTION
  !    Works with arrays to deal with parallelization
  ! OUTPUT
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_kintyp,         only : ip,rp
  use def_master,         only : npari,npari,nparc,nparr,parin,parin,pari1
  use def_master,         only : party,parki,pardi,IPARALL,pard1,npasr,paris
  use def_master,         only : NPOIN_TYPE,NFACE_TYPE,npasi,nfacg
  use def_master,         only : kfl_desti_par
  use def_domain,         only : npoin,nbopo,ndime,npoin_2
  use mod_communications, only : PAR_MIN,PAR_MAX,PAR_SUM,PAR_INTERFACE_NODE_EXCHANGE
  implicit none
  character(3), intent(in) :: wtask
  integer(ip),  intent(in) :: ntype,ndimi
  integer(ip),  target     :: rvari(ndimi)
 
  if( IPARALL ) then

     nparr = 0
     nparc = 0

     if( wtask =='SND' ) then  
        !
        ! par_sendin
        !
        npari =  ndimi
        parin => rvari
        call par_sendin()

     else if( wtask =='RCV' ) then 
        !
        ! par_receiv
        !
        npari =  ndimi
        parin => rvari
        call par_receiv() 

     else if( wtask == 'SLX' ) then
        !
        ! par_slexch for vectors(ndimi)
        !
        if( ntype == NPOIN_TYPE ) then
           
           pard1 = ndimi/npoin
           call PAR_INTERFACE_NODE_EXCHANGE(pard1,rvari,'SUM')

        else if( ntype == NFACE_TYPE ) then

           if( nfacg == 0 ) return
           pard1 = ndimi/nfacg
           party = ntype
           if( pard1 == 1 ) then
              parki =  1
              pardi =  1
           else
              parki =  6
              pardi =  2
           end if
           pari1 => rvari
           call par_lgface(2_ip)
        else
           call runend('PARARR: NOT CODED')
        end if

     else
        
        call runend('PARARI: WRONG CASE') 
        
     end if
     
     npari = 0
     nullify(parin)
     nullify(pari1)

  end if
  
end subroutine parari
 
