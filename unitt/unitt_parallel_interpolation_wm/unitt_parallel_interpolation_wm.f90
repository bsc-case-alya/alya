!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_parallel_interpolation

  use def_kintyp
  use def_master
  use def_domain
  use mod_elmgeo
  use def_maths_bin
  use def_maths_tree
  use def_kermod
  use def_search_method
  use mod_parall
  use def_interpolation_method
  use mod_memory_basic
  use mod_communications_global
  use def_mpi  
  implicit none
#include "def_mpi.inc"
  real(rp),    parameter :: eps = 1.0e-10_rp
  real(rp),    pointer   :: bobox(:,:,:)
  real(rp),    pointer   :: subox(:,:,:)
  integer(ip)            :: pelty,ielem,inode,ipoin,ii
  integer(ip)            :: idime,ipass
  MY_MPI_COMM            :: par_comm
  type(maths_bin)        :: bin_seq
  type(maths_bin)        :: bin_par
  type(maths_octree)     :: oct_par
  real(rp),    pointer   :: xx(:,:),vv(:,:),dd(:,:)
  real(rp),    pointer   :: x1(:),v1(:)
  real(rp),    pointer   :: xcoor(:)
  type(interpolation)    :: interp

  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction


  nullify(bobox)
  nullify(subox)
  nullify(xcoor)
  nullify(xx)
  nullify(vv)
  nullify(x1)
  nullify(v1)
  nullify(dd)


  if( INOTMASTER ) then
     if( kfl_paral == 1 ) then
        allocate(xx(2,4))  
        allocate(vv(2,4))
        allocate(v1(4))
        allocate(dd(2,4))
        xx(1,1) = 0.2_rp
        xx(2,1) = 0.2_rp
        xx(1,2) = 0.5_rp
        xx(2,2) = 0.5_rp
        xx(1,3) = 0.8_rp
        xx(2,3) = 0.8_rp
        xx(1,4) = 2.0_rp
        xx(2,4) = 2.0_rp
        vv      = -1.0_rp
        v1      = -1.0_rp
     else if( kfl_paral == 2 ) then
        allocate(xx(2,2))  
        allocate(vv(2,2))
        allocate(v1(2))
        allocate(dd(2,2))
        xx(1,1) = 0.1_rp
        xx(2,1) = 0.1_rp
        xx(1,2) = 0.9_rp
        xx(2,2) = 0.9_rp
        vv      = -1.0_rp
        v1      = -1.0_rp
     end if
     !
     ! Sequential search method
     !
     call meshe(ndivi) % element_bb(bobox)

     do ipass = 1,2

        call bin_seq % init  ()
        call bin_seq % input (boxes=(/10_ip,10_ip,1_ip/)) 
        call bin_seq % fill  (BOBOX=bobox)
        !
        ! Sequential search method
        !
        allocate(subox(2,ndime,0:npart-1))
        subox = 0.0_rp
        do idime = 1,ndime
           subox(1,idime,kfl_paral-1) = minval(bobox(1,idime,:))
           subox(2,idime,kfl_paral-1) = maxval(bobox(2,idime,:))
        end do
        call PAR_SUM(subox,'IN MY CODE WITHOUT MASTER',include_root=.TRUE.)

        if( ipass == 1 ) then
           call oct_par % init  ()
           call oct_par % input (limit=1_ip) 
           call oct_par % fill  (BOBOX=subox)
        else
           call bin_par % init  ()
           call bin_par % input (boxes=(/10_ip,10_ip,1_ip/)) 
           call bin_par % fill  (BOBOX=subox)
        end if 
        !
        ! Interpolation
        !
        call interp % init      ()
        par_comm = PAR_COMM_MY_CODE_WM 
        if( ipass == 1 ) then
           call interp % input     (bin_seq,oct_par,COMM=par_comm)
        else
           call interp % input     (bin_seq,bin_par,COMM=par_comm)
        end if
        call interp % preprocess(xx,meshe(ndivi))
        !
        ! Values
        !
        print*,'test 1'
        if( associated(vv) ) vv = 0.0_rp
        call interp % values(meshe(ndivi) % coord,vv)
        do ii = 1,memory_size(xx,2_ip)
           if( ii /= 4 ) then
              if( abs(xx(1,ii)-vv(1,ii)) > eps ) then
                 print*,'test 1: error 1=',kfl_paral,': ',ii,xx(1,ii),vv(1,ii)
                 stop 1
              end if
              if( abs(xx(2,ii)-vv(2,ii)) > eps ) then
                 print*,'test 1: error 2=',kfl_paral,': ',ii,xx(2,ii),vv(2,ii)
                 stop 1
              end if
           end if
        end do
        !
        ! Value on single point
        !
        print*,'test 2'
        if( associated(vv) ) vv = 0.0_rp
        call interp % values(meshe(ndivi) % coord,vv,POINT=2_ip)
        if( associated(xx) ) then
           ii = 2
           if( abs(xx(1,ii)-vv(1,1)) > eps ) then
              print*,'test 2: error 1=',kfl_paral,': ',xx(1,ii),vv(1,1)
              stop 1
           end if
           if( abs(xx(2,ii)-vv(2,1)) > eps ) then
              print*,'test 2: error 2=',kfl_paral,': ',xx(2,ii),vv(2,1)
              stop 1
           end if
        end if
        !
        ! Values scalars
        !
        print*,'test 3' 
        if( npoin > 0 ) allocate(x1(npoin))
        do ipoin = 1,npoin
           x1(ipoin) = coord(1,ipoin)
        end do
        call interp % values(x1,v1)
        do ii = 1,memory_size(xx,2_ip)
           if( ii /= 4 ) then
              if( abs(xx(1,ii)-v1(ii)) > eps ) then
                 print*,'error 1=',kfl_paral,': ',xx(1,ii),v1(ii)
                 stop 1
              end if
           end if
        end do

        call bin_seq % deallo()
        if( ipass == 1 ) then
           call oct_par % deallo()
        else
           call bin_par % deallo()
        end if
        call interp  % deallo()
        deallocate(subox)

        if( bin_seq % memor(1) /= 0_8 ) then
           print*,'memor seq=',bin_seq % memor(1) ; stop 1
        end if
        if( ipass == 1 ) then
           if( oct_par % memor(1) /= 0_8 ) then
              print*,'memor par=',oct_par % memor(1) ; stop 1
           end if
        else
           if( bin_par % memor(1) /= 0_8 ) then
              print*,'memor par=',bin_par % memor(1) ; stop 1
           end if
        end if
        if( interp  % memor(1) /= 0_8 ) then
           print*,'memor int=',interp  % memor(1) ; stop 1
        end if

     end do

  end if

  call runend('O.K.!')

  !
  ! Derivatives
  !
!!$  allocate(xcoor(npoin))
!!$  do ipoin = 1,npoin
!!$     xcoor(ipoin) = meshe(ndivi)%coord(1,ipoin)
!!$  end do
!!$  print*,'test 3'
!!$  call interp % derivatives(xcoor,dd)
!!$  do ii = 1,memory_size(xx,2_ip)
!!$     if( abs(dd(1,ii)-1.0_rp) > eps ) then
!!$        print*,'error 3=',dd(1,ii),1.0_rp
!!$        stop 1
!!$     end if
!!$  end do
!!$  do ipoin = 1,npoin
!!$     xcoor(ipoin) = meshe(ndivi)%coord(2,ipoin)
!!$  end do  
!!$  call interp % derivatives(xcoor,dd)
!!$  do ii = 1,memory_size(xx,2_ip)
!!$     if( abs(dd(2,ii)-1.0_rp) > eps ) then
!!$        print*,'error 4',dd(2,ii),1.0_rp
!!$        stop 1
!!$     end if
!!$  end do

end program unitt_parallel_interpolation
 
