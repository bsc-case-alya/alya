!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Kinds_and_types
!> @{
!> @file    def_kintyp_petsc.f90
!> @author  Adria Quintanas-Corominas
!> @brief   Bridge to PETSc
!-----------------------------------------------------------------------

module def_kintyp_petsc 


#ifdef PETSC
#include <petsc/finclude/petscksp.h>

    use def_kintyp_basic, only: lg, ip, rp
    use petscksp

    implicit none

    type PETSC_LINEAR_SOLVER
        character(10)           :: name
        logical(lg)             :: I_PARALL
        logical(lg)             :: I_WORKER
        logical(lg)             :: I_WORKER_NOT_EMPTY
        character(6)            :: PETSC_STRATEGY = 'mpiaij'
        character(6)            :: PETSC_MATRIX_TYPE = 'mpiaij'
        character(6)            :: PETSC_VECTOR_TYPE = 'vecmpi'
        PetscInt                :: ndofn
        PetscInt                :: nequa
        PetscInt                :: nequa_own
        PetscInt                :: nunkn
        PetscInt                :: nunkn_own
        PetscInt                :: nzmat
        PetscInt                :: nzmat_own
        PetscInt,     pointer   :: ia(:)
        PetscInt,     pointer   :: ja(:)
        PetscInt,     pointer   :: ia_full(:)
        PetscInt,     pointer   :: ja_full(:)
        PetscInt,     pointer   :: ia_full_cooLex(:)
        PetscInt,     pointer   :: ja_full_cooLex(:)
        PetscInt,     pointer   :: permr(:)
        PetscInt,     pointer   :: nndiag(:)
        PetscInt,     pointer   :: nnoutd(:)
        Mat                     :: A
        Vec                     :: b
        Vec                     :: x
        KSP                     :: ksp
        PC                      :: pc
    end type PETSC_LINEAR_SOLVER

    private

    public :: PETSC_LINEAR_SOLVER

#endif

end module def_kintyp_petsc 
!> @}
