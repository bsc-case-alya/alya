!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine ker_solmem()

  use def_master
  use def_domain
  use def_kermod
  use def_solver
  use mod_maths
  use mod_memory
  use mod_chktyp, only : check_type
  implicit none
  integer(ip) :: ipoin,kfl_value

  !----------------------------------------------------------------------
  !
  ! Memory for solver
  !
  !----------------------------------------------------------------------

  if( kfl_extro /= 0 ) then  
     solve_sol => solve(1:1)  ! Roughness extension
     call soldef(4_ip)
  end if
  if( kfl_walld /= 0 ) then
     solve_sol => solve(2:2)  ! Roughness extension
     call soldef(4_ip)
  end if
  if( kfl_suppo /= 0 ) then
     solve_sol => solve(3:3)  ! Support geometry for mesh multiplication 
     call soldef(4_ip)
  end if
  if( kfl_walln /= 0 ) then
     solve_sol => solve(5:5)  ! Wall normal
     call soldef(4_ip)
  end if
  ! if( kfl_defor /= 0 ) then
  !    solve_sol => solve(4:4)  ! Mesh deformation
  !    call soldef(4_ip)
  ! end if
  !
  ! Solver fixity
  !
  solve(1) % kfl_fixno => kfl_fixno_rough_ker
  solve(2) % kfl_fixno => kfl_fixno_walld_ker
  solve(3) % kfl_fixno => kfl_fixno_suppo_ker
  solve(5) % kfl_fixno => kfl_fixno_walln_ker

  !if( kfl_conma /= 0 ) then
  !   call memory_alloca(memor_dom,'CMASS','mod_mass_matrix',cmass,nzdom)
  !end if
  !if( kfl_conma_weighted /= 0 ) then
  !   call memory_alloca(memor_dom,'CMASS_WEIGHTED','mod_mass_matrix',cmass_weighted,nzdom)
  !end if


end subroutine ker_solmem
