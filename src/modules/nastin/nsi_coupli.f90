!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Nastal
!> @{
!> @file    nsa_coupli.f90
!> @author  Mariano Vazquez
!> @date    16/11/1966
!> @brief   Nastal coupling 
!> @details Nastal coupling
!> @} 
!-----------------------------------------------------------------------
subroutine nsi_coupli(itask)
  use def_master
  use def_domain
  use def_elmtyp
  use def_nastin
  use def_coupli,      only : FIXED_UNKNOWN
  use mod_local_basis, only : local_basis_global_to_local
  use mod_local_basis, only : local_basis_local_to_global
  implicit none
  integer(ip), intent(in) :: itask
  integer(ip)             :: ipoin,idime,idofn,incnt
  integer(ip)             :: ipoin_fluid,ipoin_solid

  select case ( itask )

  case ( ITASK_CONCOU )


  case ( ITASK_BEGSTE )


  case ( ITASK_INIUNK )

     !-------------------------------------------------------------------
     !
     ! INIUNK
     !
     !-------------------------------------------------------------------

  case ( ITASK_BEGITE )

     !-------------------------------------------------------------------
     !
     ! BEGITE
     !
     !-------------------------------------------------------------------
     
  case ( ITASK_ENDITE )

     !-------------------------------------------------------------------
     !
     ! ENDITE
     !
     !-------------------------------------------------------------------
     !
     ! Coupling with Immbou: compute force
     !
     if( coupling('IMMBOU','NASTIN') >= 1 ) then
        call nsi_immbou()
     end if
     !
     ! Coupling with Alefor: compute force for rigid body
     !
     if( ( coupling('ALEFOR','NASTIN') >= 1 ) .and. ( nrbod > 0_ip ) ) then
        call nsi_rbobou()
     end if

  case ( ITASK_MATRIX )

     !-------------------------------------------------------------------
     !
     ! MATRIX: After assembly
     !
     !-------------------------------------------------------------------

     if( coupling('NASTIN','IMMBOU') == 1 ) then
        !
        ! Coupling with Immbou: mass matrix conservation variables
        !                
        call nsi_massma()
        if( INOTMASTER ) then
           !
           ! Coupling with Immbou: impose force FORCF: interpolate b.c.
           !     
           call nsi_embedd(&
                amatr(poauu_nsi),amatr(poaup_nsi),amatr(poapu_nsi),amatr(poapp_nsi),&
                lapla_nsi,rhsid,rhsid(ndbgs_nsi+1),unkno,unkno(ndbgs_nsi+1))           
        end if
     end if
     !
     ! Coupling with Solidz in IBM: add force from solidz 
     !
     call nsi_dimbou()
     !
     ! Coupling with Partis: take off momentum from momentum equations
     !    
     call nsi_partis()

  end select

end subroutine nsi_coupli
