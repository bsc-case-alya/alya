!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Kermod
!> @{
!> @file    nsi_endste.f90
!> @author  Guillaume Houzeaux
!> @date    05/06/2013
!> @brief   End a time step
!> @details End a time step
!> @} 
!-----------------------------------------------------------------------
subroutine ker_endste()
  use def_kintyp,        only : ip 
  use def_master,        only : ITASK_ENDSTE
  use mod_ker_detection, only : ker_detection_boundaries
  use mod_eccoupling,    only : kfl_exmsld_ecc, eccou_manage_variables
  use mod_biofibers,     only : kfl_biofibers, biofibers
  implicit none
  !
  ! Feature detection
  !
  !call ker_detection_boundaries()
  !
  ! Velocity, temperature, concentration and displacement functions
  ! (:,3) <= (:,1)
  !
  call ker_velfun(ITASK_ENDSTE)
  call ker_temfun(ITASK_ENDSTE)
  call ker_confun(ITASK_ENDSTE)
  call ker_disfun(ITASK_ENDSTE)
  call ker_arefun(ITASK_ENDSTE)
  !
  ! Electro-mechanical coupling
  ! 
  if( kfl_exmsld_ecc )then
     call eccou_manage_variables(ITASK_ENDSTE)
  endif
  !
  ! Bio-fibers
  ! 
  if( kfl_biofibers )then
     call biofibers % update_fibers_at_nodes(ITASK_ENDSTE)
  endif

end subroutine ker_endste
