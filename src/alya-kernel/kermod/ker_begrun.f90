!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    ker_begrun.f90
!> @date    14/06/2019
!> @author  Guillaume Houzeaux
!> @brief   Beginning the run... 
!> @details Beginning the run... we can compute matrices!
!> @}
!-----------------------------------------------------------------------

subroutine ker_begrun()

  use def_master,        only : ITASK_BEGRUN
  use mod_ker_noslwa,    only : ker_noslwa
  use mod_wall_exchange, only : ker_waexlo
  
  implicit none
  !
  ! Wall exchange strategy
  ! 
  call ker_waexlo()
  !
  ! NO Slip Wall law - wall law adding extra viscosity 
  !
  call ker_noslwa() 
  !
  ! Velocity and temperature field, etc.
  ! Some field may be needed at this point
  !
  call ker_velfun(ITASK_BEGRUN)
  call ker_temfun(ITASK_BEGRUN)
  call ker_confun(ITASK_BEGRUN)
  call ker_disfun(ITASK_BEGRUN)
  call ker_arefun(ITASK_BEGRUN)

end subroutine ker_begrun
