#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



project(cavtri03_petsc_gmres)

set(MPI_PROCESSES 4)
set(OPENMP_THREADS 0)

set(CASE_DIR "${CMAKE_CURRENT_BINARY_DIR}/${MPI_PROCESSES}_${OPENMP_THREADS}") 

add_custom_target(${PROJECT_NAME} ALL)
add_custom_command(TARGET ${PROJECT_NAME} COMMAND ${CMAKE_COMMAND} -E remove_directory "${CASE_DIR}"
                                          COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR} ${CASE_DIR})

add_test(NAME "${PROJECT_NAME}_${MPI_PROCESSES}_${OPENMP_THREADS}" COMMAND "${MPIEXEC_EXECUTABLE}" "${MPIEXEC_NUMPROC_FLAG}" "${MPI_PROCESSES}" ${MPIEXEC_PREFLAGS} ${CMAKE_BINARY_DIR}/src/alya/alya ${PROJECT_NAME} WORKING_DIRECTORY ${CASE_DIR})
