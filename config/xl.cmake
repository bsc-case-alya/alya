#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



message(STATUS "Compiling with XL")
set(CUSTOM_C_FLAGS "-qflag=i -DNOUNDERSCORE")
set(CUSTOM_Fortran_FLAGS "-qflag=i -qnoescape -qextname=flush:etime -qfree=f90 -qsuffix=cpp=f90 -qxlf2003=polymorphic")
set(CUSTOM_Fortran_FLAGS_RELEASE "")
set(CUSTOM_Fortran_FLAGS_DEBUG "")
set(CUSTOM_Fortran_FLAGS_I4 "")
set(CUSTOM_Fortran_FLAGS_I8 "")
set(CUSTOM_Fortran_FLAGS_O0 "-O0")
set(CUSTOM_Fortran_FLAGS_O1 "-O1")
set(CUSTOM_Fortran_FLAGS_O2 "-O2")
set(CUSTOM_Fortran_FLAGS_O3 "-O3")
set(CUSTOM_Fortran_FLAGS_STD2008 "-DFORTRAN_STD")
