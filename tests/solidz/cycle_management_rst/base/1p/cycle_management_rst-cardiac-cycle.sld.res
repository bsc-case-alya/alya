# --| ALYA Cardiac Cycle Computations
# --| 
# --|  1. Step increment
# --|  2. Step time
# --|  3. Cavity #1
# --|  4. Cavity #1: cycle
# --|  5. Cavity #1: phase
# --|  6. Cavity #1: volume
# --|  7. Cavity #1: cavity pressure
# --|  8. Cavity #1: outflow windkessel pressure
# --|  9. Cavity #2
# --|  10. Cavity #2: cycle
# --|  11. Cavity #2: phase
# --|  12. Cavity #2: volume
# --|  13. Cavity #2: cavity pressure
# --|  14. Cavity #2: outflow windkessel pressure
# --|  15. Overall calcium integral
# --| 
          1   0.25000000E-001   1   1   0   0.44483966E+000   0.50050000E+002   0.15000000E+003   2   2   0   0.47360887E-001   0.30050000E+002   0.80000000E+002   0.00000000E+000
          2   0.50000000E-001   1   1   1   0.45416498E+000   0.10010000E+003   0.15000000E+003   2   2   1   0.47600394E-001   0.60100000E+002   0.80000000E+002   0.00000000E+000
          3   0.75000000E-001   1   1   1   0.42987113E+000   0.10981818E+003   0.14205953E+003   2   2   1   0.44090404E-001   0.61503998E+002   0.75765081E+002   0.00000000E+000
          4   0.10000000E+000   1   1   1   0.40188864E+000   0.12101132E+003   0.13453939E+003   2   2   1   0.39889747E-001   0.63184267E+002   0.71754343E+002   0.00000000E+000
          5   0.12500000E+000   1   1   1   0.43100841E+000   0.12101132E+003   0.12741735E+003   2   2   1   0.44097792E-001   0.63184267E+002   0.67955920E+002   0.00000000E+000
          6   0.15000000E+000   1   1   2   0.45608219E+000   0.12101132E+003   0.12067232E+003   2   2   1   0.47612667E-001   0.63184267E+002   0.64358570E+002   0.00000000E+000
