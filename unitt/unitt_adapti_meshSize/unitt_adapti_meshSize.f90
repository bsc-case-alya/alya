!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_adapti_meshSize ! adapting mesh to analytical mesh size (sequential, only via mod_adapt no mod_amr)
  !
  ! Test boundary mesh
  !
  use def_kintyp,            only : ip,lg,rp
  use def_master
  use def_domain
  use def_kermod
  use mod_AMR,               only : AMR
  use def_kermod
  use mod_memory,            only : memory_alloca, memory_deallo
  use def_adapt,             only : memor_adapt 
  use mod_out_paraview,      only : out_paraview_inp
  use def_kintyp_mesh_basic
  use mod_strings,           only : integer_to_string
  use mod_metricComputation, only : compute_sizeField_from_sol_viaHessian, compute_sizeField_mesh
  use mod_quality,           only : compute_mesh_quality_shape
  use mod_messages,          only : messages_live

  implicit none

  type(mesh_type_basic)  :: mesh_out
  character(len=100)     :: mesh_name_out
  integer(ip)            :: numTargetNodes
  integer(ip)            :: numLoopsAdaptivity 
  integer(ip)            :: iloop
  integer(ip)            :: test_function, inode
  logical(lg)            :: is_passed_mesh_size
  real(rp)               :: r, fun_hmin, fun_hmax
  real(rp)               :: b, eps, a
  real(rp)               :: amplitude 
  logical(lg), parameter :: do_export_paraview = .false.
  logical(lg), parameter :: set_AMR_off = .false. ! not run AMR
  real(rp),    pointer   :: u(:)
  real(rp),    pointer   :: hh(:),q(:)

  ! TODOs:
  ! - test convergence => do in some other test
  ! - automatically set max mesh size -> DONE (tested in unitt_adapti_meshSize)
  ! - allow to repair according to a input size field (to perform mehs repair and not adaptation) -> DONE (tested in unitt_adapti_meshSize)

  numLoopsAdaptivity = 1_ip!5_ip

  is_passed_mesh_size = .true.

  nullify(hh,q,u)

  !numTargetNodes = 1500_ip ! handwritten from input [strictly for visualization]

  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction

  do iloop = 1_ip,numLoopsAdaptivity

     call compute_sizeField_mesh(hh,meshe(ndivi))
     !hh=0.1_rp

     !       print*,kfl_paral," ",hh(1:4)
     !       print*,size(hh),"   ",npoin
     !       !hh = hh/5.0_rp
     !       !hh = 0.01_rp + x/10.0_rp
     !
     !       hh = hh - abs(sin(3.14_rp*x)*sin(3.14_rp*y))*hh/1.2_rp
     !
     !       print*,kfl_paral," ",hh(1:4)

     do inode=1,meshe(ndivi) %npoin
        !         r = sqrt( (x(inode)-0.5_rp)**2 + (y(inode)-0.5_rp)**2 )
        !         eps = 0.2_rp
        !         amplitude = 0.05_rp
        !         fun_hmin = 0.01_rp
        !         fun_hmax = 0.2_rp
        !         if(       r > eps + amplitude) then
        !           hh(inode) = fun_hmax
        !         else if(  r < eps - amplitude) then
        !           hh(inode) = fun_hmax
        !         else
        !           hh(inode) = fun_hmin
        !         end if

        !         r = sqrt( (x(inode)-0.25_rp)**2 + (y(inode)-0.75_rp)**2 )
        !         hh(inode) = 0.01_rp + r**2_ip

        if(meshe(ndivi) % coord(1,inode)<0.5_rp) then
           hh(inode) = hh(inode)/5.0_rp
        end if
     end do

     if(do_export_paraview) then
        call mesh_out      % init      ('MESH_NEW')
        call mesh_out      % copy      (meshe(ndivi))
        mesh_name_out = './unitt/unitt_adapti_meshSize/mesh_h_proc'//integer_to_string(kfl_paral)//'_'//integer_to_string(2*iloop-1)
        if(npoin>0_ip) call out_paraview_inp(mesh_out,filename=TRIM(mesh_name_out),nodeField=hh)
        call mesh_out      % deallo()
     end if

     if(.not.set_AMR_off) then
        !call AMR(u,maxMeshSize,is_passed_mesh_size)                                      ! Adaptive mesh refinement
        call AMR(target_solut_input=hh,is_solut_mesh_size_input=is_passed_mesh_size)
     end if

     call memory_deallo(memor_adapt,'hh','unitt_adapti_meshSize',hh)
     call compute_sizeField_mesh(hh,meshe(ndivi))
     do inode=1,meshe(ndivi) %npoin
        !         r = sqrt( (x(inode)-0.5_rp)**2 + (y(inode)-0.5_rp)**2 )
        !         if(       r > eps + amplitude) then
        !           hh(inode) = fun_hmax
        !         else if(  r < eps - amplitude) then
        !           hh(inode) = fun_hmax
        !         else
        !           hh(inode) = fun_hmin
        !         end if
        !
        !         r = sqrt( (x(inode)-0.25_rp)**2 + (y(inode)-0.75_rp)**2 )
        !         hh(inode) = 0.01_rp + r**2_ip

        if(meshe(ndivi) % coord(1,inode)<0.5_rp) then
           hh(inode) = hh(inode)/5.0_rp
        end if
     end do

     if(do_export_paraview) then
        call mesh_out      % init      ('MESH_NEW')
        call mesh_out      % copy      (meshe(ndivi))
        mesh_name_out = './unitt/unitt_adapti_meshSize/mesh_h_proc'//integer_to_string(kfl_paral)//'_'//integer_to_string(2*iloop)
        if(npoin>0_ip) call out_paraview_inp(mesh_out,filename=TRIM(mesh_name_out),nodeField=hh)
        call mesh_out      % deallo()
     end if

     call memory_deallo(memor_adapt,'hh','unitt_adapti_meshSize',hh)

  end do

  call runend('O.K.!')                                               ! Finish Alya

end program unitt_adapti_meshSize

!call meshe%output(filename='./unitt/unitt_adapti_meshSize/mesh_0')
!call mesh_out % output(FILENAME='./unitt/unitt_adapti_meshSize/mesh_'//integer_to_string(kfl_paral)//'_0')


