!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_exm_ohara
   !test drug interactions

   use def_kintyp_basic, only : ip,rp
   use def_master
   use mod_exm_cellmodel
   use mod_exm_oharaprecalc
   use mod_exm_ohararudy
   use mod_eccoupling, only : EXMSLD_CELL_OHARA, EXMSLD_CELL_OHARA_INAPA
   use def_exmedi
   

   implicit none
   real(rp) :: ttparmate(3,17)
   type(ohara_constants)      :: params
   real(rp), parameter :: tol = 5.0e-14_rp

   real(rp)  :: viclo_exm(26), viclo_exm1(26)
   real(rp)  :: vcolo_exm(11,2), vaulo_exm(29,2), vcolo_exm1(11,2), vaulo_exm1(29,2)
   real(rp)  :: voltage_local, voltage_local1, stim

   integer(ip), parameter :: models(2)    = (/EXMSLD_CELL_OHARA, EXMSLD_CELL_OHARA_INAPA/)
   integer(ip), parameter :: celltypes(3) = (/EXM_CELLTYPE_ENDO, EXM_CELLTYPE_MID, EXM_CELLTYPE_EPI/)
   integer(ip)            :: i,j,k
   type(ohara_precalc)    :: precalc
   type(ohara_outputs)    :: oo

   real(rp) :: voltages(4)



   print *,'Testing OHARA original formulation exponent calculation'
   call test_ohara_pure_inftau_calculation()
   print *,'Testing OHARA INa Passini formulation exponent calculation'
   call test_ohara_passini_inftau_calculation()

   print *,'Testing ohara one time step calculations, no drugs'
   ttparmate(1,:) = (/1.0_rp, 1.87_rp, 1.698_rp, 1.013_rp, 1.0_rp, 2.661_rp, 1.0_rp, 1.0_rp, 1.007_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 0.0_rp, 1.0_rp, 1.0_rp, 1.0_rp /)     
   ttparmate(2,:) = (/1.0_rp, 1.87_rp, 1.698_rp, 1.013_rp, 1.0_rp, 2.661_rp, 1.0_rp, 1.0_rp, 1.007_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 0.0_rp, 1.0_rp, 1.0_rp, 1.0_rp /)
   ttparmate(3,:) = (/0.6_rp, 1.04_rp, 0.98_rp, 1.09_rp, 1.0_rp, 2.661_rp, 1.0_rp, 1.0_rp, 0.88_rp, 0.94_rp, 1.07_rp, 1.42_rp, 1.0_rp, 0.0_rp, 1.0_rp, 1.0_rp, 1.0_rp /) 
   voltages(:)    = (/-80.0_rp, -40.0_rp, -10.0_rp, 30.0_rp/)

   nconc_exm = 11_ip
   nauxi_exm = 29_ip 

   do i = 1_ip, size(models)
      do j = 1_ip, size(celltypes)
            do k = 1_ip, size(voltages)
                print *,'Testing model ',i,' cell ',j, 'voltage ',voltages(k)
         call exm_ohara_constants_initialize( params, ttparmate, -1.0_rp, celltypes(j), models(i) )
         oo % qnet  = 0.0_rp
         oo % Inet  = 0.0_rp
         oo % xioni = 0.0_rp
         stim = -80.0_rp     ! Value of I_stim current [pA/pF]

         !no land
         call set_ohara_initial_values(celltypes(j), viclo_exm, vcolo_exm, vaulo_exm, voltage_local)
         voltage_local = voltages(k)
         call exm_ohara_execute_timestep_jazmin( params,  ttparmate, models(i), .FALSE., celltypes(j), &
                           vcolo_exm(:, 2), vcolo_exm(:, 1), vaulo_exm(:, 2), vaulo_exm(:, 1), viclo_exm, voltage_local, oo, stim ) 

         call exm_ohara_constants_initialize( params, ttparmate, -1.0_rp, celltypes(j), models(i) )
         oo % qnet  = 0.0_rp
         oo % Inet  = 0.0_rp
         oo % xioni = 0.0_rp
         call precalc%init( models(i), .FALSE. )
         call set_ohara_initial_values(celltypes(j), viclo_exm1, vcolo_exm1, vaulo_exm1, voltage_local1)
         voltage_local1 = voltages(k)
         call exm_ohara_execute_timestep( params,  ttparmate, models(i), .FALSE., celltypes(j), &
            vcolo_exm1(:, 2), vcolo_exm1(:, 1), vaulo_exm1(:, 2), vaulo_exm1(:, 1), viclo_exm1, voltage_local1, precalc, oo ) 

         voltage_local1 = voltage_local1 + params % dtimon * ( -(oo % xioni + stim) ) ! *params % farad/params % acap

         if (abs(voltage_local-voltage_local1)>tol) then
            print *,'Voltage is different: ',voltage_local ," vs ", voltage_local1, " init voltage = ",voltages(k)
            stop 1
         end if

            
         ! in the accelerated part the divisions are replafced by multiplication by 1/x, so there is a change in accuracy
         if( any( abs(vcolo_exm1(:,1) - vcolo_exm(:,1)) > tol ) ) then
            print *,'Noland, vcolo is different, initial voltage = ',voltages(k)
            stop 1
         end if
         if( any( abs(vaulo_exm1(:,1) - vaulo_exm(:,1)) > tol ) ) then
            print *,'Noland, vaulo is different, initial voltage = ',voltages(k)
            stop 1
         end if         
         if( any( abs(viclo_exm1 - viclo_exm) > tol ) ) then
            print *,'Noland, viclo is different, initial voltage = ',voltages(k)
            stop 1
         end if
            end do
      end do
   end do


   contains

real(rp) function error(v_measured, v_correct)
   real(rp), intent(in) :: v_measured, v_correct

   error = abs(( v_measured - v_correct )/v_correct )
end function 

subroutine set_ohara_initial_values(celltype, viclo_exm, vcolo_exm, vaulo_exm, voltage_local)
   implicit none
   integer(ip), intent(in) :: celltype
   real(rp), dimension(:), intent(inout) :: viclo_exm
   real(rp), dimension(:,:), intent(inout) :: vcolo_exm, vaulo_exm
   real(rp), intent(inout) :: voltage_local

   select case (celltype)
   case (EXM_CELLTYPE_ENDO)
      !endocardium
      voltage_local = -87.5_rp     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
      !voltage_local = -87.99_rp
      viclo_exm(1:26) = 0.0_rp
      vcolo_exm(1, 1:2) = 0.0001_rp  !8.03149767106260e-05       !cai=6)
      vcolo_exm(2, 1:2) = 7.0_rp !8.17212071948942       !nass=3);
      vcolo_exm(3, 1:2) = 145.0_rp !143.675184333376       !ki=4);
      vcolo_exm(4, 1:2) = 145.0_rp  !143.675147146842       !kss=5);
      vcolo_exm(5, 1:2) = 7.0_rp  !8.17202753912090      ! nai=2
      vcolo_exm(6, 1:2) = 0.0001_rp  !7.96912949697674e-05       !cass=7);
      vcolo_exm(7, 1:2) = 1.2_rp  !2.14811455007091       !cansr=8);
      vcolo_exm(8, 1:2) = 1.2_rp  !2.03335899236798      !cajsr=9);
      vcolo_exm(9, 1:2) = 0.0_rp !4.74946280300893e-07       !Jrelnp=39);
      vcolo_exm(10, 1:2) = 0.0_rp !5.93539009244893e-07      !Jrelp=40);
      vcolo_exm(11, 1:2) = 0.0_rp !0.0228529042639590      !CaMKt=41);
      vaulo_exm(1, 1:2) = 0.0_rp !0.00739719746920272       !m=10);
      vaulo_exm(2, 1:2) = 1.0_rp  !0.695621622011335       !hf=11);
      vaulo_exm(3, 1:2) = 1.0_rp  !0.695601842634086       !hs=12);
      vaulo_exm(4, 1:2) = 1.0_rp  !0.695486248719023       !j=13);
      vaulo_exm(5, 1:2) = 1.0_rp  !0.452023628358454       !hsp=14);
      vaulo_exm(6, 1:2) = 1.0_rp  !0.695403157533235       !jp=15);
      vaulo_exm(7, 1:2) = 0.0_rp !0.000190839777466418       !mL=16);
      vaulo_exm(8, 1:2) = 1.0_rp  !0.493606704642336       !hL=17);
      vaulo_exm(9, 1:2) = 1.0_rp  !0.264304293390731       !hLp=18);
      vaulo_exm(10, 1:2) = 0.0_rp !0.00100594231451985      !a=19);
      vaulo_exm(11, 1:2) = 1.0_rp  !0.999548606668578      !iF=20);
      vaulo_exm(12, 1:2) = 1.0_rp  !0.999488774162635      !iS=21);
      vaulo_exm(13, 1:2) = 0.0_rp !0.000512555980943569      !ap=22);
      vaulo_exm(14, 1:2) = 1.0_rp  !0.999548607287668      !iFp=23);
      vaulo_exm(15, 1:2) = 1.0_rp  !0.999488774162635      !iSp=24);
      vaulo_exm(16, 1:2) = 0.0_rp !2.38076098345898e-09      !d=25);
      vaulo_exm(17, 1:2) = 1.0_rp  !0.999999990696210      !ff=26);
      vaulo_exm(18, 1:2) = 1.0_rp  !0.904906458666787      !fs=27);
      vaulo_exm(19, 1:2) = 1.0_rp  !0.999999990696060      !fcaf=28);
      vaulo_exm(20, 1:2) = 1.0_rp  !0.999581201974281      !fcas=29);
      vaulo_exm(21, 1:2) = 1.0_rp  !0.999903346883777      !jca=30);
      vaulo_exm(22, 1:2) = 0.0_rp !0.00215555277945401      !nca=31);
      vaulo_exm(23, 1:2) = 1.0_rp  !0.999999990680285      !ffp=32);
      vaulo_exm(24, 1:2) = 1.0_rp  !0.999999990692529      !fcafp=33);
      vaulo_exm(25, 1:2) = 0.0_rp !8.64222375034682e-06      !xrf=34);
      vaulo_exm(26, 1:2) = 0.0_rp !0.487585264457487      !xrs=35);
      vaulo_exm(27, 1:2) = 0.0_rp !0.276203479404767      !xs1=36);
      vaulo_exm(28, 1:2) = 0.0_rp !0.000194412216700766      !xs2=37);
      vaulo_exm(29, 1:2) = 1.0_rp  !0.996778581263402      !xk1=38);
   case (EXM_CELLTYPE_EPI)
      !epicardial
      voltage_local = -87.5_rp     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
      !voltage_local = -87.99_rp
      viclo_exm(1:26) = 0.0_rp
      vcolo_exm(1, 1:2) = 0.0001_rp  !8.03149767106260e-05       !cai=6)
      vcolo_exm(2, 1:2) = 7.0_rp !8.17212071948942       !nass=3);
      vcolo_exm(3, 1:2) = 145.0_rp !143.675184333376       !ki=4);
      vcolo_exm(4, 1:2) = 145.0_rp  !143.675147146842       !kss=5);
      vcolo_exm(5, 1:2) = 7.0_rp  !8.17202753912090      ! nai=2
      vcolo_exm(6, 1:2) = 0.0001_rp  !7.96912949697674e-05       !cass=7);
      vcolo_exm(7, 1:2) = 1.2_rp  !2.14811455007091       !cansr=8);
      vcolo_exm(8, 1:2) = 1.2_rp  !2.03335899236798      !cajsr=9);
      vcolo_exm(9, 1:2) = 0.0_rp !4.74946280300893e-07       !Jrelnp=39);
      vcolo_exm(10, 1:2) = 0.0_rp !5.93539009244893e-07      !Jrelp=40);
      vcolo_exm(11, 1:2) = 0.0_rp !0.0228529042639590      !CaMKt=41);
      vaulo_exm(1, 1:2) = 0.0_rp !0.00739719746920272       !m=10);
      vaulo_exm(2, 1:2) = 1.0_rp  !0.695621622011335       !hf=11);
      vaulo_exm(3, 1:2) = 1.0_rp  !0.695601842634086       !hs=12);
      vaulo_exm(4, 1:2) = 1.0_rp  !0.695486248719023       !j=13);
      vaulo_exm(5, 1:2) = 1.0_rp  !0.452023628358454       !hsp=14);
      vaulo_exm(6, 1:2) = 1.0_rp  !0.695403157533235       !jp=15);
      vaulo_exm(7, 1:2) = 0.0_rp !0.000190839777466418       !mL=16);
      vaulo_exm(8, 1:2) = 1.0_rp  !0.493606704642336       !hL=17);
      vaulo_exm(9, 1:2) = 1.0_rp  !0.264304293390731       !hLp=18);
      vaulo_exm(10, 1:2) = 0.0_rp !0.00100594231451985      !a=19);
      vaulo_exm(11, 1:2) = 1.0_rp  !0.999548606668578      !iF=20);
      vaulo_exm(12, 1:2) = 1.0_rp  !0.999488774162635      !iS=21);
      vaulo_exm(13, 1:2) = 0.0_rp !0.000512555980943569      !ap=22);
      vaulo_exm(14, 1:2) = 1.0_rp  !0.999548607287668      !iFp=23);
      vaulo_exm(15, 1:2) = 1.0_rp  !0.999488774162635      !iSp=24);
      vaulo_exm(16, 1:2) = 0.0_rp !2.38076098345898e-09      !d=25);
      vaulo_exm(17, 1:2) = 1.0_rp  !0.999999990696210      !ff=26);
      vaulo_exm(18, 1:2) = 1.0_rp  !0.904906458666787      !fs=27);
      vaulo_exm(19, 1:2) = 1.0_rp  !0.999999990696060      !fcaf=28);
      vaulo_exm(20, 1:2) = 1.0_rp  !0.999581201974281      !fcas=29);
      vaulo_exm(21, 1:2) = 1.0_rp  !0.999903346883777      !jca=30);
      vaulo_exm(22, 1:2) = 0.0_rp !0.00215555277945401      !nca=31);
      vaulo_exm(23, 1:2) = 1.0_rp  !0.999999990680285      !ffp=32);
      vaulo_exm(24, 1:2) = 1.0_rp  !0.999999990692529      !fcafp=33);
      vaulo_exm(25, 1:2) = 0.0_rp !8.64222375034682e-06      !xrf=34);
      vaulo_exm(26, 1:2) = 0.0_rp !0.487585264457487      !xrs=35);
      vaulo_exm(27, 1:2) = 0.0_rp !0.276203479404767      !xs1=36);
      vaulo_exm(28, 1:2) = 0.0_rp !0.000194412216700766      !xs2=37);
      vaulo_exm(29, 1:2) = 1.0_rp  !0.996778581263402      !xk1=38);
   case (EXM_CELLTYPE_MID)
      !MIDmyocardial
      voltage_local = -87.5_rp     ! INTRA (BI-DOMAIN) OR SINGLE (MONO) POTENTIAL
      !voltage_local = -87.99_rp
      viclo_exm(1:26) = 0.0_rp
      vcolo_exm(1, 1:2) = 0.0001_rp  !8.03149767106260e-05       !cai=6)
      vcolo_exm(2, 1:2) = 7.0_rp !8.17212071948942       !nass=3);
      vcolo_exm(3, 1:2) = 145.0_rp !143.675184333376       !ki=4);
      vcolo_exm(4, 1:2) = 145.0_rp  !143.675147146842       !kss=5);
      vcolo_exm(5, 1:2) = 7.0_rp  !8.17202753912090      ! nai=2
      vcolo_exm(6, 1:2) = 0.0001_rp  !7.96912949697674e-05       !cass=7);
      vcolo_exm(7, 1:2) = 1.2_rp  !2.14811455007091       !cansr=8);
      vcolo_exm(8, 1:2) = 1.2_rp  !2.03335899236798      !cajsr=9);
      vcolo_exm(9, 1:2) = 0.0_rp !4.74946280300893e-07       !Jrelnp=39);
      vcolo_exm(10, 1:2) = 0.0_rp !5.93539009244893e-07      !Jrelp=40);
      vcolo_exm(11, 1:2) = 0.0_rp !0.0228529042639590      !CaMKt=41);
      vaulo_exm(1, 1:2) = 0.0_rp !0.00739719746920272       !m=10);
      vaulo_exm(2, 1:2) = 1.0_rp  !0.695621622011335       !hf=11);
      vaulo_exm(3, 1:2) = 1.0_rp  !0.695601842634086       !hs=12);
      vaulo_exm(4, 1:2) = 1.0_rp  !0.695486248719023       !j=13);
      vaulo_exm(5, 1:2) = 1.0_rp  !0.452023628358454       !hsp=14);
      vaulo_exm(6, 1:2) = 1.0_rp  !0.695403157533235       !jp=15);
      vaulo_exm(7, 1:2) = 0.0_rp !0.000190839777466418       !mL=16);
      vaulo_exm(8, 1:2) = 1.0_rp  !0.493606704642336       !hL=17);
      vaulo_exm(9, 1:2) = 1.0_rp  !0.264304293390731       !hLp=18);
      vaulo_exm(10, 1:2) = 0.0_rp !0.00100594231451985      !a=19);
      vaulo_exm(11, 1:2) = 1.0_rp  !0.999548606668578      !iF=20);
      vaulo_exm(12, 1:2) = 1.0_rp  !0.999488774162635      !iS=21);
      vaulo_exm(13, 1:2) = 0.0_rp !0.000512555980943569      !ap=22);
      vaulo_exm(14, 1:2) = 1.0_rp  !0.999548607287668      !iFp=23);
      vaulo_exm(15, 1:2) = 1.0_rp  !0.999488774162635      !iSp=24);
      vaulo_exm(16, 1:2) = 0.0_rp !2.38076098345898e-09      !d=25);
      vaulo_exm(17, 1:2) = 1.0_rp  !0.999999990696210      !ff=26);
      vaulo_exm(18, 1:2) = 1.0_rp  !0.904906458666787      !fs=27);
      vaulo_exm(19, 1:2) = 1.0_rp  !0.999999990696060      !fcaf=28);
      vaulo_exm(20, 1:2) = 1.0_rp  !0.999581201974281      !fcas=29);
      vaulo_exm(21, 1:2) = 1.0_rp  !0.999903346883777      !jca=30);
      vaulo_exm(22, 1:2) = 0.0_rp !0.00215555277945401      !nca=31);
      vaulo_exm(23, 1:2) = 1.0_rp  !0.999999990680285      !ffp=32);
      vaulo_exm(24, 1:2) = 1.0_rp  !0.999999990692529      !fcafp=33);
      vaulo_exm(25, 1:2) = 0.0_rp !8.64222375034682e-06      !xrf=34);
      vaulo_exm(26, 1:2) = 0.0_rp !0.487585264457487      !xrs=35);
      vaulo_exm(27, 1:2) = 0.0_rp !0.276203479404767      !xs1=36);
      vaulo_exm(28, 1:2) = 0.0_rp !0.000194412216700766      !xs2=37);
      vaulo_exm(29, 1:2) = 1.0_rp  !0.996778581263402      !xk1=38);
   case default
      call runend("EXM_INIT_VOLATGES: Undefined cell type.")
   end select

end subroutine set_ohara_initial_values


!-------------------------------------------------------------------
!
!
! Testing mod_exm_oharaequations
!
!

   subroutine test_ohara_passini_inftau_calculation()
      implicit none
   
      real(rp), parameter :: infs_v80(EXM_OHR_INF_EQUATIONS,2) = reshape( (/&
      real(OHR_M_INF             ,rp),   1.6369406338341302E-002_rp ,&    
      real(OHR_H_INF             ,rp),  0.55999889106497114_rp      ,&           
      real(OHR_H_CAMK_INF        ,rp),  0.31959644177695873_rp      ,&           
      real(OHR_M_L_INF           ,rp),   8.6029819612767531E-004_rp ,&          
      real(OHR_H_L_INF           ,rp),  0.26575015957755954_rp      ,&                 
      real(OHR_H_L_CAMK_INF      ,rp),  0.13654539662679432_rp      ,&           
      real(OHR_A_INF             ,rp),   1.7165479337177718E-003_rp ,&         
      real(OHR_INV_TAU_A_1       ,rp),   2.8054427282450653E-002_rp ,&          
      real(OHR_INV_TAU_A_2       ,rp),   1.1763656535736702_rp      ,&                 
      real(OHR_I_INF             ,rp),  0.99819273301373856_rp      ,&           
      real(OHR_A_I_FAST          ,rp),  0.87454964302408766_rp      ,&         
      real(OHR_A_CAMK_INF        ,rp),   8.7493483578003932E-004_rp ,&          
      real(OHR_DELTA_CAMK_RECOVER,rp),  0.68877033439907276_rp      ,&                 
      real(OHR_D_INF             ,rp),   1.5520757956663981E-008_rp ,&           
      real(OHR_F_INF             ,rp),  0.99999992049156161_rp      ,&         
      real(OHR_A_F_CA_FAST       ,rp),  0.89992596325440810_rp      ,&          
      real(OHR_X_R_INF           ,rp),   2.6042571473496338E-005_rp ,&                 
      real(OHR_A_XR_FAST         ,rp),  0.65909222935902878_rp      ,&           
      real(OHR_R_KR_1            ,rp),  0.58257020646231472_rp      ,&         
      real(OHR_R_KR_2            ,rp),  0.95257412682243336_rp      ,&        
      real(OHR_X_S1_INF          ,rp),   4.7209440212676900E-004_rp ,&                 
      real(OHR_X_KB              ,rp),   5.7569078163134191E-003_rp ,&           
      real(OHR_DELTA_EPI         ,rp),  0.16324277592101177_rp       &
      /), (/ EXM_OHR_INF_EQUATIONS, 2_ip /), ORDER= (/ 2_ip,1_ip /) )   
   

      real(rp), parameter :: taus_v80(EXM_OHR_TAU_EQUATIONS,2) = reshape( (/&
      real(OHR_TAU_M             ,rp) ,    7.0738559308230115E-002_rp,&      
      real(OHR_TAU_H_FAST        ,rp) ,    16.177065880823434_rp     ,&      
      real(OHR_TAU_H_SLOW        ,rp) ,    5.5680060426181592_rp     ,&             
      real(OHR_TAU_J             ,rp) ,    143.86042907105150_rp     ,&        
      real(OHR_TAU_I_FAST        ,rp) ,    7.5459369035467851_rp     ,&        
      real(OHR_TAU_I_SLOW        ,rp) ,    956.75567340542943_rp     ,&      
      real(OHR_DELTA_CAMK_DEVELOP,rp) ,    1.3540000000000001_rp     ,&             
      real(OHR_TAU_D             ,rp) ,   0.62472191773963281_rp     ,&        
      real(OHR_TAU_F_FAST        ,rp) ,    7.5508304326178202_rp     ,&        
      real(OHR_TAU_F_SLOW        ,rp) ,    1000.0002055466580_rp     ,&      
      real(OHR_TAU_F_CA_FAST     ,rp) ,    7.0001536053088271_rp     ,&             
      real(OHR_TAU_F_CA_SLOW     ,rp) ,    100.00000002185911_rp     ,&         
      real(OHR_TAU_XR_FAST       ,rp) ,    58.880315344595189_rp     ,&        
      real(OHR_TAU_XR_SLOW       ,rp) ,    1291.2657968412204_rp     ,&                                               
      real(OHR_TAU_X_S1          ,rp) ,    2110.4463564565467_rp     ,&             
      real(OHR_TAU_X_S2          ,rp) ,    33.547157862162862_rp     ,&        
      real(OHR_TAU_X_K1          ,rp) ,  12.601405449776777_rp      &      
      /), (/ EXM_OHR_TAU_EQUATIONS, 2_ip /), ORDER= (/ 2_ip,1_ip /) )   
      
   
      real(rp)    :: result
      integer(ip) :: i
   
      type(ohara_precalc)  :: oe
   
      call oe%init( EXMSLD_CELL_OHARA_INAPA, .FALSE. )
   
      call oe%set_voltage(-80.0_rp)
   
      !print *,"infs"
      !do i=1_ip, size(infs_v80, 1, kind=ip)
      !   print *,oe % get_inf( int(infs_v80(i,1), kind=ip) )
      !end do   
      do i=1_ip, size(infs_v80, 1, kind=ip)
         result = error( oe % get_inf( int(infs_v80(i,1), kind=ip) ), infs_v80(i,2) )
         if ( result > 10*epsilon(1.0_rp) ) then
            print *, "test_ohara_passini_exp_calculation failed on row "//trim(intost(i))&
                           //" difference = "//trim(retost(result))
            stop 1
         end if
      end do
   
      print *,"taus"
      do i=1_ip, size(taus_v80, 1, kind=ip)
         print *,oe % get_tau( int(taus_v80(i,1), kind=ip) )
      end do   
      do i=1_ip, size(taus_v80, 1, kind=ip)
         result = error( oe % get_tau( int(taus_v80(i,1), kind=ip) ), taus_v80(i,2) )
         if ( result > 10*epsilon(1.0_rp) ) then
            print *, "test_ohara_passini_exp_calculation failed on row "//trim(intost(i))&
                           //" difference = "//trim(retost(result))
            stop 1
         end if
      end do
   
   end subroutine test_ohara_passini_inftau_calculation
   
   



subroutine test_ohara_pure_inftau_calculation()
   implicit none

   real(rp), parameter :: infs_v80(EXM_OHR_INF_EQUATIONS,2) = reshape( (/&
   real(OHR_M_INF             ,rp),    1.6369406338341302E-002_rp  ,&  
   real(OHR_H_INF             ,rp),   0.38307812369973809_rp       ,&       
   real(OHR_H_CAMK_INF        ,rp),   0.18313728752891270_rp       ,&       
   real(OHR_M_L_INF           ,rp),    8.6029819612767531E-004_rp  ,&  
   real(OHR_H_L_INF           ,rp),   0.26575015957755954_rp       ,&       
   real(OHR_H_L_CAMK_INF      ,rp),   0.13654539662679432_rp       ,&       
   real(OHR_A_INF             ,rp),    1.7165479337177718E-003_rp  ,&  
   real(OHR_INV_TAU_A_1       ,rp),    2.8054427282450653E-002_rp  ,&  
   real(OHR_INV_TAU_A_2       ,rp),    1.1763656535736702_rp       ,&       
   real(OHR_I_INF             ,rp),   0.99819273301373856_rp       ,&       
   real(OHR_A_I_FAST          ,rp),   0.87454964302408766_rp       ,&       
   real(OHR_A_CAMK_INF        ,rp),    8.7493483578003932E-004_rp  ,&  
   real(OHR_DELTA_CAMK_RECOVER,rp),   0.68877033439907276_rp       ,&       
   real(OHR_D_INF             ,rp),    1.5520757956663981E-008_rp  ,&  
   real(OHR_F_INF             ,rp),   0.99999992049156161_rp       ,&       
   real(OHR_A_F_CA_FAST       ,rp),   0.89992596325440810_rp       ,&       
   real(OHR_X_R_INF           ,rp),    2.6042571473496338E-005_rp  ,&  
   real(OHR_A_XR_FAST         ,rp),   0.65909222935902878_rp       ,&       
   real(OHR_R_KR_1            ,rp),   0.58257020646231472_rp       ,&       
   real(OHR_R_KR_2            ,rp),   0.95257412682243336_rp       ,&       
   real(OHR_X_S1_INF          ,rp),    4.7209440212676900E-004_rp  ,&  
   real(OHR_X_KB              ,rp),    5.7569078163134191E-003_rp  ,&  
   real(OHR_DELTA_EPI         ,rp),   0.16324277592101177_rp        &       
   /), (/ EXM_OHR_INF_EQUATIONS, 2_ip /), ORDER= (/ 2_ip,1_ip /) )   


   real(rp), parameter :: infs_v10(EXM_OHR_INF_EQUATIONS,2_ip) = reshape( (/&
   real(OHR_M_INF             ,rp),   0.99345040531867934_rp  ,&    
   real(OHR_H_INF             ,rp),    2.3479763871668285E-007_rp  ,&           
   real(OHR_H_CAMK_INF        ,rp),    8.4774319970719803E-008_rp  ,&           
   real(OHR_M_L_INF           ,rp),   0.99995637748930410_rp  ,&          
   real(OHR_H_L_INF           ,rp),    2.1814392649788416E-006_rp  ,&                 
   real(OHR_H_L_CAMK_INF      ,rp),    9.5312868550945030E-007_rp  ,&           
   real(OHR_A_INF             ,rp),   0.42730689445366160_rp  ,&         
   real(OHR_INV_TAU_A_1       ,rp),   0.35480731169511914_rp  ,&          
   real(OHR_INV_TAU_A_2       ,rp),    8.0904239839130837E-002_rp  ,&                 
   real(OHR_I_INF             ,rp),    7.9083213725506614E-005_rp  ,&           
   real(OHR_A_I_FAST          ,rp),   0.79356679992265677_rp  ,&         
   real(OHR_A_CAMK_INF        ,rp),   0.27535686953938560_rp  ,&          
   real(OHR_DELTA_CAMK_RECOVER,rp),   0.99100689501895423_rp  ,&                 
   real(OHR_D_INF             ,rp),   0.96427439626084022_rp  ,&           
   real(OHR_F_INF             ,rp),    3.3426346031685419E-004_rp  ,&         
   real(OHR_A_F_CA_FAST       ,rp),   0.59999999999999998_rp  ,&          
   real(OHR_X_R_INF           ,rp),   0.93708485298718047_rp  ,&                 
   real(OHR_A_XR_FAST         ,rp),   0.15496839454840039_rp  ,&           
   real(OHR_R_KR_1            ,rp),   0.29594837238283755_rp  ,&         
   real(OHR_R_KR_2            ,rp),   0.50000000000000000_rp  ,&        
   real(OHR_X_S1_INF          ,rp),   0.91821001861785512_rp  ,&                 
   real(OHR_X_KB              ,rp),   0.43923316166042470_rp  ,&           
   real(OHR_DELTA_EPI         ,rp),   0.99999989309159609_rp   &
   /), (/ EXM_OHR_INF_EQUATIONS, 2_ip /), ORDER= (/ 2_ip,1_ip /) )   

   real(rp), parameter :: taus_v80(EXM_OHR_TAU_EQUATIONS,2_ip) = reshape( (/&
   real(OHR_TAU_M             ,rp) ,       7.0738559308230115E-002_rp  ,&        
   real(OHR_TAU_H_FAST        ,rp) ,      0.24301781703704950_rp       ,&          
   real(OHR_TAU_H_SLOW        ,rp) ,       5.5680060426181583_rp       ,&                
   real(OHR_TAU_J             ,rp) ,       26.500973261834744_rp       ,&           
   real(OHR_TAU_I_FAST        ,rp) ,       7.5459369035467851_rp       ,&           
   real(OHR_TAU_I_SLOW        ,rp) ,       956.75567340542943_rp       ,&         
   real(OHR_DELTA_CAMK_DEVELOP,rp) ,       1.3540000000000001_rp       ,&                
   real(OHR_TAU_D             ,rp) ,      0.62472191773963281_rp       ,&            
   real(OHR_TAU_F_FAST        ,rp) ,       7.5508304326178202_rp       ,&           
   real(OHR_TAU_F_SLOW        ,rp) ,       1000.0002055466580_rp       ,&         
   real(OHR_TAU_F_CA_FAST     ,rp) ,       7.0001536053088271_rp       ,&                
   real(OHR_TAU_F_CA_SLOW     ,rp) ,       100.00000002185911_rp       ,&            
   real(OHR_TAU_XR_FAST       ,rp) ,       58.880315344595189_rp       ,&           
   real(OHR_TAU_XR_SLOW       ,rp) ,       1291.2657968412204_rp       ,&                                                  
   real(OHR_TAU_X_S1          ,rp) ,       2110.4463564565467_rp       ,&                
   real(OHR_TAU_X_S2          ,rp) ,       33.547157862162862_rp       ,&           
   real(OHR_TAU_X_K1          ,rp) ,       12.601405449776777_rp     &    
   /), (/ EXM_OHR_TAU_EQUATIONS, 2_ip /), ORDER= (/ 2_ip,1_ip /) )   

   real(rp), parameter :: taus_v10(EXM_OHR_TAU_EQUATIONS,2) = reshape( (/&
   real(OHR_TAU_M             ,rp) ,   7.9330059744636652E-002_rp,&       
   real(OHR_TAU_H_FAST        ,rp) ,   9.6832730818423751E-002_rp,&       
   real(OHR_TAU_H_SLOW        ,rp) ,   2.2477613076040277_rp     ,&              
   real(OHR_TAU_J             ,rp) ,   4.4997082427931634_rp     ,&         
   real(OHR_TAU_I_FAST        ,rp) ,   4.8836072873503262_rp     ,&         
   real(OHR_TAU_I_SLOW        ,rp) ,   35.561479149049433_rp     ,& !TODO: needs correction, there was a bug     
   real(OHR_DELTA_CAMK_DEVELOP,rp) ,   1.3540000319023162_rp     ,&              
   real(OHR_TAU_D             ,rp) ,  0.70964351292336025_rp     ,&         
   real(OHR_TAU_F_FAST        ,rp) ,   18.036436379937022_rp     ,&         
   real(OHR_TAU_F_SLOW        ,rp) ,   3340.7669317631699_rp     ,&       
   real(OHR_TAU_F_CA_FAST     ,rp) ,   15.990246807568068_rp     ,&              
   real(OHR_TAU_F_CA_SLOW     ,rp) ,   2080.1629445152648_rp     ,&          
   real(OHR_TAU_XR_FAST       ,rp) ,   631.82883126239813_rp     ,&         
   real(OHR_TAU_XR_SLOW       ,rp) ,   430.91059380158003_rp     ,&        
   real(OHR_TAU_X_S1          ,rp) ,   967.85854466136425_rp     ,&              
   real(OHR_TAU_X_S2          ,rp) ,   334.73686238494781_rp     ,&         
   real(OHR_TAU_X_K1          ,rp) ,   3.4758492414298989_rp      &       
   /), (/ EXM_OHR_TAU_EQUATIONS, 2_ip /), ORDER= (/ 2_ip,1_ip /) )   


   real(rp)    :: result
   integer(ip) :: i

   type(ohara_precalc)  :: oe

   call oe%init( EXMSLD_CELL_OHARA, .FALSE. )

   call oe%set_voltage(-80.0_rp)
   if ( .not. oe%ismodified() ) then
      print *, "test_ohara_pure_inftau_calculation: failed to set modified flag after v=-80"
      stop 1
   end if


   !print *,"infs"
   !do i=1_ip, size(infs_v80, 1, kind=ip)
   !   print *,oe % get_inf( int(infs_v80(i,1), kind=ip) )
   !end do
   do i=1_ip, size(infs_v80, 1, kind=ip)
      result = error( oe % get_inf( int(infs_v80(i,1), kind=ip) ), infs_v80(i,2) )
      if ( result > 10*epsilon(1.0_rp) ) then
         print *, "test_ohara_pure_exp_calculation infs v80 failed on row "//trim(intost(i))&
                        //" difference = "//trim(retost(result))
         stop 1
      end if
   end do

   print *,"taus"
   do i=1_ip, size(taus_v80, 1, kind=ip)
      print *,oe % get_tau( int(taus_v80(i,1), kind=ip) )
   end do
   do i=1_ip, size(taus_v80, 1, kind=ip)
      result = error( oe % get_tau( int(taus_v80(i,1), kind=ip) ), taus_v80(i,2) )
      if ( result > 10*epsilon(1.0_rp) ) then
         print *, "test_ohara_pure_exp_calculation taus v80 failed on row "//trim(intost(i))&
                        //" difference = "//trim(retost(result))
         stop 1
      end if
   end do


   if ( oe%ismodified() ) then
      print *, "test_ohara_pure_inftau_calculation: failed to unset modified flag after v=-80"
      stop 1
   end if

   result = error( oe % get_inf( int(infs_v80(1,1), kind=ip) ), infs_v80(1,2) )

   if ( result > 10*epsilon(1.0_rp) ) then
      print *, "test_ohara_pure_inftau_calculation failed on v=-80. difference = "//trim(retost(result))
      stop 1
   end if

   call oe%set_voltage(10.0_rp)
   if ( .not. oe%ismodified() ) then
      print *, "test_ohara_pure_inftau_calculation: failed to set modified flag after v=-4m0"
      stop 1
   end if

   result = error( oe % get_inf( int(infs_v10(1,1), kind=ip) ), infs_v10(1,2) )
   if ( result > 10*epsilon(1.0_rp) ) then
      print *, "test_ohara_pure_inftau_calculation failed on v=-40. difference = "//trim(retost(result))
      stop 1
   end if


   


end subroutine test_ohara_pure_inftau_calculation



   
!modifies params (Inet, qnet, S, W, CaTRPN, B, zeta_s, zeta_w) , viclo, voltage
!as programmed by Jaz, used as a gold reference
subroutine exm_ohara_execute_timestep_jazmin( params, material_params, model_type, has_land, cell_type, &
                                       vcolo_in, vcolo_out, vaulo_in, vaulo_out, viclo, &
                                       voltage, output_params, stim )
   use mod_memory, only : memory_size
   implicit none
   type(ohara_constants),     intent(inout)   :: params
   type(ohara_outputs),       intent(inout)   :: output_params
   real(rp), dimension(:),    intent(in)      :: vcolo_in 
   real(rp), dimension(:),    intent(out)     :: vcolo_out
   real(rp), dimension(:),    intent(inout)   :: viclo
   real(rp), dimension(:),    intent(in)      :: vaulo_in
   real(rp), dimension(:),    intent(out)     :: vaulo_out
   integer(ip),               intent(in)      :: model_type
   integer(ip),               intent(in)      :: cell_type
   logical(lg),               intent(in)      :: has_land
   real(rp), dimension(:,:),  intent(in)      :: material_params ! = ttparmate(:,:,imate)
   real(rp),                  intent(inout)   :: voltage
   real(rp),                  intent(in)      :: stim

   integer(ip) :: i
   real(rp)    :: vinf, xitaux, vaux0, vaux1, vaux2, vaux3, vaux4
   real(rp)    :: vffrt, vfrt, ena, ek, eks, bt, a_rel, btp, a_relp
   real(rp)    :: delepi, ta, devel, recov, fss
   real(rp)    :: iss, tif, tis, tj, v1, v2
   real(rp)    :: a1, a2, a3, a4, b1, b2, b3, b4, k3p, k4p
   real(rp)    :: h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11, h12, x1, x2, x3, x4
   real(rp)    :: k1, k2, k3, k4, k5, k6, k7, k8, e1, e2, e3, e4, k3pp, k4pp, knai
   real(rp)    :: phical, phicana, phicak
   real(rp)    :: hp, hh, finalp, ii, afcaf, afcas, fcap, km2n
   real(rp)    :: jss, ths, tm, ksca
   real(rp)    :: hca, hna, allo, zna, jncxna, jncxca
   real(rp)    :: knao, pp, zk
   real(rp)    :: jnakna, jnakk, xkb, ipp, aif, ais, ikatp
   real(rp)    :: tff, tfcaf, fp, axrf, axrs, xr, rkr, rk1, fjupp
   real(rp)    :: anca, dnca, ff, fca, ficalp, finap, fjrelp
   real(rp)    :: jupnp, jupp, fitop
   real(rp)    :: cai, cass, cansr, cajsr, nass, nai, ki, kss, camkt
   real(rp)    :: rhsx1, rhsx2, rhsx, val0
   real(rp)    :: HFjleak, INaCa
   

   !% START OF CEIOHR CURRENT CALCULATIONS
   vaux1 = params % rgas  * params % temp / params % farad
   ena   = vaux1 * log(  params % nao / vcolo_in(5))
   ek    = vaux1 * log(  params % ko  / vcolo_in(3))
   eks   = vaux1 * log(( params % ko + params % pkna * params % nao ) / (vcolo_in(3) + params % pkna*vcolo_in(5)))

   !%convenient shorthand calculations
   vffrt = voltage * params % farad * params % farad/(params % rgas*params % temp)
   vfrt  = voltage * params % farad/(params % rgas*params % temp)

      !!! First calculate the auxiliaries, Currents, then concentrations and then calculate new voltage,

      !!! Update CaMKa
   vaux2 = 1.0_rp/(1.0_rp + (params % kmcam/vcolo_in(6)))
   viclo(26) = vaux2*params % camko*(1.0_rp - vcolo_in(11))
   viclo(22) = viclo(26) + vcolo_in(11)  ! camka

   viclo(26) = vaux2*params % camko*(1.0_rp - vcolo_in(11))
   viclo(22) = viclo(26) + vcolo_in(11)  ! camka


   !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   !!!! CALCULATE GATES


      !!!!!!   calculate INA
   !calculate m gate
   vinf            = exp(-(voltage + 39.57_rp)/9.871_rp)
   vinf            = 1.0_rp/(1.0_rp + vinf)
   vaux1           = 8.552_rp*exp(-(voltage + 77.42_rp)/5.955_rp)
   xitaux          = vaux1 + (6.765_rp*exp((voltage + 11.64_rp)/34.77_rp))
   tm              = 1.0_rp/xitaux
   vaux0           = vaulo_in(1)
   vaux3           = vinf - (vinf - vaux0)*exp(-params % dtimon/tm)
   vaulo_out(1) = vaux3 ! value of variable m

      !!!!!!! hf
   if ( model_type == EXMSLD_CELL_OHARA_INAPA ) then
      vinf   = exp((voltage + 78.50_rp)/6.22_rp) !modified from Passini et al. with LMS no reg(original: exp((voltage + 82.90_rp) / 6.086_rp))
      vinf   = 1.0_rp/(1.0_rp + vinf)
      vaux1  = exp(-(voltage + 3.8875_rp)/7.8579_rp) !modified from Passini et al. with LMS no reg(original: exp(-(voltage + 1.196_rp) / 6.285_rp))
      vaux2  = exp((voltage - 0.4963_rp)/9.1843_rp) !modified from Passini et al. with LMS no reg(original: exp((voltage + 0.5096_rp) / 20.27_rp))
      xitaux = (0.000003686_rp*vaux1) + (16.0_rp*vaux2) !modified from Passini et al. with LMS no reg(original: (0.00001432_rp * vaux1) + (6.149_rp * vaux2) )
   else
      vinf   = exp((voltage + 82.90_rp)/6.086_rp) !original O'Hara-Rudy formulation
      vinf   = 1.0_rp/(1.0_rp + vinf)
      vaux1  = exp(-(voltage + 1.196_rp)/6.285_rp) !original O'Hara-Rudy formulation
      vaux2  = exp((voltage + 0.5096_rp)/20.27_rp) !origina O'Hara-Rudy formulation
      xitaux = (0.00001432_rp*vaux1) + (6.149_rp*vaux2) !original O'Hara-Rudy formulation
   end if
   xitaux          = 1.0_rp/xitaux
   vaux0           = vaulo_in(2)
   vaux3           = vinf - (vinf - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(2) = vaux3      ! value of variable hf
   
   jss = vinf
      !!!! hs
   vaux1           = exp(-(voltage + 17.95_rp)/28.05_rp)
   vaux2           = exp((voltage + 5.730_rp)/56.66_rp)
   xitaux          = (0.009794_rp*vaux1) + (0.3343_rp*vaux2)
   ths             = 1.0_rp/xitaux
   vaux0           = vaulo_in(3)
   vaux3           = vinf - (vinf - vaux0)*exp(-params % dtimon/ths)
   vaulo_out(3) = vaux3      ! value of variable hs

      !!!!!  params % j
   if ( model_type == EXMSLD_CELL_OHARA_INAPA ) then
      vaux1  = exp(-(voltage + 116.7258_rp)/7.6005_rp) !modified from Passini et al. with LMS no reg(original: exp(-(v+100.6)/8.281))
      vaux2  = exp((voltage + 6.2719_rp)/9.0358_rp) !modified from Passini et al. with LMS no reg(original:  exp((voltage + 0.9941_rp) / 38.45_rp))
      xitaux = (0.8628_rp*vaux1) + (1.1096_rp*vaux2) !modified from Passini et al. with LMS no reg(original: (0.02136_rp * vaux1) + (0.3052_rp * vaux2))
      tj     = 4.8590_rp + (1.0_rp/xitaux) !modified from Passini et al. with LMS no reg(original: 2.038_rp + (1.0_rp / xitaux))
   else
      vaux1  = exp(-(voltage + 100.6_rp)/8.281_rp) !original O'Hara-Rudy formulation
      vaux2  = exp((voltage + 0.9941_rp)/38.45_rp) !original O'Hara-Rudy formulation
      xitaux = (0.02136_rp*vaux1) + (0.3052_rp*vaux2) !original O'Hara-Rudy formulation
      tj     = 2.038_rp + (1.0_rp/xitaux) !original O'Hara-Rudy formulation
   end if
   vaux0           = vaulo_in(4)
   vaux3           = jss - (jss - vaux0)*exp(-params % dtimon/tj)
   vaulo_out(4) = vaux3      ! value of variable params % j

      !!!!  hsp
   if ( model_type == EXMSLD_CELL_OHARA_INAPA ) then
      vinf = 1.0_rp/(1.0_rp + exp((voltage + 84.7_rp)/6.22_rp)) !modified from Passini et al. (original:   1.0_rp / (1.0_rp + exp((voltage + 89.1_rp) / 6.086_rp)))
   else
      vinf = 1.0_rp/(1.0_rp + exp((voltage + 89.1_rp)/6.086_rp)) !original:   1.0_rp / (1.0_rp + exp((voltage + 89.1_rp) / 6.086_rp)))
   end if
   xitaux          = 3.0_rp*ths
   vaux0           = vaulo_in(5)
   vaux3           = vinf - (vinf - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(5) = vaux3      ! value of variable hsp

      !!! jp
   xitaux          = 1.46_rp*tj
   vaux0           = vaulo_in(6)
   vaux3           = jss - (jss - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(6) = vaux3      ! value of variable jp

   !Calculate INA
            !!!!  hp
   hp       = (params % ahf*vaulo_out(2)) + (params % ahs*vaulo_out(5))
   vaux1    = params % ahf*vaulo_out(2)
   hh       = vaux1 + (params % ahs*vaulo_out(3))  !h
   finap    = 1.0_rp/(1.0_rp + (params % kmcamk/viclo(22)))
   vaux1    = params % gna*(voltage - ena)
   vaux1    = vaux1*vaulo_out(1)*vaulo_out(1)*vaulo_out(1)
   vaux2    = (1.0_rp - finap)*hh*vaulo_out(4)
   vaux2    = vaux2 + (finap*hp*vaulo_out(6))
   viclo(1) = vaux1*vaux2  !!!!  ina

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !%calculate inal
      !!!!!!  ml
   vinf            = 1.0_rp + exp(-(voltage + 42.85_rp)/5.264_rp)
   vinf            = 1.0_rp/vinf
   vaux0           = vaulo_in(7)
   vaux3           = vinf - (vinf - vaux0)*exp(-params % dtimon/tm)
   vaulo_out(7) = vaux3      ! value of variable ml

      !!!!! hl
   vinf = 1.0_rp + exp((voltage + 87.61_rp)/7.488_rp)
   vinf = 1.0_rp/vinf
   vaux0 = vaulo_in(8)
   vaux3 = vinf - (vinf - vaux0)*exp(-params % dtimon/params % tauHL)
   vaulo_out(8) = vaux3      ! value of variable hl

      !!!!  hlp
   vinf            = 1.0_rp + exp((voltage + 93.81_rp)/7.488_rp)
   vinf            = 1.0_rp/vinf
   xitaux          = 3.0_rp*200.0_rp
   vaux0           = vaulo_in(9)
   vaux3           = vinf - (vinf - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(9) = vaux3      ! value of variable hlp

         !!!  Calculate inal current
   finalp   = 1.0_rp + (params % kmcamk/viclo(22))
   finalp   = 1.0_rp/finalp
   vaux1    = params % gnal*(voltage - ena)*vaulo_out(7)
   vaux2    = (1.0_rp - finalp)*vaulo_out(8) + (finalp*vaulo_out(9))
   viclo(2) = vaux1*vaux2

      !!!!! calculate ito
      !!!!!!  calculate variable a
   vinf             = 1.0_rp + exp(-(voltage - 14.34_rp)/14.82_rp)
   vinf             = 1.0_rp/vinf
   vaux1            = 1.0_rp/(1.2089_rp*(1.0_rp + exp(-(voltage - 18.4099_rp)/29.3814_rp)))
   vaux2            = 3.5_rp/(1.0_rp + exp((voltage + 100.0_rp)/29.3814_rp))
   ta               = 1.0515_rp/(vaux1 + vaux2)
   vaux0            = vaulo_in(10)
   vaux3            = vinf - (vinf - vaux0)*exp(-params % dtimon/ta)
   vaulo_out(10) = vaux3      ! value of variable a

      !!!!! calculate ifast  iss
   vinf = 1.0_rp + exp((voltage + 43.94_rp)/5.711_rp)
   iss  = 1.0_rp/vinf
   if (cell_type == EXM_CELLTYPE_EPI) then
      delepi = (1.0_rp + exp((voltage + 70.0_rp)/5.0_rp))
      delepi = (0.95_rp/delepi)
      delepi = 1.0_rp - delepi
   else
      delepi = 1.0_rp
   end if
   vaux1            = 0.08004_rp*exp((voltage + 50.0_rp)/16.59_rp) !tif
   vaux2            = 0.3933_rp*exp(-(voltage + 100.0_rp)/100.0_rp)
   vaux3            = vaux1 + vaux2
   xitaux           = 4.562_rp + (1.0_rp/vaux3)
   xitaux           = delepi*xitaux
   tif              = xitaux
   vaux0            = vaulo_in(11)
   vaux3            = iss - (iss - vaux0)*exp(-params % dtimon/tif)
   vaulo_out(11) = vaux3      ! value of variable ifast

      !!!!!!  calculate islow  tis
   vaux1            = 0.001416_rp*exp(-(voltage + 96.52_rp)/59.05_rp)
   vaux2            = 0.00000001780_rp*exp((voltage + 114.1_rp)/8.079_rp)
   vaux3            = vaux1 + vaux2
   xitaux           = 23.62_rp + (1.0_rp/vaux3)
   xitaux           = delepi*xitaux
   tis              = xitaux
   vaux0            = vaulo_in(12)
   vaux3            = iss - (iss - vaux0)*exp(-params % dtimon/tis)
   vaulo_out(12) = vaux3      ! value of variable islow

      !!!!  calculate ap  (assp)
   vinf             = 1.0_rp + exp(-(voltage - 24.34_rp)/14.82_rp)
   vinf             = 1.0_rp/vinf
   vaux0            = vaulo_in(13)
   vaux3            = vinf - (vinf - vaux0)*exp(-params % dtimon/ta)
   vaulo_out(13) = vaux3      ! value of variable ap

      !!!!!!!!!! calculate ifp (dti_develop)
   vaux1 = exp((voltage - 167.4_rp)/15.89_rp)
   vaux2 = exp(-(voltage - 12.23_rp)/0.2154_rp)
   devel = vaux1 + vaux2
   devel = 1.354_rp + (0.0001_rp/devel)

   vaux1            = exp((voltage + 70.0_rp)/20.0_rp)
   vaux1            = 1.0_rp + vaux1
   recov            = 1.0_rp - (0.5_rp/vaux1)
   xitaux           = devel*recov*tif
   vaux0            = vaulo_in(14)
   vaux3            = iss - (iss - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(14) = vaux3      ! value of variable ifp

      !!!!! calculate isp
   xitaux           = devel*recov*tis
   vaux0            = vaulo_in(15)
   vaux3            = iss - (iss - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(15) = vaux3      ! value of variable isp

            !!!!!!  Ito current
            !!!! calculate aif, ais and ii
   vaux1    =  exp((voltage - 213.6_rp)/151.2_rp)      
   aif      = 1.0_rp/(1.0_rp + vaux1)
   !aif      = precalc % get_inf(OHR_A_I_FAST)
   ais      = 1.0_rp - aif
   ii       = aif*vaulo_out(11) + (ais*vaulo_out(12))
   ipp      = aif*vaulo_out(14) + (ais*vaulo_out(15))
   vaux1    = params % kmcamk/viclo(22)
   fitop    = 1.0_rp/(1.0_rp + vaux1)
   vaux2    = (fitop*vaulo_out(13)*ipp)
   vaux1    = ((1.0_rp - fitop)*vaulo_out(10)*ii) + vaux2
   vaux1    = params % gto*(voltage - ek)*vaux1
   viclo(3) = vaux1        !!! ito

      !!! calculate ical, icana, icak
      !%calculate ical
      !!!!  calculate gate d
   vaux1            = exp(-(voltage + 3.940_rp)/4.230_rp)
   vinf             = 1.0_rp/(1.0_rp + vaux1)
   vaux1            = exp(0.09_rp*(voltage + 14.0_rp))
   vaux2            = exp(-0.05_rp*(voltage + 6.0_rp))
   xitaux           = vaux1 + vaux2
   xitaux           = 0.6_rp + (1.0_rp/xitaux)
   vaux0            = vaulo_in(16)
   vaux3            = vinf - (vinf - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(16) = vaux3      ! value of gate d

      !!!! calculate gate ff
   vaux1            = exp((voltage + 19.58_rp)/3.696_rp)
   vinf             = 1.0_rp/(1.0_rp + vaux1)
   fss              = vinf
   vaux1            = 0.0045_rp*exp((voltage + 20.0_rp)/10.0_rp)
   vaux2            = 0.0045_rp*exp(-(voltage + 20.0_rp)/10.0_rp)
   tff              = 7.0_rp + (1.0_rp/(vaux1 + vaux2))
   vaux0            = vaulo_in(17)
   vaux3            = fss - (fss - vaux0)*exp(-params % dtimon/tff)
   vaulo_out(17) = vaux3      ! value of gate ff

      !!!!!!  calculate gate fs
   vaux1            = 0.000035_rp*exp((voltage + 5.0_rp)/6.0_rp)
   vaux2            = 0.000035_rp*exp(-(voltage + 5.0_rp)/4.0_rp)
   xitaux           = 1000.0_rp + (1.0_rp/(vaux1 + vaux2))
   vaux0            = vaulo_in(18)
   vaux3            = fss - (fss - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(18) = vaux3      ! value of variable fs

      !!!!! calculate fcass
            !!fcass=fss
   vaux1            = (0.04_rp*exp((voltage - 4.0_rp)/7.0_rp))
   vaux2            = (0.04_rp*exp(-(voltage - 4.0_rp)/7.0_rp))
   tfcaf            = 7.0_rp + (1.0_rp/(vaux1 + vaux2))
   vaux0            = vaulo_in(19)
   vaux3            = fss - (fss - vaux0)*exp(-params % dtimon/tfcaf)
   vaulo_out(19) = vaux3      ! value of variable fcaf

      !!!! calculate gate fcas
   vaux1            = (0.00012_rp*exp(voltage/7.0_rp))
   vaux2            = (0.00012_rp*exp(-voltage/3.0_rp))
   xitaux           = 100.0_rp + (1.0_rp/(vaux1 + vaux2))
   vaux0            = vaulo_in(20)
   vaux3            = fss - (fss - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(20) = vaux3      ! value of variable fcas

      !!! calculate gate jca
   xitaux           = 75.0_rp
   vaux0            = vaulo_in(21)
   vaux3            = fss - (fss - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(21) = vaux3      ! value of variable jca

      !!! calculate gate ffp
   xitaux           = 2.5_rp*tff
   vaux0            = vaulo_in(23)
   vaux3            = fss - (fss - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(23) = vaux3      ! value of variable ffp

      !!!! calculate gate fcafp
   xitaux           = 2.5_rp*tfcaf
   vaux0            = vaulo_in(24)
   vaux3            = fss - (fss - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(24) = vaux3      ! value of variable ffp   

      !!!!! calculate icana and icak
   km2n             = vaulo_out(21)*1.0_rp
   vaux1            = params % kmn/vcolo_in(6)
   vaux4            = (1.0_rp + vaux1)*(1.0_rp + vaux1)*(1.0_rp + vaux1)*(1.0_rp + vaux1)
   vaux2            = (params % k2n/km2n) + vaux4
   anca             = 1.0_rp/vaux2
   dnca             = (anca*params % k2n/km2n)
   vaux0            = vaulo_in(22)
   vaux3            = dnca - (dnca - vaux0)*exp(-params % dtimon*km2n)
   vaulo_out(22)    = vaux3

      !!!!!! calculate ff  = F
   ff    = (params % aff*vaulo_out(17)) + (params % afs*vaulo_out(18))   !value of variable ff
         !!! calculate fca
   vaux1 = 1.0_rp + exp((voltage - 10.0_rp)/10.0_rp)
   afcaf = 0.3_rp + (0.6_rp/vaux1)
   !afcaf = precalc % get_inf(OHR_A_F_CA_FAST) 
   afcas = 1.0_rp - afcaf
   fca   = (afcaf*vaulo_out(19)) + (afcas*vaulo_out(20))
      !!!!  calculate fcap
   fcap  = afcaf*vaulo_out(24) + (afcas*vaulo_out(20))
      !!!!! calculate fp
   fp      = params % aff*vaulo_out(23) + (params % afs*vaulo_out(18))
   vaux2   = exp(2.0_rp*vfrt)
   vaux1   = exp(1.0_rp*vfrt)
   vaux3   = 1.0_rp/(vaux2 - 1.0_rp)
   phical  = 4.0_rp*vffrt*((vcolo_in(6)*vaux2) - (0.341_rp*params % cao))*vaux3
   vaux3   = 1.0_rp/(vaux1 - 1.0_rp)
   phicana = 1.0_rp*vffrt*((0.75_rp*vcolo_in(2)*vaux1) - (0.75_rp*params % nao))*vaux3
   phicak  = 1.0_rp*vffrt*((0.75_rp*vcolo_in(4)*vaux1) - (0.75_rp*params % ko))*vaux3
   ficalp  = 1.0_rp/(1.0_rp + (params % kmcamk/viclo(22)))

      !!!! calculate ical current
   vaux1 = (fp*(1.0_rp - vaulo_out(22)) + (vaulo_out(21)*fcap*vaulo_out(22)))
   vaux1 = ficalp*params % pcap*phical*vaulo_out(16)*vaux1
   vaux2 = ff*(1.0_rp - vaulo_out(22)) + (vaulo_out(21)*fca*vaulo_out(22))
   vaux3 = (1.0_rp - ficalp)*params % pca*phical*vaulo_out(16)
   viclo(4) = (vaux3*vaux2) + vaux1  !!! ical


      !!!! calculate icana current
   vaux1 = (fp*(1.0_rp - vaulo_out(22)) + (vaulo_out(21)*fcap*vaulo_out(22)))
   vaux1 = vaux1*ficalp*params % pcanap*phicana*vaulo_out(16)
   vaux2 = (ff*(1.0_rp - vaulo_out(22))) + (vaulo_out(21)*fca*vaulo_out(22))
   vaux2 = vaux2*(1.0_rp - ficalp)*params % pcana*phicana*vaulo_out(16)
   viclo(24) = vaux2 + vaux1  !! icana

      !!!!! calculate icak
   vaux1 = fp*(1.0_rp - vaulo_out(22)) + (vaulo_out(21)*fcap*vaulo_out(22))
   vaux1 = vaux1*ficalp*params % pcakp*phicak*vaulo_out(16)
   vaux2 = ff*(1.0_rp - vaulo_out(22)) + (vaulo_out(21)*fca*vaulo_out(22))
   vaux2 = vaux2*(1.0_rp - ficalp)*params % pcak*phicak*vaulo_out(16)
   viclo(25) = vaux2 + vaux1   !!! icak

      !!!!  calculate ikr GATES  XRF
   vinf   = exp(-(voltage + 8.337_rp)/6.789_rp)
   vinf   = 1.0_rp/(1.0_rp + vinf)
   vaux1  = 0.00004123_rp*exp(-(voltage - 47.78_rp)/20.38_rp)
   vaux2  = 0.3652_rp*exp((voltage - 31.66_rp)/3.869_rp)
   xitaux = 12.98_rp + (1.0_rp/(vaux1 + vaux2))
   vaux0  = vaulo_in(25)
   vaux3  = vinf - (vinf - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(25) = vaux3      ! value of variable xrf
      !! GATE XRS
   vaux1  = 0.06629_rp*exp((voltage - 34.70_rp)/7.355_rp)
   vaux2  = 0.00001128_rp*exp(-(voltage - 29.74_rp)/25.94_rp)
   xitaux = 1.865_rp + (1.0_rp/(vaux1 + vaux2))
   vaux0  = vaulo_in(26)
   vaux3  = vinf - (vinf - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(26) = vaux3      ! value of variable xrs

   !%calculate ikr CURRENT
   vaux1 = exp((voltage + 54.81_rp)/38.21_rp)
   axrf  = 1.0_rp/(1.0_rp + vaux1)
   !axrf  = precalc % get_inf(OHR_A_XR_FAST) !1.0_rp/(1.0_rp + vaux1)
   axrs  = 1.0_rp - axrf
   xr    = axrf*vaulo_out(25) + (axrs*vaulo_out(26))
   vaux1 = 1.0_rp + exp((voltage + 55.0_rp)/75.0_rp)
   vaux2 = 1.0_rp + exp((voltage - 10.0_rp)/30.0_rp)
   vaux3 = vaux1*vaux2
   rkr   = 1.0_rp/vaux3
   !rkr   = precalc % get_inf(OHR_R_KR_1) * precalc % get_inf(OHR_R_KR_2)
   vaux1 = sqrt(params % ko/5.4_rp)*(voltage - ek)
   viclo(5) = params % gkr*vaux1*xr*rkr   !!! ikr

      !!! %calculate iks GATES
   vinf = exp(-(voltage + 11.60_rp)/8.932_rp)
   vinf = 1.0_rp/(1.0_rp + vinf)
   vaux1 = 0.0002326_rp*exp((voltage + 48.28_rp)/17.80_rp)
   vaux2 = 0.001292_rp*exp(-(voltage + 210.0_rp)/230.0_rp)
   xitaux = 817.3_rp + (1.0_rp/(vaux1 + vaux2))
   vaux0 = vaulo_in(27)
   vaux3 = vinf - (vinf - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(27) = vaux3      ! value of variable xs1

   !vinf = xs2ss = xs1ss
   vaux1 = 0.01_rp*exp((voltage - 50.0_rp)/20.0_rp)
   vaux2 = 0.0193_rp*exp(-(voltage + 66.54_rp)/31.0_rp)
   xitaux = 1.0_rp/(vaux1 + vaux2)
   vaux0 = vaulo_in(28)
   vaux3 = vinf - (vinf - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(28) = vaux3      ! value of variable xs2

      !!!!  calculate gate xk1 GATES
   vaux1 = 1.0_rp/(1.5692_rp*params % ko + 3.8115_rp)
   vaux2 = voltage + 2.5538_rp*params % ko + 144.59_rp
   vinf = exp(-vaux2*vaux1)
   vinf = 1.0_rp/(1.0_rp + vinf)
   vaux1 = exp(-(voltage + 127.2_rp)/20.36_rp)
   vaux2 = exp((voltage + 236.8_rp)/69.33_rp)
   xitaux = 122.2_rp/(vaux1 + vaux2)
   vaux0 = vaulo_in(29)
   vaux3 = vinf - (vinf - vaux0)*exp(-params % dtimon/xitaux)
   vaulo_out(29) = vaux3      ! value of variable xk1

   !print *,abs(vaulo_out - vaulo_tmp )
   !stop 1

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !!
   !!  Concentrations and everything else
   !!
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      !!!! calculate iks CURRENTS
   ksca = (0.000038_rp/vcolo_in(1))**(1.4_rp) !!(7.0_rp/5.0_rp)
   ksca = 1.0_rp + (0.6_rp/(1.0_rp + ksca))
   vaux1 = params % gks*ksca*vaulo_out(27)*vaulo_out(28)
   viclo(6) = vaux1*(voltage - eks)


            !!!! calculate ik1 CURRENT
   rk1 = 1.0_rp + exp((voltage + 105.8_rp - 2.6_rp*params % ko)/9.493_rp)
   rk1 = 1.0_rp/rk1
   vaux1 = params % gk1*sqrt(params % ko)*rk1*vaulo_out(29)
   viclo(7) = vaux1*(voltage - ek) !!! ik1
   
   !!!! calculate IkATP CURRENT for ISCHEMIA modelling
   ikatp=params % gkatp*params % fkatp *(params % ko/5.4_rp)**0.24_rp * (voltage-ek)
   viclo(23)=ikatp
   
   !%calculate INaCa CURRENT
   hca    = exp(params % qca*vfrt)
   hna    = exp(params % qna*vfrt)
   h1     = 1.0_rp + (vcolo_in(5)/params % kna3)*(1.0_rp + hna)
   h2     = (vcolo_in(5)*hna)/(params % kna3*h1)
   h3     = 1.0_rp/h1
   h4     = 1.0_rp + vcolo_in(5)/params % kna1*(1.0_rp + (vcolo_in(5)/params % kna2))
   h5     = vcolo_in(5)*vcolo_in(5)/(h4*params % kna1*params % kna2)
   h6     = 1.0_rp/h4
   h7     = 1.0_rp + params % nao/params % kna3*(1.0_rp + (1.0_rp/hna))
   h8     = params % nao/(params % kna3*hna*h7)
   h9     = 1.0_rp/h7
   h10    = params % kasymm + 1.0_rp + params % nao/params % kna1*(1.0_rp + (params % nao/params % kna2))
   h11    = params % nao*params % nao/(h10*params % kna1*params % kna2)
   h12    = 1.0_rp/h10
   k1     = h12*params % cao*params % kcaon
   k2     = params % kcaoff
   k3p    = h9*params % wca
   k3pp   = h8*params % wnaca
   k3     = k3p + k3pp
   k4p    = h3*params % wca/hca
   k4pp   = h2*params % wnaca
   k4     = k4p + k4pp
   k5     = params % kcaoff
   k6     = h6*vcolo_in(1)*params % kcaon
   k7     = h5*h2*params % wna
   k8     = h8*h11*params % wna
   x1     = (k2*k4*(k7 + k6)) + (k5*k7*(k2 + k3))
   x2     = (k1*k7*(k4 + k5)) + (k4*k6*(k1 + k8))
   x3     = (k1*k3*(k7 + k6)) + (k8*k6*(k2 + k3))
   x4     = (k2*k8*(k4 + k5)) + (k3*k5*(k1 + k8))
   e1     = x1/(x1 + x2 + x3 + x4)
   e2     = x2/(x1 + x2 + x3 + x4)
   e3     = x3/(x1 + x2 + x3 + x4)
   e4     = x4/(x1 + x2 + x3 + x4)
   allo   = (params % kmcaact/vcolo_in(1))*(params % kmcaact/vcolo_in(1))
   allo   = 1.0_rp/(1.0_rp + allo)
   zna    = 1.0_rp
   jncxna = (3.0_rp*(e4*k7 - e1*k8)) + (e3*k4pp) - (e2*k3pp)
   jncxca = (e2*k2) - (e1*k1)
   vaux1  = (zna*jncxna) + (params % zca*jncxca)
   viclo(8) = 0.8_rp*params % gncx*allo*vaux1  !!! inaca_i Current

   !%calculate inaca_ss
   h1     = 1.0_rp + vcolo_in(2)/params % kna3*(1.0_rp + hna)
   h2     = (vcolo_in(2)*hna)/(params % kna3*h1)
   h3     = 1.0_rp/h1
   h4     = 1.0_rp + vcolo_in(2)/params % kna1*(1.0_rp + (vcolo_in(2)/params % kna2))
   h5     = vcolo_in(2)*vcolo_in(2)/(h4*params % kna1*params % kna2)
   h6     = 1.0_rp/h4
   h7     = 1.0_rp + (params % nao/params % kna3)*(1.0_rp + (1.0_rp/hna))
   h8     = params % nao/(params % kna3*hna*h7)
   h9     = 1.0_rp/h7
   h10    = params % kasymm + 1.0_rp + params % nao/params % kna1*(1.0_rp + (params % nao/params % kna2))
   h11    = params % nao*params % nao/(h10*params % kna1*params % kna2)
   h12    = 1.0_rp/h10
   k1     = h12*params % cao*params % kcaon
   k2     = params % kcaoff
   k3p    = h9*params % wca
   k3pp   = h8*params % wnaca
   k3     = k3p + k3pp
   k4p    = h3*params % wca/hca
   k4pp   = h2*params % wnaca
   k4     = k4p + k4pp
   k5     = params % kcaoff
   k6     = h6*vcolo_in(6)*params % kcaon
   k7     = h5*h2*params % wna
   k8     = h8*h11*params % wna
   x1     = (k2*k4*(k7 + k6)) + (k5*k7*(k2 + k3))
   x2     = (k1*k7*(k4 + k5)) + (k4*k6*(k1 + k8))
   x3     = (k1*k3*(k7 + k6)) + (k8*k6*(k2 + k3))
   x4     = (k2*k8*(k4 + k5)) + (k3*k5*(k1 + k8))
   e1     = x1/(x1 + x2 + x3 + x4)
   e2     = x2/(x1 + x2 + x3 + x4)
   e3     = x3/(x1 + x2 + x3 + x4)
   e4     = x4/(x1 + x2 + x3 + x4)
   allo   = (params % kmcaact/vcolo_in(6))*(params % kmcaact/vcolo_in(6))
   allo   = 1.0_rp/(1.0_rp + allo)
   jncxna = (3.0_rp*(e4*k7 - e1*k8)) + (e3*k4pp) - (e2*k3pp)
   jncxca = (e2*k2) - (e1*k1)
   viclo(9) = 0.2_rp*params % gncx*allo*((zna*jncxna) + (params % zca*jncxca))   !!! inaca_ss

   INaCa = viclo(8) + viclo(9)

   !%calculate inak
   k3p    = 1899.0_rp
   k4p    = 639.0_rp
   knai   = params % knai0*exp(params % delta*vfrt/3.0_rp)
   knao   = params % knao0*exp((1.0_rp - params % delta)*vfrt/3.0_rp)
   pp     = params % ep/(1.0_rp + params % hbig/params % khp + (vcolo_in(5)/params % knap) + (vcolo_in(3)/params % kxkur))
   vaux1  = (vcolo_in(5)/knai)*(vcolo_in(5)/knai)*(vcolo_in(5)/knai)
   vaux2  = (1.0_rp + (vcolo_in(5)/knai))*(1.0_rp + (vcolo_in(5)/knai))*(1.0_rp + (vcolo_in(5)/knai))
   vaux3  = (1.0_rp + (vcolo_in(3)/params % kki))*(1.0_rp + (vcolo_in(3)/params % kki))
   a1     = (params % k1p*vaux1)/(vaux2 + vaux3 - 1.0_rp)
   b1     = params % k1m*params % mgadp
   a2     = params % k2p
   vaux1  = (params % nao/knao)*(params % nao/knao)*(params % nao/knao)
   vaux2  = (1.0_rp + (params % nao/knao))*(1.0_rp + (params % nao/knao))*(1.0_rp + (params % nao/knao))
   vaux3  = (1.0_rp + (params % ko/params % kko))*(1.0_rp + (params % ko/params % kko))
   b2     = (params % k2m*vaux1)/(vaux2 + vaux3 - 1.0_rp)
   vaux1  = (params % ko/params % kko)*(params % ko/params % kko)
   a3     = (k3p*vaux1)/(vaux2 + vaux3 - 1.0_rp)
   b3     = (params % k3m*pp*params % hbig)/(1.0_rp + (params % mgatp/params % kmgatp))
   a4     = (k4p*params % mgatp/params % kmgatp)/(1.0_rp + (params % mgatp/params % kmgatp))
   vaux1  = (vcolo_in(3)/params % kki)*(vcolo_in(3)/params % kki)
   vaux2  = (1.0_rp + (vcolo_in(5)/knai))*(1.0_rp + (vcolo_in(5)/knai))*(1.0_rp + (vcolo_in(5)/knai))
   vaux3  = (1.0_rp + (vcolo_in(3)/params % kki))*(1.0_rp + (vcolo_in(3)/params % kki))
   b4     = (params % k4m*vaux1)/(vaux2 + vaux3 - 1.0_rp)
   x1     = (a4*a1*a2) + (b2*b4*b3) + (a2*b4*b3) + (b3*a1*a2)
   x2     = (b2*b1*b4) + (a1*a2*a3) + (a3*b1*b4) + (a2*a3*b4)
   x3     = (a2*a3*a4) + (b3*b2*b1) + (b2*b1*a4) + (a3*a4*b1)
   x4     = (b4*b3*b2) + (a3*a4*a1) + (b2*a4*a1) + (b3*b2*a1)
   e1     = x1/(x1 + x2 + x3 + x4)
   e2     = x2/(x1 + x2 + x3 + x4)
   e3     = x3/(x1 + x2 + x3 + x4)
   e4     = x4/(x1 + x2 + x3 + x4)
   zk     = 1.0_rp
   jnakna = 3.0_rp*((e1*a3) - (e2*b3))
   jnakk  = 2.0_rp*((e4*b1) - (e3*a1))
   viclo(10) = params % pnak*(zna*jnakna + zk*jnakk)  !!!! inak

   !%calculate ikb CURRENT
   xkb = 1.0_rp/(1.0_rp + exp(-(voltage - 14.48_rp)/18.34_rp))
   viclo(11) = params % gkb*xkb*(voltage - ek)  !!!! ikb

   !%calculate inab CURRENT
   vaux1 = params % pnab*vffrt*(vcolo_in(5)*exp(vfrt) - params % nao)
   viclo(12) = vaux1/(exp(vfrt) - 1.0_rp)

   !%calculate icab CURRENT
   vaux1 = (vcolo_in(1)*exp(2.0_rp*vfrt) - (0.341_rp*params % cao))/(exp(2.0_rp*vfrt) - 1.0_rp)
   viclo(13) = params % pcab*4.0_rp*vffrt*vaux1   !!!!icab

   !%calculate ipca CURRENT
   viclo(14) = params % gpca*vcolo_in(1)/(0.0005_rp + vcolo_in(1))  !!! ipca

   !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         !!  VOLTAGE INTEGRATION
   v1 = 0.0_rp
   v2 = 0.0_rp
   do i = 1, 14
      v1 = v1 + viclo(i)
   end do
   do i = 23, 25
      v2 = v2 + viclo(i)
   end do
   do i=2,7 
      output_params % Inet = output_params % Inet + viclo(i)
   end do
   output_params % Inet = output_params % Inet + viclo(23)
   output_params % qnet = output_params % qnet + (output_params % Inet * params % dtimon)
   voltage = voltage + params % dtimon * ( -(v1 + v2 + stim) ) ! *params % farad/params % acap

   !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      !!! Update CaMKa
   vaux2 = 1.0_rp/(1.0_rp + ( params % kmcam/vcolo_in(6)))
   viclo(26) = vaux2*params % camko*(1.0_rp - vcolo_in(11))
   viclo(22) = viclo(26) + vcolo_in(11)  ! camka

   !%update camk
   rhsx1 = params % acamk*viclo(26)*(viclo(26) + vcolo_in(11))
   rhsx = rhsx1 - (params % bcamk*vcolo_in(11))
   val0 = vcolo_in(11)
   camkt = val0 + params % dtimon*rhsx
   !k1 = rhsx
   !k2 = rhsx + 0.5_rp * params % dtimon* k1
   !k3 = rhsx + 0.5_rp * params % dtimon * k2
   !k4 = rhsx + params % dtimon * k3
   !camkt = val0 + ((params % dtimon / (6.0_rp)) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))
   vcolo_out(11) = camkt      ! value of camkt concentration

   !%calculate diffusion fluxes
   viclo(16) = (vcolo_in(2) - vcolo_in(5))/2.0_rp    !!! jdiffna
   viclo(17) = (vcolo_in(4) - vcolo_in(3))/2.0_rp    !!! jdiffk
   viclo(15) = (vcolo_in(6) - vcolo_in(1))/0.2_rp    !!! jdiff
   !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   !%calculate ryanodione receptor calcium induced calcium release from the jsr
   bt = 4.75_rp
   a_rel = 0.5_rp*bt
   vaux1 = (1.5_rp/vcolo_in(8))**8.0_rp
   vinf = a_rel*(-viclo(4))/(1.0_rp + vaux1)

   if     (cell_type == EXM_CELLTYPE_MID) then
      vinf = vinf * 1.7_rp * material_params(2, 16)
   elseif (cell_type == EXM_CELLTYPE_ENDO) then
      vinf = vinf          * material_params(1, 16)
   elseif (cell_type == EXM_CELLTYPE_EPI) then
      vinf = vinf          * material_params(3, 16)
   end if
   xitaux = bt/(1.0_rp + 0.0123_rp/vcolo_in(8))

   if (xitaux < 0.001_rp) then  !fixed bug was wrongly 0.005
      xitaux = 0.001_rp    !fixed bug was wrongly 0.005
   end if

   vaux0 = vcolo_in(9)
   !vaux1 = (vinf - vaux0) * xitaux;
   !vaux2 = (vinf - (vaux0 + 0.5_rp * params % dtimon * vaux1)) * xitaux;
   !vaux3 = (vinf - (vaux0 + 0.5_rp * params % dtimon * vaux2)) * xitaux;
   !vaux4 = (vinf - (vaux0 + params % dtimon * vaux3)) * xitaux;
   !vaux5 = vaux0 + (params % dtimon / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4);
   !vaux3 = vaux5
   vaux3 = vinf - (vinf - vaux0)*exp(-params % dtimon/xitaux)
   vcolo_out(9) = vaux3      ! value of variable jrelnp


            !!  JRELP
   btp = 1.25_rp*bt
   a_relp = 0.5_rp*btp
   vaux1 = (1.5_rp/vcolo_in(8))**8.0_rp
   vinf = a_relp*(-viclo(4))/(1.0_rp + vaux1)
   if (cell_type == EXM_CELLTYPE_MID) then
      vinf = vinf*1.7_rp
   end if
   xitaux = btp/(1.0_rp + (0.0123_rp/vcolo_in(8)))

   if (xitaux < 0.001_rp) then    !fixed bug was wrongly 0.005
      xitaux = 0.001_rp   !fixed bug was wrongly 0.005
   end if
   vaux0 = vcolo_in(10)
   !vaux1 = (vinf - vaux0) * xitaux;
   !vaux2 = (vinf - (vaux0 + 0.5_rp * params % dtimon * vaux1)) * xitaux;
   !vaux3 = (vinf - (vaux0 + 0.5_rp * params % dtimon * vaux2)) * xitaux;
   !vaux4 = (vinf - (vaux0 + params % dtimon * vaux3)) * xitaux;
   !vaux5 = vaux0 + (params % dtimon / 6.0_rp) * (vaux1 + 2.0_rp * vaux2 + 2.0_rp * vaux3 + vaux4);
   !vaux3 = vaux5
   vaux3 = vinf - (vinf - vaux0)*exp(-params % dtimon/xitaux)
   vcolo_out(10) = vaux3      ! value of jrelp

   fjrelp = 1.0_rp/(1.0_rp + (params % kmcamk/viclo(22)))
   viclo(21) = (1.0_rp - fjrelp)*vcolo_out(9) + (fjrelp*vcolo_out(10)) !!! jrel

   !%calculate serca pump, ca uptake flux
   jupnp = (0.004375_rp*vcolo_in(1)/(vcolo_in(1) + 0.00092_rp))
   vaux1 = 1.0_rp/(vcolo_in(1) + 0.00092_rp - 0.00017_rp)
   jupp = 2.75_rp*0.004375_rp*vcolo_in(1)*vaux1
   if      (cell_type == EXM_CELLTYPE_EPI) then
      jupnp   = jupnp * 1.3_rp * material_params(3, 12)
      jupp    = jupp  * 1.3_rp * material_params(3, 12)
      HFjleak =                  material_params(3, 15)
   else if (cell_type == EXM_CELLTYPE_ENDO) then  !fixed this input 
      jupnp   = jupnp * material_params(1, 12)
      jupp    = jupp  * material_params(1, 12)
      HFjleak =         material_params(1, 15)
   else if (cell_type == EXM_CELLTYPE_MID) then
      jupnp   = jupnp * material_params(2, 12)
      jupp    = jupp  * material_params(2, 12)
      HFjleak =         material_params(2, 15)
   else 
      stop 1
   end if
   fjupp = (1.0_rp/(1.0_rp + params % kmcamk/viclo(22)))
   viclo(19) = (0.0039375_rp*vcolo_in(7)/15.0_rp )* HFjleak  !!!! jleak
   viclo(18) = (1.0_rp - fjupp)*jupnp + fjupp*jupp - viclo(19)  !!! jup

   !%calculate tranlocation flux
   viclo(20) = (vcolo_in(7) - vcolo_in(8))/100.0_rp  !!!! jtr

                  !!!!!  CONCENTRATIONS!!!!

   !%update intracellular concentrations, using buffers for cai, cass, cajsr
            !! calculate na current
   vaux1 = viclo(1) + viclo(2) + viclo(12)
   vaux2 = 3.0_rp*viclo(8) + 3.0_rp*viclo(10)
   vaux3 = -(vaux1 + vaux2)*params % acap/(params % farad*params % vmyo)
   rhsx = vaux3 + (viclo(16)*params % vss/params % vmyo)
   val0 = vcolo_in(5)
   nai = val0 + params % dtimon*rhsx
   !k1 = rhsx
   !k2 = rhsx + 0.5_rp * params % dtimon* k1
   !k3 = rhsx + 0.5_rp * params % dtimon * k2
   !k4 = rhsx + params % dtimon * k3
   !nai = val0 + (params % dtimon / 6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4)
   vcolo_out(5) = nai


            !!! calculate na current in subspace ss
   vaux1 = (viclo(24) + 3.0_rp*viclo(9))*params % acap/(params % farad*params % vss)
   rhsx = -vaux1 - viclo(16)
   val0 = vcolo_in(2)
   nass = val0 + params % dtimon*rhsx
   !k1 = rhsx
   !k2 = rhsx + 0.5_rp * params % dtimon* k1
   !k3 = rhsx + 0.5_rp * params % dtimon * k2
   !k4 = rhsx + params % dtimon * k3
   !nass = val0 + (params % dtimon / 6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4)
   vcolo_out(2) = nass             !!! nass

            !!! calculate k current
   vaux1 = viclo(3) + viclo(5) + viclo(6) + viclo(23)
   vaux2 = viclo(7) + viclo(11) - (2.0_rp*viclo(10))
   vaux3 = (viclo(17)*params % vss/params % vmyo)
   rhsx = -((vaux1 + vaux2)*params % acap/(params % farad*params % vmyo)) + vaux3
   val0 = vcolo_in(3)
   ki = val0 + params % dtimon*rhsx
   !k1 = rhsx
   !k2 = rhsx + 0.5_rp * params % dtimon* k1
   !k3 = rhsx + 0.5_rp * params % dtimon * k2
   !k4 = rhsx + params % dtimon * k3
   !ki = val0 + (params % dtimon / 6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4)
   vcolo_out(3) = ki             !!! ki

            !!!!  calculate k current in the subspace ss
   rhsx = -(viclo(25)*params % acap/(params % farad*params % vss)) - viclo(17)
   val0 = vcolo_in(4)
   kss = val0 + params % dtimon*rhsx
   !k1 = rhsx
   !k2 = rhsx + 0.5_rp * params % dtimon* k1
   !k3 = rhsx + 0.5_rp * params % dtimon * k2
   !k4 = rhsx + params % dtimon * k3
   !kss = val0 + (params % dtimon / 6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4)
   vcolo_out(4) = kss             !!! kss

            !!!  calculate ca current (Cai)

   if( .not. has_land ) then
      vaux1 = (params % kmcmdn + vcolo_in(1))*(params % kmcmdn + vcolo_in(1))
      vaux2 = (params % kmtrpn + vcolo_in(1))*(params % kmtrpn + vcolo_in(1))
      vaux3 = 1.0_rp/(1.0_rp + (params % cmdnmax*params % kmcmdn/vaux1) + (params % trpnmax*params % kmtrpn/vaux2))
      rhsx1 = viclo(14) + viclo(13) - (2.0_rp*viclo(8))
      rhsx2 = -(viclo(18)*params % vnsr/params % vmyo) + (viclo(15)*params % vss/params % vmyo)
   else 
      print *,"Land is not tested here"
      stop 1
      !disabled! vaux1 = (params % kmcmdn + vcolo_in(1))*(params % kmcmdn + vcolo_in(1))
      !disabled! vaux3 = 1.0_rp/(1.0_rp + (params % cmdnmax*params % kmcmdn/vaux1))
      !disabled! rhsx1 = viclo(14) + viclo(13) - (2.0_rp*viclo(8))
      !disabled! rhsx2 = -(viclo(18)*params % vnsr/params % vmyo) + (viclo(15)*params % vss/params % vmyo) - (params % k_TRPN*(((vcolo_in(1)*1000.0_rp / params % Ca50)**params % n_TRPN)*(1.0_rp-params % CaTRPN)-params % CaTRPN))*params % trpnmax
   end if
      
   rhsx = vaux3*(-(rhsx1*params % acap/(2.0_rp*params % farad*params % vmyo)) + rhsx2)
   val0 = vcolo_in(1)
   cai = val0 + params % dtimon*rhsx
   !k1 = rhsx
   !k2 = rhsx + 0.5_rp * params % dtimon* k1
   !k3 = rhsx + 0.5_rp * params % dtimon * k2
   !k4 = rhsx + params % dtimon * k3
   !cai = val0 + (params % dtimon / 6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4)
   vcolo_out(1) = cai             !!! cai

            !!!! calculate ca current in the subspace ss
   vaux1 = (params % kmbsr + vcolo_in(6))*(params % kmbsr + vcolo_in(6))
   vaux2 = (params % kmbsl + vcolo_in(6))*(params % kmbsl + vcolo_in(6))
   vaux3 = 1.0_rp/(1.0_rp + (params % bsrmax*params % kmbsr/vaux1) + (params % bslmax*params % kmbsl/vaux2)) !Bcass
   rhsx1 = viclo(4) - (2.0_rp*viclo(9))
   rhsx2 = viclo(21)*params % vjsr/params % vss
   rhsx = vaux3*(-rhsx1*params % acap/(2.0_rp*params % farad*params % vss) + rhsx2 - viclo(15))
   val0 = vcolo_in(6)
   cass = val0 + params % dtimon*rhsx
   !k1 = rhsx
   !k2 = rhsx + 0.5_rp * params % dtimon* k1
   !k3 = rhsx + 0.5_rp * params % dtimon * k2
   !k4 = rhsx + params % dtimon * k3
   !cass = val0 + (params % dtimon / 6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4)
   vcolo_out(6) = cass             !!! cass

            !!!! calculate ca current in the sarcoplasmic reticulum nsr
   rhsx1 = viclo(20)*params % vjsr/params % vnsr
   rhsx = viclo(18) - rhsx1
   val0 = vcolo_in(7)
   cansr = val0 + params % dtimon*rhsx
   !k1 = rhsx
   !k2 = rhsx + 0.5_rp * params % dtimon* k1
   !k3 = rhsx + 0.5_rp * params % dtimon * k2
   !k4 = rhsx + params % dtimon * k3
   !cansr = val0 + (params % dtimon / 6.0_rp) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4)
   vcolo_out(7) = cansr             !!! cansr

            !!!! calculate ca current in the junctional sarcoplasmic reticulum jsr
   vaux1 = (params % kmcsqn + vcolo_in(8))*(params % kmcsqn + vcolo_in(8))
   vaux3 = params % csqnmax*params % kmcsqn/vaux1
   vaux2 = 1.0_rp/(1.0_rp + vaux3)
   rhsx = vaux2*(viclo(20) - viclo(21))
   val0 = vcolo_in(8)
   cajsr = val0 + params % dtimon*rhsx
   !k1 = rhsx
   !k2 = rhsx + 0.5_rp * params % dtimon* k1
   !k3 = rhsx + 0.5_rp * params % dtimon * k2
   !k4 = rhsx + params % dtimon * k3
   !cajsr = val0 + ((params % dtimon / (6.0_rp)) *(k1 + 2.0_rp * k2 + 2.0_rp * k3 + k4))
   vcolo_out(8) = cajsr             !!! cajsr


   !disabled!if (has_land) then
   !disabled!   !------------------------------------------------------
   !disabled!   ! 
   !disabled!   !   Beginning of Land
   !disabled!   !
   !disabled!   !----------------------------------------------------
   !disabled!
   !disabled!   ! State variable dependent parameters (regularised parameters)
   !disabled!   if ((params % zeta_s + 1.0_rp) < 0.0_rp) then
   !disabled!      y_su = params % y_s*(-params % zeta_s - 1.0_rp)
   !disabled!   elseif ((params % zeta_s + 1.0_rp) > 1.0_rp) then
   !disabled!      y_su = params % y_s*params % zeta_s
   !disabled!   else
   !disabled!      y_su = 0.0_rp
   !disabled!   end if
   !disabled!   y_wu = params % y_w*ABS(params % zeta_w)
   !disabled!
   !disabled!   ! Define the RHS of the ODE system of Land
   !disabled!   ydot(1) = params % k_ws*params % W - params % k_su*params % S - y_su*params % S
   !disabled!   ydot(2) = params % k_uw*(1.0_rp - params % B - params % S - params % W) - params % k_wu*params % W - params % k_ws*params % W - y_wu*params % W
   !disabled!   ydot(3) = params % k_TRPN*(((vcolo_in(1)*1000.0_rp/params % Ca50)**params % n_TRPN)*(1.0_rp - params % CaTRPN) - params % CaTRPN)
   !disabled!   ydot(4) = params % k_u_2*MIN((params % CaTRPN**(-params % n_tm/2.0_rp)), 100.0_rp)*(1.0_rp - params % B - params % S - params % W) - params % k_u*(params % CaTRPN**(params % n_tm/2.0_rp))*params % B
   !disabled!   ydot(5) = params % A_s*params % lambda_rate - params % c_s*params % zeta_s
   !disabled!   ydot(6) = params % A_s*params % lambda_rate - params % c_w*params % zeta_w
   !disabled!
   !disabled!   ! Update variables of Land
   !disabled!   params % S = params % S + params % dtimon*ydot(1)
   !disabled!   params % W = params % W + params % dtimon*ydot(2)
   !disabled!   params % CaTRPN = params % CaTRPN + params % dtimon*ydot(3)
   !disabled!   params % B = params % B + params % dtimon*ydot(4)
   !disabled!   params % zeta_s = params % zeta_s + params % dtimon*ydot(5)
   !disabled!   params % zeta_w = params % zeta_w + params % dtimon*ydot(6)
   !disabled!   
   !disabled!   !------------------------------------------------------
   !disabled!   ! 
   !disabled!   !   End of Land
   !disabled!   !
   !disabled!   !----------------------------------------------------
   !disabled!end if 


end subroutine exm_ohara_execute_timestep_jazmin

   

   



end program unitt_exm_ohara
