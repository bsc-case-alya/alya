!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine boucar(&
     pnodb,pgaub,gbsha,deriv,weigb,bocod,gbsur,baloc)
  !-----------------------------------------------------------------------
  !****f* domain/boucar
  ! NAME
  !    boucar
  ! DESCRIPTION
  !    This routine calculates:
  !    GBSUR: Unit surface
  !    BALOC: Tangent system
  ! USES
  !    invmtx
  ! USED BY
  !    ***_elmope
  !    extnor
  ! SOURCE
  !-----------------------------------------------------------------------
  use def_parame, only     :  twopi
  use def_domain, only     :  ndime,ndimb,kfl_naxis
  use def_kintyp, only     :  ip,rp
  use mod_bouder
  implicit none
  integer(ip), intent(in)  :: pnodb,pgaub
  real(rp),    intent(in)  :: gbsha(pnodb,pgaub)
  real(rp),    intent(in)  :: deriv(ndimb,pnodb,pgaub)
  real(rp),    intent(in)  :: weigb(pgaub)
  real(rp),    intent(in)  :: bocod(ndime,pnodb)
  real(rp),    intent(out) :: gbsur(pgaub)
  real(rp),    intent(out) :: baloc(ndime,ndime)
  integer(ip)              :: igaub,inodb
  real(rp)                 :: eucta,gpcod

  if(ndime==1) then
     gbsur(1)   = 1.0_rp
     baloc(1,1) = 1.0_rp
  else
     do igaub=1,pgaub
        call bouder(&
             pnodb,ndime,ndimb,deriv(1,1,igaub),&
             bocod,baloc,eucta)
        gbsur(igaub)=weigb(igaub)*eucta 
     end do
     !
     ! Cylindrical coordinates
     !  
     if(kfl_naxis==1) then
        do igaub=1,pgaub
           gpcod=0.0_rp
           do inodb=1,pnodb
              gpcod=gpcod+bocod(1,inodb)*gbsha(inodb,igaub)
           end do
           gbsur(igaub)=gbsur(igaub)*gpcod*twopi
        end do
     end if

  end if
  
end subroutine boucar
