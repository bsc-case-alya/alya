!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_mat
  !
  ! Order strings
  !
  use def_kintyp_basic, only : ip,rp
  use def_master,       only : amatr
  use def_mat
  use def_mat_csr
  use def_mat_coo
  use def_mat_den

  implicit none

  type(mat_csr), pointer, contiguous :: c(:)
  type(mat_csr), pointer             :: a
  type(mat_csr), pointer             :: m
  real(rp),      pointer, contiguous :: bmatr(:)
  integer(ip),   pointer             :: ia(:),ja(:)
  integer(ip)                        :: ndof,nz,nrows,nn
  integer(ip)                        :: band
  real(rp)                           :: prof
  
  nrows = 0
  ndof  = 2
  nz    = 2
  allocate(amatr(ndof*ndof*nz))
  call popo(amatr)
  ndof  = 2
  nz    = 2
  allocate(bmatr(ndof*ndof*nz))
  call pop2(bmatr)
  
  !call asys % init()

  allocate(a)
  call a % init  ()
  call a % alloca((/nz,ndof,ndof,nrows/))
  call a % assign(amatr)

  allocate(m)
  call m % init()
  m % ndof1 = ndof
  m % ndof2 = ndof
  m % nz    = nz
  call m % alloca()
  call m % assign(bmatr)
  
  print*,'popo=',a % vA(1,1,1),a % vA(1,2,1),a % vA(2,1,1)
  print*,'popo=',m % vA(1,1,1),m % vA(1,2,1),m % vA(2,1,1)

  allocate(c(1))
  call c(1) % init()
  c % nrows = 2
  c % ncols = 2
  c(1) % vA(1:2,1:2,1:2) => amatr
  
  !allocate(d)
  !d % vA(1:1,1:1) => c
  !class type ( v => d % vA )
  !type is ( mat_csr ) 
  !   print*,v % nrows
  !end select
  !
  ! Check graphs
  !
  call a % deallo ()
  call a % init   ()
  a % nrows = 9
  a % nz    = 49
  call a % alloca ()

  allocate(ia(a % nrows+1))
  allocate(ja(a % nz))
  call fill_iaja(ia,ja)
  a % iA = ia
  a % jA = ja

  print*,'nrows,ncols=',a % get_nrows(),a % get_ncols()

  call a % bandwidth(band,prof)

  print*,'band,prof=',band,prof
  
end program unitt_mat

subroutine popo(amatr)

  use def_kintyp_basic, only : ip,rp
  real(rp), intent(inout) :: amatr(2,2,2)

  amatr(1,1,1) = 1.0_rp
  amatr(1,2,1) = 2.0_rp
  amatr(2,1,1) = 3.0_rp
  amatr(2,2,1) = 4.0_rp

  amatr(1,1,2) = 5.0_rp
  amatr(1,2,2) = 6.0_rp
  amatr(2,1,2) = 7.0_rp
  amatr(2,2,2) = 8.0_rp
  
end subroutine popo

subroutine pop2(amatr)

  use def_kintyp_basic, only : ip,rp
  real(rp), intent(inout) :: amatr(2,2,2)

  amatr(1,1,1) = -1.0_rp
  amatr(1,2,1) = -2.0_rp
  amatr(2,1,1) = -3.0_rp
  amatr(2,2,1) = -4.0_rp

  amatr(1,1,2) = -5.0_rp
  amatr(1,2,2) = -6.0_rp
  amatr(2,1,2) = -7.0_rp
  amatr(2,2,2) = -8.0_rp
  
end subroutine pop2

subroutine fill_iaja(ia,ja)
  use def_kintyp_basic, only : ip,rp
  integer(ip), intent(inout) :: ia(*)
  integer(ip), intent(inout) :: ja(*)

  ia(1) =  1
  ia(2) =  5
  ia(3) = 11
  ia(4) = 17
  ia(5) = 26
  ia(6) = 30
  ia(7) = 34
  ia(8) = 40
  ia(9) = 46
  ia(10) = 50

  ja( 1) = 1
  ja( 2) = 2
  ja( 3) = 3
  ja( 4) = 4
  ja( 5) = 1
  ja( 6) = 2
  ja( 7) = 3
  ja( 8) = 4
  ja( 9) = 5
  ja(10) = 8
  ja(11) = 1
  ja(12) = 2
  ja(13) = 3
  ja(14) = 4
  ja(15) = 6
  ja(16) = 7
  ja(17) = 1
  ja(18) = 2
  ja(19) = 3
  ja(20) = 4
  ja(21) = 5
  ja(22) = 6
  ja(23) = 7
  ja(24) = 8
  ja(25) = 9
  ja(26) = 2
  ja(27) = 4
  ja(28) = 5
  ja(29) = 8
  ja(30) = 3
  ja(31) = 4
  ja(32) = 6
  ja(33) = 7
  ja(34) = 3
  ja(35) = 4
  ja(36) = 6
  ja(37) = 7
  ja(38) = 8
  ja(39) = 9
  ja(40) = 2
  ja(41) = 4
  ja(42) = 5
  ja(43) = 7
  ja(44) = 8
  ja(45) = 9
  ja(46) = 4
  ja(47) = 7
  ja(48) = 8
  ja(49) = 9

end subroutine fill_iaja
