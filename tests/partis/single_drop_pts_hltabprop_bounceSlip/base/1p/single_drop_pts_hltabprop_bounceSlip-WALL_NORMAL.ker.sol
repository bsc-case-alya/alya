#
# -----------------
# ALGEBRAIC SOLVER:
# -----------------
# 
# TYPE OF SOLVER        : CONJUGATE GRADIENT
# --------------
# MINIMUM TOLERANCE     = 0.1000E-08
# MAX. ITERATIONS       =       5000
# ADAPTIVE RESIDUAL     : OFF
# NON-ZERO COEFF.       =       1792 (R*8), MEMORY=   14.00 kbytes
# FORMAT                : CSR
#
# PARALLELIZATION       : 
# ---------------
# SpMV MPI MATRIX       : PARTIAL ROW (SQUARE MATRIX)
# SpMV MPI COMMUNICAT.  : NON-BLOCKING
#
# PRECONDITIONER        : DIAGONAL diag(A)                                  
# --------------
# LEFT/RIGHT            : M^{-1/2} A M^{-1/2} M^{1/2} x = M^{-1/2} b
#
# --|  Columns displayed:
# --|  1. Number of iterations   2. Initial prec. residual   
# --|  3. Final prec. residual   4. Initial residual         
# --|  5. Final residual         6. Convergence rate         
# --|  7. CPU time               8. Orthogonality            
# --|  9. RHS norm ||b||        10. Precon. upd (1=yes,0=no) 
# --| 11. Cond. number kappa    12. Min. eigenvalue          
# --| 13. Max. eigenvalue                                    
# --|                                                        
# --| -999 Means was not calculated                          
# --| -888 Means was not converged                           
# --|                                                        
#                                                            
#     1                 2                 3                 4                 5                 6                 7                 8                 9            10                11                12                13
#
# Time step:        0, Outer iter:      0, Inner iter:      0
      3   0.47070799E+000   0.16720711E-011   0.60806634E+000   0.26618392E-011   0.99654431E+001   0.32901764E-004   0.14987349E-005   0.64331105E-001             1  -0.99900000E+003  -0.99900000E+003  -0.99900000E+003
# Time step:        0, Outer iter:      0, Inner iter:      0
      8   0.57785641E+000   0.33560291E-012   0.71729363E+000   0.41616761E-012   0.50413998E+001   0.51975250E-004   0.30162756E-004   0.68741047E+000             1  -0.99900000E+003  -0.99900000E+003  -0.99900000E+003
# Time step:        0, Outer iter:      0, Inner iter:      0
      8   0.57785641E+000   0.40874326E-013   0.71729363E+000   0.49196069E-013   0.59557749E+001   0.52213669E-004  -0.16914891E-003   0.68741047E+000             1  -0.99900000E+003  -0.99900000E+003  -0.99900000E+003
