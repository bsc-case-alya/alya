!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Solmem
!> @{
!> @file    Solmem.f90
!> @author  houzeaux
!> @date    2019-06-17
!> @brief   Solver memory
!> @details Compute solver requirements
!> @} 
!-----------------------------------------------------------------------

subroutine Solmem()

  use def_kintyp,             only : ip
  use def_kintyp,             only : lg
  use def_master,             only : iblok
  use def_master,             only : nblok
  use def_master,             only : modul
  use def_master,             only : mmodu
  use def_master,             only : momod
  use def_master,             only : kfl_modul
  use def_master,             only : ITASK_SOLMEM
  use def_domain,             only : nzmat,nzrhs
  use mod_output_postprocess, only : output_postprocess_allocate_sets_and_witness
  use mod_moduls,             only : moduls
  use mod_solver,             only : solver_matrix_RHS_sizes
  
  implicit none
  integer(ip) :: ivari,imodu

  call Kermod(ITASK_SOLMEM)
  do iblok = 1,nblok
     call moduls(ITASK_SOLMEM)
  end do
  modul = 0
  !
  ! Postprocess NOT GOOD, CHECK ORDER OF THINGS... put open file in turnon after moddef...
  !
  do imodu = 1,mmodu
     if( kfl_modul(imodu) /= 0 ) then
        call output_postprocess_allocate_sets_and_witness(imodu,momod(imodu) % postp(1))
     end if
  end do
  !
  ! Matrix and RHS sizes
  !
  do imodu = 1,mmodu
     if( kfl_modul(imodu) /= 0 ) then
        if( associated(momod(imodu) % solve) ) then
           do ivari = 1,size(momod(imodu) % solve,KIND=ip)
              call solver_matrix_RHS_sizes(momod(imodu) % solve(ivari),nzmat,nzrhs)
           end do
        end if
     end if
  end do
  !
  ! Allocate memory for all unknowns of the problem and coefficients
  !
  call memunk(1_ip)

end subroutine Solmem
