!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Turnon
!> @{
!> @file    Reamod.f90
!> @author  houzeaux
!> @date    2019-06-12
!> @brief   Read module data
!> @details Read module data
!> @} 
!-----------------------------------------------------------------------

subroutine Reamod()
  
  use def_kintyp,   only : ip
  use def_master,   only : ITASK_TURNON
  use mod_messages, only : messages_live
  use mod_moduls,   only : moduls

  implicit none
  !
  ! Read modules
  !
  call messages_live('MODULE DATA','START SECTION')
  call moduls(ITASK_TURNON)
  call messages_live('MODULE DATA','END SECTION')
  !
  ! Close module data files and open occasional output files
  !  
  call moddef(2_ip)

end subroutine Reamod
