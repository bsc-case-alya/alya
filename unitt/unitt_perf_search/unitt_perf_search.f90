!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_perf_search

  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_search_method
  use def_maths_bin
  use def_maths_tree
  use def_maths_skdtree  
  use def_master
  use mod_memory
  use def_kermod
  use def_domain
  use def_interpolation_method
  use mod_maths
  
  implicit none
  type(maths_bin)                 :: bin
  type(maths_octree)              :: oct
  type(maths_skdtree)             :: kdt
  type(maths_skd_tree)            :: skdt

  integer(ip)                     :: ii,jj
  real(rp),             pointer   :: bobox(:,:,:)
  real(rp),             pointer   :: bbbox(:,:,:)
  real(rp),             pointer   :: xx(:,:)
  real(rp),             pointer   :: xx2(:,:)  
  type(i1p),            pointer   :: list_entities(:)    
  type(i1p),            pointer   :: list_entities_2(:)    
  type(interpolation)             :: interp
  
  real(rp),             pointer   :: vv1(:,:)  
  real(rp),             pointer   :: vv2(:,:)  

  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction

  nullify(bobox,bbbox,list_entities,list_entities_2,xx)

  allocate(xx(ndime,npoin))
  call meshe(ndivi) % element_bb (bobox)
  call meshe(ndivi) % boundary_bb(bbbox)

  allocate(xx2(ndime,1))
  allocate(vv1(ndime,1))
  allocate(vv2(ndime,1))
  xx2(:,1) = (/ -5.0_rp,5.0_rp,5.0_rp /)
  xx2(:,1) = (/ -5.0_rp,5.0_rp,4.0_rp /)
  vv1 =-3.0_rp
  vv2 = 3.0_rp
  !
  ! OCTREE
  !
  print*,'------------------------------------'
  call oct % init      ()
  call oct % input     (limit=100_ip)
  call oct % fill      (BOBOX=bobox)
  call oct % candidate (coord,list_entities)

  print*,'graph'
  call oct % graph    (SEARCH_MESH_ALL)


  print*,'oct % fill=      ',oct % times(1)
  print*,'oct % max elem=  ',int(oct % stats(4),ip)

  call interp % init      ()
  call interp % input     (oct)
  call interp % preprocess(coord,meshe(ndivi))
  call interp % values    (coord,xx)
  call interp % deallo    ()
  call oct    % deallo    (list_entities)

  print*,'interp, candidate=      ',interp % times(1)
  print*,'interp, element search= ',interp % times(2)
  print*,'interp, values=         ',interp % times(3)
  !
  ! BIN
  !
  print*,'------------------------------------'
  call bin % init      ()
  call bin % input     (limit=5_ip,MAX_BINS=1000000_ip)
  call bin % fill      (BOBOX=bobox)
  
  print*,'boxes=           ',bin % boxip
  print*,'ave points/bin=  ',bin % stats(1)
  print*,'bin % fill=      ',bin % times(1)
  print*,'bin % max elem=  ',int(bin % stats(4),ip)

  call interp % init      ()
  call interp % input     (bin)
  call interp % preprocess(coord,meshe(ndivi))
  call interp % values    (coord,xx)
  call interp % deallo    ()
  
  print*,'interp, candidate=      ',interp % times(1)
  print*,'interp, element search= ',interp % times(2)
  print*,'interp, values=         ',interp % times(3)
  !
  ! KDTREE
  !
  print*,'------------------------------------'
  call kdt % init      ()
  call kdt % input     ()
  call kdt % fill      (BOBOX=bbbox)
  call kdt % candidate (xx2,list_entities,METHOD=CANDIDATE_NEAREST)
  
  print*,'kdt % fill=      ',kdt % times(1)
  print*,'kdt % candidate= ',kdt % times(2)

  call interp % init      ()
  call interp % input     (kdt,INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION)
  call interp % preprocess(xx2,meshe(ndivi))
  call interp % values    (coord,vv1)
  call interp % deallo    ()
  !
  ! SKDTREE
  !
  print*,'------------------------------------'
  call skdt % init      ()
  call skdt % input     ()
  call skdt % fill      (BOBOX=bbbox)
  call skdt % candidate (xx2,list_entities_2,METHOD=CANDIDATE_NEAREST)

  print*,'skdt % fill=      ',skdt % times(1)
  print*,'skdt % candidate= ',skdt % times(2)
  
  call interp % init      ()
  call interp % input     (skdt,INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION)
  call interp % preprocess(xx2,meshe(ndivi))
  call interp % values    (coord,vv2)
  call interp % deallo    ()

  print*,'------------------------------------'
  ii = int(size(list_entities  (1) % l),ip)
  jj = int(size(list_entities_2(1) % l),ip)
  call heapsorti1(2_ip,ii,list_entities  (1) % l)
  call heapsorti1(2_ip,jj,list_entities_2(1) % l)
  print*,'vv1=',vv1
  print*,'vv2=',vv2
  print*,'ll1=',list_entities(1) % l
  print*,'ll2=',list_entities_2(1) % l
  if( dot_product(vv1(:,1)-vv2(:,1),vv1(:,1)-vv2(:,1)) > 1.0e-6_rp ) then
     print*,'oups'
     stop 1
  end if
  !
  ! Extensive comparison between both kdtrees
  !
  print*,'------------------------------------'
  print*,'Interpolate over whole mesh'
  deallocate(vv1)
  deallocate(vv2)
  allocate(vv1(ndime,meshe(ndivi) % npoin))
  allocate(vv2(ndime,meshe(ndivi) % npoin))
  vv1 = 5.0_rp
  vv2 = 5.0_rp
  
  call kdt    % deallo    ()  
  call skdt   % deallo    ()

  print*,'kdt tree --------------------------'
  
  call kdt    % init      ()
  call kdt    % input     ()
  call kdt    % fill      (BOBOX=bbbox)
  call interp % init      ()
  call interp % input     (kdt,INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION)
  call interp % preprocess(coord,meshe(ndivi))
  call interp % values    (coord,vv1)
  call interp % deallo    ()

  print*,'kdt % fill=      ',kdt % times(1)
  print*,'kdt % candidate= ',kdt % times(2)

  call kdt    % deallo    ()
  
  print*,'skdt tree -------------------------'
  call skdt   % init      ()
  call skdt   % input     ()
  call skdt   % fill      (BOBOX=bbbox)
  call interp % init      ()
  call interp % input     (skdt,INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION)
  call interp % preprocess(coord,meshe(ndivi))
  call interp % values    (coord,vv2)
  call interp % deallo    ()

  print*,'skdt % fill=      ',skdt % times(1)
  print*,'skdt % candidate= ',skdt % times(2)
  
  call skdt   % deallo    ()
  
  print*,'------------------------------------'
  do ii = 1,meshe(ndivi) % npoin
     if( dot_product(vv1(:,ii)-vv2(:,ii),vv1(:,ii)-vv2(:,ii)) > 1.0e-6_rp ) then
        print*,'ii    =',ii
        print*,'coord =',coord(:,ii)
        print*,'vv1   =',vv1(:,ii)
        print*,'vv2   =',vv2(:,ii)
        stop 2
     end if
  end do
  
  call runend('O.K.!')

end program unitt_perf_search
