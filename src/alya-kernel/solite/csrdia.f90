!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine csrdia(ipoin,kfl_symme,izmat)
  !-----------------------------------------------------------------------
  !****f* master/csrdia
  ! NAME 
  !    csrdia
  ! DESCRIPTION
  !    Diagonal
  ! USES
  !    memchk
  !    mediso
  ! USED BY
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp,lg
  use def_domain, only       :  r_sym,r_dom,c_dom
  use def_master, only       :  INOTMASTER
  implicit none
  integer(ip), intent(in)    :: ipoin,kfl_symme
  integer(ip), intent(out)   :: izmat
  logical(lg)                :: ifoun

  if( INOTMASTER ) then

     if( kfl_symme == 1 ) then
       izmat = r_sym(ipoin+1)-1

     else
        izmat = r_dom(ipoin)-1
        ifoun = .true.
        do while( ifoun )
           izmat = izmat + 1
           if( c_dom(izmat) == ipoin ) ifoun = .false.
        end do
        
     end if

  end if
 
end subroutine csrdia
