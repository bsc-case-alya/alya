!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_interpolation

  use def_kintyp
  use def_master
  use def_domain
  use mod_elmgeo
  use def_maths_bin
  use def_kermod
  use def_search_method
  use mod_parall
  use def_interpolation_method
  use mod_memory_basic
  use mod_maths_geometry
  implicit none
  real(rp),    parameter :: eps = 1.0e-10_rp
  real(rp),    pointer   :: bobox(:,:,:)
  integer(ip)            :: pelty,ielem,inode,ipoin,ii
  type(maths_bin)        :: bin_seq
  real(rp),    pointer   :: xx(:,:),vv(:,:),dd(:,:)
  real(rp),    pointer   :: xcoor(:)
  real(rp)               :: xp(2),dista(2),temp
  type(interpolation)    :: interp
  
  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction


  nullify(bobox)
  nullify(xcoor)
  nullify(xx)
  nullify(vv)
  nullify(dd)
  
  allocate(xx(2,3))
  allocate(vv(2,3))
  allocate(dd(2,3))
  
  xx(1,1) = 0.2_rp
  xx(2,1) = 0.2_rp
  xx(1,2) = 0.5_rp
  xx(2,2) = 0.5_rp
  xx(1,3) = 0.8_rp
  xx(2,3) = 0.8_rp
  !
  ! Sequential search method
  !
  call meshe(ndivi) % element_bb(bobox)
  call bin_seq % init  ()
  call bin_seq % input (boxes=(/10_ip,10_ip,10_ip/)) 
  call bin_seq % fill  (BOBOX=bobox)
  !
  ! Interpolation
  !
  call interp % init      ()
  call interp % input     (bin_seq,DERIVATIVES=.true.)
  call interp % preprocess(xx,meshe(ndivi))
  !
  ! Values
  !
  print*,'test 1'
  call interp % values(meshe(ndivi) % coord,vv)
  do ii = 1,memory_size(xx,2_ip)
     if( abs(xx(1,ii)-vv(1,ii)) > eps ) then
        print*,'error 1=',xx(1,ii),vv(1,ii)
        stop 1
     end if
     if( abs(xx(2,ii)-vv(2,ii)) > eps ) then
        print*,'error 2=',xx(2,ii),vv(2,ii)
        stop 2
     end if
  end do
  !
  ! Value on single point
  !
  print*,'test 2'
  call interp % values(meshe(ndivi) % coord,vv,POINT=2_ip)
  ii = 2
  if( abs(xx(1,ii)-vv(1,1)) > eps ) then
     print*,'error 1=',xx(1,ii),vv(1,1)
     stop 3
  end if
  if( abs(xx(2,ii)-vv(2,1)) > eps ) then
     print*,'error 2=',xx(2,ii),vv(2,1)
     stop 4
  end if
  !
  ! Derivatives
  !
  allocate(xcoor(npoin))
  do ipoin = 1,npoin
     xcoor(ipoin) = meshe(ndivi)%coord(1,ipoin)
  end do
  print*,'test 3'
  call interp % derivatives(xcoor,dd)
  do ii = 1,memory_size(xx,2_ip)
     if( abs(dd(1,ii)-1.0_rp) > eps ) then
        print*,'error 3=',dd(1,ii),1.0_rp
        stop 5
     end if
  end do
  do ipoin = 1,npoin
     xcoor(ipoin) = meshe(ndivi)%coord(2,ipoin)
  end do  
  call interp % derivatives(xcoor,dd)
  do ii = 1,memory_size(xx,2_ip)
     if( abs(dd(2,ii)-1.0_rp) > eps ) then
        print*,'error 4',dd(2,ii),1.0_rp
        stop 6
     end if
  end do

  call bin_seq % deallo()
  call interp  % deallo()

  print*,'memory int=',interp  % memor(1)
  print*,'memory bin=',bin_seq % memor(1)
  if( bin_seq % memor(1) /= 0_8 ) then
     print*,'memor seq=',bin_seq % memor(1) ; stop 7
  end if
  if( interp  % memor(1) /= 0_8 ) then
     print*,'memor int=',interp  % memor(1) ; stop 8
  end if

  !bobox(1,1,1) =  0.0_rp
  !bobox(1,2,1) =  0.0_rp
  !bobox(2,1,1) =  1.0_rp
  !bobox(2,2,1) =  1.0_rp
  !xp = (/0.5_rp,0.5_rp/)
  !dista = maths_min_max_box_vertices(xp,bobox(1:2,1:2,1))
  !print*,dista(1),dista(2)

  !call runend('O.K.!')
  call runend('O.K.!')
  
end program unitt_interpolation
 
