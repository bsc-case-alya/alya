!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_solidz_maths_simd
   use def_kintyp_basic, only : ip,rp,lg
   use def_domain,       only : ndime
   use mod_sld_vect_maths

   implicit none

   real(rp),    parameter :: Inn(3,3) = reshape( [ 1.0_rp, 0.0_rp, 0.0_rp  , &
                                                   0.0_rp, 1.0_rp, 0.0_rp  , &
                                                   0.0_rp, 0.0_rp, 1.0_rp ], &
                                                  [ 3, 3 ] )

#ifdef VECTOR_SIZE
#define DVS VECTOR_SIZE

   logical(ip) :: ierror
   real(rp)    :: SM(DVS), SR(DVS)
   real(rp)    :: T1A(DVS,3), T1B(DVS,3), T1M(DVS,3), T1R(DVS,3)
   real(rp)    :: T2A(DVS,3,3), T2B(DVS,3,3), T2C(DVS,3,3), T2M(DVS,3,3), T2R(DVS,3,3)
   real(rp)    :: T4M(DVS,3,3,3,3), T4R(DVS,3,3,3,3)
   real(rp)    :: T1AUX(3), T2AUX(3,3)
   integer(ip) :: iv, ii

   ! Fix ndime
   ndime = 3_ip

   ! Fill random tensors
   ! - 1st order
   call random_number(T1A)
   call random_number(T1B)
   ! - 2n order
   call random_number(T2A)
   call random_number(T2B)
   ! - impose zero 
   T2A(1,1:3,1:3) = 0.0_rp
   T2B(1,1:3,1:3) = 0.0_rp
   ! - impose identity
   do ii = 1, 3
      T2A(2,ii,ii) = 1.0_rp
      T2B(2,ii,ii) = 1.0_rp
   enddo


   write(*,*) 'Trace'
   ! - manufactued solution
   SM = 0.0_rp
   do iv = 1, DVS; do ii = 1, 3
      SM(iv) = SM(iv) + T2A(iv,ii,ii) 
   enddo; enddo
   ! - computed solution
   SR = vmath_TRACE(T2A)
   ! - check 
   ierror = compare(int(DVS,ip),SM,SR)
   

   write(*,*) 'Dot Product : MxV'
   ! - manufactued solution
   T1M = 0.0_rp
   do iv = 1, DVS
      T1M(iv,1:3) = matmul(T2A(iv,1:3,1:3),T1A(iv,1:3))
   enddo
   ! - computed solution
   T1R = vmath_MxV(T2A,T1A)
   ! - check
   ierror = compare(int(DVS,ip)*3_ip,T1M,T1R)


   write(*,*) 'Dot Product : TxV'
   ! - manufactued solution
   T1M = 0.0_rp
   do iv = 1, DVS
      T2AUX(1:3,1:3) = transpose(T2A(iv,1:3,1:3))
      T1M(iv,1:3) = matmul(T2AUX(1:3,1:3),T1A(iv,1:3))
   enddo
   ! - computed solution
   T1R = vmath_TxV(T2A,T1A)
   ! - check
   ierror = compare(int(DVS,ip)*3_ip,T1M,T1R)


   write(*,*) 'Dot Product : VxMxV'
   ! - manufactued solution
   SM = 0.0_rp
   do iv = 1, DVS
      T1AUX(1:3) = matmul(T2A(iv,1:3,1:3),T1B(iv,1:3))
      SM(iv) = dot_product(T1A(iv,1:3),T1AUX(1:3))
   enddo
   ! - computed solution
   SR = vmath_VxMxV(T1A,T2A,T1B)
   ! - check
   ierror = compare(int(DVS,ip),SM,SR)


   write(*,*) 'Matrix Multiplication : MxM'
   ! - manufactued solution
   T2M = 0.0_rp
   do iv = 1, DVS
      T2M(iv,1:3,1:3) = matmul(T2A(iv,1:3,1:3),T2B(iv,1:3,1:3))
   enddo
   ! - computed solution
   T2R = vmath_AxB(T2A,T2B)
   ! - check
   ierror = compare(int(DVS,ip)*9_ip,T2M,T2R)


   write(*,*) 'Matrix Multiplication : TxM'
   ! - manufactued solution
   T2M = 0.0_rp
   do iv = 1, DVS
      T2AUX(1:3,1:3) = transpose(T2A(iv,1:3,1:3))
      T2M(iv,1:3,1:3) = matmul(T2AUX(1:3,1:3),T2B(iv,1:3,1:3))
   enddo
   ! - computed solution
   T2R = vmath_TxM(T2A,T2B)
   ! - check
   ierror = compare(int(DVS,ip)*9_ip,T2M,T2R)


   write(*,*) 'Matrix Multiplication : MxT'
   ! - manufactued solution
   T2M = 0.0_rp
   do iv = 1, DVS
      T2AUX(1:3,1:3) = transpose(T2B(iv,1:3,1:3))
      T2M(iv,1:3,1:3) = matmul(T2A(iv,1:3,1:3),T2AUX(1:3,1:3))
   enddo
   ! - computed solution
   T2R = vmath_MxT(T2A,T2B)
   ! - check
   ierror = compare(int(DVS,ip)*9_ip,T2M,T2R)


   write(*,*) 'Outter product : VxV'
   ! - manufactured solution
   T2M = 0.0_rp
   do iv = 1, DVS
      T2M(iv,1:3,1:3) = get_outproduct_vxv_3(T1A(iv,1:3),T1B(iv,1:3))
   enddo
   ! - computed solution
   T2R = vmath_OUT_VxV(T1A,T1B)
   ! - check
   ierror = compare(int(DVS,ip)*9_ip,T2M,T2R) 


   write(*,*) 'Outter product : MxM'
   ! - manufactured solution
   T4M = 0.0_rp
   do iv = 1, DVS
      T4M(iv,1:3,1:3,1:3,1:3) = get_outproduct_MxM_3(T2A(iv,1:3,1:3),T2B(iv,1:3,1:3))
   enddo
   ! - computed solution
   T4R = vmath_OUT_MxM(T2A,T2B)
   ! - check
   ierror = compare(int(DVS,ip)*81_ip,T4M,T4R)


   write(*,*) 'Determinant : 3x3'
   ! - manufactued solution
   SM = 0.0_rp
   do iv = 1, DVS
      SM(iv) = get_det_3x3(T2A(iv,1:3,1:3)) 
   enddo
   ! - computed solution
   SR = vmath_DET(ndime,T2A)
   ! - check
   ierror = compare(int(DVS,ip),SM,SR)


   write(*,*) 'Inverse : 3x3'
   ! - manufactued solution
   T2M = 0.0_rp
   do iv = 2, DVS
      T2M(iv,1:3,1:3) = Inn(1:3,1:3) 
   enddo
   ! - computed solution
   T2C = vmath_INV(ndime,T2A)
   ! - check -> inv(M) * M = I
   T2R = vmath_AxB(T2C,T2A)
   ierror = compare(int(DVS,ip),T2M,T2R)

#else
   print*,'NOT TESTED BECAUSE VECTOR_SIZE = 1'
   stop 0
#endif


   contains

      function compare(n,A,B) result(equal)
         implicit none
         integer(ip), intent(in) :: n
         real(rp),    intent(in) :: A(*), B(*)
         logical(lg)             :: equal
         integer(ip)             :: ii
         real(rp), parameter     :: tol = 1.0e-10_rp
         equal = .true.
         do ii = 1, n
             if( abs(A(ii) - B(ii)) > tol )then
                equal = .false.
                print*,ii,A(ii),B(ii),abs(A(ii) - B(ii))
             endif
         enddo
         if( .not. equal ) stop 1
      end function compare 

      function get_det_3x3(A) result(det)
         implicit none
         real(rp), intent(in) :: A(3,3)
         real(rp)             :: det, t1, t2, t3
         t1 = A(2,2) * A(3,3) - A(3,2) * A(2,3)
         t2 = A(2,1) * A(3,3) - A(3,1) * A(2,3)
         t3 = A(2,1) * A(3,2) - A(3,1) * A(2,2)
         det = A(1,1) * t1 - A(1,2) * t2 + A(1,3) * t3
      end function get_det_3x3

      function get_outproduct_vxv_3(u,v) result(M)
         implicit none
         real(rp), intent(in) :: u(3), v(3)
         real(rp)             :: M(3,3)
         M(1,1) = u(1) * v(1)
         M(2,1) = u(2) * v(1)
         M(3,1) = u(3) * v(1)
         M(1,2) = u(1) * v(2)
         M(2,2) = u(2) * v(2)
         M(3,2) = u(3) * v(2)
         M(1,3) = u(1) * v(3)
         M(2,3) = u(2) * v(3)
         M(3,3) = u(3) * v(3)  
      end function get_outproduct_vxv_3


      function get_outproduct_MxM_3(A,B) result(R)
         implicit none
         real(rp), intent(in) :: A(3,3), B(3,3)
         real(rp)             :: R(3,3,3,3)
         integer(ip)          :: ii, jj, kk, ll
         do ii = 1, 3; do jj = 1, 3; do kk = 1, 3; do ll = 1, 3
            R(ii,jj,kk,ll) = A(ii,jj) * B(kk,ll)
         enddo; enddo; enddo; enddo
      end function get_outproduct_MxM_3

end program unitt_solidz_maths_simd
