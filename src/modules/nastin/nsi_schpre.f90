!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_schpre()
  !----------------------------------------------------------------------
  !****f* Nastin/nsi_schpre
  ! NAME 
  !    nsi_schpre
  ! DESCRIPTION
  !    This routine imposes boundary conditions on the preconditioner
  ! USES
  ! USED BY
  !    nsi_solbgs
  !***
  !----------------------------------------------------------------------
  use def_master
  use def_domain
  use def_nastin
  use def_solver
  use mod_memchk
  implicit none
  integer(ip) :: icomp,ipoin

  if( IMASTER ) return

  !----------------------------------------------------------------------
  !
  ! Compute preconditioner
  !
  !----------------------------------------------------------------------

  if( kfl_predi_nsi==5 ) then
     !
     ! Lumped Mass 
     !
     call runend('NSI_SCHPRE: NOT CODED')
     if( solve(2)%kfl_symme==1 ) then
        do ipoin=1,npoin
           icomp=r_sym(ipoin+1)-1 
           !lapla_nsi(icomp)=vmass(ipoin)/visco_nsi(1,1)
        end do
     else
        do ipoin=1,npoin
           icomp=r_dom(ipoin)
           do while(c_dom(icomp)/=ipoin)
              icomp=icomp+1
           end do
           !lapla_nsi(icomp)=vmass(ipoin)/visco_nsi(1,1)
        end do
     end if
  end if

end subroutine nsi_schpre

