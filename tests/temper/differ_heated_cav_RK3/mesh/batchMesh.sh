#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



gmsh -2 mesh2d.geo
gmsh2alya.pl mesh2d -bcs=boundaries
sed -i -e '1d; $d' mesh2d.fix.bou 
mv mesh2d.geo.dat mesh2d.geo.dat_unchecked
python InviertePorJacobianoTriQuad_v4.py mesh2d.geo.dat_unchecked mesh2d.geo.dat
rm mesh2d.geo.dat_unchecked
cp mesh2d.dims.dat ..
cp mesh2d.geo.dat ..
cp mesh2d.fix.bou ..
