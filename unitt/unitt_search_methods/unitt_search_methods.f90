!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_search_methods
  !
  ! Test copy, extract and merge operators
  !
  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_maths_bin
  use def_maths_tree
  use def_maths_octbin
  use def_master
  use mod_memory
  implicit none

  real(rp),             parameter :: epsil = 1.0e-12_rp
  type(mesh_type_basic)           :: mesh
  type(mesh_type_basic)           :: mesh_out
  type(maths_bin)                 :: bin
  type(maths_octree)              :: oct
  type(maths_octbin)              :: octbin
  type(maths_skdtree)             :: skdtree
  integer(ip)                     :: ipoin,idime,ielem,inode,iboun
  integer(ip)                     :: pelty,ii
  real(rp)                        :: dista,xx_test(2)
  integer(8)                      :: count_rate8
  integer(8)                      :: memor_loc(2)
  integer(8)                      :: memor_tmp(2)
  type(i1p),            pointer   :: list_entities(:)    
  real(rp),             pointer   :: bobox(:,:,:)
  real(rp),             pointer   :: shapf(:,:),xx(:,:)
  integer(ip),          pointer   :: lelem(:)
  real(rp),             pointer   :: res(:,:)
  character(len=5),     pointer   :: names(:)

  memor_loc = 0_8
  memor_tmp = 0_8
  nullify(bobox,list_entities,shapf,lelem,xx,res,names)
  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)
  !
  ! Create mesh
  !
  call elmgeo_element_type_initialization()

  call mesh % init('MY_MESH')
  
  mesh % nelem = 9
  mesh % npoin = 16
  mesh % ndime = 2
  mesh % mnode = 4
  
  call mesh % alloca()

  mesh % lnods(:,1)  = (/  12 ,7 ,5 ,10  /)
  mesh % lnods(:,2)  = (/  14 ,9 ,7 ,12   /)
  mesh % lnods(:,3)  = (/  16, 15, 9 ,14  /) 
  mesh % lnods(:,4)  = (/  7, 4 ,2 ,5   /)
  mesh % lnods(:,5)  = (/  9 ,8 ,4 ,7  /) 
  mesh % lnods(:,6)  = (/  15, 13, 8, 9 /)  
  mesh % lnods(:,7)  = (/  4 ,3 ,1 ,2  /) 
  mesh % lnods(:,8)  = (/  8 ,6 ,3 ,4  /) 
  mesh % lnods(:,9)  = (/  13 ,11, 6, 8  /)

  mesh % ltype(1:9)  = QUA04

  mesh % coord(:,1 ) = (/ 0.000000e+00_rp , 1.000000e+00_rp /) 
  mesh % coord(:,2 ) = (/ 0.000000e+00_rp , 6.666667e-01_rp /) 
  mesh % coord(:,3 ) = (/ 3.333333e-01_rp , 1.000000e+00_rp /) 
  mesh % coord(:,4 ) = (/ 3.333333e-01_rp , 6.666667e-01_rp /) 
  mesh % coord(:,5 ) = (/ 0.000000e+00_rp , 3.333333e-01_rp /) 
  mesh % coord(:,6 ) = (/ 6.666667e-01_rp , 1.000000e+00_rp /) 
  mesh % coord(:,7 ) = (/ 3.333333e-01_rp , 3.333333e-01_rp /) 
  mesh % coord(:,8 ) = (/ 6.666667e-01_rp , 6.666667e-01_rp /) 
  mesh % coord(:,9 ) = (/ 6.666667e-01_rp , 3.333333e-01_rp /) 
  mesh % coord(:,10) = (/ 0.000000e+00_rp , 0.000000e+00_rp /) 
  mesh % coord(:,11) = (/ 1.000000e+00_rp , 1.000000e+00_rp /) 
  mesh % coord(:,12) = (/ 3.333333e-01_rp , 0.000000e+00_rp /) 
  mesh % coord(:,13) = (/ 1.000000e+00_rp , 6.666667e-01_rp /) 
  mesh % coord(:,14) = (/ 6.666667e-01_rp , 0.000000e+00_rp /) 
  mesh % coord(:,15) = (/ 1.000000e+00_rp , 3.333333e-01_rp /) 
  mesh % coord(:,16) = (/ 1.000000e+00_rp , 0.000000e+00_rp /)
  !
  !call mesh % output(10_ip)
  !
  ! Element bounding boxes
  !
  call mesh % element_bb(bobox)

  !----------------------------------------------------------------------
  !
  ! BIN
  ! 
  !----------------------------------------------------------------------
  
  print*,'-----------------------------------------------------'
  print*,'BIN'
  
  allocate(xx(mesh % ndime,3))
  xx(:,1) = (/ 0.5_rp,0.5_rp /)
  xx(:,2) = (/ 0.1_rp,0.1_rp /)
  xx(:,3) = (/ 0.9_rp,0.9_rp /)
  !
  ! Look for candidates
  !
  call bin  % init  ()
  call bin  % input (boxes=(/2_ip,2_ip/)) 
  call bin  % fill  (BOBOX=bobox)
  call bin  % candidate(xx,list_entities)
  
  print*,'BIN CANDIDATE POINT 1=',list_entities(1) % l
  print*,'BIN CANDIDATE POINT 2=',list_entities(2) % l
  print*,'BIN CANDIDATE POINT 3=',list_entities(3) % l
  call bin  % deallo(list_entities)
  !
  ! Find elements
  !
  call bin  % init  ()
  call bin  % input (boxes=(/2_ip,2_ip/)) 
  call bin  % fill  (BOBOX=bobox)
  call bin  % candidate(xx,list_entities)
  call bin  % deallo(list_entities)
!!$  allocate(lelem(3),shapf(mesh % mnode,3))
!!$  call bin % search_element(xx,mesh,lelem,shapf)
!!$  !
!!$  ! Check interpolation
!!$  !
!!$  do ii = 1,size(xx,2)
!!$     xx_test = 0.0_rp
!!$     ielem = lelem(ii)
!!$     pelty = mesh % ltype(ielem)
!!$     do inode = 1,element_type(pelty) % number_nodes
!!$        ipoin = mesh % lnods(inode,ielem)
!!$        xx_test(1:2) = xx_test(1:2) + shapf(inode,ii) * mesh % coord(1:2,ipoin)
!!$     end do
!!$     print*,'coordinates= ',abs(xx_test(1)-xx(1,ii)),abs(xx_test(2)-xx(2,ii))
!!$     if( abs(xx_test(1)-xx(1,ii)) > epsil ) then
!!$        print*,'wrong interpolation 1: ',xx_test(1),xx(1,ii)
!!$        stop 1        
!!$     end if
!!$     if( abs(xx_test(2)-xx(2,ii)) > epsil ) then
!!$        print*,'wrong interpolation 2: ',xx_test(2),xx(2,ii)
!!$        stop 1       
!!$     end if
!!$  end do
!!$  deallocate(lelem,shapf)
  
  !----------------------------------------------------------------------
  !
  ! OCTREE
  ! 
  !----------------------------------------------------------------------
  
  print*,'-----------------------------------------------------'
  print*,'OCTREE'
  
  call oct  % init  ()
  call oct  % input (limit=2_ip)
  call oct  % fill  (BOBOX=bobox)
  call oct  % candidate(xx,list_entities)

  print*,'OCT CANDIDATE POINT 1=',list_entities(1) % l
  print*,'OCT CANDIDATE POINT 2=',list_entities(2) % l
  print*,'OCT CANDIDATE POINT 3=',list_entities(3) % l
  call oct  % deallo(list_entities)
  print*,'memory 1=',oct % memor
  if( oct % memor(1) /= 0 ) then
     print*,'memory 1=',oct % memor
     stop 1
  end if
  !
  ! Find elements
  !
  call oct  % init  ()
  call oct  % input (limit=2_ip)
  call oct  % fill  (BOBOX=bobox)
  
!!$  call oct % search_element(xx,mesh,lelem,shapf,MEMORY_COUNTER=memor_loc)
!!$
!!$  !
!!$  ! Check interpolation
!!$  !
!!$  do ii = 1,size(xx,2)
!!$     xx_test = 0.0_rp
!!$     ielem = lelem(ii)
!!$     pelty = mesh % ltype(ielem)
!!$     do inode = 1,element_type(pelty) % number_nodes
!!$        ipoin = mesh % lnods(inode,ielem)
!!$        xx_test(1:2) = xx_test(1:2) + shapf(inode,ii) * mesh % coord(1:2,ipoin)
!!$     end do
!!$     print*,'coordinates= ',abs(xx_test(1)-xx(1,ii)),abs(xx_test(2)-xx(2,ii))
!!$     if( abs(xx_test(1)-xx(1,ii)) > epsil ) then
!!$        print*,'wrong interpolation 1: ',xx_test(1),xx(1,ii)
!!$        stop 1        
!!$     end if
!!$     if( abs(xx_test(2)-xx(2,ii)) > epsil ) then
!!$        print*,'wrong interpolation 2: ',xx_test(2),xx(2,ii)
!!$        stop 1        
!!$     end if
!!$  end do

  call mesh_out % init()
  print*,'create mesh'
  call oct      % mesh(&
       mesh_out % ndime,mesh_out % mnode,mesh_out % nelem,&
       mesh_out % npoin,mesh_out % lnods,mesh_out % ltype,&
       mesh_out % coord,MEMORY_COUNTER=memor_loc)
  print*,'create results'
  call oct      % results(res,names)
  call mesh_out % output (          FILENAME='octree')
  call mesh_out % results(res,names,FILENAME='octree')
  call oct      % results(res,names,ONLY_DEALLOCATE=.true.)
  call oct      % deallo()


  print*,'memory 2=',oct % memor
  if( oct % memor(1) /= 0 ) then
     print*,'memory 2=',oct % memor
     stop 1
  end if
  
!!$  deallocate(lelem,shapf)
  
  !----------------------------------------------------------------------
  !
  ! OCTBIN
  ! 
  !----------------------------------------------------------------------

  print*,'-----------------------------------------------------'
  print*,'OCTBIN'
  
  call octbin  % init  ()
  call octbin  % input (boxes=(/4_ip,4_ip/),limit=2_ip)
  call octbin  % fill  (BOBOX=bobox)
  call octbin  % candidate(xx,list_entities)

  print*,'OCTBIN CANDIDATE POINT 1=',list_entities(1) % l
  print*,'OCTBIN CANDIDATE POINT 2=',list_entities(2) % l
  print*,'OCTBIN CANDIDATE POINT 3=',list_entities(3) % l
  call octbin  % deallo(list_entities)
  !
  ! Find elements
  !
  call octbin  % init  ()
  call octbin  % input (boxes=(/4_ip,4_ip/),limit=2_ip)
  call octbin  % fill  (BOBOX=bobox)
  print*,'START-------------------------------'
!!$  call octbin % search_element(xx,mesh,lelem,shapf,MEMORY_COUNTER=memor_loc)
!!$  !
!!$  ! Check interpolation
!!$  !
!!$  do ii = 1,size(xx,2)
!!$     xx_test = 0.0_rp
!!$     ielem = lelem(ii)
!!$     if( ielem == 0 ) then
!!$        print*,'null element found'
!!$        stop 1
!!$     else
!!$        pelty = mesh % ltype(ielem)
!!$        do inode = 1,element_type(pelty) % number_nodes
!!$           ipoin = mesh % lnods(inode,ielem)
!!$           xx_test(1:2) = xx_test(1:2) + shapf(inode,ii) * mesh % coord(1:2,ipoin)
!!$        end do
!!$        print*,'coordinates= ',abs(xx_test(1)-xx(1,ii)),abs(xx_test(2)-xx(2,ii))
!!$        if( abs(xx_test(1)-xx(1,ii)) > epsil ) then
!!$           print*,'wrong interpolation 1: ',xx_test(1),xx(1,ii)
!!$           stop 1        
!!$        end if
!!$        if( abs(xx_test(2)-xx(2,ii)) > epsil ) then
!!$           print*,'wrong interpolation 2: ',xx_test(2),xx(2,ii)
!!$           stop 1        
!!$        end if
!!$     end if
!!$  end do
  
  call octbin  % deallo(list_entities)
  
  print*,'memory=',octbin % memor
  if( octbin % memor(1) /= 0 ) then
     print*,'memory=',octbin % memor
     stop 1
  end if
  
  !----------------------------------------------------------------------
  !
  ! SKDTREE
  ! 
  !----------------------------------------------------------------------
  
!  print*,'-----------------------------------------------------'
!  print*,'KDTREE'
  
!  call kdtree % init  ()
!  call kdtree % input (limit=2_ip)
!  call kdtree % fill  (BOBOX=bobox)
!  call kdtree % candidate(xx,list_entities)
  
!  print*,'OCT CANDIDATE POINT 1=',list_entities(1) % l
!  print*,'OCT CANDIDATE POINT 2=',list_entities(2) % l
!  print*,'OCT CANDIDATE POINT 3=',list_entities(3) % l
!  call kdtree % deallo(list_entities)
!  print*,'memory 1=',oct % memor
!  if( oct % memor(1) /= 0 ) then
!     print*,'memory 1=',oct % memor
!     stop 1
!  end if
  !
  ! Find elements
  !
!  call kdtree % init  ()
!  call kdtree % input (limit=2_ip)
!  call kdtree % fill  (BOBOX=bobox)
  
!!$  call oct % search_element(xx,mesh,lelem,shapf,MEMORY_COUNTER=memor_loc)
!!$
!!$  !
!!$  ! Check interpolation
!!$  !
!!$  do ii = 1,size(xx,2)
!!$     xx_test = 0.0_rp
!!$     ielem = lelem(ii)
!!$     pelty = mesh % ltype(ielem)
!!$     do inode = 1,element_type(pelty) % number_nodes
!!$        ipoin = mesh % lnods(inode,ielem)
!!$        xx_test(1:2) = xx_test(1:2) + shapf(inode,ii) * mesh % coord(1:2,ipoin)
!!$     end do
!!$     print*,'coordinates= ',abs(xx_test(1)-xx(1,ii)),abs(xx_test(2)-xx(2,ii))
!!$     if( abs(xx_test(1)-xx(1,ii)) > epsil ) then
!!$        print*,'wrong interpolation 1: ',xx_test(1),xx(1,ii)
!!$        stop 1        
!!$     end if
!!$     if( abs(xx_test(2)-xx(2,ii)) > epsil ) then
!!$        print*,'wrong interpolation 2: ',xx_test(2),xx(2,ii)
!!$        stop 1        
!!$     end if
!!$  end do

!  call mesh_out % init()
!  call kdtree     % mesh(&
!       mesh_out % ndime,mesh_out % mnode,mesh_out % nelem,&
!       mesh_out % npoin,mesh_out % lnods,mesh_out % ltype,&
!       mesh_out % coord,MEMORY_COUNTER=memor_loc)
!  call kdtree     % results(res,names)
!  call mesh_out % output (          FILENAME='kdtree')
!  call mesh_out % results(res,names,FILENAME='kdtree')
!  call kdtree     % results(res,names,ONLY_DEALLOCATE=.true.)

!  call kdtree % deallo()

!  print*,'memory 2=',oct % memor
!  if( oct % memor(1) /= 0 ) then
!     print*,'memory 2=',oct % memor
!     stop 1
!  end if
  
!!$  deallocate(lelem,shapf)
  
end program unitt_search_methods
