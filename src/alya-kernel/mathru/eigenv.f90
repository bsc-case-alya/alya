!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!------------------------------------------------------------------------
!> @addtogroup Mathematics
!> @{
!> @file    eigenv.f90
!> @author  Herbert Owen
!> @brief   computes all the eigenvalues and eigenvectors of real, symmetrix matrix A (3x3)
!> @details The eigenvalues go into matrix D, while the
!!          eigenvectors go into matrix V. NROT is the number of Jacobi rotations
!!          which were required.
!!          Beware A is altered.
!!          *This subroutine is taken from "Numerical Recipes", page 346
!> @}
!------------------------------------------------------------------------

subroutine eigenv(A,D,V,NROT)

  use def_kintyp
  implicit none
  real(rp),intent(inout)   :: A(3,3)
  real(rp),intent(out)     :: D(3), V(3,3)
  integer(ip),intent(out)  :: NROT

  real(rp)                 :: B(3), Z(3), SM, THRESH
  real(rp)                 :: C, S, G, H, T, TAU, THETA, tolei, tolej, toletole, toler
  integer(ip)              :: J, idime, jdime, i

  call runend('EIGENV: YOU SHOULD CODE THIS, THIS SHOULD NOT BE COMPLICATED...!')
  
end subroutine eigenv

