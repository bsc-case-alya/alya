!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Parall
!> @{
!> @file    par_checkpoint.f90
!> @author  houzeaux
!> @date    2020-05-08
!> @brief   MPI checkpointing
!> @details MPI checkpointing
!> @} 
!-----------------------------------------------------------------------

subroutine par_checkpoint()
  use def_master
  use def_parall
  use mod_parall
  implicit none
  integer(ip)             :: ichec
  
  if(nproc_par>1) then                                ! Checkpoint for communication
     call par_livinf(11_ip,' ',ichec)
     call par_chkpoi(ichec)
     if(ichec==0) then
        call par_livinf(12_ip,' ',ichec)
     else
        call runend('MPI IS NOT WORKING WELL')
     end if
  end if
  
end subroutine par_checkpoint
