!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!




program unitt_adapt_nastin
  
  use def_kintyp,         only : ip,lg
  use def_master,         only : ITASK_INITIA
  use def_master,         only : ITASK_ENDTIM
  use def_master,         only : ITASK_ENDRUN
  use def_master,         only : ITASK_READ_RESTART
  use def_master,         only : ITASK_WRITE_RESTART
  use def_master,         only : kfl_gotim
  use def_master,         only : kfl_stop
  use def_master,         only : kfl_goblk
  use def_master,         only : kfl_gocou
  use def_coupli,         only : kfl_gozon
  use def_kermod,         only : kfl_reset
  use mod_AMR,            only : AMR
  implicit none

  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction
  call Turnon()                                               ! Read modules
  call Solmem()                                               ! Solver and output memory
  call Begrun()                                               ! Initial computations

  call Restar(ITASK_READ_RESTART)                             ! Read restart
  call Iniunk()                                               ! Initial solution
  call Output(ITASK_INITIA)                                   ! Initial output
 
  time: do while ( kfl_gotim == 1 .and. kfl_stop == 0 )

      call Timste()                                           ! Compute time step

      reset: do
        call Begste()                                         ! Begin time step

         block: do while ( kfl_goblk == 1 .and. kfl_stop == 0 )

           zone_coupling: do while ( kfl_gozon == 1 .and. kfl_stop == 0 )

              call Begzon()                                   ! Start multicode block coupling

              coupling_modules: do while ( kfl_gocou == 1 .and. kfl_stop == 0 .and. kfl_reset /= 1 )
                 call Doiter()                                ! Inner loop of modules
                 call Concou()                                ! Check coupling convergence
              end do coupling_modules

              call Endzon()

           end do zone_coupling

           call Conblk()                                      ! End multicode block coupling

        end do block
        if( kfl_reset /= 1 .or. kfl_stop /= 0 ) exit reset

     enddo reset

     call Endste()                                            ! End of time step

     !call Filter(ITASK_ENDTIM)
     call Output(ITASK_ENDTIM)
     call EndTim(ITASK_ENDTIM)

     call Restar(ITASK_WRITE_RESTART)                         ! Write restart
     call AMR()                                               ! Adaptive mesh refinement
     call Repart()                                            ! Repartitioning
     
  end do time
  
  call Output(ITASK_ENDRUN)

  call Turnof()                                               ! Finish Alya
  call runend('O.K.!')                                        ! Finish Alya
  
end program unitt_adapt_nastin
