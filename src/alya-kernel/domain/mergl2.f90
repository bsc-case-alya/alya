!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine mergl2( kk, lista, lsize, nodes, nnode, me )
  !-----------------------------------------------------------------------
  !****f* Domain/mergl2
  ! NAME
  !    mergl2
  ! DESCRIPTION
  !    This routine merges to list of nodes
  ! OUTPUT
  !    LISTA
  !    LSIZE
  ! USED BY
  !    domgra
  !***
  !-----------------------------------------------------------------------
  use      def_kintyp
  implicit none
  integer(ip), intent(inout) :: lsize,lista(*)
  integer(ip), intent(in)    :: kk,nnode,me
  integer(ip), intent(in)    :: nodes(nnode)
  integer(ip)                :: ii, jj, n1, n2
  logical(lg)                :: noEncontrado

  if (me<0) then
     do ii= 1, nnode
        n1 = nodes(ii)
        if( n1 /= kk ) then
           jj = 1
           noEncontrado = .true.
           do while( jj<=lsize .and. noEncontrado)
              n2 = lista(jj)
              if (n1==n2) then
                 noEncontrado = .false.
              end if
              jj = jj + 1
           end do
           if (noEncontrado) then
              lsize = lsize + 1
              lista(lsize) = n1
           end if
        end if
     end do
  else
     do ii= 1, nnode
        n1 = nodes(ii)
        if( n1 /= kk ) then
           if (n1/=me) then
              jj = 1
              noEncontrado = .true.
              do while( jj<=lsize .and. noEncontrado)
                 n2 = lista(jj)
                 if (n1==n2) then
                    noEncontrado = .false.
                 endif
                 jj = jj + 1
              enddo
              if (noEncontrado) then
                 lsize = lsize + 1
                 lista(lsize) = n1
              endif
           endif
        end if
     enddo
  end if

end subroutine mergl2
