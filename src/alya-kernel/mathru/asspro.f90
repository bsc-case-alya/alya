!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine asspro(&
     itask,pnode,ndofn,pgaus,lnods,lelch,gpden,gpvis,gpvol,&
     gpsha,elrhs,prope)
  !-----------------------------------------------------------------------
  !****f* mathru/asspro
  ! NAME 
  !    asspro
  ! DESCRIPTION
  !    Assembly of properties
  ! USES
  ! USED BY
  !    *_elmope
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only        :  ip,rp
  use def_elmtyp, only        :  ELEXT
  implicit none
  integer(ip),  intent(in)    :: itask,pnode,ndofn,pgaus
  integer(ip),  intent(in)    :: lnods(pnode)
  integer(ip),  intent(in)    :: lelch
  real(rp),     intent(in)    :: gpden(pgaus)
  real(rp),     intent(in)    :: gpvis(pgaus)
  real(rp),     intent(in)    :: gpvol(pgaus)
  real(rp),     intent(in)    :: gpsha(pnode,pgaus)
  real(rp),     intent(out)   :: elrhs(ndofn,*)
  real(rp),     intent(inout) :: prope(ndofn,*)
  integer(ip)                 :: inode,ipoin,idofn,igaus
  real(rp)                    :: fact1,fact2

  do inode = 1,pnode
     do idofn = 1,ndofn
        elrhs(idofn,inode) = 0.0_rp
     end do
  end do
   
  if( itask == 10 ) then
     !
     ! Density
     !
     do igaus = 1,pgaus
        fact1 = gpden(igaus) * gpvol(igaus)
        do inode = 1,pnode
           elrhs(1,inode) = elrhs(1,inode) + fact1 * gpsha(inode,igaus) 
        end do
     end do

  else if( itask == 11 ) then
     !
     ! Viscosity
     !
     do igaus = 1,pgaus
        fact1 = gpvis(igaus) * gpvol(igaus)
        do inode = 1,pnode
           elrhs(1,inode) = elrhs(1,inode) + fact1 * gpsha(inode,igaus) 
        end do
     end do
     
  else if( itask == 12 ) then
     !
     ! Density and viscosity
     ! 
     do igaus = 1,pgaus
        fact1 = gpden(igaus) * gpvol(igaus)
        fact2 = gpvis(igaus) * gpvol(igaus)
        do inode = 1,pnode
           elrhs(1,inode) = elrhs(1,inode) + fact1 * gpsha(inode,igaus) 
           elrhs(2,inode) = elrhs(2,inode) + fact2 * gpsha(inode,igaus) 
        end do
     end do
     
  end if

  !----------------------------------------------------------------------
  !
  ! Assembly
  !
  !----------------------------------------------------------------------

  if( lelch == ELEXT ) then

     inode = 1

     if( itask == 10 .or. itask == 11 ) then
        ipoin          = lnods(inode)
        !$OMP ATOMIC
        prope(1,ipoin) = prope(1,ipoin) + elrhs(1,inode)
        
     else if( itask == 12 ) then
        
        ipoin          = lnods(inode)
        !$OMP ATOMIC
        prope(1,ipoin) = prope(1,ipoin) + elrhs(1,inode)
        !$OMP ATOMIC
        prope(2,ipoin) = prope(2,ipoin) + elrhs(2,inode)

     end if

  else

     if( itask == 10 .or. itask == 11 ) then

        do inode = 1,pnode
           ipoin          = lnods(inode)
           !$OMP ATOMIC
           prope(1,ipoin) = prope(1,ipoin) + elrhs(1,inode)
        end do
        
     else if( itask == 12 ) then

        do inode = 1,pnode
           ipoin          = lnods(inode)
           !$OMP ATOMIC
           prope(1,ipoin) = prope(1,ipoin) + elrhs(1,inode)
           !$OMP ATOMIC
           prope(2,ipoin) = prope(2,ipoin) + elrhs(2,inode)
        end do
        
     end if
  end if

end subroutine asspro
