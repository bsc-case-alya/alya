!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_lagrange
  !
  ! Check the intergration of some elements
  !
  use def_kintyp_basic
  use def_elmtyp
  use def_domain, only : ndime,mnode,ntens
  use def_kintyp_basic
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_isoparametric
  use def_quadrature
  use def_chebyshev
  use mod_element_integration
  use def_master, only : zeror
  implicit none

  integer(ip)              :: ielty,idime,inode,pnode,ielem,ipoin,ii
  type(mesh_type_basic)    :: mesh
  real(rp)                 :: elcod(3,64)
  integer(ip)              :: igaus,pgaus,pdime
  real(rp)                 :: vol
  real(rp), allocatable    :: gpsha(:,:)                    ! N
  real(rp), allocatable    :: gpder(:,:,:)              ! dN/dsi
  real(rp), allocatable    :: gpcar(:,:,:)              ! dN/dxi
  real(rp), allocatable    :: gphes(:,:,:)              ! dN/dxi
  real(rp), allocatable    :: gpvol(:)                          ! w*|J|, |J|
  real(rp), allocatable    :: x(:,:)
  real(rp), allocatable    :: y(:)
  real(rp)                 :: grad(3,3)
  real(rp)                 :: lapl(6)

#ifndef NDIMEPAR
  ndime = 3
#endif

  mnode = 64
  ntens = 6

  call elmgeo_element_type_initialization()
  !
  ! Define mesh
  !

  do ii = 1,4
     call mesh % init()

     select case  ( ii )
     case (1,2) ; call mesh_HEX64(mesh)
     case (3,4) ; call mesh_QUA16(mesh)
     end select     
     !
     ! Define interpolation and quadrature rule
     !
     ielty                      = mesh % ltype(1)     
     mesh % quad(ielty) % ngaus = 8**ndime

     select case ( ii ) 
     case (1,3) ; mesh % quad(ielty) % type  = GAUSS_LEGENDRE_RULE
     case (2,4) ; mesh % quad(ielty) % type  = TRAPEZOIDAL_RULE
     end select
     
     mesh % quad(ielty) % ndime = element_type(ielty) % dimensions
     mesh % iso(ielty)  % inter = LAGRANGE_INTERPOLATION 
     call mesh % quad(ielty) % set()
     call mesh % iso(ielty)  % set(element_type(ielty) % number_nodes,mesh % quad(ielty))
     !
     ! Change mesh coordinates
     !
     pnode = mesh % npoin
     pgaus = mesh % quad(ielty) % ngaus
     pdime = mesh % ndime
     !
     ! Compute volume and gradients
     !
     allocate(gpsha(pnode,pgaus))        ! N
     allocate(gpder(pdime,pnode,pgaus))  ! dN/dsi
     allocate(gpcar(pdime,pnode,pgaus))  ! dN/dxi
     allocate(gphes(ntens,pnode,pgaus))  ! d2N/dxidxi
     allocate(gpvol(pgaus))              ! w*|J|, |J|

     allocate(x(pdime,mesh % npoin))
     allocate(y(mesh % npoin))
     do ipoin = 1,mesh % npoin   
        x(1,ipoin) = 2.0_rp*mesh % coord(1,ipoin)    + 3.0_rp*mesh % coord(2,ipoin) 
        x(2,ipoin) = 5.0_rp*mesh % coord(1,ipoin)    + 6.0_rp*mesh % coord(2,ipoin) 
        y(ipoin)   = 2.0_rp*mesh % coord(1,ipoin)**2 + 3.0_rp*mesh % coord(2,ipoin)**2 + &
             5.0_rp * mesh % coord(1,ipoin) * mesh % coord(2,ipoin)
        if( pdime == 3 ) then
           x(1,ipoin) = x(1,ipoin) + 4.0_rp*mesh % coord(3,ipoin)
           x(2,ipoin) = x(2,ipoin) + 7.0_rp*mesh % coord(3,ipoin)
           x(3,ipoin) = 8.0_rp*mesh % coord(1,ipoin) + 9.0_rp*mesh % coord(2,ipoin) + 10.0_rp*mesh % coord(3,ipoin)
           y(ipoin)   = y(ipoin) + 4.0_rp*mesh % coord(3,ipoin)**2 + &
             6.0_rp * mesh % coord(1,ipoin) * mesh % coord(3,ipoin) + &
             7.0_rp * mesh % coord(2,ipoin) * mesh % coord(3,ipoin)
        end if
     end do
     !
     ! gphes = ( d11,d22,d12 ) in 2D
     !       = 
     !
     vol = 0.0_rp
     do ielem = 1,mesh % nelem
        elcod(1:pdime,1:pnode) = mesh % coord(1:pdime,mesh % lnods(1:pnode,ielem))
        call element_shape_function_derivatives_jacobian(&
             pnode,pgaus,1_ip,mesh % quad(ielty) % weigp,mesh % iso(ielty) % shape,&
             mesh % iso(ielty) % deriv,mesh % iso(ielty) % heslo,&
             elcod,gpvol,gpsha,gpder,gpcar,gphes)
        do igaus = 1,pgaus
           vol  = vol + gpvol(igaus)
           ! du_i/dx_p
           grad = 0.0_rp
           lapl = 0.0_rp
           do inode = 1,pnode
              do idime = 1,pdime
                 grad(1:pdime,idime) = grad(1:pdime,idime) + gpcar(1:pdime,inode,igaus) * x(idime,mesh % lnods(inode,ielem))
              end do
           end do
           do inode = 1,pnode
              do idime = 1,6
                 lapl(idime) = lapl(idime) + gphes(idime,inode,igaus) * y(mesh % lnods(inode,ielem))
              end do
           end do

           if( abs(grad(1,1)- 2.0_rp) > 1.0e-06_rp ) stop 10
           if( abs(grad(2,1)- 3.0_rp) > 1.0e-06_rp ) stop 11
           if( abs(grad(1,2)- 5.0_rp) > 1.0e-06_rp ) stop 13
           if( abs(grad(2,2)- 6.0_rp) > 1.0e-06_rp ) stop 14

           if( pdime == 3 ) then
              if( abs(grad(3,1)- 4.0_rp) > 1.0e-06_rp ) stop 12
              if( abs(grad(3,2)- 7.0_rp) > 1.0e-06_rp ) stop 15
              if( abs(grad(1,3)- 8.0_rp) > 1.0e-06_rp ) stop 16
              if( abs(grad(2,3)- 9.0_rp) > 1.0e-06_rp ) stop 17
              if( abs(grad(3,3)-10.0_rp) > 1.0e-06_rp ) stop 18
           end if
           ! dx2, dy2, dz2, dxdy, dxdz, dydz
           if( ndime == 2 ) then
              if( abs(lapl(1) - 4.0_rp) > 1.0e-06_rp ) stop 25
              if( abs(lapl(2) - 6.0_rp) > 1.0e-06_rp ) stop 26
              if( abs(lapl(3) - 5.0_rp) > 1.0e-06_rp ) stop 27 
           else
              ! dx2, dy2, dxdy
              if( abs(lapl(1) - 4.0_rp) > 1.0e-06_rp ) stop 19 
              if( abs(lapl(2) - 6.0_rp) > 1.0e-06_rp ) stop 20
              if( abs(lapl(3) - 8.0_rp) > 1.0e-06_rp ) stop 21 
              if( abs(lapl(4) - 5.0_rp) > 1.0e-06_rp ) stop 22 
              if( abs(lapl(5) - 6.0_rp) > 1.0e-06_rp ) stop 23 
              if( abs(lapl(6) - 7.0_rp) > 1.0e-06_rp ) stop 24 
           end if
           
        end do
     end do

     if( pdime == 3 ) then
        if( abs(vol-8.0_rp) > 1.0e-06_rp ) then
           print*,'wrong volume=',vol
           stop 3
        end if
     else
        if( abs(vol-4.0_rp) > 1.0e-06_rp ) then
           print*,'wrong volume=',vol
           stop 3
        end if        
     end if
     print*,'volume= ',vol


     deallocate(gpsha,gpder,gpcar,gphes,gpvol,x,y)

  end do

contains

  subroutine mesh_HEX64(mesh)
    type(mesh_type_basic)    :: mesh

     mesh % mnode = 64
     mesh % ndime = 3
     mesh % npoin = 64
     mesh % nelem = 1

     call mesh % alloca()

     mesh % ltype      = HEX64

     do inode = 1,mesh % npoin
        mesh % lnods(inode,1) = inode
     end do

     write(*,'(64(1x,i2))') mesh % lnods(:,1)
     stop
     
     mesh % coord(1:3, 1) = (/ -1.000000000000000000000_rp, -1.000000000000000000000_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3, 2) = (/  1.000000000000000000000_rp, -1.000000000000000000000_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3, 3) = (/  1.000000000000000000000_rp,  1.000000000000000000000_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3, 4) = (/ -1.000000000000000000000_rp,  1.000000000000000000000_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3, 5) = (/ -1.000000000000000000000_rp, -1.000000000000000000000_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3, 6) = (/  1.000000000000000000000_rp, -1.000000000000000000000_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3, 7) = (/  1.000000000000000000000_rp,  1.000000000000000000000_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3, 8) = (/ -1.000000000000000000000_rp,  1.000000000000000000000_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3, 9) = (/ -0.333333333333333370341_rp, -1.000000000000000000000_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3,10) = (/  0.333333333333333259318_rp, -1.000000000000000000000_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3,11) = (/  1.000000000000000000000_rp, -0.333333333333333370341_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3,12) = (/  1.000000000000000000000_rp,  0.333333333333333259318_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3,13) = (/  0.333333333333333259318_rp,  1.000000000000000000000_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3,14) = (/ -0.333333333333333370341_rp,  1.000000000000000000000_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3,15) = (/ -1.000000000000000000000_rp,  0.333333333333333259318_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3,16) = (/ -1.000000000000000000000_rp, -0.333333333333333370341_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3,17) = (/ -1.000000000000000000000_rp, -1.000000000000000000000_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,18) = (/  1.000000000000000000000_rp, -1.000000000000000000000_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,19) = (/  1.000000000000000000000_rp,  1.000000000000000000000_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,20) = (/ -1.000000000000000000000_rp,  1.000000000000000000000_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,21) = (/ -1.000000000000000000000_rp, -1.000000000000000000000_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,22) = (/  1.000000000000000000000_rp, -1.000000000000000000000_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,23) = (/  1.000000000000000000000_rp,  1.000000000000000000000_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,24) = (/ -1.000000000000000000000_rp,  1.000000000000000000000_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,25) = (/ -0.333333333333333370341_rp, -1.000000000000000000000_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3,26) = (/  0.333333333333333259318_rp, -1.000000000000000000000_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3,27) = (/  1.000000000000000000000_rp, -0.333333333333333370341_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3,28) = (/  1.000000000000000000000_rp,  0.333333333333333259318_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3,29) = (/  0.333333333333333259318_rp,  1.000000000000000000000_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3,30) = (/ -0.333333333333333370341_rp,  1.000000000000000000000_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3,31) = (/ -1.000000000000000000000_rp,  0.333333333333333259318_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3,32) = (/ -1.000000000000000000000_rp, -0.333333333333333370341_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3,33) = (/ -0.333333333333333370341_rp, -0.333333333333333370341_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3,34) = (/  0.333333333333333259318_rp, -0.333333333333333370341_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3,35) = (/  0.333333333333333259318_rp,  0.333333333333333259318_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3,36) = (/ -0.333333333333333370341_rp,  0.333333333333333259318_rp, -1.000000000000000000000_rp /)
     mesh % coord(1:3,37) = (/ -0.333333333333333370341_rp, -1.000000000000000000000_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,38) = (/  0.333333333333333259318_rp, -1.000000000000000000000_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,39) = (/  1.000000000000000000000_rp, -0.333333333333333370341_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,40) = (/  1.000000000000000000000_rp,  0.333333333333333259318_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,41) = (/  0.333333333333333259318_rp,  1.000000000000000000000_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,42) = (/ -0.333333333333333370341_rp,  1.000000000000000000000_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,43) = (/ -1.000000000000000000000_rp,  0.333333333333333259318_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,44) = (/ -1.000000000000000000000_rp, -0.333333333333333370341_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,45) = (/ -0.333333333333333370341_rp, -1.000000000000000000000_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,46) = (/  0.333333333333333259318_rp, -1.000000000000000000000_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,47) = (/  1.000000000000000000000_rp, -0.333333333333333370341_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,48) = (/  1.000000000000000000000_rp,  0.333333333333333259318_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,49) = (/  0.333333333333333259318_rp,  1.000000000000000000000_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,50) = (/ -0.333333333333333370341_rp,  1.000000000000000000000_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,51) = (/ -1.000000000000000000000_rp,  0.333333333333333259318_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,52) = (/ -1.000000000000000000000_rp, -0.333333333333333370341_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,53) = (/ -0.333333333333333370341_rp, -0.333333333333333370341_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3,54) = (/  0.333333333333333259318_rp, -0.333333333333333370341_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3,55) = (/  0.333333333333333259318_rp,  0.333333333333333259318_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3,56) = (/ -0.333333333333333370341_rp,  0.333333333333333259318_rp,  1.000000000000000000000_rp /)
     mesh % coord(1:3,57) = (/ -0.333333333333333370341_rp, -0.333333333333333370341_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,58) = (/  0.333333333333333259318_rp, -0.333333333333333370341_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,59) = (/  0.333333333333333259318_rp,  0.333333333333333259318_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,60) = (/ -0.333333333333333370341_rp,  0.333333333333333259318_rp, -0.333333333333333370341_rp /)
     mesh % coord(1:3,61) = (/ -0.333333333333333370341_rp, -0.333333333333333370341_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,62) = (/  0.333333333333333259318_rp, -0.333333333333333370341_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,63) = (/  0.333333333333333259318_rp,  0.333333333333333259318_rp,  0.333333333333333259318_rp /)
     mesh % coord(1:3,64) = (/ -0.333333333333333370341_rp,  0.333333333333333259318_rp,  0.333333333333333259318_rp /)

   end subroutine mesh_HEX64

   subroutine mesh_QUA16(mesh)
     !
     ! Skewd
     !
     type(mesh_type_basic)    :: mesh

     mesh % mnode = 16
     mesh % ndime = 2
     mesh % npoin = 16
     mesh % nelem = 1

     call mesh % alloca()

     mesh % ltype      = QUA16

     do inode = 1,mesh % npoin
        mesh % lnods(inode,1) = inode
     end do
              
     mesh % coord(1:2,1 ) = (/  0.0_rp,                0.0_rp               /)
     mesh % coord(1:2,2 ) = (/  4.0_rp,                0.0_rp               /)
     mesh % coord(1:2,3 ) = (/  5.0_rp,                4.0_rp               /)
     mesh % coord(1:2,4 ) = (/  1.0_rp,                4.0_rp               /)
     mesh % coord(1:2,5 ) = (/  1.333333333329966_rp,  0.0_rp               /)
     mesh % coord(1:2,6 ) = (/  2.666666666663132_rp,  0.0_rp               /)
     mesh % coord(1:2,7 ) = (/  4.333333333332389_rp,  1.333333333329555_rp /)
     mesh % coord(1:2,8 ) = (/  4.666666666665957_rp,  2.666666666663829_rp /)
     mesh % coord(1:2,9 ) = (/  3.66666666666675_rp,   4.0_rp               /)
     mesh % coord(1:2,10) = (/  2.333333333336423_rp,  4.0_rp               /)
     mesh % coord(1:2,11) = (/  0.6666666666668164_rp, 2.666666666667266_rp /)
     mesh % coord(1:2,12) = (/  0.3333333333341704_rp, 1.333333333336681_rp /)
     mesh % coord(1:2,13) = (/  3.333333333331788_rp,  2.666666666664975_rp /)
     mesh % coord(1:2,14) = (/  2.000000000000801_rp,  2.66666666666612_rp  /)
     mesh % coord(1:2,15) = (/  1.666666666665695_rp,  1.333333333334306_rp /)
     mesh % coord(1:2,16) = (/  2.999999999997321_rp,  1.333333333331931_rp /)

   end subroutine mesh_QUA16
 
end program unitt_lagrange




