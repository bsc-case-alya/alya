#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



project(${ProjectId} LANGUAGES Fortran C CXX)
file(GLOB_RECURSE SRC_FILES ${CMAKE_HOME_DIRECTORY}/unitt/${PROJECT_NAME}/*.f90 ${CMAKE_HOME_DIRECTORY}/src/alya-core/*.f90) 

add_executable(${PROJECT_NAME} ${SRC_FILES})

include(compiler)
set_flags()
set_defs()
set_parall()
include(dependencies)

include(lib-management)
alya_link(module-interface)

target_link_libraries(${PROJECT_NAME} c)

set_target_properties(${PROJECT_NAME} PROPERTIES POSITION_INDEPENDENT_CODE ON)
set_target_properties(${PROJECT_NAME} PROPERTIES LINKER_LANGUAGE Fortran)
