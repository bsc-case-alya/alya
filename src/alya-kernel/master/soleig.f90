!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine soleig(amatr,eigva,eigen,bmatr,iter)
  !-----------------------------------------------------------------------
  !****f* master/soleig
  ! NAME 
  !    solver
  ! DESCRIPTION
  !    This routine calls the solvers
  ! USES
  !    memchk
  !    mediso
  ! USED BY
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  rp,ip
  use def_solver, only     :  eigen_sol,cpu_eigen
  use def_domain, only     :  npoin,r_dom,c_dom
  implicit none
  real(rp),    intent(in)  :: eigva(*),eigen(*)
  real(rp),    intent(in)  :: amatr(*),bmatr(*) 
  integer(ip), intent(in)  :: iter
  integer(ip)              :: izdom,ipoin,jpoin
  real(rp)                 :: time1,time2

  call cputim(time1)
  
  call runend('SOLEIG: NO MORE EIGEN SOLVER')

  call cputim(time2)
  cpu_eigen = cpu_eigen + (time2-time1)

end subroutine soleig
