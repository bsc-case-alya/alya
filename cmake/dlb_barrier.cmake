#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(init_dlb_barrier)
  set(DLB_INCLUDE_DIR "${DLB_DIR}/include" CACHE INTERNAL "")
  set(DLB_LIB_DIR "${DLB_DIR}/lib" CACHE INTERNAL "")
endfunction()

function(dlb_barrier)
  if (WITH_DLB_BARRIER MATCHES "external")
    add_definitions(-DALYA_DLB_BARRIER)
    include_directories(${DLB_INCLUDE_DIR})
    link_directories(${DLB_LIB_DIR})

    find_library(DLB_LIBRARY
       NAMES dlb
       PATHS ${DLB_DIR} ENV DLB_HOME
       HINTS ${DLB_LIB_DIR} 
       REQUIRED
    )

    message("Setting up DLB_BARRIER")
    target_link_libraries(${PROJECT_NAME} ${DLB_LIBRARY})
    set_target_properties(${PROJECT_NAME} PROPERTIES INSTALL_RPATH "${DLB_LIB_DIR}")
    set_target_properties(${PROJECT_NAME} PROPERTIES BUILD_WITH_INSTALL_RPATH ON)
  endif()
endfunction()
