#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(init_fti)
  set(FTI_HOME "${CMAKE_BINARY_DIR}/submodules/fti" CACHE INTERNAL "")
  set(FTI_ENABLE_HDF5 OFF CACHE INTERNAL "")
  set(FTI_ENABLE_FORTRAN "1" CACHE INTERNAL "")
  set(FTI_ENABLE_TESTS "0" CACHE INTERNAL "")
  set(FTI_ENABLE_EXAMPLES "0" CACHE INTERNAL "")
endfunction()

function(fti)
  if (NOT WITH_FTI MATCHES "OFF")
    add_dependencies(${PROJECT_NAME} fti)
    add_definitions(-DALYA_FTI)
    include_directories(${FTI_INCLUDE_DIR})
    set_property(GLOBAL PROPERTY FIND_LIBRARY_USE_LIB64_PATHS ON)
    include(GNUInstallDirs)
    target_link_libraries(${PROJECT_NAME} ${FTI_HOME}/${CMAKE_INSTALL_LIBDIR}/libfti.so ${FTI_HOME}/${CMAKE_INSTALL_LIBDIR}/libfti_f90.so) 
    set_target_properties(${PROJECT_NAME} PROPERTIES INSTALL_RPATH "${FTI_HOME}/${CMAKE_INSTALL_LIBDIR}")
    set_target_properties(${PROJECT_NAME} PROPERTIES BUILD_WITH_INSTALL_RPATH ON)
  endif()
endfunction()
