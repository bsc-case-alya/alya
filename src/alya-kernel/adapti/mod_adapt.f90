!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Adaptivity
!> @{
!> @file    mod_adapt.f90
!> @author  abel.gargallo
!> @date    2021-03-31
!> @brief   mod_adapt
!> @details mod_adapt
!>
!>          To add further details
!>
!>
!-----------------------------------------------------------------------

MODULE mod_adapt
!***************************************************************
!*
!*  Module for performing adaptive topological operation
!*
!***************************************************************
use def_kintyp_basic,       only: ip,rp,lg
use def_kintyp_mesh_basic,  only: mesh_type_basic
use mod_memory,             only: memory_alloca, memory_deallo

use def_adapt,              only: memor_adapt 
use mod_metric,             only: mesh_metric_type
use mod_debugTools,         only: out_debug_text, out_debug_paraview

use mod_meshTopology, only : edgeData_type



implicit none

integer(ip), parameter :: adapt_strat_split    = 1
integer(ip), parameter :: adapt_strat_collapse = 2
integer(ip), parameter :: adapt_strat_swap     = 3
integer(ip), parameter :: adapt_strat_smooth   = 4

real(rp),    parameter :: minQ_validity   = 1.0e-6! 0.00001_rp!1.0e-5
logical(lg), parameter :: do_warnValidity = .true.

logical(lg), parameter :: output_steps_paraview       = .true.
logical(lg), parameter :: output_steps_swap_paraview  = .false.
logical(lg), parameter :: output_steps_edgeRep_paraview  = .true.

logical(lg), parameter :: out_verbose = .true.
logical(lg), parameter :: out_time = .true.

character(len=200), parameter :: file_out =TRIM('mesh_adapt')

! ---- ADAPTIVITY PARAMETERS ----------------------------
!
! In adapt_mesh_to_metric: quality to set the subregion in where the mesh is adapted (below threshold)
!
! real(rp)   , parameter:: threshold_quality_to_repair = 0.4_rp ! abelcruz !2.0_rp!0.5_rp!0.3_rp
real(rp)  :: threshold_quality_to_repair != 0.4_rp ! abelcruz !2.0_rp!0.5_rp!0.3_rp

integer(ip), parameter:: numNieghLevels = 2_ip

!
! In adapt_mesh_to_metric_private:
!
integer(ip),parameter :: numRepairs = 4
  !real(rp)   ,parameter:: tols_coll(numRepairs)   = (/    0.5,   0.5, 1.0/3.0, 1.0/3.0 /)
!   logical(lg),parameter:: doCheckColl(numRepairs) = (/.false.,.false.,  .false.,  .false./) ! check improvement from prev configuration
  !logical(lg),parameter:: doCheckColl(numRepairs) = (/.true.,.true.,  .true.,  .true./) ! check improvement from prev configuration
real(rp)   ,parameter:: tols_coll(numRepairs)   = (/     0.1,     0.2,     0.2,     0.4 /)
! real(rp)   ,parameter:: tols_coll(numRepairs)   = (/     0.00001,     0.00002,     0.00002,     0.00004 /)
logical(lg),parameter:: doCheckColl(numRepairs) = (/  .true.,  .true.,  .true.,  .true. /) ! check improvement from prev configuration
! logical(lg),parameter:: doCheckColl(numRepairs) = (/  .false.,  .false.,  .true.,  .true. /) ! check improvement from prev configuration
  
!   real(rp)   ,parameter:: tols_split(numRepairs)  = (/     2.0,      2.0,      1.5,      1.5 /) !sqrt(2.0)
! !   logical(lg),parameter:: doCheckSplit(numRepairs)= (/ .false.,  .false.,   .true.,   .true. /) ! check improvement from prev configuration
!   logical(lg),parameter:: doCheckSplit(numRepairs)= (/  .true.,  .true.,   .true.,   .true. /) ! check improvement from prev configuration
!   !logical(lg),parameter:: doCheckSplit(numRepairs)= (/.false.,.false.,  .true.,  .true./) ! check improvement from prev configuration
  !real(rp)   ,parameter:: tols_split(numRepairs)  = (/     10.0,     2.0,      2.0,     1.5 /) !sqrt(2.0)
  !logical(lg),parameter:: doCheckSplit(numRepairs)= (/   .true.,  .true.,   .true.,   .true. /) ! check improvement from prev configuration
real(rp)   ,parameter:: tols_split(numRepairs)  = (/ 10.0,    3.0,     2.0,     1.5/) !sqrt(2.0)
! logical(lg),parameter:: doCheckSplit(numRepairs)= (/.false.,.false.,  .true.,  .true./) ! check improvement from prev configuration
! logical(lg),parameter:: doCheckSplit(numRepairs)= (/.true.,.true.,  .true.,  .true./) ! check improvement from prev configuration
logical(lg),parameter:: doCheckSplit(numRepairs)= (/.false.,.true.,  .true.,  .true./) ! check improvement from prev configuration
  
! !   integer(ip),parameter:: num_swaps(numRepairs)     = (/   2,   2,   2,   2/) ! 1 1 2 3
! !   real(rp)   ,parameter:: tols_swap_ini(numRepairs) = (/ 0.2, 0.2, 0.2, 0.2/)
! !   real(rp)   ,parameter:: tols_swap_step(numRepairs)= (/ 0.1, 0.1, 0.2, 0.2/)
!   !integer(ip),parameter:: num_swaps(numRepairs)     = (/   1,   1,   2,   2/) ! 1 1 2 3
! !   integer(ip),parameter:: num_swaps(numRepairs)     = (/   1,   1,   2,   2/) ! 1 1 2 3 ! XXYYZZ aqui canviat per abelcruz
! !   real(rp)   ,parameter:: tols_swap_ini(numRepairs) = (/ 0.1, 0.1, 0.1, 0.2/)
! !   real(rp)   ,parameter:: tols_swap_step(numRepairs)= (/ 0.1, 0.1, 0.2, 0.2/)
! integer(ip),parameter:: num_swaps(numRepairs)     = (/   1,   2,   2,   3/) ! 1 1 2 3 ! XXYYZZ aqui canviat per abelcruz
! real(rp)   ,parameter:: tols_swap_ini(numRepairs) = (/ 0.1, 0.1, 0.1, 0.2/)
! real(rp)   ,parameter:: tols_swap_step(numRepairs)= (/ 0.1, 0.1, 0.2, 0.2/)
!
! !real(rp)   ,parameter:: tols_smooth(numRepairs)   = (/ 0.3, 0.3, 0.4, 0.4/)
! !   real(rp)   ,parameter:: tols_smooth(numRepairs)   = (/ 0.1, 0.1, 0.2, 0.3/)
! real(rp)   ,parameter:: tols_smooth(numRepairs)   = (/ 0.1, 0.2, 0.3, 0.4/) ! XXYYZZ aqui canviat per abelcruz
! !   real(rp)   ,parameter:: tols_smooth(numRepairs)   = (/ 0.1, 0.2, 0.4, 0.6/) ! XXYYZZ aqui canviat per abelcruz

integer(ip) :: num_swaps(       numRepairs  ) != (/   1,   2,   2,   3/) ! 1 1 2 3 ! XXYYZZ aqui canviat per abelcruz
real(rp)    :: tols_swap_ini(   numRepairs  ) != (/ 0.1, 0.1, 0.1, 0.2/)
real(rp)    :: tols_swap_step(  numRepairs  ) != (/ 0.1, 0.1, 0.2, 0.2/)

real(rp)    :: tols_smooth(     numRepairs  ) != (/ 0.1, 0.2, 0.3, 0.4/) ! XXYYZZ aqui canviat per abelcruz

!
! Performance variables:
!
logical(lg), parameter :: out_performance = .false.
real(rp) :: time_total
real(rp) :: time_split, time_coll, time_swap, time_smooth


private

public :: adapt_mesh_to_metric
!
!
!
CONTAINS
!
!
!
subroutine setAdaptParameters(thresholdQuality)
  implicit none
  
  real(rp), optional, intent(in) ::thresholdQuality
  
  
  if(.not.present(thresholdQuality)) then
    !call setAdaptParameters(0.4_rp) ! -> removed call to itself to avoid declaring it as a recursion...
    threshold_quality_to_repair = 0.4_rp ! abelcruz !2.0_rp!0.5_rp!0.3_rp

    num_swaps     = (/   1_ip,   2_ip,   2_ip,   3_ip/) ! 1 1 2 3 ! XXYYZZ aqui canviat per abelcruz
    tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.2_rp/)
    tols_swap_step= (/ 0.1_rp, 0.1_rp, 0.2_rp, 0.2_rp/)

    tols_smooth   = (/ 0.1_rp, 0.2_rp, 0.3_rp, 0.4_rp/) ! XXYYZZ aqui canviat per abelcruz
    
  else
    threshold_quality_to_repair = thresholdQuality+0.001_rp
    
    if(     thresholdQuality<0.1001_rp) then
!       num_swaps     = (/   1_ip,   1_ip,   1_ip,   1_ip/)
!       tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.1_rp/)
!       tols_swap_step= (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.1_rp/)
!       tols_smooth(:)   = 0.1_rp
      num_swaps     = (/    1_ip,    1_ip,   1_ip,   1_ip/)
      tols_swap_ini = (/ 0.05_rp, 0.05_rp, 0.1_rp, 0.1_rp/)
      tols_swap_step= (/  0.0_rp,  0.0_rp, 0.0_rp, 0.0_rp/)
      tols_smooth(:)   = 0.1_rp
    !
    else if(thresholdQuality<0.2001_rp) then
!       num_swaps     = (/   1_ip,   1_ip,   2_ip,   2_ip/)
!       tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.1_rp/)
!       tols_swap_step= (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.1_rp/)
!       tols_smooth   = (/ 0.1_rp, 0.1_rp, 0.2_rp, 0.2_rp/)
      num_swaps     = (/   1_ip,   1_ip,   1_ip,   1_ip/)
      tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.2_rp, 0.2_rp/)
      tols_swap_step= (/ 0.0_rp, 0.0_rp, 0.0_rp, 0.0_rp/)
      tols_smooth   = (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.2_rp/)
    !
    else if(thresholdQuality<0.3001_rp) then
!       num_swaps     = (/   1_ip,   1_ip,   2_ip,   3_ip/)
!       tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.1_rp/)
!       tols_swap_step= (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.1_rp/)
!       tols_smooth   = (/ 0.1_rp, 0.1_rp, 0.2_rp, 0.3_rp/)

      num_swaps     = (/   1_ip,   1_ip,   1_ip,   1_ip/)
      tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.2_rp, 0.2_rp/)
      tols_swap_step= (/ 0.0_rp, 0.0_rp, 0.0_rp, 0.0_rp/)
      tols_smooth   = (/ 0.1_rp, 0.1_rp, 0.2_rp, 0.2_rp/)
    !
    else if(thresholdQuality<0.4001_rp) then
!       num_swaps     = (/   1_ip,   2_ip,   2_ip,   2_ip/)
!       tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.2_rp/)
!       tols_swap_step= (/ 0.1_rp, 0.1_rp, 0.2_rp, 0.2_rp/)
!       tols_smooth   = (/ 0.1_rp, 0.2_rp, 0.3_rp, 0.4_rp/)
      
      num_swaps     = (/   1_ip,   1_ip,   1_ip,   1_ip/)
      tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.2_rp, 0.3_rp/)
      tols_swap_step= (/ 0.0_rp, 0.0_rp, 0.0_rp, 0.0_rp/)
      tols_smooth   = (/ 0.1_rp, 0.1_rp, 0.2_rp, 0.3_rp/)
    !
    else if(thresholdQuality<0.5001_rp) then
!       num_swaps     = (/   1_ip,   2_ip,   2_ip,   3_ip/)
!       tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.1_rp/)
!       tols_swap_step= (/ 0.1_rp, 0.1_rp, 0.2_rp, 0.2_rp/)
!       tols_smooth   = (/ 0.1_rp, 0.2_rp, 0.3_rp, 0.5_rp/)
      num_swaps     = (/   1_ip,   1_ip,   1_ip,   1_ip/)
      tols_swap_ini = (/ 0.1_rp, 0.2_rp, 0.2_rp, 0.4_rp/)
      tols_swap_step= (/ 0.0_rp, 0.0_rp, 0.0_rp, 0.0_rp/)
      tols_smooth   = (/ 0.1_rp, 0.2_rp, 0.3_rp, 0.4_rp/)
    !
    else if(thresholdQuality<0.6001_rp) then
!       num_swaps     = (/   1_ip,   2_ip,   2_ip,   3_ip/)
!       tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.1_rp/)
!       tols_swap_step= (/ 0.1_rp, 0.1_rp, 0.3_rp, 0.25_rp/)
!       tols_smooth   = (/ 0.1_rp, 0.2_rp, 0.3_rp, 0.6_rp/)
      num_swaps     = (/   1_ip,   1_ip,   1_ip,   1_ip/)
      tols_swap_ini = (/ 0.1_rp, 0.2_rp, 0.3_rp, 0.5_rp/)
      tols_swap_step= (/ 0.0_rp, 0.0_rp, 0.0_rp, 0.0_rp/)
      tols_smooth   = (/ 0.1_rp, 0.2_rp, 0.3_rp, 0.5_rp/)
    !
    else if(thresholdQuality<0.7001_rp) then
!       num_swaps     = (/   1_ip,   2_ip,   2_ip,   3_ip/)
!       tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.1_rp/)
!       tols_swap_step= (/ 0.1_rp, 0.1_rp, 0.3_rp, 0.3_rp/)
!       tols_smooth   = (/ 0.1_rp, 0.2_rp, 0.3_rp, 0.7_rp/)
      num_swaps     = (/   1_ip,   1_ip,   1_ip,   1_ip/)
      tols_swap_ini = (/ 0.1_rp, 0.2_rp, 0.3_rp, 0.6_rp/)
      tols_swap_step= (/ 0.0_rp, 0.0_rp, 0.0_rp, 0.0_rp/)
      tols_smooth   = (/ 0.1_rp, 0.2_rp, 0.3_rp, 0.6_rp/)
    !
    else if(thresholdQuality<0.8001_rp) then
!       num_swaps     = (/   1_ip,   2_ip,   2_ip,   3_ip/)
!       tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.1_rp/)
!       tols_swap_step= (/ 0.1_rp, 0.1_rp, 0.3_rp, 0.35_rp/)
!       tols_smooth   = (/ 0.1_rp, 0.2_rp, 0.4_rp, 0.8_rp/)
      num_swaps     = (/   1_ip,   1_ip,   1_ip,   1_ip/)
      tols_swap_ini = (/ 0.1_rp, 0.2_rp, 0.4_rp, 0.6_rp/)
      tols_swap_step= (/ 0.0_rp, 0.0_rp, 0.0_rp, 0.0_rp/)
      tols_smooth   = (/ 0.1_rp, 0.3_rp, 0.4_rp, 0.6_rp/)
    !
    else if(thresholdQuality<0.9001_rp) then
      num_swaps     = (/   1_ip,   2_ip,   2_ip,   3_ip/)
      tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.1_rp/)
      tols_swap_step= (/ 0.1_rp, 0.1_rp, 0.3_rp, 0.4_rp/)
      tols_smooth   = (/ 0.1_rp, 0.2_rp, 0.4_rp, 0.8_rp/)
    !
    else if(thresholdQuality<1.0001_rp) then
      num_swaps     = (/   1_ip,   2_ip,   2_ip,   3_ip/)
      tols_swap_ini = (/ 0.1_rp, 0.1_rp, 0.1_rp, 0.1_rp/)
      tols_swap_step= (/ 0.1_rp, 0.1_rp, 0.3_rp, 0.4_rp/)
      tols_smooth   = (/ 0.1_rp, 0.2_rp, 0.4_rp, 0.9_rp/)
    else
      call runend('Quality should be in range [0,1]')
    end if
    
  end if
  
end subroutine setAdaptParameters
!
!
!
subroutine adapt_mesh_to_metric(&
  mesh,&
  metric,&
  mapNodes_input_to_adapted_mesh,&
  lock_valid_elems,&
  isModifiedMesh,&
  thresholdQuality,&
  activationQuality)
  !
  use mod_quality,      only: compute_mesh_quality_sizeShape, quality_deallo
  use mod_quality,      only: print_q_stats, do_print_q_stats
  use mod_meshTopology, only: node_to_elems_type
  use def_master,       only: kfl_paral
  use mod_strings,      only: real_to_string
  use mod_communications_global,    only: PAR_MIN
  use mod_messages,     only : messages_live
  !
  type(mesh_type_basic),          intent(inout) :: mesh
  type(mesh_metric_type),         intent(inout) :: metric
  integer(ip), pointer, optional, intent(inout) :: mapNodes_input_to_adapted_mesh(:)
  logical(lg)         , optional, intent(in)    :: lock_valid_elems
  logical(lg)         , optional, intent(inout) :: isModifiedMesh
  real(rp)            , optional, intent(inout) :: thresholdQuality
  real(rp)            , optional, intent(inout) :: activationQuality
  !
  real(rp), pointer :: q(:)
  integer(ip) :: ielem, iele_good, iele_bad, iele_aux, inode
  integer(ip) :: list_good(mesh%nelem),list_bad(mesh%nelem),list_bad_prev(mesh%nelem)
  integer(ip), pointer :: lnods_good(:,:), lnods_bad(:,:), lnods_aux(:,:)
  logical(lg) :: is_bad_ele(mesh%nelem),is_bad_ele_prev(mesh%nelem)
  
  integer(ip) :: ltype_save
  integer(ip), pointer :: mapNodes_input_to_adapted_mesh_private(:)
  
  integer(ip) :: ilevel, i_ele_bad_prev
  integer(ip), pointer      :: elems_adjToNode(:)
  type(node_to_elems_type)  :: node_to_elems
  !
  real(rp)    :: minQ
  logical(lg) :: activate_adaptation
  
  logical(lg) ::isq_allo 
  
  real(rp) :: t0,t1
  !
  time_total = 0.0_rp
  if(out_performance) then
    call cpu_time(t0)
    call compute_mesh_quality_sizeShape(mesh,metric,q)
    call do_print_q_stats(q)
  end if
  
  call setAdaptParameters(thresholdQuality)
  
  call out_firstLast(mesh,metric,file_out)

  if(do_warnValidity) then
    ! check mesh validity.. just in case
    call compute_mesh_quality_sizeShape(mesh,metric,q)
    
    call print_q_stats(q)
    
    if(minval(q)<minQ_validity) then
      !call runend('Invalid mesh BEFORE mesh adaption (ensure it was valid before or check what happened..)') 
      call messages_live('Invalid mesh BEFORE mesh adaption','WARNING')
    end if
    isq_allo = .true.
  end if

  if(present(activationQuality)) then
    if(.not.isq_allo) then
      call compute_mesh_quality_sizeShape(mesh,metric,q)
      isq_allo = .true.
    end if

    minQ = minval(q)

    !call PAR_MIN(minQ)

    if(minQ>activationQuality) then
      !call messages_live('NOT ACTIVATED -> MIN QUAL: '//real_to_string(minQ)//' > '//real_to_string(activationQuality) )
      
      if(present(isModifiedMesh)) then
        isModifiedMesh = .false. 
      end if
      return
    else
      ! There are bad elements, so we mark it as potentially modified
      ! Another thing is that we cannot modify it.. but may be due to parallel constrains,
      ! so we risk entering again with a different partition... safe decicions right now
      !call messages_live('ACTIVATED -> MIN QUAL: '//real_to_string(minQ)//' < '//real_to_string(activationQuality) )
      if(present(isModifiedMesh)) then
        isModifiedMesh = .true. 
      end if
    end if
  end if

  if(present(lock_valid_elems).and.(.not.lock_valid_elems)) then
    
    if(present(isModifiedMesh)) then
      ! First I thought to update this in adapt_mesh_to_metric_private is mesh is actually modified
      ! Then I decided that not... becaus in parallel iterations mesh could not be modified due
      ! to inner region boundaries but being bad
      ! Another thing is that we cannot modify it.. but may be due to parallel constrains,
      ! so we risk entering again with a different partition... safe decicions right now
      isModifiedMesh = .true. 
    end if
    
    call adapt_mesh_to_metric_private(mesh,metric,mapNodes_input_to_adapted_mesh_private)
    
  else

!     if(present(activationQuality)) then
!       ! q already computed
!     else
!       call compute_mesh_quality_sizeShape(mesh,metric,q)
!     end if
    if(.not.isq_allo) then
      call compute_mesh_quality_sizeShape(mesh,metric,q)
    end if

    is_bad_ele = q(:)<threshold_quality_to_repair
  
    if(count(is_bad_ele).eq.0_ip) then
      if(present(mapNodes_input_to_adapted_mesh).and.mesh%npoin>0_ip) then
        nullify(mapNodes_input_to_adapted_mesh)
        call memory_alloca(memor_adapt,'mapNodes_input_to_adapted_mesh','mod_adapt',mapNodes_input_to_adapted_mesh,mesh%npoin)
        mapNodes_input_to_adapted_mesh = (/(ielem, ielem=1_ip,mesh%npoin, 1_ip)/)
      end if
      if(present(isModifiedMesh)) then
        isModifiedMesh = .false.
      end if
      return
    else
      if(present(isModifiedMesh)) then
        isModifiedMesh = .true.
      end if
    end if

    call node_to_elems%set(mesh)
    do ilevel = 1,numNieghLevels
      is_bad_ele_prev = is_bad_ele
      do ielem=1,mesh%nelem
        if(is_bad_ele_prev(ielem)) then
          do inode=1,size(mesh%lnods,1)
            call node_to_elems%get_elems(mesh%lnods(inode,ielem),elems_adjToNode)
            !print*,kfl_paral," -> ",elems_adjToNode
            is_bad_ele(elems_adjToNode) = .true.
          end do
        end if
      end do
    end do

    iele_good = 0_ip
    iele_bad  = 0_ip
    do ielem=1,mesh%nelem
      if(is_bad_ele(ielem)) then
        iele_bad = iele_bad+1_ip
        list_bad(iele_bad) = ielem
      else
        iele_good = iele_good+1_ip
        list_good(iele_good) = ielem
      end if
    end do

  !  print*,kfl_paral,"    ",mesh%nelem,"   vs bad ampli: ",iele_bad

    if(iele_good>0_ip) then
      nullify(lnods_good)
      call memory_alloca(memor_adapt,'lnods_good','mod_adapt',lnods_good,INT(size(mesh%lnods,1),ip),iele_good)
      lnods_good = mesh%lnods(:,list_good(1:iele_good))

      nullify(lnods_bad)
      call memory_alloca(memor_adapt,'lnods_bad' ,'mod_adapt',lnods_bad ,INT(size(mesh%lnods,1),ip),iele_bad)
      lnods_bad = mesh%lnods(:,list_bad(1:iele_bad))

      call memory_deallo(memor_adapt,'mesh%lnods' ,'mod_adapt',mesh%lnods )
      nullify(mesh%lnods)
      call memory_alloca(memor_adapt,'lnods','mod_adapt',mesh%lnods,INT(size(lnods_bad,1),ip),iele_bad)
      mesh%nelem = iele_bad
      mesh%lnods = lnods_bad(:,1:iele_bad)
      call memory_deallo(memor_adapt,'lnods_bad' ,'mod_adapt',lnods_bad )

      ltype_save = mesh%ltype(1_ip)
      call memory_deallo(memor_adapt,'ltype'      ,'mod_adapt',mesh%ltype )
      call memory_alloca(memor_adapt,'ltype'      ,'mod_adapt',mesh%ltype,mesh%nelem)
      call memory_deallo(memor_adapt,'leinv_loc'  ,'mod_adapt',mesh%leinv_loc )
      call memory_alloca(memor_adapt,'leinv_loc'  ,'mod_adapt',mesh%leinv_loc,mesh % nelem)
      call memory_deallo(memor_adapt,'perme'      ,'mod_adapt',mesh%perme )
      call memory_alloca(memor_adapt,'perme'      ,'mod_adapt',mesh%perme,mesh % nelem)
      mesh%ltype = ltype_save
      do ielem = 1,mesh % nelem
         mesh % leinv_loc(ielem) = ielem
         mesh % perme(ielem) = ielem
      end do
    end if
    !
    !
    !
    call adapt_mesh_to_metric_private(mesh,metric,mapNodes_input_to_adapted_mesh_private)
    !
    !
    !
    if(iele_good>0_ip) then
      iele_aux = mesh%nelem
      nullify(lnods_aux)
      call memory_alloca(memor_adapt,'lnods_aux' ,'mod_adapt',lnods_aux ,INT(size(mesh%lnods,1),ip),iele_aux)
      lnods_aux = mesh%lnods

      mesh%nelem = iele_good + iele_aux
      call memory_deallo(memor_adapt,'mesh%lnods' ,'mod_adapt',mesh%lnods )
      nullify(mesh%lnods)
      call memory_alloca(memor_adapt,'lnods','mod_adapt',mesh%lnods,INT(size(lnods_bad,1),ip),mesh%nelem)
      !mesh%lnods(:,1:iele_good) = lnods_good
      do inode=1,size(mesh%lnods,1)
        mesh%lnods(inode,1:iele_good) = mapNodes_input_to_adapted_mesh_private(lnods_good(inode,:))
      end do
      mesh%lnods(:,(iele_good+1):mesh%nelem) = lnods_aux
      call memory_deallo(memor_adapt,'lnods_good' ,'mod_adapt',lnods_good)
      call memory_deallo(memor_adapt,'lnods_aux' ,'mod_adapt',lnods_aux)

      call memory_deallo(memor_adapt,'ltype'      ,'mod_adapt',mesh%ltype )
      call memory_alloca(memor_adapt,'ltype'      ,'mod_adapt',mesh%ltype,mesh%nelem)
      call memory_deallo(memor_adapt,'leinv_loc'  ,'mod_adapt',mesh%leinv_loc )
      call memory_alloca(memor_adapt,'leinv_loc'  ,'mod_adapt',mesh%leinv_loc,mesh % nelem)
      call memory_deallo(memor_adapt,'perme'      ,'mod_adapt',mesh%perme )
      call memory_alloca(memor_adapt,'perme'      ,'mod_adapt',mesh%perme,mesh % nelem)
      mesh%ltype = ltype_save
      do ielem = 1,mesh % nelem
         mesh % leinv_loc(ielem) = ielem
         mesh % perme(ielem) = ielem
      end do
    end if
    
  end if
  
  if(present(mapNodes_input_to_adapted_mesh)) then ! if present, allocate it
    nullify(mapNodes_input_to_adapted_mesh)
    call memory_alloca(memor_adapt,'mapNodes_input_to_adapted','mod_adapt',mapNodes_input_to_adapted_mesh,size(mapNodes_input_to_adapted_mesh_private))
    mapNodes_input_to_adapted_mesh = mapNodes_input_to_adapted_mesh_private
    call memory_deallo(memor_adapt,'mapNodes_input_to_adapted_mesh','mod_adapt',mapNodes_input_to_adapted_mesh_private)
  end if
  
  call out_firstLast(mesh,metric,file_out)
  
  if(do_warnValidity) then
    ! check mesh validity.. just in case
    if(isq_allo) then
      call quality_deallo(q)
    end if
    
    call compute_mesh_quality_sizeShape(mesh,metric,q)
    
    call print_q_stats(q)
    
    if(minval(q)<minQ_validity) then
      !call runend('Invalid mesh AFTER mesh adaption (ensure it was valid before or check what happened..)') 
      call messages_live('Invalid mesh AFTER mesh adaption','WARNING')
    end if
  end if
  
  if(isq_allo) then
    call quality_deallo(q)
  end if
  

  if(out_performance) then
    call cpu_time(t1)
    time_total = t1-t0

    call compute_mesh_quality_sizeShape(mesh,metric,q)
    call do_print_q_stats(q)
    
    call out_performance_profile()
  end if
  
end subroutine adapt_mesh_to_metric
!
!
!
subroutine out_performance_profile()
  implicit none
  
  if(out_performance) then
   write(*,1) time_total,(time_coll+time_split+time_swap+time_smooth),time_coll,time_split, time_swap, time_smooth
   1  format(/,&
   5x,'|---------------------------|',/, &
   5x,'| Total time     : ',f8.2,' |',/, &
   5x,'|   Time_oper    : ',f8.2,' |',/, &
   5x,'|     time_coll  : ',f8.2,' |',/, &
   5x,'|     time_split : ',f8.2,' |',/, &
   5x,'|     time_swap  : ',f8.2,' |',/, &
   5x,'|     time_smooth: ',f8.2,' |',/, &
   5x,'|---------------------------|',/, &
   /) !5x,'| Num inverted elements: ',i8,' |',
  end if
  
end subroutine out_performance_profile
!
!
!
subroutine adapt_mesh_to_metric_private(mesh,metric,mapNodes_input_to_adapted_mesh)
  use mod_smoothing,    only: smooth_mesh
  use mod_meshTopology, only: setBoundaryEdgeData
  use mod_meshTopology, only: getIsBoundaryNode
  
  use mod_quality,      only: compute_mesh_quality_shape,compute_mesh_quality_sizeShape
  use def_master, only: kfl_paral
  
  implicit none
  
  type(mesh_type_basic),  intent(inout)    :: mesh
  type(mesh_metric_type), intent(inout)    :: metric
  integer(ip), pointer, optional,   intent(inout) :: mapNodes_input_to_adapted_mesh(:)
  
  integer(ip) :: i_repair
  
  real(rp)    :: tol_length_collapse, tol_length_split, tol_qual_swap, tol_qual_smooth
  logical(lg) :: do_check_qual_coll , do_check_qual_split
  integer(ip) :: iswap
  integer(ip) :: iout
  ! character(len=200) :: file_out
  real(rp) :: t0,t1

  integer(ip), pointer  :: mapNodes_new_to_old(:)
  integer(ip) :: iaux, node_old, node_new
  
  type(edgeData_type) :: edgeData_boun
  logical(lg), pointer:: isBouNode_iniMesh(:)
  real(rp), pointer :: q(:)
  !
  time_split  = 0.0_rp
  time_coll   = 0.0_rp
  time_swap   = 0.0_rp
  time_smooth = 0.0_rp
  
!   if(mesh%npoin>0_ip) then
!     call compute_mesh_quality_shape(mesh,q)
!     print*,kfl_paral," pre adapt -> minval(q): ",minval(q(1:mesh%nelem)),"- maxval(q): ",maxval(q(1:mesh%nelem))," - nelem: ",mesh%nelem
!     call memory_deallo(memor_adapt,'q','compute_mesh_quality_sizeShape',q)
!     call compute_mesh_quality_sizeShape(mesh,metric,q)
!     print*,kfl_paral," pre adapt -> minval(q): ",minval(q(1:mesh%nelem)),"- maxval(q): ",maxval(q(1:mesh%nelem))," - nelem: ",mesh%nelem
!     call memory_deallo(memor_adapt,'q','compute_mesh_quality_sizeShape',q)
!   end if
  !
!   print*,'abelcruz modification: tol_swaps and tol_splits, and non-check improvement in first split to force longer-edge splits'
  
  nullify(mapNodes_new_to_old)
  call memory_alloca(memor_adapt,'mapNodes_new_to_old','mod_adapt',mapNodes_new_to_old,size(mesh%coord,2))
  mapNodes_new_to_old(:) = (/ (iaux, iaux = 1_ip, size(mesh%coord,2)) /) ! identity mapping to start
  
  if(present(mapNodes_input_to_adapted_mesh)) then ! if present, allocate it
    nullify(mapNodes_input_to_adapted_mesh)
    call memory_alloca(memor_adapt,'mapNodes_input_to_adapted_mesh','mod_adapt',mapNodes_input_to_adapted_mesh,mesh%npoin)! size(mesh%coord,2)
  end if
  
  edgeData_boun = setBoundaryEdgeData(mesh%lnods)
  
  call getIsBoundaryNode(mesh,isBouNode_iniMesh)
  
  if(out_debug_text) then
    print*,'Num points and elems before repair'
    print*,size(mesh%coord,2)
    print*,size(mesh%lnods,2)
  end if

  iout = -1_ip
  call out_with_q(mesh,metric,file_out,iout)
  
  do i_repair = 1,numRepairs
    
    if(out_debug_text.and.out_verbose) then
      print*,"i_repair: ",i_repair
      print*,'Num points:',size(mesh%coord,2)
      print*,'Num elems: ',size(mesh%lnods,2)
    end if

    if(out_debug_text.and.out_verbose) print*,"_____COLLAPSE___________________________________________________________________________"
    tol_length_collapse = tols_coll(  i_repair)
    do_check_qual_coll  = doCheckColl(i_repair)
    if(out_debug_text.and.out_verbose) print '(" -> collapse: ",f3.1,1x , L1)',tol_length_collapse,do_check_qual_coll
    if((out_debug_text.and.out_verbose.and.out_time).or.out_performance) call cpu_time(t0)
    !
    call repair_mesh_edgeLengths(mesh,metric,tol_length_collapse,do_check_qual_coll,&
      mapNodes_new_to_old, edgeData_boun, isBouNode_iniMesh) ! collapse
    !
    if((out_debug_text.and.out_verbose.and.out_time).or.out_performance) call cpu_time(t1)
    if(out_debug_text.and.out_verbose.and.out_time) print '("    Time = ",f6.0," sec")',(t1-t0)
    if(out_performance) time_coll = time_coll+(t1-t0)
    call out_with_q(mesh,metric,file_out,iout)
    !
    if(out_debug_text.and.out_verbose) print*,"_____SPLIT______________________________________________________________________________"
    tol_length_split    = tols_split(  i_repair)
    do_check_qual_split = doCheckSplit(i_repair)
    if(out_debug_text.and.out_verbose) print '(" -> split: ",f3.1,1x , L1)',tol_length_split,do_check_qual_split
    if((out_debug_text.and.out_verbose.and.out_time).or.out_performance) call cpu_time(t0)
    !
    call repair_mesh_edgeLengths(mesh,metric,tol_length_split   ,do_check_qual_split,&
      mapNodes_new_to_old, edgeData_boun, isBouNode_iniMesh) !split
    !
    if((out_debug_text.and.out_verbose.and.out_time).or.out_performance) call cpu_time(t1)
    if(out_debug_text.and.out_verbose.and.out_time) print '("    Time = ",f6.0," sec")',(t1-t0)
    if(out_performance) time_split = time_split+(t1-t0)
    call out_with_q(mesh,metric,file_out,iout)
    !
    if(out_debug_text.and.out_verbose) print*,"_____SWAP______________________________________________________________________________"
    do iswap=1,num_swaps(i_repair)
      tol_qual_swap       = tols_swap_ini(i_repair) + (iswap-1)*tols_swap_step(i_repair)
      if(out_debug_text.and.out_verbose) print '(" -> swap: target q ",f3.1)',tol_qual_swap
      if((out_debug_text.and.out_verbose.and.out_time).or.out_performance) call cpu_time(t0)
      !
      call swap_mesh_edges(mesh,metric,tol_qual_swap, mapNodes_new_to_old, edgeData_boun, isBouNode_iniMesh)
      !
      if((out_debug_text.and.out_verbose.and.out_time).or.out_performance) call cpu_time(t1)
      if(out_debug_text.and.out_verbose.and.out_time) print '("    Time = ",f6.0," sec")',(t1-t0)
      if(out_performance) time_swap = time_swap+(t1-t0)
      call out_with_q(mesh,metric,file_out,iout)
    end do
    !
    if(out_debug_text.and.out_verbose) print*,"______SMOOTH____________________________________________________________________________"
    tol_qual_smooth     = tols_smooth( i_repair)
    if(out_debug_text.and.out_verbose) print '(" -> smoothing: target q ",f3.1)',tol_qual_smooth
    if((out_debug_text.and.out_verbose.and.out_time).or.out_performance) call cpu_time(t0)
    !
    call smooth_mesh(mesh,metric,tol_qual_smooth)
    !
    if((out_debug_text.and.out_verbose.and.out_time).or.out_performance) call cpu_time(t1)
    if(out_debug_text.and.out_verbose.and.out_time) print '("    Time = ",f6.0," sec")',(t1-t0)
    if(out_performance) time_smooth = time_smooth+(t1-t0)
    call out_with_q(mesh,metric,file_out,iout)
    !
  end do
  
  if(out_debug_text.and.out_verbose) print*,'Mesh adaptation finalized'
  if(out_debug_text.and.out_verbose) print*,'Num points:',size(mesh%coord,2)
  if(out_debug_text.and.out_verbose) print*,'Num elems: ',size(mesh%lnods,2)
  
  if(present(mapNodes_input_to_adapted_mesh)) then
    mapNodes_input_to_adapted_mesh(:) = 0_ip
    do node_new=1,size(mapNodes_new_to_old)
      node_old = mapNodes_new_to_old(node_new)
      if( node_old>0_ip) then
        mapNodes_input_to_adapted_mesh(node_old) = node_new
      end if
    end do
  end if
  call memory_deallo(memor_adapt,'mapNodes_new_to_old','mod_adapt',mapNodes_new_to_old)
  call memory_deallo(memor_adapt,'isBouNode_iniMesh','mod_adapt',isBouNode_iniMesh)
  
  
!   if(mesh%npoin>0_ip) then
!     call compute_mesh_quality_shape(mesh,q)
!     print*,kfl_paral," post adapt -> minval(q): ",minval(q(1:mesh%nelem)),"- maxval(q): ",maxval(q(1:mesh%nelem))," - nelem: ",mesh%nelem
!     call memory_deallo(memor_adapt,'q','compute_mesh_quality_sizeShape',q)
!     call compute_mesh_quality_sizeShape(mesh,metric,q)
!     print*,kfl_paral," post adapt -> minval(q): ",minval(q(1:mesh%nelem)),"- maxval(q): ",maxval(q(1:mesh%nelem))," - nelem: ",mesh%nelem
!     call memory_deallo(memor_adapt,'q','compute_mesh_quality_sizeShape',q)
!   end if
  
  
  return 
end subroutine adapt_mesh_to_metric_private
!
!
!
!--------- SUBMODULE 1: _adapt_lengthRepair ---------------------------------------------------------------------------
! include "_adapt_lengthRepair.f90"
!
!
!
subroutine repair_mesh_edgeLengths(mesh,metric,tol_length,do_checkQual,mapNodes_new_to_old,edgeData_boun,isBouNode_iniMesh)
  use mod_meshTopology,       only: setIsBoundaryNodeFromIniMesh

  use def_master,    only: kfl_paral
  use mod_out_paraview, only: out_paraview_inp
  
  implicit none
  
  type(mesh_type_basic),   intent(inout) :: mesh
  type(mesh_metric_type),  intent(inout) :: metric
  real(rp),                intent(in)    :: tol_length
  logical(lg),             intent(in)    :: do_checkQual
  integer(ip), pointer,    intent(inout) :: mapNodes_new_to_old(:)
  type(edgeData_type), intent(in)        :: edgeData_boun
  logical(lg), pointer:: isBouNode_iniMesh(:)
  !
  
  logical(lg) :: isModifiedMesh_frozen, hasBeenModified
  integer(ip) :: iter
  integer(ip), parameter :: max_iter_frozen = 1000
  integer(ip), parameter :: max_iter_global = 100
  integer(ip) :: counter_global
!   integer(ip) ::counter_global_inside
  logical(lg) :: isModified_global

  integer(ip) :: current_strategy
  integer(ip) :: numTaggedEdges
  integer(ip), pointer :: taggedEdges_to_node(:,:)
  logical(lg), pointer :: isPerformedEdge(:)
  logical(lg), pointer :: isRemovedNode(:)

  logical(lg), pointer   :: isBoundaryNode(:)
  integer(ip) :: numPerfEdges
  integer(ip) :: iout
  
  iout= -1_ip
  
  if ( tol_length.lt.(1.0_rp) ) then ! COLLAPSE
    current_strategy = adapt_strat_collapse
  else !SPLIT 
    current_strategy = adapt_strat_split
  end if
  
  counter_global = 0
  isModified_global = .true.
  numPerfEdges = 0_ip
  do while(isModified_global)
  
    call set_tagged_edges_byLength(current_strategy,tol_length,mesh,metric,numTaggedEdges,taggedEdges_to_node)
    
    if(numTaggedEdges==0) then
      return
    else
      nullify(isPerformedEdge)
      call memory_alloca(memor_adapt,'isPerformedEdge','repair_mesh_edgeLengths',isPerformedEdge,int(size(taggedEdges_to_node,2),ip))
      isPerformedEdge = .false.
    
      nullify(isRemovedNode)
      call memory_alloca(memor_adapt,'isRemovedNode','repair_mesh_edgeLengths',isRemovedNode,mesh%npoin)
      isRemovedNode = .false.

      !call getIsBoundaryNode(mesh,isBoundaryNode)
      call setIsBoundaryNodeFromIniMesh(isBoundaryNode,isBouNode_iniMesh,mapNodes_new_to_old)
    end if
  
    hasBeenModified = .false.
    isModifiedMesh_frozen = .true.
    iter = 0
    
!     if(output_steps_edgeRep_paraview) then
!       print*,'iter: ',iter
!       call out_with_q(mesh,metric,file_out//"_edgeRep",iout)
!     end if
! call out_paraview_inp(mesh,filename='debugColl_'//int2str(iter),nodeField=metric%M(1,1,:))
    do while ( isModifiedMesh_frozen.and.(iter<max_iter_frozen) )
      iter = iter+1
      
      !print*,'repair_mesh_edgeLengths_frozen: ', iter
      call repair_mesh_edgeLengths_frozen(mesh,metric,&
        current_strategy,do_checkQual,taggedEdges_to_node,&
        isBoundaryNode,isPerformedEdge,isRemovedNode,isModifiedMesh_frozen,numPerfEdges,&
        mapNodes_new_to_old,edgeData_boun)
      
      hasBeenModified = isModifiedMesh_frozen.or.hasBeenModified
      
!       call out_paraview_inp(mesh,filename='debugColl_'//int2str(iter),nodeField=metric%M(1,1,:))
!       if(output_steps_edgeRep_paraview) then
!         print*,'iter: ',iter
!         call out_with_q(mesh,metric,file_out//"_edgeRep",iout)
!       end if
    end do
    
!     print*,kfl_paral,'  -> removeDeletedNodes...'
    call removeDeletedNodes(mesh,metric,isRemovedNode,mapNodes_new_to_old)
!     print*,kfl_paral,'  -> ... removeDeletedNodes done'
    
    if(output_steps_edgeRep_paraview) then
      call out_with_q(mesh,metric,file_out//"_edgeRep",iout)
    end if
  
    call memory_deallo(memor_adapt,'taggedEdges_to_node','repair_mesh_edgeLengths',taggedEdges_to_node)
    call memory_deallo(memor_adapt,'isPerformedEdge',    'repair_mesh_edgeLengths',isPerformedEdge    )
    call memory_deallo(memor_adapt,'isRemovedNode',      'repair_mesh_edgeLengths',isRemovedNode      )
    call memory_deallo(memor_adapt,'isBoundaryNode',     'repair_mesh_edgeLengths',isBoundaryNode     )
    
    counter_global = counter_global+1
    isModified_global = hasBeenModified .and.(counter_global<max_iter_global)
    
  end do
  
  return 
end subroutine repair_mesh_edgeLengths
!
!
!
subroutine repair_mesh_edgeLengths_frozen(mesh,metric,&
  current_strategy,do_checkQual,taggedEdges_to_node,&
  isBoundaryNode,isPerformedEdge,isRemovedNode,isModifiedMesh,numPerfEdges,&
  mapNodes_new_to_old,edgeData_boun)
  use mod_cavity, only: cavity_insert_point, cavity_reinsert
  use mod_quality,            only: compute_minQuality_sizeShape_cavity
  use mod_meshTopology,       only: node_to_elems_type
  
  use def_master,    only: kfl_paral
  
  implicit none
  
  type(mesh_type_basic),    intent(inout) :: mesh
  type(mesh_metric_type),   intent(inout) :: metric
  integer(ip),              intent(in)    :: current_strategy
  logical(lg),              intent(in)    :: do_checkQual
  integer(ip), pointer,     intent(in)    :: taggedEdges_to_node(:,:)
  logical(lg), pointer,     intent(in)    :: isBoundaryNode(:)
  logical(lg), pointer,     intent(inout) :: isPerformedEdge(:)
  logical(lg), pointer,     intent(inout) :: isRemovedNode(:)
  logical(lg),              intent(out)   :: isModifiedMesh
  integer(ip), intent(inout) :: numPerfEdges
  integer(ip), pointer,    intent(in) :: mapNodes_new_to_old(:)
  type(edgeData_type), intent(in)        :: edgeData_boun
  
  integer(ip) :: numTaggedEdges, numTaggedEdges_remaining
    
  logical(lg) :: isFrozenElem(mesh%nelem)
  integer(ip) :: iedge
  integer(ip) :: n1,n2
  integer(ip) :: n1_initialMesh,n2_initialMesh
  integer(ip), pointer :: elems_cavity(:)
  
  logical(lg) :: isValidRemesh, isRemovedEdge,isEdgeAllowed
  logical(lg) :: isBoundEdge, isPotentialBoundEdge
  
  integer(ip) :: last_point_new, last_point_global, last_elem_new, ini_elem
  real(rp)    :: p_insert(mesh%ndime)
  type(mesh_metric_type):: metric_new
  integer(ip) :: max_newPoints, max_newElems
  real(rp),    pointer  :: coord_new(:,:)
  integer(ip), pointer  :: lnods_new(:,:)
  integer(ip), pointer :: Tcav_remeshed(:,:)
  integer(ip) :: num_new_elems
  
  integer(ip) :: nodeId_toInsert
  logical(lg) :: isNodeId_existent
  
  logical(lg) :: isExceededAllocatedSize
  
  real(rp) :: q_prev, q_new
  type(node_to_elems_type) :: node_to_elems
  type(mesh_type_basic)    :: mesh_aux
  !
  integer(ip), pointer  :: elems_aux(:)
  !
  real(rp) :: M_p_insert(1,1)
  !
  
  call node_to_elems%set(mesh)
  
  numTaggedEdges = size(taggedEdges_to_node,2)
  numTaggedEdges_remaining = numTaggedEdges- count(isPerformedEdge)
  max_newPoints = numTaggedEdges_remaining 
  nullify(coord_new)
  call memory_alloca(memor_adapt,'coord_new','repair_mesh_edgeLengths_frozen',coord_new,mesh%ndime,max_newPoints)
  
  select case (current_strategy)
    case  (adapt_strat_collapse)
      if(mesh%ndime==2) then ! each node has an adjacency approx of 6 elems, 2 nodes per edge
        max_newElems = numTaggedEdges_remaining*6_ip*2_ip
      else ! each node has an adjacency approx of 16 elems, 2 nodes per edge
        max_newElems = numTaggedEdges_remaining*16_ip*2_ip
      end if
    case  (adapt_strat_split)
      if(mesh%ndime==2) then ! and edge is shared by 2 triangles (each one split in 2)
        max_newElems = numTaggedEdges_remaining*4_ip
      else ! edge is contained at most by 10 elems (in average), each element is split in 2
        max_newElems = numTaggedEdges_remaining*10_ip*2_ip
      end if
    case default
      call runend('Not implemented strategy in repair_mesh_edges_frozen')
  end select
  nullify(lnods_new)
  call memory_alloca(memor_adapt,'lnods_new','repair_mesh_edgeLengths_frozen',lnods_new,int(size(mesh%lnods,1),ip),max_newElems)  
  
  call metric_new%alloca(metric%dim,max_newPoints,metric%isSizeField)
  
  last_point_new    = 0_ip
  last_point_global = size(mesh%coord,2)
  last_elem_new     = 0_ip!size(mesh%lnods,2)
  
  isFrozenElem(:) = .false.
  isModifiedMesh  = .false.
  
!   print*,"numTaggedEdges: ",numTaggedEdges
  
  do iedge = 1,numTaggedEdges
    n1 = taggedEdges_to_node(1,iedge)
    n2 = taggedEdges_to_node(2,iedge)
    isRemovedEdge = isRemovedNode(n1).or.isRemovedNode(n2)
    isPotentialBoundEdge   = isBoundaryNode(n1).and.isBoundaryNode(n2)
    if(isPotentialBoundEdge) then
      if(current_strategy.eq.adapt_strat_collapse) then
        ! can not collapse two boundary edges, even if edge is interior
        isBoundEdge = isPotentialBoundEdge
      else
        n1_initialMesh = mapNodes_new_to_old(n1)
        n2_initialMesh = mapNodes_new_to_old(n2)
        isBoundEdge = edgeData_boun%isEdge(n1_initialMesh,n2_initialMesh)
      end if 
    else
      isBoundEdge = .false.
    end if
    !
    isEdgeAllowed = ( .not.isPerformedEdge(iedge) ).and.&
                    ( .not.isRemovedEdge          ).and.&
                    ( .not.isBoundEdge          )
    !
    if(isEdgeAllowed) then
      
      select case (current_strategy)
        case  (adapt_strat_collapse)
          call node_to_elems%get_edgeAdjacency(n1,n2,elems_cavity)
        case  (adapt_strat_split)
          call node_to_elems%get_edgeContainers(n1,n2,elems_cavity)
      end select
      
      if( .not.(any(isFrozenElem(elems_cavity))) ) then
        
        if((current_strategy.eq.adapt_strat_collapse).and.(isBoundaryNode(n1).or.isBoundaryNode(n2))) then
          ! if one of the two nodes is boundary, the collapse keeps its id
          if(isBoundaryNode(n1).and.isBoundaryNode(n2)) then
            call runend('Not possible to collapse and edge with two boundary nodes...')
          end if
          isNodeId_existent = .true.
          if(isBoundaryNode(n1)) then
            nodeId_toInsert = n1
          else
            nodeId_toInsert = n2
          end if
          !p_insert = mesh%coord(:,nodeId_toInsert) !=> not necessary to be set
        else
          last_point_global = last_point_global + 1_ip
          last_point_new    = last_point_new    + 1_ip
          call metric%generate_edge_point(n1,n2,mesh%coord(:,n1),mesh%coord(:,n2),p_insert,M_p_insert)
          nodeId_toInsert = last_point_global
          isNodeId_existent = .false.
        end if
        
        if( do_checkQual ) then
          q_prev = compute_minQuality_sizeShape_cavity(mesh%coord,mesh%lnods(:,elems_cavity),metric)
        end if
        
        if(isNodeId_existent) then
          call cavity_reinsert(nodeId_toInsert,mesh%lnods(:,elems_cavity),Tcav_remeshed)
        else
          call cavity_insert_point(nodeId_toInsert, mesh%lnods(:,elems_cavity), Tcav_remeshed)
        end if
        
        num_new_elems = size(Tcav_remeshed,2)
        isExceededAllocatedSize = (last_elem_new + num_new_elems)>max_newElems ! no need to check the nodes (exactly calculated)
        isValidRemesh = .not.isExceededAllocatedSize
      
        if(isValidRemesh) then ! check quality
          
          if(isNodeId_existent) then
            q_new = compute_minQuality_sizeShape_cavity(mesh%coord,Tcav_remeshed(:,:),metric)
          else
            coord_new(:,last_point_new) = p_insert
            call metric_new%set_metric_node( last_point_new , M_p_insert )
            q_new = compute_minQuality_sizeShape_cavity( mesh%coord,Tcav_remeshed(:,:),metric,&
              p_insert, metric_new%M(:,:,last_point_new) )
          end if
          
          if( do_checkQual ) then  
            isValidRemesh = (q_new>q_prev).and.(q_new>minQ_validity)
          else
            isValidRemesh = (q_new>minQ_validity) !.and.(q_new>q_prev) !(q_new>q_prev/2.0_rp)
          end if
        end if
      
        if(isValidRemesh) then
          
          isModifiedMesh = .true.
          
          numPerfEdges = numPerfEdges+1_ip
          
          isPerformedEdge(iedge)     = .true.
          isFrozenElem(elems_cavity) = .true.
          
          if(current_strategy==adapt_strat_collapse) then
            
            isRemovedNode(n1) = .true.
            isRemovedNode(n2) = .true.
            
            if(isNodeId_existent) then
              isRemovedNode(nodeId_toInsert) = .false.
            end if
            
          end if
          
          ini_elem      = last_elem_new+1
          last_elem_new = last_elem_new+num_new_elems
          lnods_new(:,ini_elem:last_elem_new) = Tcav_remeshed
          call memory_deallo(memor_adapt,'Tcav_remeshed','repair_mesh_edgeLengths_frozen',Tcav_remeshed)
          
        else
          if(.not.isNodeId_existent) then
            last_point_global = last_point_global-1_ip
            last_point_new    = last_point_new   -1_ip
          end if
          if(isExceededAllocatedSize) then
            call memory_deallo(memor_adapt,'elems_cavity','repair_mesh_edgeLengths_frozen',elems_cavity)
            go to 666
          end if
        end if
        
      end if
      
      call memory_deallo(memor_adapt,'elems_cavity','repair_mesh_edgeLengths_frozen',elems_cavity)
    end if
  end do
  
  666 continue
  
!   print*,kfl_paral,'   ->set_mesh_new_modify... ',isModifiedMesh
  if (isModifiedMesh) then
!     print*,kfl_paral,'   ->set_mesh_new_modify'
    call set_mesh_new_modify(mesh,coord_new,lnods_new,last_point_new,last_elem_new,isFrozenElem)!,isRemovedNode)
!     print*,kfl_paral,'   ->metric'
    call metric%merge(metric_new,last_point_new)!,isRemovedNode-> do not remove here)
!     print*,kfl_paral,'   ->metric done'
  else
    call metric_new%deallo() ! performed in metric%merge
    call memory_deallo(memor_adapt,'coord_new','set_tagged_edges',coord_new)! performed in set_mesh_new
    call memory_deallo(memor_adapt,'lnods_new','set_tagged_edges',lnods_new)! performed in set_mesh_new
  end if
!   print*,kfl_paral,'   ->...set_mesh_new_modify done'
  
  return 
end subroutine repair_mesh_edgeLengths_frozen
!
!
!
subroutine set_tagged_edges_byLength(current_strategy,tol_length,mesh,metric,numTaggedEdges,taggedEdges_to_node)
  use mod_meshEdges, only: compute_mesh_edgeLengths
  implicit none
  
  integer(ip),            intent(in) :: current_strategy
  real(rp),               intent(in) :: tol_length
  type(mesh_type_basic),  intent(in) :: mesh
  type(mesh_metric_type), intent(in) :: metric
  !
  integer(ip), pointer, intent(inout)  :: taggedEdges_to_node(:,:)
  integer(ip), intent(out) :: numTaggedEdges
  real(rp),    pointer               :: edge_lengths(:)
  integer(ip), pointer               :: edge_to_node(:,:)
  !
  integer(ip), pointer  :: perm_edges_byLength(:)
  integer(ip) :: iedge, numEdges, theEdge
  real(rp) :: l_edge
  !
  
  call compute_mesh_edgeLengths(mesh,metric,edge_to_node,edge_lengths)
  
  numEdges = size(edge_to_node,2)
  
  select case (current_strategy)
    case  (adapt_strat_collapse)
      call sort_perm_reals_increasing(edge_lengths,perm_edges_byLength)
    case  (adapt_strat_split)
      call sort_perm_reals_decreasing(edge_lengths,perm_edges_byLength)
    case default
      call runend('Nos implemented strategy in set_tagged_edges')
  end select
  
  loopEdges: do iedge=1,numEdges
    theEdge = perm_edges_byLength(iedge)
    l_edge = edge_lengths(theEdge)
    
    select case (current_strategy)
      case  (adapt_strat_collapse)
        if(l_edge>tol_length) exit loopEdges
      case  (adapt_strat_split)
        if(l_edge<tol_length) exit loopEdges
    end select
    
  end do loopEdges
  numTaggedEdges = iedge-1
  
  nullify(taggedEdges_to_node)
  if(numTaggedEdges>0) then
    call memory_alloca(memor_adapt,'tagged_edges','set_tagged_edges',taggedEdges_to_node,2_ip,numTaggedEdges)
    taggedEdges_to_node(:,:) = edge_to_node( : , perm_edges_byLength(1:numTaggedEdges) )
  end if
  
  ! Deallocate and nullify
  call memory_deallo(memor_adapt,'edge_to_node','set_tagged_edges',edge_to_node)
  nullify(edge_to_node)
  
  call memory_deallo(memor_adapt,'edge_lengths','set_tagged_edges',edge_lengths)
  nullify(edge_lengths)
  
  call memory_deallo(memor_adapt,'perm_edges_byLength','set_tagged_edges',perm_edges_byLength)
  nullify(perm_edges_byLength)
  
end subroutine set_tagged_edges_byLength
!
!
!
!--------- SUBMODULE 2: _adapt_swap ---------------------------------------------------------------------------
! include "_adapt_swap.f90"
!
!
!
subroutine swap_mesh_edges(mesh,metric,tol_qual,mapNodes_new_to_old,edgeData_boun,isBouNode_iniMesh)
  use mod_meshTopology,       only: setIsBoundaryNodeFromIniMesh!getIsBoundaryNode
  use mod_quality,    only: compute_mesh_quality_sizeShape
  implicit none
  type(mesh_type_basic),  intent(inout)   :: mesh
  type(mesh_metric_type), intent(in)      :: metric
  real(rp),               intent(in)      :: tol_qual 
  integer(ip), pointer,   intent(inout)   :: mapNodes_new_to_old(:)
  type(edgeData_type), intent(in)        :: edgeData_boun
  logical(lg), pointer:: isBouNode_iniMesh(:)
  !
  
  logical(lg) :: isModifiedMesh_frozen
  integer(ip) :: iter
  integer(ip), parameter :: max_iter_frozen = 1000

  logical(lg), pointer   :: isBoundaryNode(:)
  integer(ip) :: iout
  character(10) :: iout_char
  real(rp), pointer:: q(:)
  !character(len=200) :: file_out
  
  !file_out = TRIM('./unitt/unitt_adaptivity/mesh_swap')
  
  !call getIsBoundaryNode(mesh,isBoundaryNode)
  call setIsBoundaryNodeFromIniMesh(isBoundaryNode,isBouNode_iniMesh,mapNodes_new_to_old)

  iout = -1_ip
  call out_with_q(mesh,metric,file_out//"_swap",iout)
  
  isModifiedMesh_frozen = .true.
  iter = 0
  do while ( isModifiedMesh_frozen.and.(iter<max_iter_frozen) )
    
    iter = iter+1
    call swap_mesh_edges_frozen(mesh,metric,isBoundaryNode,tol_qual,isModifiedMesh_frozen)
    
    if(output_steps_swap_paraview) then
      call out_with_q(mesh,metric,file_out//"_swap",iout)
    end if

  end do

  call memory_deallo(memor_adapt,'isBoundaryNode',     'repair_mesh_edgeLengths',isBoundaryNode     )

  return 
end subroutine swap_mesh_edges
!
!
!
subroutine swap_mesh_edges_frozen(mesh,metric,isBoundaryNode,tol_qual,isModifiedMesh)
  use mod_meshTopology, only: node_to_elems_type, getNodesCav
  use mod_quality,      only: compute_mesh_quality_sizeShape
  use mod_quality,      only: compute_minQuality_sizeShape_cavity, compute_minQuality_sizeshape_subset
  use mod_quality,      only: quality_deallo
  use mod_cavity,       only: cavity_reinsert
  implicit none
  type(mesh_type_basic),  intent(inout) :: mesh
  type(mesh_metric_type), intent(in)    :: metric
  real(rp),               intent(in)    :: tol_qual 
  logical(lg), pointer,   intent(in)    :: isBoundaryNode(:)
  logical(lg),            intent(out)   :: isModifiedMesh

  type(node_to_elems_type) :: node_to_elems
  integer(ip), pointer  :: perm_elems_byQ(:)
  integer(ip) :: iedge, n1, n2
  logical(lg) :: isFrozenElem(mesh%nelem)
  integer(ip), pointer :: elems_cavity(:)
  real(rp), pointer :: q(:)
  integer(ip) :: numNodCav
  integer(ip),pointer :: nodesCav(:)
  
  real(rp) :: q_bestSwap, q_current
  integer(ip), pointer :: T_bestSwap(:,:)
  !integer(ip), pointer :: cav_bestSwap(:)
  integer(ip), pointer :: Tcav_remeshed(:,:)
  integer(ip) :: knode, pointToReinsert
  logical(lg) :: isImproved

  logical(lg) :: isBoundEdge
  
  integer(ip), pointer:: lnods_new(:,:)
  integer(ip)         :: max_newElems, count_newElems, count_newPoints
  real(rp), pointer   :: coords_new_fake(:,:)
  
  integer(ip) :: numTaggedEdges
  integer(ip), pointer :: taggedEdges_to_node(:,:)
  logical(lg) :: isFrozenCavity
  real(rp)    :: eps_dim
  
  eps_dim = 0.1_rp**(14_rp/mesh%ndime) !1.0/( huge(1.0_rp)**(1.0/mesh%ndime) ) 
  !
  ! compute low quality elems
  ! sort edges according to lowest adjacent elem quality
  ! for each edge
  !   compute cavity of the edge
  !   for each boundary node of the cavity
  !     reinsert the node
  !     compute quality of the new cavity
  !     save the swap if it improves current quality
  ! that is perform the reinsertion of the _edge and cavity _node with highest _q  (_q,_edge,_node)

  call node_to_elems%set(mesh)
  call compute_mesh_quality_sizeShape(mesh,metric,q)
  
  call set_tagged_edges_byQ(tol_qual,mesh,node_to_elems,q,numTaggedEdges,taggedEdges_to_node)
  call quality_deallo(q)
  
  !max_newElems = ceiling(0.2_rp*mesh%nelem)
  if(mesh%ndime==2) then ! each node has an adjacency approx of 6 elems, 2 nodes per edge
    max_newElems = numTaggedEdges*6_ip*2_ip
  else ! each node has an adjacency approx of 16 elems, 2 nodes per edge
    max_newElems = numTaggedEdges*16_ip*2_ip
  end if
  nullify(lnods_new)
  call memory_alloca(memor_adapt,'lnods_new','repair_mesh_edgeLengths_frozen',lnods_new,int(size(mesh%lnods,1),ip),max_newElems)  
  
  isModifiedMesh = .false.
  isFrozenElem   = .false.
  count_newElems = 0_ip
  loopEdges: do iedge=1,numTaggedEdges
    !
    n1 = taggedEdges_to_node(1,iedge)
    n2 = taggedEdges_to_node(2,iedge)
    
    isBoundEdge = isBoundaryNode(n1).and.isBoundaryNode(n2)
    
    if(.not.isBoundEdge) then
      
      call node_to_elems%get_edgeContainers(n1,n2,elems_cavity)
    
      isFrozenCavity = any(isFrozenElem(elems_cavity))
      if( .not.isFrozenCavity ) then
        !!! ---- reinsert all nodes of cavity ---- !!!!
        q_bestSwap = compute_minQuality_sizeshape_subset(mesh,metric,elems_cavity)
        !print*,"----------------------------------------------"
        !print*,"      q_initial",q_bestSwap

        call getNodesCav(mesh%lnods(:,elems_cavity),numNodCav,nodesCav)
        
        !print*,"numNodCav: ",numNodCav
        !print*,"sizeCavEl: ",size(elems_cavity)
        isImproved = .false.
        do knode =1,numNodCav
          
          pointToReinsert = nodesCav(knode)
          !print*,"pointToReinsert: ",pointToReinsert
          
          call cavity_reinsert(pointToReinsert,mesh%lnods(:,elems_cavity),Tcav_remeshed)
          !print*,"sizeCavRe: ",size(Tcav_remeshed,2)
          
          q_current = compute_minQuality_sizeShape_cavity(mesh%coord,Tcav_remeshed(:,:),metric)
          !print*,"      q_current",q_current, "      ---> q_bestSwap: ",q_bestSwap
        
          if( q_current > q_bestSwap + eps_dim) then
            !print*," IS IMPROVED: ",q_bestSwap," to ",q_current
            if( isImproved ) then
              call memory_deallo(memor_adapt,'T_bestSwap'  ,'swap_mesh_edges_frozen',T_bestSwap)
            end if
          
            isImproved = .true.
            q_bestSwap = q_current
          
            nullify(T_bestSwap)
            call memory_alloca(memor_adapt,'T_bestSwap','swap_mesh_edges_frozen',T_bestSwap,int(size(Tcav_remeshed,1),ip),int(size(Tcav_remeshed,2),ip))
            T_bestSwap = Tcav_remeshed(:,:)
          end if
        
          call memory_deallo(memor_adapt,'Tcav_remeshed','swap_mesh_edges_frozen',Tcav_remeshed)
        end do
        
        if( isImproved ) then
          
          if( (count_newElems+size(T_bestSwap,2)) > max_newElems ) then
            go to 6666
          end if
          
          isModifiedMesh = .true.
          isFrozenElem(elems_cavity) = .true.
      
          lnods_new(:,(count_newElems+1):(count_newElems+size(T_bestSwap,2))) = T_bestSwap
          count_newElems = count_newElems+size(T_bestSwap,2)
      
          call memory_deallo(memor_adapt,'T_bestSwap'  ,'swap_mesh_edges_frozen',  T_bestSwap)
        end if
        call memory_deallo(memor_adapt,'nodesCav','swap_mesh_edges_frozen',nodesCav)
        
      end if !isFrozen
      
      call memory_deallo(memor_adapt,'elems_cavity','swap_mesh_edges_frozen',elems_cavity)
    end if !isBoundEdge
  end do loopEdges
  
  6666 continue
  
  
  if( isModifiedMesh ) then
    count_newPoints = 0_ip
    nullify(coords_new_fake)
    call memory_alloca(memor_adapt,'coords_new_fake','swap_mesh_edges_frozen',coords_new_fake,mesh%ndime,2_ip)!mesh%ndime,1_ip)
    call set_mesh_new_modify(mesh,coords_new_fake,lnods_new,count_newPoints,count_newElems,isFrozenElem)
    call memory_deallo(memor_adapt,'coords_new_fake','swap_mesh_edges_frozen',coords_new_fake)
  else
    call memory_deallo(memor_adapt,'lnods_new'      ,'set_tagged_edges',lnods_new)
  end if

!   if(associated(q)) then
!     call memory_deallo(memor_adapt,'q','swap_mesh_edges_frozen',q)
!   end if

  return 
end subroutine swap_mesh_edges_frozen
!
!
!
subroutine set_tagged_edges_byQ(tol_q,mesh,node_to_elems,q,numTaggedEdges,taggedEdges_to_node)
  use mod_meshEdges, only: compute_mesh_edgeQuality
  use mod_meshTopology,       only: node_to_elems_type
  implicit none
  
  real(rp),                intent(in) :: tol_q
  type(mesh_type_basic),   intent(in) :: mesh
!   type(mesh_metric_type),  intent(in) :: metric
  type(node_to_elems_type),intent(in) :: node_to_elems
  real(rp), pointer,       intent(in) :: q(:)
  !
  integer(ip), pointer, intent(inout)  :: taggedEdges_to_node(:,:)
  integer(ip), intent(out) :: numTaggedEdges
  real(rp),    pointer               :: edge_q(:)
  integer(ip), pointer               :: edge_to_node(:,:)
  !
  integer(ip), pointer  :: perm_edges_byQ(:)
  integer(ip) :: iedge, numEdges, theEdge
  real(rp) :: q_edge
  !
  
  call compute_mesh_edgeQuality(mesh,node_to_elems,q,edge_to_node,edge_q)
  
  call sort_perm_reals_increasing(edge_q,perm_edges_byQ)
  
  numEdges = size(edge_to_node,2)
  loopEdges: do iedge=1,numEdges
    theEdge = perm_edges_byQ(iedge)
    q_edge = edge_q(theEdge)
    
    if(q_edge>tol_q) exit loopEdges
  end do loopEdges
  numTaggedEdges = iedge-1
  
  nullify(taggedEdges_to_node)
  if(numTaggedEdges>0) then
    call memory_alloca(memor_adapt,'tagged_edges','set_tagged_edges_byQ',taggedEdges_to_node,2_ip,numTaggedEdges)
    taggedEdges_to_node(:,:) = edge_to_node( : , perm_edges_byQ(1:numTaggedEdges) )
  end if
  
  ! Deallocate and nullify
  call memory_deallo(memor_adapt,'edge_to_node',  'set_tagged_edges_byQ',edge_to_node)
  call memory_deallo(memor_adapt,'edge_q',  'set_tagged_edges_byQ',edge_q)
  call memory_deallo(memor_adapt,'perm_edges_byQ','set_tagged_edges_byQ',perm_edges_byQ)
  !
end subroutine set_tagged_edges_byQ
!
!
!
!--------- SUBMODULE 3: _adapt_auxMesh ---------------------------------------------------------------------------
! include "_adapt_auxMesh.f90"
!
!
!
subroutine out_with_q(mesh,metric,file_out,iout)
  use mod_out_paraview, only: out_paraview_inp
  use mod_quality,      only: compute_mesh_quality_sizeShape
  
  use def_master, only: kfl_paral
  use mod_strings,               only : integer_to_string

  use def_master, only: kfl_paral
  
  implicit none
  !
  type(mesh_type_basic)  , intent(in):: mesh
  type(mesh_metric_type) , intent(in):: metric
  character(len=*), intent(in) :: file_out
  integer(ip) , intent(inout) :: iout
  !
  character(20) :: iout_char
  real(rp), pointer :: q(:)
  !
  if(out_debug_paraview.and.output_steps_paraview) then
    iout = iout+1
    iout_char=int2str(iout)
    !print*,"iout: ",iout
    call compute_mesh_quality_sizeShape(mesh,metric,q)
    !print*,"kfl_paral: ",kfl_paral,"  -> minval(q): ",minval(q),"- maxval(q): ",maxval(q)
    call out_paraview_inp(mesh,filename=TRIM(file_out)//'_'//integer_to_string(kfl_paral)//'_'//iout_char,nodeField=metric%M(1,1,:),elemField=q)
    
    call memory_deallo(memor_adapt,'QUALITY','compute_mesh_quality_sizeShape',q)
    
  end if
  !
end subroutine out_with_q

  !
  !
  !
subroutine out_firstLast(mesh,metric,file_out)

  use def_master,     only: kfl_paral
  use mod_strings,    only: integer_to_string
  use mod_quality,    only: compute_mesh_quality_sizeShape
  use mod_debugTools, only: out_debug_paraview_firstLast, out_id_firstLast
  use mod_out_paraview, only: out_paraview_inp

  implicit none
  !
  type(mesh_type_basic)  , intent(in):: mesh
  type(mesh_metric_type) , intent(in):: metric
  character(len=*), intent(in) :: file_out
  !
  character(20) :: iout_char
  real(rp), pointer :: q(:)
  !    
  if(out_debug_paraview_firstLast) then
    
    print*,kfl_paral,"   ",out_id_firstLast
    !if(kfl_paral.eq.1) then
      out_id_firstLast = out_id_firstLast+1
    !end if
    !call PAR_BARRIER()
    
    iout_char=int2str(out_id_firstLast)
    !print*,"iout: ",iout
    call compute_mesh_quality_sizeShape(mesh,metric,q)
    !print*,"kfl_paral: ",kfl_paral,"  -> minval(q): ",minval(q),"- maxval(q): ",maxval(q)
    call out_paraview_inp(mesh,&
      filename=&
      '/Users/abel/Desktop/Work/CasesAlya/paraview/'//&
      TRIM(file_out)//'_FL_'//integer_to_string(kfl_paral)//'_'//iout_char,&
      nodeField=metric%M(1,1,:),elemField=q)
  
    call memory_deallo(memor_adapt,'QUALITY','compute_mesh_quality_sizeShape',q)
  end if
  !
end subroutine out_firstLast
!
!
!
function int2str(num) result(str)
  implicit none
  integer(ip), intent(in):: num
  character(20) :: str
  ! convert integer to string using formatted write
  write(str, '(i20)') num
  str = adjustl(str)
end function int2str
!
!
!
subroutine removeDeletedNodes(mesh_new,metric_new,isDeletedPoint,mapNodes_new_to_old)
  use mod_out_paraview
  implicit none
  
  type(mesh_type_basic),  intent(inout) :: mesh_new
  type(mesh_metric_type), intent(inout) :: metric_new
  logical(lg),            intent(inout) :: isDeletedPoint(:)  
  integer(ip), pointer,   intent(inout) :: mapNodes_new_to_old(:)
  !
  integer(ip) :: map_old_to_new_point(mesh_new%npoin)
  integer(ip) :: ipoin,ielem,numPoints,numDelPoints,numPoints_preAdapt,countPoints
  
  integer(ip), pointer :: mapNodes_new_to_old_aux(:)
  integer(ip) :: iaux
  
  type(mesh_type_basic)  :: mesh ! to copy the old mesh (change names to improve routine)
  !
  !
  numDelPoints = count(isDeletedPoint)
  numPoints = mesh_new%npoin - numDelPoints

  numPoints_preAdapt = size(isDeletedPoint)
  
  nullify(mapNodes_new_to_old_aux)
  call memory_alloca(memor_adapt,'mapNodes_new_to_old_aux','mod_adapt',mapNodes_new_to_old_aux,numPoints_preAdapt)
  mapNodes_new_to_old_aux(:) = mapNodes_new_to_old
  
  call memory_deallo(memor_adapt,'mapNodes_new_to_old','mod_adapt',mapNodes_new_to_old)
  nullify(mapNodes_new_to_old)
  call memory_alloca(memor_adapt,'mapNodes_new_to_old','mod_adapt',mapNodes_new_to_old,numPoints)
  mapNodes_new_to_old(:) = 0_ip !(/ ( 0_ip*iaux, iaux = 1_ip, numPoints) /) ! identity mapping to start
  
  !
  ! *** COPY MESH DELETING REMOVED NODES ****************************************
  call mesh%init(mesh_new%name)
  call mesh%copy(mesh_new)
  call mesh_new%deallo()
  
  call mesh_new%init(mesh%name)
  call mesh_new%alloca(mesh%ndime,mesh%mnode,mesh%nelem,numPoints)
  mesh_new%ltype(:) = mesh%ltype(1)

  !print*,'count(isDeletedPoint): ', count(isDeletedPoint)
  countPoints = 0_ip
  do ipoin=1,numPoints_preAdapt!size(isDeletedPoint)!mesh%npoin
    if(isDeletedPoint(ipoin)) then
      map_old_to_new_point(ipoin) = -1000_ip
    else
      countPoints = countPoints + 1_ip
      mesh_new%coord(:,countPoints) = mesh%coord(:,ipoin)
      map_old_to_new_point(ipoin) = countPoints
      !
      mapNodes_new_to_old(countPoints) = mapNodes_new_to_old_aux(ipoin)
    end if
  end do
  do ipoin = (numPoints_preAdapt+1),mesh%npoin
    countPoints = countPoints+1
    mesh_new%coord(:,countPoints) = mesh%coord(:,ipoin)
    map_old_to_new_point(ipoin) = countPoints
  end do
  if(countPoints.ne.numPoints) then
    print*,'error in num points'
    print*,countPoints
    print*,numPoints
    call runend('error in removeDeletedNodes')
  end if

  do ielem=1,mesh%nelem
!     do ipoin=1,3
!       if(map_old_to_new_point(mesh%lnods(ipoin,ielem))<0_ip) then
!         print*,mesh%lnods(ipoin,ielem),'   ',map_old_to_new_point(mesh%lnods(ipoin,ielem))
!         print*,mesh%lnods(:,ielem)
!         call out_paraview_inp(mesh,'./unitt/unitt_adapti_hessian_alya/mesh_kk',nodeField=map_old_to_new_point*1.0_rp)
!       end if
!     end do
    mesh_new%lnods(:,ielem) = map_old_to_new_point(mesh%lnods(:,ielem))
  end do
  
  call mesh%deallo()
  
  ! *** DELETING REMOVED NODES OF THE METIRC ****************************************
  call metric_new%remove_nodes(isDeletedPoint)
  
  call memory_deallo(memor_adapt,'mapNodes_new_to_old_aux','mod_adapt',mapNodes_new_to_old_aux)
  
end subroutine removeDeletedNodes
! 
!
!
function generate_mid_point(x1,x2,M1,M2) result(x_new)
  real(rp), intent(in) :: x1(:), x2(:)
  real(rp), optional, intent(in) :: M1(:,:), M2(:,:)
  real(rp) :: x_new(size(x1))
  
  !real(rp) :: alpha
  
  call runend('should not be used any more.. moved to metric')
  
  if(present(M1)) then
    !print*,"implement generate_mid_point with metric"
    !call runend("epa")
    !x_new = (x1+x2)/2.0
    
   ! alpha = 
    
  else
    x_new = (x1+x2)/2.0
  end if
  
end function generate_mid_point
!
!
!
subroutine set_mesh_new_modify(&
  mesh_new,coord_new,lnods_new,num_new_points,num_new_elems,isDeletedElem,isDeletedPoint)

  type(mesh_type_basic), intent(inout) :: mesh_new
  real(rp),    pointer,  intent(inout) :: coord_new(:,:)
  integer(ip), pointer,  intent(inout) :: lnods_new(:,:)
  integer(ip),           intent(in)    :: num_new_points
  integer(ip),           intent(in)    :: num_new_elems
  logical(lg),           intent(in)    :: isDeletedElem(:)
  logical(lg), optional, intent(in)    :: isDeletedPoint(:)
  
  integer(ip) :: numElems, numDelElems, numPoints, ielem, numDelPoints, ipoin
  integer(ip) :: ini_elems, fin_elems, ini_points, fin_points
  
  integer(ip) :: map_old_to_new_point(mesh_new%npoin)
  type(mesh_type_basic)                :: mesh ! to copy the old mesh (change names to improve routine)
  !
  
  if(present(isDeletedPoint)) then
    !print*,'Implement removing points from mesh%coord'
    numDelPoints = count(isDeletedPoint)
    call runend('do not use inside the frozen loop, since tagged edge nodes are not updated')
  else
    !print*,'Only points are added, not removed'
    numDelPoints = 0_ip
  end if

  call mesh%init(mesh_new%name)
  call mesh%copy(mesh_new)
  
  numDelElems = count(isDeletedElem)
  numElems  = mesh%nelem - numDelElems  + num_new_elems
  numPoints = mesh%npoin - numDelPoints + num_new_points

  call mesh_new%deallo()
  
  call mesh_new%init(mesh%name)
  call mesh_new%alloca(mesh%ndime,mesh%mnode,numElems,numPoints)

  if(present(isDeletedPoint)) then
    numPoints = 0_ip
    do ipoin=1,size(isDeletedPoint)!mesh%npoin
      if(.not.isDeletedPoint(ipoin)) then
        numPoints = numPoints + 1_ip
        mesh_new%coord(:,numPoints) = mesh%coord(:,ipoin)
        map_old_to_new_point(ipoin) = numPoints
      end if
    end do
  else
    mesh_new%coord(:,1:mesh%npoin) = mesh%coord
    numPoints = mesh%npoin
  end if
  
  if( num_new_points > 0 ) then
    ini_points = numPoints+1_ip
    fin_points = numPoints+num_new_points
    mesh_new%coord(:,ini_points:fin_points) = coord_new(:,1:num_new_points)
  end if
  
  mesh_new%ltype(:) = mesh%ltype(1)

  numElems = 0_ip
  do ielem=1,mesh%nelem
!     if(ielem.eq.142_ip.or.ielem.eq.100_ip.or.ielem.eq.126_ip) then
!       print*,ielem," ... ",isDeletedElem(ielem)
!     end if
    if(.not.isDeletedElem(ielem)) then
      numElems = numElems + 1_ip
      if(present(isDeletedPoint)) then
        mesh_new%lnods(:,numElems) = map_old_to_new_point(mesh%lnods(:,ielem))
      else
        mesh_new%lnods(:,numElems) = mesh%lnods(:,ielem)
      end if
    end if
  end do
  ini_elems = numElems+1_ip
  fin_elems = numElems+num_new_elems
  mesh_new%lnods(:,ini_elems:fin_elems) = lnods_new(:,1:num_new_elems)
  
  call memory_deallo(memor_adapt,'coord_new','set_mesh_new_modify',coord_new)
  call memory_deallo(memor_adapt,'lnods_new','set_mesh_new_modify',lnods_new) 
  call mesh%deallo()

  return
end subroutine set_mesh_new_modify
!
!
!
subroutine sort_perm_reals_decreasing(f,sort_perm)
  use mod_maths_sort, only: maths_heap_sort
  !use mod_maths_sort, only: maths_quick_sort
  implicit none
  
  real(rp), intent(in)             :: f(:)
  integer(ip), pointer, intent(inout) :: sort_perm(:)
  
  real(rp) :: f_toSort(size(f))
  integer(ip) :: n_f
  integer(ip) ::task_sort
  integer(ip) :: iaux
  !integer(ip) :: perm_toSort(:)
  
  
  nullify(sort_perm)
  call memory_alloca(memor_adapt,'sort_perm','sort_perm_reals_decreasing',sort_perm,int(size(f),ip))
  
  !f_toSort = nint(f/(minval(f)*0.000001_rp)) ! convert real to int with number of 0.000001^-1 decimal precission
  f_toSort = f
  n_f = size(f)
  sort_perm = (/ (iaux, iaux = 1, n_f) /)
  
  task_sort = 1_ip  ! 1->Decreasing value, 2->Increasing value
  call maths_heap_sort(itask=task_sort, nrows=n_f, rvin=f_toSort, ivo1=sort_perm)
  
!   f_toSort = (/1,2,3,4,5/)
!   sort_perm = (/ (iaux, iaux = 1, 5) /)
!   call maths_heap_sort(itask=task_sort, nrows=n_f, rvin=f_toSort, ivo1=sort_perm)!, ELIMINATE_REPLICATES=.false.)
!   print*,'f_toSort: ',f_toSort
!   print*,'sort_perm:',sort_perm
!   print*,'---'
!
!   f_toSort = (/1,3,2,4,5/)
!   sort_perm = (/ (iaux, iaux = 1, 5) /)
!   call maths_heap_sort(itask=task_sort, nrows=n_f, rvin=f_toSort, ivo1=sort_perm)!, ELIMINATE_REPLICATES=.false.)
!   print*,'f_toSort: ',f_toSort
!   print*,'sort_perm:',sort_perm
!   print*,'---'
!
!   f_toSort = (/5,4,3,2,1/)
!   sort_perm = (/ (iaux, iaux = 1, 5) /)
!   call maths_heap_sort(itask=task_sort, nrows=n_f, rvin=f_toSort, ivo1=sort_perm)!, ELIMINATE_REPLICATES=.false.)
!   print*,'f_toSort: ',f_toSort
!   print*,'sort_perm:',sort_perm
!   print*,'---'
  
end subroutine sort_perm_reals_decreasing
!
!
!
subroutine sort_perm_reals_increasing(f,sort_perm)
  use mod_maths_sort, only: maths_heap_sort
  !use mod_maths_sort, only: maths_quick_sort
  implicit none
  
  real(rp), intent(in)             :: f(:)
  integer(ip), pointer, intent(inout) :: sort_perm(:)
  
  real(rp) :: f_toSort(size(f))
  integer(ip) :: n_f
  integer(ip) :: task_sort
  integer(ip) :: iaux

  
  nullify(sort_perm)
  call memory_alloca(memor_adapt,'sort_perm','sort_perm_reals_decreasing',sort_perm,int(size(f),ip))
  
  f_toSort = f
  n_f = size(f)
  sort_perm = (/ (iaux, iaux = 1, n_f) /)
  
  task_sort = 2_ip  ! 1->Decreasing value, 2->Increasing value
  call maths_heap_sort(itask=task_sort, nrows=n_f, rvin=f_toSort, ivo1=sort_perm)
  
end subroutine sort_perm_reals_increasing
!
!
!
!
!
!
END MODULE mod_adapt

!> @}

!
!
!
! function set_mesh_new_deprecated(&
!   mesh,coord_new,lnods_new,num_new_points,num_new_elems,isDeletedElem,isDeletedPoint) result(mesh_new)
!   type(mesh_type_basic)                :: mesh_new
!   type(mesh_type_basic), intent(inout) :: mesh
!   real(rp),    pointer,  intent(inout) :: coord_new(:,:)
!   integer(ip), pointer,  intent(inout) :: lnods_new(:,:)
!   integer(ip),           intent(in)    :: num_new_points
!   integer(ip),           intent(in)    :: num_new_elems
!   logical(lg),           intent(in)    :: isDeletedElem(:)
!   logical(lg), optional, intent(in)    :: isDeletedPoint(:)
!
!   integer(ip) :: numElems, numDelElems, numPoints, ielem, numDelPoints, ipoin
!   integer(ip) :: ini_elems, fin_elems, ini_points, fin_points
!
!   integer(ip) :: map_old_to_new_point(mesh%npoin)
!
!   if(present(isDeletedPoint)) then
!     !print*,'Implement removing points from mesh%coord'
!     numDelPoints = count(isDeletedPoint)
!     call runend('do not use inside the frozen loop, since tagged edge nodes are not updated')
!   else
!     !print*,'Only points are added, not removed'
!     numDelPoints = 0_ip
!   end if
!
!   numDelElems = count(isDeletedElem)
!   numElems  = mesh%nelem - numDelElems  + num_new_elems
!   numPoints = mesh%npoin - numDelPoints + num_new_points
!
!   call mesh_new%init(mesh%name)
!   call mesh_new%alloca(mesh%ndime,mesh%mnode,numElems,numPoints)
!
!   if(present(isDeletedPoint)) then
!     numPoints = 0_ip
!     do ipoin=1,mesh%npoin
!       if(.not.isDeletedPoint(ipoin)) then
!         numPoints = numPoints + 1_ip
!         mesh_new%coord(:,numPoints) = mesh%coord(:,ipoin)
!         map_old_to_new_point(ipoin) = numPoints
!       end if
!     end do
!   else
!     mesh_new%coord(:,1:mesh%npoin) = mesh%coord
!     numPoints = mesh%npoin
!   end if
!   ini_points = numPoints+1_ip
!   fin_points = numPoints+num_new_points
!   mesh_new%coord(:,ini_points:fin_points) = coord_new(:,1:num_new_points)
!
!   mesh_new%ltype(:) = mesh%ltype(1)
!
!   numElems = 0_ip
!   do ielem=1,mesh%nelem
!     if(.not.isDeletedElem(ielem)) then
!       numElems = numElems + 1_ip
!       if(present(isDeletedPoint)) then
!         mesh_new%lnods(:,numElems) = map_old_to_new_point(mesh%lnods(:,ielem))
!       else
!         mesh_new%lnods(:,numElems) = mesh%lnods(:,ielem)
!       end if
!     end if
!   end do
!   ini_elems = numElems+1_ip
!   fin_elems = numElems+num_new_elems
!   mesh_new%lnods(:,ini_elems:fin_elems) = lnods_new(:,1:num_new_elems)
!
!   call memory_deallo(memor_adapt,'coord_new','set_tagged_edges',coord_new)
!   call memory_deallo(memor_adapt,'lnods_new','set_tagged_edges',lnods_new)
!
!   call mesh%deallo()
!   return
! end function set_mesh_new_deprecated