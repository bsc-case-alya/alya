!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_endste()
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_endste
  ! NAME 
  !    nsi_endste
  ! DESCRIPTION
  !    This routine ends a time step of the incompressible NS equations.
  ! USES
  !    nsi_cvgunk
  !    nsi_updunk
  ! USED BY
  !    Nastin
  !***
  !-----------------------------------------------------------------------
  use def_master
  use def_parame
  use def_nastin
  use def_kermod, only : kfl_cos_opt,kfl_adj_prob
  use def_domain
  implicit none
  !
  ! Compute convergence residual of the time evolution (that is,
  ! || u(n,*,*) - u(n-1,*,*)|| / ||u(n,*,*)||) and update unknowns
  ! u(n-1,*,*) <-- u(n,*,*) 
  !    
  if( kfl_stead_nsi == 0 .and. kfl_timei_nsi == 1 ) then
     call nsi_cvgunk(ITASK_ENDSTE) ! Convergence
     call nsi_updunk(ITASK_ENDSTE) ! VELOC, PRESS
  end if
  !
  ! Compute averaged variables
  !
  call nsi_averag()
  !
  ! If not steady, go on
  !
  if(kfl_stead_nsi==0.and.kfl_timei_nsi==1.and.kfl_conve(modul)==1) kfl_gotim = 1
  !
  ! Calculate functional and sensitivities
  !
  if (kfl_cos_opt == 1) call nsi_costcal()
  if (kfl_adj_prob == 1) call nsi_senscal()
  
end subroutine nsi_endste
