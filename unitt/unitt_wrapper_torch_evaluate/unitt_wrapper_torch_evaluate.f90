!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_torch_create_vector
  use def_kintyp_basic, only : ip,rp
  character(len=:), allocatable ::  nn_path             
  integer(4)                   ::  len_path 
  integer(4)                   ::  n_dim_input 
  integer(4)                   ::  n_dim_output
  integer(4)                   ::  shape_input(1)
  integer(4)                   ::  shape_output(1) 
  real(4), pointer             ::  input(:)
  real(4), pointer             ::  output(:) 
  real(4), pointer             ::  input2D(:,:)
  real(4), pointer             ::  output2D(:,:) 

  real(8), pointer             ::  input8(:)
  real(8), pointer             ::  output8(:) 
  real(8), pointer             ::  input2D8(:,:)
  real(8), pointer             ::  output2D8(:,:) 

  integer(ip)                  ::  ii, len_var 

#ifdef TORCH
  !
  ! Initialize multiple places to hold networks
  !
  call initialize_neural_networks(5)


  !
  ! Read a neural networks
  !
  nn_path = "./ANN_prop_Yc_source_NO_pytorch.pt"
  len_path = len(nn_path)
  call read_neural_network(2_4, nn_path, len_path)
  call set_neural_network_precision_to_single(2_4)

  nn_path = "./ANN_model.pt"
  len_path = len(nn_path)
  call read_neural_network(3_4, nn_path, len_path)
  call set_neural_network_precision_to_single(3_4)

  !==============================================================!
  ! Evaluate simple neural network with one input and one output !
  !==============================================================!
  n_dim_input = 1
  n_dim_output = 1
  shape_input(1) = 1
  shape_output(1) = 1
  
  !
  ! Allocate input structure
  !
  len_var = 1
  do ii = 1,n_dim_input
    len_var = len_var * shape_input(ii)
  enddo
  allocate(input(len_var))
  input = reshape(input, shape_input)

  !
  ! Allocate output structure
  !
  len_var = 1
  do ii = 1,n_dim_output
    len_var = len_var * shape_output(ii)
  enddo
  allocate(output(len_var))
  output = reshape(output, shape_output)

  !
  ! Forward pass and check output
  !
  input(1) = 0.2_rp 
  call forward_pass_neural_network(2_4, 1_4, input, output, n_dim_input, shape_input(1:n_dim_input), n_dim_output )
  print*, "output=", output; if( abs( output(1)  - (-0.654261708) ) > 1.0e-5_rp ) stop 1

  deallocate(input)
  deallocate(output)


  !==============================================!
  ! Evaluate a realistic NN with multiple inputs ! 
  !==============================================!
  allocate(input(3))
  allocate(output(1))
  input(1) = 0.1
  input(2) = 0.9
  input(3) = 0.02
  call forward_pass_neural_network(3_4, 1_4, input, output, 1_4, (/ 3_4 /), 1_4 )
  print*, "output=", output; if( abs( output(1)  - (0.846406400) ) > 1.0e-5_rp ) stop 1

  deallocate(input)
  deallocate(output)

  !================!
  ! Try vectorised !  
  !================!
  allocate(input2D(10,3))
  allocate(output2D(10,1))
  !
  ! Initialize
  !
  input2D(:,1) = 0.1
  input2D(:,2) = 0.9
  input2D(:,3) = 0.02
  
  !
  ! Add some variations
  !
  input2D(2,1) = 0.11
  input2D(2,2) = 0.9
  input2D(2,3) = 0.02

  input2D(3,1) = 0.1
  input2D(3,2) = 0.92
  input2D(3,3) = 0.02

  input2D(4,1) = 0.1
  input2D(4,2) = 0.9
  input2D(4,3) = 0.03

  input2D(5,1) = 0.11
  input2D(5,2) = 0.92
  input2D(5,3) = 0.03

  !
  ! Call forward
  !
  call forward_pass_neural_network(3_4, 1_4, input2D, output2D, 2_4, (/ 10_4, 3_4 /), 2_4 )
  print*, "output2D(1,1)=", output2D(1,1); if( abs( output2D(1,1)  - (0.846406400) ) > 1.0e-5_rp ) stop 1
  print*, "output2D(2,1)=", output2D(2,1); if( abs( output2D(2,1)  - (0.541784704) ) > 1.0e-5_rp ) stop 1
  print*, "output2D(3,1)=", output2D(3,1); if( abs( output2D(3,1)  - (0.928905904) ) > 1.0e-5_rp ) stop 1
  print*, "output2D(4,1)=", output2D(4,1); if( abs( output2D(4,1)  - (0.832901418) ) > 1.0e-5_rp ) stop 1
  print*, "output2D(5,1)=", output2D(5,1); if( abs( output2D(5,1)  - (0.630887091) ) > 1.0e-5_rp ) stop 1


  deallocate(input2D)
  deallocate(output2D)

  !
  ! Try double precision 
  !
  call set_neural_network_precision_to_double(3_4)

  allocate(input8(3))
  allocate(output8(1))
  input8(1) = 0.1
  input8(2) = 0.9
  input8(3) = 0.02
  call forward_pass_neural_network(3_4, 2_4, input8, output8, 1_4, (/ 3_4 /), 1_4 )
  print*, "output8=", output8; if( abs( output8(1)  - (0.846406400) ) > 1.0e-5_rp ) stop 1

  deallocate(input8)
  deallocate(output8)

  !================!
  ! Try vectorised !  
  !================!
  allocate(input2D8(10,3))
  allocate(output2D8(10,1))
  !
  ! Initialize
  !
  input2D8(:,1) = 0.1
  input2D8(:,2) = 0.9
  input2D8(:,3) = 0.02
  
  !
  ! Add some variations
  !
  input2D8(2,1) = 0.11
  input2D8(2,2) = 0.9
  input2D8(2,3) = 0.02

  input2D8(3,1) = 0.1
  input2D8(3,2) = 0.92
  input2D8(3,3) = 0.02

  input2D8(4,1) = 0.1
  input2D8(4,2) = 0.9
  input2D8(4,3) = 0.03

  input2D8(5,1) = 0.11
  input2D8(5,2) = 0.92
  input2D8(5,3) = 0.03

  !
  ! Call forward
  !
  call forward_pass_neural_network(3_4, 2_4, input2D8, output2D8, 2_4, (/ 10_4, 3_4 /), 2_4 )
  print*, "output2D8(1,1)=", output2D8(1,1); if( abs( output2D8(1,1)  - (0.846406400) ) > 1.0e-5_rp ) stop 1
  print*, "output2D8(2,1)=", output2D8(2,1); if( abs( output2D8(2,1)  - (0.541784704) ) > 1.0e-5_rp ) stop 1
  print*, "output2D8(3,1)=", output2D8(3,1); if( abs( output2D8(3,1)  - (0.928905904) ) > 1.0e-5_rp ) stop 1
  print*, "output2D8(4,1)=", output2D8(4,1); if( abs( output2D8(4,1)  - (0.832901418) ) > 1.0e-5_rp ) stop 1
  print*, "output2D8(5,1)=", output2D8(5,1); if( abs( output2D8(5,1)  - (0.630887091) ) > 1.0e-5_rp ) stop 1


  deallocate(input2D8)
  deallocate(output2D8)

#endif

end program unitt_torch_create_vector
