!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_adapti_hash
  
  use def_kintyp_basic, only: ip,rp,lg
  use mod_dictionary, only: dictionary_type
  
  use mod_edict, only: edict_type
  
  implicit none
  
  !
  type(dictionary_type) :: dict
  integer(ip) :: dict_size
  
  type(edict_type) :: edict
  integer(ip) :: edict_size
  
  dict_size = 3_ip
  call dict%init(dict_size)
  call dict%set("key1","value1")
  call dict%set("key2","value2")
  call dict%set("key1","value1 machacado")
  
  if(     trim(dict%get("key1"))/="value1 machacado") then
    call runend("error with key1")
  elseif( trim(dict%get("key2"))/="value2") then
    call runend("error with key2")
  end if
  
  !
  ! The following keys break due to the length of the integers
!   call dict%print()
!
!   call dict%set("FYFYFYEzFYFYFYEzFYEzFYFYFYFYFY","1a")
!   call dict%set("EzFYEzEzEzFYFYFYFYEzEzFYEzEzFY","2a")
!   call dict%set("EzEzEzEzEzEzEzEzEzEzEzEzEzEzEz","3a")
!   call dict%set("FYFYFYFYFYFYFYFYFYFYFYFYFYFYFY","4a")
!   call dict%set("FYEzEzFYFYEzEzFYFYFYFYEzFYFYFY","5a")
!
!   call dict%print()
  !
  
  !print*,'_______ EDGE DICTIONARY _________'
  
  edict_size = 1_ip
  call edict%init(edict_size)
  call edict%setByNodes(1_ip,2_ip,.true.)
  call edict%setByNodes(2_ip,3_ip,.true.)
  call edict%setByNodes(3_ip,1_ip,.true.)
  
  if(edict%chunks(1_ip)%count.ne.3_ip) then
    call runend("count should have increased to 3")
  end if
  
  call edict%deallo()
  edict_size = 10_ip
  call edict%init(edict_size)
  call edict%setByNodes(1_ip,2_ip,.true.)
  call edict%setByNodes(2_ip,3_ip,.true.)
  call edict%setByNodes(3_ip,1_ip,.true.)
  
  if( .not.edict%areEdge(1_ip,2_ip) ) then
    call runend("this edge should be in the dictionary")
  end if
  call edict%setByNodes(1_ip,2_ip,.false.)
  if( edict%areEdge(1_ip,2_ip) ) then
    call runend("this edge should have been removed from the dictionary")
  end if
  
!   call runend('O.K.!')                                               ! Finish Alya
  
end program unitt_adapti_hash     
