!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_read_mesh
  !
  ! Read a mesh and output it
  !
  use def_kintyp_basic,      only : ip,rp,lg
  use def_kintyp_mesh_basic, only : mesh_type_basic
  use mod_elmgeo,            only : elmgeo_element_type_initialization
  use mod_iofile,            only : iofile_open_unit
  implicit none

  type(mesh_type_basic) :: mesh
  integer(ip)           :: nunit
  integer(ip)           :: ierr,ioerr
  logical(lg)           :: opened

  call elmgeo_element_type_initialization()

  print*,'open file'
  nunit = 90
  call iofile_open_unit(nunit,'example.msh',stato='old',crash_if_cannot_open=.true.)

  inquire(unit=nunit,opened=opened,iostat=ioerr)

  call mesh % init          ()
  print*,'read mesh=',opened,ioerr
  if( ioerr /= 0 ) stop 4
  call mesh % read_from_file(lunit=nunit)
  print*,'output mesh'
  call mesh % output        (filename='example')

  ierr = mesh % check() 

  print*,'mesh=',mesh % nelem,mesh % npoin
  if( ierr /= 0          ) then
     print*,'mesh error= ',ierr
     stop 1
  end if
  
  if( mesh % nelem /= 8  ) stop 2
  if( mesh % npoin /= 12 ) stop 3
 
end program unitt_read_mesh
