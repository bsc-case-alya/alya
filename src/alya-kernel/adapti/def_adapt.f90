!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Adaptivity
!> @{
!> @file    def_adapt.f90
!> @author  abel.gargallo
!> @date    2021-04-20
!> @brief   def_adapt
!> @details def_adapt
!>
!>          To add further details
!>
!>
!-----------------------------------------------------------------------

MODULE def_adapt
!***************************************************************
!*
!*  Module for performing adaptive topological operation
!*
!***************************************************************
use def_kintyp_basic, only : ip,rp,lg

implicit none
private
public :: memor_adapt

integer(8)               :: memor_adapt(2)

END MODULE def_adapt


!> @}