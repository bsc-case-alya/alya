!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Parall
!> Bridge to FTI
!> @{
!> @file    mod_alya2fti.f90
!> @author  jchiva
!> @date    2020-06-19
!> @brief   Bridge to FTI
!> @details Interfaces with FTI
!-----------------------------------------------------------------------

module mod_alya2fti

#ifdef ALYA_FTI
  use def_master,         only : kfl_rstar, ittim, mitim
  use def_kintyp,         only : ip
  use def_inpout,         only : words
!  use mod_messages,       only : livinf !circular dependency
  use def_mpio,           only : PAR_MPIO_ON
  use def_communications, only : MPI_COMM_WORLD
  use FTI
#endif

  implicit none
     
#ifdef ALYA_FTI
  integer, target :: FTI_comm_world           ! FTI COMMUNICATOR
  integer(ip)     :: FTI_st=0,           &    ! FTI status (if 0 /= FTI_st then a previous FTI file exists and will be read
                     FTI_write_ckpt=0,   &    ! A checkpoint will be written using FTI in this step
                     FTI_usage,          &    ! Is FTI enabled
                     FTI_DBG                  ! Is FTI debug
#endif  

  private

#ifdef ALYA_FTI
  public :: FTI_comm_world
  public :: FTI_st
  public :: FTI_write_ckpt
  public :: FTI_usage
  public :: FTI_DBG
#endif
  public :: alya2fti_initialization
  public :: alya2fti_finalization
  public :: alya2fti_iexcha
  public :: alya2fti_RecoverVarInit
  public :: alya2fti_RecoverVarFinalize
  public :: alya2fti_InitICP
  public :: alya2fti_FinalizeICP

contains

  !-----------------------------------------------------------------------
  !> 
  !> @author  bsc21943
  !> @date    2019-07-19
  !> @brief   Initialization
  !> @details Initialization of FTI 
  !> 
  !-----------------------------------------------------------------------
  
  subroutine alya2fti_initialization()
#ifdef ALYA_FTI
    integer, target :: err
#if defined USEMPIF08
    FTI_comm_world = MPI_COMM_WORLD % MPI_VAL
#else
    FTI_comm_world = MPI_COMM_WORLD 
#endif
    call FTI_Init("config.fti",FTI_comm_world,err)
    call FTI_status(FTI_st)
#endif
  end subroutine alya2fti_initialization

  subroutine alya2fti_finalization()
#ifdef ALYA_FTI
    integer(4) :: ierror
    ierror=1
    call FTI_Finalize(ierror) !ierror is output, should be used for something?
#endif
  end subroutine alya2fti_finalization

  subroutine alya2fti_iexcha()
#ifdef ALYA_FTI
    call iexcha(FTI_usage)
    call iexcha(FTI_DBG)
#endif
  end subroutine alya2fti_iexcha

  subroutine alya2fti_RecoverVarInit()
#ifdef ALYA_FTI
    if ( FTI_st /= 0 ) then
        call FTI_RecoverVarInit()
    end if
#endif
  end subroutine alya2fti_RecoverVarInit

  subroutine alya2fti_RecoverVarFinalize()
#ifdef ALYA_FTI
    if ( FTI_st /= 0 .and. kfl_rstar == 2 ) then
        call FTI_RecoverVarFinalize()
    end if
#endif
  end subroutine alya2fti_RecoverVarFinalize

  subroutine alya2fti_InitICP()
#ifdef ALYA_FTI
    integer(ip) :: FTI_error
!    print *,__FILE__,__LINE__,FTI_usage," ",PAR_MPIO_ON," ",FTI_DBG
    if ( FTI_usage == PAR_MPIO_ON .and. (FTI_DBG /= 1 .or. ittim < mitim) ) then
        FTI_write_ckpt = 1_ip
        call FTI_initICP(ittim,4,.true.,FTI_error)
    end if
#endif
  end subroutine alya2fti_InitICP

  subroutine alya2fti_FinalizeICP()
#ifdef ALYA_FTI
    integer(ip) :: FTI_error
    if (FTI_write_ckpt /= 0) then
        FTI_write_ckpt = 0
        call FTI_FinalizeICP(FTI_error)
        if ( FTI_error /= 0 ) then
!            call livinf(0_ip, "Failed to write checkpoint file",0_ip) !Circular dependency
            print *, "Failed to write checkpoint file"
        endif
    endif
#endif
  end subroutine alya2fti_FinalizeICP

end module mod_alya2fti
!> @}
