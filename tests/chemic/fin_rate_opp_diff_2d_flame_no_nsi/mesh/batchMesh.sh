#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



name=mesh2d
gmsh -2 $name.geo
gmsh2alya.pl $name -bcs=boundaries
python getCoordinates.py $name
python initialCondition.py $name 2S_CM2.xml "CH4:1" "N2:0.767083, O2: 0.232917" 298.0 101325 "1,1 & 3" "2,2 & 3" 
sed -i -e '1d; $d' $name.fix.bou 

cp $name.dims.dat ..
cp $name.geo.dat ..
cp $name.fix.bou ..
rm -rf ../Fields
mkdir ../Fields
cp Fields/*.alya ../Fields/
cp fields.dat ..
cp field_size.dat ..
cp speciesBoundary.dat ..
