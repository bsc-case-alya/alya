#!/bin/bash
#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



#
#  Submit jobs in MN-IV
#     sbatch < job.sh
#
#SBATCH --job-name=cyl-fsi
#SBATCH -D .
#SBATCH --error=%j.err
#SBATCH --output=%j.out
#SBATCH --qos=debug
#SBATCH --ntasks=6
#SBATCH --tasks-per-node=48
##SBATCH --ntasks-per-node=46
##SBATCH --ntasks-per-socket=23
#SBATCH --time=02:00:00

#
ALYAPATH="./Alya.g"
#ALYAPATH="/home/bsc21/bsc21946/alya/master-pull-fsi/Executables/unix-2d/Alya.x"
#ALYAPATH="/home/bsc21/bsc21946/alya/master/Executables/unix-debug/Alya.g"
#ALYAPATH="/home/bsc21/bsc21946/alya/214-solidz-sherloc/Executables/unix/Alya.x"
#288and360
MODEL1=fluid
MODEL2=structure
CPUSM1=3
CPUSM2=3
#
# Launches ALYA
#
mpirun -np $CPUSM1 $ALYAPATH $MODEL1 : -np $CPUSM2 $ALYAPATH $MODEL2
