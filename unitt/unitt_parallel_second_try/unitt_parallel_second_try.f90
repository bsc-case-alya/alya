!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_parallel_second_try

  use def_kintyp
  use def_master
  use def_domain
  use mod_elmgeo
  use def_maths_bin
  use def_kermod
  use def_search_method
  use mod_parall
  use def_interpolation_method
  use mod_memory_basic
  use mod_communications_global
  implicit none
  real(rp),    parameter :: eps = 1.0e-10_rp
  real(rp),    pointer   :: bobox(:,:,:)
  real(rp),    pointer   :: subox(:,:,:)
  integer(ip)            :: pelty,ielem,inode,ipoin,ii
  integer(ip)            :: idime
  type(maths_bin)        :: bin_seq
  type(maths_bin)        :: bin_par
  type(maths_bin)        :: bin_par_2nd
  type(maths_bin)        :: bin_2nd
  real(rp),    pointer   :: xx(:,:),vv(:,:),dd(:,:)
  real(rp),    pointer   :: x1(:),v1(:)
  real(rp),    pointer   :: xcoor(:)
  type(interpolation)    :: interp
  type(interpolation)    :: inter2
  
  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction


  nullify(bobox)
  nullify(subox)
  nullify(xcoor)
  nullify(xx)
  nullify(vv)
  nullify(x1)
  nullify(v1)
  nullify(dd)
  
  if( kfl_paral == 0 ) then
     allocate(xx(2,1))  
     allocate(vv(2,1))
     allocate(v1(1))
     allocate(dd(2,1))
     xx(1,1) = -0.1_rp
     xx(2,1) = -0.1_rp
     vv = 1.0e16_rp
  else if( kfl_paral == 1 ) then
     allocate(xx(2,4))  
     allocate(vv(2,4))
     allocate(v1(4))
     allocate(dd(2,4))
     xx(1,1) = 0.200_rp
     xx(2,1) = 0.202_rp
     xx(1,2) = 0.500_rp
     xx(2,2) = 0.505_rp
     xx(1,3) = 0.800_rp
     xx(2,3) = 0.808_rp
     xx(1,4) = 0.100_rp
     xx(2,4) = 0.800_rp
     vv = 1.0e16_rp
  else if( kfl_paral == 2 ) then
     allocate(xx(2,2))  
     allocate(vv(2,2))
     allocate(v1(2))
     allocate(dd(2,2))
     xx(1,1) = 0.1_rp
     xx(2,1) = 0.1_rp
     xx(1,2) = 1.1_rp
     xx(2,2) = 1.1_rp
     vv = huge(1.0_rp)
  end if
!!$  if( kfl_paral == 0 ) then
!!$     allocate(xx(2,1))  
!!$     allocate(vv(2,1))
!!$     allocate(v1(1))
!!$     allocate(dd(2,1))
!!$     xx(1,1) = -0.1_rp
!!$     xx(2,1) = -0.1_rp
!!$     vv = huge(1.0_rp)*0.1_rp
!!$  end if
  !
  ! Sequential search method
  !
  call meshe(ndivi) % element_bb(bobox)
  call bin_seq % init  ()
  call bin_seq % input (boxes=(/10_ip,10_ip,1_ip/)) 
  bin_seq % name = 'BIN SEQ'
  call bin_seq % fill  (BOBOX=bobox)
  !
  ! Sequential search method
  !
  allocate(subox(2,ndime,0:npart))
  subox = 0.0_rp
  if( inotmaster ) then
     do idime = 1,ndime
        subox(1,idime,kfl_paral) = minval(bobox(1,idime,:))
        subox(2,idime,kfl_paral) = maxval(bobox(2,idime,:))
     end do
  else 
     subox(1,:,kfl_paral) =  1.0e16_rp
     subox(2,:,kfl_paral) = -1.0e16_rp
  end if
  call PAR_SUM(subox,INCLUDE_ROOT=.true.)

  !if(kfl_paral==0) then
  !   do ii = 0,3
  !      print*,ii,': ','min=',subox(1,:,ii),'max=',subox(2,:,ii)
  !   end do
  !end if
  
  call bin_par % init      ()
  call bin_par % input     (boxes=(/10_ip,10_ip,1_ip/)) 
  bin_par % name = 'BIN PAR'
  call bin_par % fill      (BOBOX=subox)
  !
  ! Second try
  !
  call bin_2nd % init      ()  
  call bin_2nd % input     (boxes=(/3_ip,3_ip/)) 
  call bin_2nd % fill      (COORD=meshe(ndivi) % coord)
  !
  ! Interpolation
  !
  call interp % init       ()
  call interp % input      (bin_seq,bin_par,&
       &                   COMM=PAR_COMM_WORLD,&
       &                   INTERPOLATION_METHOD=INT_ELEMENT_INTERPOLATION,&
       &                   NAME='FIRST TRY')

    
  call inter2 % init       ()
  call inter2 % input      (bin_2nd,bin_par,&
       &                   COMM=PAR_COMM_WORLD,&
       &                   INTERPOLATION_METHOD=INT_NEAREST_NODE,&
       &                   NAME='SECOND TRY')

  !call interp % preprocess (xx,meshe(ndivi))
  !call inter2 % preprocess (xx,meshe(ndivi))
  !call runend('O.K.!')
  
  call interp % preprocess (xx,meshe(ndivi),SECOND_TRY=inter2)
  call interp % values     (meshe(ndivi) % coord,vv)
  !
  ! Values
  !
  print*,'test 1'
  !do ii = 1,memory_size(vv,2_ip)
  !   print*,'subdo=',kfl_paral,'point ',ii,'=',vv(:,ii)
  !end do
  
  do ii = 1,memory_size(xx,2_ip)
     if( kfl_paral == 2 .and. ii == 2 ) then
        if( abs(vv(1,ii)-1.0_rp ) > eps ) then
           print*,'test 1: error 1=',xx(1,ii),vv(1,ii)
           stop 1
        end if
        if( abs(vv(2,ii)-1.0_rp ) > eps ) then
           print*,'test 1: error 1=',xx(2,ii),vv(2,ii)
           stop 1
        end if
     else if( kfl_paral == 0 .and. ii == 1 ) then
        if( abs(vv(1,ii)-0.0_rp ) > eps ) then
           print*,'test 1: error 1=',xx(1,ii),vv(1,ii)
           stop 2
        end if
        if( abs(vv(2,ii)-0.0_rp ) > eps ) then
           print*,'test 1: error 1=',xx(2,ii),vv(2,ii)
           stop 2
        end if
     else
        if( abs(xx(1,ii)-vv(1,ii)) > eps ) then
           print*,'test 1: error 1=',xx(1,ii),vv(1,ii)
           stop 3
        end if
        if( abs(xx(2,ii)-vv(2,ii)) > eps ) then
           print*,'test 1: error 2=',xx(2,ii),vv(2,ii)
           stop 3
        end if
     end if
  end do

  
  call interp % output(HEADER='|- ALYA  ')
  call inter2 % output()

  call runend('O.K.!')
  
end program unitt_parallel_second_try
 
