#include <torch/script.h>
#include <iostream>
#include <memory>

//
// PERSISTENT GLOBAL VARIABLES
//
std::vector<torch::jit::script::Module> neural_networks;

//
// HEADERS
//
const char* string_f2c(const char* licName, int nameLen);

template<typename T>
torch::Tensor convert_array_to_tensor(int* n_dim, int* f_shape, T* data, int precision);

void permute_order_of_tensor(torch::Tensor& tensor);

extern "C"
{
    //
    // CORE FUNCTIONALITY
    //
    void initialize_neural_networks_(int* n_net)
    {
        /*
         * Create n_net number of persistent networks
        */ 
        for(int ii=0; ii<n_net[0]; ii++){
            //
            // Create pointer
            //
            torch::jit::script::Module *nn_loc = new torch::jit::script::Module;

            //
            // Add it to vector
            //
            neural_networks.push_back(*nn_loc);
        }
    }


    void read_neural_network_(int* ind_net, const char* path_to_pt_file, int* len_path)
    {
        /*
         * Read a .pt model given by path_to_pt_file, into position ind_net-1 
         * (the lenght of the path string (len_path) has to be supplied)
        */ 

        //std::cout << "Path to pt file:" << path_to_pt_file << std::endl;
        //
        //
        // Create local model pointer
        //
        torch::jit::Module nn_loc = torch::jit::load(string_f2c(path_to_pt_file, len_path[0]));

        //
        // Assign it to specific index
        //
        neural_networks[ind_net[0]-1] = nn_loc.clone();
    }

    void set_neural_network_precision_to_single_(int* ind_net)
    {
        /*
         * Set the floating point precision of the neural network to single 
        */ 
        neural_networks[ind_net[0]-1].to(torch::kFloat32);
    }

    void set_neural_network_precision_to_double_(int* ind_net)
    {
        /*
         * Set the floating point precision of the neural network to single 
        */ 
        neural_networks[ind_net[0]-1].to(torch::kFloat64);
    }

    void forward_pass_neural_network_(int* ind_net, int* precision,  void* input,  void* output, int* n_dim_input, int* shape_input, int* n_dim_output)
    {
        /*
         * Evaluate neural network of index ind_net-1, with given input 
         * Return the result in output
        */ 

        ////
        //// Print shapes
        ////
        //std::cout << "Shape input:";
        //for(int ii=0; ii<n_dim_input[0]; ii++){
        //    std::cout << shape_input[ii];
        //}
        //std::cout << std::endl;

        
        //
        // Format Fortran input vector onto ATan tensor
        // The second input is the shape of the Fortran array
        //
        // then
        //
        // Switch the memory layout, 
        // this should be an issue for multi-dimensional inputs
        // The argument is a reverse list of the dimensions: 
        // {n, n-1, ..., 2, 1, 0}
        //
        at::Tensor input_tensor;
        if (precision[0] == 1) {
            input_tensor = convert_array_to_tensor(n_dim_input, shape_input, (float *)input, precision[0]); 
        } else if (precision[0] == 2) {
            input_tensor = convert_array_to_tensor(n_dim_input, shape_input, (double *)input, precision[0]); 
        }


        //
        // Compile input
        //
        std::vector<torch::jit::IValue> inputs_local;
        inputs_local.push_back(input_tensor);


        //
        // Evaluate
        //
        at::Tensor output_local = neural_networks[ind_net[0]-1].forward(inputs_local).toTensor();

        //
        // Switch back output to Fortran layout,
        // Again, this is relevant for multi-dimensional 
        // Then convert from a discontinous array to contiguous
        // And bring it back to the CPU if required
        //
        permute_order_of_tensor(output_local);
        output_local = output_local.contiguous().cpu();

        //
        // Compile output
        //
        if (precision[0] == 1) {
            std::memcpy( output, output_local.data_ptr(), output_local.numel() * sizeof(float) );
        } else if (precision[0] == 2) {
            std::memcpy( output, output_local.data_ptr(), output_local.numel() * sizeof(double) );
        }
    }


    //
    // TEST SUBROUTINES
    //
    void test_torch_type_()
    {
      /*
       * Test creating a simple torch data structure. 
      */ 
      std::cout << "Creating vector of: torch::jit::IValue"  << std::endl;
      
      std::vector<torch::jit::IValue> inputs;
      inputs.push_back(torch::ones({2}));

      std::cout << "Done:" << inputs << std::endl;
    }


}


//
// AUXILIARY SUBROUTINES
//
const char* string_f2c(const char* licName, int nameLen)
{
  /*
   * Function to process fortran strings 
   * and return string with correct ending.
  */ 
  char* name;
  name = (char*)malloc(sizeof(char)*(nameLen));
  for(int i = 0; i<nameLen; i++) name[i] = licName[i];
  name[nameLen] = '\0';
  return name ;
}


void permute_order_of_tensor(torch::Tensor& tensor) {
    /*
     * Reverse ordering of Torch tensor 
    */

    //
    // Reverse the ordering of the shape 
    //
    std::vector<int64_t> reverse_order(tensor.dim());
    for (size_t i=0; i<tensor.dim(); i++) {
        reverse_order[i] = tensor.dim() - (i+1);
    }

    //
    // Permute back to original shape
    //
    tensor = tensor.permute(reverse_order); 

}  


template<typename T>
torch::Tensor convert_array_to_tensor(int* n_dim, int* f_shape, T* data, int precision) {
    /*
     * Convert fortran array to torch tensor 
    */
    torch::Device            device = torch::kCPU;

    //
    // Reverse the ordering of the shape 
    //
    std::vector<int64_t> shape(n_dim[0]);
    std::vector<int64_t> reverse_order(n_dim[0]);
    for (size_t i=0; i<n_dim[0]; i++) {
        shape[i] = f_shape[ n_dim[0] - (i+1) ];
        reverse_order[i] = n_dim[0] - (i+1);
    }

    //
    // Set tensor options
    // 
    torch::TensorOptions options;
    if (precision == 1) {
        options = torch::TensorOptions().dtype(torch::kFloat32);
    }
    else{
        options = torch::TensorOptions().dtype(torch::kFloat64);
    }

    //
    // Convert to torch tensor
    //
    torch::Tensor tensor = torch::from_blob(data, shape, options);

    //
    // Permute back to original shape
    //
    permute_order_of_tensor(tensor); 

    return tensor; 
}  



