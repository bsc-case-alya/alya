!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!------------------------------------------------------------------------
!> @addtogroup Coupling 
!> @{
!> @file    cou_turnon.f90
!> @date    03/03/2014
!> @author  Guillaume Houzeaux
!> @brief   Turn on coupling 
!> @details Read data and allocate memory
!> @} 
!------------------------------------------------------------------------
subroutine cou_turnon()
  use def_kintyp, only : ip
  implicit none
  !
  ! Open files
  !  
  call cou_openfi()
  !
  ! Read data
  !  
  call cou_readat()
  !
  ! Broadcast data
  !  
  call cou_parall()

end subroutine cou_turnon
