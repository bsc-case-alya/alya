Rw = 0.001;
Ra = 0.0012;

cx = 0;
cy = 0;

Point(1) = {0, 0, 0};

Point(2) = {Rw, 0, 0};
Point(3) = {0, Rw, 0};
Point(4) = {-Rw, 0, 0};
Point(5) = {0, -Rw, 0};

Circle(10) = {2, 1, 3};
Circle(11) = {3, 1, 4};
Circle(12) = {4, 1, 5};
Circle(13) = {5, 1, 2};

Line(20) = {1, 2};
Line(21) = {1, 3};
Line(22) = {1, 4};
Line(23) = {1, 5};

Point(6) = {Ra, 0, 0};
Point(7) = {0, Ra, 0};
Point(8) = {-Ra, 0, 0};
Point(9) = {0, -Ra, 0};

Circle(14) = {6, 1, 7};
Circle(15) = {7, 1, 8};
Circle(16) = {8, 1, 9};
Circle(17) = {9, 1, 6};

Line(24) = {2, 6};
Line(25) = {3, 7};
Line(26) = {4, 8};
Line(27) = {5, 9};

Line Loop(30) = {20, 10, -21};
Plane Surface(31) = {30};

Line Loop(32) = {21, 11, -22};
Plane Surface(33) = {32};

Line Loop(34) = {22, 12, -23};
Plane Surface(35) = {34};

Line Loop(36) = {23, 13, -20};
Plane Surface(37) = {36};


Line Loop(40) = {24, 14, -25, -10};
Plane Surface(41) = {40};

Line Loop(42) = {25, 15, -26, -11};
Plane Surface(43) = {42};

Line Loop(44) = {26, 16, -27, -12};
Plane Surface(45) = {44};

Line Loop(46) = {27, 17, -24, -13};
Plane Surface(47) = {46};


Transfinite Line{10, 11, 12, 13} = 40; // Using Progression 2;
Transfinite Line{14, 15, 16, 17} = 40;
Transfinite Line{20, 21, 22, 23} = 40; 
Transfinite Line{24, 25, 26, 27} = 10; // Using Progression 1.1; // 10

//Recombine Surface{31,33,35,37};

Physical Line("BOUNDARY") = {14, 15, 16, 17};
Physical Surface("SC") = {31, 33, 35, 37};
Physical Surface("AIR") = {41, 43, 45, 47};

Mesh 2;

Save "wire.msh";
