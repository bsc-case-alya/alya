!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine lexcha( lg_vari )
  !-----------------------------------------------------------------------
  !****f* iexcha
  ! NAME
  !    iexcha
  ! DESCRIPTION
  !    This routine exchange integer data individually
  ! USES
  ! USED BY
  !    nsi_sendat
  !    nsa_sendat
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_kintyp_basic,     only :  ip, lg
  implicit none

  logical(lg), intent(inout)    ::  lg_vari
  integer(ip)                   ::  ip_vari
  
  if( lg_vari )then
        ip_vari = 1_ip
  else
        ip_vari = 0_ip
  endif
  call iexcha( ip_vari )
  if( ip_vari == 1_ip )then
        lg_vari = .True.
  else
        lg_vari = .False.
  endif
end subroutine lexcha
