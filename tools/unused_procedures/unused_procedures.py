###################################################################
# Copyright 2005 - 2022 Barcelona Supercomputing Center.          #
# Distributed under the ALYA AVAILABLE SOURCE ("ALYA AS") LICENSE #
# for nonprofit scientific purposes only.                         #
# See companion file LICENSE.txt.                                 #
###################################################################



#---------------------------------------------------------------
#
# Check if declared subroutines are called somewhere in the code
#
#---------------------------------------------------------------

import os
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'
   
directory = 'src'
for root_call, dirs_call, files_call in os.walk(directory):
    my_dir = "Checking directory "+root_call
    print(" ")
    print(my_dir)
    print("-"*len(my_dir))
    for filename_call in files_call:
        if filename_call.endswith(".f90"):
            #
            # Not considering certain files
            #
            if filename_call != "dagmg_mumps.f90":
                with open(root_call+"/"+filename_call) as f_call:
                    data_call = f_call.read().lower()
                    for item in data_call.split("\n"):
                        if " subroutine " in item:
                            item = item.replace("("," ")
                            flat_list = item.split()
                            #print(filename_call)
                            #print(item)
                            position = flat_list.index("subroutine")+1
                            if position < len(flat_list)-1:
                                subrou = flat_list[position].lower()
                                #
                                # It would ne nice te detect subroutines declared in interfaces
                                #
                                if "_rp" not in subrou and "_ip" not in subrou and "_lg" not in subrou and "_ch" not in subrou and "_scalar" not in subrou and "_vector" not in subrou:
                                    found = 0
                                    mycall1 = ("call "+subrou).lower()
                                    mycall2 = "% "+subrou
                                    mycall3 = "%"+subrou
                                    for root, dirs, files in os.walk(directory):
                                        for filename in files:
                                            if filename.endswith(".f90"):
                                                with open(root+"/"+filename) as f:
                                                    data = f.read().lower()
                                                    if mycall1 in data or mycall2 in data or mycall3 in data:
                                                        found = found + 1
                                    if found == 0:
                                        subrou = ""+subrou
                                        print("No call for "+color.RED+subrou+color.END+" defined in file "+filename_call)
                                    else:
                                        found = 0

