!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!***************************************************************
!*
!*              Module for numerical differentiation
!* 
!***************************************************************
MODULE mod_numDer

  use def_kintyp_basic,       only: ip,rp,lg
  
  IMPLICIT NONE
  SAVE
  
  public :: numericalDerivative,numericalHessian
  
CONTAINS
  !
  !
  !
  function numericalDerivative(fun,dim,x) result(df)
    !***************************************************
    !*
    !*   Computes the distortion and its derivative on the objective node of the SRF mesh
    !*
    !***************************************************
    implicit none
    !
    ! input-output variables
    interface
      function fun(dim,x) result(f)
        use def_kintyp_basic,       only: ip,rp
        integer(ip), intent(in)  :: dim
        real(rp),    intent(in)  :: x(dim)
        real(rp) :: f
     end function fun
    end interface
    
    integer(ip),intent(in)  :: dim
    real(rp),intent(in)   :: x(dim)
    real(rp) :: df(dim)
    
    real(rp) :: df_bis(dim)
    
    integer(ip) :: idim
    real(rp)    :: eps3, typ, ei, temp
    real (rp)   :: x_min(dim),x_plus(dim)
    real(rp)    :: f_plus, f_min

    eps3 = 4.64e-6_rp ! 1.0e-4 ! 4.64e-6
    typ  = 0.5_rp

    do idim=1,dim

      ei   = eps3*max( abs(x(idim)),  typ )!*sign(1.0,x(1))  ! sign is not necessary (centered)
      temp = x(idim) +  ei
      ei   = temp -  x(idim)

      x_min=x
      x_min(idim)=x_min(idim)-ei
      f_min= fun(dim,x_min)

      x_plus=x
      x_plus(idim)=x_plus(idim)+ei
      f_plus= fun(dim,x_plus)

      df(idim) = (f_plus-f_min)/(2.0_rp*ei)

    end do
    
!     df_bis = numericalDerivative_harcoded23(fun,dim,x)
!
!     if( norm2(df-df_bis)>1e-12) then
!       print*,'error in df numer'
!       print*,df
!       print*,df_bis
!       call runend("error")
!     end if

  end function numericalDerivative
  !
  !
  !
  function numericalHessian(fun,dim,x) result(H)
    !***************************************************
    !*
    !*   Computes the distortion and its derivative on the objective node of the SRF mesh
    !*
    !***************************************************
    implicit none
    !
    ! input-output variables
    interface
      function fun(dim,x) result(f)
        use def_kintyp_basic,       only: ip,rp
        integer(ip), intent(in)  :: dim
        real(rp),    intent(in)  :: x(dim)
        real(rp) :: f
     end function fun
    end interface

    integer(ip),intent(in)  :: dim
    real(rp),   intent(in)   :: x(dim)

    real(rp) :: H(dim,dim)
    real(rp) :: H_bis(dim,dim)

    integer(ip) :: idim, jdim
    real(rp)    :: eps3, typ, ei, ej, temp
    real (rp)   :: x_aux(dim)
    real(rp)    :: fPiPj,fPiMj,fMiPj,fMiMj,f
    real(rp) :: kkaux

    eps3 = 1.0e-4_rp ! 4.64e-6!
    typ  = 0.5_rp

!     print*,"computing f"
    f = fun(dim,x)
!     print*,"x: ",x
!     print*,"f: ",f

    do idim=1,int(size(x),ip)

      ei   = eps3*max( abs(x(idim)),  typ )!*sign(1.0,x(1))  ! sign is not necessary (centered)
      temp = x(idim) +  ei
      ei   = temp -  x(idim)
      
      do jdim=idim,int(size(x),ip)
        
        ej   = eps3*max( abs(x(jdim)),  typ )!*sign(1.0,x(1))  ! sign is not necessary (centered)
        temp = x(jdim) +  ej
        ej   = temp -  x(jdim)
       
        x_aux       = x
        x_aux(idim) = x_aux(idim)+ei
        x_aux(jdim) = x_aux(jdim)+ej
        fPiPj       = fun(dim,x_aux)

        x_aux       = x
        x_aux(idim) = x_aux(idim)-ei
        x_aux(jdim) = x_aux(jdim)-ej
        fMiMj       = fun(dim,x_aux)
        
        if(idim.eq.jdim) then
          fPiMj = f
          fMiPj = f
        else
          x_aux       = x
          x_aux(idim) = x_aux(idim)+ei
          x_aux(jdim) = x_aux(jdim)-ej
          fPiMj       = fun(dim,x_aux)

          x_aux       = x
          x_aux(idim) = x_aux(idim)-ei
          x_aux(jdim) = x_aux(jdim)+ej
          fMiPj       = fun(dim,x_aux)
        end if

        !H(idim,jdim) = (fPiPj-fPiMj-fMiPj+fMiMj)/(4_rp*ei*ej)
        H(idim,jdim) = ((fPiPj+fMiMj)-(fPiMj+fMiPj))/(4_rp*ei*ej)
        H(jdim,idim) = H(idim,jdim)
        
!         if((idim==2).and.(jdim==2)) then
!         !if(idim.eq.1 .and. jdim.eq.1) then
! !           print*,"fPiPj: ",fPiPj
! !           print*,"fPiMj: ",fPiMj
! !           print*,"fMiPj: ",fMiPj
! !           print*,"fMiMj: ",fMiMj
! !           print*,"(4_rp*ei*ej)               ",(4_rp*ei*ej)
! !           print*,"(fPiPj-fPiMj)              ",(fPiPj-fPiMj)
! !           print*,"(fPiPj-fPiMj-fMiPj)        ",(fPiPj-fPiMj-fMiPj)
! !           print*,"(fPiPj-fPiMj-fMiPj+fMiMj)  ",(fPiPj-fPiMj-fMiPj+fMiMj)
!           !print*,"(fPiPj+fMiMj)-(fPiMj+fMiPj)",(fPiPj+fMiMj)-(fPiMj+fMiPj)
!           print*,"(fPiPj+fMiMj):           ",(fPiPj+fMiMj)
!           print*,"(fPiPj):           ",(fPiPj)
!           print*,"(fMiMj):           ",(fMiMj)
!           !print*,"(fPiMj+fMiPj)            ",(fPiMj+fMiPj)
!           ! x_aux       = x
! !           x_aux(idim) = x_aux(idim)+ei
! !           x_aux(jdim) = x_aux(jdim)+ej
! !           print*,"x_aux: ",x_aux
!           H_bis = numericalHessian_harcoded23(fun,dim,x)
!           print*,H(2,2)-H_bis(2,2)
!           print*," "
!    !        print*,"(fPiPj+fMiMj):",(fPiPj+fMiMj)
! !           print*,"-fPiMj-fMiPj: ",-fPiMj-fMiPj
! !           print*,"2 tersm: ",fPiPj-2*fPiMj
! !           print*,"3 tersm: ",fPiPj-fPiMj-fMiPj
! !           print*,"2+1 tersm: ",fPiPj-2*fPiMj+fMiMj
! !           print*,"3+1 tersm: ",fPiPj-fPiMj-fMiPj+fMiMj
! !           print*,"2+1 tersm/e: ",(fPiPj-2*fPiMj+fMiMj)/(4_rp*ei*ej)
! !           print*,"3+1 tersm/e: ",(fPiPj-fPiMj-fMiPj+fMiMj)/(4_rp*ei*ej)
! !           print*,(fPiPj-fPiMj-fMiPj+fMiMj)/(4_rp*ei*ej)
!         end if
        
      end do
      
    end do
    
!     ! TODO: remove this check
!     H_bis = numericalHessian_harcoded23(fun,dim,x)
!
!     if( norm2(H-H_bis)>1e-12_rp) then
!       print*,'error in H numer'
!       print*,H(1,:),           H(2,2:3),             H(3,3)
!       print*,       H_bis(1,:),         H_bis(2,2:3),       H_bis(3,3)
!       print*,H(1,:)-H_bis(1,:),H(2,2:3)-H_bis(2,2:3),H(3,3)-H_bis(3,3)
!       print*,H(2,2)-H_bis(2,2)
!       !print*,H
!       !print*,H_bis
!       print*,"x:        ",x
!       call runend("error")
!     end if

    return
  end function numericalHessian
  !
!           ! TODO: remove this check
!           x_aux       = x
!           x_aux(idim) = x_aux(idim)+ei
!           x_aux(jdim) = x_aux(jdim)-ej
! !           print*,"cheking i=j"
! !           kkaux = fun(dim,x_aux)
! !           print*,"fun(dim,x_aux): ",kkaux
! !           kkaux = fun(dim,x)
! !           print*,"fun(dim,x):     ",kkaux
! !           kkaux = fun(dim,x_aux)
!           fPiMj       = fun(dim,x_aux)
! !           print*,"kkaux: ",kkaux
! !           print*,"fPiMj: ",fPiMj
! !           kkaux = fun(dim,x_aux)
! !           print*,"fun(dim,x_aux): ",kkaux
! !           kkaux = fun(dim,x)
! !           print*,"fun(dim,x):     ",kkaux
! !           kkaux = fun(dim,x_aux)
! !           print*,"fun(dim,x_aux): ",kkaux
! !           kkaux = fun(dim,x)
! !           print*,"fun(dim,x):     ",kkaux
!           if(abs(fPiMj-f)>1e-15) then
!             print*,'no son iguals i ho haurien de ser'
!             print*,"fPiMj:    ",fPiMj
!             print*,"f:        ",f
!             print*,"ei:       ",ei
!             print*,"ej:       ",ej
!             print*,"x:        ",x
!             print*,"x_aux:    ",x_aux
!             print*,"fun(dim,x):     ",fun(dim,x)
!             print*,"fun(dim,x_aux): ",fun(dim,x_aux)
!             call runend('xq??')
!           end if
!           ! END CHECK
  !
  !
  !
  function numericalDerivative_harcoded23(fun,dim,x) result(df)
    !***************************************************
    !*
    !*   Computes the distortion and its derivative on the objective node of the SRF mesh
    !*
    !***************************************************
    implicit none
    !
    ! input-output variables
    integer(ip),intent(in)  :: dim
    real(rp),intent(in)   :: x(dim)

    interface
      function fun(dim,x) result(f)
        use def_kintyp_basic,       only: ip,rp
        integer(ip), intent(in)  :: dim
        real(rp),    intent(in)  :: x(dim)
        real(rp) :: f
     end function fun
    end interface

    real(rp) :: df(dim)
    !
    ! inside variables
    !real(rp)    :: f
    real (rp)  :: typ, ex, ey, ez, temp, eps3
    real (rp)  :: x_min(dim),x_plus(dim), f_plus, f_min,fx,fy,fz

    if(dim>3) then
      call runend('numerical derivative implmeneted only for dim<=3')
    end if

    !f = fun(dim,x)

    eps3 = 4.64e-6_rp ! 1.0e-4 ! 4.64e-6
    typ  = 0.5_rp
    ex   = eps3*max( abs(x(1)),  typ )!*sign(1.0,x(1))  ! sign is not necessary (centered)
    temp = x(1) +  ex
    ex   = temp -  x(1)
    ey   = eps3*max( abs(x(2)),  typ)!*sign(1.0,x(2))  ! sign is not necessary (centered)
    temp = x(2) +  ey
    ey   = temp - x(2)
    if(dim>2) then
      ez   = eps3*max(abs(x(3)),   typ )!*sign(1.0,x(3))  ! sign is not necessary (centered)
      temp = x(3) +  ez
      ez   = temp - x(3)
    end if

    ! Derivative with respect x
    x_min   =x
    x_min(1)  =x_min(1)-ex
    f_min   = fun(dim,x_min)

    x_plus  =x
    x_plus(1)  =x_plus(1)+ex
    f_plus   = fun(dim,x_plus)

    fx=(f_plus-f_min)/(2*ex)

!     print*,"ex: ",ex
!     print*,"f_plus: ",f_plus
!     print*,"f_min: ",f_min

    ! Derivative with respect y
    x_min   =x
    x_min(2)  =x_min(2)-ey
    f_min   = fun(dim,x_min)

    x_plus  =x
    x_plus(2)  =x_plus(2)+ey
    f_plus   = fun(dim,x_plus)

     fy=(f_plus-f_min)/(2*ey)

!      print*,"f_plus: ",f_plus
!      print*,"f_min: ",f_min

     if(dim>2) then
       ! Derivative with respect z
       x_min   =x
       x_min(3)  =x_min(3)-ez
       f_min   = fun(dim,x_min)

       x_plus  =x
       x_plus(3)  =x_plus(3)+ez
       f_plus   = fun(dim,x_plus)

        fz=(f_plus-f_min)/(2*ez)

       ! Compose the differential vector
       df(:)=(/ fx, fy, fz /);
     else
       df(:)=(/fx,fy/)
     end if
    return
  end function numericalDerivative_harcoded23
  !
  !
  !
  function numericalHessian_harcoded23(fun,dim,x) result(H)
    !***************************************************
    !*
    !*   Computes the distortion and its derivative on the objective node of the SRF mesh
    !*
    !***************************************************
    implicit none
    !
    ! input-output variables
    integer(ip),intent(in)  :: dim
    real(rp),intent(in)   :: x(dim)

    interface
      function fun(dim,x) result(f)
        use def_kintyp_basic,       only: ip,rp
        integer(ip), intent(in)  :: dim
        real(rp),    intent(in)  :: x(dim)
        real(rp) :: f
     end function fun
    end interface

    real(rp) :: H(dim,dim)
    !
    ! inside variables
    real (rp)  :: f
    real (rp)  :: typ, ex, ey, ez, temp, eps3
    real (rp)  :: x_aux(dim)
    real (rp)  :: fminx,fplusx,fminy,fplusy,fplusz,fminz
    real (rp)  :: fMxPy,fPxMy,fMxPz,fPxMz,fMyPz,fPyMz
    real (rp)  :: fPxPy,fMxMy,fPxPz,fMxMz,fMyMz,fPyPz

    if(dim>3) then
      call runend('numerical derivative implmeneted only for dim<=3')
    end if

    f = fun(dim,x)

    eps3 = 1.0e-4_rp !4.64e-6 !1.0e-4
    typ  = 0.5_rp
    ex   = eps3*max( abs(x(1)),  typ )!*sign(1.0,x(1))  ! sign is not necessary (centered)
    temp = x(1) +  ex
    ex   = temp -  x(1)
    ey   = eps3*max( abs(x(2)),  typ)!*sign(1.0,x(2))  ! sign is not necessary (centered)
    temp = x(2) +  ey
    ey   = temp - x(2)
    if(dim>2) then
      ez   = eps3*max( abs(x(3)),   typ )!*sign(1.0,x(3))  ! sign is not necessary (centered)
      temp = x(3) +  ez
      ez   = temp - x(3)
    end if
    
    x_aux   =x
    x_aux(1)  =x(1)-(2_rp*ex)
    fminx   = fun(dim,x_aux)
    x_aux(1)  =x(1)+(2_rp*ex)
    fplusx   = fun(dim,x_aux)

    x_aux   =x
    x_aux(2)  =x(2)-(2_rp*ey)
    fminy   = fun(dim,x_aux)
    x_aux(2)  =x(2)+(2_rp*ey)
    fplusy  = fun(dim,x_aux)

    x_aux   =x
    x_aux(1)  =x(1)+ex
    x_aux(2)  =x(2)-ey
    fPxMy   = fun(dim,x_aux)
    x_aux(1)  =x(1)-ex
    x_aux(2)  =x(2)+ey
    fMxPy   = fun(dim,x_aux)
    x_aux(1)  =x(1)+ex
    x_aux(2)  =x(2)+ey
    fPxPy   = fun(dim,x_aux)
    x_aux(1)  =x(1)-ex
    x_aux(2)  =x(2)-ey
    fMxMy   = fun(dim,x_aux)

    !H(1,1) = (fplusx-2*f+fminx)/(4*ex*ex)
    !H(2,2) = (fplusy-2*f+fminy)/(4*ey*ey)
    !H(1,2) = (fPxPy-fPxMy-fMxPy+fMxMy)/(4*ex*ey)
    !H(2,1) = H(1,2)
    H(1,1) = ((fplusx+fminx)-(2_rp*f))/(4_rp*ex*ex)
    H(2,2) = ((fplusy+fminy)-(2_rp*f))/(4_rp*ey*ey)
    H(1,2) = ((fPxPy+fMxMy)-(fPxMy+fMxPy))/(4_rp*ex*ey)
    H(2,1) = H(1,2)
    
    
!     print*,'in num der'
!     print*,"(fplusy+fminy):         ",(fplusy+fminy)
!     print*,"(fplusy):         ",(fplusy)
!     print*,"(fminy):         ",(fminy)
    !print*,"2*f:                    ",2_rp*f
    !print*,"((fplusy+fminy)-(2*f)): ",((fplusy+fminy)-(2_rp*f))
!     print*,"(4*ey*ey):              ",(4*ey*ey)

!     print*,"fplusx: ",fplusx
!     print*,"f:      ",f
!     print*,"fminx:  ",fminx
!     print*,4*ex*ex
!     print*,"fplusx+fminx: ",fplusx+fminx
!     print*,"-2*f: ",-2*f
!     print*,"2tens: ",fplusx-2*f
!     print*,"3tens: ",fplusx-f-f
!     print*,(fplusx-f)
!     print*,(fplusx-2_rp*f)
!     print*,(fplusx-2_rp*f+fminx)
!     print*,(fplusx+fminx)-(2_rp*f)
!     print*,(4_rp*ex*ex)
!     print*,(fplusx-f-f+fminx)/(4_rp*ex*ex)
!     print*,(fplusx-2_rp*f+fminx)/(4_rp*ex*ex)
!     print*,(fplusx-2*f+fminx)/(4*ex*ex)

    if(dim>2) then
      x_aux   =x
      x_aux(3)  =x(3)-2*ez
      fminz   = fun(dim,x_aux)
      x_aux(3)  =x(3)+2*ez
      fplusz  = fun(dim,x_aux)

      x_aux   =x
      x_aux(1)  =x(1)+ex
      x_aux(3)  =x(3)-ez
      fPxMz  = fun(dim,x_aux)
      x_aux(1)  =x(1)-ex
      x_aux(3)  =x(3)+ez
      fMxPz  = fun(dim,x_aux)
      x_aux(1)  =x(1)+ex
      x_aux(3)  =x(3)+ez
      fPxPz   = fun(dim,x_aux)
      x_aux(1)  =x(1)-ex
      x_aux(3)  =x(3)-ez
      fMxMz   = fun(dim,x_aux)

      x_aux   =x
      x_aux(2)  =x(2)+ey
      x_aux(3)  =x(3)-ez
      fPyMz  = fun(dim,x_aux)
      x_aux(2)  =x(2)-ey
      x_aux(3)  =x(3)+ez
      fMyPz   = fun(dim,x_aux)
      x_aux(2)  =x(2)+ey
      x_aux(3)  =x(3)+ez
      fPyPz   = fun(dim,x_aux)
      x_aux(2)  =x(2)-ey
      x_aux(3)  =x(3)-ez
      fMyMz   = fun(dim,x_aux)

      H(3,3) = ((fplusz+fminz)-(2*f))/(4*ez*ez)
      H(1,3) = ((fPxPz+fMxMz)-(fPxMz+fMxPz))/(4*ex*ez)
      H(2,3) = ((fPyPz+fMyMz)-(fPyMz+fMyPz))/(4*ey*ez)
!       H(1,3) = ((fPxPz+fMxMz)-(fPyMz+fMyPz))/(4*ex*ez)
!       H(2,3) = ((fPyPz+fMyMz)-(fPxMz+fMxPz))/(4*ey*ez)
      H(3,1) = H(1,3)
      H(3,2) = H(2,3)
    end if


    return
  end function numericalHessian_harcoded23
  !
  !
  !
END MODULE mod_numDer