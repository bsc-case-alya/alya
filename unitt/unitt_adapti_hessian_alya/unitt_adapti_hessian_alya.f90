!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_adapti_hessian_alya ! adapting mesh to analytical mesh size (sequential, only via mod_adapt no mod_amr)
  !
  ! Test boundary mesh
  !
  use def_kintyp,         only : ip,lg,rp
!   use def_master,         only : ITASK_INITIA
!   use def_master,         only : ITASK_ENDTIM
!   use def_master,         only : ITASK_ENDRUN
!   use def_master,         only : ITASK_READ_RESTART
!   use def_master,         only : ITASK_WRITE_RESTART
!   use def_master,         only : kfl_gotim
!   use def_master,         only : kfl_stop
!   use def_master,         only : kfl_goblk
!   use def_master,         only : kfl_gocou
!   use def_coupli,         only : kfl_gozon
!   use def_kermod,         only : kfl_reset
  use mod_AMR,            only : AMR
  
  use def_master
  use def_domain
  use def_kermod
  use mod_AMR,            only : AMR
  
  use def_kermod
!   use def_domain, only: meshe
!   use def_domain, only: npoin
!   use def_domain, only: coord
  
  use mod_memory,             only: memory_alloca, memory_deallo
  use def_adapt,              only: memor_adapt 
  
  use mod_out_paraview, only: out_paraview_inp
  use def_kintyp_mesh_basic

  use mod_strings,               only : integer_to_string
  
  use mod_metricComputation, only: compute_sizeField_from_sol_viaHessian
  
  use mod_quality, only: compute_mesh_quality_shape

  use mod_messages,                            only : messages_live
  
  implicit none
  
  
  type(mesh_type_basic)  :: mesh_out
  character(len=100)     :: mesh_name_out
  integer(ip)            :: numTargetNodes
  integer(ip)            :: numLoopsAdaptivity 
  integer(ip)            :: iloop
  integer(ip)            :: test_function, inode
  real(rp)               :: r, fun_hmin, fun_hmax,b
  real(rp)               :: eps, a
  real(rp)               :: maxMeshSize   
  real(rp)               :: amplitude 
  logical(lg)            :: is_passed_mesh_size
  logical(lg), parameter :: do_export_paraview = .false.
  logical(lg), parameter :: set_AMR_off = .false. ! not run AMR
  real(rp),    pointer   :: hh(:),q(:)
  real(rp),    pointer   :: u(:)
  real(rp),    pointer   :: x(:),y(:)

  nullify(u,x,y,hh,q)
  ! TODOs:
  ! - test convergence -> do in other test
  ! - automatically set max mesh size -> DONE (tested in unitt_adapti_meshSize)
  ! - allow to repair according to a input size field (to perform mehs repair and not adaptation) -> DONE (tested in unitt_adapti_meshSize)
  
  test_function      = 1_ip!4_ip!1_ip

  numLoopsAdaptivity = 1_ip !5_ip
  
  maxMeshSize        = 0.5_rp !100.0_rp!0.5_rp
   
  numTargetNodes     = 1500_ip ! handwritten from input [strictly for visualization]
  
  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction
  
!   if(npoin>0) then
!     meshe(ndivi) % coord(:,1:npoin) = meshe(ndivi) % coord(:,1:npoin)*3
!     meshe(ndivi) % coord(:,1:npoin) = meshe(ndivi) % coord(:,1:npoin) - 1
!   end if
  
  do iloop = 1_ip,numLoopsAdaptivity
    
    !print*,'iloop: ',iloop,'      npoin: ',npoin,'------------------------------------------------------'
    
     if( npoin > 0 ) then
        x=> meshe(ndivi) % coord(1,1:npoin)
        y=> meshe(ndivi) % coord(2,1:npoin)
     end if
!     if(npoin>0) then
!       x=> meshe(ndivi) % coord(1,:)
!       y=> meshe(ndivi) % coord(2,:)
!     end if
    
    nullify(u)
    call memory_alloca(memor_adapt,'u','unitt_adapti_hessian_alya',u,npoin)
    is_passed_mesh_size = test_function>3_ip
    if(npoin>0_ip) then
      is_passed_mesh_size = .false.
      if(     test_function.eq.1_ip) then 
        
        eps = 0.75_rp
        amplitude = 200_rp
        u(:) = 10.0*tanh( amplitude*( (4.0_rp*(x-0.5_rp))**2 + (4.0_rp*(y-0.5_rp))**2 - eps) )
        
!       else if(test_function.eq.2_ip) then
!
!         eps =0.01_rp! 0.25_rp
!         amplitude = 100_rp !30
!         a = 0.2_rp!0.75_rp
!         b = 2.0_rp !4
!         u(:) = tanh( amplitude*( (b*(x-0.5_rp)  )**2 + (b*(y-0.5_rp)  )**2 - eps) ) + &
!                tanh( amplitude*( (b*(x-0.5_rp)-a)**2 + (b*(y-0.5_rp)-a)**2 - eps) ) + &
!                tanh( amplitude*( (b*(x-0.5_rp)-a)**2 + (b*(y-0.5_rp)+a)**2 - eps) ) + &
!                tanh( amplitude*( (b*(x-0.5_rp)+a)**2 + (b*(y-0.5_rp)-a)**2 - eps) ) + &
!                tanh( amplitude*( (b*(x-0.5_rp)+a)**2 + (b*(y-0.5_rp)+a)**2 - eps) )
!
!       else if(test_function.eq.3_ip) then
!
!         a = 100.0_rp
!         u(:) = tanh(a*(y-x))
!
!       else if(test_function.eq.4_ip) then
!         is_passed_mesh_size = .true.
!         do inode=1,meshe(ndivi) %npoin
!           !r = (4.0_rp*(x(inode)-0.5_rp))**2 + (4.0_rp*(y(inode)-0.5_rp))**2
!           r = sqrt( (x(inode)-0.5_rp)**2 + (y(inode)-0.5_rp)**2 )
!           eps = 0.2_rp
!           amplitude = 0.01_rp !0.05_rp
!           fun_hmin = 0.01_rp
!           fun_hmax = 0.2_rp
!           if(       r > eps + amplitude) then
!             u(inode) = fun_hmax
!           else if(  r < eps - amplitude) then
!             u(inode) = fun_hmax
!           else
!             u(inode) = fun_hmin
!           end if
!         end do
!
!       else if(test_function.eq.5_ip) then
!         is_passed_mesh_size = .true.
!         fun_hmin = 0.01_rp
!         u = fun_hmin
      else
        call runend('Not implemented test function in unitt_test')
      end if
    end if
    
    if((iloop==1_ip).and.do_export_paraview) then
      call mesh_out      % init      ('MESH_NEW')
      call mesh_out      % copy      (meshe(ndivi))
      mesh_name_out = './unitt/unitt_adapti_hessian_alya/mesh_proc'//integer_to_string(kfl_paral)//'_'//integer_to_string(iloop-1)
      if(npoin>0_ip) call out_paraview_inp(mesh_out,filename=TRIM(mesh_name_out),nodeField=u)
  
      call compute_sizeField_from_sol_viaHessian(hh,u,ndime,maxMeshSize,numTargetNodes)
      mesh_name_out = './unitt/unitt_adapti_hessian_alya/mesh_h_proc'//integer_to_string(kfl_paral)//'_'//integer_to_string(iloop-1)
      if(npoin>0_ip) call out_paraview_inp(mesh_out,filename=TRIM(mesh_name_out),nodeField=hh)
    
      call mesh_out      % deallo()
    end if
    
    if(.not.set_AMR_off) then
      !call AMR(u,maxMeshSize,is_passed_mesh_size)                                      ! Adaptive mesh refinement
      !call AMR(target_solut=u,maxMeshSize_input=maxMeshSize,is_solut_mesh_size=is_passed_mesh_size)
      call AMR(target_solut_input=u,is_solut_mesh_size_input=is_passed_mesh_size)
    end if
    
     if( npoin > 0 ) then
        x=> meshe(ndivi) % coord(1,1:npoin)
        y=> meshe(ndivi) % coord(2,1:npoin)
     end if

    call memory_deallo(memor_adapt,'u','unitt_adapti_hessian_alya',u)
    call memory_alloca(memor_adapt,'u','unitt_adapti_hessian_alya',u,npoin)
    if(npoin>0_ip) then
      !u(:) = tanh( 30_rp*( (4.0_rp*x-2.0_rp)**2 + (4.0_rp*y-2.0_rp)**2 - eps) )
      !u(:) = tanh( 30_rp*( (4.0_rp*(x-0.5_rp))**2 + (4.0_rp*(y-0.5_rp))**2 - eps) )
      if(     test_function.eq.1_ip) then 
        eps = 0.75_rp
        amplitude = 30_rp
        u(:) = tanh( amplitude*( (4.0_rp*(x-0.5_rp))**2 + (4.0_rp*(y-0.5_rp))**2 - eps) )
!       else if(test_function.eq.2_ip) then
!         u(:) = tanh( amplitude*( (b*(x-0.5_rp)  )**2 + (b*(y-0.5_rp)  )**2 - eps) ) + &
!                tanh( amplitude*( (b*(x-0.5_rp)-a)**2 + (b*(y-0.5_rp)-a)**2 - eps) ) + &
!                tanh( amplitude*( (b*(x-0.5_rp)-a)**2 + (b*(y-0.5_rp)+a)**2 - eps) ) + &
!                tanh( amplitude*( (b*(x-0.5_rp)+a)**2 + (b*(y-0.5_rp)-a)**2 - eps) ) + &
!                tanh( amplitude*( (b*(x-0.5_rp)+a)**2 + (b*(y-0.5_rp)+a)**2 - eps) )
!       else if(test_function.eq.3_ip) then
!         a = 100.0_rp
!         u(:) = tanh(a*(y-x))
!       else if(test_function.eq.4_ip) then
!         do inode=1,meshe(ndivi) %npoin
!           !r = (4.0_rp*(x(inode)-0.5_rp))**2 + (4.0_rp*(y(inode)-0.5_rp))**2
!           r = sqrt( (x(inode)-0.5_rp)**2 + (y(inode)-0.5_rp)**2 )
!           if(       r > eps + amplitude) then
!             u(inode) = fun_hmax
!           else if(  r < eps - amplitude) then
!             u(inode) = fun_hmax
!           else
!             u(inode) = fun_hmin
!           end if
!         end do
!       else if(test_function.eq.5_ip) then
!           u = fun_hmin
      else
        call runend('Not implemented test function in unitt_test')
      end if
    end if
    
    if(do_export_paraview) then
      call mesh_out      % init      ('MESH_NEW')
      call mesh_out      % copy      (meshe(ndivi))
      mesh_name_out = './unitt/unitt_adapti_hessian_alya/mesh_proc'//integer_to_string(kfl_paral)//'_'//integer_to_string(iloop)
      if(npoin>0_ip) call out_paraview_inp(mesh_out,filename=TRIM(mesh_name_out),nodeField=u)
  
      call compute_sizeField_from_sol_viaHessian(hh,u,ndime,maxMeshSize,numTargetNodes)
      mesh_name_out = './unitt/unitt_adapti_hessian_alya/mesh_h_proc'//integer_to_string(kfl_paral)//'_'//integer_to_string(iloop)
      if(npoin>0_ip) call out_paraview_inp(mesh_out,filename=TRIM(mesh_name_out),nodeField=hh)
  
      call mesh_out      % deallo()
    end if
    
    call memory_deallo(memor_adapt,'u','unitt_adapti_hessian_alya',u)
    
  end do
  
  call runend('O.K.!')                                               ! Finish Alya
  
end program unitt_adapti_hessian_alya

!call meshe%output(filename='./unitt/unitt_adapti_hessian_alya/mesh_0')
!call mesh_out % output(FILENAME='./unitt/unitt_adapti_hessian_alya/mesh_'//integer_to_string(kfl_paral)//'_0')


