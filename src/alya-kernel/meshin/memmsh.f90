!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine memmsh(itask)
  !-----------------------------------------------------------------------
  !****f* meshin/memmsh
  ! NAME
  !    memmsh
  ! DESCRIPTION
  !    Allocate/Deallocate meshin arrays
  ! OUTPUT
  ! USED BY
  !    reamsh
  !***
  !-----------------------------------------------------------------------
  use def_kintyp
  use def_domain, only    :  ndime
  use def_parame
  use def_master
  use def_inpout
  use def_meshin
  use mod_memchk
  use mod_iofile
  implicit none
  integer(ip), intent(in) :: itask
  integer(4)              :: istat

  select case(itask)

  case(1_ip)
     !
     ! Sources
     !
     allocate(rsgeo(ndime,ndime,nsour),stat=istat)
     call memchk(zero,istat,memor_msh,'RSGEO','memmsh',rsgeo)              
     allocate(rsour(3,nsour),stat=istat)
     call memchk(zero,istat,memor_msh,'RSOUR','memmsh',rsour)

  case(2_ip)
     !
     ! Allocate memory for renum
     !

  end select

end subroutine memmsh
