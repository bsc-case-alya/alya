!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!------------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_dimbou.f90
!> @author  David Oks 
!> @brief   Immersed boundary method for deformable bodies 
!> @details Immersed boundary method for deformable bodies 
!> @}
!------------------------------------------------------------------------

subroutine nsi_dimbou()

  use def_master, only : rhsid
  use def_kintyp, only : ip, rp 
  use def_domain, only : npoin, ndime 
  use def_coupli, only : kfl_dimbou
  use def_nastin, only : kfl_fixno_nsi, fsifo_nsi

  implicit none

  integer(ip) :: idime, ipoin, idofn

  if( kfl_dimbou ) then
     do ipoin = 1,npoin
        do idime = 1,ndime
           if( kfl_fixno_nsi(idime,ipoin) <= 0 ) then
              idofn = ndime*(ipoin-1) + idime
              rhsid(idofn) = rhsid(idofn) + fsifo_nsi(idime,ipoin)
           end if
        end do
     end do
  end if

end subroutine nsi_dimbou
