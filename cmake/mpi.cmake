#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(init_mpi)
  if (WITH_MPI)
    find_package(MPI)
    if (NOT MPI_FOUND)
      message(FATAL_ERROR "MPI not found!")
    elseif (NOT MPI_Fortran_FOUND)
      message(STATUS "MPI Fortran not found! You can ignore this warning unless errors occur during the build.")
      set(MPI_Fortran_FOUND TRUE)
    else()
      message(STATUS "MPI found!")
    endif()
    if (NOT MPIEXEC_EXECUTABLE)
      if (MPIEXEC)
        set(MPIEXEC_EXECUTABLE ${MPIEXEC} CACHE STRING "mpiexec binary (defined from MPIEXEC CMake variable)")
      endif()
    endif()
    message(STATUS "Maximum available MPI slots ${MPIEXEC_MAX_NUMPROCS}")
    set(MAX_MPI_PROCESS_NUMBER 4 CACHE INTERNAL "")
    if (${MPIEXEC_MAX_NUMPROCS} LESS ${MAX_MPI_PROCESS_NUMBER})
      if (NOT MPIEXEC_PREFLAGS MATCHES "--oversubscribe")
        execute_process(COMMAND ${MPIEXEC_EXECUTABLE} ${MPIEXEC_PREFLAGS} "--oversubscribe" OUTPUT_VARIABLE OVERSUBSCRIBE_TEST ERROR_VARIABLE OVERSUBSCRIBE_TEST)
        if (NOT OVERSUBSCRIBE_TEST MATCHES "unrecognized argument oversubscribe")
          set(MPIEXEC_PREFLAGS ${MPIEXEC_PREFLAGS} "--oversubscribe" CACHE STRING "" FORCE)
        endif()
      endif()
    endif()
    message(STATUS "MPIEXEC_PREFLAGS: ${MPIEXEC_PREFLAGS}")

    set(USEMPIF08 OFF CACHE INTERNAL "")
    if (MPI_FORTRAN_BINDINGS MATCHES "USEMPIF08")
      try_compile(USEMPIF08_COMPILATION ${CMAKE_BINARY_DIR}/CMakeTmp ${CMAKE_HOME_DIRECTORY}/cmake/testUSEMPIF08 testUSEMPIF08)
      if (NOT USEMPIF08_COMPILATION)
        message(FATAL_ERROR "Cannot use MPI fortran 08 module! Change MPI_FORTRAN_BINDINGS CMake variable value to compile with mpi.h")
      endif()
      set(USEMPIF08 ON CACHE INTERNAL "")
    elseif (MPI_FORTRAN_BINDINGS MATCHES "auto")
      if (NOT CMAKE_C_COMPILER_ID STREQUAL "PGI")
        try_compile(USEMPIF08_COMPILATION ${CMAKE_BINARY_DIR}/CMakeTmp ${CMAKE_HOME_DIRECTORY}/cmake/testUSEMPIF08 testUSEMPIF08)
        if (USEMPIF08_COMPILATION)
          message(STATUS "Use MPI fortran 08 module")
          set(USEMPIF08 ON CACHE INTERNAL "")
        else()
          message(STATUS "Use mpi.h include")
        endif()
      else()
        message(STATUS "Use mpi.h include")
      endif()
    endif()
  else()
    message(WARNING "MPI disabled!")
  endif()
endfunction()

function(mpi_fortran)
  if (WITH_MPI)
    if(NOT TARGET MPI::MPI_Fortran)
      add_library(MPI::MPI_Fortran IMPORTED INTERFACE)
      if (NOT ${MPI_Fortran_COMPILE_FLAGS} STREQUAL "")
        STRING(REPLACE "\"" "" ${MPI_Fortran_COMPILE_FLAGS} MPI_Fortran_COMPILE_FLAGS)
        STRING(STRIP ${MPI_Fortran_COMPILE_FLAGS} MPI_Fortran_COMPILE_FLAGS)
      endif()
      set_property(TARGET MPI::MPI_Fortran
                   PROPERTY INTERFACE_COMPILE_OPTIONS ${MPI_Fortran_COMPILE_FLAGS})
      set_property(TARGET MPI::MPI_Fortran
                   PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${MPI_Fortran_INCLUDE_PATH}")
      set_property(TARGET MPI::MPI_Fortran
                   PROPERTY INTERFACE_LINK_LIBRARIES ${MPI_Fortran_LINK_FLAGS} ${MPI_Fortran_LIBRARIES})
    endif()
    target_link_libraries(${PROJECT_NAME} MPI::MPI_Fortran)
  endif()
endfunction()

function(mpi_c)
  if (WITH_MPI)
    if(NOT TARGET MPI::MPI_C)
      add_library(MPI::MPI_C IMPORTED INTERFACE)
      if (NOT ${MPI_C_COMPILE_FLAGS} STREQUAL "")
        STRING(REPLACE "\"" "" ${MPI_C_COMPILE_FLAGS} MPI_C_COMPILE_FLAGS)
        STRING(STRIP ${MPI_C_COMPILE_FLAGS} MPI_C_COMPILE_FLAGS)
      endif()
      set_property(TARGET MPI::MPI_C
                   PROPERTY INTERFACE_COMPILE_OPTIONS ${MPI_C_COMPILE_FLAGS})
      set_property(TARGET MPI::MPI_C
                   PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${MPI_C_INCLUDE_PATH}")
      set_property(TARGET MPI::MPI_C
                   PROPERTY INTERFACE_LINK_LIBRARIES ${MPI_C_LINK_FLAGS} ${MPI_C_LIBRARIES})
    endif()
    target_link_libraries(${PROJECT_NAME} MPI::MPI_C)
  endif()
endfunction()

function(mpi_cxx)
  if (WITH_MPI)
    if(NOT TARGET MPI::MPI_CXX)
      add_library(MPI::MPI_CXX IMPORTED INTERFACE)
      if (NOT ${MPI_CXX_COMPILE_FLAGS} STREQUAL "")
        STRING(REPLACE "\"" "" ${MPI_CXX_COMPILE_FLAGS} MPI_CXX_COMPILE_FLAGS)
        STRING(STRIP ${MPI_CXX_COMPILE_FLAGS} MPI_CXX_COMPILE_FLAGS)
      endif()
      set_property(TARGET MPI::MPI_CXX
                   PROPERTY INTERFACE_COMPILE_OPTIONS ${MPI_CXX_COMPILE_FLAGS})
      set_property(TARGET MPI::MPI_CXX
                   PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${MPI_CXX_INCLUDE_PATH}")
      set_property(TARGET MPI::MPI_CXX
                   PROPERTY INTERFACE_LINK_LIBRARIES ${MPI_CXX_LINK_FLAGS} ${MPI_CXX_LIBRARIES})
    endif()
    target_link_libraries(${PROJECT_NAME} MPI::MPI_CXX)
  endif()
endfunction()
