#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(init_talp)
  set(DLB_INCLUDE_DIR "${DLB_DIR}/include" CACHE INTERNAL "")
  set(DLB_LIB_DIR "${DLB_DIR}/lib" CACHE INTERNAL "")
endfunction()

function(talp)
  if (WITH_DLB_TALP MATCHES "external")
    add_definitions(-DALYA_TALP)
    include_directories(${DLB_INCLUDE_DIR})
    link_directories(${DLB_LIB_DIR})
    if (DLB_DIR STREQUAL "")
      target_link_libraries(${PROJECT_NAME} libdlb.so)
    else()
      target_link_libraries(${PROJECT_NAME} ${DLB_LIB_DIR}/libdlb.so)
      set_target_properties(${PROJECT_NAME} PROPERTIES INSTALL_RPATH "${DLB_LIB_DIR}")
      set_target_properties(${PROJECT_NAME} PROPERTIES BUILD_WITH_INSTALL_RPATH ON)
    endif()
  endif()
endfunction()
