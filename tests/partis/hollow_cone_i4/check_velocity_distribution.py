#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import numpy as np
import matplotlib.pyplot as plt

#
# Put this in .pts.dat to only get the 
# freshly injected droplets in the output:
# OUTPUT_&_POST_PROCESS
#    NOOUT MINEX=-5, MAXEX=-5
# END_OUTPUT_&_POST_PROCESS
#
data = np.loadtxt("hollow_cone_i4.pts.res") 

vx = data[:,7]
d = data[:,15]


plt.figure()
plt.plot(vx,".")

nbins = 10
plt.figure()
plt.hist(vx, bins=nbins)

plt.figure()
plt.hist(d, bins=nbins)

mean = np.mean(vx)
std = np.std(vx)
print("mean: ",mean,"m/s")
print("std:  ",std,"m/s")

mean = np.mean(d)
std = np.std(d)
print("mean: ",mean*1e6,"um")
print("std:  ",std*1e6,"um")

plt.show()


