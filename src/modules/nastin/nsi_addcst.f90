!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_addcst(ndofn,amatr_nsi,amatr)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_addcst
  ! NAME 
  !    nsi_addcst
  ! DESCRIPTION
  !    This routine adds the constant matrix to the momentum
  !    global matrix
  ! USES
  ! USED BY
  !    nsi_matrix
  !***
  !-----------------------------------------------------------------------
  use def_kintyp,   only     :  ip,rp
  use def_domain,   only     :  npoin,ndime,r_dom
  use def_nastin,   only     :  kfl_savco_nsi,kfl_fixno_nsi
  implicit none
  integer(ip), intent(in)    :: ndofn
  real(rp),    intent(inout) :: amatr_nsi(*),amatr(ndofn,ndofn,*)
  integer(ip)                :: ipoin,izdom,idime

  if(kfl_savco_nsi==1) then
     !
     ! Impose boundary conditions on constant matrix
     !     
     do ipoin=1,npoin
        if(kfl_fixno_nsi(1,ipoin)/=0) then
           do izdom=r_dom(ipoin),r_dom(ipoin+1)-1
              amatr_nsi(izdom)=0.0_rp
           end do
        end if
     end do
     
  end if

  if(kfl_savco_nsi>=1) then
     !
     ! Add constant matrix
     !
     kfl_savco_nsi=2     
     do ipoin=1,npoin
        do izdom=r_dom(ipoin),r_dom(ipoin+1)-1
           do idime=1,ndime
              amatr(idime,idime,izdom)=amatr(idime,idime,izdom)&
                   +amatr_nsi(izdom)
           end do
        end do
     end do
  end if

end subroutine nsi_addcst
