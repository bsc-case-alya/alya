!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!




subroutine nsi_elmdcost_all(&
		    elvel,pnode,pgaus,gpsha,gpvol,lnods,elrbu,elrbp)
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_elmdcost_all
  ! NAME 
  !    nsi_elmdcost_all
  ! DESCRIPTION
  !    This subroutine calculates elemental contributions of the dcost .. d(f)/d(U)
  ! USES
  ! USED BY
  !------------------------------------------------------------------------

  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime,ntens,mnode,npoin
  use def_kermod, only       :  kfl_cost_type
  use def_master
  use mod_matrix
  
  implicit none

  integer(ip), intent(in)    :: pnode,pgaus
  
  
  real(rp)    :: gpunk(pgaus),gpveltar(pgaus)
  integer(ip) :: inode,igaus,idime,ipoin
  
  integer(ip),    intent(in)        :: lnods(pnode)
  real(rp),       intent(in)        :: gpvol(pgaus)                          ! |J|*w
  real(rp),       intent(in)        :: elvel(ndime,pnode),gpsha(pnode,pgaus)
  real(rp),       intent(inout)     :: elrbu(ndime,pnode)
  real(rp),       intent(inout)     :: elrbp(pnode)
  
  real(rp)                          :: elmdcost(ndime,pnode)
  
  if (kfl_cost_type == 2) then
    !
    ! initializations
    !
    do inode = 1,pnode
      do idime = 1,ndime
	  elmdcost(idime,inode) = 0.0_rp
      enddo
    end do
    do igaus=1,pgaus
	  gpunk(igaus) = 0.0_rp
    end do
    !
    ! calculate elmdcost
    !
    if (kfl_coupl(ID_CHEMIC,ID_NASTIN) == 0) then
      do igaus=1,pgaus
	  do inode =1,pnode
	    gpunk(igaus) = gpunk(igaus) + gpsha(inode,igaus)*elvel(1,inode)
	  end do
	  do inode =1,pnode
	    elmdcost(1,inode) = elmdcost(1,inode) + gpvol(igaus)*2*gpsha(inode,igaus)*gpunk(igaus)
	  end do
      end do 
    endif
    !
    ! elrbu = elrbu + elmdcost
    !  
    do inode = 1,pnode
      do idime = 1,ndime
	  elrbu(idime,inode) = elrbu(idime,inode) - elmdcost(idime,inode)
      enddo
    end do
  
  endif
  
 
end subroutine nsi_elmdcost_all
