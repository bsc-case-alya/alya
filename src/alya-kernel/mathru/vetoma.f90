!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine vetoma(vecto,xmatr,ndime,ntens)
  !**********************************************************************
  !                                      
  !**** This routine stores a vector VECTO as a symmetric matrix XMATR
  !
  !**********************************************************************
  use      def_kintyp
  implicit none
  integer(ip),intent(in)   :: ndime,ntens
  real(rp),intent(out)     ::xmatr(ndime,ndime)
  real(rp),intent(in)      ::vecto(ntens)

  if(ndime==1) then
     xmatr(1,1)=vecto(1)
  else if(ndime==2) then
     xmatr(1,1)=vecto(1)
     xmatr(1,2)=vecto(3)
     xmatr(2,1)=vecto(3)
     xmatr(2,2)=vecto(2)
  else
     xmatr(1,1)=vecto(1)
     xmatr(1,2)=vecto(4)
     xmatr(1,3)=vecto(5)
     xmatr(2,1)=vecto(4)
     xmatr(2,2)=vecto(2)
     xmatr(2,3)=vecto(6)
     xmatr(3,1)=vecto(5)
     xmatr(3,2)=vecto(6)
     xmatr(3,3)=vecto(3)
  end if

end subroutine vetoma
