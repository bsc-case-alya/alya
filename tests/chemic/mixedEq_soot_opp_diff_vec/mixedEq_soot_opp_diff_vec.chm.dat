$------------------------------------------------------------------------
PHYSICAL_PROBLEM
  PROBLEM_DEFINITION
    MODEL:            MIXED 
    TEMPORAL:         ON
    CONVECTION:       ON, VELOC
    DIFFUSION:        ON
  END_PROBLEM_DEFINITION

  PROPERTIES:
	TAB_FRAMEWORK: 1
  END_PROPERTIES

  SOOT_MODEL
    MODEL:          SECTIONAL $SECTIONAL
    SECTIONS:       30
    DENSITY:        1860
    VMAX_SOOT:      5.23e-16
    RAD_PARAM:      0.85
    GAS_COUPLING:   ON
    VALIDATION:     OFF $$CHEM1D
    NUCLEATION:     ON $$ / C2H2, C16H10
    CONDENSATION:   ON $$ / ON
    COAGULATION:    ON $$ / ON
    SURFACE_GROWTH: ON $$ / JACA / OTHER
    YK_FRAMEWORK:   2
  END_SOOT_MODEL

  EQUATIONS, NGROUP=2  NEQUA=32
    IGROU = 1, TYPE = CONTROL,   NEQUA = 2  $ groop for control variabls
    IGROU = 2, TYPE = SECTI,     NEQUA = 30 $ group for soot
    $
    $ Turn on thermophoretic diffusion for the sectional group:
    $ 
    IGROU = 2, TPH_DIFUSION=ON

	$
    $ Control variables
    $
    IEQUA = 1, TYPE = Z
    IEQUA = 2, TYPE = Yc, TYPE_SRC = TABLE, SRC_FRAMEWORK=1, SRC_COLUMN = 1
    IEQUA = 1, INI_FIELD=3, DO_POSTPROCESS 
    IEQUA = 2, INI_FIELD=4, DO_POSTPROCESS  

	
    $
    $ Set equation properties by the gropu:
    $	
    IGROU = 2, EQ_NAME=DEFAULT
    IGROU = 2, DO_POSTPROCESS
    IGROU = 2, LEWIS=100000.0 
    IGROU = 2, EQ_TYPE=SECTI
    IGROU = 2, EQ_SRC_TYPE=DSM

  END_EQUATIONS

END_PHYSICAL_PROBLEM
$------------------------------------------------------------------------
NUMERICAL_TREATMENT
  STABILIZATION:           ASGS
  ELEMENT_LENGTH:          MINIMUM
  TIME_INTEGRATION:        RUNGE, ORDER: 3
  SAFETY_FACTOR=           100.0

  STEADY_STATE_TOLERANCE=  1e-8
  NORM_OF_CONVERGENCE:     L2
  CONVERGENCE_TOLERANCE=   1e-6

  ALGEBRAIC_SOLVER
      SOLVER:               EXPLICIT, LUMPED
  END_ALGEBRAIC_SOLVER

  NEGATIVE_CLIP =         ON
  POSITIVE_CLIP =         ON 
END_NUMERICAL_TREATMENT
$------------------------------------------------------------------------
OUTPUT_&_POST_PROCESS
  START_POST  STEP = 0
  POSTPROCESS NAMED_UNKNOWNS
  POSTPROCESS SCONC
  POSTPROCESS SOURC
  POSTPROCESS CONDU
  POSTPROCESS VISCO
  POSTPROCESS XZR
  POSTPROCESS YKSSM
  POSTPROCESS VTHERMO      $ thermophoretic velocity
  POSTPROCESS Q_NUC_TOTAL  $ total soot source term due to nucleation
  POSTPROCESS Q_COA_TOTAL  $ total soot source term due to coagualtion: should be zero
  POSTPROCESS Q_CON_TOTAL  $ total soot source term due to condensation
  POSTPROCESS Q_SUR_TOTAL  $ total soot source term due to surface growth
  POSTPROCESS Q_TOT_TOTAL  $ total soot source term due to all effects   
  WITNESS 
    CONCE
    SCONC
  END_WITNESS
END_OUTPUT_&_POST_PROCESS
$------------------------------------------------------------------------
BOUNDARY_CONDITIONS
 CODES, NODES, CLASS = 1
    1    1   1.0
    2    1   0.0
  1 & 3  1   1.0
  2 & 3  1   0.0
 END_CODES
 CODES, NODES, CLASS = 2
    1    1   0.0
    2    1   0.0 
  1 & 3  1   0.0
  2 & 3  1   0.0
 END_CODES

 INCLUDE  ./mesh/sootBoundary.dat

END_BOUNDARY_CONDITIONS
$------------------------------------------------------------------------
