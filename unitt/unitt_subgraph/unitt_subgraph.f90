!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_subgraph
  !
  ! Order strings
  !
  use def_kintyp_basic, only : ip,rp
  use mod_graphs
  use mod_matrix

  implicit none

  character(10)        :: wtest(4)
  integer(ip)          :: nn1,nn2,nz1,nz2,ii,nz,ndof
  integer(ip), pointer :: ia1(:),ia2(:)
  integer(ip), pointer :: ja1(:),ja2(:)
  real(rp),    pointer :: aa1(:,:,:),aa2(:)
  integer(ip), pointer :: invpr(:)
  integer(ip), pointer :: permr(:)
  integer(8)           :: memor(2)

  memor=0_8
  nn1 = 3
  nn2 = 2
  nz1 = 9
  nz2 = 4
  ndof = 2
  allocate(ia1(nn1+1))
  allocate(ia2(nn1+1))
  allocate(ja1(nz1))
  allocate(ja2(nz1))
  allocate(aa1(ndof,ndof,nz1))
  allocate(invpr(nn1))
  allocate(permr(nn1))
  ia1(1)   =  1
  ia1(2)   =  4
  ia1(3)   =  7
  ia1(4)   = 10
  ja1      = (/ 1,2,3,1,2,3,1,2,3 /)
  aa1      = 0.0_rp
  aa1(1,1,1) = 1.1_rp
  aa1(2,2,1) = 1.2_rp
  aa1(1,1,2) = 2.1_rp
  aa1(2,2,2) = 2.2_rp
  aa1(1,1,3) = 3.1_rp
  aa1(2,2,3) = 3.2_rp
  aa1(1,1,4) = 4.1_rp
  aa1(2,2,4) = 4.2_rp
  aa1(1,1,5) = 5.1_rp
  aa1(2,2,5) = 5.2_rp
  aa1(1,1,6) = 6.1_rp
  aa1(2,2,6) = 6.2_rp
  aa1(1,1,7) = 7.1_rp
  aa1(2,2,7) = 7.2_rp
  aa1(1,1,8) = 8.1_rp
  aa1(2,2,8) = 8.2_rp
  aa1(1,1,9) = 9.1_rp
  aa1(2,2,9) = 9.2_rp
  
  invpr(1) = 1
  invpr(2) = 0
  invpr(3) = 2
  permr(1) = 1
  permr(2) = 3

  ia2 = ia1
  ja2 = ja1
  
  nz2 = nz1
  call graphs_subgra(nn1,nz2,permr,invpr,ia2,ja2,memor=memor)
  allocate(aa2(nz2))

  call matrix_copy_matrix_block(&
       nn1  , ndof,  &
       2_ip , ia1, ja1 , &
       aa1  , aa2,    &
       ia2, ja2, invpr)
  
  do ii = 1,nn2
     write(*,*) 'node ',ii,': ',ja2(ia2(ii):ia2(ii+1)-1)
  end do

  if( abs(aa2(1)-aa1(2,2,1)) > 1.0e-10_rp ) stop 1
  if( abs(aa2(2)-aa1(2,2,3)) > 1.0e-10_rp ) stop 1
  if( abs(aa2(3)-aa1(2,2,7)) > 1.0e-10_rp ) stop 1
  if( abs(aa2(4)-aa1(2,2,9)) > 1.0e-10_rp ) stop 1

end program unitt_subgraph
