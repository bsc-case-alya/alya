!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_elmini(pevat,pgaus,wmatr,wrhsi)
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_elmini
  ! NAME 
  !    nsi_elmini
  ! DESCRIPTION
  !    Initialize matrix and RHS
  ! USES
  ! USED BY
  !***
  !-----------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  implicit none
  integer(ip), intent(in)  :: pevat,pgaus
  real(rp),    intent(out) :: wmatr(pevat,pevat,pgaus)
  real(rp),    intent(out) :: wrhsi(pevat,pgaus)
  integer(ip)              :: ievat,jevat,igaus
  !
  ! Initialization
  !
  do igaus=1,pgaus
     do ievat=1,pevat
        wrhsi(ievat,igaus)=0.0_rp
        do jevat=1,pevat
           wmatr(jevat,ievat,igaus)=0.0_rp
        end do
     end do
  end do

end subroutine nsi_elmini
