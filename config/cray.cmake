#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



message(STATUS "Compiling with cray")
set(CMAKE_Fortran_COMPILER ftn)
set(CUSTOM_Fortran_FLAGS_DEBUG "-g")
set(CUSTOM_Fortran_FLAGS_O0 "-O0")
set(CUSTOM_Fortran_FLAGS_O1 "-O1")
set(CUSTOM_Fortran_FLAGS_O2 "-O2 -h ipa2")
set(CUSTOM_Fortran_FLAGS_O3 "-O3 -h ipa2")
set(CUSTOM_Fortran_FLAGS "-c -hnoomp -hnoacc -e F -e I -e T -e Z -sdefault32 -hlist=a")


