!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Adaptivity
!> @{
!> @file    mod_debugTools.f90
!> @author  abel.gargallo
!> @date    2021-03-31
!> @brief   Simple module to allow/remove outputing in adaptivity
!> @details Simple module to allow/remove outputing in adaptivity
!-----------------------------------------------------------------------
MODULE mod_debugTools
  use def_kintyp_basic,       only: ip,rp,lg

!   use mod_parall
!   use def_parall
  
  implicit none
  
  logical(lg), parameter :: out_debug_text      = .false.!.true.!
  logical(lg), parameter :: out_debug_paraview  = .false.!.true.!
  
  logical(lg), parameter :: out_debug_paraview_firstLast  = .false.!.false.!.true.!
  integer(ip) :: out_id_firstLast = 0_ip
  
  private  
  public :: out_debug_text, out_debug_paraview
  public :: out_debug_paraview_firstLast, out_id_firstLast
  
  
CONTAINS
  !
  !
  !
END MODULE mod_debugTools