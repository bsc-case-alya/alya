!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine limite(nbvar,ia,ja,an)  
  !-----------------------------------------------------------------------
  ! 
  ! Algebraic limiter
  !
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp
  use def_master, only       :  INOTMASTER
  use def_solver, only       :  solve_sol
  use def_domain, only       :  npoin
  implicit none
  integer(ip), intent(in)    :: nbvar 
  real(rp),    intent(inout) :: an(nbvar,nbvar,*)
  integer(ip), intent(in)    :: ja(*),ia(*)
  integer(ip)                :: ii,jj,kk,ll,nn

  if( INOTMASTER .and. solve_sol(1)%kfl_limit == 1 ) then

     if( nbvar == 1 ) then
        !
        ! NBVAR = 1
        !
        do ii= 1, npoin 
           jj = ia(ii)
           ll = -1
           do while ( jj < ia(ii+1) .and. ll == -1 )
              if(ja(jj)==ii) then
                 ll = jj
              end if
              jj = jj + 1
           end do
           jj = ia(ii)
           do while ( jj < ia(ii+1) )
              if( jj /= ll .and. an(1,1,jj) > 0.0_rp ) then
                 an(1,1,ll) = an(1,1,ll) + an(1,1,jj)
                 an(1,1,jj) = 0.0_rp
              end if
              jj = jj + 1
           end do
        end do

     else
        !
        ! NBVAR > 1
        !
        do ii= 1, npoin 
           
           jj = ia(ii)
           ll = -1
           do while ( jj < ia(ii+1) .and. ll == -1 )
              if(ja(jj)==ii) then
                 ll = jj
              end if
              jj = jj + 1
           end do

           jj = ia(ii)
           do while ( jj < ia(ii+1) )
              if( jj == ll ) then
                 do kk = 1,nbvar
                    do nn = 1,nbvar
                       if( an(kk,nn,ll) > 0.0_rp .and. kk /= nn ) then
                          an(nn,nn,ll) = an(nn,nn,ll) + an(kk,nn,ll)
                          an(kk,nn,ll) = 0.0_rp
                       end if
                    end do
                 end do
              else
                 do kk = 1,nbvar
                    do nn = 1,nbvar
                       if( an(kk,nn,jj) > 0.0_rp ) then
                          an(nn,nn,ll) = an(nn,nn,ll) + an(kk,nn,jj)
                          an(kk,nn,jj) = 0.0_rp
                       end if
                    end do
                 end do
              end if
              jj = jj + 1
           end do

        end do

     end if
  end if

end subroutine limite
