#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(init_metis)
  if (WITH_METIS MATCHES "auto")
    set(ASSERT OFF CACHE INTERNAL "")
    set(ASSERT2 OFF CACHE INTERNAL "")
    set(DEBUG OFF CACHE INTERNAL "")
    set(GDB OFF CACHE INTERNAL "")
    set(GKRAND OFF CACHE INTERNAL "")
    set(GKREGEX OFF CACHE INTERNAL "")
    set(GPROF OFF CACHE INTERNAL "")
    set(METIS_REAL "64" CACHE INTERNAL "")
    set(OPENMP OFF CACHE INTERNAL "")
    if (INTEGER_SIZE MATCHES "4")
      set(METIS_INTEGER "32" CACHE INTERNAL "")
    elseif (INTEGER_SIZE MATCHES "8")
      set(METIS_INTEGER "64" CACHE INTERNAL "")
    endif()
  elseif (WITH_METIS MATCHES "external")
     set(METIS_INCLUDE_DIR "${METIS_DIR}/include" CACHE INTERNAL "METIS: include directory" FORCE)
     set(METIS_LIB_DIR "${METIS_DIR}/lib" CACHE INTERNAL "METIS: library directory" FORCE)
  endif()

endfunction()

function(define_metis)
  if (NOT WITH_METIS MATCHES "OFF")
    add_definitions(-DV51METIS)
  endif()
endfunction()

function(link_metis)
  if (WITH_METIS MATCHES "auto")
    include_directories(${METIS_INCLUDE_DIR})
    target_link_libraries(${PROJECT_NAME} metis)
  elseif(WITH_METIS MATCHES "external")
    include_directories(${METIS_INCLUDE_DIR})
    find_library(LIBMETIS metis PATHS ${METIS_LIB_DIR})
    target_link_libraries(${PROJECT_NAME} ${LIBMETIS})
  endif()
endfunction()


