!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine cexcha(nsize,chava)
  !-----------------------------------------------------------------------
  !****f* cexcha
  ! NAME
  !    cexcha
  ! DESCRIPTION
  !    This routine exchange character data individually
  ! USES
  ! USED BY
  !    *_sendat
  !    *_parall
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  implicit none
  integer(ip), intent(in) :: nsize
  character(*)            :: chava
  integer(ip)             :: iposi,parch_size

  iposi = nparc
  nparc = nparc + nsize
  parch_size = len(parch)

  if( parii == 2 ) then

     if( kfl_ptask == 1 ) then

        if( INOTSLAVE ) then
           if( iposi+nsize > parch_size ) call runend('CEXCHA: MAXIMUM PARCH LENGTH EXCEEDED')
           parch(iposi+1:iposi+nsize) = chava(1:nsize)
        else
           if( iposi+nsize > parch_size ) call runend('CEXCHA: MAXIMUM PARCH LENGTH EXCEEDED')
           chava(1:nsize) = parch(iposi+1:iposi+nsize)
        end if

     else if( kfl_ptask == 0 ) then

        if( INOTSLAVE ) then
           if( iposi+nsize > parch_size ) call runend('CEXCHA: MAXIMUM PARCH LENGTH EXCEEDED')
           parch(iposi+1:iposi+nsize) = chava(1:nsize)
        end if

     else if( kfl_ptask == 2 ) then

        if( iposi+nsize > parch_size ) call runend('CEXCHA: MAXIMUM PARCH LENGTH EXCEEDED ')
        chava(1:nsize) = parch(iposi+1:iposi+nsize)

     end if

  end if

end subroutine cexcha
