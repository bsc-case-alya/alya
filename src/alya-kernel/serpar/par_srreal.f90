!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_srreal(itask,ndimr,rvarr)
  !------------------------------------------------------------------------
  !****f* Parall/par_srreal
  ! NAME
  !    par_srreal
  ! DESCRIPTION
  ! OUTPUT
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only    :  ip,rp
  use def_master, only    :  npari,nparr,nparc,parin,parre
  implicit none
  integer(ip), intent(in) :: itask,ndimr
  real(rp),    target     :: rvarr(max(1_ip,ndimr))

  npari = 0
  nparc = 0

  select case ( itask )

  case ( 1_ip ) 

     nparr =  ndimr
     parre => rvarr
     call par_sendin()
     nullify(parre)

  case ( 2_ip )

     nparr =  ndimr
     parre => rvarr
     call par_receiv()
     nullify(parre)

  end select
  
end subroutine par_srreal
 
