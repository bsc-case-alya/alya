!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_adapti_edgeBound
  !
  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_master
  use mod_memory
  
  use mod_meshTopology, only : setBoundaryEdgeData, edgeData_type
  
  
  !
  implicit none
  !
  type(mesh_type_basic)           :: mesh
  real(rp)    :: hx,hy,x0,x1,y0,y1,r,PI,z0,z1,hz
  integer(ip) :: nx,ny,ratioXY,nz, itet
  integer(ip) :: ipoin,idime,ielem,inode,iboun,idim,ix,iy,quad(4),hex(8),iz
  type(edgeData_type) :: edgeData_boun
  logical(lg) :: areOK_edges
  !
  call elmgeo_element_type_initialization()
  
  call mesh     % init('MY_MESH')
  
  ratioXY = 1_ip
  nx = 2_ip ! Don't change.. checking that edge data is correct at the end
  ny = nx
  nx = nx*ratioXY
  
  mesh % nelem = nx*ny*2
  mesh % npoin = (nx+1)*(ny+1)
  mesh % ndime = 2
  mesh % mnode = 3
  call mesh % alloca()
  mesh % ltype(:)  = TRI03
  
  x0 = -1.0_rp*ratioXY
  x1 =  1.0_rp*ratioXY
  y0 = -1.0_rp
  y1 =  1.0_rp
  hx = (x1-x0)/nx
  hy = (y1-y0)/ny
  do iy=0,ny
    do ix=0,nx
      inode = (1+ix) + iy*(1+nx)
      mesh%coord(:, inode ) = (/ x0 + hx*ix, y0 + hy*iy/)
      if((ix>0).and.(iy>0)) then
        quad(1) = (1+ix-1) + (iy-1)*(1+nx) 
        quad(2) = (1+ix  ) + (iy-1)*(1+nx) 
        quad(3) = (1+ix  ) + (iy-0)*(1+nx) 
        quad(4) = (1+ix-1) + (iy-0)*(1+nx) 
        mesh%lnods(:, (2*ix - 1) + (iy-1)*(2*nx) ) = quad(1:3)
        mesh%lnods(:, (2*ix    ) + (iy-1)*(2*nx) ) = quad((/1,3,4/))
      end if
    end do
  end do
  
  edgeData_boun = setBoundaryEdgeData(mesh%lnods)
  
  areOK_edges = edgeData_boun%isEdge(1_ip,2_ip).and.&
                edgeData_boun%isEdge(2_ip,3_ip).and.&
                edgeData_boun%isEdge(1_ip,4_ip).and.&
                edgeData_boun%isEdge(3_ip,6_ip).and.&
                edgeData_boun%isEdge(4_ip,7_ip).and.&
                edgeData_boun%isEdge(6_ip,9_ip).and.&
                edgeData_boun%isEdge(7_ip,8_ip).and.&
                edgeData_boun%isEdge(8_ip,9_ip)
  if(.not.areOK_edges) then
    print*,'error in detecting/storing some boundary edge'
    call runend('error in detecting/storing some boundary edge')
  end if
  
  areOK_edges = edgeData_boun%isEdge(1_ip,2_ip).and.&
                edgeData_boun%isEdge(2_ip,1_ip)
  if(.not.areOK_edges) then
    print*,'edge and inverse edge'
    call runend('edge and inverse edge')
  end if
  
  areOK_edges = .not.(edgeData_boun%isEdge(1_ip,5_ip).or.&
                edgeData_boun%isEdge(2_ip,6_ip).or.&
                edgeData_boun%isEdge(4_ip,8_ip).or.&
                edgeData_boun%isEdge(5_ip,9_ip).or.&
                edgeData_boun%isEdge(2_ip,5_ip).or.&
                edgeData_boun%isEdge(5_ip,8_ip).or.&
                edgeData_boun%isEdge(4_ip,5_ip).or.&
                edgeData_boun%isEdge(5_ip,6_ip) )
  if(.not.areOK_edges) then
    print*,'a interior edge is detected as exterior...'
    call runend('a interior edge is detected as exterior...')
  end if
  
  
  !call edgeData_boun%edge_dict%printKeys()
  
  
!   call runend('O.K.!')                                               ! Finish Alya
  
end program unitt_adapti_edgeBound






