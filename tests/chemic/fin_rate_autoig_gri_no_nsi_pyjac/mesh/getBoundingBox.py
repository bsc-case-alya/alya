#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import sys
meshName=sys.argv[1]
direction=sys.argv[2]
minmax=sys.argv[3]
coordName='{}.coord'.format(meshName)

f=open(coordName,'r')

boundMax=[float('-Inf'),float('-Inf'),float('-Inf')]
boundMin=[float('Inf' ),float('Inf' ),float('Inf' )]


for line in f:
	data = line.split()
	for i in range(len(data)-1):
		boundMax[i] = max(boundMax[i],float(data[i+1])) 
		boundMin[i] = min(boundMin[i],float(data[i+1])) 

if boundMin[2] == float('Inf' ):
	boundMin[2] = 0.0
if boundMax[2] == float('-Inf'):
	boundMax[2] = 0.0


if minmax == 'min':
	res = boundMin
else:
	res = boundMax

if direction == 'x':
	print('{}'.format(res[0]))

if direction == 'y':
	print('{}'.format(res[1]))

if direction == 'z':
	print('{}'.format(res[2]))

f.close()

