!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_outres()

!------------------------------------------------------------------------
!****f* Nastin/nsi_outres
! NAME 
!    nsi_outres
! DESCRIPTION
!    This routine output the velocity residual
! USES
! USED BY
!    nsi_output
!------------------------------------------------------------------------
  use def_master
  use def_domain
  use def_nastin
  use mod_communications, only : PAR_MAX
  implicit none
  integer(ip) :: ipoin,idime
  real(rp)    :: rnorm,resid(2),rmax
  !
  ! Compute residual
  !
  rnorm = 0.0_rp
  
  do ipoin = 1,npoin
     resid = 0.0_rp
     do idime = 1,ndime
        resid(1) = resid(1) + (veloc(idime,ipoin,1)-veold_nsi(idime,ipoin))**2
        resid(2) = resid(2) + veloc(idime,ipoin,1)**2
     end do
     resid        = sqrt(resid)
     rnorm        = max(rnorm,resid(2))
     gesca(ipoin) = resid(1)
  end do

  call PAR_MAX(rnorm)

  rmax = 0.0_rp
  do ipoin = 1,npoin
     gesca(ipoin) = gesca(ipoin)/(rnorm+zeror)
     rmax = max(rmax,gesca(ipoin))
  end do

end subroutine nsi_outres
