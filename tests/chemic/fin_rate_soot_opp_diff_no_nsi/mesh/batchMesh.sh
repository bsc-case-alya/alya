#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



name=mesh2d
gmsh -2 -format msh2 $name.geo
gmsh2alya.pl $name -bcs=boundaries
python getCoordinates.py $name
    MECH=../KaustMech2.xml
 FUEL_YK='C2H4:1'
  OXI_YK='N2:0.767083,O2:0.232917'
  FUEL_T=300.0
   OXI_T=300.0
   PRESS=101325.0
FUEL_BOU='1,1&3' 
 OXI_BOU='2,2&3'
  STRAIN=5.0
  N_SECT=30
python initialCondition.py $name  $MECH  $FUEL_YK  $OXI_YK  $FUEL_T  $OXI_T  $PRESS  $FUEL_BOU  $OXI_BOU  $STRAIN  $N_SECT 

sed -i -e '1d; $d' $name.fix.bou 

