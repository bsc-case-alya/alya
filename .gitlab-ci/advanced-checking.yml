stages:
  - lock
  - minimal_testsuite
  - fast_testsuite
  - full_testsuite
  - cleanup
  - codecoverage_mr
  - codecoverage_master

.draft: &draft
    - if echo $CI_MERGE_REQUEST_TITLE | grep "^WIP:"; then echo "Remove the WIP status to run the full pipeline!"; exit 1; fi
    - if echo $CI_MERGE_REQUEST_TITLE | grep "^Draft:"; then echo "Remove the Draft status to run the full pipeline!"; exit 1; fi

.behind_master: &behind_master
    - git fetch
    - BEHIND_MASTER=`git rev-list --left-right --count origin/master...HEAD | cut -f1`
    - if [ $BEHIND_MASTER -gt 0 ]; then echo "Branch is behind master!"; exit 1; fi

.changelog: &changelog
    - git fetch --unshallow
    - export PYTHONUNBUFFERED=1
    - python3 $CICD_PATH/changelog.py -a $GITLAB_API_TOKEN -i $CI_PROJECT_ID -m $CI_MERGE_REQUEST_IID -s $CI_COMMIT_SHA --size $CHANGELOG_SIZE -u $GITLAB_API_URL

.check_ts: &check_ts
    - export PYTHONUNBUFFERED=1
    - python3 $CICD_PATH/check_ts_jobs.py -a $GITLAB_API_TOKEN -i $CI_PROJECT_ID -u $GITLAB_API_URL -d $CI_PIPELINE_ID -r $ADMIN -us $GITLAB_USER_ID -mt $MAX_TS -mr $MAX_ADMIN_TS -mu $MAX_USER_TS -s $PIPELINE_SCHEDULING

.changes: &changes
    - "**/CMakeLists.txt"
    - cmake/**/*
    - config/**/*
    - src/**/*
    - deps/**/*
    - tests/**/*
    - unitt/**/*

.testsuite_configuration: &testsuite_configuration
  image: bscalya/python:latest

lock:
  stage: lock
  image: bscalya/python:latest
  script:
    - *draft
    - if grep -x $GITLAB_USER_LOGIN $PIPELINE_BLACKLIST; then echo "You are not allowed to unblock the pipeline"; exit 1; fi
    - *behind_master
    - *changelog
    - *check_ts
  rules:
    - if: $CI_MERGE_REQUEST_ID && $GITLAB_USER_ID != $ADMIN && $GITLAB_USER_ID != $BOT || $CI_MERGE_REQUEST_TITLE =~ /^WIP:/ || $CI_MERGE_REQUEST_TITLE =~ /^Draft/
      changes: *changes
      when: manual
    - if: $CI_MERGE_REQUEST_ID && $GITLAB_USER_ID == $ADMIN || $CI_MERGE_REQUEST_ID && $GITLAB_USER_ID == $BOT
      changes: *changes
  allow_failure: false

fast_testsuite:
  <<: *testsuite_configuration
  stage: fast_testsuite
  only:
    refs:
      - merge_requests
    changes: *changes
  script:
    - *draft
    - if grep -x $GITLAB_USER_LOGIN $PIPELINE_BLACKLIST; then echo "You are not allowed to run the testsuite"; exit 1; fi
    - *behind_master
    - *changelog
    - *check_ts
    - export TYPE="fast"
    - export PYTHONUNBUFFERED=1
    - python3 $CICD_PATH/only_md.py || python3 $CICD_PATH/trigger_ts_pipeline.py -u $GITLAB_API_URL -a $GITLAB_API_TOKEN -p $ALYA_CICD_TOKEN -i $ALYA_CICD_PROJECT_ID -t $ALYA_CICD_REF -e ALYA_REF=$CI_COMMIT_SHORT_SHA -e ALYA_BRANCH=$CI_COMMIT_REF_NAME -e TYPE=$TYPE -e SRC_PIPELINE_ID=$CI_PIPELINE_ID -e SRC_MR_ID=$CI_MERGE_REQUEST_IID 

mn_testsuite:
  <<: *testsuite_configuration
  stage: full_testsuite
  only:
    refs:
      - merge_requests
    changes: *changes
  script:
    - *draft
    - if grep -x $GITLAB_USER_LOGIN $PIPELINE_BLACKLIST; then echo "You are not allowed to run the testsuite"; exit 1; fi
    - *behind_master
    - *changelog
    - *check_ts
    - export TYPE="mn"
    - export PYTHONUNBUFFERED=1
    - python3 $CICD_PATH/only_md.py || python3 $CICD_PATH/trigger_ts_pipeline.py -u $GITLAB_API_URL -a $GITLAB_API_TOKEN -p $ALYA_CICD_TOKEN -i $ALYA_CICD_PROJECT_ID -t $ALYA_CICD_REF -e ALYA_REF=$CI_COMMIT_SHORT_SHA -e ALYA_BRANCH=$CI_COMMIT_REF_NAME -e TYPE=$TYPE -e SRC_PIPELINE_ID=$CI_PIPELINE_ID -e SRC_MR_ID=$CI_MERGE_REQUEST_IID -cc
  artifacts:
    when: always
    paths:
      - cc.txt
    expire_in: 1 week
    public: true

cte_testsuite:
  <<: *testsuite_configuration
  stage: full_testsuite
  only:
    refs:
      - merge_requests
    changes: *changes
  script:
    - *draft
    - if grep -x $GITLAB_USER_LOGIN $PIPELINE_BLACKLIST; then echo "You are not allowed to run the testsuite"; exit 1; fi
    - *behind_master
    - *changelog
    - *check_ts
    - export TYPE="cte"
    - export PYTHONUNBUFFERED=1
    - python3 $CICD_PATH/only_md.py || python3 $CICD_PATH/trigger_ts_pipeline.py -u $GITLAB_API_URL -a $GITLAB_API_TOKEN -p $ALYA_CICD_TOKEN -i $ALYA_CICD_PROJECT_ID -t $ALYA_CICD_REF -e ALYA_REF=$CI_COMMIT_SHORT_SHA -e ALYA_BRANCH=$CI_COMMIT_REF_NAME -e TYPE=$TYPE -e SRC_PIPELINE_ID=$CI_PIPELINE_ID -e SRC_MR_ID=$CI_MERGE_REQUEST_IID 

.postts_cleanup_configuration: &postts_cleanup_configuration
  stage: cleanup
  image: bscalya/python:latest
  only:
    refs:
      - merge_requests
    changes: *changes
  when: on_failure
  script:
    - export PYTHONUNBUFFERED=1
    - python3 $CICD_PATH/draft_mr.py -a $GITLAB_API_TOKEN -i $CI_PROJECT_ID -d $CI_PIPELINE_ID -m $CI_MERGE_REQUEST_IID -u $GITLAB_API_URL 
    - python3 $CICD_PATH/kill_current_pipeline.py -a $GITLAB_API_TOKEN -i $CI_PROJECT_ID -d $CI_PIPELINE_ID -m $CI_MERGE_REQUEST_IID -u $GITLAB_API_URL 
    
postts_cleanup:fast:
  needs: ["fast_testsuite"]
  <<: *postts_cleanup_configuration

postts_cleanup:mn:
  needs: ["mn_testsuite"]
  <<: *postts_cleanup_configuration

postts_cleanup:cte:
  needs: ["cte_testsuite"]
  <<: *postts_cleanup_configuration

codecoverage:mr:
  stage: codecoverage_mr
  image: bscalya/python:latest
  needs:
    - mn_testsuite
  dependencies:
    - mn_testsuite
  script:
    - *draft
    - *behind_master
    - *changelog
    - *check_ts
    - export PYTHONUNBUFFERED=1
    - python3 $CICD_PATH/only_md.py || python3 $CICD_PATH/get_codecoverage.py -a $GITLAB_API_TOKEN -i $CI_PROJECT_ID -u $GITLAB_API_URL
    - COV=`cat cc.txt | tr -d '\r'`
    - echo "(${COV}%) covered"
    - COV_MASTER=`cat cc_master.txt | tr -d '\r'`
    - if (( $(echo "$COV_MASTER > $COV" | bc -l) )); then echo python3 $CICD_PATH/automatic_merge_mr.py -a $GITLAB_API_TOKEN -i $CI_PROJECT_ID -m $CI_MERGE_REQUEST_IID -u $GITLAB_API_URL; sleep 180 ;fi
  coverage: '/\(\d+.\d+\%\) covered/'
  only:
    refs:
      - merge_requests
    changes: *changes

codecoverage:master:
  stage: codecoverage_master
  image: bscalya/python:latest
  script:
    - export PYTHONUNBUFFERED=1
    - python3 $CICD_PATH/get_codecoverage.py -a $GITLAB_API_TOKEN -i $CI_PROJECT_ID -s $CI_COMMIT_SHA -u $GITLAB_API_URL --master
    - COV=`cat cc.txt | tr -d '\r'`
    - echo "(${COV}%) covered"
  coverage: '/\(\d+.\d+\%\) covered/'
  only:
    refs:
      - master
  allow_failure: true
