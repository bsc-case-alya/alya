!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!
!> @defgroup Maths_Toolbox
!> Toolbox for mathematical functions and subroutines
!> @{
!> @name    ToolBox for mathematics operations
!> @file    def_maths.f90
!> @author  Guillaume Houzeaux
!> @brief   Variables
!> @details Variables fot mod_maths.f90
!
!-----------------------------------------------------------------------
module def_maths
  
  use def_kintyp_basic,      only : ip,rp,lg,i1p
  use mod_memory_basic,      only : memory_alloca
  use mod_memory_basic,      only : memory_deallo
  use mod_memory_basic,      only : memory_resize
  use mod_memory_basic,      only : memory_size
  use mod_memory_basic,      only : memory_copy
  use mod_optional_argument, only : optional_argument
  use mod_std                                  ! defintion of qp and count
  implicit none
  
  integer(8)              :: memor(2)
#ifndef __PGI
  integer,     parameter  :: qp = 16
#endif
  real(rp),    parameter  :: epsil = epsilon(1.0_rp)
  real(rp),    parameter  :: pi    = 3.141592653589793238462643383279502884197_rp
  
end module def_maths
!> @}

