#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################




echo "RUNNIG:" $(date +'%Y-%m-%d') $(date +'%T')
echo

printf -v nameA "%s_witness1.dat" $1
printf -v nameB "%s_witness1.dat" $2 

echo $nameA 
grep "CURRENT TIME t" $1.log | tail 

echo $nameB 
grep "CURRENT TIME t" $2.log | tail                    

#Plot_wits.py -F $1.tem.wit -W TEMPE
##Plot_multignuplot.py -F $nameA -X  '( ($1>=0.0)?($1):(1/0) )' -Y  '($2)' -Rx ':' -Ry ':'
#
#gnuplot ../TOUSE/plotBNDRYLAYER01.gp 


