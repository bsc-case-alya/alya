!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine prodxy(nbvar,nbnodes,xx,yy,sumxy)
  !------------------------------------------------------------------------
  !****f* solite/prodxy
  ! NAME 
  !    prodxy
  ! DESCRIPTION
  !    This routine computes the sclara product of two vectors:
  !    SUMXY=XX^t.YY
  ! USES
  ! USED BY 
  !***
  !------------------------------------------------------------------------
  use def_kintyp,         only : ip,rp
  use def_master,         only : kfl_paral,npoi1,npoi2,npoi3
  use mod_communications, only : PAR_SUM
  implicit none
  integer(ip), intent(in)  :: nbvar,nbnodes
  real(rp),    intent(in)  :: xx(*),yy(*)
  real(rp),    intent(out) :: sumxy
  integer(ip)              :: ii,jj,kk
#ifdef BLAS
  real(rp) :: DDOT
  external DDOT
#endif

  if( kfl_paral == -1 ) then
#ifdef BLAS
     sumxy = DDOT(nbvar*nbnodes,xx,1_ip,yy,1_ip)
#else
     sumxy = dot_product(xx(1:nbvar*nbnodes),yy(1:nbvar*nbnodes))
#endif
     
  else if( kfl_paral >= 1 ) then
#ifdef BLAS
     sumxy = DDOT(nbvar*npoi3,xx,1_ip,yy,1_ip)
#else  
     sumxy = dot_product(xx(1:nbvar*npoi3),yy(1:nbvar*npoi3))
#endif
  end if
  
  if( kfl_paral >= 0 ) call PAR_SUM(sumxy)

end subroutine prodxy
