!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_wrapper_petsc

# include <petsc/finclude/petscksp.h>

    use petscksp
    use def_kintyp_basic
    use iso_c_binding

    implicit none

    !call test_alya2petsc_seq()
    !call test_alya2petsc_par()
    !call test_alya2petsc_alya_laplace_seq()
    call test_alya2petsc_alya_laplace_par()


    contains 


    subroutine test_alya2petsc_seq()
        
        integer(ip)            :: ii
        character(len=120)     :: str
        PetscErrorCode         :: ierr
        Mat                    :: A
        Vec                    :: b
        Vec                    :: x
        Vec                    :: r
        KSP                    :: solver
        PetscInt,    pointer   :: ai(:), aj(:), bi(:)
        PetscScalar, pointer   :: xx(:), yy(:)
        PetscReal              :: xnorm
        PetscScalar, parameter :: mrone = -1.0
        PetscInt,    parameter :: n = 5
        PetscScalar, parameter :: one = 1
        PetscReal,   parameter :: rtol = 1.e-8
    
        ! PETSc initialisation
        call PetscInitialize(PETSC_NULL_CHARACTER, ierr)
        if( ierr > 0 )then
            print *, "Error: Could not initialize PETSc, aborting..."
            stop 1
        endif
    
        ! Create matrix
        call MatCreate(PETSC_COMM_WORLD, A, ierr); CHKERRA(ierr)
        call MatSetType(A, MATMPIAIJ, ierr); CHKERRA(ierr)
        call MatSetSizes(A, n, n, PETSC_DECIDE, PETSC_DECIDE, ierr); CHKERRA(ierr)
        call MatSetup(A, ierr); CHKERRA(ierr)
    
        allocate(ai(11)); ai = [0,1,1,1,2,2,2,3,3,3,4]
        allocate(aj(11)); aj = [0,0,1,2,1,2,3,2,3,4,4]
        allocate(xx(11)); xx = [1.0,-1.0,2.0,-1.0,-1.0,2.0,-1.0,-1.0,2.0,-1.0,1.0]
    
        do ii = 1, 11
            call MatSetValue(A, ai(ii), aj(ii), xx(ii), INSERT_VALUES, ierr); CHKERRA(ierr)
        enddo
    
        deallocate(ai)
        deallocate(aj)
        deallocate(xx)
    
        call MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY, ierr); CHKERRA(ierr)
        call MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY, ierr); CHKERRA(ierr)
        call MatView(A, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)
    
        ! Create vecvtor
        call VecCreate(PETSC_COMM_WORLD, b, ierr); CHKERRA(ierr)
        call VecSetType(b, VECMPI, ierr); CHKERRA(ierr)
        call VecSetSizes(b, n, PETSC_DECIDE, ierr); CHKERRA(ierr)
    
        allocate(bi(5)); bi = [0,1,2,3,4]
        allocate(yy(5)); yy = [0.0,1.0,1.0,1.0,0.0]
    
        do ii = 1, 5
            call VecSetValue(b, bi(ii), yy(ii), INSERT_VALUES, ierr); CHKERRA(ierr)
        enddo
    
        deallocate(bi)
        deallocate(yy)
    
        call VecAssemblyBegin(b, ierr); CHKERRA(ierr)
        call VecAssemblyEnd(b, ierr); CHKERRA(ierr)
        call VecView(b, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)
    
        ! Create solution vector
        call VecDuplicate(b, x, ierr); CHKERRA(ierr)
    
        ! Solve
        call KSPCreate(PETSC_COMM_WORLD, solver, ierr); CHKERRA(ierr)
        call KSPSetOperators(solver, A, A, ierr); CHKERRA(ierr)
        call KSPSetTolerances(solver, rtol, PETSC_DEFAULT_REAL, PETSC_DEFAULT_REAL, PETSC_DEFAULT_INTEGER, ierr); CHKERRA(ierr)
        call KSPSetFromOptions(solver, ierr); CHKERRA(ierr)
        call KSPSetUp(solver, ierr); CHKERRA(ierr)
        call KSPView(solver, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)
        call KSPSolve(solver, b, x, ierr); CHKERRA(ierr)
        call VecView(x, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)

        ! Check norm
        call MatMult(A, x, r, ierr); CHKERRA(ierr)
        call VecAXPY(r, mrone, b, ierr); CHKERRA(ierr)
        call VecNorm(r, NORM_2, xnorm, ierr); CHKERRA(ierr)
        write(str, "('norm2 = ',E17.5,A)") xnorm, C_NEW_LINE
        call PetscPrintf(PETSC_COMM_WORLD, trim(str), ierr); CHKERRA(ierr)

        ! Unitt test stop criteria
        if( xnorm > 1.0e-8_rp )then
            stop 1
        endif
    
        ! Destroy 
        call MatDestroy(A, ierr); CHKERRA(ierr)
        call VecDestroy(b, ierr); CHKERRA(ierr)
        call KSPDestroy(solver, ierr); CHKERRA(ierr)
        call VecDestroy(x, ierr); CHKERRA(ierr)
        call PetscFinalize(ierr)
    
    end subroutine test_alya2petsc_seq


    subroutine test_alya2petsc_par()
        
        integer(ip)            :: ii, rank, ierror
        character(len=120)     :: str
        PetscErrorCode         :: ierr
        Mat                    :: A
        Vec                    :: b
        Vec                    :: x
        Vec                    :: r
        KSP                    :: solver
        PetscInt,    pointer   :: ai(:), aj(:), bi(:)
        PetscScalar, pointer   :: xx(:), yy(:)
        PetscReal              :: xnorm
        PetscScalar, parameter :: mrone = -1.0
        PetscInt,    parameter :: n = 5
        PetscScalar, parameter :: one = 1
        PetscReal,   parameter :: rtol = 1.e-8
        
        
        ! MPI initialisation
        call MPI_Init( ierror )
        call MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierror)

        ! PETSc initialisation using the flobal communicator
        PETSC_COMM_WORLD = MPI_COMM_WORLD
        call PetscInitialize(PETSC_NULL_CHARACTER, ierr)
        if( ierr > 0 )then
            print *, "Error: Could not initialize PETSc, aborting..."
            stop
        endif
    
        ! Create matrix
        call MatCreate(MPI_COMM_WORLD, A, ierr); CHKERRA(ierr)
        call MatSetType(A, MATMPIAIJ, ierr); CHKERRA(ierr)
        if( rank == 0 )then
            call MatSetSizes(A, 2, 2, PETSC_DETERMINE, PETSC_DETERMINE, ierr); CHKERRA(ierr)
        else
            call MatSetSizes(A, 3, 3, PETSC_DETERMINE, PETSC_DETERMINE, ierr); CHKERRA(ierr)
        endif
        call MatSetup(A, ierr); CHKERRA(ierr)
    
        allocate(ai(11)); ai = [0,1,1,1,2,2,2,3,3,3,4]
        allocate(aj(11)); aj = [0,0,1,2,1,2,3,2,3,4,4]
        allocate(xx(11)); xx = [1.0,-1.0,2.0,-1.0,-1.0,2.0,-1.0,-1.0,2.0,-1.0,1.0]
    
        if( rank == 0 )then
            do ii = 1, 4
                call MatSetValue(A, ai(ii), aj(ii), xx(ii), INSERT_VALUES, ierr); CHKERRA(ierr)
            enddo
        elseif( rank == 1 )then
            do ii = 5, 11
                call MatSetValue(A, ai(ii), aj(ii), xx(ii), INSERT_VALUES, ierr); CHKERRA(ierr)
            enddo
        endif
    
        deallocate(ai)
        deallocate(aj)
        deallocate(xx)
    
        call MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY, ierr); CHKERRA(ierr)
        call MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY, ierr); CHKERRA(ierr)
        call MatView(A, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)
    
        ! Create vecvtor
        call VecCreate(MPI_COMM_WORLD, b, ierr); CHKERRA(ierr)
        call VecSetType(b, VECMPI, ierr); CHKERRA(ierr)
        if( rank == 0 )then
            call VecSetSizes(b, 2, PETSC_DETERMINE, ierr); CHKERRA(ierr)
        elseif( rank == 1 )then
            call VecSetSizes(b, 3, PETSC_DETERMINE, ierr); CHKERRA(ierr)
        endif
    
        allocate(bi(5)); bi = [0,1,2,3,4]
        allocate(yy(5)); yy = [0.0,1.0,1.0,1.0,0.0]
    
        if( rank == 0 )then
            do ii = 1, 2
                call VecSetValue(b, bi(ii), yy(ii), INSERT_VALUES, ierr); CHKERRA(ierr)
            enddo
        elseif( rank == 1 )then
            do ii = 3, 5
                call VecSetValue(b, bi(ii), yy(ii), INSERT_VALUES, ierr); CHKERRA(ierr)
            enddo
        endif
    
        deallocate(bi)
        deallocate(yy)
    
        call VecAssemblyBegin(b, ierr); CHKERRA(ierr)
        call VecAssemblyEnd(b, ierr); CHKERRA(ierr)
        call VecView(b, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)
    
        ! Create solution vector
        call VecDuplicate(b, x, ierr); CHKERRA(ierr)
    
        ! Solve
        call KSPCreate(MPI_COMM_WORLD, solver, ierr); CHKERRA(ierr)
        call KSPSetOperators(solver, A, A, ierr); CHKERRA(ierr)
        call KSPSetTolerances(solver, rtol, PETSC_DEFAULT_REAL, PETSC_DEFAULT_REAL, PETSC_DEFAULT_INTEGER, ierr); CHKERRA(ierr)
        call KSPSetFromOptions(solver, ierr); CHKERRA(ierr)
        call KSPSetUp(solver, ierr); CHKERRA(ierr)
        call KSPView(solver, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)
        call KSPSolve(solver, b, x, ierr); CHKERRA(ierr)
        call VecView(x, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)

        ! Check norm
        call MatMult(A, x, r, ierr); CHKERRA(ierr)
        call VecAXPY(r, mrone, b, ierr); CHKERRA(ierr)
        call VecNorm(r, NORM_2, xnorm, ierr); CHKERRA(ierr)
        write(str, "('norm2 = ',E17.5,A)") xnorm, C_NEW_LINE
        call PetscPrintf(PETSC_COMM_WORLD, trim(str), ierr); CHKERRA(ierr)

        ! Unitt test stop criteria
        if( xnorm > 1.0e-8_rp )then
            stop 1
        endif
    
        ! Destroy and finalize PETSc
        call MatDestroy(A, ierr); CHKERRA(ierr)
        call VecDestroy(b, ierr); CHKERRA(ierr)
        call KSPDestroy(solver, ierr); CHKERRA(ierr)
        call VecDestroy(x, ierr); CHKERRA(ierr)
        call PetscFinalize( ierr )

        ! MPI finalisation
        call MPI_Finalize( ierror )
    
    end subroutine test_alya2petsc_par


    subroutine test_alya2petsc_alya_laplace_seq()
        use def_kintyp_basic
        use def_kintyp_mesh_basic
        use def_maths_bin 
        use def_domain
        use def_master
        use def_parame
        use mod_memory
        use def_kermod
        use def_elmtyp
        use mod_memory
        use mod_elmgeo
        
        implicit none

        ! --< General / Alya stuff > ----------
        integer(ip)           :: ii, jj
        integer(ip)           :: ielem, igaus, inode, jnode, idime, iboun, ipoin
        integer(ip)           :: pelty, pmate, pnode, pgaus
        real(rp),  pointer    :: el_X(:,:)
        real(rp),  pointer    :: gp_W                   
        real(rp),  pointer    :: gp_N(:)               
        real(rp),  pointer    :: gp_dNde(:,:)         
        real(rp),  pointer    :: gp_dNdx(:,:)   
        real(rp),  pointer    :: gp_J(:,:)
        real(rp)              :: gp_J_det
        real(rp),  pointer    :: gp_J_inv(:,:)
        real(rp)              :: gp_factor
        real(rp),  pointer    :: el_MAT(:,:)
        real(rp),  pointer    :: el_RHS(:)
        real(rp)              :: adiag, val
        real(rp),  parameter  :: deltaX = 1.0e-8_rp
        character(len=120)    :: str


        ! --< PETSc stuff > ----------
        PetscErrorCode         :: ierr
        Mat                    :: A
        Vec                    :: b
        Vec                    :: x
        Vec                    :: r
        KSP                    :: solver
        PetscInt               :: ai, aj, bj
        PetscScalar            :: xx, yy
        PetscScalar, pointer   :: xx_v(:)
        PetscReal              :: xnorm
        PetscScalar, parameter :: mrone = -1.0
        PetscInt,    parameter :: n = 5
        PetscReal,   parameter :: rtol = 1.e-8
        

        ! ------------------------------------------------- |
        ! Create Alya structures assembing a Laplace eq.
        ! ------------------------------------------------- |

        ! Read geometry from Alya files
        call Initia()  ! Initialization of the run
        call Readom()  ! Domain reading
        call Partit()  ! Domain partitioning
        call Reaker()  ! Read Kermod
        call Domtra()  ! Domain transformation
        call Domain()  ! Domain construction

        ! Allocate memory for the RHS and AMATR
        ! - emulating memunk subroutine
        nzrhs = size(meshe(ndivi) % r_dom(:)) - 1; allocate(rhsid(max(1_ip,nzrhs))); rhsid = 0.0_rp
        nzmat = size(meshe(ndivi) % c_dom(:)) - 1; allocate(amatr(max(1_ip,nzmat))); amatr = 0.0_rp

        ! Assemble MAT and RHS
        do ielem = 1, meshe(ndivi) % nelem
            
            !pelty = meshe(ndivi) % ltype(ielem)
            !pmate = meshe(ndivi) % lmate(ielem)
            pelty = ltype(ielem)
            pmate = lmate(ielem)
            pnode = nnode(pelty)
            pgaus = ngaus(pelty)

            ! Allocate some local variables
            allocate(el_X(ndime,pnode))
            allocate(gp_J(ndime,ndime))
            allocate(gp_J_inv(ndime,ndime))
            allocate(gp_dNdx(ndime,pnode))
            allocate(el_RHS(pnode));         el_RHS = 0.0_rp
            allocate(el_MAT(pnode,pnode));   el_MAT = 0.0_rp

            ! Gather coordinates
            do concurrent ( inode = 1:pnode )
                el_X(1:ndime,inode) = coord(1:ndime,lnods(inode,ielem))
            end do

            ! Get elemental J and RHS
            do igaus = 1, pgaus

                ! Get interpolation
                gp_W => elmar(pelty) % weigp(igaus) 
                gp_N => elmar(pelty) % shape(:,igaus)
                gp_dNde => elmar(pelty) % deriv(:,:,igaus)

                ! Cartesian derivatives (dN/dX)
                gp_J = matmul(gp_dNde,transpose(el_X))
                call invmtx(gp_J,gp_J_inv,gp_J_det,ndime)
                gp_dNdx = matmul(gp_J_inv, gp_dNde)

                ! Numerical integration factor  
                gp_factor = gp_W * gp_J_det

                ! Elemental assembly
                do inode = 1, pnode
                    ! MAT
                    do jnode = 1, pnode
                        do idime = 1, ndime
                            el_MAT(inode,jnode) = el_MAT(inode,jnode) + 1.0_rp * gp_dNdx(idime,jnode) * gp_dNdx(idime,inode) * gp_factor
                        enddo
                    enddo
                    ! RHS
                    el_RHS(inode) = el_RHS(inode) + 1.0_rp * gp_N(inode) * gp_factor
                enddo

            enddo

            ! Impose dirichlet BC
            do inode = 1, pnode
                if( el_X(1,inode) < 0.0 + deltaX .or.  &
                    el_X(1,inode) > 1.0 - deltaX .or.  & 
                    el_X(2,inode) < 0.0 + deltaX .or.  &
                    el_X(2,inode) > 1.0 - deltaX )then
                    ! RHS
                    el_RHS(inode) = 0.0_rp
                    ! MAT
                    adiag = el_MAT(inode,inode)
                    el_MAT(inode,:) = 0.0_rp
                    el_MAT(:,inode) = 0.0_rp
                    el_MAT(inode,inode) = adiag 
                endif
            enddo

            ! Global assembly
            call assemble_ELMAT_to_CSR_1DOF( &
                pnode, pnode, ielem, lnods(:,ielem), el_MAT, &
                meshe(ndivi) % r_dom(:), &
                meshe(ndivi) % c_dom(:), &
                amatr ) 

            call assemble_ELRHS_to_CSR_1DOF( &
                pnode, pnode, ielem, lnods(:,ielem), el_RHS, &
                rhsid )

            ! Deallocate
            deallocate(el_X)
            deallocate(gp_J)
            deallocate(gp_J_inv)
            deallocate(gp_dNdx)
            deallocate(el_RHS)
            deallocate(el_MAT)

        enddo


        ! ------------------------------------------------- |
        ! From Alya structures to PETSc one
        ! ------------------------------------------------- |
        allocate(meshe(ndivi) % coo_rows(nzmat))
        allocate(meshe(ndivi) % coo_cols(nzmat))
        
        call csr_to_coo_1DOF( &
            nzrhs, &
            meshe(ndivi) % r_dom(:), &
            meshe(ndivi) % c_dom(:), &
            meshe(ndivi) % coo_rows(:), &
            meshe(ndivi) % coo_cols(:) &
        )

        ! ------------------------------------------------- |
        ! Solve using PETSc 
        ! ------------------------------------------------- |
        ! Assign Alya communicator to the PETSc' one
        PETSC_COMM_WORLD = MPI_COMM_WORLD

        ! PETSc initialisation using the flobal communicator
        call PetscInitialize(PETSC_NULL_CHARACTER, ierr)
        if( ierr > 0 )then
            print *, "Error: Could not initialize PETSc, aborting..."
            stop
        endif
    
        ! 
        ! Create and fill MATRIX
        !
        call MatCreate(PETSC_COMM_WORLD, A, ierr); CHKERRA(ierr)
        call MatSetType(A, MATMPIAIJ, ierr); CHKERRA(ierr)
        call MatSetSizes(A, nzrhs - 1, nzrhs - 1, PETSC_DETERMINE, PETSC_DETERMINE, ierr); CHKERRA(ierr)
        call MatSetup(A, ierr); CHKERRA(ierr)

        do ii = 1, nzmat
            ai = meshe(ndivi) % coo_rows(ii)
            aj = meshe(ndivi) % coo_cols(ii)
            xx = amatr(ii)
            call MatSetValue( A, ai, aj, xx, INSERT_VALUES, ierr); CHKERRA(ierr)
        enddo

        call MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY, ierr); CHKERRA(ierr)
        call MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY, ierr); CHKERRA(ierr)
        !call MatView(A, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)

        !
        ! Create and fill RHS
        !
        call VecCreate(PETSC_COMM_WORLD, b, ierr); CHKERRA(ierr)
        call VecSetType(b, VECMPI, ierr); CHKERRA(ierr)
        call VecSetSizes(b, nzrhs - 1, PETSC_DECIDE, ierr); CHKERRA(ierr)

        do jj = 1, nzrhs
            bj = jj
            yy = rhsid(jj)
            call VecSetValue(b, bj, yy, INSERT_VALUES, ierr); CHKERRA(ierr)
        enddo
    
        call VecAssemblyBegin(b, ierr); CHKERRA(ierr)
        call VecAssemblyEnd(b, ierr); CHKERRA(ierr)
        !call VecView(b, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)

        !
        ! Initialise SOL
        !
        call VecDuplicate(b, x, ierr); CHKERRA(ierr)

        !
        ! Solve 
        !
        call KSPCreate(PETSC_COMM_WORLD, solver, ierr); CHKERRA(ierr)
        call KSPSetOperators(solver, A, A, ierr); CHKERRA(ierr)
        call KSPSetTolerances(solver, rtol, PETSC_DEFAULT_REAL, PETSC_DEFAULT_REAL, PETSC_DEFAULT_INTEGER, ierr); CHKERRA(ierr)
        call KSPSetFromOptions(solver, ierr); CHKERRA(ierr)
        call KSPSetUp(solver, ierr); CHKERRA(ierr)
        call KSPView(solver, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)
        call KSPSolve(solver, b, x, ierr); CHKERRA(ierr)
        !call VecView(x, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)


        ! ------------------------------------------------- |
        ! Check norm
        ! ------------------------------------------------- |
        call MatMult(A, x, r, ierr); CHKERRA(ierr)
        call VecAXPY(r, mrone, b, ierr); CHKERRA(ierr)
        call VecNorm(r, NORM_2, xnorm, ierr); CHKERRA(ierr)
        write(str, "('norm2 = ',E17.5,A)") xnorm, C_NEW_LINE
        call PetscPrintf(PETSC_COMM_WORLD, trim(str), ierr); CHKERRA(ierr)

        ! Unitt test stop criteria
        if( xnorm > 1.0e-8_rp )then
            stop 1
        endif

        ! ------------------------------------------------- |
        ! Write solution
        ! ------------------------------------------------- |
        call VecGetArrayF90(x,xx_v,ierr)
        do ipoin = 1, npoin
            write(99,*) coord(1,ipoin), coord(2,ipoin), xx_v(ipoin)
        enddo
        call VecRestoreArrayF90(x,xx_v,ierr)


        ! ------------------------------------------------- |
        ! Release memory
        ! ------------------------------------------------- |
        call MatDestroy(A, ierr); CHKERRA(ierr)
        call VecDestroy(b, ierr); CHKERRA(ierr)
        call KSPDestroy(solver, ierr); CHKERRA(ierr)
        call VecDestroy(x, ierr); CHKERRA(ierr)
        call PetscFinalize( ierr )

    end subroutine test_alya2petsc_alya_laplace_seq


    subroutine test_alya2petsc_alya_laplace_par()
        use def_kintyp_basic
        use def_kintyp_mesh_basic
        use def_maths_bin 
        use def_domain
        use def_master
        use def_parame
        use mod_memory
        use def_kermod
        use def_elmtyp
        use mod_memory
        use mod_elmgeo
        use mod_parall,      only : PAR_COMM_MY_CODE_WM
        use mod_renumbering, only : renumbering_lexical_order_type
        
        implicit none

        ! --< General / Alya stuff > ----------
        integer(ip)           :: ierror
        integer(ip)           :: ii, jj
        integer(ip)           :: ielem, igaus, inode, jnode, idime, iboun, ipoin
        integer(ip)           :: pelty, pmate, pnode, pgaus
        real(rp),  pointer    :: el_X(:,:)
        real(rp),  pointer    :: gp_W                   
        real(rp),  pointer    :: gp_N(:)               
        real(rp),  pointer    :: gp_dNde(:,:)         
        real(rp),  pointer    :: gp_dNdx(:,:)   
        real(rp),  pointer    :: gp_J(:,:)
        real(rp)              :: gp_J_det
        real(rp),  pointer    :: gp_J_inv(:,:)
        real(rp)              :: gp_factor
        real(rp),  pointer    :: el_MAT(:,:)
        real(rp),  pointer    :: el_RHS(:)
        real(rp)              :: adiag, val
        real(rp),  parameter  :: deltaX = 1.0e-8_rp
        integer(ip), pointer  :: lninv_lex(:) => null()
        character(len=120)    :: str

        ! --< PETSc stuff > ----------
        PetscErrorCode         :: ierr
        Mat                    :: A
        Vec                    :: b
        Vec                    :: x
        Vec                    :: r
        KSP                    :: solver
        PetscInt               :: ai, aj, bj
        PetscScalar            :: xx, yy
        PetscScalar, pointer   :: xx_v(:)
        PetscReal              :: xnorm
        PetscScalar, parameter :: mrone = -1.0
        PetscInt,    parameter :: n = 5
        PetscReal,   parameter :: rtol = 1.e-8

        ! ------------------------------------------------- |
        ! Create Alya structures assembing a Laplace eq.
        ! ------------------------------------------------- |

        ! Read geometry from Alya files
        call Initia()  ! Initialization of the run
        call Readom()  ! Domain reading
        call Partit()  ! Domain partitioning
        call Reaker()  ! Read Kermod
        call Domtra()  ! Domain transformation
        call Domain()  ! Domain construction

        ! Allocate memory for the RHS and AMATR
        ! - emulating memunk subroutine
        nzrhs = size(meshe(ndivi) % r_dom(:)) - 1_ip
        allocate(rhsid(max(1_ip,nzrhs))); rhsid = 0.0_rp
        allocate(unkno(max(1_ip,nzrhs))); unkno = 0.0_rp
        
        nzmat = size(meshe(ndivi) % c_dom(:))
        allocate(amatr(max(1_ip,nzmat))); amatr = 0.0_rp


        ! Assemble MAT and RHS
        if( INOTMASTER )then

            do ielem = 1, meshe(ndivi) % nelem

                !pelty = meshe(ndivi) % ltype(ielem)
                !pmate = meshe(ndivi) % lmate(ielem)
                pelty = ltype(ielem)
                pmate = lmate(ielem)
                pnode = nnode(pelty)
                pgaus = ngaus(pelty)

                ! Allocate some local variables
                allocate(el_X(ndime,pnode))
                allocate(gp_J(ndime,ndime))
                allocate(gp_J_inv(ndime,ndime))
                allocate(gp_dNdx(ndime,pnode))
                allocate(el_RHS(pnode));         el_RHS = 0.0_rp
                allocate(el_MAT(pnode,pnode));   el_MAT = 0.0_rp

                ! Gather coordinates
                do concurrent ( inode = 1:pnode )
                    el_X(1:ndime,inode) = coord(1:ndime,lnods(inode,ielem))
                end do

                ! Get elemental J and RHS
                do igaus = 1, pgaus

                    ! Get interpolation
                    gp_W => elmar(pelty) % weigp(igaus) 
                    gp_N => elmar(pelty) % shape(:,igaus)
                    gp_dNde => elmar(pelty) % deriv(:,:,igaus)

                    ! Cartesian derivatives (dN/dX)
                    gp_J = matmul(gp_dNde,transpose(el_X))
                    call invmtx(gp_J,gp_J_inv,gp_J_det,ndime)
                    gp_dNdx = matmul(gp_J_inv, gp_dNde)

                    ! Numerical integration factor  
                    gp_factor = gp_W * gp_J_det

                    ! Elemental assembly
                    do inode = 1, pnode
                        ! MAT
                        do jnode = 1, pnode
                            do idime = 1, ndime
                                el_MAT(inode,jnode) = el_MAT(inode,jnode) + 1.0_rp * gp_dNdx(idime,jnode) * gp_dNdx(idime,inode) * gp_factor
                            enddo
                        enddo
                        ! RHS
                        el_RHS(inode) = el_RHS(inode) + 1.0_rp * gp_N(inode) * gp_factor
                    enddo

                enddo

                ! Impose dirichlet BC
                do inode = 1, pnode
                    if( el_X(1,inode) < 0.0 + deltaX .or.  &
                        el_X(1,inode) > 1.0 - deltaX .or.  & 
                        el_X(2,inode) < 0.0 + deltaX .or.  &
                        el_X(2,inode) > 1.0 - deltaX )then
                        ! RHS
                        el_RHS(inode) = 0.0_rp
                        ! MAT
                        adiag = el_MAT(inode,inode)
                        el_MAT(inode,:) = 0.0_rp
                        el_MAT(:,inode) = 0.0_rp
                        el_MAT(inode,inode) = adiag 
                    endif
                enddo

                ! Global assembly
                call assemble_ELMAT_to_CSR_1DOF( &
                    pnode, pnode, ielem, lnods(:,ielem), el_MAT, &
                    meshe(ndivi) % r_dom(:), &
                    meshe(ndivi) % c_dom(:), &
                    amatr ) 

                call assemble_ELRHS_to_CSR_1DOF( &
                    pnode, pnode, ielem, lnods(:,ielem), el_RHS, &
                    rhsid )

                ! Deallocate
                deallocate(el_X)
                deallocate(gp_J)
                deallocate(gp_J_inv)
                deallocate(gp_dNdx)
                deallocate(el_RHS)
                deallocate(el_MAT)

            enddo

        end if


        ! ------------------------------------------------- |
        ! From Alya structures to PETSc one
        ! ------------------------------------------------- |

        allocate(meshe(ndivi) % coo_rows(max(1,nzmat)))
        allocate(meshe(ndivi) % coo_cols(max(1,nzmat)))

        call renumbering_lexical_order_type( &
            meshe(ndivi), lninv_lex, 'WITHOUT HALOS', &
            1_ip &
        )

        if( INOTMASTER )then

            call csr_to_coo_glb_1DOF( &
                nzrhs, &
                lninv_lex, &
                meshe(ndivi) % r_dom(:), &
                meshe(ndivi) % c_dom(:), &
                meshe(ndivi) % coo_rows(:), &
                meshe(ndivi) % coo_cols(:) &
            )

        endif


        ! ------------------------------------------------- |
        ! Solve using PETSc 
        ! ------------------------------------------------- |
        if( INOTMASTER )then

            ! Assign Alya communicator to the PETSc' one
            PETSC_COMM_WORLD = PAR_COMM_MY_CODE_WM

            ! PETSc initialisation using the flobal communicator
            call PetscInitialize(PETSC_NULL_CHARACTER, ierr)
            if( ierr > 0 )then
                print *, "Error: Could not initialize PETSc, aborting..."
                stop
            endif
        
            ! 
            ! Create and fill MATRIX
            !
            call MatCreate(PETSC_COMM_WORLD, A, ierr); CHKERRA(ierr)
            call MatSetType(A, MATMPIAIJ, ierr); CHKERRA(ierr)
            call MatSetSizes(A, npoin_own, npoin_own, PETSC_DETERMINE, PETSC_DETERMINE, ierr); CHKERRA(ierr)
            call MatSetup(A, ierr); CHKERRA(ierr)

            do ii = 1, nzmat
                ai = meshe(ndivi) % coo_rows(ii)
                aj = meshe(ndivi) % coo_cols(ii)
                xx = amatr(ii)
                call MatSetValue( A, ai, aj, xx, ADD_VALUES, ierr); CHKERRA(ierr)
            enddo

            call MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY, ierr); CHKERRA(ierr)
            call MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY, ierr); CHKERRA(ierr)
            !call MatView(A, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)

            !
            ! Create and fill RHS
            !
            call VecCreate(PETSC_COMM_WORLD, b, ierr); CHKERRA(ierr)
            call VecSetType(b, VECMPI, ierr); CHKERRA(ierr)
            call VecSetSizes(b, npoin_own, PETSC_DETERMINE, ierr); CHKERRA(ierr)

            do jj = 1, npoin
                bj = lninv_lex(jj) - 1 
                yy = rhsid(jj)
                call VecSetValue(b, bj, yy, ADD_VALUES, ierr); CHKERRA(ierr)
            enddo
        
            call VecAssemblyBegin(b, ierr); CHKERRA(ierr)
            call VecAssemblyEnd(b, ierr); CHKERRA(ierr)
            !call VecView(b, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)

            !
            ! Initialise SOLUTION and RESIDUAL vectors
            !
            call VecDuplicate(b, x, ierr); CHKERRA(ierr)
            call VecDuplicate(b, r, ierr); CHKERRA(ierr)

            !
            ! Solve 
            !
            call KSPCreate(PETSC_COMM_WORLD, solver, ierr); CHKERRA(ierr)
            call KSPSetOperators(solver, A, A, ierr); CHKERRA(ierr)
            call KSPSetTolerances(solver, rtol, PETSC_DEFAULT_REAL, PETSC_DEFAULT_REAL, PETSC_DEFAULT_INTEGER, ierr); CHKERRA(ierr)
            call KSPSetFromOptions(solver, ierr); CHKERRA(ierr)
            call KSPSetUp(solver, ierr); CHKERRA(ierr)
            call KSPView(solver, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)
            call KSPSolve(solver, b, x, ierr); CHKERRA(ierr)
            !call VecView(x, PETSC_VIEWER_STDOUT_WORLD, ierr); CHKERRA(ierr)

            ! ------------------------------------------------- |
            ! Check norm
            ! ------------------------------------------------- |
            call MatMult(A, x, r, ierr); CHKERRA(ierr)
            call VecAXPY(r, mrone, b, ierr); CHKERRA(ierr)
            call VecNorm(r, NORM_2, xnorm, ierr); CHKERRA(ierr)
            write(str, "('norm2 = ',E17.5,A)") xnorm, C_NEW_LINE
            call PetscPrintf(PETSC_COMM_WORLD, trim(str), ierr); CHKERRA(ierr)

            ! Unitt test stop criteria
            if( xnorm > 1.0e-8_rp )then
                stop 1
            endif

            ! ------------------------------------------------- |
            ! Write solution
            ! ------------------------------------------------- |
            call VecGetArrayF90(x,xx_v,ierr)
            do ipoin = 1, npoin_own
                write(100+kfl_paral,*) coord(1,ipoin), coord(2,ipoin), xx_v(ipoin)
            enddo
            call VecRestoreArrayF90(x,xx_v,ierr)


            ! ------------------------------------------------- |
            ! Release memory
            ! ------------------------------------------------- |
            call MatDestroy(A, ierr); CHKERRA(ierr)
            call VecDestroy(b, ierr); CHKERRA(ierr)
            call KSPDestroy(solver, ierr); CHKERRA(ierr)
            call VecDestroy(x, ierr); CHKERRA(ierr)
            call PetscFinalize( ierr )

        endif

        call MPI_Finalize( ierror )

    end subroutine test_alya2petsc_alya_laplace_par

    !
    ! Auxiliar routines
    !
    subroutine assemble_ELMAT_to_CSR_1DOF(pnode,pevat,ielem,lnods,elmat,ia,ja,an)

        integer(ip), intent(in)                      :: pnode
        integer(ip), intent(in)                      :: pevat
        integer(ip), intent(in)                      :: ielem
        integer(ip), intent(in)                      :: lnods(pnode)
        integer(ip), intent(in)                      :: ia(*)
        integer(ip), intent(in)                      :: ja(*)
        real(rp),    intent(in)                      :: elmat(pevat,pevat)
        real(rp),    intent(inout)                   :: an(*)
        integer(ip)                                  :: inode,jnode
        integer(ip)                                  :: ipoin,jpoin,izsol,jcolu

        do inode = 1,pnode
            ipoin = lnods(inode)
            do jnode = 1,pnode
                jpoin = lnods(jnode)
                izsol = ia(ipoin)
                jcolu = ja(izsol)
                do while( jcolu /= jpoin .and. izsol < ia(ipoin+1)-1)
                    izsol = izsol + 1
                    jcolu = ja(izsol)
                end do
                if( jcolu == jpoin ) then
                    an(izsol) = an(izsol) + elmat(inode,jnode)
                end if
            end do
        end do
 
    end subroutine assemble_ELMAT_to_CSR_1DOF


    subroutine assemble_ELMAT_to_CSR_NDOF(pnode,pdofn,ielem,lnods,elmat,ia,ja,an)

        integer(ip), intent(in)                      :: pnode
        integer(ip), intent(in)                      :: pdofn
        integer(ip), intent(in)                      :: ielem
        integer(ip), intent(in)                      :: lnods(pnode)
        integer(ip), intent(in)                      :: ia(*)
        integer(ip), intent(in)                      :: ja(*)
        real(rp),    intent(in)                      :: elmat(pnode*pdofn,pnode*pdofn)
        real(rp),    intent(inout)                   :: an(pdofn,pdofn,*)
        integer(ip)                                  :: inode,jnode
        integer(ip)                                  :: ipoin,jpoin,izsol,jcolu
        integer(ip)                                  :: idofn,jdofn,iposi,jposi


        do inode = 1, pnode
            ipoin = lnods(inode)
            do jnode = 1,pnode
                jpoin = lnods(jnode)
                izsol = ia(ipoin)
                jcolu = ja(izsol)
                do while( jcolu /= jpoin .and. izsol < ia(ipoin + 1_ip) - 1_ip )
                    izsol = izsol + 1_ip
                    jcolu = ja(izsol)
                end do
                if( jcolu == jpoin ) then
                    do idofn = 1, pdofn
                        iposi = (inode - 1_ip) * pdofn + idofn
                        do jdofn = 1,pdofn
                            jposi = (jnode - 1_ip) * pdofn + jdofn
                            an(jdofn,idofn,izsol) = an(jdofn,idofn,izsol) + elmat(iposi,jposi)
                        end do
                    end do
                end if
            end do
        end do
    
    end subroutine assemble_ELMAT_to_CSR_NDOF

    subroutine assemble_ELRHS_to_CSR_1DOF(pnode,pevat,ielem,lnods,elrhs,bn)

        integer(ip), intent(in)                      :: pnode
        integer(ip), intent(in)                      :: pevat
        integer(ip), intent(in)                      :: ielem
        integer(ip), intent(in)                      :: lnods(pnode)
        real(rp),    intent(in)                      :: elrhs(pevat)
        real(rp),    intent(inout)                   :: bn(*)
        integer(ip)                                  :: inode,ipoin

        do inode = 1,pnode
            ipoin = lnods(inode)
            bn(ipoin) = bn(ipoin) + elrhs(inode)
        end do
 
    end subroutine assemble_ELRHS_to_CSR_1DOF


    subroutine assemble_ELRHS_to_CSR_NDOF(pnode,pdofn,ielem,lnods,elrhs,bn)

        integer(ip), intent(in)                      :: pnode
        integer(ip), intent(in)                      :: pdofn
        integer(ip), intent(in)                      :: ielem
        integer(ip), intent(in)                      :: lnods(pnode)
        real(rp),    intent(in)                      :: elrhs(pnode*pdofn)
        real(rp),    intent(inout)                   :: bn(pdofn,*)
        integer(ip)                                  :: inode, ipoin, idofn, iposi

        do inode = 1, pnode
            ipoin = lnods(inode)
            do idofn = 1, pdofn
                iposi = (inode - 1_ip) * pdofn + idofn
                bn(idofn,ipoin) = bn(idofn,ipoin) + elrhs(iposi)
            enddo
        end do
 
    end subroutine assemble_ELRHS_to_CSR_NDOF


    subroutine csr_to_coo_1DOF(nn,ia,ja,row,col)
        
        implicit none

        integer(ip),  intent(in)     :: nn
        integer(ip),  intent(in)     :: ia(nn+1)
        integer(ip),  intent(in)     :: ja(*)
        integer(ip),  intent(inout)  :: row(:)
        integer(ip),  intent(inout)  :: col(:)
        integer(ip)                  :: ii, iz, jj

        do ii = 1,nn
            do iz = ia(ii),ia(ii+1)-1
                jj       = ja(iz)
                row(iz)  = ii - 1_ip
                col(iz)  = jj - 1_ip
            end do
        end do

    end subroutine csr_to_coo_1DOF


    subroutine csr_to_coo_glb_1DOF(nn,ninv,ia,ja,row,col)
        
        implicit none

        integer(ip),  intent(in)     :: nn
        integer(ip),  intent(in)     :: ninv(:)
        integer(ip),  intent(in)     :: ia(nn+1)
        integer(ip),  intent(in)     :: ja(*)
        integer(ip),  intent(inout)  :: row(:)
        integer(ip),  intent(inout)  :: col(:)
        integer(ip)                  :: ii, iz, jj

        do ii = 1,nn
            do iz = ia(ii),ia(ii+1)-1
                jj       = ja(iz)
                row(iz)  = ninv(ii) - 1_ip
                col(iz)  = ninv(jj) - 1_ip
            end do
        end do

    end subroutine csr_to_coo_glb_1DOF


end program unitt_wrapper_petsc
