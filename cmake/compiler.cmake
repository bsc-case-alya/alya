#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



function(print_message_once name message)
  # Name of the custom function to check
  set(fname _check_first_dummy_${name})
  if (NOT COMMAND ${fname})
    message(STATUS "${message}")
    # Define a function so next time it will exist
    function(${fname})
    endfunction()
  endif()
endfunction()

function(set_flags_fortran)
  #Fortran Management

  if (DEFINED CUSTOM_Fortran_FLAGS)
    set(APPEND_Fortran_FLAGS "${CUSTOM_Fortran_FLAGS}")
  endif()
  
  #Standard 2008
  if (WITH_STD2008)
    set(APPEND_Fortran_FLAGS "${APPEND_Fortran_FLAGS} ${CUSTOM_Fortran_FLAGS_STD2008}")
  endif()
 
  #Code coverage
  if (WITH_CODE_COVERAGE)
    set(APPEND_Fortran_FLAGS "${APPEND_Fortran_FLAGS} ${CUSTOM_Fortran_FLAGS_CODE_COVERAGE}")
  endif()

  if (DEFINED APPEND_Fortran_FLAGS)
    #Fortran compilation flag management
    string(REPLACE " " ";" REPLACED_Fortran_FLAGS ${APPEND_Fortran_FLAGS})
    target_compile_options(${PROJECT_NAME} PUBLIC $<$<COMPILE_LANGUAGE:Fortran>:${REPLACED_Fortran_FLAGS}>)
  endif()

  #Release flag management
  if (DEFINED CUSTOM_Fortran_FLAGS_RELEASE)
    set(APPEND_Fortran_FLAGS_RELEASE "${CUSTOM_Fortran_FLAGS_RELEASE}")
  endif()
  
  #Optimizations
  if (DEFINED CUSTOM_Fortran_FLAGS_O${OPTIMIZATION_LEVEL})
    set(APPEND_Fortran_FLAGS_RELEASE "${APPEND_Fortran_FLAGS_RELEASE} ${CUSTOM_Fortran_FLAGS_O${OPTIMIZATION_LEVEL}}")
  endif()

  #Architecture
  if (DEFINED CUSTOM_Fortran_FLAGS_ARCHITECTURE)
    include(CheckFortranCompilerFlag)
    check_fortran_compiler_flag(${CUSTOM_Fortran_FLAGS_ARCHITECTURE} _archi)
    if (NOT _archi)
      print_message_once(architecture_flag "Following architecture flags are incompatible with the current fortran compiler and will be disabled: ${CUSTOM_Fortran_FLAGS_ARCHITECTURE}")
    else()
      set(APPEND_Fortran_FLAGS_RELEASE "${APPEND_Fortran_FLAGS_RELEASE} ${CUSTOM_Fortran_FLAGS_ARCHITECTURE}")
    endif()
  endif()
  
  if (WITH_IPO)
    #Architecture
    if (DEFINED CUSTOM_Fortran_FLAGS_IPO)
      set(APPEND_Fortran_FLAGS_RELEASE "${CMAKE_Fortran_FLAGS_RELEASE} ${CUSTOM_Fortran_FLAGS_IPO}")
    endif()
  endif()

  if (DEFINED APPEND_Fortran_FLAGS_RELEASE)
    #Fortran compilation flag management
    string(REPLACE " " ";" REPLACED_Fortran_FLAGS_RELEASE ${APPEND_Fortran_FLAGS_RELEASE})
    target_compile_options(${PROJECT_NAME} PUBLIC $<$<COMPILE_LANGUAGE:Fortran>:$<$<CONFIG:Release>:${REPLACED_Fortran_FLAGS_RELEASE}>>)
    unset(CMAKE_Fortran_FLAGS_RELEASE CACHE)
  endif()
  
  #Debug flag management
  if (DEFINED CUSTOM_Fortran_FLAGS_DEBUG)
    set(APPEND_Fortran_FLAGS_DEBUG "${CUSTOM_Fortran_FLAGS_DEBUG}")
  endif()
  
  if (DEFINED APPEND_Fortran_FLAGS_DEBUG)
    if (NOT "${APPEND_Fortran_FLAGS_DEBUG}" STREQUAL "")
      #Fortran compilation flag management
      string(REPLACE " " ";" REPLACED_Fortran_FLAGS_DEBUG ${APPEND_Fortran_FLAGS_DEBUG})
      target_compile_options(${PROJECT_NAME} PUBLIC $<$<COMPILE_LANGUAGE:Fortran>:$<$<CONFIG:Debug>:${REPLACED_Fortran_FLAGS_DEBUG}>>)
      unset(CMAKE_Fortran_FLAGS_DEBUG CACHE)
    endif()
  endif()

endfunction()

function(set_flags_c)

  #C Management
  
  if (DEFINED CUSTOM_C_FLAGS)
    set(APPEND_C_FLAGS "${CUSTOM_C_FLAGS}")
    string(REPLACE " " ";" REPLACED_C_FLAGS ${APPEND_C_FLAGS})
    target_compile_options(${PROJECT_NAME} PUBLIC $<$<COMPILE_LANGUAGE:C>:${REPLACED_C_FLAGS}>)
  endif()

endfunction()

function(set_flags_cxx)

  #C++ Management
  if (DEFINED CUSTOM_CXX_FLAGS)
    set(APPEND_CXX_FLAGS "${CUSTOM_CXX_FLAGS}")
    string(REPLACE " " ";" REPLACED_CXX_FLAGS ${APPEND_CXX_FLAGS})
    target_compile_options(${PROJECT_NAME} PUBLIC $<$<COMPILE_LANGUAGE:CXX>:${REPLACED_CXX_FLAGS}>)
  endif()

endfunction()

function(set_flags)
  set_flags_fortran()
  set_flags_c()
  set_flags_cxx()
endfunction()

function(set_defs)
  #CMake flag
  add_definitions(-DCMAKE)
  
  #MPI flag
  if (NOT WITH_MPI)
    add_definitions(-DMPI_OFF)
  endif()

  #MPI fortran bindings
  if (WITH_MPI)
    if (USEMPIF08)
      add_definitions(-DUSEMPIF08)
    else()
      add_definitions(-DMPIFH)
    endif()    
  endif()
  
  #NDIMEPAR flag
  if (WITH_NDIMEPAR)
    add_definitions(-DNDIMEPAR)
  endif()

  #GPU flag
  if (WITH_GPU)
    add_definitions(-DOPENACCHHH -DSUPER_FAST -DDETAILED_TIMES)
  endif()
  
  #REAL16 flag
  if (WITH_REAL16)
    add_definitions(-DR16_COMPATIBLE)
  endif()

  #pnode value flag
  if (NOT PNODE_VALUE STREQUAL "0")
    add_definitions(-DPNODE_VALUE=${PNODE_VALUE})
  endif()

  #pgaus value flag
  if (NOT PGAUS_VALUE STREQUAL "0")
    add_definitions(-DPGAUS_VALUE=${PGAUS_VALUE})
  endif()
  
  #NO PROPER flags
  if (NOT WITH_PROPER_PRIVATE)
    add_definitions(-DPROPER_PRIVATE_OFF)
  endif()

  if (NOT WITH_PROPER_ELEM_PRIVATE)
    add_definitions(-DPROPER_ELEM_PRIVATE_OFF)
  endif()

  #Vector size flag
  if (NOT VECTOR_SIZE STREQUAL "0")
    add_definitions(-DVECTOR_SIZE=${VECTOR_SIZE})
  endif()

  #Vector size CPU flag
  if (NOT VECTOR_SIZE_CPU STREQUAL "0")
    add_definitions(-DVECTOR_SIZE_CPU=${VECTOR_SIZE_CPU})
  endif()

  #Integer management
  if (INTEGER_SIZE MATCHES "4")
    set(CMAKE_Fortran_FLAGS "${CUSTOM_Fortran_FLAGS_I4}" PARENT_SCOPE)
  elseif (INTEGER_SIZE MATCHES "8")
    add_definitions(-DI8)
    set(CMAKE_Fortran_FLAGS "${CUSTOM_Fortran_FLAGS_I8}" PARENT_SCOPE)
  endif()
endfunction()

function(set_parall)
 include(mpi)
 mpi_fortran()
 mpi_c()
 mpi_cxx()
 include(openmp)
 target_compile_flags_openmp()
 target_link_flags_openmp()
 include(openacc)
 target_compile_flags_openacc()
 target_link_flags_openacc()
endfunction()
