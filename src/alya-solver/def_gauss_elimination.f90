!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!




!-----------------------------------------------------------------------
!> @addtogroup Solvers
!> @{
!> @file    def_krylov_solvers.f90
!> @author  houzeaux
!> @date    2022-03-01
!> @brief   Krylov solvers
!> @details Krylov solvers
!>         
!-----------------------------------------------------------------------

module def_gauss_elimination

  use def_kintyp,         only : ip,rp
  use def_mat,            only : mat
  use def_mat_dia,        only : mat_dia
  use def_mat_den,        only : mat_den
  use def_direct_solvers, only : direct_solver
  implicit none
  private
  
  type, extends(direct_solver) :: gauss_elimination
   contains
     procedure, pass :: init     
     procedure, pass :: alloca   
     procedure, pass :: deallo   
     procedure, pass :: solve    
     procedure, pass :: setup    
  end type gauss_elimination

  public :: gauss_elimination
  
contains

  !-----------------------------------------------------------------------
  !> 
  !> @author  houzeaux
  !> @date    2022-03-02
  !> @brief   Diag solver
  !> @details Diag solver Ax=b
  !> 
  !-----------------------------------------------------------------------

  subroutine init(self)
    
    class(gauss_elimination), intent(inout) :: self
    
  end subroutine init
  
  subroutine deallo(self)
    
    class(gauss_elimination), intent(inout) :: self
    
  end subroutine deallo
  
  subroutine alloca(self)
    
    class(gauss_elimination), intent(inout) :: self
        
  end subroutine alloca
  
  subroutine setup(self,a)

    class(gauss_elimination),           intent(inout) :: self          !< Solver
    class(mat),                         intent(inout) :: a             !< Matrix A

    self % n    = a % nrows
    self % ndof = a % ndof1
    self % nn   = self % n * self % ndof

  end subroutine setup
  
  subroutine solve(self,b,x,a)

    class(gauss_elimination), intent(inout) :: self          !< Solver
    real(rp),        pointer, intent(in)    :: b(:)          !< RHS b
    real(rp),        pointer, intent(inout) :: x(:)          !< Solve x
    class(mat),               intent(in)    :: a             !< Matrix A
    integer(ip)                             :: i,j,id

    if( associated(b) .and. associated(x) ) then
       
       select type ( a )
          
       class is ( mat_dia ) ; call a % solve(b,x)
       class is ( mat_den ) ; call a % solve(b,x)
          
       end select

    end if
    
  end subroutine solve

end module def_gauss_elimination
!> @}
