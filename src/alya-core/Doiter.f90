!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine Doiter(iblok)
!-----------------------------------------------------------------------
!****f* master/Doiter
! NAME
!    Doiter
! DESCRIPTION
!    This routine calls the different problems to be solved within
!    one iteration      
! USES
!    Nastin
!    Temper
!    Codire
!    Alefor
! USED BY
!    Alya
!***
!-----------------------------------------------------------------------
  use def_kintyp,        only : ip
  use def_master,        only : itinn
  use def_master,        only : ittim
  use def_master,        only : ITASK_DOITER
  use mod_ker_detection, only : ker_detection_doiter
  use def_kermod,        only : kfl_detection
  use mod_messages,      only : livinf
  use mod_moduls,        only : moduls 
  use mod_alya2talp,     only : alya2talp_MonitoringRegionStart
  use mod_alya2talp,     only : alya2talp_MonitoringRegionStop
  use mod_reset,         only : reset_check
  implicit none
  integer(ip), intent(in) :: iblok
  
  call livinf(5_ip,' ',0_ip)
  call livinf(6_ip,' ',0_ip)

  itinn(0) = ittim

  call moduls(ITASK_DOITER)  
  call Kermod(ITASK_DOITER)
  !
  ! Detect non-converged modules
  !
  !if( kfl_detection /= 0 ) call ker_detection_doiter()
  !
  ! Check reset
  !
  call reset_check()

end subroutine Doiter
