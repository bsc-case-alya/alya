#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



message(STATUS "Compiling with GNU")
set(CUSTOM_Fortran_FLAGS_RELEASE "")
set(CUSTOM_Fortran_FLAGS_DEBUG "-fbounds-check -fbacktrace -ftrapv -Wconversion-extra -g")
set(CUSTOM_Fortran_FLAGS_I4 "")
set(CUSTOM_Fortran_FLAGS_I8 "-m64")
set(CUSTOM_Fortran_FLAGS_ARCHITECTURE "-march=native")
set(CUSTOM_Fortran_FLAGS_O0 "-O0")
set(CUSTOM_Fortran_FLAGS_O1 "-O1")
set(CUSTOM_Fortran_FLAGS_O2 "-O2")
set(CUSTOM_Fortran_FLAGS_O3 "-O3")
set(CUSTOM_Fortran_FLAGS_STD2008 "-std=f2008")
if (CMAKE_C_COMPILER_VERSION VERSION_LESS 10.0.0)
  set(CUSTOM_Fortran_FLAGS "-Wall -x f95-cpp-input -ffree-line-length-none -fimplicit-none")
else()
  set(CUSTOM_Fortran_FLAGS "-Wall -x f95-cpp-input -ffree-line-length-none -fimplicit-none -fallow-argument-mismatch")
endif()
