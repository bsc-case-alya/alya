#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import sys
import os
import math
import numpy as np
from scipy.special import erf
import cantera as ct

def strainFlow(xglob,yglob,a,x0=0.0,y0=0.0,width=np.inf):
	x = xglob - x0
	y = yglob - y0
	if abs(x) < width/2.0:
		U =  0.0
		V = -1.0 * a * y
	else:
		U = 0.0
		V = 0.0

	return(U,V)

print(sys.argv)

meshName	= sys.argv[1]
mech		= sys.argv[2]
fuel		= sys.argv[3]
oxidizer	= sys.argv[4]
Tf			= float(sys.argv[5])
Tox			= float(sys.argv[6])
P			= float(sys.argv[7])

fuelBoundaries = sys.argv[8].split(',')
oxiBoundaries = sys.argv[9].split(',')

strain	  =  float(sys.argv[10])
Nsec		=  int(sys.argv[11])

#
# Shift center of strain flow and Z profile
#
if len(sys.argv)>12:
	x0 = float(sys.argv[12])
else:
	x0 = 0.0 

if len(sys.argv)>13:
	y0 = float(sys.argv[13])
else:
	y0 = 0.0

coordfile	   = '{}.coord'.format(meshName)

velofile		= 'VELOC.alya'
enthfile		= 'ENTHA.alya'
zfile			= 'Z.alya'
ycfile			= 'YC.alya'



gasFuel						  = ct.Solution(mech)
gasFuel.TPY					  = (Tf,P,fuel)

gasOxidizer					  = ct.Solution(mech)
gasOxidizer.TPY				  = (Tox,P,oxidizer)
diff						  = gasOxidizer.thermal_conductivity/(gasOxidizer.density_mass * gasOxidizer.cp_mass)
lengthScale					  = ( 2.0 * diff / strain )**0.5
print('Length scale: {}'.format(lengthScale))

print(gasFuel.report())
print(gasOxidizer.report())


ymin =  np.inf
ymax = -np.inf
fCoord		   =open(coordfile,'r')
for line in fCoord:
	data=line.split()

	pid = int(data[0])
	dims = len(data)-1
	x   = float(data[1])
	y   = float(data[2])
	ymax = max(ymax,y)
	ymin = min(ymin,y)
fCoord.close()

width = 1.5 * (ymax-ymin)
print('Total thickness of domain: {}'.format(ymax-ymin))
U,V = strainFlow(0.0,ymax,strain)
print('Downward velocity: {}'.format(V))
U,V = strainFlow(0.0,ymin,strain)
print('Upward velocity:   {}'.format(V))


fCoord		   =open(coordfile,'r')
fVelo		   =open(velofile,'w')
fEnth		   =open(enthfile,'w')
fZ   		   =open(zfile,'w')
fYc  		   =open(ycfile,'w')

print('---| Start writing initial condition')

for line in fCoord:
	data=line.split()

	pid = int(data[0])
	dims = len(data)-1
	x   = float(data[1]) - x0
	y   = float(data[2]) - y0


	U,V = strainFlow(x,y,strain,width=width)

	zeta = y / lengthScale
	Z = 0.5 * (1.0 - erf( zeta ))
	H = gasOxidizer.enthalpy_mass + (gasFuel.enthalpy_mass-gasOxidizer.enthalpy_mass)*Z
	if abs(x) < width/2.0:
		Yc = 1.0
	else:
		Yc = 0.0

	#
	# Write IC  
	#
	fVelo.write('{} {} {}\n'.format(pid,U,V))
	fEnth.write('{} {}\n'.format(pid,H))
	fZ.write('{} {}\n'.format(pid,Z))
	fYc.write('{} {}\n'.format(pid,Yc))
		

fCoord.close()
fVelo.close()
fEnth.close()
fZ.close()
fYc.close()
print('---| End writing initial condition')

#
# Write soot boundary conditions
#
shift = 3
with open('sootBoundary.dat','w') as f:
	for ii in range(Nsec):
		f.write('  CODES, NODES, CLASS = {}\n'.format(ii+shift))
		for bc in fuelBoundaries:
			f.write('{:>10}  1  {}\n'.format(bc, 0.0))
		for bc in oxiBoundaries:
			f.write('{:>10}  1  {}\n'.format(bc, 0.0))
		f.write('  END_CODES\n\n')



