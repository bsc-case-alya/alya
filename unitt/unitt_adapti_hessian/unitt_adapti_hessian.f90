!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_adapti_hessian ! adapting mesh to analytical mesh size (sequential, only via mod_adapt no mod_amr)
  !
  ! Test boundary mesh
  !
  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_master
  use mod_memory
  use mod_memory,             only : memory_alloca, memory_deallo  
  use mod_quality,            only : compute_mesh_quality_sizeShape
  use mod_quality,            only : print_q_stats
  use mod_adapt,              only : adapt_mesh_to_metric
  use mod_metric,             only : mesh_metric_type
  use mod_meshEdges,          only : compute_mesh_edgeLengths
  use mod_meshTopology,       only : node_to_elems_type
  use mod_out_paraview,       only : out_paraview_inp
  use def_adapt,              only : memor_adapt 
  use mod_debugTools,         only : out_debug_text, out_debug_paraview
  use mod_metricComputation,  only : compute_metric_from_sol_viaHessian, compute_metric_from_hessian

  implicit none

  real(rp),             parameter  :: epsil = 1.0e-12_rp
  type(mesh_type_basic)            :: mesh
  integer(ip)                      :: ipoin,idime,ielem,inode,iboun,idim,ix,iy,quad(4),hex(8),iz
  integer(ip)                      :: pelty,ii
  integer(8)                       :: count_rate8
  integer(8)                       :: memor_loc(2)
  integer(8)                       :: memor_tmp(2)
  integer(ip)                      :: nx,ny,ratioXY,nz, itet
  integer(ip)                      :: select_metric 
  real(rp)                         :: hx,hy,x0,x1,y0,y1,r,PI,z0,z1,hz
  real(rp)                         :: dista,xx_test(2)
  real(rp)                         :: hmax,eps,a
  type(mesh_metric_type)           :: metric
  logical(lg)                      :: isSizeField
  logical(lg)                      :: isIsotropic
  type(node_to_elems_type)         :: node_to_elems    
  integer(ip), pointer             :: mapNodes_old_to_new(:)
  integer(ip), pointer             :: edge_to_node(:,:)
  real(rp),    pointer             :: q(:)
  real(rp),    pointer             :: edge_lengths(:)
  real(rp),    pointer             :: u(:),hessu(:,:,:), aux(:),x(:),y(:)

  nullify(q,edge_lengths,edge_to_node,mapNodes_old_to_new,u,hessu,aux,x,y)

  memor_loc = 0_8
  memor_tmp = 0_8
  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)
  !
  ! Generate mesh
  !
  call elmgeo_element_type_initialization()
  !
  call mesh     % init('MY_MESH')

  ratioXY = 1_ip  !2_ip
  nx = 10_ip
  ny = nx
  if(.true.) then !2D
    nx = nx*ratioXY
    
    mesh % nelem = nx*ny*2_ip
    mesh % npoin = (nx+1_ip)*(ny+1_ip)
    mesh % ndime = 2_ip
    mesh % mnode = 3_ip
    call mesh % alloca()
    mesh % ltype(:)  = TRI03
    
    x0 =  0.0_rp*ratioXY
    x1 =  1.0_rp*ratioXY
    y0 =  0.0_rp
    y1 =  1.0_rp
    hx = (x1-x0)/nx
    hy = (y1-y0)/ny
    do iy=0,ny
      do ix=0,nx
        inode = (1+ix) + iy*(1+nx)
        mesh%coord(:, inode ) = (/ x0 + hx*ix, y0 + hy*iy/)
        if((ix>0).and.(iy>0)) then
          quad(1) = (1+ix-1) + (iy-1)*(1+nx) 
          quad(2) = (1+ix  ) + (iy-1)*(1+nx) 
          quad(3) = (1+ix  ) + (iy-0)*(1+nx) 
          quad(4) = (1+ix-1) + (iy-0)*(1+nx) 
          mesh%lnods(:, (2*ix - 1) + (iy-1)*(2*nx) ) = quad(1:3)
          mesh%lnods(:, (2*ix    ) + (iy-1)*(2*nx) ) = quad((/1,3,4/))
        end if
      end do
    end do
  end if
  
  ! tanh 30(u +v −ε)
  !ε=0.25 and u=4x−2, v=4y−2
  !print*,'ESTIC AQUIIIIIII'
  !call runend('eeee')
  print*,"u"
  nullify(u)
  call memory_alloca(memor_adapt,'u','unitt_adapti_hessian',u,mesh%npoin)
  eps = 0.25_rp
  !u(:) = tanh( 30_rp*( (4.0_rp*mesh%coord(1,:)-2.0_rp)**2 + (4.0_rp*mesh%coord(2,:)-2.0_rp)**2 - eps) ) 
  u(:) = tanh( 30_rp*( (4.0_rp*mesh%coord(1,:)-2.0_rp)**2 + (4.0_rp*mesh%coord(2,:)-2.0_rp)**2 - eps) ) 
  
  nullify(hessu)
  call memory_alloca(memor_adapt,'hessu','unitt_adapti_hessian',hessu,mesh%ndime,mesh%ndime,mesh%npoin)
  nullify(aux)
  call memory_alloca(memor_adapt,'aux','unitt_adapti_hessian',aux,mesh%npoin)
  nullify(x)
  !call memory_alloca(memor_adapt,'x','unitt_adapti_hessian',x,mesh%npoin)
  nullify(y)
  !call memory_alloca(memor_adapt,'y','unitt_adapti_hessian',y,mesh%npoin)
  x => mesh%coord(1,:)
  y => mesh%coord(2,:)
  aux(:) = 30*((2-4*x)**2 + (2-4*y)**2 + eps)
  hessu(1,1,:) = 960*(1 - 480*(1 - 2*x)**2 *tanh(aux))*(1/cosh(aux))**2
  hessu(2,2,:) = 960*(1 - 480*(1 - 2*y)**2 *tanh(aux))*(1/cosh(aux))**2
  hessu(1,2,:) = (-460800)*(2*x- 1)*(2*y-1)*tanh(aux) *(1/cosh(aux))**2
  hessu(2,1,:) = hessu(1,2,:)
  !xx: 960 (1 - 480 (1 - 2 x)^2 tanh(30 ((2 - 4 x)^2 + (2 - 4 y)^2 + ϵ))) sech^2(30 ((2 - 4 x)^2 + (2 - 4 y)^2 + ϵ))
  !yy: 960 (1 - 480 (1 - 2 y)^2 tanh(30 ((2 - 4 x)^2 + (2 - 4 y)^2 + ϵ))) sech^2(30 ((2 - 4 x)^2 + (2 - 4 y)^2 + ϵ))
  !xy: -460800(2x- 1) (2 y - 1) tanh(30 ((2 - 4 x)^2 + (2 - 4 y)^2 + ϵ))  sech^2(30 ((2 - 4 x)^2 + (2 - 4 y)^2 + ϵ))
  
  a = 100
  u(:) = tanh(a*(y-x))
  !d/dx((dtanh(a (y - x)))/(dx)) = -2 a^2 tanh(a (y - x)) sech^2(a (y - x))
  !d/dy((dtanh(a (y - x)))/(dy)) = -2 a^2 tanh(a (y - x)) sech^2(a (y - x))
  !(2 a^2 sinh(a (-x + y)))/(cosh^3(a (-x + y)))
  hessu(1,1,:) = -2*(a**2)*tanh(a*(y - x))/(cosh(a*(y - x)))**2
  hessu(2,2,:) = -2*(a**2)*tanh(a*(y - x))/(cosh(a*(y - x)))**2
  hessu(1,2,:) =  2*(a**2)*sinh(a*(y - x))/(cosh(a*(y - x)))**3
  hessu(2,1,:) = hessu(1,2,:)
  

!   print*,"metric"
!   print*,u(1),"  ", u(mesh%npoin)
!   print*,mesh%ndime
  isIsotropic = .true.
  
  !(metric,hessu,ndime,hmax,isIsotropic)
  hmax = 1.0_rp
  call  compute_metric_from_hessian(metric,hessu,mesh%ndime,hmax,isIsotropic)
!   !metric =  compute_metric_from_hessian(u,mesh%ndime,isIsotropic)
!
!   metric%M = metric%M/maxval(metric%M)
!   print*,minval(metric%M)
!   print*,maxval(metric%M)
!   metric%M(1,1,:) =   metric%M(1,1,:)**40
!   print*,minval(metric%M)
!   print*,maxval(metric%M)
!   metric%M = metric%M/nx
!   !metric%M(1,1,:) = u
!
!   print*,"qual"
!   call compute_mesh_quality_sizeShape(mesh,metric,q)
!   call print_q_stats(q)
!
!   if(out_debug_paraview) call mesh%output(filename='./unitt/unitt_adapti_hessian/mesh')
!   if(out_debug_paraview) call out_paraview_inp(mesh,filename='./unitt/unitt_adapti_hessian/mesh_0',nodeField=metric%M(1,1,:),elemField=q)
!
!   print*,"adapt_mesh_to_metric"
!   call adapt_mesh_to_metric(mesh,metric,mapNodes_old_to_new)
!
!   if(out_debug_text) print*,'_______________________________________________________________'
!   if(out_debug_text) print*,'_____________________Checking quality....______________________'
!   call compute_mesh_quality_sizeShape(mesh,metric,q)
!   call print_q_stats(q)
!
!   if(out_debug_paraview) call out_paraview_inp(mesh,filename='./unitt/unitt_adapti_hessian/mesh_1',nodeField=metric%M(1,1,:),elemField=q)
!
  call mesh%deallo()
  call metric%deallo()
!   call memory_deallo(memor_adapt,'q','unitt_adapti_hessian',q)
!   call memory_deallo(memor_adapt,'mapNodes_old_to_new','unitt_adapti_hessian',mapNodes_old_to_new)

!   call runend('O.K.!')                                               ! Finish Alya
end program unitt_adapti_hessian     
