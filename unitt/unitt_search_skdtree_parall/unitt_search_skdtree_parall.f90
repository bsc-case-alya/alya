!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_search_skdtree_parall

  use def_kintyp_basic
  use def_parame
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_search_method
  use def_maths_tree
  use def_master
  use mod_memory
  use def_kermod
  use def_domain
  use def_interpolation_method
  use mod_parall,                only : PAR_COMM_MY_CODE
  use mod_strings,               only : integer_to_string
  use mod_par_tools,             only : par_subdomain_bb
  implicit none
  type(mesh_type_basic)           :: mesh_out
  !type(maths_skdtree)             :: skdtree
  !type(maths_skdtree)             :: skdtree_par
  integer(ip)                     :: ipoin,idime,inodb,nn
  integer(ip)                     :: pelty,ii,iboun,imeth,ipart
  integer(ip)                     :: pblty,ifoun,pnodb,iboun_min
  integer(8)                      :: memor_loc(2)
  integer(8)                      :: memor_tmp(2)
  real(rp)                        :: xx_test(2),dista,dimin
  real(rp)                        :: proje(3),coloc(3)
  real(rp)                        :: vmin(3),r,theta
  real(rp)                        :: bocod(2,64)
  real(rp)                        :: shapf(64)
  real(rp)                        :: shapf_min(64)
  real(rp)                        :: derit(2,64)
  real(rp),             pointer   :: xx(:,:),vv(:,:),dd(:)
  real(rp),             pointer   :: bbbox(:,:,:)
  class(search_method), pointer   :: skdtree        ! Search method for mesh size
  class(search_method), pointer   :: skdtree_par    ! Search method for mesh size
  class(interpolation), pointer   :: interp
  character(len=5),     pointer   :: names(:)
  real(rp),             pointer   :: res(:,:)
  real(rp)                        :: pp_near(2)
  real(rp),             pointer   :: sbbox(:,:,:)
  
  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction

  memor_loc = 0_8
  memor_tmp = 0_8
  nullify(bbbox,xx,vv,dd,names,res,sbbox,skdtree,skdtree_par,interp)
  !
  ! Test points
  !
  nn = 0

  select case ( kfl_paral ) 
  case ( 1 ) ; nn = 1
  case ( 2 ) ; nn = 3
  case ( 3 ) ; nn = 2
  end select

  if( nn > 0 ) then
     allocate(xx(2,nn))
     allocate(vv(2,nn))
     allocate(dd(  nn))
  end if
  
  select case ( kfl_paral ) 
  case ( 1 ) 
     xx(:,1) = (/  0.5_rp , 0.5_rp /) ; dd(1) = 0.5_rp
  case ( 2 )
     xx(:,1) = (/  0.7_rp , 0.8_rp /) ; dd(1) = 0.2_rp
     xx(:,2) = (/  1.1_rp , 1.1_rp /) ; dd(2) = 0.14142135623731_rp
     xx(:,3) = (/  1.3_rp , 0.1_rp /) ; dd(3) = 0.3_rp
  case ( 3 )
     xx(:,1) = (/ -0.7_rp , 0.8_rp /) ; dd(1) = 0.7_rp
     xx(:,2) = (/  0.1_rp , 0.4_rp /) ; dd(2) = 0.1_rp
  end select
  !
  ! Bounding boxes
  !
  imeth = 1
  if( .not. associated(meshe(ndivi) % boundary) ) then
     allocate(meshe(ndivi) % boundary)
     meshe(ndivi) % boundary % mesh => meshe(ndivi)
     call meshe(ndivi) % boundary % init  ()
     call meshe(ndivi) % boundary % assoc (ndime,mnodb,nboun,npoin,lnodb,ltypb,coord,LELBO=lelbo,LBOEL=lboel)
     meshe(ndivi) % boundary % mesh => meshe(ndivi)
  end if
  call meshe(ndivi) % boundary_bb(bbbox)
  call par_subdomain_bb(meshe(ndivi) % comm,bbbox,sbbox)

  !----------------------------------------------------------------------
  !
  ! Skdtree
  !  
  !----------------------------------------------------------------------

  allocate( maths_skdtree :: skdtree     )
  allocate( maths_skdtree :: skdtree_par )

  call skdtree_par % init    ()
  select type ( skdtree_par )
  class is (maths_skdtree ) ; call skdtree_par % input   ()
  end select
  call skdtree_par % fill    (BOBOX=sbbox)

  call skdtree     % init    ()
  select type ( skdtree )
  class is (maths_skdtree ) ; call skdtree     % input   ()
  end select
  call skdtree     % fill    (BOBOX=bbbox)
  
  !----------------------------------------------------------------------
  !
  ! Output meshes
  !
  !----------------------------------------------------------------------
  
  call mesh_out % init()  
  call skdtree  % mesh(&
       mesh_out % ndime,mesh_out % mnode,mesh_out % nelem,&
       mesh_out % npoin,mesh_out % lnods,mesh_out % ltype,&
       mesh_out % coord,MEMORY_COUNTER=memor_loc)
  call skdtree  % results(res,names)
  call mesh_out % output (          FILENAME='skdtree_seq-'//integer_to_string(kfl_paral))
  call mesh_out % results(res,names,FILENAME='skdtree_seq-'//integer_to_string(kfl_paral))
  call skdtree  % results(res,names,ONLY_DEALLOCATE=.true.)
  call mesh_out % deallo(MEMORY_COUNTER=memor_loc)
  
  
  call mesh_out    % init()  
  call skdtree_par % mesh(&
       mesh_out % ndime,mesh_out % mnode,mesh_out % nelem,&
       mesh_out % npoin,mesh_out % lnods,mesh_out % ltype,&
       mesh_out % coord,MEMORY_COUNTER=memor_loc)
  call mesh_out % output (FILENAME='skdtree_par-'//integer_to_string(kfl_paral))
  call mesh_out % deallo (MEMORY_COUNTER=memor_loc)

  !----------------------------------------------------------------------
  !
  ! Interpolation
  !   
  !----------------------------------------------------------------------

  allocate(interp)
  call interp      % init    ()
  call interp      % input   (skdtree,skdtree_par,INTERPOLATION_METHOD=INT_BOUNDARY_INTERPOLATION,COMM=PAR_COMM_MY_CODE)
  if( imeth == 1 ) then
     call interp % preprocess(xx,meshe(ndivi) % boundary)
  else
     call interp % preprocess(xx,meshe(ndivi))      
  end if
  call interp % values       (meshe(ndivi) % coord,vv)
  
  !----------------------------------------------------------------------
  !
  ! Checks
  !  
  !----------------------------------------------------------------------
  !
  ! Memory
  !
  call interp % deallo()
  if( interp % memor(1) /= 0 ) then
     print*,'bad interp memory'
     stop 1
  end if
  !
  ! Check nearest boundary
  !
  print*,'checking=',kfl_paral
  do ii = 1,nn
     dimin = sqrt(dot_product(vv(1:ndime,ii)-xx(1:ndime,ii),vv(1:ndime,ii)-xx(1:ndime,ii)))
     if( abs(dd(ii)-dimin) > 1.0e-10_rp ) then
        print*,'kfl_paral= '//integer_to_string(kfl_paral)//' failed for point '//integer_to_string(ii)
        print*,'xx=        ',xx(:,ii)
        print*,'vv=        ',vv(1:ndime,ii)
        print*,'dd=        ',dd(ii)
        print*,'dimin=     ',dimin
     end if
  end do

  call interp      % deallo()
  call skdtree     % deallo()
  call skdtree_par % deallo()
  deallocate(interp)
  deallocate(skdtree)
  deallocate(skdtree_par)
  print*,'end=',kfl_paral
  call runend('O.K.!')
  
end program unitt_search_skdtree_parall
 
