!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_doiter()
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_doiter
  ! NAME 
  !    nsi_doiter
  ! DESCRIPTION
  !    This routine solves an iteration of the linearized incompressible NS
  !    equations.
  ! USES
  !    nsi_begite
  !    nsi_updbcs
  !    nsi_solite
  ! USED BY
  !    Nastin
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_solver
  use def_domain
  use def_nastin
  use mod_timings, only : timings_ini
  use mod_timings, only : timings_end
  implicit none
  
  if( kfl_stead_nsi == 0 ) then
     call timings_ini()
     call nsi_begite()
     call timings_end(ITASK_BEGITE)

     do while( kfl_goite_nsi == 1 )
        call nsi_solite()
        call nsi_endite(ITASK_ENDINN)
     end do

     call timings_ini()
     call nsi_endite(ITASK_ENDITE)
     call timings_end(ITASK_ENDITE)

  end if

end subroutine nsi_doiter

