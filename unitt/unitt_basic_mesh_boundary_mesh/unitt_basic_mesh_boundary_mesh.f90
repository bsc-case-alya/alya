!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_basic_mesh_boundary_mesh
  !
  ! Test boundary mesh
  !
  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_maths_bin
  use def_maths_tree
  use def_maths_octbin
  use def_master
  use mod_memory
  implicit none

  real(rp),             parameter :: epsil = 1.0e-12_rp
  type(mesh_type_basic)           :: mesh
  type(mesh_type_basic)           :: mesh_bou
  integer(ip)                     :: ipoin,idime,ielem,inode,iboun
  integer(ip)                     :: pelty,ii
  real(rp)                        :: dista,xx_test(2)
  integer(8)                      :: count_rate8
  integer(8)                      :: memor_loc(2)
  integer(8)                      :: memor_tmp(2)

  memor_loc = 0_8
  memor_tmp = 0_8
  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)
  !
  ! Create mesh
  !
  call elmgeo_element_type_initialization()

  call mesh     % init('MY_MESH')
  call mesh_bou % init('BOUNDARY')
  
  mesh % nelem = 9
  mesh % npoin = 16
  mesh % ndime = 2
  mesh % mnode = 4
  
  call mesh % alloca()

  mesh % lnods(:,1)  = (/  12 ,7 ,5 ,10  /)
  mesh % lnods(:,2)  = (/  14 ,9 ,7 ,12   /)
  mesh % lnods(:,3)  = (/  16, 15, 9 ,14  /) 
  mesh % lnods(:,4)  = (/  7, 4 ,2 ,5   /)
  mesh % lnods(:,5)  = (/  9 ,8 ,4 ,7  /) 
  mesh % lnods(:,6)  = (/  15, 13, 8, 9 /)  
  mesh % lnods(:,7)  = (/  4 ,3 ,1 ,2  /) 
  mesh % lnods(:,8)  = (/  8 ,6 ,3 ,4  /) 
  mesh % lnods(:,9)  = (/  13 ,11, 6, 8  /)

  mesh % ltype(1:9)  = QUA04

  mesh % coord(:,1 ) = (/ 0.000000e+00_rp , 1.000000e+00_rp /) 
  mesh % coord(:,2 ) = (/ 0.000000e+00_rp , 6.666667e-01_rp /) 
  mesh % coord(:,3 ) = (/ 3.333333e-01_rp , 1.000000e+00_rp /) 
  mesh % coord(:,4 ) = (/ 3.333333e-01_rp , 6.666667e-01_rp /) 
  mesh % coord(:,5 ) = (/ 0.000000e+00_rp , 3.333333e-01_rp /) 
  mesh % coord(:,6 ) = (/ 6.666667e-01_rp , 1.000000e+00_rp /) 
  mesh % coord(:,7 ) = (/ 3.333333e-01_rp , 3.333333e-01_rp /) 
  mesh % coord(:,8 ) = (/ 6.666667e-01_rp , 6.666667e-01_rp /) 
  mesh % coord(:,9 ) = (/ 6.666667e-01_rp , 3.333333e-01_rp /) 
  mesh % coord(:,10) = (/ 0.000000e+00_rp , 0.000000e+00_rp /) 
  mesh % coord(:,11) = (/ 1.000000e+00_rp , 1.000000e+00_rp /) 
  mesh % coord(:,12) = (/ 3.333333e-01_rp , 0.000000e+00_rp /) 
  mesh % coord(:,13) = (/ 1.000000e+00_rp , 6.666667e-01_rp /) 
  mesh % coord(:,14) = (/ 6.666667e-01_rp , 0.000000e+00_rp /) 
  mesh % coord(:,15) = (/ 1.000000e+00_rp , 3.333333e-01_rp /) 
  mesh % coord(:,16) = (/ 1.000000e+00_rp , 0.000000e+00_rp /)
  !

  !call mesh % output(10_ip)
  call mesh_bou % boundary_mesh(mesh)
  call mesh_bou % output(filename='boundary')

   if( mesh_bou % lnods( 1,1) /= 1 ) stop 1
   if( mesh_bou % lnods( 2,1) /= 2 ) stop 1

   if( mesh_bou % lnods( 1,    2 ) /=  2 ) stop 1 
   if( mesh_bou % lnods( 2,    2 ) /=  3 ) stop 1
   if( mesh_bou % lnods( 1,    3 ) /=  3 ) stop 1
   if( mesh_bou % lnods( 2,    3 ) /=  4 ) stop 1
   if( mesh_bou % lnods( 1,    4 ) /=  5 ) stop 1
   if( mesh_bou % lnods( 2,    4 ) /=  6 ) stop 1
   if( mesh_bou % lnods( 1,    5 ) /=  4 ) stop 1
   if( mesh_bou % lnods( 2,    5 ) /=  5 ) stop 1
   if( mesh_bou % lnods( 1,    6 ) /=  7 ) stop 1
   if( mesh_bou % lnods( 2,    6 ) /=  1 ) stop 1
   if( mesh_bou % lnods( 1,    7 ) /=  6 ) stop 1
   if( mesh_bou % lnods( 2,    7 ) /=  8 ) stop 1
   if( mesh_bou % lnods( 1,    8 ) /=  9 ) stop 1
   if( mesh_bou % lnods( 2,    8 ) /= 10 ) stop 1
   if( mesh_bou % lnods( 1,    9 ) /= 10 ) stop 1
   if( mesh_bou % lnods( 2,    9 ) /=  7 ) stop 1
   if( mesh_bou % lnods( 1,   10 ) /= 11 ) stop 1
   if( mesh_bou % lnods( 2,   10 ) /=  9 ) stop 1
   if( mesh_bou % lnods( 1,   11 ) /=  8 ) stop 1
   if( mesh_bou % lnods( 2,   11 ) /= 12 ) stop 1
   if( mesh_bou % lnods( 1,   12 ) /= 12 ) stop 1
   if( mesh_bou % lnods( 2,   12 ) /= 11 ) stop 1

   if( abs(mesh_bou % coord(1,9)-0.33333330E+000) > 1.0e-6 ) stop 2

 end program unitt_basic_mesh_boundary_mesh
