#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



#BSUB -n 64  
#BSUB -R"span[ptile=16]"
#BSUB -o out00.run
#BSUB -e err00.run 
#BSUB -J R0040 
#BSUB -W 00:59  
# #BSUB -q bsc_case  

ALYA_PATH=/home/bsc21/bsc21704/z2016/REPOSITORY/ALYAs/ALYA_2016Mar12/
ALYA_PATH=/home/bsc21/bsc21704/z2016/REPOSITORY/ALYAs/ALYA_2016JUL18/
ALYA_PATH=/home/bsc21/bsc21704/z2017/REPOSITORY/ALYA_2017JAN10/Executables/unix
ALYA_PATH=/home/bsc21/bsc21704/z2017/REPOSITORY/ALYA_2017MAY09_MNT/Executables/unix

#-------------------------------------------------------------------------||--# 
#
ALYAi=3
ALYAj=3 
#
#-------------------------------------------------------------------------||--# 
find . -type l -delete
#
CASEi=fluid
CASEj=solid
#
ln -s cou.dat  $CASEi.cou.dat
ln -s cou.dat  $CASEj.cou.dat
#
#ALYAij=64 
#ALYAj=4 
#ALYAi=$((ALYAij-ALYAj))
#
time mpirun \
-np $ALYAi  \
 $ALYA_PATH/Alya.x $CASEi  \
 : \
-np $ALYAj \
 $ALYA_PATH/Alya.x $CASEj 
#
find . -type l -delete
#-------------------------------------------------------------------------||--# 
#
#NOTA:
python TOOLs/analizeSets02.py -F fluid 
#
