!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Kernel
!> @{
!> @file    what_is_being_done.f90
!> @author  houzeaux
!> @date    2020-03-04
!> @brief   Do things
!> @details According to what is being done, change some flags
!> @} 
!-----------------------------------------------------------------------

subroutine what_is_being_done()

  use def_master
  use def_solver
  implicit none
  integer(ip) :: imodu,ivari
  !
  ! Force assembly os solvers if we have just read a restart or did a repartitioning
  ! Sometime matrices are only assembled when ittim==1 !
  !
  do imodu = 1,mmodu
     if( kfl_modul(imodu) == 1 ) then
        if( associated(momod(imodu) % solve) ) then
           do ivari = 1,size(momod(imodu) % solve,KIND=ip) 
              if( momod(imodu) % solve(ivari) % kfl_algso /= SOL_NO_SOLVER ) then
                 if( do_repartitioning .or. do_amr .or. read_restart ) then
                    momod(imodu) % solve(ivari) % kfl_force_assembly = 1
                 else
                    momod(imodu) % solve(ivari) % kfl_force_assembly = 0
                 end if
              end if
           end do
        end if
     end if
  end do
  do_repartitioning = .false.
  do_amr            = .false.
  read_restart      = .false.
  write_restart     = .false.

end subroutine what_is_being_done
  
  
