!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine corner(exwor,ipoin,ibott)

!------------------------------------------------------------------------
!
! This check if a corner point is of step of bottom type
!   
!          jpoin         jpoin
!            O             O<--------O ipoin
!           /|\                     /|
!  (domain)  |                     / |  (domain)
!            |                   \/_ |     
!            |                      \|/
!  O<--------O ipoin                 O 
!             \
!             _\|
!
!     Bottom type            Step type
!     angle>90               0<angle<90
!
!------------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_domain, only     :  lpoty,r_bou,c_bou,coord,npoin,nzbou,ndime
  implicit none
  integer(ip), intent(in)  :: ipoin
  real(rp),    intent(in)  :: exwor(ndime)
  integer(ip), intent(out) :: ibott
  integer(ip)              :: ista0,ista1,icolu,jpoin
  real(rp)                 :: vnorm,vecau(3)
      
  ibott=1                                                 ! By default, 
     
  ista0=r_bou(ipoin)
  ista1=r_bou(ipoin+1)-1
  icolu=ista0
  do while(icolu/=ista1)
     icolu=icolu+1
     jpoin=c_bou(icolu)
     if(ipoin/=jpoin) then
        vecau(    1) = coord(    1,jpoin) - coord(    1,ipoin)
        vecau(    2) = coord(    2,jpoin) - coord(    2,ipoin)
        vecau(ndime) = coord(ndime,jpoin) - coord(ndime,ipoin)
        call vecuni(ndime,vecau,vnorm)            
        vnorm=dot_product(vecau(1:ndime),exwor(1:ndime))
        if(vnorm>0.1_rp) then                             ! ipoin is of step type            
           icolu=ista1
           ibott=0
           return
        end if
     end if
  end do
  
end subroutine corner
