!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Kermod
!> @{
!> @file    def_kermod_generic.f90
!> @author  houzeaux
!> @date    2020-11-20
!> @brief   Definition for properties
!> @details Definitions for generic calculations of properties
!-----------------------------------------------------------------------

module def_vectorization

#ifndef VECTOR_SIZE
  use def_master,       only : VECTOR_SIZE 
#endif
  use def_kintyp_basic, only : ip
  implicit none
  public
  
  integer(ip) :: ivect

#ifdef OPENACCHHH
#define DEF_VECT ivect
#else
#ifdef VECTOR_SIZE_VARIABLE
#define DEF_VECT 1:VECTOR_DIM
#else
#define DEF_VECT 1:VECTOR_SIZE
#endif
#endif
  
end module def_vectorization
!> @}
