!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_maths_quadratic_equation 
  
  use def_kintyp_basic, only : ip, rp
  use mod_maths,        only : maths_quadratic_equation
  
  implicit none
  real(rp)                 :: aa,bb,cc,x1,x2,rr,rrmax,dd
  real(rp),    parameter   :: epsil=epsilon(1.0_8)
  integer(ip)              :: num_solutions,ii,iimax
  integer(ip), parameter   :: numb = 100000_ip
  real(rp)                 :: rval(3)
  integer(4)              :: nums
  integer(4), allocatable :: seed(:)
  integer(ip)              :: values(8)

  !call date_and_time(VALUES=values)
  call random_seed(size = nums)
  allocate(seed(nums))
  !iimax = min(nums,size(values))
  !seed(1:iimax) = values(1:iimax)
  !seed(:) = values(:)
  !call random_seed(put=seed)
  call random_seed()
  rrmax = 0.0_rp
  
  do ii = 1,numb
     
     call RANDOM_NUMBER(rval)     
     aa = rval(1)
     bb = rval(2)
     cc = rval(3)
     if (ii == 1) then
        aa = 0.0_rp
     end if
     dd = max(abs(aa),abs(bb),abs(cc),epsil)
     call maths_quadratic_equation(aa,bb,cc,x1,x2,num_solutions)

     if( num_solutions > 0 ) then
        rr    = ( aa*x1**2 + bb*x1 + cc ) / dd
        rrmax = max(rr,rrmax)
        if( abs(rr) > 1.0e-06_rp ) then
           print*,'ii, a,b,c,x1= ',aa,bb,cc,x1
           print*,'rr= ',rr
           stop 1
        end if
        rr    = ( aa*x2**2 + bb*x2 + cc ) / dd
        rrmax = max(rr,rrmax)
        if( abs(rr) > 1.0e-06_rp ) then
           print*,'ii, a,b,c,x2= ',aa,bb,cc,x2
           print*,'rr= ',rr
           stop 1
        end if
     end if
     
  end do

  deallocate(seed)

  aa = 1.7567778243243737E-007_rp
  bb = 0.96967507176162515_rp
  cc = 0.37749187734578438_rp
  dd = max(abs(aa),abs(bb),abs(cc),epsil)
  call maths_quadratic_equation(aa,bb,cc,x1,x2,num_solutions)
  
  if( num_solutions > 0 ) then
     rr    = ( aa*x1**2 + bb*x1 + cc ) / dd
     rrmax = max(rr,rrmax)
     if( abs(rr) > 1.0e-06_rp ) then
        print*,'ii, a,b,c,x1= ',aa,bb,cc,x1
        print*,'rr= ',rr
        stop 1
     end if
     rr    = ( aa*x2**2 + bb*x2 + cc ) / dd
     rrmax = max(rr,rrmax)
     if( abs(rr) > 1.0e-06_rp ) then
        print*,'ii, a,b,c,x2= ',aa,bb,cc,x2
        print*,'rr= ',rr
        stop 1
     end if
  end if
  
  stop
  
  !-5519622.2877167519 
end program unitt_maths_quadratic_equation
