!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_dyncou(itask)
  !------------------------------------------------------------------------
  !****f* Temper/nsi_dyncou
  ! NAME 
  !    nsi_dyncou
  ! DESCRIPTION
  !    This routine manages the dynamic coupling of nastin module
  ! USES
  ! USED BY
  !    nsi_turnon
  !------------------------------------------------------------------------
  use def_parame
  use def_inpout
  use def_master
  use def_nastin
  use def_domain
  implicit none
  integer(ip), intent(in) :: itask

  select case(kfl_dynco_nsi)

  case(1_ip)
     !
     ! Marek: Cooling system + typical room
     !
     call nsi_dync01(itask)

  end select

end subroutine nsi_dyncou
