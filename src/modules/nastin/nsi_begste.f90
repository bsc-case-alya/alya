!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_begste()
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_begste
  ! NAME 
  !    nsi_begste
  ! DESCRIPTION
  !    This routine prepares for a new time step of the incompressible NS
  !    equations.
  ! USES
  !    nsi_iniunk
  !    nsi_updtss
  !    nsi_updbcs
  !    nsi_updunk
  ! USED BY
  !    Nastin
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_nastin
  use mod_nsi_low_Mach,      only : nsi_low_Mach_upd_mass_rho
  use mod_nsi_multi_step_fs, only : nsi_multi_step_fs_matrices
  use mod_nsi_multi_step_fs, only : nsi_multi_step_fs_solution

  implicit none

  if( kfl_stead_nsi /= 1 )  then 
     !
     ! Update frame of reference
     !
     call nsi_updfor()
     !
     ! Update boundary conditions
     !
     call nsi_updbcs(ITASK_BEGSTE) 
     call nsi_updunk(ITASK_BEGSTE) ! (:,2) <= (:,1)
     !
     ! Initialize some variables before starting the time step
     !
     call nsi_inivar(3_ip)
     !
     ! Coupling with dynamic solver
     !
     call nsi_dyncou(1_ip)
     !
     ! Update mass_rho term for low-Mach, for drho/dt calculation
     !
     if( associated(mass_rho_nsi) ) &
          call nsi_low_Mach_upd_mass_rho()
     !
     ! Moving subdomains, recompute matrices
     !
     if( associated(velom) .and. NSI_FRACTIONAL_STEP ) then
        call nsi_multi_step_fs_matrices()
     end if
     
  end if 
  !
  ! Surface tension
  !
  if( kfl_surte_nsi /= 0 ) call nsi_norcur

  call nsi_coupli(ITASK_BEGSTE)
  !
  ! Initialize coupling
  !
  call nsi_plugin_init()

end subroutine nsi_begste
