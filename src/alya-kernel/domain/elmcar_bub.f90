!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine elmcar_bub(&
     pnode,pgaus,plapl,deriv,shapf_bub,deriv_bub,elcod,&
     gpsha,gpcar,gpsha_bub,gpcar_bub,ielem)
  !-----------------------------------------------------------------------
  !****f* domain/elmca2
  ! NAME
  !    elmca2
  ! DESCRIPTION
  !    This routine calculates:
  !    GPCAR: Cartesian derivatives
  !    GPVOL: Unit volume
  !    GPHES: Hessian matrix
  ! USES
  !    invmtx
  ! USED BY
  !    ***_elmope
  !    extnor
  ! SOURCE
  !-----------------------------------------------------------------------
  use def_parame, only       :  twopi
  use def_domain, only       :  ndime,ntens,kfl_naxis,kfl_spher,mnode,&
       &                        kfl_savda,elmda,lelch,ltype
  use def_elmtyp, only       :  ELCUT
  use def_kintyp, only       :  ip,rp
  use mod_cutele, only       :  elmcar_cut
  use mod_elmgeo, only       :  elmgeo_jacobian_matrix
  implicit none
  integer(ip), intent(in)    :: pnode
  integer(ip), intent(in)    :: plapl
  integer(ip), intent(in)    :: ielem
  integer(ip), intent(inout) :: pgaus
  real(rp),    intent(in)    :: deriv(ndime,pnode,*)
  real(rp),    intent(in)    :: elcod(ndime,pnode)
  real(rp),    intent(in)    :: shapf_bub(*)
  real(rp),    intent(in)    :: deriv_bub(ndime,*)
  real(rp),    intent(out)   :: gpsha(pnode,*)
  real(rp),    intent(out)   :: gpcar(ndime,mnode,*)
  real(rp),    intent(out)   :: gpsha_bub(*)
  real(rp),    intent(out)   :: gpcar_bub(ndime,*)

  integer(ip)                :: igaus,inode,idime,jdime
  real(rp)                   :: gpdet
  real(rp)                   :: xjaci(ndime,ndime)

  do igaus = 1,pgaus
     gpsha_bub(igaus) = shapf_bub(igaus)
     call elmgeo_jacobian_matrix(&
          ndime,pnode,elcod,deriv(1,1,igaus),&
          gpdet,xjaci)
     do idime = 1,ndime
        gpcar_bub(idime,igaus) = 0.0_rp
        do jdime = 1,ndime
           gpcar_bub(idime,igaus) = gpcar_bub(idime,igaus) + xjaci(jdime,idime) * deriv_bub(jdime,igaus)
        end do
     end do
  end do

end subroutine elmcar_bub
