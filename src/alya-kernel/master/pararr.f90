!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine pararr(wtask,ntype,ndimr,rvarr)
  !------------------------------------------------------------------------
  !****f* Parall/pararr
  ! NAME
  !    pararr
  ! DESCRIPTION
  !    Works with arrays to deal with parallelization
  ! OUTPUT
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_kintyp,         only  :  ip,rp,lg
  use def_master,         only  :  npari,nparr,nparc,parin,parre,parr1,kfl_paral
  use def_master,         only  :  party,parki,pardi,IPARALL,pard1,pari1,npasi
  use def_master,         only  :  ISEQUEN,INOTMASTER,INOTEMPTY
  use def_master,         only  :  NPOIN_TYPE,NFACE_TYPE,lntra,nfacg,lninv_loc
  use def_master,         only  :  kfl_desti_par
  use def_domain,         only  :  npoin,nbopo,npoin_2,lmast,nperi
  use def_master,         only  :  NEDGE_TYPE
  use def_domain,         only  :  nedge
  use mod_couplings,      only  :  COU_INTERPOLATE_NODAL_VALUES_go
  use mod_communications, only  :  PAR_INTERFACE_NODE_EXCHANGE
  use def_coupli,         only  :  ncoup_implicit
  use def_coupli,         only  :  mcoup
  use def_coupli,         only  :  coupling_type
  use def_coupli,         only  :  ncoup_implicit_d
  use def_coupli,         only  :  lcoup_implicit_d
  use def_coupli,         only  :  ncoup_implicit_n
  use def_coupli,         only  :  lcoup_implicit_n
  use def_coupli,         only  :  RESIDUAL,UNKNOWN
  use def_coupli,         only  :  BETWEEN_SUBDOMAINS
  use def_master,         only  :  current_zone,kfl_paral,modul,ISLAVE,mmodu
  use def_solver,         only  :  solve_sol
  use mod_communications, only  :  PAR_MIN,PAR_MAX,PAR_SUM
  use mod_periodicity,    only  :  periodicity_sequential
  use mod_par_slexca,     only  :  par_slexca
  implicit none 
  character(3), intent(in) :: wtask
  integer(ip),  intent(in) :: ntype,ndimr
  real(rp),     target     :: rvarr(ndimr)

  integer(ip)              :: ii,kk,icoup,jcoup,jpoin
  integer(ip)              :: ipoin,jj,ll,kcoup,idime
  integer(ip)              :: kfl_mask,kpoin
  integer(ip)              :: idofn,jdofn
  real(rp),     pointer    :: rvarr_tmp(:)
  real(rp),     pointer    :: rvarr_int(:)
  integer(ip)              :: dummi(2)
  real(rp)                 :: dummr(2)
  

  if( IPARALL .or. ( ISEQUEN .and. (wtask == 'SLX' .or. wtask == 'SOL' .or. wtask == 'SLA') ) ) then

     npari = 0
     nparc = 0

     if( wtask == 'SND' ) then

        nparr =  ndimr
        parre => rvarr
        call par_sendin()

     else if( wtask == 'RCV' ) then

        nparr =  ndimr
        parre => rvarr
        call par_receiv()

     else if( wtask == 'SLX' .or. wtask == 'SOL' ) then

        if( ISEQUEN .and. nperi /= 0 .and. ntype == NPOIN_TYPE ) then
           pard1 = ndimr/npoin
           call periodicity_sequential(pard1,rvarr)
        end if
        !
        ! par_slexch
        !
        if( wtask == 'SOL' ) then
           kfl_mask = 1
        else
           kfl_mask = 0
        end if

        if( INOTEMPTY ) then
           
           if( ntype == NPOIN_TYPE .or. ntype == NEDGE_TYPE ) then
              
              if ( ntype == NPOIN_TYPE ) then
                 pard1 = ndimr/npoin
              else
                 pard1 = ndimr/nedge
              end if
              
              party = ntype
              if( pard1 == 1 ) then
                 parki =  2
                 pardi =  1
              else
                 parki =  5
                 pardi =  1
              end if

              if( ISLAVE ) then
                 !parr1 => rvarr
                 call PAR_INTERFACE_NODE_EXCHANGE(pard1,rvarr,'SUM')
                 !call par_slexch() 
              end if
              
           else if( ntype == NFACE_TYPE ) then

              pard1 = ndimr/nfacg
              party = ntype
              if( pard1 == 1 ) then
                 parki =  2
                 pardi =  1
              else
                 parki =  5
                 pardi =  1
              end if
              
              if( ISLAVE ) then
                 parr1 => rvarr
                 call par_lgface(2_ip)
              end if
              
           else
              call runend('PARARR: NOT CODED')
           end if
           !
           ! Subdomain coupling 
           !
           if( ntype == NPOIN_TYPE .and. ncoup_implicit > 0 .and. INOTEMPTY ) then
              
              allocate( rvarr_tmp(ndimr),rvarr_int(ndimr) )
              rvarr_tmp(1:ndimr) = rvarr(1:ndimr)
              kk                 = ndimr/npoin
              !
              ! Neumann transmission condition
              !
              do kcoup = 1,ncoup_implicit_n
                 icoup = lcoup_implicit_n(kcoup)
                 rvarr_int(1:ndimr) = 0.0_rp
                 if( wtask == 'SOL' ) then
                    call COU_INTERPOLATE_NODAL_VALUES_go(icoup,kk,rvarr_int,rvarr_tmp,solve_sol(1) &
                         &% kfl_fixno)
                 else
                    call COU_INTERPOLATE_NODAL_VALUES_go(icoup,kk,rvarr_int,rvarr_tmp)
                 end if                 
                 rvarr(1:ndimr) = rvarr(1:ndimr) + rvarr_int(1:ndimr)
              end do
              deallocate(rvarr_int)
              rvarr_tmp(1:ndimr) = rvarr(1:ndimr)
              !
              ! Dirichlet transmission condition
              !              
              do kcoup = 1,ncoup_implicit_d
                 icoup = lcoup_implicit_d(kcoup)
                 if( wtask == 'SOL' ) then
                    call COU_INTERPOLATE_NODAL_VALUES_go(icoup,kk,rvarr,rvarr_tmp,solve_sol(1) %&
                         & kfl_fixno)
                 else
                    call COU_INTERPOLATE_NODAL_VALUES_go(icoup,kk,rvarr,rvarr_tmp)
                 end if
              end do
              deallocate(rvarr_tmp)

           end if
        end if

     else if( wtask == 'SLA' ) then
        
        if( ISEQUEN .and. nperi /= 0 .and. ntype == NPOIN_TYPE ) then
           pard1 = ndimr/npoin
           call periodicity_sequential(pard1,rvarr)
        end if
        !
        ! par_slexca
        !
        if( ntype == NPOIN_TYPE ) then
          
           if( ntype == NPOIN_TYPE ) pard1 = ndimr/npoin

           party =  ntype
           if( pard1 == 1 ) then
              parki =  2
              pardi =  1
           else
              parki =  5
              pardi =  1
           end if
           parr1 => rvarr
           if( ISLAVE ) call par_slexca()
        else
           call runend('PARARR: NOT CODED')
        end if

     else

        call runend('PARARR: WRONG CASE')

     end if

     nparr = 0
     nullify(parre)
     nullify(parr1)

  end if

end subroutine pararr
