!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_bounda()
!-------------------------------------------------------------------------------
!****f* parall/par_bounda
! NAME
!    par_bounda
! DESCRIPTION
!    
! INPUT
!    lnpar_par 
!    xadj 
!    adj
! OUTPUT
!    part
! USED BY
!    par_arrays
!***
!-------------------------------------------------------------------------------
  use def_kintyp
  use def_domain
  use def_master
  use def_parall
  implicit none
  integer(ip) :: iboun, ielem, domai

  nboun_par(1:npart_par) = 0
  nboun_total = 0
  if(kfl_bouel==1) then
     do iboun= 1, nboun
        ielem            = lelbo(iboun)
        if( ielem == 0 ) then
           call runend('PAR_BOUNDA: YOU HAVE NOT PROVIDED THE BOUNDARY/ELEMENT CONNECTIVITY FILE (LELBO)')
        end if
        domai            = lepar_par(ielem)
        lbpar_par(iboun) = domai
        nboun_par(domai) = nboun_par(domai) + 1
     enddo
     do domai = 1,npart_par
        nboun_total = nboun_total + nboun_par(domai)
     end do
  else if(nboun>0) then
     call runend('PARALL: MUST GIVE BOUNDARY/ELEMENT CONNECTIVITY')
  end if

end subroutine par_bounda
