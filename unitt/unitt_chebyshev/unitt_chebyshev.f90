!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_chebyshev
  !
  ! Order strings 
  !
  use def_kintyp_basic
  use def_elmtyp
  use def_domain, only : ndime,mnode,ntens
  use def_kintyp_basic
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_isoparametric
  use def_quadrature
  use def_chebyshev
  use mod_element_integration
  use def_master, only : zeror
  implicit none

  integer(ip)              :: ielty,idime,inode,pnode,ielem,ipoin
  type(mesh_type_basic)    :: mesh
  real(rp)                 :: elcod(3,64)
  integer(ip)              :: igaus,pgaus,pdime
  real(rp)                 :: N(64,64),vol,one3
  real(rp), allocatable    :: gpsha(:,:)                    ! N
  real(rp), allocatable    :: gpder(:,:,:)              ! dN/dsi
  real(rp), allocatable    :: gpcar(:,:,:)              ! dN/dxi
  real(rp), allocatable    :: gpvol(:)                          ! w*|J|, |J|
  real(rp), allocatable    :: x(:,:)
  real(rp)                 :: grad(3,3)
  
#ifndef NDIMEPAR
  ndime = 3
#endif

  mnode = 64
  ntens = 6
  one3 = 1.0_rp/3.0_rp
  
  call elmgeo_element_type_initialization()
  !
  ! Define mesh
  !
  call mesh % init()

  mesh % mnode = 64
  mesh % ndime = 3
  mesh % npoin = 64
  mesh % nelem = 1

  call mesh % alloca()

  mesh % ltype      = HEX64

  mesh % lnods(:,1) = [ &
      1 ,   2 ,  3 ,  4 ,  5 ,  6 ,  7 ,  8 ,  9 ,  10,  11,  12,  13, &
      14,   15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25, &
      26,   27,  28,  29,  30,  31,  32,  33,  34,  35,  36,  37, &
      38,   39,  40,  41,  42,  43,  44,  45,  46,  47,  48,  49, &
      50,   51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61, &
      62,   63,  64 ]

       mesh % coord(1:3, 1) = (/ -1.0_rp, -1.0_rp, -1.0_rp /)
       mesh % coord(1:3, 2) = (/  1.0_rp, -1.0_rp, -1.0_rp /)    
       mesh % coord(1:3, 3) = (/  1.0_rp,  1.0_rp, -1.0_rp /)    
       mesh % coord(1:3, 4) = (/ -1.0_rp,  1.0_rp, -1.0_rp /)    
       mesh % coord(1:3, 5) = (/ -1.0_rp, -1.0_rp,  1.0_rp /)    
       mesh % coord(1:3, 6) = (/  1.0_rp, -1.0_rp,  1.0_rp /)    
       mesh % coord(1:3, 7) = (/  1.0_rp,  1.0_rp,  1.0_rp /)    
       mesh % coord(1:3, 8) = (/ -1.0_rp,  1.0_rp,  1.0_rp /)    
       mesh % coord(1:3, 9) = (/   -one3, -1.0_rp, -1.0_rp /)    
       mesh % coord(1:3,10) = (/    one3, -1.0_rp, -1.0_rp /)    
       mesh % coord(1:3,11) = (/  1.0_rp,   -one3, -1.0_rp /)    
       mesh % coord(1:3,12) = (/  1.0_rp,    one3, -1.0_rp /)    
       mesh % coord(1:3,13) = (/    one3,  1.0_rp, -1.0_rp /)
       mesh % coord(1:3,14) = (/   -one3,  1.0_rp, -1.0_rp /)    
       mesh % coord(1:3,15) = (/ -1.0_rp,    one3, -1.0_rp /)    
       mesh % coord(1:3,16) = (/ -1.0_rp,   -one3, -1.0_rp /)    
       mesh % coord(1:3,17) = (/ -1.0_rp, -1.0_rp,   -one3 /)      
       mesh % coord(1:3,18) = (/  1.0_rp, -1.0_rp,   -one3 /)    
       mesh % coord(1:3,19) = (/  1.0_rp,  1.0_rp,   -one3 /)    
       mesh % coord(1:3,20) = (/ -1.0_rp,  1.0_rp,   -one3 /)    
       mesh % coord(1:3,21) = (/ -1.0_rp, -1.0_rp,    one3 /)
       mesh % coord(1:3,22) = (/  1.0_rp, -1.0_rp,    one3 /)    
       mesh % coord(1:3,23) = (/  1.0_rp,  1.0_rp,    one3 /) 
       mesh % coord(1:3,24) = (/ -1.0_rp,  1.0_rp,    one3 /)    
       mesh % coord(1:3,25) = (/   -one3, -1.0_rp,  1.0_rp /)    
       mesh % coord(1:3,26) = (/    one3, -1.0_rp,  1.0_rp /)    
       mesh % coord(1:3,27) = (/  1.0_rp,   -one3,  1.0_rp /)    
       mesh % coord(1:3,28) = (/  1.0_rp,    one3,  1.0_rp /)    
       mesh % coord(1:3,29) = (/    one3,  1.0_rp,  1.0_rp /)    
       mesh % coord(1:3,30) = (/   -one3,  1.0_rp,  1.0_rp /)    
       mesh % coord(1:3,31) = (/ -1.0_rp,    one3,  1.0_rp /)    
       mesh % coord(1:3,32) = (/ -1.0_rp,   -one3,  1.0_rp /)    
       mesh % coord(1:3,33) = (/   -one3,   -one3, -1.0_rp /)   
       mesh % coord(1:3,34) = (/    one3,   -one3, -1.0_rp /)    
       mesh % coord(1:3,35) = (/    one3,    one3, -1.0_rp /)    
       mesh % coord(1:3,36) = (/   -one3,    one3, -1.0_rp /)    
       mesh % coord(1:3,37) = (/   -one3, -1.0_rp,   -one3 /)    
       mesh % coord(1:3,38) = (/    one3, -1.0_rp,   -one3 /)    
       mesh % coord(1:3,39) = (/  1.0_rp,   -one3,   -one3 /)    
       mesh % coord(1:3,40) = (/  1.0_rp,    one3,   -one3 /)    
       mesh % coord(1:3,41) = (/    one3,  1.0_rp,   -one3 /)    
       mesh % coord(1:3,42) = (/   -one3,  1.0_rp,   -one3 /)    
       mesh % coord(1:3,43) = (/ -1.0_rp,    one3,   -one3 /)      
       mesh % coord(1:3,44) = (/ -1.0_rp,   -one3,   -one3 /)
       mesh % coord(1:3,45) = (/   -one3, -1.0_rp,    one3 /)    
       mesh % coord(1:3,46) = (/    one3, -1.0_rp,    one3 /)    
       mesh % coord(1:3,47) = (/  1.0_rp,   -one3,    one3 /)    
       mesh % coord(1:3,48) = (/  1.0_rp,    one3,    one3 /)    
       mesh % coord(1:3,49) = (/    one3,  1.0_rp,    one3 /)    
       mesh % coord(1:3,50) = (/   -one3,  1.0_rp,    one3 /)           
       mesh % coord(1:3,51) = (/ -1.0_rp,    one3,    one3 /)    
       mesh % coord(1:3,52) = (/ -1.0_rp,   -one3,    one3 /)
       mesh % coord(1:3,53) = (/   -one3,   -one3,  1.0_rp /)    
       mesh % coord(1:3,54) = (/    one3,   -one3,  1.0_rp /)    
       mesh % coord(1:3,55) = (/    one3,    one3,  1.0_rp /)    
       mesh % coord(1:3,56) = (/   -one3,    one3,  1.0_rp /)    
       mesh % coord(1:3,57) = (/   -one3,   -one3,   -one3 /)
       mesh % coord(1:3,58) = (/    one3,   -one3,   -one3 /)    
       mesh % coord(1:3,59) = (/    one3,    one3,   -one3 /)    
       mesh % coord(1:3,60) = (/   -one3,    one3,   -one3 /)    
       mesh % coord(1:3,61) = (/   -one3,   -one3,    one3 /)    
       mesh % coord(1:3,62) = (/    one3,   -one3,    one3 /)    
       mesh % coord(1:3,63) = (/    one3,    one3,    one3 /)    
       mesh % coord(1:3,64) = (/   -one3,    one3,    one3 /)    

!!$  mesh % coord(:, 1) = [ 0.0_rp , 0.0_rp , 0.0_rp ]
!!$  mesh % coord(:, 2) = [ 6.2831853_rp , 0.0_rp , 0.0_rp ]
!!$  mesh % coord(:, 3) = [  6.2831853_rp , 6.2831853_rp , 0.0_rp ]
!!$  mesh % coord(:, 4) = [  0.0_rp , 6.2831853_rp , 0.0_rp ]
!!$  mesh % coord(:, 5) = [  0.0_rp , 0.0_rp , 6.2831853_rp ]
!!$  mesh % coord(:, 6) = [  6.2831853_rp , 0.0_rp , 6.2831853_rp ]
!!$  mesh % coord(:, 7) = [  6.2831853_rp , 6.2831853_rp , 6.2831853_rp ]
!!$  mesh % coord(:, 8) = [  0.0_rp , 6.2831853_rp , 6.2831853_rp ]
!!$  mesh % coord(:, 9) = [  2.094395099995039_rp , 0.0_rp , 0.0_rp ]
!!$  mesh % coord(:,10) = [ 4.18879019999396_rp , 0.0_rp , 0.0_rp ]
!!$  mesh % coord(:,11) = [ 6.2831853_rp , 2.094395099995039_rp , 0.0_rp ]
!!$  mesh % coord(:,12) = [ 6.2831853_rp , 4.18879019999396_rp , 0.0_rp ]
!!$  mesh % coord(:,13) = [ 4.188790200006006_rp , 6.2831853_rp , 0.0_rp ]
!!$  mesh % coord(:,14) = [ 2.094395100006563_rp , 6.2831853_rp , 0.0_rp ]
!!$  mesh % coord(:,15) = [ 0.0_rp , 4.188790200006006_rp , 0.0_rp ]
!!$  mesh % coord(:,16) = [ 0.0_rp , 2.094395100006563_rp , 0.0_rp ]
!!$  mesh % coord(:,17) = [ 2.094395099995039_rp , 0.0_rp , 6.2831853_rp ]
!!$  mesh % coord(:,18) = [ 4.18879019999396_rp , 0.0_rp , 6.2831853_rp ]
!!$  mesh % coord(:,19) = [ 6.2831853_rp , 2.094395099995039_rp , 6.2831853_rp ]
!!$  mesh % coord(:,20) = [ 6.2831853_rp , 4.18879019999396_rp , 6.2831853_rp ]
!!$  mesh % coord(:,21) = [ 4.188790200006006_rp , 6.2831853_rp , 6.2831853_rp ]
!!$  mesh % coord(:,22) = [ 2.094395100006563_rp , 6.2831853_rp , 6.2831853_rp ]
!!$  mesh % coord(:,23) = [ 0.0_rp , 4.188790200006006_rp , 6.2831853_rp ]
!!$  mesh % coord(:,24) = [ 0.0_rp , 2.094395100006563_rp , 6.2831853_rp ]
!!$  mesh % coord(:,25) = [ 0.0_rp , 0.0_rp , 2.094395099995039_rp ]
!!$  mesh % coord(:,26) = [ 0.0_rp , 0.0_rp , 4.18879019999396_rp ]
!!$  mesh % coord(:,27) = [ 6.2831853_rp , 0.0_rp , 2.094395099995039_rp ]
!!$  mesh % coord(:,28) = [ 6.2831853_rp , 0.0_rp , 4.18879019999396_rp ]
!!$  mesh % coord(:,29) = [ 6.2831853_rp , 6.2831853_rp , 2.094395099995039_rp ]
!!$  mesh % coord(:,30) = [ 6.2831853_rp , 6.2831853_rp , 4.18879019999396_rp ]
!!$  mesh % coord(:,31) = [ 0.0_rp , 6.2831853_rp , 2.094395099995039_rp ]
!!$  mesh % coord(:,32) = [ 0.0_rp , 6.2831853_rp , 4.18879019999396_rp ]
!!$  mesh % coord(:,33) = [ 2.094395099998879_rp , 2.094395100002722_rp , 0.0_rp ]
!!$  mesh % coord(:,34) = [ 4.188790199997975_rp , 2.09439509999888_rp , 0.0_rp ]
!!$  mesh % coord(:,35) = [ 4.188790200001991_rp , 4.188790199997975_rp , 0.0_rp ]
!!$  mesh % coord(:,36) = [ 2.094395100002721_rp , 4.18879020000199_rp , 0.0_rp ]
!!$  mesh % coord(:,37) = [ 2.094395099995038_rp , 0.0_rp , 2.094395099995038_rp ]
!!$  mesh % coord(:,38) = [ 4.18879019999396_rp , 0.0_rp , 2.094395099995038_rp ]
!!$  mesh % coord(:,39) = [ 4.18879019999396_rp , 0.0_rp , 4.18879019999396_rp ] 
!!$  mesh % coord(:,40) = [ 2.094395099995038_rp , 0.0_rp , 4.18879019999396_rp ]
!!$  mesh % coord(:,41) = [ 6.2831853_rp , 2.094395099995038_rp , 2.094395099995038_rp ]
!!$  mesh % coord(:,42) = [ 6.2831853_rp , 4.18879019999396_rp , 2.094395099995038_rp ]
!!$  mesh % coord(:,43) = [ 6.2831853_rp , 4.18879019999396_rp , 4.18879019999396_rp ]
!!$  mesh % coord(:,44) = [ 6.2831853_rp , 2.094395099995038_rp , 4.18879019999396_rp ]
!!$  mesh % coord(:,45) = [ 4.188790200006006_rp , 6.2831853_rp , 2.094395099995038_rp ]
!!$  mesh % coord(:,46) = [ 2.094395100006563_rp , 6.2831853_rp , 2.094395099995038_rp ]
!!$  mesh % coord(:,47) = [ 2.094395100006563_rp , 6.2831853_rp , 4.18879019999396_rp ]
!!$  mesh % coord(:,48) = [ 4.188790200006006_rp , 6.2831853_rp , 4.18879019999396_rp ]
!!$  mesh % coord(:,49) = [ 0.0_rp , 4.188790200006006_rp , 2.094395099995038_rp ]
!!$  mesh % coord(:,50) = [ 0.0_rp , 2.094395100006563_rp , 2.094395099995038_rp ]
!!$  mesh % coord(:,51) = [ 0.0_rp , 2.094395100006563_rp , 4.18879019999396_rp ]
!!$  mesh % coord(:,52) = [ 0.0_rp , 4.188790200006006_rp , 4.18879019999396_rp ]
!!$  mesh % coord(:,53) = [ 2.094395099998879_rp , 2.094395100002722_rp , 6.2831853_rp ]
!!$  mesh % coord(:,54) = [ 4.188790199997975_rp , 2.09439509999888_rp , 6.283185299999998_rp ]
!!$  mesh % coord(:,55) = [ 4.188790200001991_rp , 4.188790199997975_rp , 6.283185299999998_rp ]
!!$  mesh % coord(:,56) = [ 2.094395100002721_rp , 4.18879020000199_rp , 6.283185299999999_rp ]
!!$  mesh % coord(:,57) = [ 2.094395099998879_rp , 2.094395100002722_rp , 2.094395099995037_rp ]
!!$  mesh % coord(:,58) = [ 4.188790199997972_rp , 2.094395099998879_rp , 2.094395099995037_rp ]
!!$  mesh % coord(:,59) = [ 4.188790200001991_rp , 4.188790199997974_rp , 2.094395099995036_rp ]
!!$  mesh % coord(:,60) = [ 2.094395100002722_rp , 4.188790200001988_rp , 2.094395099995037_rp ]
!!$  mesh % coord(:,61) = [ 2.094395099998879_rp , 2.094395100002721_rp , 4.188790199993957_rp ]
!!$  mesh % coord(:,62) = [ 4.188790199997975_rp , 2.094395099998879_rp , 4.188790199993957_rp ]
!!$  mesh % coord(:,63) = [ 4.188790200001989_rp , 4.188790199997976_rp , 4.188790199993957_rp ]
!!$  mesh % coord(:,64) = [ 2.09439510000272_rp ,  4.188790200001987_rp , 4.188790199993955_rp ]
  !
  ! Define interpolation and quadrature rule
  !
  ielty                      = HEX64
  mesh % quad(ielty) % ngaus = element_type(ielty) % number_nodes
  mesh % quad(ielty) % type  = CHEBYSHEV_RULE
  mesh % quad(ielty) % ndime = element_type(ielty) % dimensions
  mesh % iso(ielty)  % inter = CHEBYSHEV_INTERPOLATION 
  call mesh % quad(ielty) % set()
  call mesh % iso(ielty)  % set(element_type(ielty) % number_nodes,mesh % quad(ielty))
  !
  ! Change mesh coordinates
  !
  ielty = HEX64
  pnode = 64
  pgaus = 64
  pdime = mesh % ndime

  !write(91,*) mesh % lnods
  !flush(91)
  !end do
  !stop

  !
  ! Transform coordinates
  !
  do igaus = 1,pgaus
     call caca(pnode,3_ip,N(:,igaus),mesh % quad(ielty) % posgp(1:3,igaus))
  end do
  do ielem = 1,mesh % nelem
     ielty = mesh % ltype(ielem)
     pnode = element_type(ielty) % number_nodes
     elcod(1:pdime,1:pnode) = mesh % coord(1:pdime,mesh % lnods(1:pnode,ielem))
     do igaus = 1,pnode
        do idime = 1,pdime
           call var_interpolate(pnode,elcod(idime,1:pnode),N(:,igaus),mesh % coord(idime,mesh % lnods(igaus,ielem)))
        end do
     end do
     !do ipoin = 1,mesh % npoin
     !write(91,*) elcod
     !write(91,*) ipoin,mesh % coord(:,11)
     !flush(91)
     !end do
     !stop
  end do
  !
  ! Check shape functions
  !
  do inode = 1,pnode
     do igaus = 1,pgaus
        if(inode==igaus.and.abs(mesh % iso(ielty) % shape(inode,igaus)-1.0_rp)>1.0e-06_rp) then
           print*,'shape=',mesh % iso(ielty) % shape(inode,igaus)
           stop 1
        else if(inode/=igaus.and.abs(mesh % iso(ielty) % shape(inode,igaus)-0.0_rp)>1.0e-06_rp) then
           print*,'shape=',mesh % iso(ielty) % shape(inode,igaus)
           stop 2
        end if
     end do
  end do
  
  print*,'after=',mesh % coord(:,21)
  call mesh % output(filename='hex64')
  !
  ! Compute volume and gradients
  !
  allocate(gpsha(pnode,pgaus))        ! N
  allocate(gpder(pdime,pnode,pgaus))  ! dN/dsi
  allocate(gpcar(pdime,pnode,pgaus))  ! dN/dxi
  allocate(gpvol(pgaus))              ! w*|J|, |J|

  allocate(x(pdime,mesh % npoin))
  do ipoin = 1,mesh % npoin
     x(1,ipoin) = 2.0_rp*mesh % coord(1,ipoin) + 3.0_rp*mesh % coord(2,ipoin) +  4.0_rp*mesh % coord(3,ipoin)
     x(2,ipoin) = 5.0_rp*mesh % coord(1,ipoin) + 6.0_rp*mesh % coord(2,ipoin) +  7.0_rp*mesh % coord(3,ipoin)
     x(3,ipoin) = 8.0_rp*mesh % coord(1,ipoin) + 9.0_rp*mesh % coord(2,ipoin) + 10.0_rp*mesh % coord(3,ipoin)
  end do

  vol = 0.0_rp
  do ielem = 1,mesh % nelem
     elcod(1:pdime,1:pnode) = mesh % coord(1:pdime,mesh % lnods(1:pnode,ielem))
     call element_shape_function_derivatives_jacobian(&
          pnode,pgaus,0_ip,mesh % quad(ielty) % weigp,mesh % iso(ielty) % shape,&
          mesh % iso(ielty) % deriv,mesh % iso(ielty) % heslo,&
          elcod,gpvol,gpsha,gpder,gpcar)
     do igaus = 1,pgaus
        vol  = vol + gpvol(igaus)
        ! du_i/dx_p
        grad = 0.0_rp
        do inode = 1,pnode
           do idime = 1,pdime
              grad(1:pdime,idime) = grad(1:pdime,idime) + gpcar(1:pdime,inode,igaus) * x(idime,mesh % lnods(inode,ielem))
           end do
        end do
        if( abs(grad(1,1)- 2.0_rp) > 1.0e-06_rp ) stop 10
        if( abs(grad(2,1)- 3.0_rp) > 1.0e-06_rp ) stop 11
        if( abs(grad(3,1)- 4.0_rp) > 1.0e-06_rp ) stop 12
        if( abs(grad(1,2)- 5.0_rp) > 1.0e-06_rp ) stop 13
        if( abs(grad(2,2)- 6.0_rp) > 1.0e-06_rp ) stop 14
        if( abs(grad(3,2)- 7.0_rp) > 1.0e-06_rp ) stop 15
        if( abs(grad(1,3)- 8.0_rp) > 1.0e-06_rp ) stop 16
        if( abs(grad(2,3)- 9.0_rp) > 1.0e-06_rp ) stop 17
        if( abs(grad(3,3)-10.0_rp) > 1.0e-06_rp ) stop 18
     end do
  end do

  if( abs(vol-8.0_rp) > 1.0e-06_rp ) then
     print*,'wrong volume=',vol
     stop 3
  end if
  print*,'volume= ',vol
  
contains

  subroutine caca(nnode,porder,N,x)
    implicit none

    integer(ip),           intent(in)  :: nnode
    integer(ip),           intent(in)  :: porder
    real(rp),              intent(in)  :: x(3)
    real(rp),    optional, intent(out) :: N(nnode)
    real(rp)                           :: dN(3,nnode)
    real(rp)                           :: xi_grid(porder+1)

    call lagrange_roots(porder,xi_grid)
    call tripleTensorProduct(nnode,porder,xi_grid,x(1),x(2),x(3),N,dN)

  end subroutine caca

end program unitt_chebyshev
