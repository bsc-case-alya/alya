#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import sys
import os
meshName=sys.argv[1]
boufile='{}.fix.bou'.format(meshName)
realbou='{}.fix.bou.real'.format(meshName)


real_boundaries=[]
for i,arg in enumerate(sys.argv):
    if i > 1:
        real_boundaries.append(int(arg))

print('Real boundaries to leave in {}: {}'.format(realbou, real_boundaries))


f=open(boufile,'r')
g=open(realbou,'w')

for line in f:
    data = line.split()
    if len(data)>1:
        if int(data[1]) in real_boundaries:
            g.write(line)

f.close()
g.close()



