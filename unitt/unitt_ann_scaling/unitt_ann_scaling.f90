!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_ann_scaling
  use def_kintyp_basic,   only : ip,rp
  use mod_ann_scaling,    only : ann_scaling
  use mod_ann_scaling,    only : ANN_SCALING_UNSCALED
  use mod_ann_scaling,    only : ANN_SCALING_LINEAR

  
  type(ann_scaling)            :: scaling    
  type(ann_scaling)            :: scaling2    
  type(ann_scaling)            :: scaling3    
  integer(ip)                  :: shap(3) 
  integer(ip)                  :: res(5,10,2)
  real(rp)                     :: unscaled_r1(3), scaled_r1(3)
  real(rp)                     :: unscaled_r2(10,3), scaled_r2(10,3)

  !
  ! Initialize
  !
  call scaling % init()
  print*, "n_dim ";        if( abs( scaling % n_dim  - 0.0_rp ) > 1.0e-3_rp ) stop 1
  print*, "n_prod_shape "; if( abs( scaling % n_prod_shape  - 0.0_rp ) > 1.0e-3_rp ) stop 1
  print*, "shape ";        if( associated(scaling % shape) ) stop 1
  print*, "types ";        if( associated(scaling % types) ) stop 1

  !
  ! Set n_dim
  !
  call scaling % set(n_dim = 3_ip)
  print*, "n_dim ";        if( abs( scaling % n_dim  - 3.0_rp ) > 1.0e-3_rp ) stop 1

  !
  ! Allocate shape
  !
  call scaling % alloc_shape()
  print*, "shape ";        if( .not. associated(scaling % shape) ) stop 1

  !
  ! Set shape 
  !
  call scaling % set(i_shape = 1_ip, n_shape=5_ip)
  call scaling % set(i_shape = 2_ip, n_shape=10_ip)
  call scaling % set(i_shape = 3_ip, n_shape=2_ip)
  print*, "n_prod_shape "; if( abs( scaling % n_prod_shape  - 100.0_rp ) > 1.0e-3_rp ) stop 1
  print*, "shape1 ";        if( abs( scaling % shape(1)  - 5.0_rp ) > 1.0e-3_rp ) stop 1
  print*, "shape2 ";        if( abs( scaling % shape(2)  - 10.0_rp ) > 1.0e-3_rp ) stop 1
  print*, "shape3 ";        if( abs( scaling % shape(3)  - 2.0_rp ) > 1.0e-3_rp ) stop 1

  !
  ! Allocate 
  !
  call scaling % alloc()
  print*, "types ";        if( .not. associated(scaling % types) ) stop 1

  !
  ! Set elements
  !
  call scaling % set( inds=(/ 1_ip,1_ip,1_ip /), typ=1_ip)
  call scaling % set( inds=(/ 5_ip,1_ip,1_ip /), typ=5_ip)
  call scaling % set( inds=(/ 1_ip,1_ip,2_ip /), typ=51_ip)
  print*, "types1 ";        if( abs( scaling % types(1)  - 1.0_rp ) > 1.0e-3_rp ) stop 1
  print*, "types5 ";        if( abs( scaling % types(5)  - 5.0_rp ) > 1.0e-3_rp ) stop 1
  print*, "types51 ";        if( abs( scaling % types(51)  - 51.0_rp ) > 1.0e-3_rp ) stop 1
  
  shap = scaling % shape
  res = reshape(scaling % types, shap)
  print*, "types51/2 ";        if( abs( res(1,1,2)  - 51.0_rp ) > 1.0e-3_rp ) stop 1
  print*, "all_types ";        if( abs( scaling % all_types - 0.0_rp ) > 1.0e-3_rp ) stop 1

  !
  ! Test inds2i 
  !
  print*, "inds2i 5,5,2 ";        if( abs( scaling % inds2i((/ 5_ip,5_ip,2_ip /)) - 75.0_rp ) > 1.0e-3_rp ) stop 1


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! A more realistic 1D scaling !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  call scaling2 % init()
  call scaling2 % set(n_dim = 1_ip)
  call scaling2 % alloc_shape()
  call scaling2 % set(i_shape = 1_ip, n_shape=3_ip)
  call scaling2 % alloc()
  call scaling2 % set( inds=(/ 1_ip /), typ=ANN_SCALING_LINEAR)
  call scaling2 % set( inds=(/ 1_ip /), coeff = 2.0_rp)
  call scaling2 % set( inds=(/ 1_ip /), shift = -1.0_rp)
  call scaling2 % set( inds=(/ 2_ip /), typ=ANN_SCALING_LINEAR)
  call scaling2 % set( inds=(/ 2_ip /), coeff = 1.0_rp)
  call scaling2 % set( inds=(/ 2_ip /), shift = -1.0_rp)
  call scaling2 % set( inds=(/ 3_ip /), typ=ANN_SCALING_LINEAR)
  call scaling2 % set( inds=(/ 3_ip /), coeff = 2.0_rp)
  call scaling2 % set( inds=(/ 3_ip /), shift = 0.0_rp)
  
  print*, "all_types ";        if( abs( scaling2 % all_types - ANN_SCALING_LINEAR ) > 1.0e-3_rp ) stop 1

  !
  ! Test scaling 1D array
  !
  unscaled_r1 = 1.0
  call scaling2 % scale( unscaled_r1, scaled_r1 )
  print*, "scaled_r1 ", scaled_r1
  print*, "scaled_r1 ";        if( abs( scaled_r1(1) - 1.0 ) > 1.0e-3_rp ) stop 1
  print*, "scaled_r1 ";        if( abs( scaled_r1(2) - 0.0 ) > 1.0e-3_rp ) stop 1
  print*, "scaled_r1 ";        if( abs( scaled_r1(3) - 2.0 ) > 1.0e-3_rp ) stop 1

  !
  ! Test unscaling 1D array
  !
  unscaled_r1 = 0.0
  call scaling2 % unscale( scaled_r1, unscaled_r1 )
  print*, "unscaled_r1 ", unscaled_r1
  print*, "unscaled_r1 ";        if( abs( unscaled_r1(1) - 1.0 ) > 1.0e-3_rp ) stop 1
  print*, "unscaled_r1 ";        if( abs( unscaled_r1(2) - 1.0 ) > 1.0e-3_rp ) stop 1
  print*, "unscaled_r1 ";        if( abs( unscaled_r1(3) - 1.0 ) > 1.0e-3_rp ) stop 1


  !
  ! Test scaling 2D array
  !
  unscaled_r2 = 1.0
  unscaled_r2(1,:) = 1.0
  unscaled_r2(2,:) = 0.0
  unscaled_r2(3,:) = -1.0
  call scaling2 % scale( 10_ip, unscaled_r2, scaled_r2 )
  print*, "scaled_r2 ", scaled_r2
  print*, "scaled_r2 ";        if( abs( scaled_r2(1,1) - 1.0 ) > 1.0e-3_rp ) stop 1
  print*, "scaled_r2 ";        if( abs( scaled_r2(1,2) - 0.0 ) > 1.0e-3_rp ) stop 1
  print*, "scaled_r2 ";        if( abs( scaled_r2(1,3) - 2.0 ) > 1.0e-3_rp ) stop 1
  print*, "scaled_r2 ";        if( abs( scaled_r2(2,1) - (-1.0) ) > 1.0e-3_rp ) stop 1
  print*, "scaled_r2 ";        if( abs( scaled_r2(2,2) - (-1.0) ) > 1.0e-3_rp ) stop 1
  print*, "scaled_r2 ";        if( abs( scaled_r2(2,3) - 0.0 ) > 1.0e-3_rp ) stop 1
  print*, "scaled_r2 ";        if( abs( scaled_r2(3,1) - (-3.0) ) > 1.0e-3_rp ) stop 1
  print*, "scaled_r2 ";        if( abs( scaled_r2(3,2) - (-2.0) ) > 1.0e-3_rp ) stop 1
  print*, "scaled_r2 ";        if( abs( scaled_r2(3,3) - (-2.0) ) > 1.0e-3_rp ) stop 1

  !
  ! Test unscaling 2D array
  !
  unscaled_r2 = -666.0
  call scaling2 % unscale( 10_ip, scaled_r2, unscaled_r2 )
  print*, "unscaled_r2 ", unscaled_r2
  print*, "unscaled_r2 ";        if( abs( unscaled_r2(1,1) - 1.0 ) > 1.0e-3_rp ) stop 1
  print*, "unscaled_r2 ";        if( abs( unscaled_r2(1,2) - 1.0 ) > 1.0e-3_rp ) stop 1
  print*, "unscaled_r2 ";        if( abs( unscaled_r2(1,3) - 1.0 ) > 1.0e-3_rp ) stop 1
  print*, "unscaled_r2 ";        if( abs( unscaled_r2(2,1) - 0.0 ) > 1.0e-3_rp ) stop 1
  print*, "unscaled_r2 ";        if( abs( unscaled_r2(2,2) - 0.0 ) > 1.0e-3_rp ) stop 1
  print*, "unscaled_r2 ";        if( abs( unscaled_r2(2,3) - 0.0 ) > 1.0e-3_rp ) stop 1
  print*, "unscaled_r2 ";        if( abs( unscaled_r2(3,1) - (-1.0) ) > 1.0e-3_rp ) stop 1
  print*, "unscaled_r2 ";        if( abs( unscaled_r2(3,2) - (-1.0) ) > 1.0e-3_rp ) stop 1
  print*, "unscaled_r2 ";        if( abs( unscaled_r2(3,3) - (-1.0) ) > 1.0e-3_rp ) stop 1


  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Add a scaling that does nothing !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  call scaling3 % init()
  call scaling3 % set(n_dim = 1_ip)
  call scaling3 % alloc_shape()
  call scaling3 % set(i_shape = 1_ip, n_shape=1_ip)
  call scaling3 % alloc()
  call scaling3 % set( inds=(/ 1_ip /), typ=ANN_SCALING_UNSCALED)
  print*, "all_types ";        if( abs( scaling3 % all_types - ANN_SCALING_UNSCALED ) > 1.0e-3_rp ) stop 1

  !
  ! Scalings
  !
  unscaled_r1 = 1.0
  call scaling3 % scale( unscaled_r1(1:1), scaled_r1(1:1) )
  print*, "scaled_r1 ", scaled_r1(1:1)
  print*, "scaled_r1 ";        if( abs( scaled_r1(1) - 1.0 ) > 1.0e-3_rp ) stop 1
  unscaled_r1 = 1.0
  call scaling3 % unscale( scaled_r1(1:1), unscaled_r1(1:1) )
  print*, "unscaled_r1 ", unscaled_r1(1:1)
  print*, "unscaled_r1 ";        if( abs( unscaled_r1(1) - 1.0 ) > 1.0e-3_rp ) stop 1

  !
  ! Test scaling 2D array
  !
  unscaled_r2 = 1.0
  call scaling3 % scale( 10_ip, unscaled_r2(:,1:1), scaled_r2(:,1:1) )
  print*, "scaled_r2 ", scaled_r2(:,1:1)
  print*, "scaled_r2 ";        if( abs( scaled_r2(1,1) - 1.0 ) > 1.0e-3_rp ) stop 1
  unscaled_r2 = -666.0
  call scaling3 % unscale( 10_ip, scaled_r2(:,1:1), unscaled_r2(:,1:1) )
  print*, "unscaled_r2 ", unscaled_r2(:,1:1)
  print*, "unscaled_r2 ";        if( abs( unscaled_r2(1,1) - 1.0 ) > 1.0e-3_rp ) stop 1

end program unitt_ann_scaling

