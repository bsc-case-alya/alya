!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine arrinb(&
     nlist,ncoef,liste,touch,nzbou,ipoin)
  !-----------------------------------------------------------------------
  !                 
  ! This routine constructs the arrays of indexes for a mesh graph.
  ! These are organized as follows (CSR format):
  !   
  ! R_BOU(IPOIN) = coefficient of the graph where IPOIN starts,
  ! C_BOU(NZBOU) = column of the NZBOU coefficient of the graph.
  !              
  !-----------------------------------------------------------------------
  use def_kintyp, only       :  ip,rp,lg
  use def_domain, only       :  mnodb,nnode,lnodb,ltypb,r_bou,c_bou
  implicit none
  integer(ip), intent(in)    :: nlist
  integer(ip), intent(in)    :: ncoef,ipoin
  integer(ip), intent(in)    :: liste(nlist)
  integer(ip), intent(inout) :: nzbou
  logical(lg), intent(inout) :: touch(ncoef)
  integer(ip)                :: jboun,jnode,jpoin,nnodj,jposi,jlist
  integer(ip)                :: kboun,knode,kpoin,nnodk,kposi,klist

  r_bou(ipoin) = nzbou

  do jlist = 1,nlist
     jboun = liste(jlist)
     nnodj = nnode(ltypb(jboun))
     do jnode = 1,nnodj
        jpoin = lnodb(jnode,jboun)
        jposi = (jlist-1)*mnodb+jnode
        if(.not.touch(jposi)) then
           do klist = 1,nlist
              kboun = liste(klist)
              nnodk = nnode(ltypb(kboun))
              do knode = 1,nnodk
                 kpoin = lnodb(knode,kboun)
                 if(kpoin==jpoin) then
                    kposi = (klist-1)*mnodb+knode
                    touch(kposi) = .true.
                 end if
              end do
           end do
           c_bou(nzbou) = jpoin
           nzbou = nzbou+1
        end if
     end do
  end do

end subroutine arrinb
