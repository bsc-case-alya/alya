!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine pararx(wtask,ntype,ndimx,rvarx)
  !------------------------------------------------------------------------
  !****f* Parall/pararx
  ! NAME
  !    pararr
  ! DESCRIPTION
  !    Works with arrays to deal with parallelization
  ! OUTPUT
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only     :  ip,rp
  use def_master, only     :  npari,nparr,nparc,parin,parre,parx1,kfl_paral
  use def_master, only     :  party,parki,pardi,IPARALL,pard1
  use def_master, only     :  NPOIN_TYPE,NBOPO_TYPE,lntra
  use def_domain, only     :  npoin,nbopo
  use mod_communications, only  :  PAR_INTERFACE_NODE_EXCHANGE
  implicit none
  character(3), intent(in) :: wtask
  integer(ip),  intent(in) :: ntype,ndimx
  complex(rp),  target     :: rvarx(ndimx)

  if( IPARALL ) then

     npari = 0
     nparc = 0
     nparr = 0

     select case ( wtask )

     case ( 'SLX' )
        !
        ! par_slexch
        !
        if( ntype == NPOIN_TYPE .or. ntype == NBOPO_TYPE ) then
           call runend('EXHCNAGE FOR COMPLEX SHOULD BE CODED')
           !call PAR_INTERFACE_NODE_EXCHANGE(pard1,rvarx)
        else
           call runend('PARARX: NOT CODED')
        end if

     case default

        call runend('PARARX: WRONG CASE')

     end select

     !nparr = 0

  end if

end subroutine pararx
 
