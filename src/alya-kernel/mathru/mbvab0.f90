!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine mbvab0(a,b,c,n1,n2)

!------------------------------------------------------------------------
!
! This routine evaluates the matrix-vector product A = B C, where
! A -> R(n1), B -> Mat(n1,n2), C -> R(n2)
!
!------------------------------------------------------------------------
  use      def_kintyp
  implicit none
  integer(ip), intent(in)  :: n1,n2
  real(rp),    intent(in)  :: b(n1,n2),c(n2)
  real(rp),    intent(out) :: a(n1)
  integer(ip)              :: i,j

  do i=1,n1
     a(i)=0.0_rp
     do j=1,n2
        a(i)=a(i)+b(i,j)*c(j)
     end do
  end do
  
end subroutine mbvab0
