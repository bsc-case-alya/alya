!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_distance_segment_point 
  !
  ! Order strings
  !
  use def_kintyp_basic,   only : ip,rp
  use mod_maths_geometry, only : maths_distance_point_segment

  implicit none

  real(rp), parameter :: eps=epsilon(1.0_rp)
  character(10) :: wtest(4)
  integer(ip)   :: nn
  real(rp)      :: bocod(2,2),xx(2),d,pp(2)
  real(rp)      :: boco3(3,4),x3(3),p3(3)

  bocod(:,1) = (/ 1.0_rp,1.0_rp /)
  bocod(:,2) = (/ 2.0_rp,2.0_rp /)
  xx(:)      = (/ 2.0_rp,1.0_rp /)

  call maths_distance_point_segment(bocod(:,1),bocod(:,2),xx,pp,d) 

  print*,'dista=',d
  print*,'proje=',pp

  boco3(:,1) = (/ 1.0_rp,1.0_rp,1.0_rp /)
  boco3(:,2) = (/ 2.0_rp,2.0_rp,1.0_rp /)
  x3(:)      = (/ 2.0_rp,1.0_rp,1.0_rp /)
  
  call maths_distance_point_segment(boco3(:,1),boco3(:,2),x3,p3,d) 

  print*,'dista=',d
  print*,'proje=',p3

  if( abs(p3(1)-1.5_rp) > eps ) stop 1
  if( abs(p3(2)-1.5_rp) > eps ) stop 1
  if( abs(p3(3)-1.0_rp) > eps ) stop 1

end program unitt_distance_segment_point
