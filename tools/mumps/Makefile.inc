LPORDDIR = $(topdir)/PORD/lib/
IPORD    = -I$(topdir)/PORD/include/
LPORD    = -L$(LPORDDIR) -lpord

# Metis
IMETIS      = -I/$(METIS_DIR)/include 
LMETIS      = -L/$(METIS_DIR)/lib -lmetis 

ORDERINGSF  = -Dpord -Dmetis
ORDERINGSC  = $(ORDERINGSF)

LORDERINGS = $(LMETIS) $(LPORD)
IORDERINGSF =
IORDERINGSC = $(IMETIS) $(IPORD) 

#End orderings
########################################################################
################################################################################

PLAT    =
LIBEXT  = .a
OUTC    = -o 
OUTF    = -o 
RM = /bin/rm -f
CC = mpiicc
FC = mpiifort
FL = mpiifort
AR = ar vr 
#RANLIB = ranlib
RANLIB  = echo
# Make this variable point to the path where the Intel MKL library is
# installed. It is set to the default install directory for Intel MKL.
MKLROOTBIS=$(MKLROOT)/lib/intel64
LAPACK = -L$(MKLROOTBIS) -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core
SCALAP = -L$(MKLROOTBIS) -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64

INCPAR = -I/apps/HWLOC/2.0.0/INTEL/include
LIBPAR = $(SCALAP) $(LAPACK) -L/apps/HWLOC/2.0.0/INTEL/lib -lhwloc

INCSEQ = -I$(topdir)/libseq
LIBSEQ  = $(LAPACK) -L$(topdir)/libseq -lmpiseq

LIBBLAS = -L$(MKLROOTBIS) -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core 
LIBOTHERS = -lpthread

#Preprocessor defs for calling Fortran from C (-DAdd_ or -DAdd__ or -DUPPER)
CDEFS   = -DAdd_

#Begin Optimized options
OPTF    = -O3 -nofor_main -DBLR_MT -qopenmp -DGEMMT_AVAILABLE  -DLRMUMPS  -Dl0omp   -DMPI_TO_K_OMP -DUSE_LIBHWLOC -DGEMMT_AVAILABLE -DANA_BLK -DPERFADVANCED -DDIST_RHS  -DLRCOMPRESSCB -DUSE_SCHEDAFFINITY
OPTL    = -O3 -nofor_main -qopenmp
OPTC    = -O3 -qopenmp  -DLRMUMPS -Ddev_version  -qopenmp -DBLR_MT -Dl0omp  -DMPI_TO_K_OMP -DUSE_LIBHWLOC -DANA_BLK -DPERFADVANCED -DDIST_RHS  -DLRCOMPRESSCB -DUSE_SCHEDAFFINITY
#End Optimized options

INCS = $(INCPAR)
LIBS = $(LIBPAR)
LIBSEQNEEDED =


