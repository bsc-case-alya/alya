!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!------------------------------------------------------------------------
!>
!> @defgroup Memory_Toolbox
!> @{
!> @name    ToolBox for memory management
!> @file    mod_memory.f90
!> @author  Guillaume Houzeaux
!> @brief   ToolBox for memory management
!> @details ToolBox for memory management
!>
!------------------------------------------------------------------------

module mod_memory

  use mod_memory_tools                        ! Tools for memory management
  use mod_memory_basic                        ! Memory for basic types defined in def_kintyp_basic.f90
#ifndef I_AM_NOT_ALYA
  use mod_memory_physics                      ! Memory for physics types defined in def_kintyp_physics.f90
  use mod_memory_parall                       ! Memory for parallelization arrays
  use mod_memory_spmat                        ! Memory for matrices
#endif

  implicit none

  private

  public :: memory_initialization             ! Initialization of the module
  public :: memory_initia                     ! Allocate memory
  public :: memory_alloca                     ! Allocate memory
  public :: memory_size                       ! Gives the size of a pointer (=0 if not associated)
  public :: memory_alloca_min                 ! Allocate a minimum memory for arrays
  public :: memory_deallo                     ! Deallocate memory
  public :: memory_copy                       ! Copy an array
  public :: memory_renumber                   ! Renumber an array
  public :: memory_resize                     ! Resize an array
  public :: memory_unit                       ! Memory scaling and units
  public :: memory_output_info                ! Info about memory
  public :: lbytm                             ! Allocated bytes
  public :: mem_curre                         ! Current memory allocation in bytes
  public :: mem_maxim                         ! Current memory allocation in bytes
  public :: kfl_memor                         ! Output
  public :: lun_memor                         ! Output unit
  public :: kfl_varcount                      ! Output
  public :: lun_varcount                      ! Variable memory counter unit
  public :: kfl_alloc                         ! Allocation mode
  public :: mem_alloc                         ! Number of allocation
  public :: Kbytes                            ! Memory units
  public :: Mbytes                            ! Memory units
  public :: Gbytes                            ! Memory units
  public :: memory_add_to_memory_counter      ! Add bytes to memory counter
  public :: memory_remove_from_memory_counter ! Add bytes to memory counter
  public :: memory_allocation_mode            ! Set allocation mode
  public :: memory_output_variable_counter    ! Output variable counter
  
end module mod_memory
!> @}

