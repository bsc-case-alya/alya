!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine cderda()
  !-----------------------------------------------------------------------
  !****f* Domain/cderda
  ! NAME
  !    cderda
  ! DESCRIPTION
  !    This routine defines the derivated parameters of the element
  ! OUTPUT
  !    NDIMB ... Boundary dimension
  !    NTENS ... # of independent Hessian matrix values
  !    NINER ... # of independent tensor of inertia values
  !    LRULE ... 1 Quad/Hexa:  open   
  !          ... 2 Quad/Hexa:  closed 
  !          ... 3 Tria/Tetra: open
  !          ... 4 Tria/Tetra: closed
  !          ... 5   - /Penta: open
  !          ... 6   - /Penta: closed
  !          ... 7   - /Pyram: open
  !          ... 8   - /Pyram: closed
  !    HNATU ... 2 Quad/Hexa
  !          ... 1 Others
  !    MNODE ... Maximum # of element nodes in the mesh
  !    MGAUS ... Maximum # of element Gauss points in the mesh
  !    MNODB ... Maximum # of boundary nodes in the mesh
  !    MGAUB ... Maximum # of boundary Gaus points in the mesh
  !    MLAPL ... 1 if at least one element needs Laplacian
  ! USED BY
  !    Domain
  !***
  !-----------------------------------------------------------------------
  use def_elmtyp
  use def_master
  use def_domain
  use def_elmtyp,        only : element_end
  use mod_memory,        only : memory_alloca
  use def_elmgeo,        only : element_type
  use def_quadrature,    only : GAUSS_LEGENDRE_RULE   
  use def_quadrature,    only : TRAPEZOIDAL_RULE      
  use def_quadrature,    only : CLOSED_RULE           
  use def_quadrature,    only : CHEBYSHEV_RULE           
  implicit none
  integer(ip) :: ielty,pface,pgaus,pquad,ndife
  !
  ! Where element and boundary element types start and stop
  ! We put IBSTA_DOM=1 to treat BARD3D elements, which boundary
  ! is the 1D POINT element
  !
  if( ndime == 1 ) then
     iesta_dom =  2
     iesto_dom =  9
     ibsta_dom =  1
     ibsto_dom =  1
  else if( ndime == 2 ) then
     iesta_dom = 10
     iesto_dom = 29
     ibsta_dom =  2
     ibsto_dom =  9
  else
     iesta_dom = 30
     iesto_dom = 60
     ibsta_dom = 10
     ibsto_dom = 29
  end if
  if( lexis(BAR3D) /= 0 ) ibsta_dom = 1
  !
  ! Gobal dimensions
  !
  ndimb = ndime-1
  if( ndime == 1 ) then
     ntens = 1
  else
     ntens = 3 * ndime - 3
  end if
  if( ndime == 3 ) then
     niner = 3
  else
     niner = 1
  end if
  !
  ! Compute LRULE, LINTE and HNATU
  ! 
  do ielty = 1,nelty

     select case ( lquad(ielty) ) 
     case ( GAUSS_LEGENDRE_RULE , CLOSED_RULE ) ; lrule(ielty) =  2*ltopo(ielty)+lquad(ielty)+1
     case ( TRAPEZOIDAL_RULE                  ) ; lrule(ielty) = -3
     case ( CHEBYSHEV_RULE                    ) ; lrule(ielty) = -4
     end select     
     if( ltopo(ielty) == -1 ) then
        hnatu(ielty) = 2.0_rp                     ! BAR
     else if( ltopo(ielty) == 0 ) then
        hnatu(ielty) = 2.0_rp                     ! QUA/HEX
     else  
        hnatu(ielty) = 1.0_rp                     ! OTHERS
     end if
  end do
  !
  ! Divide mesh
  !
  if( kfl_divid == 1 ) then
     ndife = 0
     do ielty = 1,nelty
        ndife = ndife + lexis(ielty)
     end do
     if( ndife /= 1 )        call runend('READIM: CANNOT DIVIDE MESH')
     if( lexis(HEX08) == 0 ) call runend('READIM: CAN ONLY DIVIDE HEXAHEDRA')
     lexis(TET04) = 1
  end if
  !
  ! If Volume Gauss points have not been assigned, put default option
  !
  do ielty = 1,nelty     
     if( lexis(ielty) /= 0 .and. ngaus(ielty) <= 0 ) then
        if( lquad(ielty) == 0 ) then
           !
           ! Open rule
           !
           if(      ielty == POINT ) then
              ngaus(ielty) = 1
           else if( ielty == BAR04 ) then
              ngaus(ielty) = 11
           else if( ielty == QUA08 ) then
              ngaus(ielty) = 9
           else if( ielty == QUA09 ) then
              ngaus(ielty) = 9

           !else if( ielty == HEX20 ) then
           !   ngaus(ielty) = 27

           else if( ielty == TRI10 ) then
              ngaus(ielty) = 13
           else if( ielty == TET20 ) then
              ngaus(ielty) = 29
           else if( ielty == HEX27 ) then
              ngaus(ielty) = 27
           else if( ielty == PEN18 ) then
              ngaus(ielty) = 21
           else if( ielty == TET10 ) then
              ngaus(ielty) = 14
           else
              ngaus(ielty) = nnode(ielty) 
           end if
        else
           !
           ! Close rule
           !
           ngaus(ielty) = nnode(ielty)
        end if
     end if
  end do
  !
  ! Treat elements of dimension ndime-1: NGAUS, LQUAD and LEXIS
  !
  do ielty = iesta_dom,iesto_dom 
     if( lexis(ielty) == 1 ) then
        pgaus = ngaus(ielty)
        pquad = lquad(ielty)
        call bouele(nelty,pgaus,pquad,ielty,ngaus,lquad,lexis)
     end if
  end do
  !
  ! Maximum values
  ! MNODE can be optionally given in dimension field
  !
  mnode = max(mnode,-1_ip)
  mgaus = -1
  mnodb = -1
  mgaub = -1
  mlapl = -1
  do ielty = iesta_dom,iesto_dom
     if( lexis(ielty) == 1 ) then
        mnode = max(mnode,nnode(ielty))
        mgaus = max(mgaus,ngaus(ielty))
        mlapl = max(mlapl,llapl(ielty))
     end if
  end do
  do ielty = ibsta_dom,ibsto_dom
     if( lexis(ielty) == 1 ) then
        mnodb = max(mnodb,nnode(ielty))
        mgaub = max(mgaub,ngaus(ielty))
     end if
  end do
  if( lexis(SHELL) == 1 ) mnodb = max(2_ip,mnodb)
  if( lexis(BAR3D) == 1 ) mnodb = max(2_ip,mnodb)
  mnoga = max(mnode,mgaus)
  !
  ! Faces
  !
  mface = maxval(element_type(:) % number_faces)
  !
  ! Check errors
  !  
  if( mnode == -1 ) call runend('DOMAIN: NO ELEMENT TYPE HAS BEEN DECLARED')
  if( mgaus == -1 ) call runend('DOMAIN: NO NUMERICAL INTEGRATION HAS BEEN DEFINED')
  
end subroutine cderda
