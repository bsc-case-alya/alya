!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!----------------------------------------------------------------------
!> @addtogroup Parall
!> @{
!> @file    par_finali.f90
!> @author  Guillaume Houzeaux
!> @date    01/02/2014
!> @brief   Finalize MPI after a call to runend
!> @details Finalize MPI. Abort MPI to kill all slaves if code
!>          is not ending properly
!> @} 
!----------------------------------------------------------------------

subroutine par_finali(kfl_endok)
  use def_kintyp, only   :  ip
  use mod_parall, only   :  PAR_COMM_WORLD
  implicit none
  integer(ip)            :: kfl_endok
  integer(4)             :: istat,ierro

#ifndef MPI_OFF
  include  'mpif.h'
#endif

  ierro = 1 

#ifndef MPI_OFF
  if ( kfl_endok == 1_ip ) then
     call MPI_Finalize(istat)
  else
     call MPI_Abort(PAR_COMM_WORLD,ierro,istat)
  end if
#endif

end subroutine par_finali
