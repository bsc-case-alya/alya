!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



module mod_log

  use def_kintyp,            only : ip,rp
  use def_master,            only : lun_outpu  
  use mod_iofile,            only : iofile_flush_unit
  
  implicit none

  public :: log_runend_error, log_end_of_analysis

contains

  subroutine log_runend_error(message)
    character(len=*), intent(in) :: message
    integer(4) :: lun_outpu4
    lun_outpu4 = int(lun_outpu,4)
    write(lun_outpu4,100) trim(message)
    call iofile_flush_unit(lun_outpu4)

100   format(//,&
         & 5x,//,&
         & 5x,'--------------------------------------------------------',/,&
         & 5x,/,&
         & 5x,'|- AN ERROR HAS BEEN DETECTED:',/,/,&
         & 9x,a,/)
  end subroutine log_runend_error
  
  subroutine log_end_of_analysis()
    integer(4) :: lun_outpu4
    lun_outpu4 = int(lun_outpu,4)
    write(lun_outpu4,101)
    call iofile_flush_unit(lun_outpu4)

101 format(//,&
         & 5x,'|- END OF ANALYSIS',/)
  end subroutine log_end_of_analysis

end module mod_log
