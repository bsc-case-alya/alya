!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



module mod_real2rp
  use def_kintyp_basic, only: rp, rp16
  implicit none

  interface real2rp
    module procedure rp16_2_rp
  end interface real2rp
    
  public :: real2rp

contains

  function rp16_2_rp(value_in) result(value_out)
    implicit none
    real(rp16), intent(in) :: value_in
    real(rp) :: value_out
#ifdef RP16
    value_out = value_in
#else
    value_out = real(value_in, rp)
#endif
  end function rp16_2_rp

end module mod_real2rp