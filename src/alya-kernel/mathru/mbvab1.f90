!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine mbvab1(a,b,c,n1,n2,en1,en2)

!-----------------------------------------------------------------------
!
! This routine evaluates the matrix-vector product A = B C, where
! A -> R(n1), B -> Mat(n1,n2), C -> R(n2), and also computes  the
! Euclidian norms of A and C
!
!-----------------------------------------------------------------------
  use      def_kintyp
  implicit none
  integer(ip), intent(in)  :: n1,n2
  real(rp),    intent(in)  :: b(n1,n2), c(n2)
  real(rp),    intent(out) :: a(n1),en1,en2
  integer(ip)              :: i,k

  do i=1,n1
     a(i)=0.0_rp
     do k=1,n2
        a(i)=a(i)+b(i,k)*c(k)
     end do
  end do
  
  en1=0.0_rp
  en2=0.0_rp
  do i=1,n1
     en1=en1+a(i)*a(i)
  end do
  do i=1,n2
     en2=en2+c(i)*c(i)
  end do
  en1=sqrt(en1)
  en2=sqrt(en2)

end subroutine mbvab1


