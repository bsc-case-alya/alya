!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_parallel_interpolation

  use def_kintyp
  use def_master
  use def_domain
  use mod_elmgeo
  use def_maths_bin
  use def_kermod
  use def_search_method
  use mod_parall
  use def_interpolation_method
  use mod_memory_basic
  use mod_communications
  use mod_random

  implicit none
  real(rp),    parameter :: eps = 1.0e-10_rp
  real(rp),    pointer   :: bobox(:,:,:)
  real(rp),    pointer   :: subox(:,:,:)
  real(rp)               :: dista,dimin
  integer(ip)            :: pelty,ielem,inode,ipoin,ii,kk
  integer(ip)            :: idime,kpoin,npoin_sum,nn
  type(maths_bin)        :: bin_seq
  type(maths_bin)        :: bin_par
  real(rp),    pointer   :: xx(:,:),vv(:,:)
  type(interpolation)    :: interp
  integer(ip), pointer   :: npoin_gat(:)
  real(rp),    pointer   :: coord_gat(:,:)
  real(rp),    pointer   :: coord_tmp(:,:)

  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction


  nullify(bobox)
  nullify(subox)
  nullify(xx)
  nullify(vv)

  call random_initialization()

  do kk = 1,100
     if( kfl_paral /= 0 ) then
        nn = int(20.0_rp*random_generate_number(),ip)
        if( nn > 0 ) then
           allocate(xx(2,nn))  
           allocate(vv(2,nn))
           do ii = 1,nn
              xx(1,ii) = -1.0_rp + 3.0_rp * random_generate_number()
              xx(2,ii) = -1.0_rp + 3.0_rp * random_generate_number()
           end do
        end if
     end if
     !
     ! Gather mesh
     !
     allocate(npoin_gat(0:npart))  
     call PAR_ALLGATHER(npoin,npoin_gat)
     npoin_sum = sum(npoin_gat)
     npoin_gat = ndime * npoin_gat
     allocate(coord_gat(ndime,npoin_sum))
     coord_tmp => coord(1:ndime,1:npoin)
     call PAR_ALLGATHERV(coord_tmp,coord_gat,npoin_gat)
     !
     ! Sequential search method
     !
     call meshe(ndivi) % element_bb(bobox)
     call bin_seq % init  ()
     call bin_seq % input (boxes=(/10_ip,10_ip,1_ip/)) 
     call bin_seq % fill  (coord=meshe(ndivi)%coord)
     !
     ! Sequential search method
     !
     allocate(subox(2,ndime,0:npart))
     subox = 0.0_rp
     if( inotmaster ) then
        do idime = 1,ndime
           subox(1,idime,kfl_paral) = minval(bobox(1,idime,:))
           subox(2,idime,kfl_paral) = maxval(bobox(2,idime,:))
        end do
     else 
        subox(1,:,:) =  huge(1.0_rp)*0.1_rp
        subox(2,:,:) = -huge(1.0_rp)*0.1_rp
     end if
     call PAR_SUM(subox)
     call bin_par % init     ()
     call bin_par % input    (boxes=(/10_ip,10_ip,1_ip/)) 
     call bin_par % fill     (bobox=subox)
     if( IMASTER ) then
        print*,'ave. # elements per bin=  ',bin_par % stats(1)
        print*,'max. # elements in a bin= ',bin_par % stats(2)
        print*,'saving ratio [0,1]=       ',bin_par % stats(5)
        print*,'time fill=                ',bin_par % times(1)
        print*,'time candidate=           ',bin_par % times(2)
        print*,'max memory                ',bin_par % memor(2)     
     end if
     !
     ! Interpolation
     !
     call interp % init      ()
     call interp % input     (bin_seq,bin_par,COMM=PAR_COMM_WORLD,INTERPOLATION_METHOD=INT_NEAREST_NODE)
     call interp % preprocess(xx,meshe(ndivi))
     call interp % values    (meshe(ndivi) % coord,vv)

     if( associated(xx) ) then
        print*,'checkoint points=',kfl_paral,': ',size(xx,2)
        do ii = 1,int(size(xx,2),ip)
           dimin = huge(1.0_rp)
           do ipoin = 1,npoin_sum
              dista = (coord_gat(1,ipoin) - xx(1,ii))**2 + (coord_gat(2,ipoin) - xx(2,ii))**2
              dista = sqrt(dista)
              if( dista < dimin ) then
                 dimin = dista
                 kpoin = ipoin
              end if
           end do
           dista = (vv(1,ii) - xx(1,ii))**2 + (vv(2,ii) - xx(2,ii))**2
           dista = sqrt(dista)
           if( abs(dista-dimin) > 1.0e-12_rp ) then
              print*,'xx=          ',kfl_paral,xx(:,ii)
              print*,'vv=          ',kfl_paral,vv(:,ii)
              print*,'coord=       ',kfl_paral,coord_gat(:,kpoin)
              print*,'wrong result=',kfl_paral,dista,dimin
              stop 1
           end if
        end do
     end if

     call bin_seq % deallo()
     call bin_par % deallo()
     call interp  % deallo()

     if( associated(xx) ) deallocate(xx)
     if( associated(vv) ) deallocate(vv)

  end do
  call runend('O.K.!')

end program unitt_parallel_interpolation
 
