!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Adaptivity
!> @{
!> @file    mod_cavity.f90
!> @author  abel.gargallo
!> @date    2021-03-31
!> @brief   Cavity operators
!> @details Cavity operators
!>
!>          To add further details
!>
!>
!-----------------------------------------------------------------------

MODULE mod_cavity
!***************************************************************
!*
!*  Module for performing cavity operations
!*
!***************************************************************
use def_kintyp_basic, only: ip,rp,lg
use mod_meshTopology, only: get_boundary_faces

use mod_memory,             only: memory_alloca, memory_deallo
use def_adapt,              only: memor_adapt

implicit none

private

public :: cavity_insert_point,cavity_reinsert

!save
!  
!
!
CONTAINS
!
!
!
! subroutine cavity_insert_point(newPoint,elems_cav,mesh,M, Tcav_remeshed, elems_cav_updated, is_remeshed)
!   implicit none
!
!   integer(ip),            intent(in) :: newPoint
!   integer(ip), pointer    intent(in) :: elems_cav
!   type(mesh_type_basic),  intent(in) :: mesh
!   type(mesh_metric_type), intent(in) :: M
!   !
!   integer(ip), pointer, intent(inout) :: Tcav_remeshed(:,:)
!   integer(ip), pointer, intent(inout) :: elems_cav_updated(:)
!   logical(lg),          intent(out) :: is_remeshed
subroutine cavity_insert_point(newPoint,Tcav,Tcav_remeshed)
  implicit none
  
  integer(ip), intent(in) :: newPoint
  integer(ip), intent(in) :: Tcav(:,:)
  !
  integer(ip), pointer, intent(inout) :: Tcav_remeshed(:,:)
  !
  integer(ip), pointer :: boundary_faces(:,:)
  integer(ip) :: iface
  !
  call get_boundary_faces(Tcav,boundary_faces)
  
  nullify(Tcav_remeshed)
  call memory_alloca(memor_adapt,'Tcav_remeshed','cavity_insert_point',Tcav_remeshed,int(size(Tcav,1),ip),int(size(boundary_faces,2),ip))

  Tcav_remeshed(size(Tcav,1),:) = newPoint
  do iface =1,size(boundary_faces,2)
    Tcav_remeshed(1:size(boundary_faces,1),iface) = boundary_faces(:,iface)
  end do
  
  call memory_deallo(memor_adapt,'boundary_faces','cavity_insert_point',boundary_faces)
  
  return 
end subroutine cavity_insert_point
!
!
!
subroutine cavity_reinsert(pointToReinsert,Tcav,Tcav_remeshed)
  implicit none
  
  integer(ip), intent(in) :: pointToReinsert
  integer(ip), intent(in) :: Tcav(:,:)
!   integer(ip), pointer, optional, intent(in):: boundary_faces_inp(:,:)
  !
  integer(ip), pointer, intent(inout) :: Tcav_remeshed(:,:)
  !
  integer(ip), pointer :: boundary_faces(:,:)
  integer(ip) :: iface, inode, newCavElems
  logical(lg), pointer :: isConsideredFace(:)
  !
!   print*,'cavity_reinsert'
!   call runend('epa, ja esta programat pero revisar si eso')
  call get_boundary_faces(Tcav,boundary_faces)

  nullify(isConsideredFace)
  call memory_alloca(memor_adapt,'isConsideredFace','cavity_insert_point',isConsideredFace,int(size(boundary_faces,2),ip))
  
  newCavElems = 0
  do iface =1,size(boundary_faces,2)
    isConsideredFace(iface) = .true.
    loop_face_nodes: do inode = 1,size(boundary_faces,1)
      if( boundary_faces(inode,iface) == pointToReinsert ) then
        isConsideredFace(iface) = .false.
        exit loop_face_nodes
      end if
    end do loop_face_nodes
    if( isConsideredFace(iface) ) then
      newCavElems = newCavElems + 1_ip
    end if
  end do
  
  nullify(Tcav_remeshed)
  call memory_alloca(memor_adapt,'Tcav_remeshed','cavity_insert_point',Tcav_remeshed,int(size(Tcav,1),ip),newCavElems)

  newCavElems = 0
  Tcav_remeshed(size(Tcav,1),:) = pointToReinsert
  do iface =1,size(boundary_faces,2)
    if( isConsideredFace(iface) ) then
      newCavElems = newCavElems +1 
      Tcav_remeshed(1:size(boundary_faces,1),newCavElems) = boundary_faces(:,iface)
    end if
  end do
!   Tcav_remeshed(size(Tcav,1),:) = Tcav_remeshed(size(Tcav,1)-1,:)
!   Tcav_remeshed(size(Tcav,1),:) = pointToReinsert
  
  call memory_deallo(memor_adapt,'boundary_faces'  ,'cavity_insert_point',boundary_faces)
  call memory_deallo(memor_adapt,'isConsideredFace','cavity_insert_point',isConsideredFace)
  
  return 
end subroutine cavity_reinsert
!
!
!
END MODULE mod_cavity
!> @}








