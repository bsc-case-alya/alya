!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @defgroup Kinds_and_types
!> Kinds ands types of Alya
!> @{
!> @file    def_kintyp.f90
!> @author  houzeaux
!> @date    2018-12-28
!> @brief   Definition of kinds and types.
!> @details Definition of kinds and types.
!>
!-----------------------------------------------------------------------

module def_kintyp
  
  use def_kintyp_basic               ! Symbolc names for integers, reals and logicals
  use def_kintyp_boundary_conditions ! Boundary conditions
  use def_kintyp_functions           ! Functions
  use def_kintyp_solvers             ! Solvers
  use def_kintyp_postprocess         ! Postprocess
  use def_kintyp_domain              ! Mesh, finite element
  use def_kintyp_module              ! Module
  use def_kintyp_physics             ! Physics
  
end module def_kintyp
!> @}
