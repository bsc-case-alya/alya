!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_turnof
  !------------------------------------------------------------------------
  !****f* Nastin/nsi_turnof
  ! NAME 
  !    nsi_turnof
  ! DESCRIPTION
  !    This routine closes NASTIN module
  ! USES
  !    nsi_output
  !    nsi_outlat
  !    nsi_openfi
  ! USED BY
  !    Nastin
  !***
  !------------------------------------------------------------------------
  use def_parame
  use def_master
  use def_solver
  use def_domain
  use def_nastin
  implicit none

  !-------------------------------------------------------------------
  !
  ! Interpolation from coarse to fine mesh
  !
  !-------------------------------------------------------------------
  !
  if(kfl_meshi_nsi /= 0_ip) call nsi_coarfine(2_ip)
  !
  ! Output latex file
  !
  call nsi_outlat(two)
  !
  ! Output miniapp
  !
  call nsi_output_miniapp()
  !
  ! llamada a Alya ADAN para cierre de archivos
  !
  call nsi_cadan(7_ip) 
  
end subroutine nsi_turnof

