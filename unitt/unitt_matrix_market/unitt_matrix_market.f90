!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_matrix_market
  !
  ! Test matrix market format
  !
  use def_kintyp_basic, only : ip,rp
  use def_mat_fmt
  use mod_matrix_market
  implicit none

  integer(ip)                      :: nz,nn,ndof1,ndof2
  integer(ip),             pointer :: ia(:)
  integer(ip),             pointer :: ja(:)
  real(rp),    contiguous, pointer :: a(:)
  type(mat_csr)                    :: a_csr
  type(mat_coo)                    :: a_coo
  
  nn    = 3
  nz    = 5
  ndof1 = 2
  ndof2 = 2
  allocate(a(nz*ndof1*ndof2))
  allocate(ia(nn+1))
  allocate(ja(nz))

  ia(1) = 1
  ia(2) = 3
  ia(3) = 5
  ia(4) = 6
  
  ja(1) = 1
  ja(2) = 3
  ja(3) = 2
  ja(4) = 3
  ja(5) = 4
  
  call fill_matrix(ndof1,ndof2,nz,a)
  call matrix_market_matrix(ndof2,ndof1,nn,ia,ja,a,FILENAME='matrix.txt')
  
  call a_csr % init()
  a_csr % nrows =  nn
  a_csr % nz    =  nz
  a_csr % ndof1 =  ndof1
  a_csr % ndof2 =  ndof2
  a_csr % ia    => ia 
  a_csr % ja    => ja 
  
  call a_csr % assign(a)
  call matrix_market_matrix(a_csr,FILENAME='matrix-csr.txt')

  call a_coo % init()
  a_coo % nrows =  nn
  a_coo % nz    =  nz
  a_coo % ndof1 =  ndof1
  a_coo % ndof2 =  ndof2
  call a_coo % alloca()
  a_coo % xA(1) = 1
  a_coo % yA(1) = 1
  a_coo % xA(2) = 1
  a_coo % yA(2) = 3
  a_coo % xA(3) = 2
  a_coo % yA(3) = 2
  a_coo % xA(4) = 2
  a_coo % yA(4) = 3
  a_coo % xA(5) = 3
  a_coo % yA(5) = 4
  
  call a_coo % assign(a)

  call matrix_market_matrix(a_coo,FILENAME='matrix-coo.txt')
  call a_coo % output('MATRIX MARKET',FILENAME='matrix-coo-output')

  call a_csr % output('GID',           FILENAME='matrix-csr')
  call a_csr % output('DENSE',         FILENAME='matrix-csr')
  call a_csr % output('MATRIX MARKET', FILENAME='matrix-csr')
  call a_csr % output('PAJEK NET',     FILENAME='matrix-csr')
  
  call a_coo % output('GID',           FILENAME='matrix-coo')
  call a_coo % output('MATRIX MARKET', FILENAME='matrix-coo')

  call a_csr % deallo()

  nn    = 2
  nz    = 4
  ndof1 = 2
  ndof2 = 2
  allocate(a(nz*ndof1*ndof2))
  allocate(ia(nn+1))
  allocate(ja(nz))

  ia(1) = 1
  ia(2) = 3
  ia(3) = 5
  
  ja(1) = 1
  ja(2) = 2
  ja(3) = 1
  ja(4) = 2
  
  call fill_matrix_2(ndof1,ndof2,nz,a)
  
  call a_csr % init()
  a_csr % nrows =  nn
  a_csr % nz    =  nz
  a_csr % ndof1 =  ndof1
  a_csr % ndof2 =  ndof2
  a_csr % ia    => ia 
  a_csr % ja    => ja 
  
  call a_csr % assign(a)
  call a_csr % output('GID',FILENAME='matrix2-csr')

  print*,'symmetry=',a_csr % symmetry(TOLERANCE=1.0e-12_rp)
  print*,'symmetry=',a_csr % symmetry(TOLERANCE=-1.0e-12_rp)
  
end program unitt_matrix_market

subroutine fill_matrix_2(ndof1,ndof2,nz,a)
  
  use def_kintyp_basic, only : ip,rp
  implicit none
  integer(ip), intent(in)    :: nz,ndof1,ndof2
  real(rp),    intent(inout) :: a(ndof1,ndof2,nz)
  
  a(1,1,1) = 1.0_rp
  a(1,2,1) = 2.0_rp
  a(2,1,1) = 2.0_rp
  a(2,2,1) = 1.0_rp

  a(1,1,2) = 3.0_rp
  a(1,2,2) = 5.0_rp
  a(2,1,2) = 4.0_rp
  a(2,2,2) = 6.0_rp

  a(1,1,3) = 3.0_rp
  a(1,2,3) = 4.0_rp
  a(2,1,3) = 5.0_rp
  a(2,2,3) = 6.0_rp

  a(1,1,4) = 1.0_rp
  a(1,2,4) = 7.0_rp
  a(2,1,4) = 7.00001_rp
  a(2,2,4) = 1.0_rp
 
end subroutine fill_matrix_2

subroutine fill_matrix(ndof1,ndof2,nz,a)

  use def_kintyp_basic, only : ip,rp
  implicit none
  integer(ip), intent(in)    :: nz,ndof1,ndof2
  real(rp),    intent(inout) :: a(ndof1,ndof2,nz)

  a(1,1,1) = 1.0_rp
  a(1,2,1) = 5.0_rp
  a(2,1,1) = 2.0_rp
  a(2,2,1) = 6.0_rp

  a(1,1,2) = 3.0_rp
  a(1,2,2) = 7.0_rp
  a(2,1,2) = 4.0_rp
  a(2,2,2) = 8.0_rp

  a(1,1,3) = 9.0_rp
  a(1,2,3) = 13.0_rp
  a(2,1,3) = 10.0_rp
  a(2,2,3) = 14.0_rp

  a(1,1,4) = 11.0_rp
  a(1,2,4) = 15.0_rp
  a(2,1,4) = 12.0_rp
  a(2,2,4) = 16.0_rp
  
  a(1,1,5) = 17.0_rp
  a(1,2,5) = 19.0_rp
  a(2,1,5) = 18.0_rp
  a(2,2,5) = 20.0_rp
  
end subroutine fill_matrix
