#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



import sys
import os
import json

import numpy as np

import torch


#
# Load model
#
model = torch.jit.load("ANN_model.pt")

#
# Feed forward
#
xtest = np.array([0.1, 0.9, 0.02])
xtest = np.array(xtest).reshape(-1, 3)
xtest = torch.tensor(xtest, dtype=torch.float)
ytest = model(xtest)


#
# Back for print
#
xtest = np.array(xtest).flatten()
ytest = ytest.detach().numpy().flatten()
print(xtest, ytest)

