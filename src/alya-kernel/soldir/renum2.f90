!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine renum2(nadj,nstart,lev,naux,nodes,ns)
!-----------------------------------------------------------------------
!
! Compute a set of posibles startings nodes
!
!-----------------------------------------------------------------------
  use def_kintyp
  implicit none
  logical(lg) :: better
  integer(ip) :: nodes,ns
  integer(ip) :: naux(1:*),nadj(1:*),lev(1:*),nstart(1:*)
  integer(ip) :: iroot,i,nnoded,idepth,ndepth,iwidth,nwidth,lhw,lr
!
! Begin iteration
! Select initial root node arbitrarily and generate its level
! structure
!
  iroot = 1
  better = .true.
  do while (better)
     call renum4(nstart,lev,idepth,nadj,iwidth,nodes,iroot,lhw)
!
! Create a list of nodes which are at maximum distance from root
! node and store the root 
!
     ns = iroot
!
! Loop over nodes at maximum distance from root node
! Generate level structure for each node
! Set switch if a level structure of greater depth occurs
!
     better = .false.
     do i = 1,lhw
        nnoded = nstart(i)
        call renum4(naux,lev,ndepth,nadj,nwidth,nodes,nnoded,lr)
        if(ndepth>=idepth) then
           if((ndepth/=idepth).or.(nwidth<iwidth)) then
              iroot  = nnoded
              idepth = ndepth
              iwidth = nwidth
              better = .true.
           end if
        end if
     end do
  end do
  
end subroutine renum2
