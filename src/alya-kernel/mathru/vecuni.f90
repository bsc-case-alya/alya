!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine vecuni(n,v,rmod)

!-----------------------------------------------------------------------
!
! This routine computes the length of vector V and converts it to  
! a unit one 
!
!-----------------------------------------------------------------------
  use      def_kintyp, only : ip,rp
  implicit none
  integer(ip), intent(in)    :: n
  real(rp),    intent(inout) :: v(n)
  real(rp),    intent(out)   :: rmod
  integer(ip)                :: i

  rmod=0.0_rp
  do i=1,n
     rmod=rmod + v(i)*v(i)
  end do
  rmod=sqrt(rmod)
  if(rmod>epsilon(1.0_rp)) v=v/rmod
 
end subroutine vecuni
      
