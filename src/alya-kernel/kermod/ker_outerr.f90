!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Kermod
!> @{
!> @file    ker_outerr.f90
!> @author  Guillaume Houzeaux
!> @date    19/02/2016
!> @brief   Check errors
!> @details Check errors
!> @} 
!-----------------------------------------------------------------------

subroutine ker_outerr()

  use def_master
  use def_kermod
  use def_domain
  use def_solver
  use mod_communications, only : PAR_SUM
  use mod_outfor, only : outfor
  use def_mpio
  implicit none
  integer(ip) :: ierro,iwarn
  integer(ip) :: ifw, id

  ierro = 0
  iwarn = 0
  !
  ! Direct solver
  ! 
  if( kfl_direct_solver == SOL_DIRECT_SOLVER_PASTIX ) then
#ifndef PASTIX 
     ierro = ierro + 1
     call outfor(1_ip,momod(modul)%lun_outpu,'COMPILE ALYA WITH -DPASTIX AND LINK IT WITH PASTIX')     
#endif
  end if
  if( kfl_direct_solver == SOL_DIRECT_SOLVER_MUMPS ) then
#ifndef MUMPS
     ierro = ierro + 1 
     call outfor(1_ip,momod(modul)%lun_outpu,'COMPILE ALYA WITH -DMUMPS AND LINK IT WITH MUMPS')     
#endif
  end if
  if( kfl_direct_solver == SOL_DIRECT_SOLVER_WSMP ) then
#ifndef WSMP
     ierro = ierro + 1
     print*,'ierro',ierro
     call outfor(1_ip,momod(modul)%lun_outpu,'COMPILE ALYA WITH -DWSMP AND LINK IT WITH WSMP')
#endif
  end if
  if( kfl_direct_solver == SOL_DIRECT_SOLVER_PWSMP) then
#ifndef PWSMP
     ierro = ierro + 1
     call outfor(1_ip,momod(modul)%lun_outpu,'COMPILE ALYA WITH -DPWSMP AND LINK IT WITH PWSMP')
#endif
  end if

!  if( kfl_noslw_ker /= 0_ip .and. abs(delmu_dom-2.0_rp) > 1.0e-8_rp) then
!     ierro = ierro + 1
!     call outfor(1_ip,momod(modul)%lun_outpu,'When no slip wall is used MULTI must be 2 for wall law')  ! else ywalb and thus ywale are incorrect
!  end if

  

#ifdef VECTOR_SIZE_VARIABLE
  if( VECTOR_SIZE <= 0 .and. VECTOR_SIZE /= -1 .and. VECTOR_SIZE /= -2 ) then
     ierro = ierro + 1
     call outfor(1_ip,momod(modul)%lun_outpu,'VECTOR SIZE SHOULD BE DEFINED > 0 or =-1')
  end if
#endif
  !
  ! Periodicity does not work with SFC 
  !
  if( nperi/=0_ip .and. kfl_ngrou /= -1_ip .and. kfl_ngrou /= 0_ip .and. new_periodicity == 0 ) then
     ierro = ierro + 1
     call outfor(1_ip,momod(modul)%lun_outpu,&
          'FOR PERIODIC CASES YOU NEED TO USE GROUPS = int, SEQUENTIAL_FRONTAL IN THE DOM.DAT')
  end if
  !
  ! Exchange location needs elsest 
  !
  if( kfl_waexl_ker/=0_ip .and. kfl_elses == 0_ip  ) then
     ierro = ierro + 1
     call outfor(1_ip,momod(modul)%lun_outpu,&
          'EXCHANGE LOCATION NEEDS ELSEST')
  end if
  !
  ! MPIO incompatibilities 
  !
  if( kfl_posdi == 0 .and. mpio_flag_post_merge == PAR_MPIO_ON .and. ndivi > 0 .and.&
       ( mpio_flag_post == PAR_MPIO_ON .or. mpio_flag_rst == PAR_MPIO_ON ) ) then
     call runend('KER_OUTERR: CANNOT POSTPROCESS OR RESTART ON ORIGINAL MESH WHEN USING MPIO WITH MERGE OPTION')
  end if

  !----------------------------------!
  ! ERRORS RELATED LOOKUP FRAMEWORKS !
  !----------------------------------!
  !
  ! Undefined scaling type 
  !
  do ifw = 1,max_lookup_fw
     !
     ! Check if this framwork exists
     !
     if (lookup_fw(ifw) % kfl_tab_main > 0_ip) then
        !
        ! Check every dimension
        ! 
        do id = 1,lookup_fw(ifw) % main_table % ndim
           !
           ! Check if un-initialized
           !
           if (lookup_fw(ifw) % kfl_scale(id) == -2_ip) then
              ierro = ierro + 1
              call outfor(1_ip,momod(modul)%lun_outpu, &
                 'IN LOOKUP FRAMEWORK: '&
                 //trim(intost(ifw))//&
                 ' THE SCALING LAW OF DIMENSION '&
                 //trim(intost(id))//&
                 '('//lookup_fw(ifw) % main_table % coords(id) % name//') IS UNINITIALIZED')
           endif
        enddo
     endif
  enddo










  call PAR_SUM(ierro,'IN MY CODE',INCLUDE_ROOT=.true.)
  if( ierro /= 0 ) call runend('AN ERROR HAS BEEN FOUND IN KERMOD')

end subroutine ker_outerr
