!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!------------------------------------------------------------------------
!> @addtogroup Nastin 
!> @{
!> @file    nsi_plugin.f90
!> @date    14/10/2014
!> @author  Guillaume Houzeaux
!> @brief   Initialize coupling
!> @details Initialize some arrays for the coupling:
!>          1. Modify fixity array in order to impose UNKNOWN type
!>             interfaces automatically.
!>          2. Check all dofs are not prescribed on RESIDUAL type
!>             interfaces.
!>          3. Use keyword FORCE to force the imposition.
!> @}
!------------------------------------------------------------------------
subroutine nsi_plugin_init()
  !
  ! Mandatory variables => 
  ! 
  use def_kintyp,    only :  ip,rp
  use def_master,    only :  modul
  use mod_couplings, only :  COU_SET_FIXITY_ON_TARGET
  use mod_couplings, only :  COU_PUT_VALUE_ON_TARGET
  !
  ! <= Mandatory variables
  ! 
  use def_nastin,    only :  kfl_fixno_nsi
  use def_nastin,    only :  kfl_fixpp_nsi
  use def_nastin,    only :  kfl_fixpr_nsi
  implicit none

  call COU_SET_FIXITY_ON_TARGET('VELOC',modul,kfl_fixno_nsi)
  call COU_SET_FIXITY_ON_TARGET('PRESS',modul,kfl_fixpp_nsi)

  call COU_SET_FIXITY_ON_TARGET('UNKNO',modul,kfl_fixno_nsi)
  call COU_SET_FIXITY_ON_TARGET('UNKNO',modul,kfl_fixpp_nsi)

  call COU_SET_FIXITY_ON_TARGET('RESID',modul,kfl_fixno_nsi)
  call COU_SET_FIXITY_ON_TARGET('MOMEN',modul,kfl_fixno_nsi)

  call COU_SET_FIXITY_ON_TARGET('RESID',modul,kfl_fixpp_nsi)
  call COU_SET_FIXITY_ON_TARGET('CONTI',modul,kfl_fixpp_nsi)

  call COU_SET_FIXITY_ON_TARGET('RESID',modul,kfl_fixpr_nsi,'FREE BETWEEN SUBDOMAINS')
  call COU_SET_FIXITY_ON_TARGET('CONTI',modul,kfl_fixpr_nsi)
  !
  ! Free pressure on wet nodes
  !
  !call COU_PUT_VALUE_ON_TARGET(0_ip,kfl_fixpr_nsi)
  !call COU_PUT_VALUE_ON_TARGET(1_ip,kfl_fixpr_nsi,3_ip)

end subroutine nsi_plugin_init 
