!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_stokes()
  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_stokes
  ! NAME 
  !    nsi_stokes
  ! DESCRIPTION
  !    This routine solves an iteration for the incompressible
  !    Navier-Stokes equations, using:
  !    - A Monolithic scheme
  !    - A block Gauss-Seidel scheme
  ! USES
  !    nsi_ifconf
  !    nsi_solmon
  !    nsi_solbgs
  !    nsi_rotunk
  ! USED BY
  !    nsi_doiter
  !***
  !-----------------------------------------------------------------------
  use def_parame
  use def_master
  use def_domain
  use def_nastin
  implicit none
  integer(ip) :: kfl_advec_old,kfl_timei_old,kfl_cotem_old
  integer(ip) :: kfl_tiacc_old,kfl_sgsco_old
  integer(ip) :: kfl_sgsti_old
  real(rp)    :: dtinv_tmp
  !
  ! Initial solution is Stokes: save original values
  !
  kfl_advec_old = kfl_advec_nsi
  kfl_sgsco_old = kfl_sgsco_nsi
  kfl_timei_old = kfl_timei_nsi
  kfl_sgsti_old = kfl_sgsti_nsi
  kfl_cotem_old = kfl_cotem_nsi
  kfl_tiacc_old = kfl_tiacc_nsi
  dtinv_tmp     = dtinv_nsi
  kfl_advec_nsi = 0
  kfl_sgsco_nsi = 0
  kfl_sgsti_nsi = 0
  kfl_timei_nsi = 0
  kfl_cotem_nsi = 0
  dtinv_nsi     = 0.0_rp
  !
  ! Set up the solver parameters for the NS equations
  !
  call nsi_inisol(one)
  !
  ! Initial solution: UNKNO <= VELOC(:,:,1)
  !
  call nsi_updunk(2200_ip) 
  !
  ! Solve Stokes flow
  !
  call nsi_solite()
  !
  ! If initial solution is Stokes: recover original values
  !
  kfl_advec_nsi = kfl_advec_old 
  kfl_sgsco_nsi = kfl_sgsco_old 
  kfl_timei_nsi = kfl_timei_old 
  kfl_sgsti_nsi = kfl_sgsti_old 
  kfl_cotem_nsi = kfl_cotem_old 
  kfl_tiacc_nsi = kfl_tiacc_old
  dtinv_nsi     = dtinv_tmp
  !
  ! Update velocity and pressure: VELOC(:,:,1) <= UNKNO
  !
  call nsi_updunk(2300_ip)

end subroutine nsi_stokes
