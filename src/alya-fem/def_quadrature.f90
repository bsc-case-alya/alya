!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Domain
!> @{
!> @file    def_mesh_type.f90
!> @author  houzeaux
!> @date    2020-04-24
!> @brief   Mesh integration 
!> @details Mesh integration strategy
!>   
!-----------------------------------------------------------------------

module def_quadrature

  use def_kintyp_basic,           only : ip,rp
  use mod_memory_basic,           only : memory_alloca
  use mod_memory_basic,           only : memory_deallo
  !
  ! Gauss-Legendre quadrature rules
  !
  use mod_quadrature_gauss,       only : gauss_poi
  use mod_quadrature_gauss,       only : gauss_bar
  use mod_quadrature_gauss,       only : gauss_qua
  use mod_quadrature_gauss,       only : gauss_tri
  use mod_quadrature_gauss,       only : gauss_pen
  use mod_quadrature_gauss,       only : gauss_pyr
  !
  ! Closed quadrature rules
  !
  use mod_quadrature_closed,      only : close_poi
  use mod_quadrature_closed,      only : close_bar
  use mod_quadrature_closed,      only : close_qua
  use mod_quadrature_closed,      only : close_tri
  use mod_quadrature_closed,      only : close_pen
  use mod_quadrature_closed,      only : close_pyr
  !
  ! Trapezoidal rules
  !
  use mod_quadrature_trapezoidal, only : trape_bar
  use mod_quadrature_trapezoidal, only : trape_qua
  !
  ! Chebyshev quadrature (closed rule)
  !
  use mod_quadrature_chebyshev,   only : cheby_qua

  implicit none
  private

  integer(ip), parameter  :: GAUSS_LEGENDRE_RULE = 0
  integer(ip), parameter  :: TRAPEZOIDAL_RULE    = 2
  integer(ip), parameter  :: CLOSED_RULE         = 1
  integer(ip), parameter  :: CHEBYSHEV_RULE      = 3

  type :: quadrature
     integer(ip)          :: ndime            ! Space dimension
     integer(ip)          :: ngaus            ! Number of Gauss points
     integer(ip)          :: type             ! Type of rule
     integer(ip)          :: topo             ! Element topology
     real(rp),    pointer :: weigp(:)         ! Weights
     real(rp),    pointer :: posgp(:,:)       ! Integration points positions
     integer(8)           :: memor(2)         ! Memory counter
   contains
     procedure,      pass :: init             ! Initialization
     procedure,      pass :: alloca           ! Allocate     
     procedure,      pass :: deallo           ! Deallocate  
     procedure,      pass :: set              ! Set integration rule   
  end type quadrature

  public :: set_quadrature
  public :: quadrature
  public :: GAUSS_LEGENDRE_RULE   
  public :: TRAPEZOIDAL_RULE      
  public :: CLOSED_RULE           
  public :: CHEBYSHEV_RULE           

contains

  !-----------------------------------------------------------------------
  !> 
  !> @author  houzeaux
  !> @date    2022-01-26
  !> @brief   Set integration rule
  !> @details Set integration rule
  !> 
  !-----------------------------------------------------------------------

  subroutine set(self,ndime,ngaus,name,topo,ierr)
    
    class(quadrature),            intent(inout) :: self
    integer(ip),        optional, intent(in)    :: ndime
    integer(ip),        optional, intent(in)    :: ngaus
    integer(ip),        optional, intent(in)    :: topo
    character(LEN=*),   optional, intent(in)    :: name
    integer(ip),        optional, intent(out)   :: ierr
    integer(ip)                                 :: my_topo
    integer(ip)                                 :: ierro

    if( present(ndime) ) self % ndime = ndime
    if( present(ngaus) ) self % ngaus = ngaus
    if( present(topo ) ) self % topo  = topo
    if( present(name ) ) then
       select case ( name )
       case ( 'GAUSS-LEGENDRE'   ) ; self % type = GAUSS_LEGENDRE_RULE
       case ( 'CLOSE' , 'CLOSED' ) ; self % type = CLOSED_RULE
       case ( 'TRAPEZOIDAL'      ) ; self % type = TRAPEZOIDAL_RULE
       end select
    end if
    !
    ! Allocate rule
    !
    call self % alloca()
    ierro = 1

    call set_quadrature(self % ndime,self % ngaus,self % topo,self % type,self % posgp,self % weigp,ierro)
!!$    select case ( self % type )
!!$       
!!$    case ( GAUSS_LEGENDRE_RULE )
!!$       !
!!$       ! This choice of quadrature weights wi and quadrature nodes xi
!!$       ! is the unique choice that allows the quadrature rule to integrate
!!$       ! degree 2*ngaus − 1 polynomials exactly.
!!$       !
!!$       select case ( self % topo ) 
!!$       case(-2_ip) ; call gauss_poi(             self % ngaus,             self % weigp,ierro) ! Point
!!$       case(-1_ip) ; call gauss_bar(             self % ngaus,self % posgp,self % weigp,ierro) ! Bar         
!!$       case( 0_ip) ; call gauss_qua(self % ndime,self % ngaus,self % posgp,self % weigp,ierro) ! Quad/Hexa   
!!$       case( 1_ip) ; call gauss_tri(self % ndime,self % ngaus,self % posgp,self % weigp,ierro) ! Tria/Tetra    
!!$       case( 2_ip) ; call gauss_pen(self % ndime,self % ngaus,self % posgp,self % weigp,ierro) ! Prism(Penta)  
!!$       case( 3_ip) ; call gauss_pyr(self % ndime,self % ngaus,self % posgp,self % weigp,ierro) ! Pyramid      
!!$       end select
!!$       
!!$    case ( CLOSED_RULE )
!!$       !
!!$       ! Uses a close rule where Gauss-points are on the nodes
!!$       !
!!$       select case ( self % topo ) 
!!$       case(-2_ip) ; call close_poi(             self % ngaus,             self % weigp,ierro) ! Point
!!$       case(-1_ip) ; call close_bar(             self % ngaus,self % posgp,self % weigp,ierro) ! Bar         
!!$       case( 0_ip) ; call close_qua(self % ndime,self % ngaus,self % posgp,self % weigp,ierro) ! Quad/Hexa   
!!$       case( 1_ip) ; call close_tri(self % ndime,self % ngaus,self % posgp,self % weigp,ierro) ! Tria/Tetra    
!!$       case( 2_ip) ; call close_pen(self % ndime,self % ngaus,self % posgp,self % weigp,ierro) ! Prism(Penta)  
!!$       case( 3_ip) ; call close_pyr(self % ndime,self % ngaus,self % posgp,self % weigp,ierro) ! Prism(Penta)  
!!$       end select
!!$       
!!$    case ( TRAPEZOIDAL_RULE )
!!$       !
!!$       ! Uses a close rule where Gauss-points are on the nodes
!!$       !
!!$       select case ( self % topo ) 
!!$       case(-1_ip) ; call trape_bar(             self % ngaus,self % posgp,self % weigp,ierro) ! Bar  
!!$       case( 0_ip) ; call trape_qua(self % ndime,self % ngaus,self % posgp,self % weigp,ierro) ! Quad/Hexa 
!!$       end select
!!$       
!!$    case ( CHEBYSHEV_RULE  )
!!$       !
!!$       ! Uses a close rule where Gauss-points are on the nodes
!!$       !
!!$       select case ( self % topo ) 
!!$       case( 0_ip) ; call cheby_qua(self % ndime,self % ngaus,self % posgp,self % weigp,ierro) ! Quad/Hexa 
!!$       end select
!!$
!!$    end select

    if( present(ierr) ) then
       ierr = ierro
    else
       if( ierro /= 0 ) stop
    end if
    
  end subroutine set
  
  !-----------------------------------------------------------------------
  !> 
  !> @author  houzeaux
  !> @date    2022-01-26
  !> @brief   Initialization
  !> @details Initialization
  !> 
  !-----------------------------------------------------------------------

  subroutine init(self)

    class(quadrature), intent(inout) :: self

    self % ndime = 0 
    self % ngaus = 0 
    self % type  = 0
    self % topo  = 0 
    self % memor = 0_8 
    nullify(self % weigp)
    nullify(self % posgp)

  end subroutine init

  !-----------------------------------------------------------------------
  !> 
  !> @author  houzeaux
  !> @date    2022-01-26
  !> @brief   Allocate
  !> @details Allocate
  !> 
  !-----------------------------------------------------------------------

  subroutine alloca(self)

    class(quadrature), intent(inout) :: self

    call memory_alloca(self % memor,'SELF % WEIGP','alloca',self % weigp,self % ngaus)
    call memory_alloca(self % memor,'SELF % POSGP','alloca',self % posgp,max(1_ip,self % ndime),self % ngaus)

  end subroutine alloca

  !-----------------------------------------------------------------------
  !> 
  !> @author  houzeaux
  !> @date    2022-01-26
  !> @brief   Allocate
  !> @details Allocate
  !> 
  !-----------------------------------------------------------------------

  subroutine deallo(self)

    class(quadrature), intent(inout) :: self

    call memory_deallo(self % memor,'SELF % WEIGP','alloca',self % weigp)
    call memory_deallo(self % memor,'SELF % POSGP','alloca',self % posgp)

  end subroutine deallo

  !-----------------------------------------------------------------------
  !> 
  !> @author  houzeaux
  !> @date    2022-01-26
  !> @brief   Set quadrature rule
  !> @details Set quadrature rule
  !> 
  !-----------------------------------------------------------------------

  subroutine set_quadrature(ndime,ngaus,topo,type,posgp,weigp,ierro)
    
    integer(ip),        intent(in)    :: ndime
    integer(ip),        intent(in)    :: ngaus
    integer(ip),        intent(in)    :: topo
    integer(ip),        intent(in)    :: type
    real(rp),           intent(out)   :: posgp(max(1_ip,ndime),ngaus)
    real(rp),           intent(out)   :: weigp(ngaus)
    integer(ip),        intent(out)   :: ierro

    select case ( type )
       
    case ( GAUSS_LEGENDRE_RULE )
       !
       ! This choice of quadrature weights wi and quadrature nodes xi
       ! is the unique choice that allows the quadrature rule to integrate
       ! degree 2*ngaus − 1 polynomials exactly.
       !
       select case (  topo ) 
       case(-2_ip) ; call gauss_poi(        ngaus,        weigp, ierro) ! Point
       case(-1_ip) ; call gauss_bar(        ngaus, posgp, weigp, ierro) ! Bar         
       case( 0_ip) ; call gauss_qua( ndime, ngaus, posgp, weigp, ierro) ! Quad/Hexa   
       case( 1_ip) ; call gauss_tri( ndime, ngaus, posgp, weigp, ierro) ! Tria/Tetra    
       case( 2_ip) ; call gauss_pen( ndime, ngaus, posgp, weigp, ierro) ! Prism(Penta)  
       case( 3_ip) ; call gauss_pyr( ndime, ngaus, posgp, weigp, ierro) ! Pyramid      
       end select
       
    case ( CLOSED_RULE )
       !
       ! Uses a close rule where Gauss-points are on the nodes
       !
       select case (  topo ) 
       case(-2_ip) ; call close_poi(        ngaus,        weigp, ierro) ! Point
       case(-1_ip) ; call close_bar(        ngaus, posgp, weigp, ierro) ! Bar         
       case( 0_ip) ; call close_qua( ndime, ngaus, posgp, weigp, ierro) ! Quad/Hexa   
       case( 1_ip) ; call close_tri( ndime, ngaus, posgp, weigp, ierro) ! Tria/Tetra    
       case( 2_ip) ; call close_pen( ndime, ngaus, posgp, weigp, ierro) ! Prism(Penta)  
       case( 3_ip) ; call close_pyr( ndime, ngaus, posgp, weigp, ierro) ! Prism(Penta)  
       end select
       
    case ( TRAPEZOIDAL_RULE )
       !
       ! Uses a close rule where Gauss-points are on the nodes
       !
       select case (  topo ) 
       case(-1_ip) ; call trape_bar(        ngaus, posgp, weigp, ierro) ! Bar  
       case( 0_ip) ; call trape_qua( ndime, ngaus, posgp, weigp, ierro) ! Quad/Hexa 
       end select
       
    case ( CHEBYSHEV_RULE  )
       !
       ! Uses a close rule where Gauss-points are on the nodes
       !
       select case ( topo ) 
       case( 0_ip) ; call cheby_qua( ndime, ngaus, posgp, weigp, ierro) ! Quad/Hexa 
       end select

    end select

  end subroutine set_quadrature
  
end module def_quadrature
!> @}
