!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Nastin
!> @{
!> @file    nsi_restar.f90
!> @author  houzeaux
!> @date    2020-05-20
!> @brief   Restart
!> @details Nastin restart variables
!> @} 
!-----------------------------------------------------------------------

subroutine nsi_restar(itask)

  use def_kintyp_basic,   only : ip
  use def_master,         only : ITASK_READ_RESTART
  use def_master,         only : ITASK_WRITE_RESTART
  use def_nastin,         only : grnor_nsi
  use def_nastin,         only : ubpre_nsi
  use def_nastin,         only : avtim_nsi
  use def_nastin,         only : avste_nsi
  use mod_nsi_arrays,     only : nsi_arrays
  use mod_restart,        only : restart_ini
  use mod_restart,        only : restart_end
  use mod_restart,        only : restart_add
  implicit none

  integer(ip), intent(in) :: itask
  
  !----------------------------------------------------------------------
  !
  ! Primary arrays
  !
  !----------------------------------------------------------------------
  
  if( itask == ITASK_READ_RESTART ) then
     call nsi_arrays('READ RESTART')
  else if( itask == ITASK_WRITE_RESTART ) then
     call nsi_arrays('WRITE RESTART')
  end if
  
  !----------------------------------------------------------------------
  !
  ! Variables
  !
  !----------------------------------------------------------------------

  call restart_ini(itask)
  call restart_add(grnor_nsi,'grnor_nsi')
  call restart_add(ubpre_nsi,'ubpre_nsi')
  call restart_add(avtim_nsi,'avtim_nsi')
  call restart_add(avste_nsi,'avste_nsi')
  call restart_end(itask)

end subroutine nsi_restar
 
