!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_order_boundary_nodes

  use def_kintyp_basic, only : ip,rp
  use def_domain, only : ndime
  use def_mpio
  use mod_restart
  use mod_elmgeo
  use def_elmtyp
  use mod_maths_geometry
  use mod_bouder
  implicit none
  integer(ip) :: lnodb(3,2)
  integer(ip) :: lnods(3,2)
  integer(ip) :: lboel(3,2)
  real(rp)    :: coord(2,4)
  real(rp)    :: bocod(2,2),deriv(2),eucta,baloc(2,2),elcod(2,3)
  integer(ip) :: pnodb,ndimb,inode,pnode

  call elmgeo_element_type_initialization()
  
  lnods(:,1) = (/ 3 , 4 , 1 /)
  lnods(:,2) = (/ 4 , 2 , 1 /)

  coord(:,1) = (/ 0.000000e+00_rp , 0.000000e+00_rp /) 
  coord(:,2) = (/ 0.000000e+00_rp , 1.000000e-01_rp /)  
  coord(:,3) = (/ 1.000000e-01_rp , 0.000000e+00_rp /)  
  coord(:,4) = (/ 1.000000e-01_rp , 1.000000e-01_rp /)   

  lnodb(:,1) = (/ 1,3,0 /)
  lnodb(:,2) = (/ 3,1,0 /)

  call elmgeo_order_boundary_nodes(TRI03,lnods(:,1),lnodb(:,1),lboel(:,1))
  call elmgeo_order_boundary_nodes(TRI03,lnods(:,1),lnodb(:,2),lboel(:,2))
  print*,'a=',lnodb(1:2,1),', ',lboel(1:2,1)
  print*,'b=',lnodb(1:2,2),', ',lboel(1:2,2)

  pnodb = 2
#ifndef NDIMEPAR
  ndime = 2
#endif
  ndimb = 1
  pnode = 3
  
  do inode = 1,pnode
     elcod(:,inode) = coord(:,lnods(inode,1))
  end do
  
  deriv = (/ -0.5_rp,0.5_rp /)
  bocod(:,1) = (/ coord(:,lnodb(1,1)) /)
  bocod(:,2) = (/ coord(:,lnodb(2,1)) /)
  call bouder(&
       pnodb,ndime,ndimb,deriv,&    ! Cartesian derivative
       bocod,baloc,eucta)                                   ! and Jacobian
  call chenor(pnode,baloc,bocod,elcod)

  call maths_normalize_basis(baloc)
  print*,'normal=',baloc(:,ndime)
  
  if( lnodb(1,1) /= 1 ) stop 1
  if( lnodb(2,1) /= 3 ) stop 1
  if( lnodb(1,2) /= 1 ) stop 1
  if( lnodb(2,2) /= 3 ) stop 1
 
  if( lboel(1,1) /= 3 ) stop 1
  if( lboel(2,1) /= 1 ) stop 1
  if( lboel(1,2) /= 3 ) stop 1
  if( lboel(2,2) /= 1 ) stop 1

end program unitt_order_boundary_nodes
