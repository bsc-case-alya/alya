!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_srinte(itask,ndimi,rvari)
  !------------------------------------------------------------------------
  !****f* Parall/par_srinte
  ! NAME
  !    par_srinte
  ! DESCRIPTION
  ! OUTPUT
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use def_kintyp, only    :  ip,rp
  use def_master, only    :  npari,nparr,nparc,parin,parre
  implicit none
  integer(ip), intent(in) :: itask,ndimi
  integer(ip), target     :: rvari(max(1_ip,ndimi))

  nparr = 0
  nparc = 0

  select case ( itask )

  case ( 1_ip ) 

     npari =  ndimi
     parin => rvari
     call par_sendin()
     nullify(parin)

  case ( 2_ip )

     npari =  ndimi
     parin => rvari
     call par_receiv()
     nullify(parin)

  end select
  
end subroutine par_srinte
 
