#!/bin/bash
#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



#SBATCH --job-name=post-s
#SBATCH --output=post_%j.out
#SBATCH --error=post_%j.err
#SBATCH --ntasks=8
#SBATCH --time=00:05:00
#SBATCH --qos=debug

module purge
module load intel/2017.4
module load impi/2017.4
module load mkl/2017.4 
module load bsc/1.0
module load ALYA/mpio
module load ALYA-MPIO-TOOLS

mpirun -np 8 mpio2txt -m coh_mshDiv_3d_init_condition
