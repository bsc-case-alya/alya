!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!------------------------------------------------------------------------
!> @addtogroup Mathematics
!> @{
!> @file    spcdec.f90
!> @author  Herbert Owen
!> @brief   Computes eigenval. and eigenvec. of real, symmetrix matrix A (3x3). Sorts them in accending order.
!> @details Output is a vector D
!!          containing the eigenvalues in ascending order, and a matrix V whose
!!          columns contain the corresponding eigenvectors.
!> @}
!------------------------------------------------------------------------
subroutine spcdec(A,D,V,NROT,kfl_wivec,callersub)
  use def_kintyp
  use mod_maths, only : maths_eigen_3x3_symmetric_matrix

  implicit none
  real(rp),    intent(in)  :: A(3,3)
  real(rp),    intent(out) :: D(3), V(3,3)
  integer(ip), intent(out) :: NROT
  integer(ip), intent(in)  :: kfl_wivec   ! also obtain eigenvectors

  real(rp)                 :: E(3,3)
  real(rp)                 :: daux(3,10)!,dauxi(3),error(3)
  integer(ip)              :: idime,jdime,imeth

  character(*)             :: callersub  ! who is calling spcdec


  !V = 1.0_rp
  !D = 1.0_rp
  !return
  
  call maths_eigen_3x3_symmetric_matrix(A,D,V)  

  
  return
  

end subroutine spcdec

