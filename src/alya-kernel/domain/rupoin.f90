!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine rupoin(ngaus,weigp,ierro)
  !-----------------------------------------------------------------------
  !****f* Domain/rupoin
  ! NAME
  !    rupoin
  ! DESCRIPTION
  !     This routine sets up the integration constants
  ! OUTPUT
  !    
  ! USED BY
  !    rulepw
  !***
  !-----------------------------------------------------------------------
  use  def_kintyp, only    :  ip,rp
  implicit none
  integer(ip), intent(in)  :: ngaus
  integer(ip), intent(out) :: ierro
  real(rp),    intent(out) :: weigp(ngaus)

  ierro    = 0
  weigp(1) = 1.0_rp

end subroutine rupoin
