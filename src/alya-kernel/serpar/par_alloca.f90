!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine par_alloca(itask)
  !------------------------------------------------------------------------
  !****f* Parall/par_alloca
  ! NAME
  !    par_alloca
  ! DESCRIPTION
  !    Allocates memory for master arrays used by Parall 
  ! OUTPUT
  ! USED BY
  !    Parall
  !***
  !------------------------------------------------------------------------
  use      def_parame
  use      def_master
  use      def_parall
  use      def_domain
  use      mod_memchk
  use mod_parall, only : par_memor
  implicit none
  integer(ip), intent(in) :: itask
  integer(4)              :: istat

  select case (itask)

  case(1_ip)
     !
     ! PARR1(NPOIN)
     !
     allocate(parr1(npoin),stat=istat)     
     call memchk(zero,istat,par_memor,'PARR1','par_alloca',parr1)

  case(2_ip)
     !
     ! PARR2(NDIME,NPOIN)
     !
     allocate(parr2(ndime,npoin),stat=istat)     
     call memchk(zero,istat,par_memor,'PARR2','par_alloca',parr2)

  case(3_ip)
     !
     ! PARI1(NPOIN)
     !
     allocate(pari1(npoin),stat=istat)     
     call memchk(zero,istat,par_memor,'PARI1','par_alloca',pari1)

  case(4_ip)
     !
     ! PARR1(PARII)
     !
     allocate(parr1(parii),stat=istat)     
     call memchk(zero,istat,par_memor,'PARR1','par_alloca',parr1)

  case(5_ip)
     !
     ! PARI1(NELEM)
     !
     allocate(pari1(nelem),stat=istat)     
     call memchk(zero,istat,par_memor,'PARI1','par_alloca',pari1)


  end select

end subroutine par_alloca
