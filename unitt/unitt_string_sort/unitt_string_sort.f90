!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_maths_maxloc_nonzero
  !
  ! Order strings
  !
  use def_kintyp_basic, only : ip
  use mod_strings,      only : string_sort

  implicit none

  character(10) :: wtest(4)
  integer(ip)   :: nn
  
  wtest(1) = 'ccc'
  wtest(2) = 'ccc2'
  wtest(3) = 'aaa'
  wtest(4) = 'bbb'
  call string_sort(wtest,nn,.false.)

  if( nn /= 4 )            stop 1
  if( wtest(1) /= 'aaa'  ) stop 1
  if( wtest(2) /= 'bbb'  ) stop 1
  if( wtest(3) /= 'ccc'  ) stop 1
  if( wtest(4) /= 'ccc2' ) stop 1

  wtest(1) = 'bb'
  wtest(2) = 'aa'
  wtest(3) = 'bb'
  wtest(4) = 'aa'
  call string_sort(wtest,nn,.false.)
  if( nn /= 2 )           stop 1
  if( wtest(1) /= 'aa'  ) stop 1
  if( wtest(2) /= 'bb'  ) stop 1
  
end program unitt_maths_maxloc_nonzero
