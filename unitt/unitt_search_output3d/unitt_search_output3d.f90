!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_search_output3d

  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_maths_bin
  use def_maths_tree
  use def_maths_octbin
  use def_search_method
  use def_master
  use mod_memory
  use def_kermod
  use def_domain
  
  implicit none
  real(rp),             parameter :: epsil = 1.0e-12_rp
  type(mesh_type_basic)           :: mesh_out
  type(maths_bin)                 :: bin
  type(maths_octree)              :: oct
  type(maths_octbin)              :: octbin
  type(maths_kdtree)              :: kdtree
  integer(ip)                     :: ipoin,inodb
  integer(ip)                     :: pelty,iboun
  integer(8)                      :: memor_loc(2)
  integer(8)                      :: memor_tmp(2)
  real(rp)                        :: comin(3),comax(3)
  real(rp),             pointer   :: bobox(:,:,:)
  real(rp),             pointer   :: res(:,:)
  character(len=5),     pointer   :: names(:)
  integer(ip),          pointer   :: level(:)

  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction

  memor_loc = 0_8
  memor_tmp = 0_8
  nullify(bobox,res,names,level)
  
  call meshe(ndivi) % element_bb(bobox)
  !
  ! OCTREE
  !
  print*,'------------------------------------'
  call oct      % init  ()
  call oct      % input (limit=5_ip)!,STOPPING_CRITERION=0_ip)
  call oct      % fill  (BOBOX=bobox)
  call mesh_out % init()
  call oct      % mesh(&
       mesh_out % ndime,mesh_out % mnode,mesh_out % nelem,&
       mesh_out % npoin,mesh_out % lnods,mesh_out % ltype,&
       mesh_out % coord, CRITERION=SEARCH_MESH_ALL,MEMORY_COUNTER=memor_loc)
  call oct      % mesh_level(level, CRITERION=SEARCH_MESH_ALL)
  call oct      % results(res,names, CRITERION=SEARCH_MESH_ALL)
  call mesh_out % output (          FILENAME='octree',material=level)
  call mesh_out % results(res,names,FILENAME='octree')
  call oct      % mesh_level(level,ONLY_DEALLOCATE=.true.)
  call oct      % results(res,names,ONLY_DEALLOCATE=.true.)
  call oct      % deallo()
  call mesh_out % deallo(MEMORY_COUNTER=memor_loc)
  print*,'memor octree=                       ',oct % memor(1),memor_loc(1)
  print*,'time  octree=                       ',oct % times(1)
  print*,'number of leaves=                   ',oct % nleaves
  print*,'number of filled leaves=            ',oct % nfilled
  print*,'average number of element per leaf= ',oct % stats(2)/real(oct % nleaves,rp)
  print*,'Maximum number elements in a leaf=  ',int(oct % stats(4),ip)
  print*,'Maximum level=                      ',int(oct % stats(3),ip)
  if( oct % memor(1) /= 0_8 .or. memor_loc(1) /= 0 ) then     
     print*,'memor octree=',oct % memor(1),memor_loc(1)
     stop 1
  end if
  !
  ! BIN
  !
  print*,'------------------------------------'
  call bin      % init  ()
  call bin      % input (boxes=(/10_ip,10_ip,10_ip/))
  call bin      % fill  (BOBOX=bobox)
  call mesh_out % init()
  call bin      % mesh(&
       mesh_out % ndime,mesh_out % mnode,mesh_out % nelem,&
       mesh_out % npoin,mesh_out % lnods,mesh_out % ltype,&
       mesh_out % coord,MEMORY_COUNTER=memor_loc)
  call bin      % results(res,names)
  call mesh_out % output (          FILENAME='bin')
  call mesh_out % results(res,names,FILENAME='bin')
  call bin      % results(res,names,ONLY_DEALLOCATE=.true.)
  call bin      % deallo()
  call mesh_out % deallo(MEMORY_COUNTER=memor_loc)
  print*,'memor bin=                           ',bin % memor(1),memor_loc(1)
  print*,'time  bin=                           ',bin % times(1)
  print*,'average number of element per bin=   ',bin % stats(1)/real(bin % boxes,rp)
  print*,'Maximum number of elements in a bin= ',int(bin % stats(2),ip)

  if( bin % memor(1) /= 0_8 .or. memor_loc(1) /= 0 ) then     
     print*,'memor bin=',bin % memor(1),memor_loc(1)
     stop 1
  end if
  !
  ! OCTBIN
  !
  print*,'------------------------------------'
  call octbin   % init  ()
  call octbin   % input (boxes=(/6_ip,6_ip,6_ip/),limit=4_ip)
  call octbin   % fill  (BOBOX=bobox)
  call mesh_out % init()
  call octbin   % mesh(&
       mesh_out % ndime,mesh_out % mnode,mesh_out % nelem,&
       mesh_out % npoin,mesh_out % lnods,mesh_out % ltype,&
       mesh_out % coord,MEMORY_COUNTER=memor_loc)
  !call octbin   % results(res,names)
  call mesh_out % output (          FILENAME='octbin')
  !call mesh_out % results(res,names,FILENAME='octbin')
  !call octbin   % results(res,names,ONLY_DEALLOCATE=.true.)
  call octbin   % deallo()
  call mesh_out % deallo(MEMORY_COUNTER=memor_loc)
  print*,'memor octbin=                       ',octbin % memor(1),memor_loc(1)
  print*,'times octbin=                       ',octbin % times(1)
  print*,'number of octrees=                  ',octbin % num_octree
  print*,'Maximum level=                      ',int(octbin % stats(3),ip)
  print*,'average number of element per leaf= ',octbin % stats(2)/octbin % stats(1)
  if( octbin % memor(1) /= 0_8 .or. memor_loc(1) /= 0 ) then     
     print*,'memor octbin=',octbin % memor(1),memor_loc(1)
     stop 1
  end if
  !
  ! KDTREE
  !
!!$  print*,'------------------------------------'
!!$  call kdtree   % init  ()
!!$  call kdtree   % input (limit=5_ip)
!!$  call kdtree   % fill  (BOBOX=bobox)
!!$  call mesh_out % init()
!!$  call kdtree % mesh(&
!!$       mesh_out % ndime,mesh_out % mnode,mesh_out % nelem,&
!!$       mesh_out % npoin,mesh_out % lnods,mesh_out % ltype,&
!!$       mesh_out % coord,MEMORY_COUNTER=memor_loc)
!!$  call kdtree % results(res,names)
!!$  call mesh_out % output (          FILENAME='kdtree')
!!$  call mesh_out % results(res,names,FILENAME='kdtree')
!!$  call kdtree   % results(res,names,ONLY_DEALLOCATE=.true.)
!!$  call kdtree   % deallo()
!!$  call mesh_out % deallo(MEMORY_COUNTER=memor_loc)
!!$  print*,'memor kdtree=                       ',kdtree % memor(1),memor_loc(1)
!!$  print*,'time  kdtree=                       ',kdtree % times(1)
!!$  print*,'number of leaves=                   ',kdtree % nleaves
!!$  print*,'number of filled leaves=            ',kdtree % nfilled
!!$  print*,'average number of element per leaf= ',kdtree % stats(2)/real(kdtree % nleaves,rp)
!!$  print*,'Maximum number elements in a leaf=  ',int(kdtree % stats(4),ip)
!!$  print*,'Maximum level=                      ',int(kdtree % stats(3),ip)
!!$  if( kdtree % memor(1) /= 0_8 .or. memor_loc(1) /= 0 ) then     
!!$     print*,'memor kdtree=',bin % memor(1),memor_loc(1)
!!$     stop 1
!!$  end if  
  !
  ! OCTREE for boundaries
  !
  print*,'------------------------------------'
  deallocate(bobox)
  call memory_alloca(memor_loc,'BOBOX','boundary_bb',bobox,2_ip,meshe(ndivi) % ndime,meshe(ndivi) % nboun)
  do iboun = 1,meshe(ndivi) % nboun
     pelty =  meshe(ndivi) % ltypb(iboun)
     comin =  huge(1.0_rp)*0.1_rp
     comax = -huge(1.0_rp)*0.1_rp
     do inodb = 1,element_type(pelty) % number_nodes
        ipoin                         = meshe(ndivi) % lnodb(inodb,iboun)
        comin(1:meshe(ndivi) % ndime) = min(comin(1:meshe(ndivi) % ndime),meshe(ndivi) % coord(1:meshe(ndivi) % ndime,ipoin))
        comax(1:meshe(ndivi) % ndime) = max(comax(1:meshe(ndivi) % ndime),meshe(ndivi) % coord(1:meshe(ndivi) % ndime,ipoin))
     end do
     bobox(1,1:meshe(ndivi) % ndime,iboun) = comin(1:meshe(ndivi) % ndime)
     bobox(2,1:meshe(ndivi) % ndime,iboun) = comax(1:meshe(ndivi) % ndime)
  end do
  call oct      % init  ()
  call oct      % input (limit=5_ip)
  call oct      % fill  (BOBOX=bobox)
  call mesh_out % init()
  call oct      % mesh(&
       mesh_out % ndime,mesh_out % mnode,mesh_out % nelem,&
       mesh_out % npoin,mesh_out % lnods,mesh_out % ltype,&
       mesh_out % coord,MEMORY_COUNTER=memor_loc)
  call oct      % results(res,names)
  call mesh_out % output (          FILENAME='octree-boun')
  call mesh_out % results(res,names,FILENAME='octree-boun')
  call oct      % results(res,names,ONLY_DEALLOCATE=.true.)
  call oct      % deallo()
  call mesh_out % deallo(MEMORY_COUNTER=memor_loc)
  call memory_deallo(memor_loc,'BOBOX','boundary_bb',bobox)
  print*,'memor boundary octbin=',oct % memor(1),memor_loc(1)
  print*,'times boundary octbin=',oct % times(1)
  if( oct % memor(1) /= 0_8 .or. memor_loc(1) /= 0 ) then     
     print*,'memor boundary octree=',oct % memor(1),memor_loc(1)
     stop 1
  end if
  call runend('O.K.!')

end program unitt_search_output3d
 
