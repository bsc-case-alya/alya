#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



get_filename_component(ProjectId ${CMAKE_CURRENT_LIST_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})

include(unitt)

set(MPI_PROCESSES 1)
set(OPENMP_THREADS 0)

add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD COMMAND ${CMAKE_COMMAND} -E remove_directory ${CMAKE_CURRENT_BINARY_DIR}/circle_${MPI_PROCESSES}_${OPENMP_THREADS}
                                                     COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/circle/ ${CMAKE_CURRENT_BINARY_DIR}/circle_${MPI_PROCESSES}_${OPENMP_THREADS})

if (WITH_MPI_SEQUENTIAL_TESTS)
  add_test(NAME "${PROJECT_NAME}_${MPI_PROCESSES}_${OPENMP_THREADS}" COMMAND "${MPIEXEC_EXECUTABLE}"
     "${MPIEXEC_NUMPROC_FLAG}" "${MPI_PROCESSES}" ${MPIEXEC_PREFLAGS} ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}
     circle WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/circle_${MPI_PROCESSES}_${OPENMP_THREADS})
else()
  add_test(NAME "${PROJECT_NAME}_${MPI_PROCESSES}_${OPENMP_THREADS}" COMMAND ${PROJECT_NAME} circle WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/circle_${MPI_PROCESSES}_${OPENMP_THREADS})
endif()
