!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine domnfa(ndime,nnode,nface)
!-----------------------------------------------------------------------
!****f* Domain/domnfa
! NAME
!    domnfa
! DESCRIPTION
!    Compute number of faces of an element
! OUTPUT
!    NFACE : # of faces of an element
! USED BY
!    Domain
!***
!-----------------------------------------------------------------------
  use      def_kintyp
  implicit none 
  integer(ip), intent(in)  ::  ndime,nnode
  integer(ip), intent(out) ::  nface
      
  if(ndime==2) then 
     !
     ! 2D elements
     !
     if (nnode<=4) then 
        nface=nnode
     else
        if (nnode==6.or.nnode==7) then
           nface=3
        else
           nface=4
        end if
     end if
  else
     !
     ! 3D elements
     !
     if (nnode==4) then
        nface=4
     else if (nnode== 5) then
        nface=5
     else if (nnode==10) then
        nface=4
     else if (nnode==18) then
        nface=5
     else if (nnode==8) then
        nface=6
     else if (nnode==20) then
        nface=6
     else if (nnode==27) then
        nface=6
     else if (nnode==6) then
        nface=5
     end if
  end if

end subroutine domnfa

