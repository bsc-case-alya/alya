!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!




subroutine nsi_elmcost_all(&
		      elvel,pnode,pgaus,gpsha,gpvol,lnods,costf)

  !------------------------------------------------------------------------
  !****f* Nastin/nsi_elmcost_all
  ! NAME 
  !    nsi_elmcost_all
  ! DESCRIPTION
  !    This subroutine calculates elemental contributions of the cost .. f
  ! USES
  ! USED BY
  !------------------------------------------------------------------------

  use def_kintyp, only       :  ip,rp
  use def_domain, only       :  ndime,ntens,mnode
  use def_kermod, only       :  kfl_adj_prob,kfl_cost_type

  implicit none
  
  integer(ip), intent(in)    :: pnode,pgaus
  
  real(rp)    :: gpunk(pgaus),elvel_pert(pnode)
  integer(ip) :: inode,igaus,ipoin
  
  real(rp),       intent(in)        :: gpvol(pgaus)                      ! |J|*w
  real(rp),       intent(in)        :: elvel(ndime, pnode),gpsha(pnode,pgaus)
  real(rp),       intent(inout)     :: costf
  integer(ip),    intent(in)        :: lnods(pnode)

  if (kfl_cost_type == 2) then  
    !
    !  Initialization
    !
    do igaus=1,pgaus
	  gpunk(igaus) = 0.0_rp
    end do
    !
    ! read the veloc from the forward values
    !    
    do igaus=1,pgaus
	do inode =1,pnode
	  gpunk(igaus) = gpunk(igaus) + gpsha(inode,igaus)*elvel(1,inode)
	end do
        !$OMP ATOMIC
	costf = costf + gpvol(igaus)*gpunk(igaus)**2
    end do
  
  endif

end subroutine nsi_elmcost_all
