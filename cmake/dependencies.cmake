#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



include(metis)
define_metis()
include(maphys)
define_maphys()
include(gmsh)
gmsh()
include(mkl)
link_mkl()
include(mumps)
define_mumps()
include(petsc)
define_petsc()
include(commdom)
commdom()
include(pyjac)
pyjac()
include(cantera)
cantera()
include(torch)
define_torch()
include(talp)
talp()
include(dlb_barrier)
dlb_barrier()
include(tubes)
tubes()
include(fti)
fti()
include(extrae)
extrae()
