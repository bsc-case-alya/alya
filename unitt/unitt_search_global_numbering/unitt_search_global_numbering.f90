!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_search_global_numbering

  use def_kintyp
  use def_master
  use def_domain
  use mod_elmgeo
  use def_maths_bin
  use def_kermod
  use def_search_method
  use mod_parall
  use def_interpolation_method
  use mod_communications_global
  implicit none
  real(rp),    parameter :: eps = 1.0e-10_rp
  real(rp),    pointer   :: bobox(:,:,:)
  real(rp),    pointer   :: subox(:,:,:)
  integer(ip)            :: pelty,ielem,inode,ipoin,ii
  integer(ip)            :: idime
  type(maths_bin)        :: bin_seq
  type(maths_bin)        :: bin_par
  real(rp),    pointer   :: xx(:,:)
  real(rp),    pointer   :: x1(:),v1(:)
  type(interpolation)    :: interp
  
  call Initia()                                               ! Initialization of the run
  call Readom()                                               ! Domain reading
  call Partit()                                               ! Domain partitioning
  call Reaker()                                               ! Read Kermod
  call Domtra()                                               ! Domain transformation
  call Domain()                                               ! Domain construction

  nullify(bobox)
  nullify(subox)
  nullify(xx)
  nullify(x1)
  nullify(v1)
  
  if(npoin>0) then
     allocate(v1(npoin))
     allocate(x1(npoin))
  end if
  !
  ! Sequential search method
  !
  call meshe(ndivi) % element_bb(bobox)
  call bin_seq % init  ()
  call bin_seq % input (boxes=(/10_ip,10_ip,1_ip/),name = 'BIN SEQ')
  call bin_seq % fill  (BOBOX=bobox)
  !
  ! Sequential search method
  !
  allocate(subox(2,ndime,0:npart))
  subox = 0.0_rp
  if( inotmaster ) then
     do idime = 1,ndime
        subox(1,idime,kfl_paral) = minval(bobox(1,idime,:))
        subox(2,idime,kfl_paral) = maxval(bobox(2,idime,:))
     end do
  else 
     subox(1,:,kfl_paral) =  1.0e16_rp
     subox(2,:,kfl_paral) = -1.0e16_rp
  end if
  call PAR_SUM(subox,INCLUDE_ROOT=.true.)
  if(imaster) print*,subox
  !
  ! Parallel bin
  !
  call bin_par % init      ()
  call bin_par % input     (boxes=(/10_ip,10_ip,1_ip/),name = 'BIN PAR')
  call bin_par % fill      (BOBOX=subox)
  !
  ! Interpolation by global numbering
  !
  call interp % init       ()
  call interp % input      (bin_seq,bin_par,&
       &                   COMM=PAR_COMM_WORLD,&
       &                   INTERPOLATION_METHOD=INT_GLOBAL_NUMBERING,&
       &                   NAME='FIRST TRY',&
       &                   MYSELF=.false.)

  if( associated(meshe(ndivi) % coord) ) &  
       xx => meshe(ndivi) % coord(1:ndime,1:npoin)
  call interp % preprocess (xx,meshe(ndivi),meshe(ndivi) % lninv_loc)

  print*,'test 1'
  do ipoin = 1,npoin
     x1(ipoin) = 1.0_rp
     v1(ipoin) = 1.0_rp
  end do
  call interp % values     (x1,v1,INITIALIZATION=.false.)
  do ipoin = 1,npoin
     if(int(v1(ipoin))>1 .and. ipoin <= npoi1 ) then
        print*,kfl_paral,': ',lninv_loc(ipoin)
        stop 1
     end if
  end do

  print*,'end=',kfl_paral
  call interp % deallo()

  call runend('O.K.!')
  
end program unitt_search_global_numbering
 
