!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_second_try
  !
  ! Test copy, extract and merge operators
  !
  use def_kintyp_basic
  use def_elmtyp
  use def_kintyp_mesh_basic
  use mod_elmgeo
  use def_maths_bin
  use def_maths_tree
  use def_maths_octbin
  use def_master
  use mod_memory
  use def_interpolation_method
  implicit none

  real(rp),             parameter :: epsil = 1.0e-12_rp
  type(mesh_type_basic)           :: mesh
  type(mesh_type_basic)           :: mesh_out
  type(maths_bin)                 :: bin
  type(maths_bin)                 :: bin_2nd
  type(maths_octree)              :: oct
  type(maths_octree)              :: oct_2nd
  type(maths_octbin)              :: octbin
  integer(ip)                     :: ipoin,idime,ielem,inode,iboun
  integer(ip)                     :: pelty,ii,nn,kk
  real(rp)                        :: dista,xx_test(2)
  integer(8)                      :: count_rate8
  integer(8)                      :: memor_loc(2)
  integer(8)                      :: memor_tmp(2)
  type(i1p),            pointer   :: list_entities(:)    
  real(rp),             pointer   :: bobox(:,:,:)
  real(rp),             pointer   :: shapf(:,:),xx(:,:),vv(:,:),x1(:),v1(:)
  integer(ip),          pointer   :: lelem(:)
  real(rp),             pointer   :: res(:,:)
  character(len=5),     pointer   :: names(:)
  type(interpolation)             :: interp
  
  type(interpolation)             :: inter2
  real(rp),             pointer   :: x2(:,:),v2(:,:)
  integer(ip),          pointer   :: permu(:)

  memor_loc = 0_8
  memor_tmp = 0_8
  nullify(bobox,list_entities,shapf,lelem,xx,res,names)
  call system_clock(count_rate=count_rate8)
  rate_time = 1.0_rp / max(real(count_rate8,rp),zeror)
  !
  ! Create mesh
  !
  call elmgeo_element_type_initialization()

  call mesh % init('MY_MESH')
  
  mesh % nelem = 9
  mesh % npoin = 16
  mesh % ndime = 2
  mesh % mnode = 4
  
  call mesh % alloca()

  mesh % lnods(:,1)  = (/  12 , 7 , 5 , 10 /)
  mesh % lnods(:,2)  = (/  14 , 9 , 7 , 12 /)
  mesh % lnods(:,3)  = (/  16, 15,  9 , 14 /) 
  mesh % lnods(:,4)  = (/   7,  4 , 2 ,  5 /)
  mesh % lnods(:,5)  = (/   9 , 8 , 4 ,  7 /) 
  mesh % lnods(:,6)  = (/  15, 13,  8,   9 /)  
  mesh % lnods(:,7)  = (/   4 , 3 , 1 ,  2 /) 
  mesh % lnods(:,8)  = (/   8 , 6 , 3 ,  4 /) 
  mesh % lnods(:,9)  = (/  13 ,11,  6,   8 /)

  mesh % ltype(1:9)  = QUA04

  mesh % coord(:,1 ) = (/ 0.000000e+00_rp , 1.000000e+00_rp /) 
  mesh % coord(:,2 ) = (/ 0.000000e+00_rp , 6.666667e-01_rp /) 
  mesh % coord(:,3 ) = (/ 3.333333e-01_rp , 1.000000e+00_rp /) 
  mesh % coord(:,4 ) = (/ 3.333333e-01_rp , 6.666667e-01_rp /) 
  mesh % coord(:,5 ) = (/ 0.000000e+00_rp , 3.333333e-01_rp /) 
  mesh % coord(:,6 ) = (/ 6.666667e-01_rp , 1.000000e+00_rp /) 
  mesh % coord(:,7 ) = (/ 3.333333e-01_rp , 3.333333e-01_rp /) 
  mesh % coord(:,8 ) = (/ 6.666667e-01_rp , 6.666667e-01_rp /) 
  mesh % coord(:,9 ) = (/ 6.666667e-01_rp , 3.333333e-01_rp /) 
  mesh % coord(:,10) = (/ 0.000000e+00_rp , 0.000000e+00_rp /) 
  mesh % coord(:,11) = (/ 1.000000e+00_rp , 1.000000e+00_rp /) 
  mesh % coord(:,12) = (/ 3.333333e-01_rp , 0.000000e+00_rp /) 
  mesh % coord(:,13) = (/ 1.000000e+00_rp , 6.666667e-01_rp /) 
  mesh % coord(:,14) = (/ 6.666667e-01_rp , 0.000000e+00_rp /) 
  mesh % coord(:,15) = (/ 1.000000e+00_rp , 3.333333e-01_rp /) 
  mesh % coord(:,16) = (/ 1.000000e+00_rp , 0.000000e+00_rp /)
  !
  ! Element bounding boxes
  !
  call mesh % element_bb(bobox)
  call mesh % output(filename='test')
  
  allocate(xx(mesh % ndime,4),vv(mesh % ndime,4))
  vv      = -1.0_rp
  xx(:,1) = (/ -0.5_rp,-0.5_rp /)
  xx(:,2) = (/  0.5_rp, 0.5_rp /)
  xx(:,3) = (/  0.1_rp, 0.1_rp /)
  xx(:,4) = (/  1.1_rp, 1.1_rp /)

  !----------------------------------------------------------------------
  !
  ! BIN
  ! 
  !----------------------------------------------------------------------

  print*,'-----------------------------------------------------'
  print*,'BIN'
  
  call bin     % init      () 
  call bin     % input     (boxes=(/2_ip,2_ip/)) 
  call bin     % fill      (BOBOX=bobox)

  call bin_2nd % init      () 
  call bin_2nd % input     (boxes=(/3_ip,3_ip/)) 
  call bin_2nd % fill      (COORD=mesh % coord)

  call interp % init       ()
  call interp % input      (bin,&
       &                   INTERPOLATION_METHOD=INT_ELEMENT_INTERPOLATION)

  call inter2 % init       ()
  call inter2 % input      (bin_2nd,&
       &                   INTERPOLATION_METHOD=INT_NEAREST_NODE)

  call interp % preprocess (xx,mesh,SECOND_TRY=inter2)
  call interp % values     (mesh % coord,vv)

  do ii = 1,size(vv,2)
     if( ii == 1 ) then
        if( abs(vv(1,ii)-0.0_rp) > zeror .or. abs(vv(2,ii)-0.0_rp) > zeror ) then
           print*,'error=',ii,vv(:,ii)
           stop 1
        end if
     else if ( ii == 4 ) then
        if( abs(vv(1,ii)-1.0_rp) > zeror .or. abs(vv(2,ii)-1.0_rp) > zeror ) then
           print*,'error=',ii,vv(:,ii)
           stop 2
        end if
     else if( abs(vv(1,ii)-xx(1,ii)) > zeror .or. abs(vv(2,ii)-xx(2,ii)) > zeror ) then
        print*,'error=',ii,vv(:,ii)
        stop 3        
     end if
  end do
  
  call interp  % deallo()
  call inter2  % deallo()
  call bin     % deallo()
  call bin_2nd % deallo()

  if( interp % memor(1) /= 0_8 ) then
     print*,'Wrong interp memory=',interp % memor(1)
     stop 4
  end if
  if( inter2 % memor(1) /= 0_8 ) then
     print*,'Wrong interp memory=',inter2 % memor(1)
     stop 4
  end if
  
  print*,'BIN OK'
  print*,'-----------------------------------------------------'

  !----------------------------------------------------------------------
  !
  ! OCTREE
  ! 
  !----------------------------------------------------------------------
  
  print*,'-----------------------------------------------------'
  print*,'OCTREE'  
 
  call oct     % init      () 
  call oct     % input     (limit=2_ip) 
  call oct     % fill      (BOBOX=bobox)

  call bin_2nd % init      () 
  call bin_2nd % input     (boxes=(/3_ip,3_ip/)) 
  call bin_2nd % fill      (COORD=mesh % coord)

  call interp % init       ()
  call interp % input      (oct,&
       &                   INTERPOLATION_METHOD=INT_ELEMENT_INTERPOLATION)

  call inter2 % init       ()
  call inter2 % input      (bin_2nd,&
       &                   INTERPOLATION_METHOD=INT_NEAREST_NODE)
  do ii = 1,size(vv,2)
     if( ii == 1 ) then
        if( abs(vv(1,ii)-0.0_rp) > zeror .or. abs(vv(2,ii)-0.0_rp) > zeror ) then
           print*,'error=',ii,vv(:,ii)
           stop 1
        end if
     else if ( ii == 4 ) then
        if( abs(vv(1,ii)-1.0_rp) > zeror .or. abs(vv(2,ii)-1.0_rp) > zeror ) then
           print*,'error=',ii,vv(:,ii)
           stop 2
        end if
     else if( abs(vv(1,ii)-xx(1,ii)) > zeror .or. abs(vv(2,ii)-xx(2,ii)) > zeror ) then
        print*,'error=',ii,vv(:,ii)
        stop 3        
     end if
  end do
  
  call interp  % deallo()
  call inter2  % deallo()
  call bin     % deallo()
  call bin_2nd % deallo()

  if( interp % memor(1) /= 0_8 ) then
     print*,'Wrong interp memory=',interp % memor(1)
     stop 4
  end if
  if( inter2 % memor(1) /= 0_8 ) then
     print*,'Wrong interp memory=',inter2 % memor(1)
     stop 4
  end if
  
  print*,'OCTREE OK'  
  print*,'-----------------------------------------------------'
  
end program unitt_second_try
