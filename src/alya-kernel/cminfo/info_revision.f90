!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



#ifdef CMAKE

subroutine infrev()
  use def_master
  use gitinfo
  implicit none
  character(150) :: log_file
  integer, parameter :: u=1457845

  log_file = adjustl(trim(namda))//"-git.log"

  open(u, file=log_file, action="write")
  write(u,*) "Remote URL: "//trim(GIT_REMOTE)
  write(u,*) "Revision: "//trim(GIT_REVISION)
  write(u,*) "Branch: "//trim(GIT_BRANCH)
  close(u)

end subroutine infrev

#endif
