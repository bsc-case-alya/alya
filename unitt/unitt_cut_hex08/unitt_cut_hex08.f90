!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_cut_hex08
  !
  ! Test copy, extract and merge operators
  !
  use def_elmtyp
  use def_kintyp_basic
  use def_kintyp_mesh_basic
  use mod_elmgeo
  implicit none

  type(mesh_type_basic)         :: mesh
  type(mesh_type_basic)         :: cut
  integer(ip)                   :: ipoin,idime,ielem,inode
  real(rp),             pointer :: dista(:)
  
  call elmgeo_element_type_initialization()

  call mesh % init()
  call cut  % init()
  
  mesh % nelem = 1
  mesh % npoin = 8
  mesh % ndime = 3
  mesh % mnode = 8
  mesh % name  = 'MESH'
  
  call mesh % alloca()

  mesh % lnods(:,1)  = (/  1,2,3,4,5,6,7,8  /)
  mesh % ltype(1)    = HEX08

  mesh % coord(:,1 ) = (/ 0.0_rp,0.0_rp,0.0_rp /) 
  mesh % coord(:,2 ) = (/ 1.0_rp,0.0_rp,0.0_rp /) 
  mesh % coord(:,3 ) = (/ 1.0_rp,1.0_rp,0.0_rp /) 
  mesh % coord(:,4 ) = (/ 0.0_rp,1.0_rp,0.0_rp /) 
  mesh % coord(:,5 ) = (/ 0.0_rp,0.0_rp,1.0_rp /) 
  mesh % coord(:,6 ) = (/ 1.0_rp,0.0_rp,1.0_rp /) 
  mesh % coord(:,7 ) = (/ 1.0_rp,1.0_rp,1.0_rp /) 
  mesh % coord(:,8 ) = (/ 0.0_rp,1.0_rp,1.0_rp /) 

  allocate(dista(8))

  dista(1:4) = -1.0_rp
  dista(5:8) =  1.0_rp
  
  call cut  % cut_level(mesh,dista)
  cut % name  = 'CUT'
  
  call cut  % output()
  call mesh % output()
  
end program unitt_cut_hex08
