!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!
!> @addtogroup Domain
!> @{
!> @name    ToolBox for periodicity
!> @file    mod_periodicity.f90
!> @author  Guillaume Houzeaux
!> @brief   ToolBox periodicity
!> @details ToolBox periodicity
!> @{
!
!-----------------------------------------------------------------------

module mod_periodicity_sequential

  use def_kintyp_basic,          only : ip,rp,lg,i1p
  use def_master,                only : ISEQUEN
  implicit none

  public :: periodicity_sequential
  
contains


  !-----------------------------------------------------------------------
  !> 
  !> @author  houzeaux
  !> @date    2019-12-14
  !> @brief   Impose periodicity
  !> @details Impose periodicity
  !> 
  !-----------------------------------------------------------------------

  subroutine periodicity_sequential(ndofn,xx)

    use def_domain
    integer(ip), intent(in)    :: ndofn
    real(rp),    intent(inout) :: xx(ndofn,*)
    integer(ip)                :: ipoin,jpoin,idime

    if( ISEQUEN ) then
       !
       ! Master = master + slaves
       !
       do ipoin = 1,npoin
          jpoin = lmast(ipoin)
          if(jpoin>0) then
             do idime = 1,ndofn
                xx(idime,jpoin) = xx(idime,jpoin) + xx(idime,ipoin) 
             end do
          end if
       end do
       !
       ! Slaves = master
       !
       do ipoin = 1,npoin
          jpoin = lmast(ipoin)
          if(jpoin>0) then
             do idime = 1,ndofn
                xx(idime,ipoin) = xx(idime,jpoin) 
             end do
          end if
       end do

    end if

  end subroutine periodicity_sequential

end module mod_periodicity_sequential
!> @}
