!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



program unitt_ker_proper_heaviside
  !
  ! Get last non-zero position
  !
  use def_kintyp_basic,       only : ip, rp

#ifdef VECTOR_SIZE  
  use def_domain,             only : ndime
  use def_domain,             only : ntens
  use def_domain,             only : elmar
  use def_domain,             only : lexis
  use def_domain,             only : lrule
  use def_domain,             only : ngaus
  use def_domain,             only : memor_dom
  use def_domain,             only : mnode
  use def_elmtyp,             only : TRI03
  use mod_ker_proper_generic, only : ker_proper_gradient
  use mod_elmgeo,             only : elmgeo_element_type_initialization
  use mod_elmgeo_vector
  
  implicit none

  integer(ip)              :: VECTOR_DIM
  real(rp),    parameter   :: eps = epsilon(1.0_rp)
  integer(ip), parameter   :: pnode=3,pgaus=3,pelty=TRI03
  real(rp),    allocatable :: elcod(:,:,:)
  real(rp),    allocatable :: elval(:,:)
  real(rp),    allocatable :: gpdet(:,:)     ! |J|
  real(rp),    allocatable :: xjaci(:,:,:,:) ! J^-1
  real(rp),    allocatable :: gpcar(:,:,:,:) ! grad(N)
  real(rp),    allocatable :: grval(:,:,:)
  integer(ip)              :: inode,ivect,igaus

#ifndef NDIMEPAR
  ndime      = 2
  ntens      = 6
  memor_dom  = 0_8
  mnode      = 6
  VECTOR_DIM = VECTOR_SIZE
  allocate(elcod(VECTOR_DIM,ndime,pnode))
  allocate(elval(VECTOR_DIM,pnode))
  allocate(gpdet(VECTOR_DIM,pgaus))
  allocate(xjaci(VECTOR_DIM,ndime,ndime,pgaus))
  allocate(grval(VECTOR_DIM,ndime,pnode))
  
  call elmgeo_element_type_initialization()
  call cshder(1_ip)  
  call elmtyp()
  lexis        = 0
  lexis(TRI03) = 1
  ngaus(TRI03) = 3
  lrule(TRI03) = 3
  call cshder(3_ip)

  elcod(1:VECTOR_DIM,1,1) = 0.0_rp
  elcod(1:VECTOR_DIM,2,1) = 0.0_rp
  elcod(1:VECTOR_DIM,1,2) = 1.0_rp
  elcod(1:VECTOR_DIM,2,2) = 0.0_rp
  elcod(1:VECTOR_DIM,1,3) = 1.0_rp
  elcod(1:VECTOR_DIM,2,3) = 1.0_rp
  !
  ! GPCAR(VECTOR_SIZE,NDIME,MNODE,PGAUS)
  !  
  allocate(gpcar(VECTOR_DIM,ndime,mnode,pgaus))
  call elmgeo_cartesian_derivatives_jacobian(&
       VECTOR_DIM,ndime,mnode,pnode,pgaus,elcod,&
       elmar(pelty) % deriv,xjaci,gpcar,gpdet)    

  do inode = 1,pnode
     elval(1:VECTOR_DIM,inode) = 2.0_rp*elcod(1:VECTOR_DIM,1,inode)+3.0_rp*elcod(1:VECTOR_DIM,2,inode)
  end do
  
  !!grval = ker_proper_gradient(VECTOR_DIM,pnode,pgaus,gpcar,elval)
  call ker_proper_gradient(VECTOR_DIM,mnode,pnode,pgaus,gpcar,elval,grval)
 
  do ivect = 1,VECTOR_DIM
     do igaus = 1,pgaus
        if( abs(grval(ivect,1,igaus)-2.0_rp) > eps ) stop 1
        if( abs(grval(ivect,2,igaus)-3.0_rp) > eps ) stop 2
     end do
  end do
  !
  ! GPCAR(VECTOR_SIZE,NDIME,PNODE,PGAUS)
  !
  deallocate(gpcar)
  allocate(gpcar(VECTOR_DIM,ndime,pnode,pgaus))
  call elmgeo_cartesian_derivatives_jacobian(&
       VECTOR_DIM,ndime,pnode,pnode,pgaus,elcod,&
       elmar(pelty) % deriv,xjaci,gpcar,gpdet)    

  do inode = 1,pnode
     elval(1:VECTOR_DIM,inode) = 2.0_rp*elcod(1:VECTOR_DIM,1,inode)+3.0_rp*elcod(1:VECTOR_DIM,2,inode)
  end do
  
  !!grval = ker_proper_gradient(VECTOR_DIM,pnode,pgaus,gpcar,elval)
  call ker_proper_gradient(VECTOR_DIM,pnode,pnode,pgaus,gpcar,elval,grval)
 
  do ivect = 1,VECTOR_DIM
     do igaus = 1,pgaus
        if( abs(grval(ivect,1,igaus)-2.0_rp) > eps ) then
           print*,'gr=',grval(ivect,1,igaus)
           stop 3
        end if
        if( abs(grval(ivect,2,igaus)-3.0_rp) > eps ) stop 4
     end do
  end do  
  
#endif
#endif
  
end program unitt_ker_proper_heaviside
