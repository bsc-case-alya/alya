!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine nsi_tluave(itask)

  !-----------------------------------------------------------------------
  !****f* Nastin/nsi_tluave
  ! NAME 
  !    nsi_tluave
  ! DESCRIPTION
  !    Calculate average velocity for two-layer model coupling
  !    U(n+1) = e*u(n+1) + (1-e)*U(n)
  !    where:
  !          e = dt/T is the weighting function
  !          T is the averaging period
  !          U(n) is the averaged velocity at timeG-step n
  !          u(n) is the instantaneous velocity at time-step n
  !
  !    ITASK = 1 ... Calculates time-averaged velocity at each time step
  !            2 ... Calculates initial value of time-averaged velocity
  ! USES
  ! USED BY
  !    nsi_averag
  !***
  !-----------------------------------------------------------------------

  use def_elmtyp
  use def_master
  use def_domain
  use def_nastin
  use def_kermod,         only : tlape_ker

  implicit none

  integer(ip),intent(in)    :: itask

  real(rp)                  :: avwei   ! e=dt/T, weighting function for averaging
  
  integer(ip)               :: ipoin


  if ( itask /= 2_ip ) then
     if ( abs(tlape_ker) > 1.0e-9_rp ) then
        avwei = dtime/tlape_ker    ! e=dt/T  where T is the time-period for averaging
     else
        avwei = 1.0_rp  ! If no averaging period is given in the input, the model uses the instantaneous velocity
     end if
  end if

  if ( itask == 1_ip ) then
  
     !-----------------------------------------------------------------------------
     !
     ! Calculate average velocity for the two-layer model
     !
     !-----------------------------------------------------------------------------
     
     do ipoin = 1, npoin
        tluav_nsi(1:ndime,ipoin) = avwei*veloc(1:ndime,ipoin,1)+(1.0_rp-avwei)*tluav_nsi(1:ndime,ipoin)
     end do

  else if ( itask == 2_ip ) then
     
     !-----------------------------------------------------------------------------
     !
     ! Calculate initial value of average velocity
     !
     !-----------------------------------------------------------------------------
 
     do ipoin = 1, npoin
        tluav_nsi(1:ndime,ipoin) = veloc(1:ndime,ipoin,1)
     end do

  end if

end subroutine nsi_tluave
