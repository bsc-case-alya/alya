#########################################################################
#                                                                       #
#  This file is part of open-alya.                                      #
#                                                                       #
#  open-alya is free software: you can redistribute it and/or modify    #
#  it under the terms of the GNU General Public License as published by #
#  the Free Software Foundation, either version 3 of the License, or    #
#  (at your option) any later version.                                  #
#                                                                       #
#  open-alya is distributed in the hope that it will be useful,         #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        # 
#  GNU General Public License for more details.                         #
#                                                                       #
#  You should have received a copy of the GNU General Public License    #
#  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   #
#                                                                       #
#########################################################################



#  
export TAU_PATH=/home/bsc21/bsc21704/z2016/REPOSITORY/TAU/TAU2251/EXEC2/x86_64
export TAU_MAKEFILE=$TAU_PATH/lib/Makefile.tau-icpc-mpi-pdt
export PATH=$PATH:$TAU_PATH/bin


### Tracing with Jumpshot (export TAU_TRACE=1)  
# 1)
tau_treemerge.pl
# 2)
tau2slog2 tau.trc tau.edf -o tau.slog2
# 3)
tar -czvf tau.tar tau.slog2



Exec01()
{
  rm F$1xS$2.ppk
  paraprof --pack F$1xS$2.ppk
  rm profile.*
  paraprof -v F$1xS$2.ppk

  pprof -t -f profile.Mean            > F$1xS$2.tau   
  pprof -t -f profile.Mean,AllThreads > F$1xS$2.tau2
 #paraprof F$1xS$2.ppk  
}

ALYA_PATH=/home/bsc21/bsc21704/z2016/REPOSITORY/ALYAs/ALYA_2016DEC23/Executables/plepp03

ALYAi=256  
ALYAj=32  

#Exec01 $ALYAi $ALYAj  


