!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



!-----------------------------------------------------------------------
!> @addtogroup Communication_Toolbox
!> @{
!> @file    def_mpi.f90
!> @author  houzeaux
!> @date    2022-08-30
!> @brief   MPI
!> @details MPI definitions
!-----------------------------------------------------------------------

module def_mpi

  use def_kintyp_basic, only : ip,rp
#include "def_mpi.inc"

  !----------------------------------------------------------------------
  !
  ! Fortran MPI bindings
  !
  !----------------------------------------------------------------------

#ifndef MPI_OFF
#if defined USEMPIF08
  !
  ! MPI module
  !
  use mpi_f08
#elif defined  MPIFH
  !
  ! MPI include
  !
  include 'mpif.h'  
#else
  !
  ! MPI include (default)
  !
  include 'mpif.h'
#endif
#else
  !
  ! No MPI
  !
  integer, parameter :: MPI_SUCCESS     =  0
  integer, parameter :: MPI_REAL4       =  0
  integer, parameter :: MPI_REAL8       =  0
  integer, parameter :: MPI_INTEGER4    =  0
  integer, parameter :: MPI_INTEGER8    =  0
  integer, parameter :: MPI_STATUS_SIZE =  1
  integer, parameter :: MPI_FILE_NULL   = -1
  integer, parameter :: MPI_COMM_NULL   = -1
#endif

  !----------------------------------------------------------------------
  !
  ! Null communicator and generic status
  !
  !----------------------------------------------------------------------
  
  MY_MPI_COMM     :: PAR_COMM_NULL
  
#ifdef USEMPIF08
  MY_MPI_STATUS   :: status  
#else
  MY_MPI_STATUS   :: status(MPI_STATUS_SIZE)
#endif
  
end module def_mpi
!> @}




  
