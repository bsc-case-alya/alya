!-----------------------------------------------------------------------!
!                                                                       ! 
!  This file is part of open-alya.                                      ! 
!                                                                       ! 
!  open-alya is free software: you can redistribute it and/or modify    ! 
!  it under the terms of the GNU General Public License as published by ! 
!  the Free Software Foundation, either version 3 of the License, or    ! 
!  (at your option) any later version.                                  ! 
!                                                                       ! 
!  open-alya is distributed in the hope that it will be useful,         ! 
!  but WITHOUT ANY WARRANTY; without even the implied warranty of       ! 
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ! 
!  GNU General Public License for more details.                         ! 
!                                                                       ! 
!  You should have received a copy of the GNU General Public License    ! 
!  along with open-alya. If not, see <https://www.gnu.org/licenses/>.   ! 
!                                                                       ! 
!-----------------------------------------------------------------------!



subroutine vecpro(v1,v2,v3,n)

!-----------------------------------------------------------------------
!
! Two and three-dimensional vectorial product of two vectors  v3 = v1 x v2.
! The same pointer as for v1 or v2 may be used for v3. If N = 2, it is
!  assumed that v1 = (0,0,v1_3) and v2 = (v2_1,v2_2,0).      
!
!-----------------------------------------------------------------------
  use      def_kintyp, only : ip,rp
  implicit none
  integer(ip), intent(in)  :: n
  real(rp),    intent(in)  :: v2(n),v1(3)
  real(rp),    intent(out) :: v3(n)
  real(rp)                 :: c1,c2,c3

  if(n==2) then
     c1=-v1(3)*v2(2)
     c2= v1(3)*v2(1)
     v3(1)=c1
     v3(2)=c2
  else if(n==3) then
     c1=v1(2)*v2(3)-v1(3)*v2(2)
     c2=v1(3)*v2(1)-v1(1)*v2(3)
     c3=v1(1)*v2(2)-v1(2)*v2(1)
     v3(1)=c1
     v3(2)=c2
     v3(3)=c3
  end if
  
end subroutine vecpro
